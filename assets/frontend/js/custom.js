
$(document).ready(function(){


});

function addonlyJSvat(price) {
    var num = parseFloat(price);
    var percentage = 15;
    num += num * (percentage / 100);
    return (Math.round(num * 100) / 100).toFixed(2);
}


function openNav() {
  document.getElementById("mySidepanel").style.width = "250px";
  document.getElementById("mySidepanel").classList.add("menuopen");
}

function closeNav() {
  document.getElementById("mySidepanel").style.width = "0";
    document.getElementById("mySidepanel").classList.add("menuopen");
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$(document).ready(function(){
$(window).scroll(function() {
    if ($(this).scrollTop() > 1){  
        $('.topNav').addClass("sticky");
    }
    else{
        $('.topNav').removeClass("sticky");
    }
});


});
$(document).ready(function(){
    $(window).scroll(function() {
        if ($(this).scrollTop() > 1){
            $('.stickymenu').addClass("sticky");
        }
        else{
            $('.stickymenu').removeClass("sticky");
        }
    });


});

$(document).ready(function(){

    if ($(window).height()) {
        var hh=$( window ).height();
        $('div.topMenu.vertical>.menu').height(hh+"px");
    }

  $(window).scroll(function() {
    if ($(this).scrollTop() > 1){  
        $('.topBar').addClass("sticky");
    }
    else{
        $('.topBar').removeClass("sticky");
    }
});
 });

$('.hamBergerMenuIcon').on('click', function (){
    $('html').toggleClass("fullscreen-menu");
    $(this).toggleClass("change");
});
$('.openMenuShadowDrop').on('click', function (){
    $('html').removeClass("fullscreen-menu");
    $('.hamBergerMenuIcon').removeClass("change");
});
$('.shoppingCart .header .cart').on('click', function (){
    $('html').removeClass("fullscreen-menu");
    $(".hamBergerMenuIcon").removeClass("change");
    $('html').addClass("sc-fullscreen");
    $('.app.catalog').addClass("shoppingCartIsExpanded");
    $('.shoppingCartWrapper').addClass("shoppingCartWrapperExpanded");
    $('.shoppingCart').removeClass("collapsed");
    $('.shoppingCart').addClass("expanded");
    if ($(window).height()) {
        var hh=$( window ).height()-200;
        $('.shoppingCart div.body').height(hh+"px");
    }

});

$('.stickyHeader').on('click', function (){
    $('html').addClass("sc-fullscreen");
    $('.app.catalog').addClass("shoppingCartIsExpanded");
    $('.shoppingCartWrapper').addClass("shoppingCartWrapperExpanded");
    $('.shoppingCart').removeClass("collapsed");
    $('.shoppingCart').addClass("expanded");
    if(lang=="ar"){
        $('.everythingElseWrapper').css("padding-left", "315px");
    }else{
        $('.everythingElseWrapper').css("padding-right", "315px");
    }
    if ($(window).height()) {
        var hh=$( window ).height()-200;
        $('.shoppingCart div.body').height(hh+"px");
    }

});


$('.shoppingCartButton, .closeCartButtonTop').on('click', function (){
    $('html').removeClass("sc-fullscreen");
    $('.app.catalog').removeClass("shoppingCartIsExpanded");
    $('.shoppingCartWrapper').removeClass("shoppingCartWrapperExpanded");
    $('.shoppingCart').addClass("collapsed");
    $('.shoppingCart').removeClass("expanded");
    if(lang=="ar"){
        $('.everythingElseWrapper').css("padding-left", "0px");
    }else{
        $('.everythingElseWrapper').css("padding-right", "0px");
    }
});

$('.hamburgerMenu').on('click', function (){
    $('.app.catalog').toggleClass("navOpen");
});
// popMenuModal
$('.loginactionlink').on('click', function (){
    $('#popMenuModal').modal('toggle');
});


// fly to cart function
let count = 0;
//if add to cart btn clicked
$('.btn-cart').on('click', function (){
    count=parseInt($(".itemCount .totalqty").html());
    let cart = $('.stickyHeader');
    if ($(window).width() < 768) {
        cart = $('.cart');
    }
    let imgtodrag="";
    if(pagename=="product_details"){
        imgtodrag = $(".product-details-slider .pimgTp li img").eq(0);
    }else{
        imgtodrag = $(this).parent().find("img.img-fluid-product").eq(0);
    }

    // find the img of that card which button is clicked by user

    if (imgtodrag.length) {
        // duplicate the img
        var imgclone = imgtodrag.clone().offset({
            top: imgtodrag.offset().top,
            left: imgtodrag.offset().left
        }).css({
            'opacity': '0.8',
            'position': 'absolute',
            'height': '150px',
            'width': '150px',
            'z-index': '100'
        }).appendTo($('body')).animate({
            'top': cart.offset().top + 20,
            'left': cart.offset().left + 30,
            'width': 75,
            'height': 75
        }, 1000, 'easeInOutExpo');

        setTimeout(function(){
            count++;
            $(".itemCount .totalqty").html(count);
        }, 1500);

        imgclone.animate({
            'width': 0,
            'height': 0
        }, function(){
            $(this).detach()
        });
    }
});
