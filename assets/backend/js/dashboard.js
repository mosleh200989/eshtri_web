$(document).on('click', '.panel-heading span.clickable', function(e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        //$this.parents('.panel').find('.panel-body').slideUp();
        $this.parents('.panel').find('.table').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        //$this.parents('.panel').find('.panel-body').slideDown();
        $this.parents('.panel').find('.table').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});

$(document).on('click', '.panel div.clickable', function(e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        //$this.parents('.panel').find('.panel-body').slideUp();
        $this.parents('.panel').find('.table').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        //$this.parents('.panel').find('.panel-body').slideDown();
        $this.parents('.panel').find('.table').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});

/* clicked state - closed panel
$(document).ready(function () {
    $('.panel-heading span.clickable').click();
    $('.panel div.clickable').click();
});*/
function resetFormValue(defaultForm) {
    $(':input', defaultForm)
        //.not(':button, :submit, :reset, :hidden')
        .not(':button,:submit,:reset,:radio,:checkbox')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
    // if ($('.selectTwo').length) {
    //     $('.selectTwo').select2('val', '');
    // }
}

function formAfterSubmit(btn, processTxt) {
    btn.children().addClass('fa-spin');
    btn.contents().first().replaceWith(processTxt);
    btn.prop('disabled', true);
}

function saveBtnClick(btn) {
    btn.click(function() {
        var btn = $(this);
        btn.children().addClass('fa-spin');
        btn.contents().first().replaceWith(" Processing... ");
        btn.prop('disabled', true);
    });
}

function resetBtnClickFn(btn, defaultForm, controllerRdAbleName) {
    btn.click(function() {
        resetFormValue(defaultForm);
        $('#headerTitle').html(controllerRdAbleName);
        $("label.error").hide();
        $(".error").removeClass("error");
        var btn = $('#save-btn');
        btn.children().removeClass('fa-spin');
        btn.contents().first().replaceWith(" Save... ");
        btn.prop('disabled', false);
    });
}

function resetFormByBtn(btn, defaultForm, controllerRdAbleName, saveTxt) {
    btn.click(function() {
        resetFormValue(defaultForm);
        $('#headerTitle').html(controllerRdAbleName);
        $("label.error").hide();
        $(".error").removeClass("error");
        var btn = $('#save-btn');
        btn.children().removeClass('fa-spin');
        btn.contents().first().replaceWith(saveTxt);
        btn.prop('disabled', false);
        $('#defaultMsgDiv').html('');
        // if ($('.selectTwo').length) {
        //     $(".selectTwo").select2('val', '');
        // }
    });
}

function formSubmitReturnMsg(btn, data, listTable, defaultForm, controllerRdAbleName, saveTxt) {

    if (data.isError == true) {
        btn.children().removeClass('fa-spin');
        btn.contents().first().replaceWith(saveTxt);
        btn.prop('disabled', false);
        $('#defaultMsgDiv').html(createMessageDiv("error-msg", data.message));
    } else {
        $('#defaultMsgDiv').html(createMessageDiv("success-msg", data.message));
        btn.children().removeClass('fa-spin');
        btn.contents().first().replaceWith(saveTxt);
        btn.prop('disabled', false);
        if (listTable) {
            listTable.DataTable().ajax.reload();
        }

        $('#headerTitle').html(controllerRdAbleName);
        resetFormValue(defaultForm);
    }
}

function createMessageDiv(returnMsg, message) {
    return '<div id="return-message" class="font-bold alert ' + returnMsg + ' alert-dismissable">' +
        '<button class="close close-btn" type="button" data-dismiss="alert" aria-hidden="true">×</button>' +
        '<p class="text-center">' + message + '</p> </div>'
}

function serverErrorToast(message) {
    function showmessage() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };
        toastr.error('Error....', 'Server Status...');
    }
    showmessage();
}

function getFormattingDate(dateObj) {
    if (dateObj == '' || dateObj == null) {
        return ''
    }
    var day = dateObj.getDate();
    var monthIndex = dateObj.getMonth();
    var year = dateObj.getFullYear();
    return (day + '-' + monthIndex + '-' + year);
}

function confirmRowMessages(isError, message, listTable) {
    toastr.options = {
        closeButton: true,
        progressBar: true,
        showMethod: 'slideDown',
        timeOut: 4000
    };
    if (isError == false) {
        listTable.DataTable().ajax.reload();
        toastr.success(message, 'Success...');
    } else {
        toastr.warning(message, 'Failed...');
    }

}

function confirmNormalMessages(isError, message, reload) {
    toastr.options = {
        closeButton: true,
        progressBar: true,
        showMethod: 'slideDown',
        timeOut: 4000
    };
    if (isError == false) {
        if(reload == true){
            location.reload();
        }
        toastr.success(message, 'Success...');
    } else {
        toastr.warning(message, 'Failed...');
    }
    $("#loader-div").hide();
}
//fix modal force focus
$.fn.modal.Constructor.prototype.enforceFocus = function() {
    var that = this;
    $(document).on('focusin.modal', function(e) {
        if ($(e.target).hasClass('select2-input')) {
            return true;
        }

        if (that.$element[0] !== e.target && !that.$element.has(e.target).length) {
            that.$element.focus();
        }
    });
};

function nullCheck(checkvalue) {
    checkvalue = (checkvalue == null || checkvalue == "null") ? '' : checkvalue
    return checkvalue;
}

function isItNull(checkvalue) {
    return (checkvalue == null || checkvalue == '') ? true : false;
}

function floatConverter(value) {
    if (value == null || value == 'null' || value == '' || value == 'NULL') {
        return parseFloat('0');
    } else {
        return parseFloat(value);
    }
}

function getActionButtons(nRow, aData) {
    var actionButtons = "";
    var colMd = Math.floor(12 / (aData.accessibleUrl.length));
    var iconName = '';
    $.each(aData.accessibleUrl, function(key, value) {
        iconName = value.iconName;
        if (value.actionName == 'activate' && aData.activeStatus == 'Inactive') {
            iconName = 'fa fa-check-square-o text-navy';
        }
        actionButtons += '<span class="col-md-' + colMd + ' no-padding"><a href="" referenceId="' + aData.DT_RowId + '" class="' + value.actionName + '-reference" data-title=\"' + value.titleName + '\">';
        actionButtons += '<i class=\"' + iconName + '\"></i>';
        actionButtons += '</a>&nbsp;</span>';
    });
    return actionButtons;
}

function getOfferActionButtons(nRow, aData) {
    var actionButtons = "";
    var colMd = Math.floor(12 / (aData.accessibleUrl.length));
    var iconName = '';
    $.each(aData.accessibleUrl, function(key, value) {
        iconName = value.iconName;
        if (value.actionName == 'activate' && aData.activeStatus == 'Inactive') {
            iconName = 'fa fa-check-square-o text-navy';
        }
        actionButtons += '<span class="col-md-' + colMd + ' no-padding"><a href="" referenceId="' + aData.DT_RowId + '" shop_id="' + aData.shop_id + '" scope="' + aData.scope + '" class="' + value.actionName + '-reference" data-title=\"' + value.titleName + '\">';
        actionButtons += '<i class=\"' + iconName + '\"></i>';
        actionButtons += '</a>&nbsp;</span>';
    });
    return actionButtons;
}

function deleteMessages(isError, message, listTable, selectRow) {
    toastr.options = {
        closeButton: true,
        progressBar: true,
        showMethod: 'slideDown',
        timeOut: 4000
    };
    if (isError == false) {
        listTable.DataTable().row(selectRow).remove().draw(false);
        toastr.success(message, 'Success...');
    } else {
        toastr.warning(message, 'Failed...');
    }

}

function confirmRowMessages(isError, message, listTable) {
    toastr.options = {
        closeButton: true,
        progressBar: true,
        showMethod: 'slideDown',
        timeOut: 4000
    };
    if (isError == false) {
        listTable.DataTable().ajax.reload();
        toastr.success(message, 'Success...');
    } else {
        toastr.warning(message, 'Failed...');
    }

}