<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for about table
 */
class Hitsinfotble extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'info_table_name', 'id', 'hits_info' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// push_noti_token_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}

		if ( isset( $conds['page'] )) {
			$this->db->where( 'page', $conds['page'] );
		}

        if ( isset( $conds['ip_address'] )) {
            $this->db->where( 'ip_address', $conds['ip_address'] );
        }
        if ( isset( $conds['user_agent'] )) {
            $this->db->where( 'user_agent', $conds['user_agent'] );
        }
		$this->db->order_by( 'id', 'desc' );
		
	}
}