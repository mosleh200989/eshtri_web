<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for Transactionstatustracking table
 */
class Timeslotsdetails extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'mk_time_slots_details', 'id', 'time_slots_dtls' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// about_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}
		if ( isset( $conds['slot_date'] )) {
			$this->db->where( 'slot_date', $conds['slot_date'] );
		}
//        if ( isset( $conds['current_time'] )) {
//            $this->db->where( 'slot_date', $conds['slot_date'] );
//        }
		$this->db->order_by( 'id', 'asc' );
	
	}
}