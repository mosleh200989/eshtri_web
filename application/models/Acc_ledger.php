<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for about table
 */
class Acc_ledger extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'acc_ledger', 'id', 'acc_ledger' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// push_noti_token_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
        }
        
        if ( isset( $conds['acc_coa_id'] )) {
			$this->db->where( 'acc_coa_id', $conds['acc_coa_id'] );
        }
        
        if ( isset( $conds['acc_period_id'] )) {
			$this->db->where( 'acc_period_id', $conds['acc_period_id'] );
		}

		$this->db->order_by( 'id', 'desc' );
		
	}
}