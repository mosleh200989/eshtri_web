<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for about table
 */
class Acc_coa_locale extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'acc_coa_locale', 'id', 'acc_coa_locale' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// push_noti_token_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}
		// push_noti_token_id condition
		if ( isset( $conds['language_enum'] )) {
			$this->db->where( 'language_enum', $conds['language_enum'] );
		}
        
        if ( isset( $conds['coa_id'] )) {
			$this->db->where( 'coa_id', $conds['coa_id'] );
		}
		$this->db->order_by( 'id', 'desc' );
		
	}
}