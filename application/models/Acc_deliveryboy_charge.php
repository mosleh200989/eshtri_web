<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for about table
 */
class Acc_deliveryboy_charge extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'acc_deliveryboy_charge', 'id', 'acc_deliveryboy_charge' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// push_noti_token_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}
		
		if ( isset( $conds['driver_id'] )) {
			$this->db->where( 'driver_id', $conds['driver_id'] );
		}

		$this->db->order_by( 'id', 'desc' );
		
	}
}