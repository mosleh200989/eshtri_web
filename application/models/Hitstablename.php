<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for about table
 */
class Hitstablename extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'hits_table_name', 'id', 'hits_' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// push_noti_token_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}

		if ( isset( $conds['page'] )) {
			$this->db->where( 'page', $conds['page'] );
		}

        if ( isset( $conds['count'] )) {
            $this->db->where( 'count', $conds['count'] );
        }

		$this->db->order_by( 'id', 'desc' );
		
	}
}