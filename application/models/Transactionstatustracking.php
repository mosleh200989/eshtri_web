<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for Transactionstatustracking table
 */
class Transactionstatustracking extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'mk_transaction_status_traking', 'id', 'trans_sts' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// about_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}
		
		if ( isset( $conds['title'] )) {
			$this->db->where( 'title', $conds['title'] );
		}

		if ( isset( $conds['transactions_header_id'] )) {
			$this->db->where( 'transactions_header_id', $conds['transactions_header_id'] );
		}

		if ( isset( $conds['transactions_status_id'] )) {
			$this->db->where( 'transactions_status_id', $conds['transactions_status_id'] );
		}
	}
}