<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for city table
 */
class Chat_message extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'chat_message', 'id', 'chat' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
	
		// city id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );	
		}

		// city name condition
		if ( isset( $conds['user_id'] )) {
			$this->db->where( 'user_id', $conds['user_id'] );
		}

		// shop id condition
		if ( isset( $conds['order_id'] )) {
			$this->db->where( 'order_id', $conds['order_id'] );
		}

		// shop id condition
		if ( isset( $conds['message_type'] )) {
			$this->db->where( 'message_type', $conds['message_type'] );
		}
		// search_term
		if ( isset( $conds['searchterm'] )) {
			
			if ($conds['searchterm'] != "") {
				$this->db->group_start();
				$this->db->like( 'message', $conds['searchterm'] );
				$this->db->or_like( 'message', $conds['searchterm'] );
				$this->db->group_end();
			}
			
			}

		$this->db->order_by( 'created_at', 'desc' );

	}
}
	