<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for about table
 */
class Acc_journal_voucher  extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'acc_journal_voucher ', 'id', 'acc_journal_voucher' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds_journal_voucher( $conds = array())
	{
		// push_noti_token_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}

		// notification_name condition
		if ( isset( $conds['code'] )) {
			$this->db->where( 'code', $conds['code'] );
		}

		// notification description condition
		if ( isset( $conds['acc_period'] )) {
			$this->db->where( 'acc_period', $conds['acc_period'] );
		}

		if ( isset( $conds['searchterm'] )) {
			$this->db->like( 'message', $conds['searchterm'] );
		}
		$this->db->order_by( 'caption', 'desc' );
		
	}
}