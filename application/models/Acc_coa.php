<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for about table
 */
class Acc_coa extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'acc_coa', 'id', 'acc_coa' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// push_noti_token_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}
		// push_noti_token_id condition
		if ( isset( $conds['journals_type'] )) {
			$this->db->where( 'journals_type', $conds['journals_type'] );
		}
		// push_noti_token_id condition
		if ( isset( $conds['code'] )) {
			$this->db->where( 'code', $conds['code'] );
		}
		$this->db->order_by( 'id', 'desc' );
		
	}
}