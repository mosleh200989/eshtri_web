<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for about table
 */
class Acc_period extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'acc_period', 'id', 'acc_period' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// push_noti_token_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}
		
		if ( isset( $conds['is_current'] )) {
			$this->db->where( 'is_current', $conds['is_current'] );
		}

		$this->db->order_by( 'id', 'desc' );
		
	}

	public function createData($data)
	{
		$query = $this->db->insert('person',$data);
		return $query;
	}
}