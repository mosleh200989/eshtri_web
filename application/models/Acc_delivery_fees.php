<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for about table
 */
class Acc_delivery_fees extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'acc_delivery_fees', 'id', 'acc_delivery_fees' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// push_noti_token_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}
		if ( isset( $conds['is_current'] )) {
			$this->db->where( 'is_current', $conds['is_current'] );
		}
        if ( isset( $conds['scope'] )) {
            $this->db->where( 'scope', $conds['scope'] );
        }
        if ( isset( $conds['shop_id'] )) {
            $this->db->where( 'shop_id', $conds['shop_id'] );
        }
		$this->db->order_by( 'id', 'desc' );
		
	}
}