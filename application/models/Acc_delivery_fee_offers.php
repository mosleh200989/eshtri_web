<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for about table
 */
class Acc_delivery_fee_offers extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'acc_delivery_fee_offers', 'id', 'acc_delivery_fee_offers' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// push_noti_token_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}
		if ( isset( $conds['is_current'] )) {
			$this->db->where( 'is_current', $conds['is_current'] );
		}
		if ( isset( $conds['offerType'] )) {
			$this->db->where( 'offerType', $conds['offerType'] );
		}
		if ( isset( $conds['offerBy'] )) {
			$this->db->where( 'offerBy', $conds['offerBy'] );
		}
        if ( isset( $conds['scope'] )) {
            $this->db->where( 'scope', $conds['scope'] );
        }
		if ( isset( $conds['shop_id'] )) {
			$this->db->where( 'shop_id', $conds['shop_id'] );
		}
		if ( isset( $conds['freeFirstCustomer'] )) {
			$this->db->where( 'freeFirstCustomer', $conds['freeFirstCustomer'] );
		}
		if ( isset( $conds['freeFirstDate'] )) {
			$this->db->where( 'freeFirstDate', $conds['freeFirstDate'] );
		}
		if ( isset( $conds['percentDiscountAll'] )) {
			$this->db->where( 'percentDiscountAll', $conds['percentDiscountAll'] );
		}
		if ( isset( $conds['percentDiscountAmnt'] )) {
			$this->db->where( 'percentDiscountAmnt', $conds['percentDiscountAmnt'] );
		}
		if ( isset( $conds['ifExceedAmount'] )) {
			$this->db->where( 'ifExceedAmount', $conds['ifExceedAmount'] );
		}
		if ( isset( $conds['eligibleFreeAmount'] )) {
			$this->db->where( 'eligibleFreeAmount', $conds['eligibleFreeAmount'] );
		}

		$this->db->order_by( 'id', 'desc' );
		
	}
}