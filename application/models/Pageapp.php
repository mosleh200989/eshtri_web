<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for category table
 */
class Pageapp extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'pageapp', 'id', 'page' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// img_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}
		// img_type condition
		if ( isset( $conds['pg_title'] )) {
			$this->db->where( 'pg_title', $conds['pg_title'] );
		}

		// img_parent_id condition
		if ( isset( $conds['pg_slug'] )) {
			$this->db->where( 'pg_slug', $conds['pg_slug'] );
		}

		// img_path condition
		if ( isset( $conds['pg_status'] )) {
			$this->db->where( 'pg_status', $conds['pg_status'] );
		}
        // img_path condition
		if ( isset( $conds['pg_descri'] )) {
            $this->db->like( 'pg_descri', $conds['pg_descri'] );
        }
		// is_default condition
		if ( isset( $conds['pg_foot'] )) {
			$this->db->where( 'pg_foot', $conds['pg_foot'] );
		}
	}
}