<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for about table
 */
class Acc_paymentgateway_fees extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'acc_paymentgateway_fees', 'id', 'acc_paymentgateway_fees' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// push_noti_token_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}
		if ( isset( $conds['name'] )) {
			$this->db->where( 'name', $conds['name'] );
		}

		$this->db->order_by( 'id', 'desc' );
		
	}
}