<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for about table
 */
class Acc_vcr_dtl extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'acc_vcr_dtl', 'id', 'acc_vcr_dtl' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// push_noti_token_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}
		if ( isset( $conds['vcr_mst_id'] )) {
			$this->db->where( 'vcr_mst_id', $conds['vcr_mst_id'] );
		}
		if ( isset( $conds['drcr_type'] )) {
			$this->db->where( 'drcr_type', $conds['drcr_type'] );
		}
		$this->db->order_by( 'id', 'desc' );
		
	}
}