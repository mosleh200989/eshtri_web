<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for Timeslotsnumber table
 */
class Timeslotsnumber extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'mk_time_slots_number', 'id', 'time_slots_nbr' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// about_id condition
		if ( isset( $conds['id'] )) {
			$this->db->where( 'id', $conds['id'] );
		}
		if ( isset( $conds['is_new'] )) {
			$this->db->where( 'is_new', $conds['is_new'] );
		}
	}
}