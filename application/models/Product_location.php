<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for country table
 */
class Product_location extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'mk_product_location', 'id', 'product_location' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		 // default where clause
		// if ( !isset( $conds['no_publish_filter'] )) {
		// 	$this->db->where( 'status', 1 );
		// }

		// country name condition
		if ( isset( $conds['name'] )) {
			$this->db->where( 'name', $conds['name'] );
		}

		if ( isset( $conds['name_alt'] )) {
			$this->db->where( 'name_alt', $conds['name_alt'] );
		}
		// shop_id condition
		if ( isset( $conds['shop_id'] )) {
			$this->db->where( 'shop_id', $conds['shop_id'] );
		}
		
		// searchterm
		if ( isset( $conds['searchterm'] )) {
			$this->db->group_start();
			$this->db->like( 'name', $conds['searchterm'] );
			$this->db->or_like( 'name', $conds['searchterm'] );
			$this->db->or_like( 'name_alt', $conds['searchterm'] );
			$this->db->group_end();
		}

		$this->db->order_by( 'id', 'desc' );
	}
}