<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model class for api table
 */
class User_location extends PS_Model {

	/**
	 * Constructs the required data
	 */
	function __construct() 
	{
		parent::__construct( 'user_locations', 'id', 'userlocation' );
	}

	/**
	 * Implement the where clause
	 *
	 * @param      array  $conds  The conds
	 */
	function custom_conds( $conds = array())
	{
		// api_id condition
		if ( isset( $conds['id'] )) {
			
			$this->db->where( 'id', $conds['id'] );
		}

		// api_constant condition
		if ( isset( $conds['user_id'] )) {
			
			$this->db->where( 'user_id', $conds['user_id'] );
		}

			// api_constant condition
		if ( isset( $conds['latitude'] )) {
			
			$this->db->where( 'latitude', $conds['latitude'] );
		}

		if ( isset( $conds['longitude'] )) {
			
			$this->db->where( 'longitude', $conds['longitude'] );
		}
		$this->db->where("DATE(`created_at`) >=CURDATE() - INTERVAL 24 HOUR");
		// $this->db->where( 'created_at', $conds['longitude'] );
		// date("Y-m-d H:i:s");
	}
}