<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Insert']	= 'Insert';
$lang['Initialize_new_ledger']	= 'Initialize new ledger';
$lang['SL']	= 'SL';
$lang['Code'] = 'Code';
$lang['Period_Type'] = 'Period Type';
$lang['Start_date']	= 'Start date';
$lang['Close_Date']	= 'Close Date';
$lang['Is_Current']	= 'Is Current';
$lang['Is_Close']	= 'Is Close';
$lang['Action']	= 'Action';
$lang['Year']	= 'Year';
$lang['Remark']	= 'Remark';
$lang['Reset']	= 'Reset';
$lang['Monthly_Period']	= 'Monthly Period';
$lang['Yearly_Period']	= 'Yearly Period';
$lang['Cancel']	= 'Cancel';
$lang['to']	= 'to';
$lang['Is_Open'] = 'Is Open';
$lang['Save'] = 'Save';
$lang['Save'] = 'Save';


