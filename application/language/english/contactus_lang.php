<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['cContactUs']	= 'Contact Us';
$lang['cCustomer_service']	= 'Customer service and support teams work 24 hours 7 days a week to provide support. answer your questions and help you.';
$lang['cName']	= 'Name';
$lang['ceMobile']	= 'Mobile Number';
$lang['ceEmail']	= 'Email';
$lang['cMessage']	= 'Message';
$lang['cContact_Information']	= 'Contact Information';
$lang['cOur_Address']	= 'Kingdom of Saudi Arabia - Jeddah - Ruwais - KING ABDULLAH BRANCH RD - ASHBELI ALMTMEZ TOWER 23213';
$lang['cMobile']	= '009665536176364';
$lang['cEmail']	= 'info@alama360.com';
$lang['cName_placeholder']	= 'Enter name here';
$lang['ceMobile_placeholder']	= 'Enter mobile number here';
$lang['ceEmail_placeholder']	= 'Enter your email here';
$lang['cMessage_placeholder']	= 'Enter messages here';
