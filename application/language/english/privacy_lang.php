<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Pprivacy']	= 'Privacy policy';
$lang['PSprivacy']	= 'Your use of the eShtri application and your provision of your personal information to us is tantamount to agreeing to the terms mentioned in the Privacy Policy, with reference to “the company” or “we”, it represents the eShtri company and is registered in the Kingdom of Saudi Arabia. As for “you” or “the user” it represents any person who Using the Eshtri application. If you do not agree to these terms, you should stop using the app.';
$lang['PSafe']	= 'Is your information safe?';
$lang['PsSafe']	= 'In the event you browse the store, we will not collect any personal data about you except when you decide to use the service and benefit from it, not for the purpose of browsing and discovery only, and in the event that you use the service you must be assured that we are doing our best to protect your personal data and prevent illegal or unauthorized access It and even protect it from treatment, change and loss. In spite of taking various protection measures and working to improve them, no system or technology can remain completely safe from damages, so if you feel any vulnerability within the application, you can contact us to work to solve this problem through the following e-mail info@alama360.com';
$lang['Ppaymethod']	= 'How is the process of processing electronic payment within the application?';
$lang['PSpaymethod']	= 'eShtri uses a trusted third party to process electronic payment transactions, and these transactions do not apply to people who choose to "pay on receipt" during the shopping process. This third party may request information about your credit card and may require you to access your contact list.';
$lang['PShare']	= 'Do we share your personal data with any other parties?';
$lang['PsShare']	= 'The privacy of eShtri users is one of the most important things that we are trying to protect in all possible ways, as we never sell or share this data nor do we disclose or share it with any third party for any purpose, and the goals that we collect this data for are statistical goals, not only as numbers Users and their services.';
$lang['Pprem']	= 'What are the required permissions from the user?';
$lang['Psprem']	= 'Access to phone status: where it was requested from a third party to complete the electronic payment process within the application Access to the camera: where your mobile camera is accessed for shopping by scanning the barcode of the product, and you can add any product that does not exist by photographing it with a private mobile camera Your access to private accounts for users: In order to customize the user experience, access your site to facilitate the process of delivering requests and calculating the time required to deliver the request to your address Access the Internet';
$lang['POwner']	= 'The right to amend the privacy policy';
$lang['PsOwner']	= 'eShtri has the full right to update eShtri privacy policy, whether by adding or removing new terms or by modifying existing terms at any time. How do we notify eShtri users of any updates to the privacy policy? We do not send any notification or email about updates to the privacy policy, but you can always see any update by checking the date of the last update of this policy, which is in the last line on this page.';
$lang['PHow']	= 'How do we notify eShtri users of any updates to the privacy policy?';
$lang['PsHow']	= 'We do not send any notification or email about updates to the privacy policy, but you can always see any update by checking the date of the last update of this policy, which is in the last line on this page.';
$lang['PUpDate']	= 'Last Update 26/04/2020';
