<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['WeCreated']	= 'We Created';
$lang['UniqueExp']	= 'Unique Experience';
$lang['onlineshop']	= 'The way shopping from stores varied and merged with fun from the moment you start shopping, to receiving your happy delivery as you wanted.';
$lang['Idea']	= 'Idea';
$lang['Team']	= 'Team';
$lang['Experience']	= 'Experience';
$lang['eshtridesc']	= 'The idea of the "eShtri" project came after we did a study of the pandemic-affected stores in the world. So this project was a solution and an electronic store, which is the first of its kind to purchase the home judge from the most famous stores in your city, to reconnect the stores to their customers electronically and the request through the application of buy and we started in Jeddah by 50% for the first stage and will be directed at the end of this year to activate the service in the rest of the cities of the Kingdom of Saudi Arab.';
$lang['eshtriexp']	= 'A new experience we offer you through the App eShtri starts the experience from the moment an individual starts using the app and purchase sought from our stores, to find the best products of high quality. This increases the customers passion for ordering again. The Experience of eShtri remains one of the most beautiful experiences you can experience in the world of online shopping.';
$lang['eshtriteam']	= 'The eShtri team is made up of a group of passionate and ambitious young people who strive to succeed at all local and international levels in the field of e-commerce. The team includes administrators, developers, designers, customer service, trained operators and equipped to complete sales, purchases and deliveries as well.';


$lang['eshtriexp']	= 'Nana Direct is developed by Saudi youth who aspire to build a sophisticated shopping and delivery system. The delivery is done by a skillful staff who is trained on picking high quality fruit. vegetables and other items. Nana promise fast delivery and high quality.';
$lang['eshtriteam']	= 'The team is composed of an ambitious youth group that strives to achieve success at all local. regional and international levels in the field of e-commerce. The team includes managers. developers. designers. customer service. operators and free lancers trained and equipped to complete sales. purchase and delivery operations.';

