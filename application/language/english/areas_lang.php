<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['CityCover']	= 'We Cover More than 4 Cities over the Kingdom';
$lang['SoonCover']	= 'And Soon in all KSA areas';
$lang['Covered']	= 'Covered Neighbourhood';
$lang['RIYADH']	= 'RIYADH';
$lang['MEDINA']	= 'MEDINA';
$lang['JEDDAH']	= 'JEDDAH';
$lang['MAKKAH']	= 'MAKKAH';

