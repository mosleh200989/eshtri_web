<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Home']	= 'Home';
$lang['CoverageArea']	= 'Coverage Area';
$lang['AboutUs']	= 'About Us';
$lang['ContactUs']	= 'Contact Us';
$lang['DownloadApp']	= 'Download App';
$lang['Login']	= 'Login';
$lang['Workwithus']	= 'Work with us';
