<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Shopping_overwhelming']	= 'With eShtri, shopping is easier.';
$lang['app_dilvery']	= ' Shopping is as easy as sending a whatsapp message. Youre making a call. Marvel at a picture on Instagram. Download the app now and try the fun of shopping from anywhere.';

