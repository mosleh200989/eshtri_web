<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['PAddurStore']	= 'Add Your Store';
$lang['increaseursale']	= 'An opportunity to increase your sales and profits without additional costs';
$lang['greatenefit']	= ' The great benefit of being a contractor with us is that we enable you to acquire new customers, we do so as we mentioned without prior investment or costs for stores and groceries, and it will increase your share of profits and increase your market share as well, and also enable you to keep your customers for a longer period , And we advertise people to know the name of your store and thus you have thus acquired free online marketing without negligible costs, and you are in your place';
$lang['Benefits']	= 'Benefits of adding your store to eshtri';
$lang['Reach']	= 'Reach Thousands of Customers';
$lang['profits']	= 'Increase your sales and profits';
$lang['e-commerce']	= 'Entering the world of e-commerce without additional costs';
$lang['Freemarketing']	= 'Free online marketing for your store';
$lang['In short']	= 'In short, Eshtri provides you with a new way of selling online without any costs Extra or risks';


