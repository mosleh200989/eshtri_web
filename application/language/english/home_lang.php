<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['headtitle']	= 'Wind your Mind and Download the eShtri App';
$lang['suptitle']	= 'Order online and your belongings reach you to the door of your house.';
$lang['hDownload_App']	= 'Download App.';
$lang['Features']	= 'eShtri Features.';
$lang['Featureline']	= 'What distinguishes eShtri is the ease of use, the quality of the service and the multiplicity of options for a special and interesting shopping pleasure.';
$lang['Allproducts']	= 'Multiple Choice.';
$lang['Allproductsdesc']	= 'Large group of stores, which previously could not be ordered and delivered.';
$lang['MethodPaying']	= 'Pay in More Ways Than One';
$lang['MethodPayingdesc']	= 'Pay by cash or by credit cards. You can pay in a safe way through the application or upon receipt.';
$lang['DeliveryPlace']	= 'Delivery to Any Place';
$lang['DeliveryPlacedesc']	= 'In order for our service to you to be fast and excellent, all you have to do is add your address details to receive the order with the highest quality.';
$lang['SchedulOrder']	= 'Scheduling Delivery';
$lang['SchedulOrderdesc']	= 'After you have finished processing your order, we will deliver the order at the time you have chosen.';
$lang['SupportShops']	= 'Support Shops';
$lang['SupportShopsdesc']	= 'eShtri offers you a range of large and medium-sized stores to suit all users requirements.';
$lang['TechnicalSup']	= '24/7 Technical Support';
$lang['TechnicalSupdesc']	= 'Our technical support team are your friends who provide all the necessary support and assistance in order to provide the best possible service.';
$lang['Hshop_method']	= 'The best online shopping method';
$lang['Eshtri_Direct']	= 'eShtri delivery service whatever you want, in your hands whatever your needs, you find it. Requested. And get to you with one app.';
$lang['SoonTM']	= 'Soon eShtri will support all regions of the Kingdom.';
$lang['SoonTMdesc']	= 'eShtri a shopping and delivery service for homes in the Kingdom looking to cover all areas of the Kingdom because you deserve the best. To cover all areas of the Kingdom and expand the regions, we need more time and effort to ensure the highest quality of service. You can browse all the supported areas and add your mobile number to your notification every time we expand to new areas';
$lang['HSupported_loc']	= 'Supported locations';
$lang['HApp_Screen']	= 'Application Screens';
$lang['Hshopkeeper']	= 'Are you a shopkeeper ?';
$lang['marketingur']	= 'Would you like to join us? And reach the hundreds of thousands of users who use the eShtri App to order everything they need? Want to double your sales and increase your profits? Join us and add your store for more orders. We helped us provide the best delivery service to our customers';
$lang['returnPolicy']	= 'Return policy';
$lang['returnPolicydesc']	= 'Return policy description';
$lang['cancelOrder']	= 'Cancel order';
$lang['cancelOrderdesc']	= 'Cancel order description';
$lang['shipmentPolicy']	= 'Shipment policy';
$lang['shipmentPolicydesc']	= 'Shipment policy description';
$lang['more']	= 'More';


