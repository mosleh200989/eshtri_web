<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['PAddurStore']	= 'اضف متجرك';
$lang['increaseursale']	= 'فرصة لزيادة مبيعاتك وأرباحك بدون تكاليف إضافية';
$lang['greatenefit']	= ' إنّ الفائدة الكبيرة من أن تكون أحد المتعاقدين معنا هو أنّ نمكنك من اكتساب عملاء جدد، نحنُ نفعل ذلك كما ذكرنا دون استثمار مسبق أو تكاليف مترتبة على المتاجر والبقالات، كما وأنها ستزيد حصتك من الأرباح وتزيد حصتك السوقية أيضًا، ونمكنك كذلك من الاحتفاظ بعملائك لفترة أكبر، ونقوم على دعاية الناس لمعرفة اسم متجرك وبالتالي أنت هكذا قد اكتسبت تسويقًا مجانيًا عبر الانترنت دون تكاليف تذكر، وأنت في مكانك!';
$lang['Benefits']	= 'فوائد اضافة متجرك لاشتر';
$lang['Reach']	= 'الوصول الى آلاف العملاء';
$lang['profits']	= 'زيادة مبيعاتك وأرباحك';
$lang['e-commerce']	= 'دخول عالم التجارة الإلكترونية بدون تكاليف اضافية';
$lang['Freemarketing']	= 'تسويق الكتروني مجاني لمتجرك';
$lang['Inshort']	= 'بإختصار  ، اشترِ يوفر لك طريقة جديدة للبيع اونلاين دون أي تكاليف اضافية أو مخاطرات';

