<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Home']	= 'الرئيسية';
$lang['CoverageArea']	= 'اماكن التغطية';
$lang['AboutUs']	= 'عن الشركة';
$lang['ContactUs']	= 'اتصل بنا';
$lang['DownloadApp']	= 'حمل التطبيق';
$lang['Login']	= 'تسجيل الدخول';
$lang['Workwithus']	= 'اعمل معنا';
