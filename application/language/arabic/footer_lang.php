<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Connect_With_Us']	= 'تواصل معنا';
$lang['Copyright']	= 'جميع الحقوق محفوظة لتطبيق اشترِ ©️ 2020';
$lang['Privacy']	= 'سياسة الخصوصية';
$lang['Terms']	= 'شروط الإستخدام';
$lang['DownloadApp']	= 'حمّل التطبيق الآن';
