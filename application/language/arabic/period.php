<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Insert']	= 'إدراج';
$lang['Initialize_new_ledger']	= 'تهيئة دفتر الأستاذ الجديد';
$lang['SL']	= 'SL';
$lang['Code'] = 'الشفرة';
$lang['Period_Type'] = 'نوع الفترة';
$lang['Start_date']	= 'تاريخ البدء';
$lang['Close_Date']	= 'تاريخ قريب';
$lang['Is_Current']	= 'حالي';
$lang['Is_Close']	= 'قريب';
$lang['Action']	= 'عمل';
$lang['Year']	= 'عام';
$lang['Remark']	= 'ملاحظة';
$lang['Reset']	= 'إعادة تعيين';
$lang['Monthly_Period']	= 'الدورة الشهرية';
$lang['Yearly_Period']	= 'الفترة السنوية';
$lang['Cancel']	= 'إلغاء';
$lang['to']	= 'إلى';
$lang['Is_Open'] = 'مفتوح';
$lang['Save'] = 'حفظ';


