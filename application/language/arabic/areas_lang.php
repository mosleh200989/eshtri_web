<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['CityCover']	= 'أكثر من ٤ مدينة حول المملكة';
$lang['SoonCover']	= 'وقريبًا في جميع مناطق المملكة';
$lang['Covered']	= 'المدن المدعومة';
$lang['RIYADH']	= 'الرياض';
$lang['MEDINA']	= 'المدينة';
$lang['JEDDAH']	= 'جدة';
$lang['MAKKAH']	= 'مكة';

