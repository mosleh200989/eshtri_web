<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['cContactUs']	= 'اتصل بنا';
$lang['cCustomer_service']	= 'فريق خدمة الزبائن و العملاء يعمل 24 ساعة 7 أيام أسبوعيا للإجابة على استفساراتكم و مساعدتكم';
$lang['cName']	= 'إسم';
$lang['ceMobile']	= 'رقم الجوال';
$lang['ceEmail']	= 'البريد الالكتروني';
$lang['cMessage']	= 'رسالة';
$lang['cContact_Information']	= 'معلومات الاتصال';
$lang['cOur_Address']	= 'المملكة العربية السعودية - جدة - الرويس - طريق الملك عبدالله - برج إشبيلية المتميز 23213';
$lang['cMobile']	= '009665536176364';
$lang['cEmail']	= 'info@alama360.com';
$lang['cName_placeholder']	= 'ادخل الاسم هنا';
$lang['ceMobile_placeholder']	= 'ادخل رقم الجوال هنا';
$lang['ceEmail_placeholder']	= 'ادخل البريد الالكتروني هنا';
$lang['cMessage_placeholder']	= 'ادخل نص الرسالة هناً';
