<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Shopping_overwhelming']	= 'مع اشترِ اصبح التسوق اسهل';
$lang['app_dilvery']	= 'أصبح الحصول على المقاضي امرًا سهلا وبنفس السهولة التي ترسل فيها رسالة بالواتس اب . تجري اتصال. تعجب بصورة على انستقرام. حمل التطبيق الآن وجرب متعة التسوق من أي مكان.';

