<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Read More
 *
 * @param      string  $string   string
 * @param      integer  $limit   character limit
 *
 * @return     string   ( description_of_the_return_value )
 */
 include("sms/includeSettings.php"); 
if ( !function_exists( 'read_more' )) 
{
	function read_more( $string, $limit )
	{
		$string = strip_tags($string);
		
		if (strlen($string) > $limit) {
		
		    // truncate string
		    $stringCut = substr($string, 0, $limit);
		
		    // make sure it ends in a word so assassinate doesn't become ass...
		    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
		}
		return $string;
	}
}

/**
 * transform 'added date' display
 *
 * @param      integer  $time   The time
 *
 * @return     string   ( description_of_the_return_value )
 */
if ( ! function_exists( 'ago' ))
{
	function ago( $time )
	{
		// get ci instance
		$CI =& get_instance();
		//for language
		if($CI->session->userdata('user_language_id')){
			$conds['id'] = $CI->session->userdata('user_language_id');
			$conds['no_publish_filter'] = 1;
		}else{
			$conds['status'] = 1;
		}
		$language = $CI->Language->get_one_by($conds);
		$language_id = $language->id;
		//for today language string
		$conds_today['key'] = "today_label";
		$conds_today['language_id'] = $language_id;
		$today_string = $CI->Language_string->get_one_by( $conds_today );
		$today_now = $just_string->value;
		if ( empty( $time )) return '"'.$today_now.'"';

		// get ci instance
		$CI =& get_instance();
		
		$time = mysql_to_unix( $time );
		$now = $CI->db->query('SELECT NOW( ) as now')->row()->now;
		$now = mysql_to_unix( $now );

		$periods = array("second_ago", "minute_ago", "hour_ago", "day_ago", "week_ago", "month_ago", "year_ago", "decade_ago");
		$lengths = array("60","60","24","7","4.35","12","10");

		$difference = $now - $time;

		for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
			$difference /= $lengths[$j];
		}

		$difference = round($difference);

		if ($difference != 1) {
			// load the language
			$conds_str['key'] = $periods[$j];
			$conds_str['language_id'] = $language_id;
			$lang_string = $CI->Language_string->get_one_by( $conds_str );
			$message = $lang_string->value;
		}
		//for just now language string
		$conds_now['key'] = "just_now_label";
		$conds_now['language_id'] = $language_id;
		$just_string = $CI->Language_string->get_one_by( $conds_now );
		$just_now = $just_string->value;
		//for ago language string
		$conds_ago['key'] = "ago_label";
		$conds_ago['language_id'] = $language_id;
		$ago_string = $CI->Language_string->get_one_by( $conds_ago );
		$ago = $ago_string->value;
		if ($difference==0) {
			return '"'.$just_now.'"';
		} else {
			return "$difference $message $ago";
		}
	}
}
/**
 * return the message
 *
 * @param      <type>  $key    The key
 */
if ( ! function_exists( 'get_msg' ))
{
	function get_msg( $key )
	{
		// get ci instance
		$CI =& get_instance();
		//$conds['status'] = 1;
		if($CI->session->userdata('user_language_id')){
			$conds['id'] = $CI->session->userdata('user_language_id');
			$conds['no_publish_filter'] = 1;
		}else{
			$conds['status'] = 1;
		}
		
		$language = $CI->Language->get_one_by($conds);
		$language_id = $language->id;
		// load the language
		$conds_str['key'] = $key;
		$conds_str['language_id'] = $language_id;
		$lang_string = $CI->Language_string->get_one_by( $conds_str );
		$message = $lang_string->value;
		
		if ( empty( $message )) {
		// if message is empty, return the key
			return $key;
		}

		// return the message
		return $message;
	}
}

if ( ! function_exists( 'getCaption' )) {
    function getCaption($name = "", $name_alt = "")
    {
        $CI =& get_instance();
        if ($CI->session->userdata('site_lang') == 'english') {
            if (isset($name_alt)) {
                if ($name_alt) {
                    return $name_alt;
                } else {
                    return $name;
                }
            } else {
                return $name;
            }
        } else {
            return $name;
        }
    }
}
/**
 * return the message
 *
 * @param      <type>  $key    The key
 */
if ( ! function_exists( 'smtp_config' ))
{
	function smtp_config( )
	{
		// get ci instance
		$CI =& get_instance();
		$smtp_host = $CI->Backend_config->get_one('be1')->smtp_host;
		$smtp_port = $CI->Backend_config->get_one('be1')->smtp_port;
		$smtp_user = $CI->Backend_config->get_one('be1')->smtp_user;
		$smtp_pass = $CI->Backend_config->get_one('be1')->smtp_pass;

		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => $smtp_host,
		    'smtp_port' => $smtp_port,
		    'smtp_user' => $smtp_user, //sender@blog.panacea-soft.com //azxcvbnm
		    'smtp_pass' => $smtp_pass,
		    'mailtype'  => 'text', 
		    'charset'   => 'iso-8859-1'
		);
		
		return $config;
	}
}

/**
 * Show the flash message
 */
if ( ! function_exists( 'flash_msg')) 
{
	function flash_msg()
	{
		// get ci instance
		$CI =& get_instance();

		$CI->load->view( 'common/flash_msg' );
	}
}

/**
 * Shows the analytic.
 */
if ( ! function_exists( 'show_analytic' ))
{
	function show_analytic()
	{
		// get ci instance
		$CI =& get_instance();

		$CI->load->view( 'ps/analytic' );
	}
}

/**
 * Shows the ads.
 */
if ( ! function_exists( 'show_ads' ))
{
	function show_ads()
	{
		// get ci instance
		$CI =& get_instance();

		$CI->load->view( 'ps/ads' );
	}
}

/**
 * Shows the breadcrumb.
 *
 * @param      <type>  $urls   The urls
 */
if ( ! function_exists( 'show_breadcrumb' )) 
{
	function show_breadcrumb( $urls = array() )
	{
		// get ci instance
		$CI =& get_instance();

		$template_path = $CI->config->item( 'be_view_path' );

		// load breadcrumb
		$CI->load->view( $template_path .'/partials/breadcrumb', array( 'urls' => $urls )); 
	}
}

/**
 * Shows the breadcrumb.
 *
 * @param      <type>  $urls   The urls
 */
if ( ! function_exists( 'show_breadcrumb_att_detail' )) 
{
	function show_breadcrumb_att_detail( $urls = array() )
	{
		// get ci instance
		$CI =& get_instance();

		$template_path = $CI->config->item( 'be_view_path' );

		// load breadcrumb
		$CI->load->view( $template_path .'/partials/breadcrumb_attribute', array( 'urls' => $urls )); 
	}
}

/**
 * Shows the data.
 *
 * @param      <type>  $string  The string
 */
if ( ! function_exists( 'show_data' )) 
{
	function show_data( $string )
	{
		// get ci instance
		$CI =& get_instance();
		$CI->load->library( 'PS_Security' );

		return $CI->ps_security->clean_output( $string );
	}
}

/**
 * Determines if view exists.
 *
 * @param      <type>   $path   The path
 *
 * @return     boolean  True if view exists, False otherwise.
 */
if ( ! function_exists( 'is_view_exists' )) 
{
	function is_view_exists( $path )
	{
		return file_exists( APPPATH .'views/'. $path .'.php' );
	}
}

/**
 * Gets the dummy photo.
 *
 * @return     <type>  The dummy photo.
 */
if ( ! function_exists( 'get_dummy_photo' )) 
{
	function get_dummy_photo()
	{
		return "default_news.jpeg";
	}
}

/**
 * Gets the configuration.
 *
 * @param      <type>  $key    The key
 *
 * @return     <type>  The configuration.
 */
if ( ! function_exists( 'get_app_config' )) 
{
	function get_app_config( $key )
	{
		// get ci instance
		$CI =& get_instance();

		$CI->load->model( 'About' );
		$abt = $CI->About->get_one( 'abt1' );

		if ( isset( $abt->{$key} )) {
			return $abt->{$key};
		}

		return false;
	}
}

/**
 * Image URL Path
 *
 * @param      <type>  $path   The path
 *
 * @return     <type>  ( description_of_the_return_value )
 */
if ( ! function_exists( 'img_url' ))
{
	function img_url( $path = false )
	{
		return base_url( '/uploads/'. $path );
	}
}

/**
 * Gets the default photo.
 *
 * @param      <type>  $id     The identifier
 * @param      <type>  $type   The type
 */
if ( ! function_exists( 'get_default_photo' ))
{
	function get_default_photo( $id, $type )
	{
		$default_photo = "";

		// get ci instance
		$CI =& get_instance();

		// get all images
		$img = $CI->Image->get_all_by( array( 'img_parent_id' => $id, 'img_type' => $type ))->result();

		if ( count( $img ) > 0 ) {
		// if there are images for news,
			
			$default_photo = $img[0];
		} else {
		// if no image, return empty object

			$default_photo = $CI->Image->get_empty_object();
		}

		return $default_photo;
	}
}

/**
 * Gets the generate_random_string
 *
 * @param      <type>  $id     The identifier
 * @param      <type>  $type   The type
 */
if ( ! function_exists( 'generate_random_string' ))
{
	function generate_random_string($length = 5) {
	    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
}

if ( ! function_exists( 'checkPageName' )) {
    function checkPageName($page_name)
    {
        $CI =& get_instance();
        // get all images
        $page_info = $CI->Hitstablename->get_one_by( array( 'page' => $page_name ));
        if ($page_info->id ==null) {
            $data = array();
            $data['page'] = $page_name;
            $data['count'] = 0;
            $CI->Hitstablename->save($data);
        }
    }
}
// UPDATE PAGE HIT COUNT
if ( ! function_exists( 'updateCounter' )) {
    function updateCounter($page_name)
    {
        checkPageName($page_name);
        if($_SERVER["REMOTE_ADDR"] !="188.54.45.241") {
            $CI =& get_instance();
            // get all images
            $page_info = $CI->Hitstablename->get_one_by(array('page' => $page_name));
            if ($page_info->id) {
                $data = array();
                $data['count'] = $page_info->count + 1;
                $CI->Hitstablename->save($data, $page_info->id);
            }
        }
    }
}
// UPDATE VISITOR INFO
if ( ! function_exists( 'updateVisitorInfo' )) {
    function updateVisitorInfo()
    {
        if($_SERVER["REMOTE_ADDR"] !="188.54.45.241") {
            $CI =& get_instance();
            // get all images
            $data = array();
            $ip = $_SERVER['REMOTE_ADDR'];
            $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
           // echo $details->city;

            $data['ip_address'] = $_SERVER["REMOTE_ADDR"];
            $data['user_agent'] = $_SERVER["HTTP_USER_AGENT"];
            $data['city'] = $details->city;
            $CI->Hitsinfotble->save($data);
        }
    }
}

function sendtexttomobile($number, $msg){
	$mobile = "966567774888";							
	$password = "193782";							
	$sender = "Alama360";
	$numbers = $number;	//966571941910						
	$msg = $msg;		
	$MsgID = rand(1,99999);					
	$timeSend = 0;							
	$dateSend = 0;							
	$deleteKey = 152485;					
	$resultType = 0;					
	// Send SMS
	return sendSMS($mobile, $password, $numbers, $sender, $msg, $MsgID, $timeSend, $dateSend, $deleteKey, $resultType);
}


function distance($lat1, $lon1, $lat2, $lon2, $unit) {
	if (($lat1 == $lat2) && ($lon1 == $lon2)) {
	  return 0;
	}
	else {
	  $theta = $lon1 - $lon2;
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  $dist = acos($dist);
	  $dist = rad2deg($dist);
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);
  
	  if ($unit == "K") {
		return ($miles * 1.609344);
	  } else if ($unit == "N") {
		return ($miles * 0.8684);
	  } else {
		return $miles;
	  }
	}
  }
//https://stackoverflow.com/questions/29003118/get-driving-distance-between-two-points-using-google-maps-api
  function GetDrivingDistance($lat1, $lat2, $long1, $long2)
{
	//$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=21.510728,39.174585&destinations=21.569419860839844,39.18499755859375&mode=driving&language=pl-PL&key=AIzaSyAjp5OfeOhMfejWHUjy0TVtdwhTlc_NYxs";
	//$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=21.4644649,39.2783275&destinations=21.569419860839844,39.18499755859375&mode=driving&language=pl-PL&key=AIzaSyAjp5OfeOhMfejWHUjy0TVtdwhTlc_NYxs";
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$lat2."&destinations=".$long1.",".$long2."&mode=driving&language=pl-PL&key=AIzaSyAjp5OfeOhMfejWHUjy0TVtdwhTlc_NYxs";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
    $time = $response_a['rows'][0]['elements'][0]['duration']['value'];
    $dist=floor($dist / 1000);
	return $dist;
   //return array('distance' => $dist, 'time' => $time);
}

function getDatesFromRange($start, $end){
  $dates = array($start);
  while(end($dates) < $end){
      $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
  }
  return $dates;
}

function resizeImage($SrcImage,$DestImage, $thumb_width,$thumb_height,$Quality)
{
    list($width,$height,$type) = getimagesize($SrcImage);
    switch(strtolower(image_type_to_mime_type($type)))
    {
        case 'image/gif':
            $NewImage = imagecreatefromgif($SrcImage);
            break;
        case 'image/png':
            $NewImage = imagecreatefrompng($SrcImage);
            break;
        case 'image/jpeg':
            $NewImage = imagecreatefromjpeg($SrcImage);
            break;
        default:
            return false;
            break;
    }
    $original_aspect = $width / $height;
    $positionwidth = 0;
    $positionheight = 0;
    if($original_aspect > 1)    {
        $new_width = $thumb_width;
        $new_height = $new_width/$original_aspect;
        while($new_height > $thumb_height) {
            $new_height = $new_height - 0.001111;
            $new_width  = $new_height * $original_aspect;
            while($new_width > $thumb_width) {
                $new_width = $new_width - 0.001111;
                $new_height = $new_width/$original_aspect;
            }

        }
    } else {
        $new_height = $thumb_height;
        $new_width = $new_height/$original_aspect;
        while($new_width > $thumb_width) {
            $new_width = $new_width - 0.001111;
            $new_height = $new_width/$original_aspect;
            while($new_height > $thumb_height) {
                $new_height = $new_height - 0.001111;
                $new_width  = $new_height * $original_aspect;
            }
        }
    }
    if($width < $new_width && $height < $new_height){
        $new_width = $width;
        $new_height = $height;
        $positionwidth = ($thumb_width - $new_width) / 2;
        $positionheight = ($thumb_height - $new_height) / 2;
    }elseif($width < $new_width && $height > $new_height){
        $new_width = $width;
        $positionwidth = ($thumb_width - $new_width) / 2;
        $positionheight = 0;
    }elseif($width > $new_width && $height < $new_height){
        $new_height = $height;
        $positionwidth = 0;
        $positionheight = ($thumb_height - $new_height) / 2;
    } elseif($width > $new_width && $height > $new_height){
        if($new_width < $thumb_width) {
            $positionwidth = ($thumb_width - $new_width) / 2;
        } elseif($new_height < $thumb_height) {
            $positionheight = ($thumb_height - $new_height) / 2;
        }
    }
    $thumb = imagecreatetruecolor( $thumb_width, $thumb_height );
    /********************* FOR WHITE BACKGROUND  *************************/
    $white = imagecolorallocate($thumb, 255,255,255);
    imagefill($thumb, 0, 0, $white);
    if(imagecopyresampled($thumb, $NewImage,$positionwidth, $positionheight,0, 0, $new_width, $new_height, $width, $height)) {
        if(imagejpeg($thumb,$DestImage,$Quality)) {
            imagedestroy($thumb);
            return true;
        }
    }
}
function resize($source,$destination,$newWidth,$newHeight)
{
    ini_set('max_execution_time', 0);
    $ImagesDirectory = $source;
    $DestImagesDirectory = $destination;
    $NewImageWidth = $newWidth;
    $NewImageHeight = $newHeight;
    $Quality = 100;
    $imagePath = $ImagesDirectory;
    $destPath = $DestImagesDirectory;
    $checkValidImage = getimagesize($imagePath);
    if(file_exists($imagePath) && $checkValidImage)
    {
        resizeImage($imagePath,$destPath,$NewImageWidth,$NewImageHeight,$Quality);
           // echo " --> ".$source.'  --> Resize Successful!<BR><BR>';
        //else
            //echo " --> ".$source.'  --> Resize Failed!<BR><BR>';
    }
}


function addvatandcomission($price="", $product_id, $commission_plan)
{

    $CI =& get_instance();

    // get all images
    $comission_info = $CI->Acc_comission_seller->get_one_by( array( 'id' => $commission_plan ));

    $num = $price;
    if($comission_info->amount_percentage && $comission_info->amount_percentage>0){
        $percentage = $comission_info->amount_percentage;
        $num += $num * ($percentage / 100);
        $num = round($num, 1);
        $num = sprintf('%0.2f', $num);
    }


    $num = $num;
    $percentage = 15;
    $num += $num * ($percentage / 100);
    return sprintf('%0.2f', $num);
}
function addonlycomission($price="", $product_id, $commission_plan)
{

    $CI =& get_instance();

    // get all images
    $comission_info = $CI->Acc_comission_seller->get_one_by( array( 'id' => $commission_plan ));

    $num = $price;
    if($comission_info->amount_percentage && $comission_info->amount_percentage>0){
        $percentage = $comission_info->amount_percentage;
        $num += $num * ($percentage / 100);
        $num = round($num, 1);
        $num = sprintf('%0.2f', $num);
    }

    return sprintf('%0.2f', $num);
}

function addonlyvat($price="")
{
    $num = $price;
    $percentage = 15;
    $num += $num * ($percentage / 100);
    return sprintf('%0.2f', $num);
}