<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

// built-in routes
$route['default_controller'] = 'Index';
$route['404_override'] = 'ps404';
$route['translate_uri_dashes'] = FALSE;

// frontend content
//$route['areas'] = 'Index/areas';
//$route['contactus'] = 'Index/contactus';
//$route['app'] = 'Index/app';
//$route['privacy'] = 'Index/privacy';
//$route['terms'] = 'Index/terms';
//$route['policies'] = 'Index/policies';
//$route['aboutus'] = 'Index/aboutus';
//$route['partnership'] = 'Index/partnership';
$route['settings'] = 'Index/settings';
$route['userAllDeliveryAddress'] = 'Index/userAllDeliveryAddress';

$route['LanguageSwitcher/switchLang/(:any)'] = 'LanguageSwitcher/switchLang';
$route['userbymobile'] = 'Index/userbymobile';
$route['createOrupdateuser'] = 'Index/createOrupdateuser';
$route['deliveryboyorder'] = 'Index/deliveryboyorder';
$route['unassignedorders'] = 'Index/unassignedorder';
$route['orders'] = 'Index/orders';
$route['orderdetails'] = 'Index/orderdetails';
$route['printOrderDetailsPdf'] = 'Index/printOrderDetailsPdf';
$route['deliveryboysingleorder'] = 'Index/deliveryboysingleorder';
$route['markdelivered'] = 'Index/markdelivered';
$route['deliveryboyterms'] = 'Index/deliveryboyterms';
$route['paytabstest'] = 'Index/paytabstest';
$route['paytabs'] = 'Index/paytabs';
$route['paymentsuccessurl/(.*)'] = 'Index/paymentsuccessurl';
$route['paymentunsuccessurl'] = 'Index/paymentunsuccessurl';
$route['paymentverifysuccessurl'] = 'Index/paymentverifysuccessurl';
$route['shareordertoconfirm'] = 'Index/shareordertoconfirm';
$route['deleteordertoconfirm'] = 'Index/deleteordertoconfirm';
$route['confirmorderbycustomer'] = 'Index/confirmorderbycustomer';
// redirect for short url
$route['login'] = "main/login";
$route['logout'] = "main/logout";
$route['reset_request'] = "main/reset_request";
$route['reset_email/(.*)'] = "main/reset_email/$1";

// if both backend and frontend exist,
//$route['admin'] = "backend/dashboard";
$route['admin'] = "backend/shops";
$route['admin/(.*)'] = "backend/$1";
$route['rest/(.*)'] = "rest/$1";

$route['api/(.*)'] = "api/$1";

$route['(.*)'] = "frontend/$1";



