<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Shopproducts extends FE_Controller

{
    function __construct()

    {

        parent::__construct(NO_AUTH_CONTROL, 'shop');
    }

//    function index($shop_id="") {
//
////        $this->data['feeds'] = $this->Feed->get_all_by( array());
//        $this->data['discountproducts'] = $this->Product->get_all_by( array("is_discount"=>1, "is_available"=>1, "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a"));
//        $this->data['featuredproducts'] = $this->Product->get_all_by( array("is_featured"=>1, "is_available"=>1, "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a"));
//        $this->data['newproduct'] = $this->Product->get_all_by( array("order_by"=>1, "order_by_field"=>"added_date", "order_by_type"=>"asc", "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a") , 10, 0 );
//        $this->data['popularproduct'] = $this->Product->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a") , 10, 0 );
//        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a"));
////        $this->data['popularshop'] = $this->Shop->get_all_by(array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc"));
//        $this->load_front_template( 'shop',$this->data, true );
//
//    }
    function shopdashboard($shop_id="") {
        $this->session->set_userdata('selected_front_shop_id', $shop_id);
        $selected_shop_id = $this->session->userdata('selected_front_shop_id');
        $this->data['bodyclass'] = "inside_shop shopdashboard";
        $this->data['page_name'] = "shop_home_page";
        $this->data['redirect_url'] = site_url('/shopproducts/shopdashboard/'.$shop_id);
        $this->data['shop_id'] = $shop_id;
        $this->data['is_shop_dashboard'] = true;
        $this->data['discountproducts'] = $this->Product->get_all_by( array("is_discount"=>1, "is_available"=>1, "shop_id"=>$shop_id));
        $this->data['featuredproducts'] = $this->Product->get_all_by( array("is_featured"=>1, "is_available"=>1, "shop_id"=>$shop_id));
        $this->data['newproduct'] = $this->Product->get_all_by( array("order_by"=>1, "order_by_field"=>"added_date", "order_by_type"=>"asc", "shop_id"=>$shop_id) , 20, 0 );
        $this->data['popularproduct'] = $this->Product->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>$shop_id) , 20, 0 );
        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>$shop_id));
//        $this->data['popularshop'] = $this->Shop->get_all_by(array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc"));
        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;
        $transaction=array();
        if($nowtime){
            $transaction=$this->Cartheader->get_one_by(array('time' => $nowtime, 'shop_id' => $shop_id));
        }
        $this->data['transaction'] = $transaction;
        $this->load_front_template( 'shop',$this->data, true );

    }
    function category($cat_id="") {
        $cat = $this->Category->get_one_by( array("id"=>$cat_id));
        $this->session->set_userdata('selected_front_shop_id', $cat->shop_id);
        $selected_shop_id = $this->session->userdata('selected_front_shop_id');
        $shop_id = $selected_shop_id['shop_id'];
        $this->data['bodyclass'] = "inside_shop product_cat";
        $this->data['page_name'] = "product_cat";
        $this->data['redirect_url'] = site_url('/shopproducts/category/'.$cat_id);
        $this->data['cat'] = $cat;
        $this->data['cat_id'] = $cat_id;
        $this->data['sub_cat_id'] = "";
        $this->data['shop_id'] = $cat->shop_id;

        if($cat->is_discount_category==1){
            $this->data['products'] = $this->Product->get_all_by( array("is_discount"=>1, "order_by"=>1, "order_by_field"=>"added_date", "order_by_type"=>"asc", "shop_id"=>$cat->shop_id) , 200, 0 );

        }else{
            $this->data['products'] = $this->Product->get_all_by( array("cat_id"=>$cat_id, "order_by"=>1, "order_by_field"=>"added_date", "order_by_type"=>"asc", "shop_id"=>$cat->shop_id) , 100, 0 );
        }

        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>$cat->shop_id));

        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;
        $transaction=array();
        if($nowtime){
            $transaction=$this->Cartheader->get_one_by(array('time' => $nowtime, 'shop_id' => $cat->shop_id));
        }
        $this->data['transaction'] = $transaction;
        $this->load_front_template( 'categoryproducts',$this->data, true );

    }

    function subcategory($subcat_id="") {
        $cat = $this->Subcategory->get_one_by( array("id"=>$subcat_id));
        $this->session->set_userdata('selected_front_shop_id', $cat->shop_id);
        $selected_shop_id = $this->session->userdata('selected_front_shop_id');
        $shop_id = $selected_shop_id['shop_id'];
        $this->data['bodyclass'] = "inside_shop product_subcat";
        $this->data['page_name'] = "product_subcat";
        $this->data['redirect_url'] = site_url('/shopproducts/subcategory/'.$subcat_id);
        $this->data['cat'] = $cat;
        $this->data['cat_id'] = $cat->cat_id;
        $this->data['sub_cat_id'] = $subcat_id;
        $this->data['shop_id'] = $cat->shop_id;
        $this->data['products'] = $this->Product->get_all_by( array("sub_cat_id"=>$subcat_id, "order_by"=>1, "order_by_field"=>"added_date", "order_by_type"=>"asc", "shop_id"=>$cat->shop_id) , 100, 0 );
        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>$cat->shop_id));
        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;
        $transaction=array();
        if($nowtime){
            $transaction=$this->Cartheader->get_one_by(array('time' => $nowtime, 'shop_id' => $cat->shop_id));
        }
        $this->data['transaction'] = $transaction;
        $this->load_front_template( 'categoryproducts',$this->data, true );

    }

    function productdetails($product_id="") {
        $product = $this->Product->get_one_by( array("id"=>$product_id));
        $this->session->set_userdata('selected_front_shop_id', $product->shop_id);
        $selected_shop_id = $this->session->userdata('selected_front_shop_id');
        //$shop_id = $selected_shop_id['shop_id'];
        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;
        $transaction=array();
        if($nowtime){
            $transaction=$this->Cartheader->get_one_by(array('time' => $nowtime, 'shop_id' => $product->shop_id));
        }
        $this->data['transaction'] = $transaction;
        $this->data['bodyclass'] = "inside_shop product_details";
        $this->data['page_name'] = "product_details";
        $this->data['redirect_url'] = site_url('/shopproducts/productdetails/'.$product_id);
        $this->data['shop_id'] = $product->shop_id;
        $this->data['product'] = $product;
        $this->data['cat_id'] = $product->cat_id;
        $this->data['sub_cat_id'] = $product->sub_cat_id;
        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>$product->shop_id));
//        $this->data['popularshop'] = $this->Shop->get_all_by(array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc"));

        //$this->data['reletedproducts'] = $this->Product->get_all_related_product_trending( array("id"=>$product->id, "cat_id"=>$product->cat_id, "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a"), 10, 0 )->result();
        $this->load_front_template( 'productdetails',$this->data, true );

    }



    function addtocart() {
        $postdata=$this->input->post("postdata");
        $product_id=$postdata["product_id"];
        $product = $this->Product->get_one_by( array("id"=>$product_id));
        $this->session->set_userdata('selected_front_shop_id', $product->shop_id);
        $selected_shop_id = $this->session->userdata('selected_front_shop_id');
        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }

            if($product->shop_id) {
                $shop_id = $product->shop_id;
                $ordercheck = $this->Cartheader->get_one_by(array('time' => $nowtime, 'shop_id' => $product->shop_id));
                $shop = $this->Shop->get_one_by(array('id' => $product->shop_id));
                $current_date_time = date("Y-m-d H:i:s");
                if ($ordercheck->id) {
                    $inserted = $ordercheck->id;
                } else {
                    $tax_val = 0.00;
                    if ($shop->overall_tax_value) {
                        $tax_val = addonlycomission($product->unit_price, $product->id, $product->commission_plan) * $shop->overall_tax_value;
                    }
                    $cart_header = array(
                        'shop_id' => $product->shop_id,
                        'time' => $nowtime,
                        'sub_total_amount' => addonlycomission($product->unit_price, $product->id, $product->commission_plan),
                        'tax_amount' => $tax_val,
                        'shipping_amount' => 0,
                        'balance_amount' => (addonlycomission($product->unit_price, $product->id, $product->commission_plan) + ($tax_val + 15)),
                        'total_item_amount' => addonlycomission($product->unit_price, $product->id, $product->commission_plan),
                        'total_item_count' => 1,
                        'discount_amount' => 0,
                        'coupon_discount_amount' => 0,
                        'added_date' => $current_date_time,
                        'shipping_tax_percent' => 0,
                        'tax_percent' => $shop->overall_tax_label,
                        'shipping_method_amount' => "15",
                    );
                    $inserted = $this->Cartheader->accSave($cart_header);
                }

                $product_info = $product;
                if ($product_info->is_available == 1 && $product_info->status == 1) {
                    $orderCriteria = array();
                    $orderCriteria['shop_id'] = $product->shop_id;
                    $orderCriteria['cart_header_id'] = $inserted;
                    $orderCriteria['product_id'] = $product_info->id;

                    $ordercheck = $this->Cartdetail->get_one_by($orderCriteria);
                    if ($ordercheck->id) {
                        $trans_detail['qty'] = $ordercheck->qty + 1;
                        $this->Cartdetail->save($trans_detail, $ordercheck->id);
                    } else {
                        $categoryid = $product_info->cat_id;
                        $trans_detail['shop_id'] = $product_info->shop_id;
                        $trans_detail['product_id'] = $product_info->id;
                        $trans_detail['product_category_id'] = $categoryid;
                        $trans_detail['product_name'] = $product_info->name;
                        $trans_detail['product_photo'] = $product_info->thumbnail;
                        $trans_detail['price'] = addonlycomission($product_info->unit_price, $product_info->id, $product_info->commission_plan);
                        $trans_detail['original_price'] = addonlycomission($product_info->original_price, $product_info->id, $product_info->commission_plan);
                        $trans_detail['qty'] = 1;
                        $trans_detail['cart_header_id'] = $inserted;
                        $trans_detail['added_date'] = $current_date_time;
                        $trans_detail['updated_date'] = $current_date_time;
                        $trans_detail['updated_user_id'] = "0";
                        $trans_detail['updated_flag'] = "0";
                        $trans_detail['currency_short_form'] = $shop->currency_short_form;
                        $trans_detail['currency_symbol'] = $shop->currency_symbol;
                        $trans_detail['product_unit'] = $product_info->product_unit;
                        $trans_detail['product_measurement'] = $product_info->product_measurement;
                        $trans_detail['shipping_cost'] = $product_info->shipping_cost;
                        $this->Cartdetail->save($trans_detail);
                    }


                }


                $transaction = $this->Cartheader->get_one_by(array("id" => $inserted));

                $totalItem = 0;
                $totalItemAmountNoTax = 0.0;
                $sub_total_amount = 0.0;
                $discount_total_amount = 0.0;
                $originalprice_total_amount = 0.0;
                $tax_amount = 0.0;
                $orderdetails = $this->Cartdetail->get_all_by(array("cart_header_id" => $inserted));
                foreach ($orderdetails->result() as $sinleDetails) {
                    if ($sinleDetails->id) {
                        $qtyDtls = $sinleDetails->qty;
                        $originalPriceDtls = $sinleDetails->original_price;
                        $discountPercentDtls = $sinleDetails->discount_percent;
                        $price = $originalPriceDtls;
                        $discount_amount = 0.0;
                        if ($discountPercentDtls > 0) {
                            $discount_amount = (($sinleDetails->discount_percent / 100) * $originalPriceDtls);
                            $discountPriceDtls = ($originalPriceDtls - (($sinleDetails->discount_percent / 100) * $originalPriceDtls));
                            $price = $discountPriceDtls;
                        }

                        $stockDtls = $sinleDetails->stock_status;
                        $itemSubtotal = $originalPriceDtls * $qtyDtls;
                        if ($stockDtls == '1') {
                            $totalItem += $qtyDtls;
                            $totalItemAmountNoTax += $itemSubtotal;

                            $itemDiscountAmount = $discount_amount * $qtyDtls;
                            $discount_total_amount += $itemDiscountAmount;
                            $sub_total_amount += $price * $qtyDtls;
                            $itemOriginalAmount = $sinleDetails->original_price * $qtyDtls;
                            $originalprice_total_amount += $itemOriginalAmount;
                        }
                    }
                }


                $total_item_amount = $sub_total_amount;
                if ($transaction->coupon_discount_amount > 0) {
                    $total_item_amount = $total_item_amount - $transaction->coupon_discount_amount;
                }

                $tax_amount = (($transaction->tax_percent / 100) * $total_item_amount);
                $master_data = array(
                    'total_item_count' => $totalItem,
                    'sub_total_amount' => $sub_total_amount, // after discount
                    'discount_amount' => $discount_total_amount,
                    'coupon_discount_amount' => $transaction->coupon_discount_amount,
                    'tax_amount' => $tax_amount,
                    'shipping_method_amount' => $transaction->shipping_method_amount,
                    'total_item_amount' => $originalprice_total_amount, // without discount
                    'balance_amount' => $total_item_amount + $transaction->shipping_method_amount + $tax_amount - $transaction->coupon_discount_amount,
                );

                // save data
                $this->Cartheader->save($master_data, $transaction->id);
        }
        $transaction = $this->Cartheader->get_one_by(array("id" => $inserted));
        $transaction->details=$this->Cartdetail->get_all_by(array("cart_header_id" => $inserted))->result();
        $arr = array('isError' => false, 'message' =>  get_msg('added_successfully'), 'data' =>  $transaction);
        echo json_encode( $arr );
        exit;
    }

    function plusminusqtycart()
    {
        $order_id=$this->input->post( 'order_id');
        $order_details_id=$this->input->post( 'order_details_id');
        $isincreaseQty=$this->input->post( 'increaseQtyData');
        if($order_id && $order_details_id){
            $transaction = $this->Cartheader->get_one( $order_id );
            if($transaction->id){
                $orderproduct=$this->Cartdetail->get_one_by(array("cart_header_id"=>$order_id, "id"=>$order_details_id));
                if($orderproduct->id){
                    if($isincreaseQty){
                        $qty=$this->input->post( 'qty');
                        if($qty>0) {
                            $dataDtls = array(
                                'qty' => $qty,
                            );
                            $this->Cartdetail->save($dataDtls, $order_details_id);
                        }
                    }else{
                        $rows_count = $this->Cartdetail->count_all_by(array("cart_header_id"=>$order_id, "stock_status"=>1));
                        if($rows_count>1){

                            $this->Cartdetail->delete_all_records(array("cart_header_id"=>$order_id, "id"=>$order_details_id));
                        }else{
                            $transaction = $this->Cartheader->get_one( $order_id );
                            $arr = array('isError' => true, 'message' =>  "can not delete", 'data' =>   $transaction, 'posss' => $this->input->post());
                            echo json_encode( $arr );
                            exit;
                        }

                    }


                    $totalItem = 0;
                    $totalItemAmountNoTax = 0.0;
                    $sub_total_amount = 0.0;
                    $discount_total_amount = 0.0;
                    $originalprice_total_amount = 0.0;
                    $tax_amount = 0.0;
                    $orderdetails=$this->Cartdetail->get_all_by(array("cart_header_id"=>$order_id));
                    foreach($orderdetails->result() as $sinleDetails) {
                        if($sinleDetails->id){
                            $qtyDtls= $sinleDetails->qty;
                            $originalPriceDtls = $sinleDetails->original_price;
                            $discountPercentDtls = $sinleDetails->discount_percent;
                            $price=$originalPriceDtls;
                            $discount_amount=0.0;
                            if($discountPercentDtls>0){
                                $discount_amount=(($sinleDetails->discount_percent / 100) * $originalPriceDtls);
                                $discountPriceDtls = ($originalPriceDtls - (($sinleDetails->discount_percent / 100) * $originalPriceDtls));
                                $price=$discountPriceDtls;
                            }

                            $stockDtls = $sinleDetails->stock_status;
                            $itemSubtotal=$originalPriceDtls * $qtyDtls;
                            if($stockDtls=='1'){
                                $totalItem += $qtyDtls;
                                $totalItemAmountNoTax += $itemSubtotal;

                                $itemDiscountAmount=$discount_amount * $qtyDtls;
                                $discount_total_amount += $itemDiscountAmount;
                                $sub_total_amount +=$price * $qtyDtls;
                                $itemOriginalAmount=$sinleDetails->original_price * $qtyDtls;
                                $originalprice_total_amount += $itemOriginalAmount;
                            }
                        }
                    }


                    $total_item_amount=$sub_total_amount;
                    if($transaction->coupon_discount_amount >0){
                        $total_item_amount=$total_item_amount - $transaction->coupon_discount_amount;
                    }

                    $tax_amount=(($transaction->tax_percent / 100) * $total_item_amount);
                    $master_data = array(
                        'total_item_count'=> $totalItem,
                        'sub_total_amount'=> $sub_total_amount, // after discount
                        'discount_amount'=> $discount_total_amount,
                        'coupon_discount_amount'=> $transaction->coupon_discount_amount,
                        'tax_amount'=> $tax_amount,
                        'shipping_method_amount'=> $transaction->shipping_method_amount,
                        'total_item_amount'=> $originalprice_total_amount, // without discount
                        'balance_amount'=> $total_item_amount + $transaction->shipping_method_amount + $tax_amount-$transaction->coupon_discount_amount,
                    );

                    // save data
                    $this->Cartheader->save( $master_data, $transaction->id);


                }

            }

        }

            $transaction = $this->Cartheader->get_one( $order_id );
            $arr = array('isError' => false, 'message' =>  "updated", 'data' =>   $transaction, 'posss' => $this->input->post());
            echo json_encode( $arr );
            exit;


    }



}