<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Checkout extends FE_Controller

{


    function __construct()

    {

        parent::__construct(NO_AUTH_CONTROL, 'shop');
    }



    function confirmation($order_id="") {
        $current_user_id=$this->session->userdata('current_user_id');
        $current_user_mobile=$this->session->userdata('current_user_mobile');
        //if($current_user_id && $current_user_mobile){
        $selected_shop_id = $this->session->userdata('selected_front_shop_id');
        $nowtime = $this->session->userdata('current_time');
        $this->data['carttime'] = $nowtime;
        $transaction=array();
        if($nowtime){
            $transaction=$this->Cartheader->get_one_by(array('time' => $nowtime, 'shop_id' => $selected_shop_id));
        }
        $this->data['transaction'] = $transaction;
        $this->session->set_userdata('current_order_id', $transaction->id);

        $this->data['bodyclass'] = "inside_shop cartpages checkout checkout_confirmation";
        $this->data['page_name'] = "checkout_confirmation";
        $this->data['redirect_url'] = site_url('/checkout/confirmation/'.$transaction->id);
       // $this->data['transaction'] = $this->Cartheader->get_one_by(array('id' => $order_id));

            $this->load_front_template( 'checkout_confirmation',$this->data, true );
//        }else{
//            redirect(site_url('/'));
//        }

    }

    function checkout_payment($order_id="") {
        $current_user_id=$this->session->userdata('current_user_id');
        $current_user_mobile=$this->session->userdata('current_user_mobile');
        //if($current_user_id && $current_user_mobile){
        // $this->data['user'] = $this->User->get_one_by(array("user_id"=>$user_id));
        // $this->data['useraddress'] = $this->UserAddress->get_all_by(array("user_id"=>$user_id));
        $selected_shop_id = $this->session->userdata('selected_front_shop_id');
//        $this->data['shop_id'] = $shop_id;
        $nowtime = $this->session->userdata('current_time');
        $this->data['current_user_id'] = $current_user_id;
        $this->data['order_id'] = $order_id;
        $this->data['timeslots'] = $this->timeslotcustom();
        $this->data['carttime'] = $nowtime;
        $this->data['bodyclass'] = "inside_shop cartpages checkout checkout_payment";
        $this->data['page_name'] = "checkout_payment";
        $this->data['redirect_url'] = site_url('/checkout/checkout_payment/'.$order_id);
        $this->load_front_template( 'checkout_payment',$this->data, true );
//        }else{
//            redirect(site_url('/'));
//        }

    }


    function checkout_submit(){
        $postdata=$this->input->post();
        $order_id=$postdata["transactions_header_id"];
        $user_id=$postdata["user_id"];
        $shipping_address_id=$postdata["shipping_address_id"];
        $time_slot_id=$postdata["time_slot_id"];
        $payment_method=$postdata["payment_method"];
        $catHeader=$this->Cartheader->get_one_by(array('id' => $order_id));
//        echo "<pre>";
//        print_r($postdata);
//        echo "</pre>";
        if($order_id && $payment_method && $shipping_address_id && $user_id) {
            $catdetails = $this->Cartdetail->get_all_by(array("cart_header_id" => $catHeader->id))->result();
            $paypal_result = 0;
            $stripe_result = 0;
            $cod_result = 0;
            if ($payment_method == "paytabs") {
                //$property_id=$request['rentproperty_id'];
                $postal_code = "23213";
                $country = "SAU";
                $postal_code_shipping = "23213";
                $country_shipping = "SAU";
                $products_per_title = "Property Title";
                $currency = "SAR";
                $site_url = "https://www.eshtri.net";
                $return_url = "https://www.eshtri.net/paymentsuccessurl";
                $cms_with_version = "API USING PHP";
                $shipping_address_2 = "Riyadh";
                //User Using COD
                $payment_method = "paytabs";
                $paytabs_result = 1;
                $cc_phone_number="966";
            } else if ($payment_method == "POSTerminal") {
                //User Using COD
                $payment_method = "POSTerminal";
                $POSTerminal_result = 1;
            } else if ($payment_method == "COD") {
                //User Using COD
                $payment_method = "COD";
                $cod_result = 1;
            } else {
                //Not selected to payment
                $this->set_flash_msg( 'error', get_msg( 'payment_not_select' ));
                redirect(site_url(' checkout/checkout_payment/'.$catHeader->id.''));
            }

            $UserAddress = $this->UserAddress->get_one_by(array('user_id' => $user_id, 'id' => $shipping_address_id));
            if ($UserAddress->id == null || !$UserAddress->id) {
                $this->set_flash_msg( 'error', get_msg( 'delivery_address_not_added' ));
                redirect(site_url(' checkout/checkout_payment/'.$catHeader->id.''));
            }

            if ($cod_result == 1 || $paytabs_result = 1 || $POSTerminal_result = 1) {
                $this->db->trans_start();

                //First Time
                $transaction_row_count = $this->Transactionheader->count_all();
                $current_date_month = date("Ym");
                $current_date_time = date("Y-m-d H:i:s");
                // print_r($current_date_month);die;
                $conds['code'] = $current_date_month;
                // print_r($conds);die;
                $trans_code_checking = $this->Code->get_one_by($conds)->code;
                // print_r($trans_code_checking);die;
                $id = false;


                if ($trans_code_checking == "") {
                    //New record for this year--mm, need to insert as inside the core_code_generator table
                    $data['type'] = "transaction";
                    $data['code'] = $today = date("Ym");;
                    $data['count'] = $transaction_row_count + 1;
                    $data['added_user_id'] = $user_id;
                    $data['added_date'] = date("Y-m-d H:i:s");
                    $data['updated_date'] = date("Y-m-d H:i:s");
                    $data['updated_user_id'] = 0;
                    $data['updated_flag'] = 0;

                    if (!$this->Code->save($data, $id)) {
                        // rollback the transaction
                        $this->db->trans_rollback();
                        $this->set_flash_msg( 'error', get_msg( 'err_model' ));
                        redirect(site_url(' checkout/checkout_payment/'.$catHeader->id.''));
                    }

                    // get inserted id
                    if (!$id) $id = $data['id'];

                    if ($id) {
                        $trans_code = $this->Code->get_one($id)->code;
                    }


                } else {
                    //record is already exist so just need to update for count field only
                    $data['count'] = $transaction_row_count + 1;

                    $core_code_generator_id = $this->Code->get_one_by($conds)->id;

                    if (!$this->Code->save($data, $core_code_generator_id)) {
                        // rollback the transaction
                        $this->db->trans_rollback();
                        $this->set_flash_msg( 'error', get_msg( 'err_model' ));
                        return;
                    }

                    $conds['id'] = $core_code_generator_id;
                    $trans_code = $this->Code->get_one_by($conds)->code . ($transaction_row_count + 1);

                }

                if ($postdata["shipping_method_amount"] < 50) {
                    $shipping_method_amount = 15.00;
                } else if ($postdata["shipping_method_amount"] > 50) {
                    $shipping_method_amount = 50.00;
                } else {
                    $shipping_method_amount = $postdata["shipping_method_amount"];
                }
                $shopinfo = $this->Shop->get_one_by(array('id' => $catHeader->shop_id));
                if($postdata["memo"]==""){
                    $postdata["memo"]="-";
                }
                $delivery_date = "";
               $delivery_time_from = "";
                $delivery_time_to = "";
//                $time_slot_id = "";
                if ($postdata["shipping_method_amount"] < 50) {
                    // $UserAddress = $this->UserAddress->get_one_by( array( 'user_id' => $this->post( 'user_id'), 'is_default' => 1));
                    $timeslotsdetails = $this->Timeslotsdetails->get_one_by(array('id' => $time_slot_id));
                    $delivery_date = $timeslotsdetails->slot_date;
                    $delivery_time_from = $timeslotsdetails->start_time;
                    $delivery_time_to = $timeslotsdetails->end_time;
                    $time_slot_id = $time_slot_id;

                }
                $shipping_latitute = '';
                $shipping_lontitude = '';
                if ($UserAddress->id) {
                    $shipping_first_name = $UserAddress->receiverName;
                    $shipping_address_1 = $UserAddress->address;
                    $shipping_address_2 = $UserAddress->landmark;
                    $shipping_city = $UserAddress->city;
                    $shipping_email = $UserAddress->email;
                    $shipping_phone = $UserAddress->mobileNo;
                    $shipping_latitute = $UserAddress->latitute;
                    $shipping_lontitude = $UserAddress->lontitude;
                } else {
                    $this->db->trans_rollback();
                    $this->set_flash_msg( 'error', get_msg( 'shipping_address_not_found' ));
                    redirect(site_url(' checkout/checkout_payment/'.$catHeader->id.''));
                }
                //Need to save inside transaction header table
                $trans_header = array(
                    'user_id' => $user_id,
                    'shop_id' => $catHeader->shop_id,
                    'sub_total_amount' => $catHeader->sub_total_amount,
                    'tax_amount' => $catHeader->tax_amount,
                    'shipping_amount' => $shipping_method_amount,
                    // 'balance_amount' 		=> $this->post( 'balance_amount' ),
                    'balance_amount' => ($catHeader->sub_total_amount + ($catHeader->tax_amount + $shipping_method_amount + ($shipping_method_amount * $catHeader->shipping_tax_percent))),
                    'total_item_amount' => $catHeader->total_item_amount,
                    'total_item_count' => $catHeader->total_item_count,
                    'contact_name' => $UserAddress->receiverName,
                    'contact_phone' => $UserAddress->mobileNo,
                    'payment_method' => $payment_method,
                    'trans_status_id' => 1,
                    'discount_amount' => $catHeader->discount_amount,
                    'coupon_discount_amount' => $catHeader->coupon_discount_amount,
                    'trans_code' => $trans_code,
                    'added_date' => $current_date_time,
                    'added_user_id' => $user_id,
                    'updated_date' => $current_date_time,
                    'updated_user_id' => "0",
                    'updated_flag' => "0",
                    'currency_symbol' => $shopinfo->currency_symbol,
                    'currency_short_form' => $shopinfo->currency_short_form,
                    'shipping_first_name' => $shipping_first_name,
                    // 'shipping_last_name'	=> $this->post( 'shipping_last_name'),
                    // 'shipping_company'		=> $this->post( 'shipping_company'),
                    'shipping_address_1' => $shipping_address_1,
                    'shipping_address_2' => $shipping_address_2,
                    // 'shipping_country' 		=> $this->post( 'shipping_country'),
                    // 'shipping_state'		=> $this->post( 'shipping_state'),
                    'shipping_city' => $shipping_city,
                    // 'shipping_postal_code'	=> $this->post( 'shipping_postal_code'),
                    'shipping_email' => $shipping_email,
                    'shipping_phone' => $shipping_phone,
                    'shipping_lat' => $shipping_latitute,
                    'shipping_lng' => $shipping_lontitude,
                    'shop_lat' => $shopinfo->lat,
                    'shop_lng' => $shopinfo->lng,
                    'shipping_tax_percent' => $catHeader->shipping_tax_percent,
                    'tax_percent' => $catHeader->tax_percent,
                    'shipping_method_amount' => $shipping_method_amount,
                    'shipping_method_name' => $catHeader->shipping_method_name,
                    'memo' => $postdata["memo"],
                    'is_zone_shipping' => 1,
                    'delivery_date' => $delivery_date,
                    'delivery_time_from' => $delivery_time_from,
                    'delivery_time_to' => $delivery_time_to,
                    'time_slot_id' => $time_slot_id,

                );

                $trans_header_id = false;
                $inserted = $this->Transactionheader->saveOrder($trans_header);
                if ($inserted == null) {
                    // rollback the transaction
                    $this->set_flash_msg( 'error', get_msg( 'err_model' ));
                    redirect(site_url(' checkout/checkout_payment/'.$catHeader->id.''));
                }

                $trans_header_id = $inserted;

                $trans_details = $catdetails;
                $totalItem = 0;
                $totalItemAmountNoTax = 0.0;
                $sub_total_amount = 0.0;
                $discount_total_amount = 0.0;
                $originalprice_total_amount = 0.0;
                $tax_amount = 0.0;
                    foreach ($trans_details as $tra_detail){
                        $product_info = $this->Product->get_one($tra_detail->product_id);
                    if ($product_info->is_available == 1 && $product_info->status == 1 && $shopinfo->id == $product_info->shop_id) {
                        $categoryid = $product_info->cat_id;
                        $trans_detail['shop_id'] = $tra_detail->shop_id;
                        $trans_detail['product_id'] = $tra_detail->product_id;
                        $trans_detail['product_category_id'] = $categoryid;
                        $trans_detail['product_name'] = $tra_detail->product_name;
                        $trans_detail['product_photo'] = $tra_detail->thumbnail;
                        $trans_detail['product_attribute_id'] = $tra_detail->product_attribute_id;
                        $trans_detail['product_attribute_price'] = $tra_detail->product_attribute_price;
                        $trans_detail['price'] = $tra_detail->price;
                        $trans_detail['product_attribute_name'] = $tra_detail->product_attribute_name;
                        $trans_detail['original_price'] = $tra_detail->original_price;
                        $trans_detail['product_color_id'] = $tra_detail->product_color_id;
                        $trans_detail['product_color_code'] = $tra_detail->product_color_code;
                        $trans_detail['product_color_name'] = "";
                        if ($tra_detail->product_color_id) {
                            $color_info = $this->Color->get_one($tra_detail->product_color_id);
                            if ($color_info->color_value && $color_info->color_name) {
                                $trans_detail['product_color_name'] = $color_info->color_name;
                            }
                        }

                        $trans_detail['qty'] = $tra_detail->qty;
                        $trans_detail['discount_value'] = $tra_detail->discount_value;
                        $trans_detail['discount_percent'] = $tra_detail->discount_percent;
                        $trans_detail['discount_amount'] = $tra_detail->discount_amount;
                        $trans_detail['transactions_header_id'] = $trans_header_id;
                        $trans_detail['added_date'] = $current_date_time;
                        $trans_detail['added_user_id'] = $user_id;
                        $trans_detail['updated_date'] = $current_date_time;
                        $trans_detail['updated_user_id'] = "0";
                        $trans_detail['updated_flag'] = "0";
                        $trans_detail['currency_short_form'] = $shopinfo->currency_short_form;
                        $trans_detail['currency_symbol'] = $shopinfo->currency_symbol;
                        $trans_detail['product_unit'] = $tra_detail->product_unit;
                        $trans_detail['product_measurement'] = $tra_detail->product_measurement;
                        $trans_detail['shipping_cost'] = $tra_detail->shipping_cost;

                        if (!$this->Transactiondetail->save($trans_detail)) {
                            // if error in saving transaction detail,
                            $this->db->trans_rollback();
                            $this->set_flash_msg( 'error', get_msg( 'err_model' ));
                            redirect(site_url(' checkout/checkout_payment/'.$catHeader->id.''));
                        }

                        //Need to update transaction count table
                        $prd_cat_id = $this->Product->get_one($tra_detail->product_id)->cat_id;
                        $prd_sub_cat_id = $this->Product->get_one($tra_detail->product_id)->sub_cat_id;

                        $trans_count['product_id'] = $tra_detail->product_id;
                        $trans_count['shop_id'] = $tra_detail->shop_id;
                        $trans_count['cat_id'] = $prd_cat_id;
                        $trans_count['sub_cat_id'] = $prd_sub_cat_id;
                        $trans_count['user_id'] = $user_id;


                        $qtyDtls = $tra_detail->qty;
                        $originalPriceDtls = $tra_detail->original_price;
                        $discountPercentDtls = $tra_detail->discount_percent;
                        $price = $originalPriceDtls;
                        $discount_amount = 0.0;
                        if ($discountPercentDtls > 0) {
                            $discount_amount = (($tra_detail->discount_percent / 100) * $originalPriceDtls);
                            $discountPriceDtls = ($originalPriceDtls - (($tra_detail->discount_percent / 100) * $originalPriceDtls));
                            $price = $discountPriceDtls;
                        }

                        $itemSubtotal = $originalPriceDtls * $qtyDtls;

                        $totalItem += $qtyDtls;
                        $totalItemAmountNoTax += $itemSubtotal;

                        $itemDiscountAmount = $discount_amount * $qtyDtls;
                        $discount_total_amount += $itemDiscountAmount;
                        $sub_total_amount += $price * $qtyDtls;
                        $itemOriginalAmount = $tra_detail->original_price * $qtyDtls;
                        $originalprice_total_amount += $itemOriginalAmount;


                        if (!$this->Transactioncount->save($trans_count)) {
                            // if error in saving review rating,
                            $this->db->trans_rollback();

                            $this->set_flash_msg( 'error', get_msg( 'err_model' ));
                            redirect(site_url(' checkout/checkout_payment/'.$catHeader->id.''));

                        }

                    }
                }


                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $this->set_flash_msg( 'error', get_msg( 'err_model' ));
                    redirect(site_url(' checkout/checkout_payment/'.$catHeader->id.''));
                } else {
                    $total_item_amount = $sub_total_amount;
                    if ($catHeader->coupon_discount_amount > 0) {
                        $total_item_amount = $total_item_amount - $catHeader->coupon_discount_amount;
                    }

                    $tax_amount = (($catHeader->tax_percent / 100) * $total_item_amount);
                    $master_data = array(
                        'total_item_count' => $totalItem,
                        'sub_total_amount' => $sub_total_amount, // after discount
                        'discount_amount' => $discount_total_amount,
                        'coupon_discount_amount' => $catHeader->coupon_discount_amount,
                        'tax_amount' => $tax_amount,
                        'shipping_method_amount' => $shipping_method_amount,
                        'total_item_amount' => $originalprice_total_amount, // without discount
                        'balance_amount' => $total_item_amount + $shipping_method_amount + $tax_amount - $catHeader->coupon_discount_amount,
                    );

                    // save data
                    $this->Transactionheader->save($master_data, $trans_header_id);


                    $status_data = array('transactions_header_id' => $trans_header_id, 'transactions_status_id' => 1);
                    // save data
                    $this->Transactionstatustracking->save($status_data);

                    if ($catHeader->shipping_method_amount < 50) {
                        $orderCount = $timeslotsdetails->order_count + 1;
                        $slot_data['order_count'] = $orderCount;
                        if ($orderCount >= $timeslotsdetails->accept_order_total) {
                            $slot_data['available'] = 0;
                        }

                        // save data
                        $this->Timeslotsdetails->save($slot_data, $timeslotsdetails->id);
                    }
                    // paytabs starts
                    $status = array();
                    if ($payment_method == "paytabs") {
                        $merchant_email = 'info@eshtri.net';
                        $secret_key = 'ZVhfg4c0GwHn6JJqoCoU3wWQSYvgshQsd7HtcUuTqchwV0XkDlfHSbxKa31ClR5yZN2bHw57ImEDmCX0YdHQoeByiCbDugbt3eFL';
                        $merchant_id = '10063345';

                        $params = array('merchant_email' => $merchant_email,
                            'merchant_id' => $merchant_id,
                            'secret_key' => $secret_key,
                            'payment_reference' => '');

                        $this->load->library('paytabs', $params);
                        $unit_price = $total_item_amount + $shipping_method_amount + $tax_amount - $catHeader->coupon_discount_amount;
                        if ($shipping_email == null || $shipping_email == "") {
                            $shipping_email = "sales@eshtri.net";
                        }
                        if ($shipping_first_name == null || $shipping_first_name == "") {
                            $shipping_first_name = "customer name";
                        }
                        if ($shipping_first_name == null || $shipping_first_name == "") {
                            $shipping_first_name = "customer name";
                        }
                        if ($cc_phone_number == null || $cc_phone_number == "") {
                            $cc_phone_number = "966";
                        }
                        if (strlen($shipping_phone) > 9) {
                            $formattedNumber = str_replace("966", "", $shipping_phone);
                            $formattedNumber = str_replace('+', "", $formattedNumber);
                            $shipping_phone = intval($formattedNumber);
                        } else if (strlen($shipping_phone) < 9) {
                            $shipping_phone = "536176364";
                        }
                        if ($shipping_address_1 == null || $shipping_address_1 == "") {
                            $shipping_address_1 = "customer address";
                        }
                        if ($shipping_address_2 == null || $shipping_address_2 == "") {
                            $shipping_address_2 = "customer state";
                        }
                        if ($shipping_city == null || $shipping_city == "") {
                            $shipping_city = "customer city";
                        }

                        $url="https://secure.paytabs.sa/payment/request";
                        $data=array(
                            "profile_id" => 58545,
                            "tran_type" => "sale",
                            "tran_class" => "ecom",
                            "cart_description" => "Order nnumber-".$trans_header_id."",
                            "cart_id" => "Order-".$trans_header_id."",
                            "cart_currency" => "SAR",
                            "cart_amount" => $unit_price,
                            "callback" => "https://www.eshtri.net",
                            "return" => "https://www.eshtri.net/paymentsuccessurl/transection/".$trans_header_id."",
                            "customer_details" => array(
                                "name"=>"".$shipping_first_name."",
                                "email"=>"".$shipping_email."",
                                "phone_number"=>"966".$shipping_phone."",
                                "street1"=>"".$shipping_address_1."",
                                "city"=>"".$shipping_city."",
                                "state"=>"".$shipping_address_2."",
                                "country"=>"".$country."",
                                "ip"=>"127.0.0.1"

                            ),
                        );
                        $postdata = json_encode($data);

                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: SDJN9JL69Z-JBDDDMB9GW-JG9DWZGNKB'));
                        $result = curl_exec($ch);
                        curl_close($ch);

                        $response=json_decode($result);
//                        $postdata = array(
//                            "merchant_email" => "info@eshtri.net",
//                            'secret_key' => "ZVhfg4c0GwHn6JJqoCoU3wWQSYvgshQsd7HtcUuTqchwV0XkDlfHSbxKa31ClR5yZN2bHw57ImEDmCX0YdHQoeByiCbDugbt3eFL",
//                            'title' => "Order no=" . $trans_header_id . "",
//                            'cc_first_name' => "" . $shipping_first_name . "",
//                            'cc_last_name' => "" . $shipping_first_name . "",
//                            'email' => "" . $shipping_email . "",
//                            'cc_phone_number' => "" . $cc_phone_number . "",
//                            'phone_number' => "" . $shipping_phone . "",
//                            'billing_address' => "" . $shipping_address_1 . "",
//                            'city' => "" . $shipping_city . "",
//                            'state' => "" . $shipping_address_2 . "",
//                            'postal_code' => "" . $postal_code . "",
//                            'country' => "" . $country . "",
//                            'address_shipping' => "" . $shipping_address_1 . "",
//                            'city_shipping' => "" . $shipping_city . "",
//                            'state_shipping' => "" . $shipping_address_2 . "",
//                            'postal_code_shipping' => "" . $postal_code . "",
//                            'country_shipping' => "" . $country . "",
//                            'currency' => "SAR",
//                            "products_per_title" => $totalItem,
//                            "unit_price" => "" . $unit_price . "",
//                            'quantity' => "1",
//                            'other_charges' => "0",
//                            'amount' => "" . $unit_price . "",
//                            'discount' => "0",
//                            "msg_lang" => "arabic",
//                            "reference_no" => "" . $trans_header_id . "",
//                            "site_url" => "https://www.eshtri.net",
//                            'return_url' => "https://www.eshtri.net/paymentsuccessurl/transection/" . $trans_header_id . "",
//                            "cms_with_version" => "API USING PHP",
//                            //"payment_type" => "mada"
//                        );
//
//                        $response = $this->paytabs->create_pay_page($postdata);
                        // echo $response->payment_url;
                        if($response->tran_ref){
//				             $status=array('success'=>1, 'url'=>$response->payment_url, 'result' =>$response->result, 'p_id' =>$response->p_id);
                            $payment_data['payment_reference']=$response->tran_ref;
                            // save data
                            $this->Transactionheader->save($payment_data, $trans_header_id);
                        }

                    }
                    // Paytabs ends


                    $this->db->trans_commit();
                }

                $trans_header_obj = $this->Transactionheader->get_one($trans_header_id);

                //Sending Email to shop
                $to_who = "shop";
                $subject = get_msg('order_receive_subject');

//                if (!send_transaction_order_emails($trans_header_id, $to_who, $subject)) {
//
//                    //$this->error_response( get_msg( 'err_email_not_send_to_shop' ));
//
//                }

                //Sending Email To user
                $to_who = "user";
                $subject = get_msg('order_receive_subject');

//                if (!send_transaction_order_emails($trans_header_id, $to_who, $subject)) {
//
//                    //$this->error_response( get_msg( 'err_email_not_send_to_user' ));
//
//                }

                //Sending Email to Eshtri
                $to_who = "eShtri";
                $subject = get_msg('order_receive_subject');

//                if (!send_transaction_order_emails($trans_header_id, $to_who, $subject)) {
//
//                    //$this->error_response( get_msg( 'err_email_not_send_to_shop' ));
//
//                }
                if ($payment_method == "paytabs") {
                    if($response->tran_ref){
                        $trans_header_obj->payment_url=$response->redirect_url;
                        $trans_header_obj->p_id=$response->tran_ref;
                        $this->Cartdetail->delete_all_records(array("cart_header_id"=>$catHeader->id));
                        $nowtime = $this->session->userdata('current_time');
                        $this->Cartheader->delete_all_records(array('time' => $nowtime, 'shop_id' => $catHeader->shop_id));
                        $this->session->set_userdata('current_order_id', "");
                        redirect($trans_header_obj->payment_url);
                    }
                }


            }


        }

        $this->Cartdetail->delete_all_records(array("cart_header_id"=>$catHeader->id));
        $nowtime = $this->session->userdata('current_time');
        $this->Cartheader->delete_all_records(array('time' => $nowtime, 'shop_id' => $catHeader->shop_id));
        $this->session->set_userdata('current_order_id', "");
        redirect(site_url('/'));
    }

}