<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class App extends FE_Controller

{


    function __construct()

    {

        parent::__construct(NO_AUTH_CONTROL, 'shop');
    }

    function index() {
        $this->data['bodyclass'] = "outside_shop basicpages app";
        $this->data['page_name'] = "app_download";
        $this->data['redirect_url'] = site_url('/app');
        $this->data['tags'] = $this->Tag->get_all_by( array());
        $this->data['feeds'] = $this->Feed->get_all_by( array());
        $this->data['featuredproducts'] = $this->Product->get_all_by( array("is_featured"=>1, "is_available"=>1, "is_available"=>1) , 10, 0 );
        $this->data['newarrival'] = $this->Product->get_all_by( array("order_by"=>1, "order_by_field"=>"added_date", "order_by_type"=>"asc") , 10, 0 );
        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc") , 10, 0 );
        $this->data['popularshop'] = $this->Shop->get_all_by(array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc"));
        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;
        $this->load_front_template( 'home',$this->data, true );

    }

}