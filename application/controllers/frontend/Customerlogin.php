<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Customerlogin extends FE_Controller

{


    function __construct()

    {

        parent::__construct(NO_AUTH_CONTROL, 'shop');
    }

    function login($shop_id="") {
        if ($this->session->userdata('current_user_id')){
            redirect(site_url('customerlogin/shippingaddress/'.$this->session->userdata('current_user_id')));
        }
        $this->data['bodyclass'] = "outside_shop basicpages customerlogin";
        $this->data['page_name'] = "customer_login";
        $this->data['redirect_url'] = "shippingaddress";
        $this->data['is_login_page'] = true;
        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;
        $this->load_front_template( 'customerlogin',$this->data, true );
    }

    function shippingaddress($user_id="") {
        $this->data['bodyclass'] = "outside_shop basicpages shipping_address";
        $this->data['redirect_url'] = site_url('/customerlogin/shippingaddress/'.$user_id);
        $current_user_id=$this->session->userdata('current_user_id');
        $current_user_mobile=$this->session->userdata('current_user_mobile');
        if($current_user_id && $current_user_mobile){
            $this->data['user'] = $this->User->get_one_by(array("user_id"=>$user_id));
            $this->data['current_order_id']=$this->session->userdata('current_order_id');
            $useraddress_result = $this->UserAddress->get_all_by(array("user_id"=>$user_id));

            if ( !empty( $useraddress_result ) && count( $useraddress_result->result()) > 0 ){
                $this->data['useraddress'] = $useraddress_result;
                $this->data['page_name'] = "shipping_address";
            }else{
                redirect(site_url('customerlogin/newaddressmap/'.$this->session->userdata('current_user_id')));
            }
            $this->load_front_template( 'shipping_address',$this->data, true );
        }else{
            redirect(site_url('/'));
        }

    }
    function newaddressmap($user_id="") {
        $this->data['bodyclass'] = "outside_shop basicpages newaddressmap";
        $this->data['redirect_url'] = site_url('/customerlogin/newaddressmap/'.$user_id);
        $current_user_id=$this->session->userdata('current_user_id');
        $current_user_mobile=$this->session->userdata('current_user_mobile');
        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;

        if($current_user_id && $current_user_mobile){
            $this->data['current_user_id'] = $current_user_id;
            $this->data['selected_front_shop_id'] = $this->session->userdata('selected_front_shop_id');
            $this->data['user'] = $this->User->get_one_by(array("user_id"=>$user_id));
            $useraddress_result = $this->UserAddress->get_all_by(array("user_id"=>$user_id));
            $this->data['useraddress'] = $useraddress_result;
            $this->data['page_name'] = "new_address_map";

            if ( !empty( $useraddress_result ) && count( $useraddress_result->result()) > 0 ){
                $this->data['hasaddress'] = 1;
            }else{
                $this->data['hasaddress'] = 0;
            }

            $this->load_front_template( 'newaddressmap',$this->data, true );
        }else{
            redirect(site_url('/'));
        }

    }

    function newaddressform($user_id="") {
        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;
        $current_user_id=$this->session->userdata('current_user_id');
        $current_user_mobile=$this->session->userdata('current_user_mobile');
        if($current_user_id && $current_user_mobile && $current_user_id == $user_id){
            $this->data['selected_front_shop_id'] = $this->session->userdata('selected_front_shop_id');
            $this->data['current_user_id'] = $current_user_id;
            $this->data['user'] = $this->User->get_one_by(array("user_id"=>$current_user_id));
            $this->data['useraddress'] = $this->UserAddress->get_all_by(array("user_id"=>$user_id));
            $this->data['page_name'] = "new_address_form";
            $this->data['redirect_url'] = site_url('/customerlogin/newaddressform/'.$user_id);
            $this->load_front_template( 'newaddressform',$this->data, true );
        }else{
            redirect(site_url('/'));
        }

    }

    function newaddresssubmit() {
        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;
        $current_user_id=$this->session->userdata('current_user_id');
        $current_user_mobile=$this->session->userdata('current_user_mobile');
        if($current_user_id && $current_user_mobile){
            $this->data['current_user_id'] = $current_user_id;
            $this->data['selected_front_shop_id'] = $this->session->userdata('selected_front_shop_id');
            $this->data['user'] = $this->User->get_one_by(array("user_id"=>$user_id));
            $this->data['useraddress'] = $this->UserAddress->get_all_by(array("user_id"=>$user_id));
            $this->data['page_name'] = "new_address_submit";
            $this->data['redirect_url'] = site_url('/customerlogin/newaddresssubmit/'.$user_id);
            $this->load_front_template( 'newaddressform',$this->data, true );
        }else{
            redirect(site_url('/'));
        }

    }

    function loginsubmit(){

        if($this->input->post( 'mobile')) {
            if ($this->input->post( 'name')) {
                $user_data['user_name'] = $this->input->post( 'name');
            }
            $mobileno=$this->input->post( 'mobile');
            $lengt=strlen($mobileno);
            $mobileno_int = (int)$mobileno;
            if($lengt>11 && preg_match('/^(9665)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/', $mobileno)){
                $mobileno=$mobileno;
            }else{
                $mobileno="+966".$mobileno_int;
            }
            $user_data['user_phone'] = $mobileno;
            $checkuser=$this->User->get_one_by(array("user_phone"=>$mobileno));
            if(!$checkuser->user_id){
                $this->User->save_user( $user_data, array(), false);
            }

            $user=$this->User->get_one_by(array("user_phone"=>$mobileno));
            if($user->user_id){
                $this->session->set_userdata('current_user_id', $user->user_id);
                $this->session->set_userdata('current_user_mobile', $user->user_phone);
                if($this->input->post( 'redirecturl')){
                    $redirect=$this->input->post( 'redirecturl');
                    if($this->input->post( 'redirecturl')=="shippingaddress"){
                        $redirect=site_url('/customerlogin/shippingaddress/'.$user->user_id);
                    }

                }
                $arr = array('isError' => false, 'message' =>  "logged", 'user' =>   $user, 'redirect' => $redirect );
                echo json_encode( $arr );
                exit;
            }else{
                redirect(site_url('/'));
                $arr = array('isError' => true, 'message' =>  "error");
                echo json_encode( $arr );
                exit;
            }

        }else{
            redirect(site_url('/'));
            $arr = array('isError' => true, 'message' =>  "error");
            echo json_encode( $arr );
            exit;
        }


    }

    function logout() {
        // logout
        $this->session->unset_userdata('current_user_id');
        $this->session->unset_userdata('current_user_mobile');

        // redirect
        redirect(site_url('/'));
    }

}