<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Dashboard extends FE_Controller

{


    function __construct()

    {

        parent::__construct(NO_AUTH_CONTROL, 'shop');
    }

    function index() {
        if (!$this->session->userdata('current_user_id')){
            redirect(site_url('customerlogin/logout'));
        }
        $this->data['bodyclass'] = "outside_shop userdashboard dashboard";
        $this->data['page_name'] = "customer_dashboard";
        $this->data['redirect_url'] = site_url('/dashboard');
        $this->data['tags'] = $this->Tag->get_all_by( array());
        $this->data['feeds'] = $this->Feed->get_all_by( array());
        $this->data['featuredproducts'] = $this->Product->get_all_by( array("is_featured"=>1, "is_available"=>1, "is_available"=>1) , 10, 0 );
        $this->data['newarrival'] = $this->Product->get_all_by( array("order_by"=>1, "order_by_field"=>"added_date", "order_by_type"=>"asc") , 10, 0 );
        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc") , 10, 0 );
        $this->data['popularshop'] = $this->Shop->get_all_by(array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc"));
        $this->load_front_template( 'dashboard',$this->data, true );
    }

    function profile() {
        if (!$this->session->userdata('current_user_id')){
            redirect(site_url('customerlogin/logout'));
        }
        $this->data['bodyclass'] = "outside_shop userdashboard accountdetails";
        $this->data['page_name'] = "customer_profile";
        $this->data['redirect_url'] = site_url('/accountdetails');
        $this->data['tags'] = $this->Tag->get_all_by( array());
        $this->data['feeds'] = $this->Feed->get_all_by( array());
        $this->data['featuredproducts'] = $this->Product->get_all_by( array("is_featured"=>1, "is_available"=>1, "is_available"=>1) , 10, 0 );
        $this->data['newarrival'] = $this->Product->get_all_by( array("order_by"=>1, "order_by_field"=>"added_date", "order_by_type"=>"asc") , 10, 0 );
        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc") , 10, 0 );
        $this->data['popularshop'] = $this->Shop->get_all_by(array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc"));
        $this->load_front_template( 'accountdetails',$this->data, true );
    }

    function addresses() {
        if (!$this->session->userdata('current_user_id')){
            redirect(site_url('customerlogin/logout'));
        }
        $this->data['bodyclass'] = "outside_shop, userdashboard, address";
        $this->data['page_name'] = "customer_panel_addresses";
        $this->data['redirect_url'] = site_url('/address');
        $current_user_id=$this->session->userdata('current_user_id');
        $current_user_mobile=$this->session->userdata('current_user_mobile');
        $this->data['user'] = $this->User->get_one_by(array("user_id"=>$current_user_id));
        $this->data['useraddress'] = $this->UserAddress->get_all_by(array("user_id"=>$current_user_id));
        $this->load_front_template( 'address',$this->data, true );
    }

    function orders() {
        if (!$this->session->userdata('current_user_id')){
            redirect(site_url('customerlogin/logout'));
        }
        $this->data['bodyclass'] = "outside_shop userdashboard orderlist";
        $this->data['page_name'] = "customer_panel_orders";
        $this->data['redirect_url'] = site_url('/orderlist');
        $this->data['tags'] = $this->Tag->get_all_by( array());
        $this->data['feeds'] = $this->Feed->get_all_by( array());
        $this->data['featuredproducts'] = $this->Product->get_all_by( array("is_featured"=>1, "is_available"=>1, "is_available"=>1) , 10, 0 );
        $this->data['newarrival'] = $this->Product->get_all_by( array("order_by"=>1, "order_by_field"=>"added_date", "order_by_type"=>"asc") , 10, 0 );
        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc") , 10, 0 );
        $this->data['popularshop'] = $this->Shop->get_all_by(array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc"));
        $this->data['orders'] = $this->Transactionheader->get_all_by(array("user_id"=>"usrff6da05a0806b658987005014046afc7"));
        $this->load_front_template( 'orderlist',$this->data, true );

    }

    function orderdetails($order_id="") {
        if (!$this->session->userdata('current_user_id')){
            redirect(site_url('customerlogin/logout'));
        }
        $this->data['redirect_url'] = site_url('/orderlist/orderdetails/'.$order_id);
        $this->data['page_name'] = "customer_panel_order_details";
        $order=$this->Transactionheader->get_one_by(array('id' => $order_id));
        $order->details=$this->Transactiondetail->get_all_by(array('transactions_header_id' => $order_id))->result();
        $this->data['transactiondetails'] = $order;
        $this->load_front_template( 'orderdetails',$this->data, true );
    }

}