<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Home extends FE_Controller

{


    function __construct()

    {

        parent::__construct(NO_AUTH_CONTROL, 'shop');
    }

    function index() {
        $this->data['bodyclass'] = "outside_shop basicpages homepage";
        $this->data['page_name'] = "website_home";
        $this->data['redirect_url'] = site_url('/');
        $this->data['tags'] = $this->Tag->get_all_by( array());
        $this->data['feeds'] = $this->Feed->get_all_by( array());

        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;
        $this->data['featuredproducts'] = $this->Product->get_all_by( array("is_featured"=>1, "is_available"=>1, "is_available"=>1) , 10, 0 );
        $this->data['newarrival'] = $this->Product->get_all_by( array("order_by"=>1, "order_by_field"=>"added_date", "order_by_type"=>"asc") , 10, 0 );
        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc") , 10, 0 );
        $json="https://eshtri.net/eshtri/rest/tags/get/api_key/companydevelopmentthebest/limit/10/offset/0/lang/en";
        $this->data['popularshop'] = $this->Shop->get_all_by(array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc"));
        $this->load_front_template( 'home',$this->data, true );
    }

    function productdetails($product_id="") {
        $product = $this->Product->get_one_by( array("id"=>$product_id));
        $this->session->set_userdata('selected_front_shop_id', array('shop_id' => $product->shop_id));
        $selected_shop_id = $this->session->userdata('selected_front_shop_id');
        $shop_id = $selected_shop_id['shop_id'];
        $this->data['redirect_url'] = site_url('/home/productdetails/'.$product_id);
        $this->data['page_name'] = "product_details";
        $this->data['product'] = $product;
        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>$shop_id));

        $nowtime = $this->session->userdata('current_time');
        $this->data['carttime'] = $nowtime;
        $transaction=array();
        if($nowtime){
            $transaction=$this->Cartheader->get_one_by(array('time' => $nowtime, 'shop_id' => $product->shop_id));
        }
        $this->data['transaction'] = $transaction;
//        $this->data['popularshop'] = $this->Shop->get_all_by(array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc"));

        //$this->data['reletedproducts'] = $this->Product->get_all_related_product_trending( array("id"=>$product->id, "cat_id"=>$product->cat_id, "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a"), 10, 0 )->result();

        $this->load_front_template( 'productdetails',$this->data, true );

    }
}