<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Cart extends FE_Controller

{


    function __construct()

    {

        parent::__construct(NO_AUTH_CONTROL, 'shop');
    }

    function cartdetails($shop_id="") {
        $this->session->set_userdata('selected_front_shop_id', $shop_id);
        $selected_shop_id = $this->session->userdata('selected_front_shop_id');
        $this->data['shop_id'] = $shop_id;
        $this->data['bodyclass'] = "inside_shop carpages cartdetails";
        $this->data['page_name'] = "cart_details";
        $this->data['redirect_url'] = site_url('/cart/cartdetails/'.$shop_id);
        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;
        $transaction=array();
        if($nowtime){
            $transaction=$this->Cartheader->get_one_by(array('time' => $nowtime, 'shop_id' => $shop_id));
        }
        $this->data['transaction'] = $transaction;
        $this->session->set_userdata('current_order_id', $transaction->id);

        $this->load_front_template( 'cartdetails',$this->data, true );

    }


    function checkout($shop_id="") {
        $this->session->set_userdata('selected_front_shop_id', $shop_id);
        $selected_shop_id = $this->session->userdata('selected_front_shop_id');
        redirect(site_url('/customerlogin/login/'.$shop_id.''));

    }

    function destroy($shop_id="") {
        $this->session->set_userdata('selected_front_shop_id', $shop_id);
        $selected_shop_id = $this->session->userdata('selected_front_shop_id');
        $this->data['shop_id'] = $shop_id;
        $nowtime = $this->session->userdata('current_time');

        $transaction = $this->Cartheader->get_one_by(array('time' => $nowtime, 'shop_id' => $shop_id));

        $this->Cartdetail->delete_all_records(array("cart_header_id"=>$transaction->id));
        $this->Cartheader->delete_all_records(array('time' => $nowtime, 'shop_id' => $shop_id));
        $this->session->set_userdata('current_order_id', "");
        redirect(site_url('/shopproducts/shopdashboard/'.$shop_id.''));
    }

}