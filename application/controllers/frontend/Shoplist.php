<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Shoplist extends FE_Controller

{
    function __construct()

    {

        parent::__construct(NO_AUTH_CONTROL, 'shop');
    }

    function index() {
        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;
        $this->data['bodyclass'] = "outside_shop basicpages homepages shoplist";
        $this->data['page_name'] = "shoplist";
        $this->data['redirect_url'] = site_url('/shoplist');
        $this->data['tags'] = $this->Tag->get_all_by( array());
        $this->data['feeds'] = $this->Feed->get_all_by( array());
//        $this->data['discountproducts'] = $this->Product->get_all_by( array("is_discount"=>1, "is_available"=>1, "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a"));
        $this->data['featuredproducts'] = $this->Product->get_all_by( array("is_featured"=>1, "is_available"=>1, "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a"));
//        $this->data['newproduct'] = $this->Product->get_all_by( array("order_by"=>1, "order_by_field"=>"added_date", "order_by_type"=>"asc", "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a") , 10, 0 );
//        $this->data['popularproduct'] = $this->Product->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a") , 10, 0 );
//        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a"));
        $this->data['popularshop'] = $this->Shop->get_all_by(array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc"));
        $this->load_front_template( 'shoplist',$this->data, true );

    }
    function shopcategory($cat_id="") {
        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;
        $this->data['bodyclass'] = "outside_shop basicpages homepages shopcategory";
        $this->data['page_name'] = "shop_category";
        $this->data['redirect_url'] = site_url('/shoplist/shopcategory/'.$cat_id);
        $this->data['catshop'] = $this->Shop->get_all_by( array("cat_id"=>$cat_id, "order_by"=>1, "order_by_field"=>"added_date", "order_by_type"=>"asc", "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a") , 10, 0 );
        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a"));
//        $this->data['popularshop'] = $this->Shop->get_all_by(array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc"));
        $this->load_front_template( 'categoryproducts',$this->data, true );

    }

//    function productdetails($product_id="") {
//
//        $product = $this->Product->get_one_by( array("id"=>$product_id, "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a"));
//        $this->data['product'] = $product;
//        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a"));
////        $this->data['popularshop'] = $this->Shop->get_all_by(array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc"));
//
//        //$this->data['reletedproducts'] = $this->Product->get_all_related_product_trending( array("id"=>$product->id, "cat_id"=>$product->cat_id, "shop_id"=>"shop98ef587bb29242db042804cf245b7d0a"), 10, 0 )->result();
//
//        $this->load_front_template( 'productdetails',$this->data, true );
//
//    }

    function productsegession() {
        if ($this->input->get("producSearch")) {
            $productsDropdownList = $this->Acc_coa->productsDropdownList($this->input->get());
            echo json_encode($productsDropdownList);
            exit();
        }
    }

}