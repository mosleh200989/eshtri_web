<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users crontroller for BE_USERS table
 */
class Payment_voucher extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'Acc_coa' );
	}

	/**
	* Load Notification Sending Form
	*/
	function index() {
		//header('Content-Type: application/json');
		$langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
		$this->data['action_title'] = "Payment Voucher";
		$this->data['currentPeriod'] = $this->Acc_coa->currentPeriod();
		$this->data['coaOptionGroup'] = $this->Acc_coa->coaOptionBankCash();
		$this->data['coaOptionExpense'] = $this->Acc_coa->coaOptionExpense();
		$this->data['paymentMethodList'] = $this->Acc_coa->paymentMethodDropdownList();
		$this->data['drcrTypeList'] = $this->Acc_coa->enumListLanguage("drcrType");
		$this->data['stakeHolderTypeList'] = $this->Acc_coa->enumListLanguage("stakeHolderType");
		$this->data['confirmStatusList'] = $this->Acc_coa->enumListLanguage("confirmStatus");
		$this->data['accPeriodList'] = $this->Acc_coa->allClosedPeriodOptions();
		$this->data['coaOptionGroupList'] = $this->Acc_coa->coaOptionGroup();
		$this->data['coaClass'] = $this->Acc_coa->coaOptionDropDown(null, "CLS", null);
		// for showing data
		if($this->input->get("showAction")){
			$conds['id'] = $this->input->get("id");
			$_data = $this->Acc_vcr_mst->get_one_by($conds);
			if (!$_data) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$condss['id'] = $_data->acc_period_id;
			$accperiod = $this->Acc_period->get_one_by($condss);
			$detailsQuery="SELECT dtl.*, coa_local.caption AS coaCaption FROM acc_vcr_mst AS mst LEFT JOIN acc_vcr_dtl dtl ON mst.id = dtl.vcr_mst_id LEFT JOIN acc_coa coa ON dtl.acc_coa_id = coa.id LEFT JOIN acc_coa coa_grp ON coa.acc_coa_id = coa_grp.id LEFT JOIN acc_coa_locale coa_local ON coa_local.coa_id = coa.id where coa_local.language_enum ='".$langEnumKey."' AND dtl.vcr_mst_id=".$this->input->get("id")."";
			$resultSet = $this->Acc_period->queryPrepare($detailsQuery);
			$accVcrDtls=$resultSet->result();
			$data = array('isError' => false, 'defaultInstance' =>  $_data, 'acc_period'=>$accperiod->caption, 'stakeHolderType'=>get_msg($_data->stake_holder_type), 'activeStatus'=>get_msg($_data->active_status), 'accVcrDtls'=>$accVcrDtls);    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		// for editing data
		if($this->input->get("editData")){
			$conds['id'] = $this->input->get("id");
			$_data = $this->Acc_vcr_mst->get_one_by($conds);
			if (!$_data) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			if ($_data->confirm_status != "Draft") {
				$data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
            }
			$condss['id'] = $_data->acc_period_id;
			$accperiod = $this->Acc_period->get_one_by($condss);
			$detailsQuery="SELECT dtl.*, coa_local.caption AS coaCaption FROM acc_vcr_mst AS mst LEFT JOIN acc_vcr_dtl dtl ON mst.id = dtl.vcr_mst_id LEFT JOIN acc_coa coa ON dtl.acc_coa_id = coa.id LEFT JOIN acc_coa coa_grp ON coa.acc_coa_id = coa_grp.id LEFT JOIN acc_coa_locale coa_local ON coa_local.coa_id = coa.id where coa_local.language_enum ='".$langEnumKey."' AND dtl.vcr_mst_id=".$this->input->get("id")."";
			$resultSet = $this->Acc_period->queryPrepare($detailsQuery);
			$accVcrDtlsResult=$resultSet->result();
			$accVcrDtls = array();
			$accCoa="";
			foreach($accVcrDtlsResult as $dtl){
				if ($dtl->drcr_type == "Debit") {
					$accVcrDtls[]=array('id'=>$dtl->id,'accCoaId'=>$dtl->acc_coa_id,'accCoaText'=>$dtl->coaCaption,'drAmount'=>$dtl->dr_amount,'crAmount'=>$dtl->cr_amount,'drcrType'=>$dtl->drcr_type,'narration'=>$dtl->narration);
                } else {
					$accCoa=$dtl->acc_coa_id;
                }
			}

			$data = array('isError' => false, 'defaultInstance' =>  $_data, 'acc_period'=>$accperiod, 'stakeHolderType'=>get_msg($_data->stake_holder_type), 'activeStatus'=>get_msg($_data->active_status), 'accVcrDtls'=>$accVcrDtls, 'accCoa'=>$accCoa);    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		if ($this->input->get("stakeholderSearch")) {
			$stakHldrDropdownList = $this->Acc_coa->stakHldrDropdownList($this->input->get());
			echo json_encode($stakHldrDropdownList);
			exit();
		}
		if ($this->input->get("stakeholderSearchRpt")) {
			$stakHldrDropdownList = $this->Acc_coa->stakHldrDropdownList($this->input->get());
			echo json_encode($stakHldrDropdownList);
			exit();
        }
		$this->load_template('payment_voucher/list', $this->data, true );
	}


	public function voucher_list()
	{

		$iDisplayStart = $this->input->get("iDisplayStart")? intval($this->input->get("iDisplayStart")):0;
		$iDisplayLength = $this->input->get("iDisplayLength")? intval($this->input->get("iDisplayLength")) :10;
		$sSortDir = $this->input->get("sSortDir_0");
		$iSortingCol = $this->input->get("iSortCol_0")? $this->input->get("iSortCol_0") : null;
		$sSearch = $this->input->get("sSearch") ? $this->input->get("sSearch") : null;
		if($sSearch){
			$sSearch = "% ".$sSearch." %";
		}
		
		$dataReturns = array();
	
		$langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
		$sqlWhere = "AND coa_local.language_enum ='".$langEnumKey."'";
		$sqlWhere .= ($this->input->get("trans_date") && $this->input->get("trans_date_to")) ? ((" AND mst.trans_date >= '".$this->input->get("trans_date")."'").(" AND mst.trans_date <= '".$this->input->get("trans_date_to")."'")) : "";
		$sqlWhere .= $this->input->get("confirm_status") ? " AND mst.confirm_status='" .$this->input->get("confirm_status"). "'" : "";
		$sqlWhere .= $this->input->get("stake_holder_type") ? " AND mst.stake_holder_type='".$this->input->get("stake_holder_type")."'" : "";
		$sqlWhere .= $this->input->get("stake_holder_name") ? " AND mst.stakeholder_name='" .$this->input->get("stake_holder_name"). "'" : "";

		$sqlWhere .= $this->input->get("acc_period") ? (" AND period.id=" . $this->input->get("acc_period")) : '';
		$sqlWhere .= $this->input->get("acc_coa") ? (" AND  dtl.acc_coa_id=" .$this->input->get("acc_coa")) : '';
		$sqlWhere .= $this->input->get("coa_class") ? (" AND coa_grp.acc_coa_id=" .$this->input->get("coa_class")) : '';
		$sqlWhere .= $this->input->get("sSearch") ? " AND (coa_local.caption LIKE '%".$this->input->get("sSearch")."%' OR mst.code LIKE '%".$this->input->get("sSearch")."%' OR coa.code LIKE '%".$this->input->get("sSearch")."%')" : "";
		$querySQL = "SELECT mst.id, mst.code AS code, mst.trans_date AS trans_date, mst.stakeholder_name AS stakeholder, period.caption AS period, mst.total_amount AS amount, (SELECT group_concat(coa_local.caption, ',') FROM acc_vcr_dtl dtl LEFT JOIN acc_coa coa ON dtl.acc_coa_id = coa.id LEFT JOIN acc_coa coa_grp ON coa.acc_coa_id = coa_grp.id LEFT JOIN acc_coa_locale coa_local ON coa_local.coa_id = coa.id where mst.entity_type = 'Payment_Voucher' AND coa_local.language_enum ='".$langEnumKey."' and dtl.vcr_mst_id = mst.id) AS Accounts2, coa_local.language_enum AS lang, mst.confirm_status AS confirm_status FROM acc_vcr_mst AS mst LEFT JOIN acc_vcr_dtl dtl ON mst.id = dtl.vcr_mst_id LEFT JOIN acc_coa coa ON dtl.acc_coa_id = coa.id LEFT JOIN acc_coa coa_grp ON coa.acc_coa_id = coa_grp.id LEFT JOIN acc_coa_locale coa_local ON coa_local.coa_id = coa.id LEFT JOIN acc_period period on mst.acc_period_id = period.id where mst.entity_type = 'Payment_Voucher' AND dtl.drcr_type = 'Debit' ".$sqlWhere." GROUP BY dtl.vcr_mst_id LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart."";
		$resultSet = $this->Acc_period->queryPrepare($querySQL);
		$totalCount = $resultSet->num_rows();
		$serial = 0;
			$total = 0;
			
		$actionList = array();
		
			$actionListConfirm = array();
			$actionList[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
			$actionList[]=array('actionName' => 'update', 'actionClass' => 'update', 'titleName' => get_msg( 'update_data'), 'iconName' => 'fa fa-pencil-square-o text-navy');
			$actionList[]=array('actionName' => 'confirm', 'actionClass' => 'confirm', 'titleName' => get_msg( 'Confirm_Data'), 'iconName' => 'fa fa-check-circle text-warning');
			$actionList[]=array('actionName' => 'delete', 'actionClass' => 'delete', 'titleName' => get_msg( 'Delete_Data'), 'iconName' => 'fa fa-trash text-danger');
			$actionListConfirm[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
			$actionListConfirm[]=array('actionName' => 'index', 'actionClass' => 'print', 'titleName' => get_msg( 'Print_Report'), 'iconName' => 'fa fa-print text-navy');
				
		foreach ($resultSet->result() as $row)
		{
			$serial++;
			$dataReturns[]=array('DT_RowId' => $row->id, 'accessibleUrl' => (($row->confirm_status == "Draft") ? $actionList : $actionListConfirm),'activeStatus' => get_msg( 'Active'), 0 => $serial, 1 => $row->period,2 => $row->trans_date,3 => $row->stakeholder,4 => $row->Accounts2,5 => $row->amount,6 => "");
		}
		
		$gridData=array();
		if ($totalCount == 0) {
			$gridData=array('iTotalRecords' => 0, 'iTotalDisplayRecords' =>0, 'aaData'=> array());
			echo json_encode($gridData);
			exit();
		}

		$gridData=array('iTotalRecords' => $totalCount, 'iTotalDisplayRecords' =>$totalCount, 'aaData'=> $dataReturns);
		echo json_encode($gridData);
		exit();
	}


	function write(){
		header('Content-Type: application/json'); 
		$rules = array(
			array(
					'field' => 'acc_period_id',
					'label' => get_msg('Period'),
					'rules' => 'required'
			),
			array(
					'field' => 'trans_date',
					'label' => get_msg('trans_date'),
					'rules' => 'required'
			),
			array(
				'field' => 'stake_holder_type',
				'label' => get_msg('bearer_type'),
				'rules' => 'required'
			),
			array(
				'field' => 'stakeholder_id',
				'label' => get_msg('stakeholder'),
				'rules' => 'required'
			),
			array(
				'field' => 'acc_coa',
				'label' => get_msg('acc_coa'),
				'rules' => 'required'
		    ),
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$receivableleHeadconds['journals_type'] = "Purchase";
		$receivableleHead = $this->Acc_coa->get_one_by($receivableleHeadconds);
		if (!$receivableleHead->id) {
			$data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$data = array(
			'entity_type' => "Payment_Voucher",
			'caption' => get_msg('Payment_Voucher')." - ".$this->input->post('trans_date'),
			'acc_period_id' => $this->input->post('acc_period_id'),
			'trans_date' => $this->input->post('trans_date'),
			'stake_holder_type' => $this->input->post('stake_holder_type'),
			'stakeholder_id' => $this->input->post('stakeholder_id'),
			'stakeholder_name' => $this->Acc_period->stakeholderName($this->input->post('stake_holder_type'), $this->input->post('stakeholder_id')),
			'reference_code' => $this->input->post('reference_code'),
			'cheque_no' => $this->input->post('cheque_no'),
			'cheque_date' => $this->input->post('cheque_date'),
			'narration' => $this->input->post('narration'),
			'confirm_status' => "Draft",
			'active_status' => "Active",
			'total_amount' => $this->input->post('amount'),
			);
			$inserted=$this->Acc_vcr_mst->accSave($data);

			$payableDtls = array(
				'vcr_mst_id' => $inserted,
				'acc_coa_id' => $receivableleHead->id,
				'trans_date' => $this->input->post('trans_date'),
				'stakeholder_id' => $this->input->post('stakeholder_id'),
				'stakeholder_name' => $this->Acc_period->stakeholderName($this->input->post('stake_holder_type'), $this->input->post('stakeholder_id')),
				'narration' => $this->input->post('narration'),
				'dr_amount' => $this->input->post('amount'),
				'cr_amount' => 0.0,
				'drcr_type' => "Debit",
			);
			$this->Acc_vcr_dtl->save($payableDtls);

			$bankOrCashDtls = array(
				'vcr_mst_id' => $inserted,
				'acc_coa_id' => $this->input->post('acc_coa'),
				'trans_date' => $this->input->post('trans_date'),
				'stakeholder_id' => $this->input->post('stakeholder_id'),
				'stakeholder_name' => $this->Acc_period->stakeholderName($this->input->post('stake_holder_type'), $this->input->post('stakeholder_id')),
				'narration' => $this->input->post('narration'),
				'dr_amount' => 0.0,
				'cr_amount' => $this->input->post('amount'),
				'drcr_type' => "Credit",
			);

		if($this->Acc_vcr_dtl->save($bankOrCashDtls)){
			$arr = array('isError' => false, 'message' =>  get_msg('save_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}
	}
	

	function editsubmit() {
		header('Content-Type: application/json'); 
		$rules = array(
			array(
					'field' => 'acc_period_id',
					'label' => get_msg('Period'),
					'rules' => 'required'
			),
			array(
					'field' => 'trans_date',
					'label' => get_msg('trans_date'),
					'rules' => 'required'
			),
			array(
				'field' => 'stake_holder_type',
				'label' => get_msg('bearer_type'),
				'rules' => 'required'
			),
			array(
				'field' => 'stakeholder_id',
				'label' => get_msg('stakeholder'),
				'rules' => 'required'
			),
			array(
				'field' => 'acc_coa',
				'label' => get_msg('acc_coa'),
				'rules' => 'required'
		    ),
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$conds['id'] = $this->input->post("id");
		$_data = $this->Acc_vcr_mst->get_one_by($conds);
		if (!$_data->id) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$payableconds['vcr_mst_id'] = $this->input->post("id");
		$payableconds['drcr_type'] = "Debit";
		$payable = $this->Acc_vcr_dtl->get_one_by($payableconds);

		$bankOrCashconds['vcr_mst_id'] = $this->input->post("id");
		$bankOrCashconds['drcr_type'] = "Credit";
		$bankOrCash = $this->Acc_vcr_dtl->get_one_by($bankOrCashconds);

		if (!$payable->id || !$bankOrCash->id) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		if (!$_data->stakeholder_name) {
			$data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		if ($_data->total_amount==0) {
			$data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$data = array(
			'entity_type' => "Payment_Voucher",
			'caption' => get_msg('Payment_Voucher')." - ".$this->input->post('trans_date'),
			'acc_period_id' => $this->input->post('acc_period_id'),
			'trans_date' => $this->input->post('trans_date'),
			'stake_holder_type' => $this->input->post('stake_holder_type'),
			'stakeholder_id' => $this->input->post('stakeholder_id'),
			'stakeholder_name' => $this->Acc_period->stakeholderName($this->input->post('stake_holder_type'), $this->input->post('stakeholder_id')),
			'reference_code' => $this->input->post('reference_code'),
			'cheque_no' => $this->input->post('cheque_no'),
			'cheque_date' => $this->input->post('cheque_date'),
			'narration' => $this->input->post('narration'),
			'confirm_status' => "Draft",
			'active_status' => "Active",
			'total_amount' => $this->input->post('amount'),
			);
			$inserted=$this->Acc_vcr_mst->save($data, $this->input->post("id"));

			$payableDtls = array(
				'vcr_mst_id' => $this->input->post("id"),
				'trans_date' => $this->input->post('trans_date'),
				'stakeholder_id' => $this->input->post('stakeholder_id'),
				'stakeholder_name' => $this->Acc_period->stakeholderName($this->input->post('stake_holder_type'), $this->input->post('stakeholder_id')),
				'narration' => $this->input->post('narration'),
				'dr_amount' => $this->input->post('amount'),
				'cr_amount' => 0.0,
				'drcr_type' => "Debit",
			);
			$this->Acc_vcr_dtl->save($payableDtls, $payable->id);

			$bankOrCashDtls = array(
				'vcr_mst_id' => $this->input->post("id"),
				'acc_coa_id' => $this->input->post('acc_coa'),
				'trans_date' => $this->input->post('trans_date'),
				'stakeholder_id' => $this->input->post('stakeholder_id'),
				'stakeholder_name' => $this->Acc_period->stakeholderName($this->input->post('stake_holder_type'), $this->input->post('stakeholder_id')),
				'narration' => $this->input->post('narration'),
				'dr_amount' => 0.0,
				'cr_amount' => $this->input->post('amount'),
				'drcr_type' => "Credit",
			);

		if($this->Acc_vcr_dtl->save($bankOrCashDtls, $bankOrCash->id)){
			$arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('update_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}
	}


	function remove(){
		$rules = array(
			array(
					'field' => 'id',
					'label' => get_msg('id'),
					'rules' => 'required'
			)
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$conds['id'] = $this->input->post("id");
		$_data = $this->Acc_vcr_mst->get_one_by($conds);
		if (!$_data->id) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$deleteconds['vcr_mst_id'] = $this->input->post("id");
		$this->Acc_vcr_dtl->delete_all_records($deleteconds);
		
		$deleteMstconds['id'] = $this->input->post("id");
		$this->Acc_vcr_mst->delete_all_records($deleteMstconds);
		$arr = array('isError' => false, 'message' =>  get_msg('deleted_successfully'));    
		echo json_encode( $arr );
		exit;

	}

}