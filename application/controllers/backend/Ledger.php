<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users crontroller for BE_USERS table
 */
class Ledger extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'Acc_coa' );
	}

	/**
	* Load Notification Sending Form
	*/
	function index() {
		$langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
		$this->data['action_title'] = "Expense Voucher";
		$this->data['currentPeriod'] = $this->Acc_coa->currentPeriod();
		$this->data['coaOptionGroup'] = $this->Acc_coa->coaOptionGroup();
		$this->data['accPeriodList'] = $this->Acc_coa->allPeriodOptions();
        $this->data['coaClass'] = $this->Acc_coa->coaOptionDropDown(null, "CLS", null);

		// for showing data
		if($this->input->get("showAction")){
			$conds['id'] = $this->input->get("id");
			$defaultInstance = $this->Acc_ledger->get_one_by($conds);
			if (!$defaultInstance && !$defaultInstance->id) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$condss['id'] = $defaultInstance->acc_period_id;
			$accperiod = $this->Acc_period->get_one_by($condss);

            $condsss['id'] = $defaultInstance->acc_coa_id;
			$acccoa = $this->Acc_coa->get_one_by($condsss);
            $iscurrent="".get_msg('Upcoming')."";
			if($defaultInstance->is_current==1){
				$iscurrent="".get_msg('OnGoing')."";
			}else if($defaultInstance->is_current=='0'){
				$iscurrent="".get_msg('completed')."";
			}
			$data = array('isError' => false, 'defaultInstance' =>  $defaultInstance, 'accPeriod'=>$accperiod->caption, 'accCoa'=>$acccoa->caption, 'isCurrent'=>$iscurrent);    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		
		
		
		$this->load_template('ledger/list', $this->data, true );
	}

	public function ledger_list()
	{

		$iDisplayStart = $this->input->get("iDisplayStart")? intval($this->input->get("iDisplayStart")):0;
		$iDisplayLength = $this->input->get("iDisplayLength")? intval($this->input->get("iDisplayLength")) :10;
		$sSortDir = $this->input->get("sSortDir_0");
		$iSortingCol = $this->input->get("iSortCol_0")? $this->input->get("iSortCol_0") : null;
		$sSearch = $this->input->get("sSearch") ? $this->input->get("sSearch") : null;
		if($sSearch){
			$sSearch = "% ".$sSearch." %";
		}
		
		$dataReturns = array();
	
        $langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
        $sqlWhere = "WHERE coa_local.language_enum ='".$langEnumKey."'";
        $sqlWhere .= $this->input->get("acc_coa") ? (" AND  ledger.acc_coa_id=" .$this->input->get("acc_coa")) : '';
        $sqlWhere .= $this->input->get("acc_period") ? (" AND period.id=" . $this->input->get("acc_period")) : '';
        $sqlWhere .= $this->input->get("coa_class") ? (" AND coa_grp.acc_coa_id=" .$this->input->get("coa_class")) : '';
        $sqlWhere .= $this->input->get("sSearch") ? " AND (coa_local.caption LIKE '%".$this->input->get("sSearch")."%' OR ledger.code LIKE '%".$this->input->get("sSearch")."%' OR coa.code LIKE '%".$this->input->get("sSearch")."%')" : "";

		$querySQL = "SELECT
        ledger.id,
        ledger.code,
        coa.code coa_code,
        coa_local.language_enum,
        coa_local.caption AS coa_caption,
        period.period_start_dt AS period_start_dt,
        period.period_close_dt AS period_close_dt,
        ledger.is_current,
        ledger.opening_dr,
        ledger.opening_cr,
        ledger.closing_dr,
        ledger.closing_cr,
        ledger.current_cr,
        ledger.current_dr
    FROM acc_ledger ledger
        LEFT JOIN acc_coa coa ON ledger.acc_coa_id = coa.id
        LEFT JOIN acc_coa coa_grp ON coa.acc_coa_id = coa_grp.id
        LEFT JOIN acc_coa_locale coa_local ON coa_local.coa_id = coa.id
        LEFT JOIN acc_period period ON period.id = ledger.acc_period_id ".$sqlWhere." ORDER BY coa.code";
        // LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart."";
//;
		$resultSet = $this->Acc_period->queryPrepare($querySQL);
		$totalCount =$resultSet->num_rows();

		$querySQL .= " LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart."";
		$resultSet = $this->Acc_period->queryPrepare($querySQL);


		// $resultSet = $this->Acc_period->queryPrepare($querySQL);
		// $totalCount = $resultSet->num_rows();
		$serial = 0;
			$total = 0;
			
		$actionList = array();
			$actionListConfirm = array();
			$actionList[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
			
		foreach ($resultSet->result() as $row)
		{
            $serial++;
            $iscurrent="".get_msg('Upcoming')."";
			if($row->is_current==1){
				$iscurrent="".get_msg('OnGoing')."";
			}else if($row->is_current=='0'){
				$iscurrent="".get_msg('completed')."";
			}
            $dataReturns[]=array('DT_RowId' => $row->id, 'accessibleUrl' =>  $actionList,'activeStatus' => get_msg( 'Active'), 
            0 => $serial, 
            1 => $row->code,
            2 => $row->period_start_dt." - ".$row->period_close_dt,
            3 => $row->coa_code." - ".$row->coa_caption,
            4 => $iscurrent,
            5 => $row->opening_dr, 
            6 => $row->opening_cr,
            7 => $row->closing_dr, 
            8 => $row->closing_cr,
            9 => $row->current_dr, 
            10 => $row->current_cr,
            11 => "");
		}
		
		$gridData=array();
		if ($totalCount == 0) {
			$gridData=array('iTotalRecords' => 0, 'iTotalDisplayRecords' =>0, 'aaData'=> array());
			echo json_encode($gridData);
			exit();
		}

		$gridData=array('iTotalRecords' => $totalCount, 'iTotalDisplayRecords' =>$totalCount, 'aaData'=> $dataReturns);
		echo json_encode($gridData);
		exit();
	}
	

}