<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
/**
 * Products Controller
 */
class Products extends BE_Controller {

    /**
     * Construt required variables
     */
    function __construct() {

        parent::__construct( MODULE_CONTROL, 'PRODUCTS' );
    }

    /**
     * List down the registered users
     */
    function index() {

        // no delete flag
        // no publish filter
        $conds['no_publish_filter'] = 1;

        $selected_shop_id = $this->session->userdata('selected_shop_id');
        $shop_id = $selected_shop_id['shop_id'];

        $conds['shop_id'] = $shop_id;
        // get rows count
        $this->data['rows_count'] = $this->Product->count_all_by( $conds );

        // get categories
        $this->data['products'] = $this->Product->get_all_by( $conds , $this->pag['per_page'], $this->uri->segment( 4 ) );

        $this->data['selected_shop_id'] = $shop_id;

        // load index logic
        parent::index();
    }


    /**
     * Searches for the first match.
     */
    function search() {


        // breadcrumb urls
        $this->data['action_title'] = get_msg( 'prd_search' );

        // condition with search term
        if($this->input->post('submit') != NULL ){

            $conds = array( 'searchterm' => $this->searchterm_handler( $this->input->post( 'searchterm' )));

            if($this->input->post('searchterm') != "") {
                $conds['searchterm'] = $this->input->post('searchterm');
                $this->data['searchterm'] = $this->input->post('searchterm');
                $this->session->set_userdata(array("searchterm" => $this->input->post('searchterm')));
            } else {

                $this->session->set_userdata(array("searchterm" => NULL));
            }

            if($this->input->post('shop_code') != "") {
                $conds['shop_code'] = $this->input->post('shop_code');
                $this->data['shop_code'] = $this->input->post('shop_code');
                $this->session->set_userdata(array("shop_code" => $this->input->post('shop_code')));
            } else {

                $this->session->set_userdata(array("shop_code" => NULL));
            }
            // barcode
            if($this->input->post('code') != "") {
                $conds['code'] = $this->input->post('code');
                $this->data['code'] = $this->input->post('code');
                $this->session->set_userdata(array("code" => $this->input->post('code')));
            } else {
                $this->session->set_userdata(array("code" => NULL));
            }

            if($this->input->post('is_available') == "is_available") {
                $conds['is_available'] = '1';
                $this->data['is_available'] = '1';
                $this->session->set_userdata(array("is_available" => '1'));
            } else if($this->input->post('is_not_available') == "is_not_available") {
                $conds['is_available'] = '0';
                $this->data['is_not_available'] = '1';
                $this->session->set_userdata(array("is_not_available" => '1'));
            } else {
                $this->session->set_userdata(array("is_available" => NULL));
                $this->session->set_userdata(array("is_not_available" => NULL));
            }


            if($this->input->post('is_publish') == "is_publish") {
                $conds['status'] = '1';
                $this->data['is_publish'] = '1';
                $this->session->set_userdata(array("is_publish" => '1'));
                $this->session->set_userdata(array("is_unpublish" => NULL));
            } else if($this->input->post('is_unpublish') == "is_unpublish") {
                $conds['status'] = '0';
                $this->data['is_unpublish'] = '1';
                $this->session->set_userdata(array("is_unpublish" => '1'));
                $this->session->set_userdata(array("is_publish" => NULL));
            } else {
                $this->session->set_userdata(array("is_publish" => NULL));
                $this->session->set_userdata(array("is_unpublish" => NULL));
            }

            if($this->input->post('is_featured') == "is_featured") {
                $conds['is_featured'] = '1';
                $this->data['is_featured'] = '1';
                $this->session->set_userdata(array("is_featured" => '1'));
            } else {

                $this->session->set_userdata(array("is_featured" => NULL));
            }

            if($this->input->post('is_discount') == "is_discount") {
                $conds['is_discount'] = '1';
                $this->data['is_discount'] = '1';
                $this->session->set_userdata(array("is_discount" => '1'));
            } else {

                $this->session->set_userdata(array("is_discount" => NULL));
            }

            if($this->input->post('cat_id') != ""  || $this->input->post('cat_id') != '0') {
                $conds['cat_id'] = $this->input->post('cat_id');
                $this->data['cat_id'] = $this->input->post('cat_id');
                $this->data['selected_cat_id'] = $this->input->post('cat_id');
                $this->session->set_userdata(array("cat_id" => $this->input->post('cat_id')));
                $this->session->set_userdata(array("selected_cat_id" => $this->input->post('cat_id')));
            } else {
                $this->session->set_userdata(array("cat_id" => NULL ));
            }

            if($this->input->post('sub_cat_id') != ""  || $this->input->post('sub_cat_id') != '0') {
                $conds['sub_cat_id'] = $this->input->post('sub_cat_id');
                $this->data['sub_cat_id'] = $this->input->post('sub_cat_id');
                $this->session->set_userdata(array("sub_cat_id" => $this->input->post('sub_cat_id')));
            } else {
                $this->session->set_userdata(array("sub_cat_id" => NULL ));
            }

            if($this->input->post('price_min') != "") {
                $conds['price_min'] = $this->input->post('price_min');
                $this->data['price_min'] = $this->input->post('price_min');
                $this->session->set_userdata(array("price_min" => $this->input->post('price_min')));
            } else {
                $this->session->set_userdata(array("price_min" => NULL ));
            }

            if($this->input->post('price_max') != "") {
                $conds['price_max'] = $this->input->post('price_max');
                $this->data['price_max'] = $this->input->post('price_max');
                $this->session->set_userdata(array("price_max" => $this->input->post('price_max')));
            } else {
                $this->session->set_userdata(array("price_max" => NULL ));
            }

            //Order By

            $conds['no_publish_filter'] = '1';
            $conds['order_by'] = '1';

            if($this->input->post('order_by') == "name_asc") {

                $conds['order_by_field'] = "name";
                $conds['order_by_type'] = "asc";

                $this->data['order_by'] = $this->input->post('order_by');
                $this->session->set_userdata(array("order_by" => $this->input->post('order_by')));

            }

            if($this->input->post('order_by') == "name_desc") {

                $conds['order_by_field'] = "name";
                $conds['order_by_type'] = "desc";

                $this->data['order_by'] = $this->input->post('order_by');
                $this->session->set_userdata(array("order_by" => $this->input->post('order_by')));


            }

            if($this->input->post('order_by') == "price_asc") {

                $conds['order_by_field'] = "unit_price";
                $conds['order_by_type'] = "asc";

                $this->data['order_by'] = $this->input->post('order_by');
                $this->session->set_userdata(array("order_by" => $this->input->post('order_by')));

            }

            if($this->input->post('order_by') == "price_desc") {

                $conds['order_by_field'] = "unit_price";
                $conds['order_by_type'] = "desc";

                $this->data['order_by'] = $this->input->post('order_by');
                $this->session->set_userdata(array("order_by" => $this->input->post('order_by')));

            }

            if($this->input->post('order_by') == "barcode_asc") {

                $conds['order_by_field'] = "code";
                $conds['order_by_type'] = "asc";

                $this->data['order_by'] = $this->input->post('order_by');
                $this->session->set_userdata(array("order_by" => $this->input->post('order_by')));

            }

            if($this->input->post('order_by') == "barcode_desc") {

                $conds['order_by_field'] = "code";
                $conds['order_by_type'] = "desc";

                $this->data['order_by'] = $this->input->post('order_by');
                $this->session->set_userdata(array("order_by" => $this->input->post('order_by')));

            }

            if($this->input->post('order_by') == "color_asc") {

                $conds['order_by_field'] = "is_has_color";
                $conds['order_by_type'] = "asc";

                $this->data['order_by'] = $this->input->post('order_by');
                $this->session->set_userdata(array("order_by" => $this->input->post('order_by')));

            }

            if($this->input->post('order_by') == "color_desc") {

                $conds['order_by_field'] = "is_has_color";
                $conds['order_by_type'] = "desc";

                $this->data['order_by'] = $this->input->post('order_by');
                $this->session->set_userdata(array("order_by" => $this->input->post('order_by')));

            }

        } else {
            //read from session value
            if($this->session->userdata('searchterm') != NULL){
                $conds['searchterm'] = $this->session->userdata('searchterm');
                $this->data['searchterm'] = $this->session->userdata('searchterm');
            }

            if($this->session->userdata('shop_code') != NULL){
                $conds['shop_code'] = $this->session->userdata('shop_code');
                $this->data['shop_code'] = $this->session->userdata('shop_code');
            }

            if($this->session->userdata('code') != NULL){
                $conds['code'] = $this->session->userdata('code');
                $this->data['code'] = $this->session->userdata('code');
            }

            if($this->session->userdata('is_available') != NULL){
                $conds['is_available'] = $this->session->userdata('is_available');
                $this->data['is_available'] = $this->session->userdata('is_available');
            }

            if($this->session->userdata('is_not_available') != NULL){
                $conds['is_available'] = $this->session->userdata('is_not_available');
                $this->data['is_not_available'] = $this->session->userdata('is_not_available');
            }

            if($this->session->userdata('is_publish') != NULL){
                $conds['status'] = 1;
                $this->data['is_publish'] = 1;
            }

            if($this->session->userdata('is_unpublish') != NULL){
                $conds['status'] = 1;
                $this->data['is_unpublish'] = 1;
            }
            if($this->session->userdata('is_featured') != NULL){
                $conds['is_featured'] = $this->session->userdata('is_featured');
                $this->data['is_featured'] = $this->session->userdata('is_featured');
            }

            if($this->session->userdata('is_discount') != NULL){
                $conds['is_discount'] = $this->session->userdata('is_discount');
                $this->data['is_discount'] = $this->session->userdata('is_discount');
            }

            if($this->session->userdata('cat_id') != NULL){
                $conds['cat_id'] = $this->session->userdata('cat_id');
                $this->data['cat_id'] = $this->session->userdata('cat_id');
                $this->data['selected_cat_id'] = $this->session->userdata('cat_id');
            }

            if($this->session->userdata('sub_cat_id') != NULL){
                $conds['sub_cat_id'] = $this->session->userdata('sub_cat_id');
                $this->data['sub_cat_id'] = $this->session->userdata('sub_cat_id');
                $this->data['selected_cat_id'] = $this->session->userdata('cat_id');
            }


            if($this->session->userdata('price_min') != NULL){
                $conds['price_min'] = $this->session->userdata('price_min');
                $this->data['price_min'] = $this->session->userdata('price_min');
            }

            if($this->session->userdata('price_max') != NULL){
                $conds['price_max'] = $this->session->userdata('price_max');
                $this->data['price_max'] = $this->session->userdata('price_max');
            }


            //Order By
            $conds['no_publish_filter'] = 1;
            $conds['order_by'] = 1;

            if($this->session->userdata('order_by') != NULL){

                if($this->session->userdata('order_by') == "name_asc") {

                    $conds['order_by_field'] = "name";
                    $conds['order_by_type'] = "asc";

                    $this->data['order_by'] = $this->session->userdata('order_by');;


                }

                if($this->session->userdata('order_by') == "name_desc") {

                    $conds['order_by_field'] = "name";
                    $conds['order_by_type'] = "desc";

                    $this->data['order_by'] = $this->session->userdata('order_by');;

                }


                if($this->session->userdata('order_by')  == "price_asc") {

                    $conds['order_by_field'] = "unit_price";
                    $conds['order_by_type'] = "asc";

                    $this->data['order_by'] = $this->session->userdata('order_by');;

                }

                if($this->session->userdata('order_by') == "price_desc") {

                    $conds['order_by_field'] = "unit_price";
                    $conds['order_by_type'] = "desc";

                    $this->data['order_by'] = $this->session->userdata('order_by');;

                }

                if($this->session->userdata('order_by')  == "barcode_asc") {

                    $conds['order_by_field'] = "code";
                    $conds['order_by_type'] = "asc";

                    $this->data['order_by'] = $this->session->userdata('order_by');;

                }

                if($this->session->userdata('order_by') == "barcode_desc") {

                    $conds['order_by_field'] = "code";
                    $conds['order_by_type'] = "desc";

                    $this->data['order_by'] = $this->session->userdata('order_by');;

                }

                if($this->session->userdata('order_by')  == "color_asc") {

                    $conds['order_by_field'] = "is_has_color";
                    $conds['order_by_type'] = "asc";

                    $this->data['order_by'] = $this->session->userdata('order_by');;

                }

                if($this->session->userdata('order_by') == "color_desc") {

                    $conds['order_by_field'] = "is_has_color";
                    $conds['order_by_type'] = "desc";

                    $this->data['order_by'] = $this->session->userdata('order_by');;

                }

            }


        }

        if ($conds['order_by_field'] == "" ){
            $conds['order_by_field'] = "added_date";
            $conds['order_by_type'] = "desc";
        }

        $selected_shop_id = $this->session->userdata('selected_shop_id');
        $shop_id = $selected_shop_id['shop_id'];

        $conds['shop_id'] = $shop_id;

        // pagination
        $this->data['rows_count'] = $this->Product->count_all_by( $conds );

        // search data
        $this->data['products'] = $this->Product->get_all_by( $conds, $this->pag['per_page'], $this->uri->segment( 4 ) );

        $this->data['selected_shop_id'] = $shop_id;
        //$this->data['selected_cat_id'] = $this->input->post( 'cat_id' );


        // load add list
        parent::search();
    }

    function downloadexcel()
    {
        if($this->session->userdata('searchterm') != NULL){
            $conds['searchterm'] = $this->session->userdata('searchterm');
            $this->data['searchterm'] = $this->session->userdata('searchterm');
        }

        if($this->session->userdata('shop_code') != NULL){
            $conds['shop_code'] = $this->session->userdata('shop_code');
            $this->data['shop_code'] = $this->session->userdata('shop_code');
        }

        if($this->session->userdata('code') != NULL){
            $conds['code'] = $this->session->userdata('code');
            $this->data['code'] = $this->session->userdata('code');
        }

        if($this->session->userdata('is_available') != NULL){
            $conds['is_available'] = $this->session->userdata('is_available');
            $this->data['is_available'] = $this->session->userdata('is_available');
        }

        if($this->session->userdata('is_not_available') != NULL){
            $conds['is_available'] = $this->session->userdata('is_not_available');
            $this->data['is_not_available'] = $this->session->userdata('is_not_available');
        }

        if($this->session->userdata('is_featured') != NULL){
            $conds['is_featured'] = $this->session->userdata('is_featured');
            $this->data['is_featured'] = $this->session->userdata('is_featured');
        }

        if($this->session->userdata('is_discount') != NULL){
            $conds['is_discount'] = $this->session->userdata('is_discount');
            $this->data['is_discount'] = $this->session->userdata('is_discount');
        }

        if($this->session->userdata('cat_id') != NULL){
            $conds['cat_id'] = $this->session->userdata('cat_id');
            $this->data['cat_id'] = $this->session->userdata('cat_id');
            $this->data['selected_cat_id'] = $this->session->userdata('cat_id');
        }

        if($this->session->userdata('sub_cat_id') != NULL){
            $conds['sub_cat_id'] = $this->session->userdata('sub_cat_id');
            $this->data['sub_cat_id'] = $this->session->userdata('sub_cat_id');
            $this->data['selected_cat_id'] = $this->session->userdata('cat_id');
        }


        if($this->session->userdata('price_min') != NULL){
            $conds['price_min'] = $this->session->userdata('price_min');
            $this->data['price_min'] = $this->session->userdata('price_min');
        }

        if($this->session->userdata('price_max') != NULL){
            $conds['price_max'] = $this->session->userdata('price_max');
            $this->data['price_max'] = $this->session->userdata('price_max');
        }


        //Order By
        $conds['no_publish_filter'] = 1;
        $conds['order_by'] = 1;

        if($this->session->userdata('order_by') != NULL){

            if($this->session->userdata('order_by') == "name_asc") {

                $conds['order_by_field'] = "name";
                $conds['order_by_type'] = "asc";

                $this->data['order_by'] = $this->session->userdata('order_by');;


            }

            if($this->session->userdata('order_by') == "name_desc") {

                $conds['order_by_field'] = "name";
                $conds['order_by_type'] = "desc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }


            if($this->session->userdata('order_by')  == "price_asc") {

                $conds['order_by_field'] = "unit_price";
                $conds['order_by_type'] = "asc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }

            if($this->session->userdata('order_by') == "price_desc") {

                $conds['order_by_field'] = "unit_price";
                $conds['order_by_type'] = "desc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }

            if($this->session->userdata('order_by')  == "barcode_asc") {

                $conds['order_by_field'] = "code";
                $conds['order_by_type'] = "asc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }

            if($this->session->userdata('order_by') == "barcode_desc") {

                $conds['order_by_field'] = "code";
                $conds['order_by_type'] = "desc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }

            if($this->session->userdata('order_by')  == "color_asc") {

                $conds['order_by_field'] = "is_has_color";
                $conds['order_by_type'] = "asc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }

            if($this->session->userdata('order_by') == "color_desc") {

                $conds['order_by_field'] = "is_has_color";
                $conds['order_by_type'] = "desc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }

        }

        if ($conds['order_by_field'] == "" ){
            $conds['order_by_field'] = "added_date";
            $conds['order_by_type'] = "desc";
        }

        $selected_shop_id = $this->session->userdata('selected_shop_id');
        $shop_id = $selected_shop_id['shop_id'];

        $conds['shop_id'] = $shop_id;

        // search data
        $products = $this->Product->get_all_by( $conds, 3000, $this->uri->segment( 4 ) );

        $spreadsheet = new Spreadsheet(); // instantiate Spreadsheet

        $sheet = $spreadsheet->getActiveSheet();

        // manually set table data value
        $sheet->setCellValue('A1', get_msg('no'));
        $sheet->setCellValue('B1', get_msg('product_image'));
        $sheet->setCellValue('C1', get_msg('name'));
        $sheet->setCellValue('D1', get_msg('name_alt'));
        $sheet->setCellValue('E1', get_msg('shop_code'));
        $sheet->setCellValue('F1', get_msg('barcode'));
        $sheet->setCellValue('G1', get_msg('cat_name'));
        $sheet->setCellValue('H1', get_msg('unit_price'));
        $sheet->setCellValue('I1', get_msg('original_price'));
        $sheet->setCellValue('J1', get_msg('is_available'));
        $sheet->setCellValue('K1', get_msg('is_publish'));
        $sheet->setCellValue('L1', get_msg('database_id'));

        $count = $this->uri->segment(4) or $count = 0;

        if ( !empty( $products ) && count( $products->result()) > 0 ){

            $rowcount=2;
            foreach($products->result() as $product){
                $sheet->setCellValue('A'.$rowcount.'', ++$count);
                if($product->thumbnail !=null) {
                    $sheet->setCellValue('B'.$rowcount.'', "".site_url( '/')."uploads/thumbnail/".$product->thumbnail."");
                }

                $sheet->setCellValue('C'.$rowcount.'', $product->name);
                $sheet->setCellValue('D'.$rowcount.'', $product->name_alt);
                $sheet->setCellValue('E'.$rowcount.'', $product->shop_code);
                $sheet->setCellValue('F'.$rowcount.'', $product->code);
                $sheet->setCellValue('G'.$rowcount.'', $this->Category->get_one( $product->cat_id )->name);
                $unit_price = $product->unit_price;
                $unit_price = round($unit_price, 2);
                $sheet->setCellValue('H'.$rowcount.'', $unit_price);
                $original_price = $product->original_price;
                $original_price = round($original_price, 2);
                $sheet->setCellValue('I'.$rowcount.'', $original_price);
                $sheet->setCellValue('J'.$rowcount.'', $product->is_available);
                $sheet->setCellValue('K'.$rowcount.'', $product->status);
                $sheet->setCellValue('L'.$rowcount.'', $product->id);
                $rowcount++;

            }
        }
        $writer = new Xlsx($spreadsheet); // instantiate Xlsx

        $filename = 'list-of-products'; // set filename for excel file to be exported

        header('Content-Type: application/vnd.ms-excel'); // generate excel file
        header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output'); // download file
    }
    function downloadproductpdf()
    {
        if($this->session->userdata('searchterm') != NULL){
            $conds['searchterm'] = $this->session->userdata('searchterm');
            $this->data['searchterm'] = $this->session->userdata('searchterm');
        }

        if($this->session->userdata('shop_code') != NULL){
            $conds['shop_code'] = $this->session->userdata('shop_code');
            $this->data['shop_code'] = $this->session->userdata('shop_code');
        }

        if($this->session->userdata('code') != NULL){
            $conds['code'] = $this->session->userdata('code');
            $this->data['code'] = $this->session->userdata('code');
        }

        if($this->session->userdata('is_available') != NULL){
            $conds['is_available'] = $this->session->userdata('is_available');
            $this->data['is_available'] = $this->session->userdata('is_available');
        }

        if($this->session->userdata('is_not_available') != NULL){
            $conds['is_available'] = $this->session->userdata('is_not_available');
            $this->data['is_not_available'] = $this->session->userdata('is_not_available');
        }

        if($this->session->userdata('is_featured') != NULL){
            $conds['is_featured'] = $this->session->userdata('is_featured');
            $this->data['is_featured'] = $this->session->userdata('is_featured');
        }

        if($this->session->userdata('is_discount') != NULL){
            $conds['is_discount'] = $this->session->userdata('is_discount');
            $this->data['is_discount'] = $this->session->userdata('is_discount');
        }

        if($this->session->userdata('cat_id') != NULL){
            $conds['cat_id'] = $this->session->userdata('cat_id');
            $this->data['cat_id'] = $this->session->userdata('cat_id');
            $this->data['selected_cat_id'] = $this->session->userdata('cat_id');
        }

        if($this->session->userdata('sub_cat_id') != NULL){
            $conds['sub_cat_id'] = $this->session->userdata('sub_cat_id');
            $this->data['sub_cat_id'] = $this->session->userdata('sub_cat_id');
            $this->data['selected_cat_id'] = $this->session->userdata('cat_id');
        }


        if($this->session->userdata('price_min') != NULL){
            $conds['price_min'] = $this->session->userdata('price_min');
            $this->data['price_min'] = $this->session->userdata('price_min');
        }

        if($this->session->userdata('price_max') != NULL){
            $conds['price_max'] = $this->session->userdata('price_max');
            $this->data['price_max'] = $this->session->userdata('price_max');
        }


        //Order By
        $conds['no_publish_filter'] = 1;
        $conds['order_by'] = 1;

        if($this->session->userdata('order_by') != NULL){

            if($this->session->userdata('order_by') == "name_asc") {

                $conds['order_by_field'] = "name";
                $conds['order_by_type'] = "asc";

                $this->data['order_by'] = $this->session->userdata('order_by');;


            }

            if($this->session->userdata('order_by') == "name_desc") {

                $conds['order_by_field'] = "name";
                $conds['order_by_type'] = "desc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }


            if($this->session->userdata('order_by')  == "price_asc") {

                $conds['order_by_field'] = "unit_price";
                $conds['order_by_type'] = "asc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }

            if($this->session->userdata('order_by') == "price_desc") {

                $conds['order_by_field'] = "unit_price";
                $conds['order_by_type'] = "desc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }

            if($this->session->userdata('order_by')  == "barcode_asc") {

                $conds['order_by_field'] = "code";
                $conds['order_by_type'] = "asc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }

            if($this->session->userdata('order_by') == "barcode_desc") {

                $conds['order_by_field'] = "code";
                $conds['order_by_type'] = "desc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }

            if($this->session->userdata('order_by')  == "color_asc") {

                $conds['order_by_field'] = "is_has_color";
                $conds['order_by_type'] = "asc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }

            if($this->session->userdata('order_by') == "color_desc") {

                $conds['order_by_field'] = "is_has_color";
                $conds['order_by_type'] = "desc";

                $this->data['order_by'] = $this->session->userdata('order_by');;

            }

        }

        if ($conds['order_by_field'] == "" ){
            $conds['order_by_field'] = "added_date";
            $conds['order_by_type'] = "desc";
        }

        $selected_shop_id = $this->session->userdata('selected_shop_id');
        $shop_id = $selected_shop_id['shop_id'];

        $conds['shop_id'] = $shop_id;
        $conds['order_by']=1;

        $conds['order_by_field']='cat_id';
        $conds['order_by_type']='asc';

        // search data
        $products = $this->Product->get_all_by( $conds, 3000, $this->uri->segment( 4 ) );


        $this->data['products'] = $products;
        //$this->load_template('orders/detail', $this->data, true );
        $html = $this->load->view('backend/products/downloadproductpdf',$this->data, true);

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8']);
        if($this->session->userdata('language_code')=="ar"){
            $mpdf->SetDirectionality('rtl');
        }
        //$mpdf = new \Mpdf\Mpdf();
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;
        $mpdf->WriteHTML($html);
        $mpdf->setFooter('{PAGENO} / {nb}');
        $mpdf->Output('products.pdf', 'I');


    }
    function syncronyzeapi()
    {
        header("Content-type: text/xml");
        //header('Content-Type: application/json');
        $selected_shop_id = $this->session->userdata('selected_shop_id');
        $shop_id = $selected_shop_id['shop_id'];

        $conds['shop_id'] = $shop_id;
        $client = new SoapClient("http://134.119.218.178/getitems.asmx?WSDL", array('trace' => true));
        $result=$client->GetAll();

        echo "<pre>";
        var_dump($result->GetAllResult->any);
        echo "</pre>";



// foreach($root->childNodes as $node){
//    // print $node->nodeName.' - '.$node->nodeValue;
//      echo "<pre>";
//  var_dump($node);
//  //print_r($result->GetAllResult->diffgr->DocumentElement->ITEMS[0]);
//  echo "</pre>";
//  break;
// }



        //    var_dump($client->__getFunctions());
        //    var_dump($client->__getTypes());

// 	    $client = new SoapClient('http://134.119.218.178/getitems.asmx', [
//     # This array and its values are optional
//     'soap_version' => SOAP_1_2,
//     'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
//     'cache_wsdl' => WSDL_CACHE_BOTH,
//     # Helps with debugging
//     'trace' => TRUE,
//     'exceptions' => TRUE
// ]);
        // $result = $client->requestData(['a', 'b', 'c']);
        // print_r($result);
        // array(4) { [0]=> string(41) "GetAllResponse  (GetAll $parameters)" [1]=> string(38) "GetIDResponse GetID(GetID $parameters)" [2]=> string(41) "GetAllResponse GetAll(GetAll $parameters)" [3]=> string(38) "GetIDResponse GetID(GetID $parameters)" } array(5) { [0]=> string(36) "struct GetAll { string xfromdate; }" [1]=> string(53) "struct GetAllResponse { GetAllResult GetAllResult; }" [2]=> string(53) "struct GetAllResult { any; any; }" [3]=> string(32) "struct GetID { string xitmid; }" [4]=> string(45) "struct GetIDResponse { string GetIDResult; }" }

        // var_dump($client->GetAll());

//920011541
// 		 $dom = new DOMDocument($result->GetAllResult);
// $dom->loadXML($result->GetAllResult);
// var_dump($dom->documentElement);
// $xml = file_get_contents($result->GetAllResult->any);
//$xml = mb_convert_encoding($result->GetAllResult->any, 'HTML-ENTITIES', "UTF-8");

// $dom = new DOMDocument();
// $dom->loadXML($result->GetAllResult->any);
//var_dump($result->GetAllResult->any);
// 		 $xml = new DOMDocument();
// $xml->load($result->GetAllResult->any);
// $root = $xml->childNodes;


        //print_r($result->GetAllResult->diffgr->DocumentElement->ITEMS[0]);
        //echo "</pre>";
// foreach($root->childNodes as $node){
//    // print $node->nodeName.' - '.$node->nodeValue;
//      echo "<pre>";
//  var_dump($node);
//  //print_r($result->GetAllResult->diffgr->DocumentElement->ITEMS[0]);
//  echo "</pre>";
//  break;
// }
        // echo "<pre>";
        // var_dump($result->GetAllResult->any);
        // //print_r($result->GetAllResult->diffgr->DocumentElement->ITEMS[0]);

        // foreach ($result->GetAllResult as $key => $value) {

        // }
    }
    /**
     * Create new one
     */
    function add() {


        $selected_shop_id = $this->session->userdata('selected_shop_id');
        $shop_id = $selected_shop_id['shop_id'];

        // breadcrumb urls
        $this->data['action_title'] = get_msg( 'prd_add' );

        $this->data['selected_shop_id'] = $shop_id;

//noon
        if($this->input->get( 'website_url' )){
            $base = $this->input->get( 'website_url' );
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_URL, $base);
            curl_setopt($curl, CURLOPT_REFERER, $base);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $str = curl_exec($curl);
            curl_close($curl);
            include('application/libraries/simplehtmldom/simple_html_dom.php');
            // Create a DOM object
            $html_base = new simple_html_dom();
            // Load HTML from a string
            $html=$html_base->load($str);

            //get all category links
            // foreach($html_base->find('a') as $element) {
            //     echo "<pre>";
            //     print_r( $element->href );
            //     echo "</pre>";
            // }


            // include('application/libraries/simplehtmldom/simple_html_dom.php');

            // $html = file_get_html($this->input->get( 'website_url' ));

            $title = ($html->find('h1.jsx-2771165322', 0)) ? $html->find('h1.jsx-2771165322', 0)->plaintext : '';
            // $price = ($html->find('span.price-wrapper', 0)) ? $html->find('span.price-wrapper', 0)->attr['data-price-amount'] : '';
            //$description = ($html->find('ul.highlights', 0)) ? $html->find('ul.highlights', 0)->plaintext : '';
            $description="";
            foreach($html_base->find('ul.highlights li') as $element) {
                $description .=$element->plaintext."\n";
            }
            // echo "<pre>";
            // print_r($this->input->get( 'website_url' ));
            // echo "</pre>";
            // die();
            $product_arr=$this->Product->get_one("abcbbbc");
            $product_arr->name=$title;
            $product_arr->name_alt=$title;
            $product_arr->search_tag=$title;
            // $product_arr->original_price=round($price, 2);
            //$product_arr->unit_price=round($price, 2);
            // $description=str_replace("Show more","",$description);
            $description=str_replace("- ","\n- ",$description);
            // $description=str_replace("• ","• ",$description);
            $description=str_replace("</br>","",$description);
            $description=trim($description);
            $ii=1;
            // foreach($html->find('.specifications li') as $element){
            // 	if($ii==1){
            // 		$description=$description."\n Specification: \n";
            // 	}
            // 	$label=$element->find('div.eiVHfJ', 0)->plaintext;
            // 	$label_val=$element->find('div.text-gray-800', 0)->plaintext;
            // 	$description=$description."\n- ".$label."".$label_val;
            // 	$ii++;
            // }
            $product_arr->description=$description;
            $this->data['product'] = $product_arr;
            $this->data['website_url'] = $this->input->get( 'website_url' );
            $html_base->clear();
            unset($html_base);
        }

        // Nahdionline
//		if($this->input->get( 'website_url' )){
//			$base = $this->input->get( 'website_url' );
//			$curl = curl_init();
//			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
//			curl_setopt($curl, CURLOPT_HEADER, false);
//			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
//			curl_setopt($curl, CURLOPT_URL, $base);
//			curl_setopt($curl, CURLOPT_REFERER, $base);
//			curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
//			$str = curl_exec($curl);
//			curl_close($curl);
//			include('application/libraries/simplehtmldom/simple_html_dom.php');
//			// Create a DOM object
//			$html_base = new simple_html_dom();
//			// Load HTML from a string
//			$html=$html_base->load($str);
//
//			//get all category links
//			// foreach($html_base->find('a') as $element) {
//			//     echo "<pre>";
//			//     print_r( $element->href );
//			//     echo "</pre>";
//			// }
//
//
//		// include('application/libraries/simplehtmldom/simple_html_dom.php');
//
//		// $html = file_get_html($this->input->get( 'website_url' ));
//
//		$title = ($html->find('h1.page-title span.base', 0)) ? $html->find('h1.page-title span.base', 0)->plaintext : '';
//		$price = ($html->find('span.price-wrapper', 0)) ? $html->find('span.price-wrapper', 0)->attr['data-price-amount'] : '';
//		$description = ($html->find('div.description', 0)) ? $html->find('div.description div.value', 0)->plaintext : '';
//			// echo "<pre>";
//			// print_r($this->input->get( 'website_url' ));
//			// echo "</pre>";
//			// die();
//			$product_arr=$this->Product->get_one("abcbbbc");
//			$product_arr->name=$title;
//			$product_arr->name_alt=$title;
//			$product_arr->search_tag=$title;
//			$product_arr->original_price=round($price, 2);
//			$product_arr->unit_price=round($price, 2);
//			// $description=str_replace("Show more","",$description);
//			$description=str_replace("- ","\n- ",$description);
//			// $description=str_replace("• ","• ",$description);
//			$description=str_replace("</br>","",$description);
//			$description=trim($description);
//			$ii=1;
//			// foreach($html->find('.specifications li') as $element){
//			// 	if($ii==1){
//			// 		$description=$description."\n Specification: \n";
//			// 	}
//			// 	$label=$element->find('div.eiVHfJ', 0)->plaintext;
//			// 	$label_val=$element->find('div.text-gray-800', 0)->plaintext;
//			// 	$description=$description."\n- ".$label."".$label_val;
//			// 	$ii++;
//			// }
//			$product_arr->description=$description;
//			$this->data['product'] = $product_arr;
//			$this->data['website_url'] = $this->input->get( 'website_url' );
//			$html_base->clear();
//			unset($html_base);
//		}

        // call the core add logic
        parent::add();


    }

    function downloadpicture() {
        $selected_shop_id = $this->session->userdata('selected_shop_id');
        $shop_id = $selected_shop_id['shop_id'];

        // breadcrumb urls
        $this->data['action_title'] = get_msg( 'prd_add' );

        $this->data['selected_shop_id'] = $shop_id;
        //noon
        if($this->input->get( 'website_url' )){
            include('application/libraries/simplehtmldom/simple_html_dom.php');
            $base = $this->input->get( 'website_url' );
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_URL, $base);
            curl_setopt($curl, CURLOPT_REFERER, $base);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $str = curl_exec($curl);
            curl_close($curl);
            // Create a DOM object
            $html_base = new simple_html_dom();
            // Load HTML from a string
            $html=$html_base->load($str);
            //$html = file_get_html($this->input->get( 'website_url' ));

            $title = rand();

            $this->data['website_url'] = $this->input->get( 'website_url' );
            $url = $html->find('div.jsx-2714670158 div.jsx-180529803 img', 0)->src;
            $path_info = pathinfo($url);
            $extension=$path_info['extension'];
            $title=trim($title);
            if(!$title){
                $title=rand();
            }
            $fileName = preg_replace('/\s+/', '_', $title);
            $img = 'files/'.$fileName.'.'.$extension;
            // die();
            // Function to write image into file
            file_put_contents($img, file_get_contents($url));

            if(file_exists($img))
            {
                header('Content-Type: image/jpeg');
                header("Content-Transfer-Encoding: Binary");
                header("Content-disposition: attachment; filename=\"" . basename($img) . "\"");
                readfile($img);
                unlink($img);
            }
        }
        // nahdionline
//		if($this->input->get( 'website_url' )){
//		include('application/libraries/simplehtmldom/simple_html_dom.php');
//		$base = $this->input->get( 'website_url' );
//		$curl = curl_init();
//		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
//		curl_setopt($curl, CURLOPT_HEADER, false);
//		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
//		curl_setopt($curl, CURLOPT_URL, $base);
//		curl_setopt($curl, CURLOPT_REFERER, $base);
//		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
//		$str = curl_exec($curl);
//		curl_close($curl);
//		// Create a DOM object
//		$html_base = new simple_html_dom();
//		// Load HTML from a string
//		$html=$html_base->load($str);
//		//$html = file_get_html($this->input->get( 'website_url' ));
//
//		$title = ($html->find('img.fotorama__img', 0)) ? $html->find('img.fotorama__img', 0)->attr['alt'] : '';
//
//		$this->data['website_url'] = $this->input->get( 'website_url' );
//	  $url = $html->find('img.fotorama__img', 0)->src;
//	  $path_info = pathinfo($url);
//	$extension=$path_info['extension'];
//	$title=trim($title);
//	if(!$title){
//		$title=rand();
//	}
//	$fileName = preg_replace('/\s+/', '_', $title);
//	 $img = 'files/'.$fileName.'.'.$extension;
//	 // die();
//	// Function to write image into file
//	file_put_contents($img, file_get_contents($url));
//
//        if(file_exists($img))
//        {
//            header('Content-Type: image/jpeg');
//		    header("Content-Transfer-Encoding: Binary");
//		    header("Content-disposition: attachment; filename=\"" . basename($img) . "\"");
//		    readfile($img);
//		    unlink($img);
//        }
//    }
        $html_base->clear();
        unset($html_base);
        //$this->save_image($url,'image.jpg');
        // call the core add logic
        parent::picturedownload();
//$this->load_template('products/downloadpicture', $this->data, true );

    }
// 	function save_image($inPath,$outPath)
// { //Download images from remote server
//     $in=    fopen($inPath, "rb");
//     $out=   fopen($outPath, "wb");
//     while ($chunk = fread($in,8192))
//     {
//         fwrite($out, $chunk, 8192);
//     }
//     fclose($in);
//     fclose($out);
// }
    /**
     * Saving Logic
     * 1) upload image
     * 2) save category
     * 3) save image
     * 4) check transaction status
     *
     * @param      boolean  $id  The user identifier
     */
    function save( $id = false ) {

        if((!isset($id ))|| (isset($id))) {
            // color count
            if($id) {
                $color_counter_total = $this->get_data( 'color_total' );
                if ($color_counter_total == "" || $color_counter_total== 0) {
                    $color_counter_total = $this->get_data( 'color_total_existing' );
                }
                $edit_prd_id = $id;
            } else {
                $color_counter_total = $this->get_data( 'color_total' );
            }

            // specification count
            if($id) {
                $spec_counter_total = $this->get_data( 'spec_total' );
                if ($spec_counter_total == "" || $spec_counter_total== 0) {
                    $spec_counter_total = $this->get_data( 'spec_total_existing' );
                }
                $edit_spec_id = $id;
            } else {
                $spec_counter_total = $this->get_data( 'spec_total' );
            }
            // start the transaction
            $this->db->trans_start();
            $logged_in_user = $this->ps_auth->get_user_info();

            /**
             * Insert Product Records
             */
            $data = array();
            $selected_shop_id = $this->session->userdata('selected_shop_id');

            // Product id
            if ( $this->has_data( 'id' )) {
                $data['id'] = $this->get_data( 'id' );

            }

            // Category id
            if ( $this->has_data( 'cat_id' )) {
                $data['cat_id'] = $this->get_data( 'cat_id' );
            }

            // Sub Category id
            if ( $this->has_data( 'sub_cat_id' )) {
                $data['sub_cat_id'] = $this->get_data( 'sub_cat_id' );
            }
            // brand id
            if ( $this->has_data( 'brand_id' )) {
                $data['brand_id'] = $this->get_data( 'brand_id' );
            }

            // Sub Category id
            if ( $this->has_data( 'discount_type_id' )) {
                $data['discount_type_id'] = $this->get_data( 'discount_type_id' );
            }

            // product_location_id id
            if ( $this->has_data( 'product_location_id' )) {
                $data['product_location_id'] = $this->get_data( 'product_location_id' );
            }

            // prepare product name
            if ( $this->has_data( 'name' )) {
                $data['name'] = $this->get_data( 'name' );
            }

            // prepare product description
            if ( $this->has_data( 'description' )) {
                $data['description'] = $this->get_data( 'description' );
            }

            if ( $this->has_data( 'name_alt' )) {
                $data['name_alt'] = $this->get_data( 'name_alt' );

            }
            // prepare product unit price
            if ( $this->has_data( 'unit_price' )) {
                $data['unit_price'] = $this->get_data( 'unit_price' );
            }

            // prepare product original price
            if ( $this->has_data( 'original_price' )) {
                $data['original_price'] = $this->get_data( 'original_price' );
                if($data['unit_price']==null || $data['unit_price']=="" || $data['unit_price']==0){
                    $data['unit_price']=$data['original_price'];
                }
            }

            // prepare product shipping cost
            if ( $this->has_data( 'shipping_cost' )) {
                $data['shipping_cost'] = $this->get_data( 'shipping_cost' );
            }

            // prepare product minimum order
            if ( $this->has_data( 'minimum_order' )) {
                $data['minimum_order'] = $this->get_data( 'minimum_order' );
            }

            // prepare product maximum order
            if ( $this->has_data( 'maximum_order' )) {
                $data['maximum_order'] = $this->get_data( 'maximum_order' );
            }

            // Product Measurement
            if ( $this->has_data( 'product_measurement' )) {
                $data['product_measurement'] = $this->get_data( 'product_measurement' );
            }

            // Product Unit
            if ( $this->has_data( 'product_unit' )) {
                $data['product_unit'] = $this->get_data( 'product_unit' );
            }


            // prepare product search tag
            if ( $this->has_data( 'search_tag' )) {
                $data['search_tag'] = $this->get_data( 'search_tag' );
            }

            // prepare product highlight information
            if ( $this->has_data( 'highlight_information' )) {
                $data['highlight_information'] = $this->get_data( 'highlight_information' );
            }

            // prepare product product code
            if ( $this->has_data( 'code' )) {
                $data['code'] = $this->get_data( 'code' );
            }

            // prepare product product code
            if ( $this->has_data( 'shop_code' )) {
                $data['shop_code'] = $this->get_data( 'shop_code' );
            }
            // prepare product product code
            if ( $this->has_data( 'commission_plan' )) {
                $data['commission_plan'] = $this->get_data( 'commission_plan' );
            }


            // if 'is featured' is checked,
            if ( $this->has_data( 'is_featured' )) {

                $data['is_featured'] = 1;
                if ($data['is_featured'] == 1) {

                    if($this->get_data( 'is_featured_stage' ) == $this->has_data( 'is_featured' )) {
                        $data['updated_date'] = date("Y-m-d H:i:s");
                    } else {
                        $data['featured_date'] = date("Y-m-d H:i:s");

                    }
                }
            } else {

                $data['is_featured'] = 0;
            }

            // if 'no_outside_city' is checked,
            if ( $this->has_data( 'no_outside_city' )) {
                $data['no_outside_city'] = 1;
            } else {
                $data['no_outside_city'] = 0;
            }

            // if 'eligible_free_ship' is checked,
            if ( $this->has_data( 'eligible_free_ship' )) {
                $data['eligible_free_ship'] = 1;
            } else {
                $data['eligible_free_ship'] = 0;
            }

            // if 'is available' is checked,
            if ( $this->has_data( 'is_available' ) && $this->has_data( 'unit_price' ) && $this->get_data( 'unit_price' )>0) {
                $data['is_available'] = 1;
            } else {
                $data['is_available'] = 0;
            }

            // if 'status' is checked,
            if ( $this->has_data( 'status' ) && $this->has_data( 'unit_price' ) && $this->get_data( 'unit_price' )>0) {
                $data['status'] = 1;
            } else {
                $data['status'] = 0;
            }

            if ( !$selected_shop_id['shop_id']) {

                // set flash error message
                $this->set_flash_msg( 'error', get_msg( 'shop_id_not_vailable' ));
                return;
            }
            if (!$this->has_data( 'cat_id' )) {
                $this->set_flash_msg( 'error', get_msg( 'cat_id_not_vailable' ));
                return;
            }
            // set timezone
            $data['shop_id'] = $selected_shop_id['shop_id'];
            $data['added_user_id'] = $logged_in_user->user_id;

            if($id == "") {
                //save

            } else {
                //edit
                unset($data['added_date']);
                unset($data['added_user_id']);
                $data['updated_date'] = date("Y-m-d H:i:s");
                $data['updated_user_id'] = $logged_in_user->user_id;
            }
            if ( $this->has_data( 'is_renew' )) {
                $data['added_date'] = date("Y-m-d H:i:s");
            }
            //save category
            if ( ! $this->Product->save( $data, $id )) {
                // if there is an error in inserting user data,

                // rollback the transaction
                $this->db->trans_rollback();

                // set error message
                $this->data['error'] = get_msg( 'err_model' );

                return;
            }

            //get inserted product id
            $id = ( !$id )? $data['id']: $id ;

            if($color_counter_total == false) {
                // edit color
                $color_counter_total = 1;
                $color_value = $this->get_data( 'colorvalue1' );
                $color_name = $this->get_data( 'color_name1' );
                if($color_value != "") {
                    $dataColor['is_has_color'] = 1;
                    $this->Product->save( $dataColor, $id );
                    $color_data['product_id'] = $id;
                    $color_data['color_value'] = $color_value;
                    $color_data['color_name'] = $color_name;
                    $color_data['added_date'] = date("Y-m-d H:i:s");
                    $color_data['added_user_id'] = $logged_in_user->user_id;

                    $this->Color->save($color_data);
                }

            } else {
                $dataColor['is_has_color'] = 1;
                $this->Product->save( $dataColor, $id );
                // save color
                $this->ps_delete->delete_color( $id );
                $color_counter_total = $color_counter_total;
                for($i=1; $i<=$color_counter_total; $i++) {

                    $color_value = $this->get_data( 'colorvalue' . $i );
                    $color_name = $this->get_data( 'color_name' . $i );

                    if($color_value != "") {

                        $color_data['product_id'] = $id;
                        $color_data['color_value'] = $color_value;
                        $color_data['color_name'] = $color_name;
                        $color_data['added_date'] = date("Y-m-d H:i:s");
                        $color_data['added_user_id'] = $logged_in_user->user_id;

                        $this->Color->save($color_data);
                    }

                }
            }

            // prepare specification
            if($spec_counter_total == false) {
                $spec_counter_total = 1;
                $spec_title = $this->get_data('prd_spec_title1');
                $spec_desc = $this->get_data('prd_spec_desc1');

                sleep(1);

                if($spec_title != "" || $spec_desc != "") {
                    $spec_data['product_id'] = $id;
                    $spec_data['name'] = $spec_title;
                    $spec_data['description'] = $spec_desc;
                    $spec_data['added_date'] = date("Y-m-d H:i:s");
                    $spec_data['added_user_id'] = $logged_in_user->user_id;

                    $this->Specification->save($spec_data);
                }
            } else {
                $this->ps_delete->delete_spec( $id );
                $spec_counter_total = $spec_counter_total;
                for($j=1; $j<=$spec_counter_total; $j++) {
                    $spec_title = $this->get_data('prd_spec_title' . $j);
                    $spec_desc = $this->get_data('prd_spec_desc' . $j);

                    sleep(1);

                    if( $spec_title != "" || $spec_desc != "" ) {
                        $spec_data['product_id'] = $id;
                        $spec_data['name'] = $spec_title;
                        $spec_data['description'] = $spec_desc;
                        $spec_data['added_date'] = date("Y-m-d H:i:s");
                        $spec_data['added_user_id'] = $logged_in_user->user_id;

                        $this->Specification->save($spec_data);
                    }
                }
            }

            /**
             * Check Transactions
             */

            // commit the transaction
            if ( ! $this->check_trans()) {

                // set flash error message
                $this->set_flash_msg( 'error', get_msg( 'err_model' ));
            } else {

                if ( $id ) {
                    // if user id is not false, show success_add message

                    $this->set_flash_msg( 'success', get_msg( 'success_prd_edit' ));
                } else {
                    // if user id is false, show success_edit message

                    $this->set_flash_msg( 'success', get_msg( 'success_prd_add' ));
                }
            }
        }

        // Product Id Checking
        if ( $this->has_data( 'gallery' )) {
            // if there is gallery, redirecti to gallery
            redirect( $this->module_site_url( 'gallery/' .$id ));
        }else if ( $this->has_data( 'attribute' )) {
            redirect( site_url( ) . '/admin/attributes/add/'.$id);
        }
        else {
            // redirect to list view
            redirect( $this->module_site_url() );
        }
    }


    function tableedit() {
        header('Content-Type: application/json');
        $rules = array(
            array(
                'field' => 'id',
                'label' => get_msg('id'),
                'rules' => 'required'
            ),
            array(
                'field' => 'tableedit',
                'label' => get_msg('tableedit_required'),
                'rules' => 'required'
            ),
            array(
                'field' => 'name',
                'label' => get_msg('name_required'),
                'rules' => 'required'
            ),
            array(
                'field' => 'name_alt',
                'label' => get_msg('name_alt_required'),
                'rules' => 'required'
            ),
            array(
                'field' => 'original_price',
                'label' => get_msg('original_price_required'),
                'rules' => 'required'
            )

        );

        $this->form_validation->set_data($this->input->post());
        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules( $rules );

        if ( $this->form_validation->run() == FALSE ) {
            $arr = array('isError' => true, 'message' =>  validation_errors());
            echo json_encode( $arr );
            exit;
        }
        $logged_in_user = $this->ps_auth->get_user_info();

        $conds['id'] = $this->input->post("id");
        $conds['no_publish_filter'] = 1;

        $_data = $this->Product->get_one_by($conds);
        if (!$_data->id) {
            $data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
            echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            exit;
        }


        $masterDtls = array(
            'name' =>$this->input->post("name"),
            'name_alt' =>$this->input->post("name_alt"),
            'original_price' =>$this->input->post("original_price"),
            'unit_price' =>$this->input->post("original_price"),
            'shop_code' =>$this->input->post("shop_code"),
            'code' =>$this->input->post("code"),
        );
        if($this->input->post("id") && $this->Product->save($masterDtls, $this->input->post("id"))){
            $arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));
            echo json_encode( $arr );
            exit;
        }else{
            $arr = array('isError' => true, 'message' =>  get_msg('update_failed_please_try_again'));
            echo json_encode( $arr );
            exit;
        }
    }
    function get_all_categories( $shop_id )
    {
        $conds['shop_id'] = $shop_id;
        $conds['no_publish_filter'] = 1;
        $categories = $this->Category->get_all_by($conds);
        echo json_encode($categories->result());
    }

    function get_all_sub_categories( $cat_id )
    {
        $conds['cat_id'] = $cat_id;
        $conds['no_publish_filter'] = 1;
        $sub_categories = $this->Subcategory->get_all_by($conds);
        echo json_encode($sub_categories->result());
    }

    /**
     * Show Gallery
     *
     * @param      <type>  $id     The identifier
     */
    function gallery( $id ) {
        // breadcrumb urls
        $edit_product = get_msg('prd_edit');

        $this->data['action_title'] = array(
            array( 'url' => 'edit/'. $id, 'label' => $edit_product ),
            array( 'label' => get_msg( 'product_gallery' ))
        );

        $_SESSION['parent_id'] = $id;
        $_SESSION['type'] = 'product';

        $this->load_gallery();
    }


    /**
     * Update the existing one
     */
    function edit( $id )
    {
        $selected_shop_id = $this->session->userdata('selected_shop_id');
        $shop_id = $selected_shop_id['shop_id'];
        // breadcrumb urls
        $this->data['action_title'] = get_msg( 'prd_edit' );

        $this->data['selected_shop_id'] = $shop_id;

        // load user
        $this->data['product'] = $this->Product->get_one( $id );

        $conds['id'] = $id;

        $discount = $this->Product->get_one_by($conds);


        $this->data['is_discount'] = $discount->is_discount;

        $dis_conds['product_id'] = $id;

        $temp_dis = $this->ProductDiscount->get_one_by( $dis_conds );

        $discount_id = $temp_dis->discount_id;

        $conds_percent['id'] = $discount_id;

        $this->data['discount'] = $this->Discount->get_one_by( $conds_percent );

        // call the parent edit logic
        parent::edit( $id );

    }


    /**
     * Determines if valid input.
     *
     * @return     boolean  True if valid input, False otherwise.
     */
    function is_valid_input( $id = 0 )
    {
        $rule = 'required|callback_is_valid_name['. $id  .']';

        $this->form_validation->set_rules( 'name', get_msg( 'name' ), $rule);

        if ( $this->form_validation->run() == FALSE ) {
            // if there is an error in validating,

            return false;
        }

        return true;
    }

    /**
     * Determines if valid name.
     *
     * @param      <type>   $name  The  name
     * @param      integer  $id     The  identifier
     *
     * @return     boolean  True if valid name, False otherwise.
     */
    function is_valid_name( $name, $id = 0, $shop_id = 0 )
    {
        $conds['name'] = $name;
        $selected_shop_id = $this->session->userdata('selected_shop_id');
        $shop_id = $selected_shop_id['shop_id'];
        $conds['shop_id'] = $shop_id;
        //print_r($conds);die;

        if ( trim(strtolower( $this->Product->get_one( $id )->name ) ) == trim(strtolower( $name )) ) {
            // if the name is existing name for that user id,
            return true;
        } else if ( $this->Product->exists( ($conds ))) {
            // if the name is existed in the system,
            $this->form_validation->set_message('is_valid_name', get_msg( 'err_dup_name' ));
            return false;
        }
        return true;

    }


    /**
     * Delete the record
     * 1) delete pajroduct
     * 2) delete image from folder and table
     * 3) check transactions
     */
    function delete( $id )
    {

        // start the transaction
        $this->db->trans_start();

        // check access
        $this->check_access( DEL );

        // delete categories and images
        $enable_trigger = true;

        // delete categories and images
        //if ( !$this->ps_delete->delete_product( $id, $enable_trigger )) {
        $type = "product";

        if ( !$this->ps_delete->delete_history( $id, $type, $enable_trigger )) {

            // set error message
            $this->set_flash_msg( 'error', get_msg( 'err_model' ));

            // rollback
            $this->trans_rollback();

            // redirect to list view
            redirect( $this->module_site_url());
        }
        /**
         * Check Transcation Status
         */
        if ( !$this->check_trans()) {

            $this->set_flash_msg( 'error', get_msg( 'err_model' ));
        } else {

            $this->set_flash_msg( 'success', get_msg( 'success_prd_delete' ));
        }

        redirect( $this->module_site_url());
    }


    /**
     * Check product name via ajax
     *
     * @param      boolean  $product_id  The cat identifier
     */
    function ajx_exists( $id = false )
    {
        // get product name
        $name = $_REQUEST['name'];
        $selected_shop_id = $this->session->userdata('selected_shop_id');
        $shop_id = $selected_shop_id['shop_id'];

        if ( $this->is_valid_name( $name, $id, $shop_id )) {
            // if the product name is valid,
            echo "true";
        } else {
            // if invalid product name,
            echo "false";
        }
    }

    /**
     * Publish the record
     *
     * @param      integer  $prd_id  The product identifier
     */
    function ajx_publish( $product_id = 0 )
    {
        // check access
        $this->check_access( PUBLISH );

        // prepare data
        $prd_data = array( 'status'=> 1 );

        // save data
        if ( $this->Product->save( $prd_data, $product_id )) {
            //Need to delete at history table because that wallpaper need to show again on app
            //$data_delete['product_id'] = $product_id;
            $data_delete['type_id'] = $product_id;
            $data_delete['type_name'] = "product";
            //$this->Product_delete->delete_by($data_delete);
            $this->Delete_history->delete_by($data_delete);
            echo 'true';
        } else {
            echo 'false';
        }
    }

    /**
     * Unpublish the records
     *
     * @param      integer  $prd_id  The category identifier
     */
    function ajx_unpublish( $product_id = 0 )
    {
        // check access
        $this->check_access( PUBLISH );

        // prepare data
        $prd_data = array( 'status'=> 0 );

        // save data
        if ( $this->Product->save( $prd_data, $product_id )) {

            //Need to save at history table because that wallpaper no need to show on app
            $data_delete['product_id'] = $product_id;
            $this->Delete_history->save($data_delete);
            echo 'true';
        } else {
            echo 'false';
        }
    }


    /**
     * Publish the record
     *
     * @param      integer  $prd_id  The product identifier
     */
    function ajx_available( $product_id = 0 )
    {
        // check access
        $this->check_access( PUBLISH );

        // prepare data
        $prd_data = array( 'is_available'=> 1 );

        // save data
        if ( $this->Product->save( $prd_data, $product_id )) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    /**
     * Unpublish the records
     *
     * @param      integer  $prd_id  The category identifier
     */
    function ajx_unavailable( $product_id = 0 )
    {
        // check access
        $this->check_access( PUBLISH );

        // prepare data
        $prd_data = array( 'is_available'=> 0 );

        // save data
        if ( $this->Product->save( $prd_data, $product_id )) {

            //Need to save at history table because that wallpaper no need to show on app
            echo 'true';
        } else {
            echo 'false';
        }
    }
    /**
     * To get all products for collection
     *
     */
    function get_all_products_for_collection($collection_id = '000')
    {

        //Datatables Variables
        if ($collection_id=='000') {

            // Datatables Variables
            $draw = intval($this->input->get("draw"));
            $start = intval($this->input->get("start"));
            $length = intval($this->input->get("length"));

            //get all products
            $selected_shop_id = $this->session->userdata('selected_shop_id');
            $shop_id = $selected_shop_id['shop_id'];
            $conds['shop_id'] = $shop_id;
            $products = $this->Product->get_all_by($conds);

            $data = array();

            foreach($products->result() as $r) {

                $unit_price = $r->unit_price;
                $unit_price = round($unit_price, 2);

                $data[] = array(
                    $r->id,
                    $r->name,
                    $r->code,
                    $unit_price
                );
            }

            $output = array(
                "draw" => $draw,
                "recordsTotal" => $products->num_rows(),
                "recordsFiltered" => $products->num_rows(),
                "data" => $data
            );
            echo json_encode($output);
            exit();
        } else {

            // Datatables Variables
            $draw = intval($this->input->get("draw"));
            $start = intval($this->input->get("start"));
            $length = intval($this->input->get("length"));

            $selected_shop_id = $this->session->userdata('selected_shop_id');
            $shop_id = $selected_shop_id['shop_id'];
            $conds['shop_id'] = $shop_id;
            $conds1['collection_id'] = $collection_id;

            $product_collection = $this->Productcollection->get_all_by($conds1)->result();
            if($product_collection) {
                $result = "";
                foreach ($product_collection as $prd_collect) {
                    $result .= "'".$prd_collect->product_id ."'" .",";

                }
                $prd_ids_from_coll = rtrim($result,",");

                $conds['prd_ids_from_coll'] = $prd_ids_from_coll;

                $prd_dis = $this->Product->get_all_product_collection($conds);

                $products_data = array();
                foreach($prd_dis->result() as $prd) {

                    $unit_price = $prd->unit_price;
                    $unit_price = round($unit_price, 2);

                    $products_data[] = array(
                        $prd->id,
                        $prd->name,
                        $prd->code,
                        $unit_price
                    );
                }


                $output = array(
                    "draw" => $draw,
                    "recordsTotal" => $prd_dis->num_rows(),
                    "recordsFiltered" => $prd_dis->num_rows(),
                    "data" => $products_data

                );
                echo json_encode($output);
                exit();
            } else {
                $products = $this->Product->get_all_by($conds);

                $data = array();

                foreach($products->result() as $r) {

                    $data[] = array(
                        $r->id,
                        $r->name,
                        $r->code,
                        $r->unit_price.$this->Shop->get_one('1')->currency_symbol
                    );
                }

                $output = array(
                    "draw" => $draw,
                    "recordsTotal" => $products->num_rows(),
                    "recordsFiltered" => $products->num_rows(),
                    "data" => $data
                );
                echo json_encode($output);
                exit();
            }
        }

    }

    function get_all_products_for_discount($discount_id='000')
    {

        if ($discount_id=='000') {
            // Datatables Variables
            $draw = intval($this->input->get("draw"));
            $start = intval($this->input->get("start"));
            $length = intval($this->input->get("length"));

            $selected_shop_id = $this->session->userdata('selected_shop_id');
            $shop_id = $selected_shop_id['shop_id'];
            $conds['shop_id'] = $shop_id;

            //// Start - Filter products inside other discount ////
            $conds2['discount_id'] = $discount_id;
            $product_discount_from_others = $this->ProductDiscount->get_all_not_in_discount($conds2)->result();

            $result_others = "";
            foreach ($product_discount_from_others as $prd_dis) {
                $result_others .= "'".$prd_dis->product_id ."'" .",";

            }

            $prd_ids_from_dis_other = rtrim($result_others,",");

            $conds['prd_ids_from_dis_other'] = $prd_ids_from_dis_other;

            //// End - Filter products inside other discount ///

            $prd_dis = $this->Product->get_all_product($conds);

            $products_data = array();
            foreach($prd_dis->result() as $prd) {

                $unit_price = $prd->unit_price;
                $unit_price = round($unit_price, 2);

                $products_data[] = array(
                    $prd->id,
                    $prd->name,
                    $prd->code,
                    $unit_price
                );
            }


            $output = array(
                "draw" => $draw,
                "recordsTotal" => $prd_dis->num_rows(),
                "recordsFiltered" => $prd_dis->num_rows(),
                "data" => $products_data

            );
            echo json_encode($output);
            exit();
        } else {
            // Datatables Variables
            $draw = intval($this->input->get("draw"));
            $start = intval($this->input->get("start"));
            $length = intval($this->input->get("length"));

            $selected_shop_id = $this->session->userdata('selected_shop_id');
            $shop_id = $selected_shop_id['shop_id'];
            $conds['shop_id'] = $shop_id;

            $conds1['discount_id'] = $discount_id;



            $product_discount = $this->ProductDiscount->get_all_in_discount($conds1)->result();

            //echo "sdasdasd"; die;

            //if($product_discount) {

            //// Start - Filter products inside current discount (To see on top)  ////
            $result = "";
            foreach ($product_discount as $prd_dis) {
                $result .= "'".$prd_dis->product_id ."'" .",";

            }
            $prd_ids_from_dis = rtrim($result,",");


            $conds['prd_ids_from_dis'] = $prd_ids_from_dis;

            //// End - Filter products inside current discount (To see on top)  ////


            //// Start - Filter products inside other discount ////
            $conds2['discount_id'] = $discount_id;
            $product_discount_from_others = $this->ProductDiscount->get_all_not_in_discount($conds2)->result();

            $result_others = "";
            foreach ($product_discount_from_others as $prd_dis) {
                $result_others .= "'".$prd_dis->product_id ."'" .",";

            }

            $prd_ids_from_dis_other = rtrim($result_others,",");

            $conds['prd_ids_from_dis_other'] = $prd_ids_from_dis_other;

            //// End - Filter products inside other discount ///

            $prd_dis = $this->Product->get_all_product($conds);

            $products_data = array();
            foreach($prd_dis->result() as $prd) {

                $unit_price = $prd->unit_price;
                $unit_price = round($unit_price, 2);

                $products_data[] = array(
                    $prd->id,
                    $prd->name,
                    $prd->code,
                    $unit_price
                );
            }


            $output = array(
                "draw" => $draw,
                "recordsTotal" => $prd_dis->num_rows(),
                "recordsFiltered" => $prd_dis->num_rows(),
                "data" => $products_data

            );
            echo json_encode($output);
            exit();
            // } else {
            // 	$prd_dis = $this->Product->get_all_by($conds);

            //   $products_data = array();
            //   foreach($prd_dis->result() as $prd) {

            //   $products_data[] = array(
            //              $prd->id,
            //              $prd->name,
            //              $prd->code,
            //              $prd->unit_price .$this->Shop->get_one('1')->currency_symbol
            //          );
            //   }


            //    	$output = array(
            //          "draw" => $draw,
            //        "recordsTotal" => $prd_dis->num_rows(),
            //        "recordsFiltered" => $prd_dis->num_rows(),
            //        "data" => $products_data

            //      );
            //    	echo json_encode($output);
            //    	exit();
            // }
        }

    }

    function import()
    {

        $this->data['action_title'] = get_msg( 'product_import' );
        $selected_shop_id = $this->session->userdata('selected_shop_id');

        $conds['no_publish_filter'] = 1;
        $conds['status'] = 0;
        $this->data['shops'] = $this->Shop->get_all_by_shop( $conds);

        $categoriesconds['shop_id'] = $selected_shop_id;
        if ( $this->has_data( 'shop' ) && $this->has_data( 'cat_id' )) {

            $condss['no_publish_filter'] = 1;
            $condss['shop_id'] = $this->get_data( 'shop' );
            $condss['cat_id'] = $this->get_data( 'cat_id' );
            if ($this->has_data('sub_cat_id')){
                $condss['sub_cat_id'] = $this->get_data( 'sub_cat_id' );
            }

            // get categories
            $products = $this->Product->get_all_by( $condss);

            $logged_in_user = $this->ps_auth->get_user_info();
            foreach($products->result() as $product){

                $images = array();
                $images=$this->Image->get_all_by(array("img_type" =>  "product", "img_parent_id" =>$product->id ))->result();
                $productcheck=$this->Product->get_one_by(array("name" =>  $product->name, "shop_id" =>  $selected_shop_id['shop_id'], "no_publish_filter" =>  1 ));

                if($productcheck->is_empty_object){
                    //  $this->db->trans_start();
                    $data = array();

                    $data['imported_shop_id'] = $product->shop_id;
                    $data['imported_product_id'] = $product->id;
                    // Category id
                    if ($this->has_data('cat_id_to')){
                        $data['cat_id'] = $this->get_data('cat_id_to');
                    }
                    // Sub Category id
                    if ($this->has_data('sub_cat_id_to')) {
                        $data['sub_cat_id'] = $this->get_data('sub_cat_id_to');
                    }
                    // brand id
                    if ( $product->brand_id) {
                        $data['brand_id'] = $product->brand_id;
                    }

                    // Sub Category id
                    if ( $product->discount_type_id) {
                        $data['discount_type_id'] = $product->discount_type_id;
                    }

                    // product_location_id id
                    if ( $product->product_location_id) {
                        $data['product_location_id'] = $product->product_location_id;
                    }

                    // prepare product name
                    if ( $product->name) {
                        $data['name'] = $product->name;
                    }

                    // prepare product description
                    if ( $product->description) {
                        $data['description'] = $product->description;
                    }

                    if ( $product->name_alt) {
                        $data['name_alt'] = $product->name_alt;

                    }
                    // prepare product unit price
                    if ( $product->unit_price) {
                        $data['unit_price'] = $product->unit_price;
                    }

                    // prepare product original price
                    if ( $product->original_price) {
                        $data['original_price'] = $product->original_price;
                        if($data['unit_price']==null || $data['unit_price']=="" || $data['unit_price']==0){
                            $data['unit_price']=$data['original_price'];
                        }
                    }

                    // prepare product shipping cost
                    if ( $product->thumbnail) {
                        $data['thumbnail'] = $product->thumbnail;
                    }
                    // prepare product shipping cost
                    if ( $product->shipping_cost) {
                        $data['shipping_cost'] = $product->shipping_cost;
                    }

                    // prepare product minimum order
                    if ( $product->minimum_order) {
                        $data['minimum_order'] = $product->minimum_order;
                    }

                    // prepare product maximum order
                    if ( $product->maximum_order) {
                        $data['maximum_order'] = $product->maximum_order;
                    }

                    // Product Measurement
                    if ( $product->product_measurement) {
                        $data['product_measurement'] = $product->product_measurement;
                    }

                    // Product Unit
                    if ( $product->product_unit) {
                        $data['product_unit'] = $product->product_unit;
                    }


                    // prepare product search tag
                    if ( $product->search_tag) {
                        $data['search_tag'] = $product->search_tag;
                    }

                    // prepare product highlight information
                    if ( $product->highlight_information) {
                        $data['highlight_information'] = $product->highlight_information;
                    }

                    // prepare product product code
                    if ( $product->code) {
                        $data['code'] = $product->code;
                    }

                    // prepare product product code
                    if ( $product->shop_code) {
                        $data['shop_code'] = $product->shop_code;
                    }


                    // if 'is featured' is checked,
                    if ($product->is_featured) {

                        $data['is_featured'] = 1;
                        if ($data['is_featured'] == 1) {
                            $data['updated_date'] = date("Y-m-d H:i:s");
                            $data['featured_date'] = date("Y-m-d H:i:s");
                        }
                    } else {

                        $data['is_featured'] = 0;
                    }

                    // if 'no_outside_city' is checked,
                    if ( $product->no_outside_city) {
                        $data['no_outside_city'] = 1;
                    } else {
                        $data['no_outside_city'] = 0;
                    }

                    // if 'eligible_free_ship' is checked,
                    if ( $product->eligible_free_ship) {
                        $data['eligible_free_ship'] = 1;
                    } else {
                        $data['eligible_free_ship'] = 0;
                    }

                    // if 'is available' is checked,
                    if ( $product->is_available && $product->unit_price && $product->unit_price>0) {
                        $data['is_available'] = 1;
                    } else {
                        $data['is_available'] = 0;
                    }

                    // if 'status' is checked,
                    if ( $product->status && $product->unit_price && $product->unit_price>0) {
                        $data['status'] = 1;
                    } else {
                        $data['status'] = 0;
                    }

                    if ( !$selected_shop_id['shop_id']) {

                        // set flash error message
                        $this->set_flash_msg( 'error', get_msg( 'shop_id_not_vailable' ));
                        return;
                    }
                    if (!$product->cat_id) {
                        $this->set_flash_msg( 'error', get_msg( 'cat_id_not_vailable' ));
                        return;
                    }
                    // set timezone
                    $data['shop_id'] = $selected_shop_id['shop_id'];
                    $data['added_user_id'] = $logged_in_user->user_id;

                    if($id == "") {
                        //save

                    } else {
                        //edit
                        unset($data['added_date']);
                        unset($data['added_user_id']);
                        $data['updated_date'] = date("Y-m-d H:i:s");
                        $data['updated_user_id'] = $logged_in_user->user_id;
                    }

                    $data['added_date'] = $product->added_date;

                    //save category
                    // if ( ! $this->Product->save( $data)) {
                    //     // if there is an error in inserting user data,

                    //     // rollback the transaction
                    //     $this->db->trans_rollback();

                    //     // set error message
                    //     $this->data['error'] = get_msg( 'err_model' );

                    //     return;
                    // }$data['name'] = $product->name;

                    //get inserted product id
                    $this->Product->save($data);
                    $ppp=$this->Product->get_one_by(array('cat_id'=>$data['cat_id'], "name" =>  $data['name'], "shop_id" =>  $selected_shop_id['shop_id'], "no_publish_filter" =>  1));
                    $intertedid=$ppp->id;
                    // cover picture
                    //$id = ( !$id )? $data['id']: $id ;
                    $image_data = array();
                    // $coverimagecheck=$this->Image->get_one_by(array('is_default'=>1, "img_type" =>  "product", "img_parent_id" =>  $produc->id ));
                    // if($coverimagecheck && $coverimagecheck->img_path){
                    //     $image = array(
                    //         'img_parent_id'=> $id,
                    //         'img_type' => $coverimagecheck->img_type,
                    //         'img_desc' => $coverimagecheck->img_desc,
                    //         'img_path' => $coverimagecheck->img_path,
                    //         'img_width'=> $coverimagecheck->img_width,
                    //         'img_height'=> $coverimagecheck->img_height,
                    //         'is_default'=> $coverimagecheck->is_default
                    //     );

                    //     $this->Image->save( $image );
                    // if($coverimagecheck->is_default==1){
                    //     $dataThumb['thumbnail'] = $coverimagecheck->img_path;
                    //     $this->Product->save( $dataThumb, $id );
                    // }
                    //}
                    foreach( $images as $singimage ) {
                        $image_data = array();
                        $image_data['img_parent_id'] = $intertedid;
                        $image_data['img_type'] = $singimage->img_type;
                        $image_data['img_path'] = $singimage->img_path;
                        $image_data['img_width'] = $singimage->img_width;
                        $image_data['img_height'] = $singimage->img_height;
                        $image_data['img_desc'] = $singimage->img_desc;
                        $image_data['is_default'] = $singimage->is_default;
                        $this->Image->accSave( $image_data );
                        // if($singimage->is_default==1){
                        //     $dataThumb['thumbnail'] = $singimage->img_path;
                        //     $this->Product->save( $dataThumb, $id );
                        // }

                    }

                    $colors = $this->Color->get_all_by( array( 'product_id' => @$product->id , "no_publish_filter" =>  1))->result();
                    if ( !empty( $colors )){

                        $dataColor['is_has_color'] = 1;
                        $this->Product->save( $dataColor, $intertedid );
                        // save color
                        $this->ps_delete->delete_color( $intertedid );
                        foreach( $colors as $color ){
                            $color_value = $color->color_value;
                            $color_name = $color->color_name;

                            if($color_value != "") {
                                $color_data = array();
                                $color_data['product_id'] = $intertedid;
                                $color_data['color_value'] = $color_value;
                                $color_data['color_name'] = $color_name;
                                $color_data['added_date'] = date("Y-m-d H:i:s");
                                $color_data['added_user_id'] = $logged_in_user->user_id;

                                $this->Color->save($color_data);
                            }

                        }

                    }
                    $spec_data_old = $this->Specification->get_all_by( array( 'product_id' => @$product->id, "no_publish_filter" =>  1 ))->result();

                    // prepare specification
                    if ( !empty( $spec_data_old )){
                        $this->ps_delete->delete_spec( $intertedid );
                        foreach( $spec_data_old as $spec ) {
                            $spec_data = array();
                            $spec_title = $spec->name;
                            $spec_desc = $spec->description;

                            //sleep(1);

                            if( $spec_title != "" || $spec_desc != "" ) {
                                $spec_data['product_id'] = $intertedid;
                                $spec_data['name'] = $spec_title;
                                $spec_data['description'] = $spec_desc;
                                $spec_data['added_date'] = date("Y-m-d H:i:s");
                                $spec_data['added_user_id'] = $logged_in_user->user_id;

                                $this->Specification->save($spec_data);
                            }
                        }
                    }



                    $attributes = $this->Attribute->get_all_by(array( 'product_id' => @$product->id, "no_publish_filter" =>  1 ))->result();
                    if ( !empty( $attributes )){
                        $dataattribute['is_has_attribute'] = 1;
                        $this->Product->save( $dataattribute, $intertedid );
                        foreach( $attributes as $attribute ) {
                            $attributename = $attribute->name;
                            $attribute_old_id = $attribute->id;

                            if( $attributename != "") {
                                $attribute_data['product_id'] = $intertedid;
                                $attribute_data['name'] = $attributename;
                                $attribute_data['shop_id'] = $selected_shop_id['shop_id'];
                                $attribute_data['added_date'] = date("Y-m-d H:i:s");
                                $attribute_data['added_user_id'] = $logged_in_user->user_id;

                                $attribute_new_id=$this->Attribute->accSave($attribute_data);
                                // attributes details

                                $attributedetails = $this->Attributedetail->get_all_by(array( 'product_id' => @$product->id, 'header_id' => $attribute_old_id, "no_publish_filter" =>  1 ))->result();
                                if ( !empty( $attributedetails)){
                                    foreach( $attributedetails as $attributedetail ) {
                                        $attributedetailname = $attributedetail->name;
                                        if( $attributedetailname != "" && $attribute_new_id !="") {
                                            $attributedetail_data['product_id'] = $intertedid;
                                            $attributedetail_data['header_id'] = $attribute_new_id;
                                            $attributedetail_data['name'] = $attributedetailname;
                                            $attributedetail_data['additional_price'] = $attributedetail->additional_price;
                                            $attributedetail_data['price_type'] = $attributedetail->price_type;
                                            $attributedetail_data['display_serial'] = $attributedetail->display_serial;
                                            $attributedetail_data['shop_id'] = $selected_shop_id['shop_id'];
                                            $attributedetail_data['added_date'] = date("Y-m-d H:i:s");
                                            $attributedetail_data['added_user_id'] = $logged_in_user->user_id;
                                            $this->Attributedetail->save($attributedetail_data);

                                        }
                                    }
                                }
                                // attributes details end
                            }
                        }
                    }
                    /**
                     * Check Transactions
                     */

                    // commit the transaction



                }
            }
            $this->set_flash_msg( 'success', get_msg( 'success_product_added' ));
        }
        // call the core add logic
        //parent::importform();
        parent::productsimportform();
    }



    function updatepricefromoldshop()
    {
// shop489800314e8dd1a6f0a7275c0d10f54e
// shopd4e92212f601ac8164c94d37991724e5
        $condss['no_publish_filter'] = 1;
        $condss['shop_id'] = "shopd4e92212f601ac8164c94d37991724e5";
        $products = $this->Product->get_all_by( $condss);
        foreach($products->result() as $product){
            $productcheck=$this->Product->get_one_by(array("imported_product_id" =>  $product->id, "imported_shop_id" =>  $product->shop_id, "shop_id" =>  "shop4f302930cd7fecfc9b60a4b240a5c736", "no_publish_filter" =>  1 ));

            if($productcheck->id){
                $data = array();
                if ( $product->unit_price) {
                    $data['unit_price'] = $product->unit_price;
                }

                // prepare product original price
                if ( $product->original_price) {
                    $data['original_price'] = $product->original_price;
                    if($data['unit_price']==null || $data['unit_price']=="" || $data['unit_price']==0){
                        $data['unit_price']=$data['original_price'];
                    }
                }

                $this->Product->save($data, $productcheck->id);
            }
        }
        print("done!");
    }

//    function copyproductfrompos()
//    {
//
//        $conds['no_publish_filter'] = 1;
//        $shop_id='shop4f302930cd7fecfc9b60a4b240a5c736';
//
//        $sqlquery = "SELECT * FROM sma_cart_elsanf";
//        $resultSet = $this->Acc_period->queryPrepare($sqlquery);
//        $rows=$resultSet->result();
//        foreach ($rows as $row) {
//            $result = $row->s3r_bi3 - $row->s3r_bi3 / (1 + 15 / 100);
//            $price = $row->s3r_bi3 - $result;
//            $cost_vat = $row->s3r_sheraa - $row->s3r_sheraa / (1 + 15 / 100);
//            $cost = $row->s3r_sheraa - $cost_vat;
//
//            if ($row->edafa == "False" && $row->sarf == "True") {
//                $checkproducts=$this->Product->get_one_by(array("shop_id" =>  "shop4f302930cd7fecfc9b60a4b240a5c736", "no_publish_filter" =>  1, "code" =>  $row->code));
//                if ($checkproducts->code == $row->code) {
//                    $data = [
//                        'unit_price' => round($price, 2),
//                        'original_price' => round($price, 2),
//                        'buy_price' => round($cost, 2),
//                    ];
//                    $this->Product->save($data, $checkproducts->id);
//                } else {
//                    $data = array();
//                    // Category id
//                    $data['cat_id'] = "cat270bb53c2332c92e715b0432c9b76569";
//                    // prepare product name
//                    if ( $row->elesm_ar) {
//                        $data['name'] = $row->elesm_ar;
//                    }
//
//
//                    if ( $row->elesm_en) {
//                        $data['name_alt'] = $row->elesm_en;
//
//                    }
//                    // prepare product unit price
//                    if ( $price) {
//                        $data['unit_price'] = round($price, 2);
//                        $data['buy_price'] = round($cost, 2);
//                    }
//
//                    // prepare product original price
//                    $data['original_price'] = round($price, 2);
//                    // prepare product minimum order
//                        $data['minimum_order'] = 1;
//                    // Product Measurement
//                    if ( $row->elw7da) {
//                        $data['product_measurement'] = $row->elw7da;
//                    }
//
//                    // Product Unit
//                    if ( $row->elw7da) {
//                        $data['product_unit'] = $row->elw7da;
//                    }
//
//
//                    // prepare product search tag
//                    if ( $row->elesm_ar) {
//                        $data['search_tag'] = $row->elesm_ar;
//                    }
//
//                    // prepare product product code
//                    if ($row->code) {
//                        $data['code'] = $row->code;
//                    }
//
//                    // if 'is featured' is checked,
//                    $data['is_featured'] = 0;
//                    // if 'no_outside_city' is checked,
//                    $data['no_outside_city'] = 1;
//                    // if 'eligible_free_ship' is checked,
//                    $data['eligible_free_ship'] = 0;
//                    // if 'is available' is checked,
//                    $data['is_available'] = 0;
//                    // if 'status' is checked,
//                    $data['status'] = 0;
//                    // set timezone
//                    $data['shop_id'] = "shop4f302930cd7fecfc9b60a4b240a5c736";
//                    $data['added_user_id'] = "c4ca4238a0b923820dcc509a6f75849b";
//
//
//                    $data['added_date'] = date("Y-m-d H:i:s");
//                    $this->Product->save($data);
//                }
//            }
//
//        }
//        print "done!!!";
//    }
}