<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users crontroller for BE_USERS table
 */
class Comission_from_seller extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'Acc_period' );
	}

	/**
	* Load Notification Sending Form
	*/
	function index() {
		$this->data['action_title'] = get_msg('comission_from_seller');

		// for showing data
		if($this->input->get("showAction")){
			$conds['id'] = $this->input->get("id");
			$defaultInstance = $this->Acc_comission_seller->get_one_by($conds);
			if (!$defaultInstance->id) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$shopdata = $this->Shop->get_one_by( array( 'id' => $defaultInstance->shop_id));
			$data = array('isError' => false, 'defaultInstance' => $defaultInstance, 'shopdata' => $shopdata);    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		// for editing data
		if($this->input->get("editData")){
			$conds['id'] = $this->input->get("id");
			$defaultInstance = $this->Acc_comission_seller->get_one_by($conds);
			if (!$defaultInstance->id) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$shopdata = $this->Shop->get_one_by( array( 'id' => $defaultInstance->shop_id));
			$data = array('isError' => false, 'defaultInstance' => $defaultInstance, 'shopdata' => $shopdata);
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		
		$this->data['contractTypeList'] = $this->Acc_coa->enumListLanguage("contract_type");
		$this->data['sellerList'] =  $this->Acc_coa->stakHldrDropdownList(array('stakeHolderType' => 'Seller'));

		$this->load_template('acc_comission_from_seller/list', $this->data, true );
	}

	
	 public function tablelist()
	 {
         header('Content-Type: application/json');
		 $iDisplayStart = $this->input->get("iDisplayStart")? intval($this->input->get("iDisplayStart")):0;
		 $iDisplayLength = $this->input->get("iDisplayLength")? intval($this->input->get("iDisplayLength")) :10;
		 $sSortDir = $this->input->get("sSortDir_0");
		 $iSortingCol = $this->input->get("iSortCol_0")? $this->input->get("iSortCol_0") : null;
		 $sSearch = $this->input->get("sSearch") ? $this->input->get("sSearch") : null;
		 if($sSearch){
			 $sSearch = "% ".$sSearch." %";
		 }
		 
		 $dataReturns = array();
	 
		 $langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
		 $sqlWhere = $this->input->get("shop_id") ? " AND acc_comission_seller.shop_id= '".$this->input->get("shop_id")."'" : "";
		 $sqlWhere .= $this->input->get("sSearch") ? " AND mk_shops.name LIKE '%".$this->input->get("sSearch")."%'" : "";
		 $querySQL = "SELECT acc_comission_seller.id, acc_comission_seller.amount_percentage, acc_comission_seller.shop_id, acc_comission_seller.is_default, acc_comission_seller.date_created, acc_comission_seller.created_by, acc_comission_seller.last_updated, acc_comission_seller.updated_by, mk_shops.name, mk_shops.address1 FROM acc_comission_seller LEFT JOIN mk_shops ON acc_comission_seller.shop_id = mk_shops.id where acc_comission_seller.id >0 ".$sqlWhere." ORDER BY acc_comission_seller.id ASC LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart."";

		 $resultSet = $this->Acc_period->queryPrepare($querySQL);
		 $totalCount = $resultSet->num_rows();
		 $serial = 0;
			 $total = 0;
			 
		 $actionList = array();

         $actionList = array();

         $actionListConfirm = array();
         $actionList[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
         $actionList[]=array('actionName' => 'update', 'actionClass' => 'update', 'titleName' => get_msg( 'update_data'), 'iconName' => 'fa fa-pencil-square-o text-navy');
         $actionList[]=array('actionName' => 'confirm', 'actionClass' => 'confirm', 'titleName' => get_msg( 'Confirm_Data'), 'iconName' => 'fa fa-check-circle text-warning');
         //  $actionList[]=array('actionName' => 'delete', 'actionClass' => 'delete', 'titleName' => get_msg( 'Delete_Data'), 'iconName' => 'fa fa-trash text-danger');
         $actionListConfirm[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');



         foreach ($resultSet->result() as $row)
		 {
			 $serial++;
			 $created="";
			 $updated="";
			 if($row->created_by){
				$createddata=$this->User->get_one_by( array( 'user_id' => $row->created_by));
				$created=$createddata->user_name;
			 }
			 if($row->updated_by){
				$updateddata=$this->User->get_one_by( array( 'user_id' => $row->updated_by));
				$updated=$updateddata->user_name;
			 }
			 $dataReturns[]=array('DT_RowId' => $row->id, 'accessibleUrl' => ($row->is_default == "1" ? $actionListConfirm : $actionList),'shop_id' => $row->shop_id, 'activeStatus' => get_msg( 'Active'),
			 0 => $serial, 
			 1 => $row->name,
			 2 => $row->address1,
			 3 => $row->amount_percentage,
			 4 => $created,
			 5 => $row->date_created,
			 6 => $row->last_updated,
			 7 => $updated,
			 8 => "");
		 }
		 
		 $gridData=array();
		 if ($totalCount == 0) {
			 $gridData=array('iTotalRecords' => 0, 'iTotalDisplayRecords' =>0, 'aaData'=> array());
			 echo json_encode($gridData);
			 exit();
		 }
 
		 $gridData=array('iTotalRecords' => $totalCount, 'iTotalDisplayRecords' =>$totalCount, 'aaData'=> $dataReturns);
		 echo json_encode($gridData);
		 exit();
	 }
	function writesubmit(){
		header('Content-Type: application/json');  
		$rules = array(
			array(
					'field' => 'shop_id',
					'label' => get_msg('shop_name'),
					'rules' => 'required'
			),
			array(
				'field' => 'amount_percentage',
				'label' => get_msg('comission_percentage'),
				'rules' => 'required'
			),
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$shop_id = $this->input->post('shop_id');
		if(!$shop_id){
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}

		$checkdata=$this->Shop->get_one_by( array( 'id' => $shop_id));
		if (!$checkdata->id)
		{
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}


//		$checkFees=$this->Acc_comission_seller->get_one_by( array( 'shop_id' => $shop_id));
//		if ($checkFees->id)
//		{
//			$arr = array('isError' => true, 'message' =>  get_msg('data_already_exist'));
//			echo json_encode( $arr );
//			exit;
//		}

		$logged_in_user = $this->ps_auth->get_user_info();
		$data = array(
			'shop_id' => $this->input->post("shop_id"),
			'amount_percentage' => $this->input->post("amount_percentage"),
			'remark' => $this->input->post('remark'),
			'created_by' => $logged_in_user->user_id,
		);

		if($this->Acc_comission_seller->save($data)){
			$arr = array('isError' => false, 'message' =>  get_msg('save_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}
	}
	function editsubmit(){
		header('Content-Type: application/json');  

		$rules = array(
			array(
				'field' => 'shop_id',
				'label' => get_msg('shop_name'),
				'rules' => 'required'
			),
			array(
				'field' => 'amount_percentage',
				'label' => get_msg('comission_percentage'),
				'rules' => 'required'
			),
			array(
				'field' => 'id',
				'label' => get_msg('id'),
				'rules' => 'required'
			),
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$commission_id = $this->input->post('id');
		if(!$commission_id){
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}

		$checkdata=$this->Shop->get_one_by( array( 'id' => $this->input->post('shop_id')));
		if (!$checkdata->id)
		{
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}

		$checkdata=$this->Acc_comission_seller->get_one_by( array( 'id' => $this->input->post('id'), 'shop_id' => $this->input->post('shop_id')));
		if (!$checkdata->id)
		{
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}
		$logged_in_user = $this->ps_auth->get_user_info();
		$data = array(
			'amount_percentage' => $this->input->post("amount_percentage"),
			'remark' => $this->input->post('remark'),
			'updated_by' => $logged_in_user->user_id,
		);
		if($this->Acc_comission_seller->save($data, $this->input->post('id'))){
			$arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}

	}

    function confirmsubmit() {
        header('Content-Type: application/json');
        $rules = array(
            array(
                'field' => 'id',
                'label' => get_msg('id'),
                'rules' => 'required'
            ),array(
                'field' => 'confirmStatus',
                'label' => get_msg('confirmStatus'),
                'rules' => 'required'
            )

        );

        $this->form_validation->set_data($this->input->post());
        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules( $rules );

        if ( $this->form_validation->run() == FALSE ) {
            $arr = array('isError' => true, 'message' =>  validation_errors());
            echo json_encode( $arr );
            exit;
        }
        $logged_in_user = $this->ps_auth->get_user_info();

        $conds['id'] = $this->input->post("id");
        $defaultInstance = $this->Acc_comission_seller->get_one_by($conds);
        if (!$defaultInstance->id) {
            $data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
            echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            exit;
        }

        if ($defaultInstance->is_default==1) {
            $data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
            echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            exit;
        }
        $shopdata = $this->Shop->get_one_by( array( 'id' => $this->input->post("shop_id")));
        if (!$shopdata->id) {
            $data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
            echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            exit;
        }


        $feesSQL = "SELECT * FROM acc_comission_seller WHERE is_default='1' AND shop_id='".$this->input->post("shop_id")."'";
        $resultSet = $this->Acc_period->queryPrepare($feesSQL);
        foreach ($resultSet->result() as $row)
        {
            $updateArr = array(
                'is_default' => 0,
            );
            $this->Acc_comission_seller->save($updateArr, $row->id);
        }

        $newupdateArr = array(
            'is_default' => 1,
        );

        if($this->Acc_comission_seller->save($newupdateArr, $this->input->post("id"))){
            $arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));
            echo json_encode( $arr );
            exit;
        }else{
            $arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));
            echo json_encode( $arr );
            exit;
        }

    }

}