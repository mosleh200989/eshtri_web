<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users crontroller for BE_USERS table
 */
class Paymentgatewayfees extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'Acc_period' );
	}

	/**
	* Load Notification Sending Form
	*/
	function index() {
		$this->data['action_title'] = get_msg('payment_gateway');
		// for showing data
		if($this->input->get("showAction")){
			$conds['id'] = $this->input->get("id");
			$defaultInstance = $this->Acc_paymentgateway_fees->get_one_by($conds);
			if (!$defaultInstance->id) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$liabilityBy="".get_msg('Shop')."";
			if($defaultInstance->liability_by==1){
				$liabilityBy="".get_msg('eShtri')."";
			}
			$data = array('isError' => false, 'defaultInstance' => $defaultInstance, 'liabilityBy' => $liabilityBy);    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		// for editing data
		if($this->input->get("editData")){
			$conds['id'] = $this->input->get("id");
			$defaultInstance = $this->Acc_paymentgateway_fees->get_one_by($conds);
			if (!$defaultInstance->id) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$data = array('isError' => false, 'defaultInstance' =>  $defaultInstance);    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$this->load_template('acc_paymentgateway_fees/list', $this->data, true );
	}

	
	 public function tablelist()
	 {
 
		 $iDisplayStart = $this->input->get("iDisplayStart")? intval($this->input->get("iDisplayStart")):0;
		 $iDisplayLength = $this->input->get("iDisplayLength")? intval($this->input->get("iDisplayLength")) :10;
		 $sSortDir = $this->input->get("sSortDir_0");
		 $iSortingCol = $this->input->get("iSortCol_0")? $this->input->get("iSortCol_0") : null;
		 $sSearch = $this->input->get("sSearch") ? $this->input->get("sSearch") : null;
		 if($sSearch){
			 $sSearch = "% ".$sSearch." %";
		 }
		 
		 $dataReturns = array();
	 
		 $langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
		 $sqlWhere = $this->input->get("sSearch") ? " AND name LIKE '%".$this->input->get("sSearch")."%'" : "";
		 $querySQL = "SELECT * FROM acc_paymentgateway_fees where id >0 ".$sqlWhere." ORDER BY id ASC LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart."";
		 $resultSet = $this->Acc_period->queryPrepare($querySQL);
		 $totalCount = $resultSet->num_rows();
		 $serial = 0;
			 $total = 0;
			 
		 $actionList = array();
		 
			 $actionListConfirm = array();
			 $actionList[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
			 $actionList[]=array('actionName' => 'update', 'actionClass' => 'update', 'titleName' => get_msg( 'update_data'), 'iconName' => 'fa fa-pencil-square-o text-navy');
			 // $actionList[]=array('actionName' => 'confirm', 'actionClass' => 'confirm', 'titleName' => get_msg( 'Confirm_Data'), 'iconName' => 'fa fa-check-circle text-warning');
			//  $actionList[]=array('actionName' => 'delete', 'actionClass' => 'delete', 'titleName' => get_msg( 'Delete_Data'), 'iconName' => 'fa fa-trash text-danger');
			 $actionListConfirm[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
			 
				 
		 foreach ($resultSet->result() as $row)
		 {
			 $serial++;

			$liabilityBy="".get_msg('Shop')."";
			if($defaultInstance->liability_by==1){
				$liabilityBy="".get_msg('eShtri')."";
			}
			 $dataReturns[]=array('DT_RowId' => $row->id, 'accessibleUrl' => $actionList,'activeStatus' => get_msg( 'Active'), 
			 0 => $serial, 
			 1 => $row->name,
			 2 => $row->cost_rate,
			 3 => $row->return_rate,
			 4 => $liabilityBy,
			 5 => $row->date_created,
			 6 => "");
		 }
		 
		 $gridData=array();
		 if ($totalCount == 0) {
			 $gridData=array('iTotalRecords' => 0, 'iTotalDisplayRecords' =>0, 'aaData'=> array());
			 echo json_encode($gridData);
			 exit();
		 }
 
		 $gridData=array('iTotalRecords' => $totalCount, 'iTotalDisplayRecords' =>$totalCount, 'aaData'=> $dataReturns);
		 echo json_encode($gridData);
		 exit();
	 }
	function writesubmit(){
		header('Content-Type: application/json');  
		$rules = array(
			array(
					'field' => 'cost_rate',
					'label' => get_msg('cost_rate'),
					'rules' => 'required'
			),
			array(
					'field' => 'name',
					'label' => get_msg('name'),
					'rules' => 'required'
			),
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$logged_in_user = $this->ps_auth->get_user_info();
		$data = array(
		'name' => $this->input->post("name"),
		'cost_rate' => $this->input->post("cost_rate"),
		'return_rate' => $this->input->post("return_rate"),
		'liability_by' => $this->input->post("liability_by"),
		'remark' => $this->input->post('remark'),
		'created_by' => $logged_in_user->user_id,
		);

		if($this->Acc_paymentgateway_fees->save($data)){
			$arr = array('isError' => false, 'message' =>  get_msg('save_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}
	}
	function editsubmit(){
		header('Content-Type: application/json');  

		$rules = array(
			array(
					'field' => 'name',
					'label' => get_msg('name'),
					'rules' => 'required'
			),
			array(
					'field' => 'cost_rate',
					'label' => get_msg('cost_rate'),
					'rules' => 'required'
			),
			array(
				'field' => 'id',
				'label' => get_msg('id'),
				'rules' => 'required'
			),
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$id = $this->input->post('id');
		if(!$id){
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}

		$checkdata=$this->Acc_paymentgateway_fees->get_one_by( array( 'id' => $id));
		if (!$checkdata->id)
		{
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}
		$logged_in_user = $this->ps_auth->get_user_info();
		$data = array(
		'name' => $this->input->post("name"),
		'cost_rate' => $this->input->post("cost_rate"),
		'return_rate' => $this->input->post("return_rate"),
		'liability_by' => $this->input->post("liability_by"),
		'remark' => $this->input->post('remark'),
		'updated_by' => $logged_in_user->user_id,
		);
		if($this->Acc_paymentgateway_fees->save($data, $id)){
			$arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}

	}


}