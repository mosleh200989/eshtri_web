<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categories Controller
 */
class Categories extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'CATEGORIES' );
	}

	/**
	 * List down the registered users
	 */
	function index() {
		
		// no delete flag
		// no publish filter
		$conds['no_publish_filter'] = 1;
		
		 $selected_shop_id = $this->session->userdata('selected_shop_id');
		 $shop_id = $selected_shop_id['shop_id'];

		 $conds['shop_id'] = $shop_id;
		// get rows count
		$this->data['rows_count'] = $this->Category->count_all_by( $conds );

		// get categories
		$this->data['categories'] = $this->Category->get_all_by( $conds , $this->pag['per_page'], $this->uri->segment( 4 ) );

		// load index logic
		parent::index();
	}

	/**
	 * Searches for the first match.
	 */
	function search() {
		

		// breadcrumb urls
		$this->data['action_title'] = get_msg( 'cat_search' );
		
		// condition with search term
		$conds = array( 'searchterm' => $this->searchterm_handler( $this->input->post( 'searchterm' )) );
		// no publish filter
		$conds['no_publish_filter'] = 1;

		$selected_shop_id = $this->session->userdata('selected_shop_id');
		$shop_id = $selected_shop_id['shop_id'];

		$conds['shop_id'] = $shop_id;


		// pagination
		$this->data['rows_count'] = $this->Category->count_all_by( $conds );

		// search data

		$this->data['categories'] = $this->Category->get_all_by( $conds, $this->pag['per_page'], $this->uri->segment( 4 ) );
		
		// load add list
		parent::search();
	}

	/**
	 * Create new one
	 */
	function add() {
        $selected_shop_id = $this->session->userdata('selected_shop_id');
        $shop_id = $selected_shop_id['shop_id'];

        $this->data['selected_shop_id'] = $shop_id;
		// breadcrumb urls
		$this->data['action_title'] = get_msg( 'cat_add' );

		// call the core add logic
		parent::add();
	}
	function import() {
		// breadcrumb urls
		$this->data['action_title'] = get_msg( 'cat_add' );
		$conds['no_publish_filter'] = 1;
		$conds['status'] = 1;
		$this->data['shops'] = $this->Shop->get_all_by_shop( $conds,$limit,$offset,$this->uri->segment( 4 ));
		if ( $this->has_data( 'shop' )) {

		$condss['no_publish_filter'] = 1;
		$condss['shop_id'] = $this->get_data( 'shop' );
		// get categories
		$categories = $this->Category->get_all_by( $condss , $this->pag['per_page'], $this->uri->segment( 4 ));
		$selected_shop_id = $this->session->userdata('selected_shop_id');
		$logged_in_user = $this->ps_auth->get_user_info();
		foreach($categories->result() as $category){
			
		
		$categorycheck=$this->Category->get_one_by(array("name" =>  $category->name, "shop_id" =>  $selected_shop_id['shop_id'] ));

		if($categorycheck->is_empty_object){
			$this->db->trans_start();
		$data = array();
		// prepare cat name
		if ($category->name) {
			$data['name'] = $category->name;
		}

		if ( $category->name_alt) {
			$data['name_alt'] = $category->name_alt;
		}

		// if 'status' is checked,
		if ($category->status) {
			$data['status'] = 1;
		} else {
			$data['status'] = 0;
		}
		
		// set timezone
		$data['shop_id'] = $selected_shop_id['shop_id'];
		$data['added_user_id'] = $logged_in_user->user_id;
			//save
		$data['added_date'] = date("Y-m-d H:i:s");
	
		$id = false;
		//save category

		$insertedData=$this->Category->save( $data, $id );
		// cover picture
		$coverimagecheck=$this->Image->get_one_by(array("img_type" =>  "category", "img_parent_id" =>  $category->id ));
		if($coverimagecheck && $coverimagecheck->img_path){
			$image = array(
				'img_parent_id'=> $data["id"],
				'img_type' => $coverimagecheck->img_type,
				'img_desc' => $coverimagecheck->img_desc,
				'img_path' => $coverimagecheck->img_path,
				'img_width'=> $coverimagecheck->img_width,
				'img_height'=> $coverimagecheck->img_height
			);
			
			$this->Image->save( $image );
		}
		// icon picture
		$iconimagecheck=$this->Image->get_one_by(array("img_type" =>  "category-icon", "img_parent_id" =>  $category->id ));
		if($iconimagecheck && $iconimagecheck->img_path){
			$iconimage = array(
				'img_parent_id'=> $data["id"],
				'img_type' => $iconimagecheck->img_type,
				'img_desc' => $iconimagecheck->img_desc,
				'img_path' => $iconimagecheck->img_path,
				'img_width'=> $iconimagecheck->img_width,
				'img_height'=> $iconimagecheck->img_height
			);
			
			$this->Image->save( $iconimage );
		}
		$this->check_trans();
		}
		}
		$this->set_flash_msg( 'success', get_msg( 'success_cat_add' ));
		}
		// call the core add logic
		parent::importform();
	}	
	
	/**
	 * Saving Logic
	 * 1) upload image
	 * 2) save category
	 * 3) save image
	 * 4) check transaction status
	 *
	 * @param      boolean  $id  The user identifier
	 */
	function save( $id = false ) {

		// start the transaction
		$this->db->trans_start();
		$logged_in_user = $this->ps_auth->get_user_info();
		
		/** 
		 * Insert Category Records 
		 */
		$data = array();
		$selected_shop_id = $this->session->userdata('selected_shop_id');
		if ( !$selected_shop_id['shop_id']) {
        	
			// set flash error message
			$this->set_flash_msg( 'error', get_msg( 'shop_id_not_vailable' ));
			return;
		}
		
		// prepare cat name
		if ( $this->has_data( 'name' )) {
			$data['name'] = $this->get_data( 'name' );
		}

		if ( $this->has_data( 'name_alt' )) {
			$data['name_alt'] = $this->get_data( 'name_alt' );
		}

		if ( $this->has_data( 'search_tag' )) {
			$data['search_tag'] = $this->get_data( 'search_tag' );
		}
		
		if ( $this->has_data( 'description' )) {
			$data['description'] = $this->get_data( 'description' );
		}
		// prepare category order
		if ( $this->has_data( 'ordering' )) {
			$data['ordering'] = $this->get_data( 'ordering' );
		}

		// prepare category order
        if ( $this->has_data( 'commission_plan' )) {
            $data['commission_plan'] = $this->get_data( 'commission_plan' );
        }

		// if 'is_discount_category' is checked,
		if ( $this->has_data( 'is_discount_category' )) {
			$data['is_discount_category'] = 1;
		} else {
			$data['is_discount_category'] = 0;
		}
		// if 'status' is checked,
		if ( $this->has_data( 'status' )) {
			$data['status'] = 1;
		} else {
			$data['status'] = 0;
		}
		
		// set timezone
		$data['shop_id'] = $selected_shop_id['shop_id'];
		$data['added_user_id'] = $logged_in_user->user_id;

		if($id == "") {
			//save
			$data['added_date'] = date("Y-m-d H:i:s");
		} else {
			//edit
			unset($data['added_date']);
			$data['updated_date'] = date("Y-m-d H:i:s");
			$data['updated_user_id'] = $logged_in_user->user_id;
		}

		//save category
		if ( ! $this->Category->save( $data, $id )) {
		// if there is an error in inserting user data,	

			// rollback the transaction
			$this->db->trans_rollback();

			// set error message
			$this->data['error'] = get_msg( 'err_model' );
			
			return;
		}

		/** 
		 * Upload Image Records 
		 */
		$catid=$id;
		if ( !$id ) {
            $catid=$data['id'];
			if ( ! $this->insert_images_icon_and_cover( $_FILES, 'category', $data['id'], "cover" )) {
				// if error in saving image

				// commit the transaction
				$this->db->trans_rollback();
				
				return;
			}


			if ( ! $this->insert_images_icon_and_cover( $_FILES, 'category-icon', $data['id'], "icon" )) {
			// if error in saving image

				// commit the transaction
				$this->db->trans_rollback();
				
				return;
			}
		
		}
        // prepare category order
//        if ( $this->has_data( 'commission_plan' )) {
//            $commission_plan = $this->get_data( 'commission_plan' );
//            $sqlquery = "SELECT * FROM `mk_products` WHERE cat_id = '".$catid."' AND shop_id='".$data['shop_id']."'";
//             $resultSet = $this->Acc_period->queryPrepare($sqlquery);
//             $products=$resultSet->result();
//             foreach($products as $product) {
//                 $dataDtls = array(
//                     'commission_plan' => $commission_plan,
//                 );
//                 $this->Product->save($dataDtls, $product->id);
//                 //$producttblnewid++;
//             }
//
//        }
//        commission_plan
		/** 
		 * Check Transactions 
		 */

		// commit the transaction
		if ( ! $this->check_trans()) {
        	
			// set flash error message
			$this->set_flash_msg( 'error', get_msg( 'err_model' ));
		} else {

			if ( $id ) {
			// if user id is not false, show success_add message
				
				$this->set_flash_msg( 'success', get_msg( 'success_cat_edit' ));
			} else {
			// if user id is false, show success_edit message

				$this->set_flash_msg( 'success', get_msg( 'success_cat_add' ));
			}
		}

		redirect( $this->module_site_url());
	}

	/**
 	* Update the existing one
	*/
	function edit( $id ) 
	{
        $selected_shop_id = $this->session->userdata('selected_shop_id');
        $shop_id = $selected_shop_id['shop_id'];

        $this->data['selected_shop_id'] = $shop_id;
		// breadcrumb urls
		$this->data['action_title'] = get_msg( 'cat_edit' );

		// load user
		$this->data['category'] = $this->Category->get_one( $id );

		// call the parent edit logic
		parent::edit( $id );

	}

	/**
	 * Delete the record
	 * 1) delete category
	 * 2) delete image from folder and table
	 * 3) check transactions
	 */
	function delete( $category_id ) {

		// start the transaction
		$this->db->trans_start();

		// check access
		$this->check_access( DEL );

		// delete categories and images
		$enable_trigger = true; 
		
		// delete categories and images
		$type = "category";
		//if ( !$this->ps_delete->delete_category( $category_id, $enable_trigger )) {
		if ( !$this->ps_delete->delete_history( $category_id, $type, $enable_trigger )) {

			// set error message
			$this->set_flash_msg( 'error', get_msg( 'err_model' ));

			// rollback
			$this->trans_rollback();

			// redirect to list view
			redirect( $this->module_site_url());
		}
			
		/**
		 * Check Transcation Status
		 */
		if ( !$this->check_trans()) {

			$this->set_flash_msg( 'error', get_msg( 'err_model' ));	
		} else {
        	
			$this->set_flash_msg( 'success', get_msg( 'success_cat_delete' ));
		}
		
		redirect( $this->module_site_url());
	}

	/**
	 * Determines if valid input.
	 *
	 * @return     boolean  True if valid input, False otherwise.
	 */
	function is_valid_input( $id = 0 ) 
	{

		$rule = 'required|callback_is_valid_name['. $id  .']';

		$this->form_validation->set_rules( 'name', get_msg( 'name' ), $rule);
		
		if ( $this->form_validation->run() == FALSE ) {
		// if there is an error in validating,

			return false;
		}

		return true;
	}

	/**
	 * Determines if valid name.
	 *
	 * @param      <type>   $name  The  name
	 * @param      integer  $id     The  identifier
	 *
	 * @return     boolean  True if valid name, False otherwise.
	 */
	function is_valid_name( $name, $id = 0, $shop_id = 0 )
	{		
		 $conds['name'] = $name;
		 $selected_shop_id = $this->session->userdata('selected_shop_id');
		 $shop_id = $selected_shop_id['shop_id'];
		 $conds['shop_id'] = $shop_id;

			if ( strtolower( $this->Category->get_one( $id )->name ) == strtolower( $name )) {
			// if the name is existing name for that user id,
				return true;
			} else if ( $this->Category->exists( ($conds ))) {
			// if the name is existed in the system,
				$this->form_validation->set_message('is_valid_name', get_msg( 'err_dup_name' ));
				return false;
			}
			return true;
	}

	/**
	 * Check category name via ajax
	 *
	 * @param      boolean  $cat_id  The cat identifier
	 */
	function ajx_exists( $id = false )
	{
		// get category name
		$cat_name = $_REQUEST['name'];

		//get shop_id
		$selected_shop_id = $this->session->userdata('selected_shop_id');
		$shop_id = $selected_shop_id['shop_id'];

		if ( $this->is_valid_name( $cat_name, $id, $shop_id )) {
		// if the category name is valid,
			
			echo "true";
		} else {
		// if invalid category name,
			
			echo "false";
		}
	}

	/**
	 * Publish the record
	 *
	 * @param      integer  $category_id  The category identifier
	 */
	function ajx_publish( $category_id = 0 )
	{
		// check access
		$this->check_access( PUBLISH );
		
		// prepare data
		$category_data = array( 'status'=> 1 );
			
		// save data
		if ( $this->Category->save( $category_data, $category_id )) {
			echo true;
		} else {
			echo false;
		}
	}
	
	/**
	 * Unpublish the records
	 *
	 * @param      integer  $category_id  The category identifier
	 */
	function ajx_unpublish( $category_id = 0 )
	{
		// check access
		$this->check_access( PUBLISH );
		
		// prepare data
		$category_data = array( 'status'=> 0 );
			
		// save data
		if ( $this->Category->save( $category_data, $category_id )) {
			echo true;
		} else {
			echo false;
		}
	}
}