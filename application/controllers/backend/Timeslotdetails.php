<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Paypal_configs Controller
 */
class Timeslotdetails extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'Time Slots' );
	}

	/**
	 * List down the registered users
	 */
	function index() {
		//Get timeslot Config Object
        $conds['no_publish_filter'] = 1;

        // get rows count
        $this->data['rows_count'] = $this->Timeslotsdetails->count_all_by( $conds );

        // get tags
        $this->data['timeslots'] = $this->Timeslotsdetails->get_all_by( $conds , $this->pag['per_page'], $this->uri->segment( 4 ) );
        $this->data['timeslotnumber'] = $this->Timeslotsnumber->get_one( "1" );
        // load index logic
        parent::tags_index();
	}

    /**
     * Searches for the first match.
     */
    function search() {

        // breadcrumb urls
        $data['action_title'] = get_msg( 'Time_Slots_search' );

        // condition with search term
        if($this->input->post('submit') != NULL ){

            $conds = array( 'searchterm' => $this->searchterm_handler( $this->input->post( 'searchterm' )));

            if($this->input->post('searchterm') != "") {
                $conds['slot_date'] = $this->input->post('searchterm');
                $this->data['searchterm'] = $this->input->post('searchterm');
                $this->session->set_userdata(array("searchterm" => $this->input->post('searchterm')));
            } else {

                $this->session->set_userdata(array("searchterm" => NULL));
            }
        } else {
            //read from session value
            if($this->session->userdata('searchterm') != NULL){
                $conds['slot_date'] = $this->session->userdata('searchterm');
                $this->data['searchterm'] = $this->session->userdata('searchterm');
            }
        }

        $this->data['rows_count'] = $this->Timeslotsdetails->count_all_by( $conds );

        $this->data['timeslots'] = $this->Timeslotsdetails->get_all_by( $conds, $this->pag['per_page'], $this->uri->segment( 4 ));

        parent::tags_search();
    }


    /**
     * Publish the record
     *
     * @param      integer  $prd_id  The product identifier
     */
    function ajx_available( $slot_id = 0 )
    {
        // check access
        $this->check_access( PUBLISH );

        // prepare data
        $prd_data = array( 'available'=> 1 );

        // save data
        if ( $this->Timeslotsdetails->save( $prd_data, $slot_id )) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    /**
     * Unpublish the records
     *
     * @param      integer  $prd_id  The category identifier
     */
    function ajx_unavailable( $slot_id = 0 )
    {
        // check access
        $this->check_access( PUBLISH );

        // prepare data
        $prd_data = array( 'available'=> 0 );

        // save data
        if ( $this->Timeslotsdetails->save( $prd_data, $slot_id )) {
            //Need to save at history table because that wallpaper no need to show on app
            echo 'true';
        } else {
            echo 'false';
        }
    }


    /**
     * Generate the time slot
     *
     * @param      integer  $prd_id  The category identifier
     */
    function ajx_date_generate()
    {
        // check access
        $this->check_access( PUBLISH );

        $start=date('Y-m-d');
        $end = date('Y-m-d', strtotime(end($dates).' +300 day'));
        $datesArr=getDatesFromRange($start, $end);
        foreach ($datesArr as $singleDate) {
            $timeslotsDetails = $this->Timeslotsdetails->get_one_by( array('slot_date' => $singleDate));
            $countfound=false;
            if($timeslotsDetails && $timeslotsDetails->id){
                $timeslotsDetailsCount=$this->db->select('*')->from('mk_time_slots_details')->where('slot_date', $singleDate)->where('order_count is NOT NULL', NULL, FALSE)->get()->row();
                if($timeslotsDetailsCount && $timeslotsDetailsCount->id){
                    $countfound=true;
                }
            }
            if(!$countfound){
                $this->Timeslotsdetails->delete_by(array('slot_date' => $singleDate));
                $timeslotsnumber = $this->Timeslotsnumber->get_one_by( array( 'id' => 1));
                $timeslots = $this->Timeslots->get_all_by(array('time_slot_number_id' => 1))->result();
                foreach ($timeslots as $singleSlot) {
                    $slotArr = array(
                        'slot_date' => $singleDate,
                        'start_time' => $singleSlot->start_time,
                        'end_time' => $singleSlot->end_time,
                        'available' => 1,
                        'accept_order_total' => $timeslotsnumber->limit_order,
                    );
                    $this->Timeslotsdetails->save($slotArr);
                }
            }


        }
        $prd_data = array( 'is_new'=> 0 );
        // save data
        if ($this->Timeslotsnumber->save( $prd_data, '1')) {
            //Need to save at history table because that wallpaper no need to show on app
            echo 'true';
        } else {
            echo 'false';
        }
    }

}
