<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users crontroller for BE_USERS table
 */
class Deliveryboycommission extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'Acc_period' );
	}

	/**
	* Load Notification Sending Form
	*/
	function index() {
		$this->data['action_title'] = get_msg('delivery_boy_commission');

		// for showing data
		if($this->input->get("showAction")){
			$conds['id'] = $this->input->get("id");
			$defaultInstance = $this->Acc_deliveryboy_charge->get_one_by($conds);
			if (!$defaultInstance->id) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$contract_type="";
			if($defaultInstance->contract_type){
				$contract_type="".get_msg($defaultInstance->contract_type)."";
			}
			$userdata = $this->User->get_one_by( array( 'user_id' => $defaultInstance->driver_id));
			$data = array('isError' => false, 'defaultInstance' => $defaultInstance, 'contractType' => $contract_type, 'userdata' => $userdata);    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		// for editing data
		if($this->input->get("editData")){
			$conds['id'] = $this->input->get("id");
			$defaultInstance = $this->Acc_deliveryboy_charge->get_one_by($conds);
			if (!$defaultInstance->id) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$userdata = $this->User->get_one_by( array( 'user_id' => $defaultInstance->driver_id));
			$data = array('isError' => false, 'defaultInstance' =>  $defaultInstance, 'userdata' => $userdata);    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		
		$this->data['contractTypeList'] = $this->Acc_coa->enumListLanguage("contract_type");
		$this->load_template('acc_delivery_boy_commission/list', $this->data, true );
	}

	
	 public function tablelist()
	 {
 
		 $iDisplayStart = $this->input->get("iDisplayStart")? intval($this->input->get("iDisplayStart")):0;
		 $iDisplayLength = $this->input->get("iDisplayLength")? intval($this->input->get("iDisplayLength")) :10;
		 $sSortDir = $this->input->get("sSortDir_0");
		 $iSortingCol = $this->input->get("iSortCol_0")? $this->input->get("iSortCol_0") : null;
		 $sSearch = $this->input->get("sSearch") ? $this->input->get("sSearch") : null;
		 if($sSearch){
			 $sSearch = "% ".$sSearch." %";
		 }
		 
		 $dataReturns = array();
	 
		 $langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
		 $sqlWhere = $this->input->get("sSearch") ? " AND acc_deliveryboy_charge.contract_type LIKE '%".$this->input->get("sSearch")."%'" : "";
		 $querySQL = "SELECT acc_deliveryboy_charge.id, acc_deliveryboy_charge.monthly_salary, acc_deliveryboy_charge.order_comission, acc_deliveryboy_charge.created_by, acc_deliveryboy_charge.date_created, core_users.* FROM acc_deliveryboy_charge LEFT JOIN core_users ON acc_deliveryboy_charge.driver_id = core_users.user_id where acc_deliveryboy_charge.id >0 ".$sqlWhere." ORDER BY acc_deliveryboy_charge.id ASC LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart."";

		 $resultSet = $this->Acc_period->queryPrepare($querySQL);
		 $totalCount = $resultSet->num_rows();
		 $serial = 0;
			 $total = 0;
			 
		 $actionList = array();
		 
			 $actionListConfirm = array();
			 $actionList[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
			 $actionList[]=array('actionName' => 'update', 'actionClass' => 'update', 'titleName' => get_msg( 'update_data'), 'iconName' => 'fa fa-pencil-square-o text-navy');
			//  $actionList[]=array('actionName' => 'confirm', 'actionClass' => 'confirm', 'titleName' => get_msg( 'Confirm_Data'), 'iconName' => 'fa fa-check-circle text-warning');
			//  $actionList[]=array('actionName' => 'delete', 'actionClass' => 'delete', 'titleName' => get_msg( 'Delete_Data'), 'iconName' => 'fa fa-trash text-danger');
			//  $actionListConfirm[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
			 
				 
		 foreach ($resultSet->result() as $row)
		 {
			 $serial++;
			 $dataReturns[]=array('DT_RowId' => $row->id, 'accessibleUrl' => $actionList,'activeStatus' => get_msg( 'Active'), 
			 0 => $serial, 
			 1 => get_msg($row->contract_type),
			 2 => $row->monthly_salary,
			 3 => $row->order_comission,
			 4 => $row->date_created,
			 5 => $row->user_name,
			 6 => $row->user_email,
			 7 => $row->user_phone,
			 8 => "");
		 }
		 
		 $gridData=array();
		 if ($totalCount == 0) {
			 $gridData=array('iTotalRecords' => 0, 'iTotalDisplayRecords' =>0, 'aaData'=> array());
			 echo json_encode($gridData);
			 exit();
		 }
 
		 $gridData=array('iTotalRecords' => $totalCount, 'iTotalDisplayRecords' =>$totalCount, 'aaData'=> $dataReturns);
		 echo json_encode($gridData);
		 exit();
	 }
	function writesubmit(){
		header('Content-Type: application/json');  
		$rules = array(
			array(
					'field' => 'contract_type',
					'label' => get_msg('contract_type'),
					'rules' => 'required'
			),
			array(
				'field' => 'delivery_boy',
				'label' => get_msg('delivery_boy'),
				'rules' => 'required'
			),
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$driver_id = $this->input->post('delivery_boy');
		if(!$driver_id){
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}

		$checkdata=$this->User->get_one_by( array( 'user_id' => $driver_id));
		if (!$checkdata->user_id)
		{
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}

		$checkdata=$this->User->get_one_by( array( 'user_id' => $driver_id, 'contract_type' => $this->input->post("contract_type")));
		if (!$checkdata->user_id)
		{
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}

		$checkFees=$this->Acc_deliveryboy_charge->get_one_by( array( 'driver_id' => $driver_id));
		if ($checkFees->id)
		{
			$arr = array('isError' => true, 'message' =>  get_msg('data_already_exist'));    
			echo json_encode( $arr );
			exit;
		}

		$logged_in_user = $this->ps_auth->get_user_info();
		$data = array(
			'contract_type' => $this->input->post("contract_type"),
			'driver_id' => $this->input->post("delivery_boy"),
			'monthly_salary' => $this->input->post("monthly_salary"),
			'order_comission' => $this->input->post("order_comission"),
			'remark' => $this->input->post('remark'),
			'created_by' => $logged_in_user->user_id,
		);

		if($this->Acc_deliveryboy_charge->save($data)){
			$comission = $this->input->post("contract_type") =='DeliveryBoy'? $this->input->post("order_comission") : $this->input->post("monthly_salary");
			$commissiondata = array('commission' => $comission);
			$this->User->save($commissiondata, $this->input->post("delivery_boy"));

			$arr = array('isError' => false, 'message' =>  get_msg('save_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}
	}
	function editsubmit(){
		header('Content-Type: application/json');  

		$rules = array(
			array(
				'field' => 'contract_type',
				'label' => get_msg('contract_type'),
				'rules' => 'required'
			),
			array(
				'field' => 'delivery_boy',
				'label' => get_msg('delivery_boy'),
				'rules' => 'required'
			),
			array(
				'field' => 'id',
				'label' => get_msg('id'),
				'rules' => 'required'
			),
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$driver_id = $this->input->post('delivery_boy');
		if(!$driver_id){
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}

		$checkdata=$this->User->get_one_by( array( 'user_id' => $driver_id));
		if (!$checkdata->user_id)
		{
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}

		$checkdata=$this->User->get_one_by( array( 'user_id' => $driver_id, 'contract_type' => $this->input->post("contract_type")));
		if (!$checkdata->user_id)
		{
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}


		$checkdata=$this->Acc_deliveryboy_charge->get_one_by( array( 'id' => $this->input->post('id')));
		if (!$checkdata->id)
		{
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}
		$logged_in_user = $this->ps_auth->get_user_info();
		$data = array(
			'contract_type' => $this->input->post("contract_type"),
			'driver_id' => $this->input->post("delivery_boy"),
			'monthly_salary' => $this->input->post("monthly_salary"),
			'order_comission' => $this->input->post("order_comission"),
			'remark' => $this->input->post('remark'),
			'updated_by' => $logged_in_user->user_id,
		);
		if($this->Acc_deliveryboy_charge->save($data, $this->input->post('id'))){
			$comission = $this->input->post("contract_type") =='DeliveryBoy'? $this->input->post("order_comission") : $this->input->post("monthly_salary");
			$commissiondata = array('commission' => $comission);
			$this->User->save($commissiondata, $this->input->post("delivery_boy"));

			$arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}

	}



}