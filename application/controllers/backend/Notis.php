<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Notis Controller
 */
class Notis extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'NOTIS' );
	}

	/**
	* Load Notification Sending Form
	*/
	function index() {
		$this->data['action_title'] = "Push Notification";
		// get rows count
		$this->data['rows_count'] = $this->Noti->count_all_by( $conds );

		// get notimsgs
		$this->data['notimsgs'] = $this->Noti->get_all_by( $conds , $this->pag['per_page'], $this->uri->segment( 4 ) );

        //$message="Right 80% discount remaining on the offer 3 days Great offers on the occasion of the National Day The Bashawat store broken prices discounts up to 80% The right and ask before you miss the offer + our offer 20% discount on delivery";
       // $message = htmlspecialchars_decode($message);

        $error_msg = "";
        $success_device_log = "";


//        $device_ids = array();
////        $device_ids[]='AM5PThB5MszlpOsP9SUtA_rfZrtokkPygHtbWFjhLtje6bLWlXr5SIi5cJdIZ37ZKe27fk88dF3TMWS8r38cBgTEuRqqXqJ-BLPLpZEKFhnmtKIOPaVM2GXQCB5Rm0XES9XAkZ4SZw9M1wTcdoq44tsbXh0ItMb1zA';
////        $device_ids[]='cCqGmP_lPKk:APA91bHdMU-fl6NtNnOPPB4gQEqclF-U8p5qfIwIJ3hrW1BCIOqC_XAAITvl0qyyKOnu8ZmLYWPYLwtwfmBOsUuJfG9s4MC97fLGx9ZWHC0fIZ7yAMOtiTr16OHvPFKo9qV47nZPMwkg';
//        $device_ids[]='eCGLfA4xH7A:APA91bGxdPZMJ0CW7SRs6tulDd-COebRktmxUwmXdINInSXOVu6tb2fadtSOGo2DFfTLoW18IcocfjBeGjEwyTMkpDmeQVSKSg-9XCsnoCuvLibZ4wkCRngIhWsxOjkiA0FTGgS1MiMc'; // mohammad ofice
//        $device_ids[]='cgOjSGU3WbI:APA91bGC9Go00FpgNbQjwiYloTu5yqxpybqb91fTElQzcmwWwZhS1FNV8dmU54nDKMk4Oh_-tRaAmihObRuwZ8ULcqX2zfOYo-SyreayTRoOwey3xDLWdvQ3RiDoxe2Py56sYs7SAnEN'; // my android
////        $status = $this->send_android_fcm( $device_ids, array( "message" => $message ));
//        $msg = array
//        (
//            'message'   => 'here is a message. message',
//            'title'     => 'This is a title. title',
//            'subtitle'  => 'This is a subtitle. subtitle',
//            'tickerText'    => 'Ticker text here...Ticker text here...Ticker text here',
//            'vibrate'   => 1,
//            'largeIcon' => 'large_icon',
//            'smallIcon' => 'small_icon'
//        );
//
//        $n = array(
//            "body"  => "great match!",
//            "title" => "NEW NOTIFICATION!",
//            "text"  => "Click me to open an Activity!",
//            "sound" => "warning"
//        );
//        $message_status = $this->send_notification($device_ids, $msg, $n);
//
//        echo "<pre>";
//        print_r($message_status);
//        echo "</pre>";

        $devices = $this->Notitoken->get_all_by(array('os_type' => 'ANDROID'))->result();

                $device_ids = array();
                if (count($devices) > 0) {
                    foreach ($devices as $device) {
                        $device_ids[] = $device->device_id;
                    }
                }

		//$this->load_template('notis/list', $this->data, true );


      //  $this->getDirContents();
	}
    function getDirContents($filter = '', &$results = array()) {
        // Source FOLDER
        $files = scandir('./uploads/missing');
        $fileCount = 1;
        foreach($files as $key => $value){
            $ext = explode(".",$value);
            $fname = $ext[0].round(microtime(true)*1000);
            $filename = $fname.".".$ext[1];
            // Source PATH
            $path = realpath('./uploads/missing/'.$value);

//            if(!is_dir($path)) {
//                unlink($path);
//            }
            if(!is_dir($path)) {
                if(empty($filter) || preg_match($filter, $path)){
                  //  echo "Image # ".$fileCount;
                    $results[] = $path;
                    // Destination PATH
                    $destination = './uploads/small/'.$value;

                       // $destination = realpath('./uploads/old/' . $value);
                       // print($destination);
                        // Change the desired "WIDTH" and "HEIGHT"
                        $newWidth = 220; // Desired WIDTH
                        $newHeight = 170; // Desired HEIGHT
                        //unlink($destination);

                    resize($path,$destination,$newWidth,$newHeight);
                    $fileCount++;
                }
            } elseif($value != "." && $value != "..") {
                //getDirContents($path, $filter, $results);
            }
        }
        echo "Completed2";
        //return $results;
    }
	/**
      * Searches for the first match.
    */
    function search() {

        // breadcrumb urls
        $this->data['action_title'] = get_msg( 'noti_search' );

        // condition with search term
        $conds = array( 'searchterm' => $this->searchterm_handler( $this->input->post( 'searchterm' )) );
        // no publish filter
        $conds['no_publish_filter'] = 1;

        // get rows count
		$this->data['rows_count'] = $this->Noti->count_all_by( $conds );

		// get notimsgs
		$this->data['notimsgs'] = $this->Noti->get_all_by( $conds , $this->pag['per_page'], $this->uri->segment( 4 ) );


        // load add list
        $this->load_template('notis/list', $this->data, true );
    }

	/**
	 * Create new one
	 */
	function add() {

		// breadcrumb urls
		$this->data['action_title'] = get_msg( 'cat_add' );

		// call the core add logic
		$this->load_template('notis/entry_form', $this->data, true );
	}

	/**
	* Sending Push Notification Message
	*/
	function push_message() {
		
		if ( $this->input->server( 'REQUEST_METHOD' ) == "POST" ) {
				
			$message = htmlspecialchars_decode($this->input->post( 'message' ));
			$title = htmlspecialchars_decode($this->input->post( 'description' ));

			$error_msg = "";
			$success_device_log = "";
            if($this->input->post( 'notifiable' ) == "all_android") {
                // Android Push Notification
//                $devices = $this->Notitoken->get_all_by(array('os_type' => 'ANDROID'))->result();
//
//                $device_ids = array();
//                if (count($devices) > 0) {
//                    foreach ($devices as $device) {
//                        $device_ids[] = $device->device_id;
//                    }
//                }
//            $device_ids[]='AM5PThB5MszlpOsP9SUtA_rfZrtokkPygHtbWFjhLtje6bLWlXr5SIi5cJdIZ37ZKe27fk88dF3TMWS8r38cBgTEuRqqXqJ-BLPLpZEKFhnmtKIOPaVM2GXQCB5Rm0XES9XAkZ4SZw9M1wTcdoq44tsbXh0ItMb1zA';
//            $device_ids[]='eAxrr_IW9Uc:APA91bHINyAhE51Kx2HWzwjfl9DwohUYmhqrc6tpShMKG8A4Xgqb3ubpRQM-g6IuHUvkw3E7DVEfd_qJKbL8nRmobG8H0QQiXnexvlR5TujmxPl891cNRkzALkvPMft-_x9DtHuoHjpH';
//            $device_ids[]='cERN7FN9m4I:APA91bEt6LwBCLn-JyvHE4JlBQZvnIz3hv9LGelIaAqZLLf7aPW_CsZ6ayAhMhiZlwWkuF1nbEjiFhqkvqk_G6TNwcZx9i421rFC2I4W3f0-LPYyAkzmM1-IUETTvmvaQ0LGahZGh0R_';
//            $device_ids[]='dsTvvUYw7eE:APA91bGAVInvbmUag6XWRGlXFNGewft9YSHFvrI6wlEC8RKYCMVxcbpuz8h9JycfRmdGcMSAqv_Dtz5pRC_7O6w85d6ELMpxz4EPZFVHwbbLv3kpDbgdOiHfVzYC4vkvaHGZM_hnZhXQ';
//            $device_ids[]='fSTCq7259hU:APA91bHDHEc_3wKubNde3WBYVJMPYSdNTxsxQs_LfJhC31-ZalqvQDKNNnPflOjtxrcGlQwbhcRw94ZnTE5g9brM9ZBC2iB3G5wLK0mjpeY7JCVzGd028qc1wRilrU498vSwyrqczUO2';
//            $device_ids[]='AM5PThBV0UEszf42Tq-vW0Alo6JzL2xbaMhHN1gZz4TnmtBv5HxKtvZL6_tiRXsigqdqp6En2X8gwxd15ap10ZMsp7Cn2eGSekmGK6-g0EQD0uJqRJHABxEe5YyOnV_yvl4KQBfroh0F8A6FTUnoQCI7uDevZ434NB6_iqfTDiXNLEEUEcVx5YY3ZWuc0n-EiGf67t_JLgEbzuNGwYq5KmYNtByRnG9NVA';
//
              $device_ids[]='eCGLfA4xH7A:APA91bGxdPZMJ0CW7SRs6tulDd-COebRktmxUwmXdINInSXOVu6tb2fadtSOGo2DFfTLoW18IcocfjBeGjEwyTMkpDmeQVSKSg-9XCsnoCuvLibZ4wkCRngIhWsxOjkiA0FTGgS1MiMc'; // mohammad ofice
             $device_ids[]='cgOjSGU3WbI:APA91bGC9Go00FpgNbQjwiYloTu5yqxpybqb91fTElQzcmwWwZhS1FNV8dmU54nDKMk4Oh_-tRaAmihObRuwZ8ULcqX2zfOYo-SyreayTRoOwey3xDLWdvQ3RiDoxe2Py56sYs7SAnEN'; // my android
             $device_ids[]='eQQALVh9umQ:APA91bFKxyGGvKcL2-W2OCNyUjEVqjaeMUnXvznhZn3hMFwg5ACSDfuGjfrOmky1NpEBZY0F2qtmrICuXnhR9-1LzwcKNGS_BNpkbhAwyhzi01f-CkM-vtykZbphSuUEXfSuJwC0I9dX'; // my android
             $device_ids[]='cXznjfvltR8:APA91bFEyvUZVw30TxlmPJxOHLCHRcnNrQaQiSKLvbJSDUOULavMnkZxmei2EIeMUGK8QfTsFVyC4NqaGOyfmEZfCeA7Qjeev8MpMdVQiFpCyoutWVYcsGzPGMGe606zTwvfmGhKTboH'; // my android
                $status = $this->send_android_fcm($device_ids, $message, $title);
                $status = 1;
                //	if ( !$status ) $error_msg .= "Fail to push all android devices <br/>";
            }
            if($this->input->post( 'notifiable' ) == "all_ios") {
                // // IOS Push Notification
                $devices = $this->Notitoken->get_all_by(array('os_type' => 'IOS'))->result();

                if (count($devices) > 0) {
                    foreach ($devices as $device) {
                        if (!$this->send_ios_apns($device->device_id, $message, $title)) {
                            $error_msg .= "Fail to push ios device named " . $device->device_id . "<br/>";
                        } else {
                            $success_device_log .= " Device Id : " . $device->device_id . "<br>";
                        }
                    }
                }
            }




       // $device_ids = array();
//        $device_ids[]='AM5PThB5MszlpOsP9SUtA_rfZrtokkPygHtbWFjhLtje6bLWlXr5SIi5cJdIZ37ZKe27fk88dF3TMWS8r38cBgTEuRqqXqJ-BLPLpZEKFhnmtKIOPaVM2GXQCB5Rm0XES9XAkZ4SZw9M1wTcdoq44tsbXh0ItMb1zA';
//        $device_ids[]='cCqGmP_lPKk:APA91bHdMU-fl6NtNnOPPB4gQEqclF-U8p5qfIwIJ3hrW1BCIOqC_XAAITvl0qyyKOnu8ZmLYWPYLwtwfmBOsUuJfG9s4MC97fLGx9ZWHC0fIZ7yAMOtiTr16OHvPFKo9qV47nZPMwkg';
      //  $device_ids[]='eCGLfA4xH7A:APA91bGxdPZMJ0CW7SRs6tulDd-COebRktmxUwmXdINInSXOVu6tb2fadtSOGo2DFfTLoW18IcocfjBeGjEwyTMkpDmeQVSKSg-9XCsnoCuvLibZ4wkCRngIhWsxOjkiA0FTGgS1MiMc'; // mohammad ofice
       // $device_ids[]='cgOjSGU3WbI:APA91bGC9Go00FpgNbQjwiYloTu5yqxpybqb91fTElQzcmwWwZhS1FNV8dmU54nDKMk4Oh_-tRaAmihObRuwZ8ULcqX2zfOYo-SyreayTRoOwey3xDLWdvQ3RiDoxe2Py56sYs7SAnEN'; // my android
//        $status = $this->send_android_fcm( $device_ids, array( "message" => $message ));
//        $msg = array
//        (
//            'message'   => 'here is a message. message',
//            'title'     => 'This is a title. title',
//            'subtitle'  => 'This is a subtitle. subtitle',
//            'tickerText'    => 'Ticker text here...Ticker text here...Ticker text here',
//            'vibrate'   => 1,
//            'largeIcon' => 'large_icon',
//            'smallIcon' => 'small_icon'
//        );
//
//        $n = array(
//            "body"  => "great match!",
//            "title" => "NEW NOTIFICATION!",
//            "text"  => "Click me to open an Activity!",
//            "sound" => "warning"
//        );
//        $message_status = $this->send_notification($device_ids, $msg, $n);

			// start the transaction
		$this->db->trans_start();
		$logged_in_user = $this->ps_auth->get_user_info();
		/** 
		 * Insert Notification Records 
		 */
		$data = array();

		// prepare noti name zawgyi
		if ( $this->has_data( 'message' )) {
			$data['message'] = $this->get_data( 'message' );
		}

		// prepare description zawgyi
		if ( $this->has_data( 'description' )) {
			$data['description'] = $this->get_data( 'description' );
		}

        // prepare notification_type
        if ( $this->has_data( 'notification_type' )) {
            $data['notification_type'] = $this->get_data( 'notification_type' );
        }
        // prepare notifiable
        if ( $this->has_data( 'notifiable' )) {
            $data['notifiable'] = $this->get_data( 'notifiable' );
        }
        // prepare notification_type
        if ( $this->has_data( 'notification_type' )) {
            $data['notification_type'] = $this->get_data( 'notification_type' );
        }

		$data['added_user_id'] = $logged_in_user->user_id;
		if($id == "") {
			//save
			$data['added_date'] = date("Y-m-d H:i:s");
		  } 
		// save notification
		if ( ! $this->Noti->save( $data, $id )) {
		// if there is an error in inserting user data,	

			// rollback the transaction
			$this->db->trans_rollback();

			// set error message
			$this->data['error'] = get_msg( 'err_model' );
			
			return;
		}
		/** 
		 * Upload Image Records 
		*/
	
		if ( !$id ) {
		// if id is false, this is adding new record

			if ( ! $this->insert_images( $_FILES, 'noti', $data['id'] )) {
		
			}

			
		}
			

			// commit the transaction
		if ( ! $this->check_trans()) {
        	
			// set flash error message
			$this->set_flash_msg( 'error', get_msg( 'err_model' ));
		} else {

			if ( $id ) {
			// if user id is not false, show success_add message
				
				//$this->set_flash_msg( 'success', get_msg( 'success_cat_edit' ));
			} else {
			// if user id is false, show success_edit message

				$this->set_flash_msg( 'success', get_msg( 'success_noti_add' ));
			}
		}

		}

		// $this->data['action_title'] = "Push Notification";
		redirect( $this->module_site_url());
	}

	/**
	* Sending Message From FCM For Android
	*/
	function send_android_fcm( $registatoin_ids, $message, $title)
    {
    	//Google cloud messaging GCM-API url
//    	$url = 'https://fcm.googleapis.com/fcm/send';
//    	$fields = array(
//    	    'registration_ids' => $registatoin_ids,
//    	    'data' => $message,
//    	);
//    	// Update your Google Cloud Messaging API Key
//    	//define("GOOGLE_API_KEY", "AIzaSyByuOIRznlsNcXh2OpofSb1uL4rC3r8xpY");
//    	define("GOOGLE_API_KEY", $this->Backend_config->get_one('be1')->fcm_api_key);
//        // 'Authorization: key=' . GOOGLE_API_KEY,
//    	$headers = array(
//            'Authorization: key=' . GOOGLE_API_KEY,
//    	    'Content-Type: application/json'
//    	);
//    	$ch = curl_init();
//    	curl_setopt($ch, CURLOPT_URL, $url);
//    	curl_setopt($ch, CURLOPT_POST, true);
//    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//    	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
//    	$result = curl_exec($ch);
//    	if ($result === FALSE) {
//    	    die('Curl failed: ' . curl_error($ch));
//    	}
//    	curl_close($ch);
//    	return $result;

        $n = array(
            "body"  => $message,
            "title" => $title,
            "text"  => "Click me to open an Activity!",
            "sound" => "warning"
        );
        $message = array
        (
            'message'   => $message,
            'title'     => $title,
            'subtitle'  => 'This is a subtitle. subtitle',
            'tickerText'    => 'Ticker text here...Ticker text here...Ticker text here',
            'vibrate'   => 1,
            'largeIcon' => 'large_icon',
            'smallIcon' => 'small_icon'
        );
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            /*'to'             => $tokens,*/
            'registration_ids' => $registatoin_ids,
            'priority'     => "high",
            'notification' => $n,
            'data'         => $message
        );

        //var_dump($fields);
$api2='AIzaSyAjp5OfeOhMfejWHUjy0TVtdwhTlc_NYxs';
$api1='AAAA0IN9-xk:APA91bEmxmQNs6_8FEEPZSmnI9j0Wz7p4piEoyvuxZDMCLRDGhLXsaCAea9uJcbtOz6_XsvYvAr5rkPq374vHXwhb_JebzbIDw-Z9TGbVthNcgSFykG1xNLVhgz989MpDtBF3ne12KoN';
        $headers = array(
            'Authorization:key = '.$api1.'',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
          //  curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }

    function send_notification ($tokens, $message = "", $n)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            /*'to'             => $tokens,*/
            'registration_ids' => $tokens,
            'priority'     => "high",
            'notification' => $n,
            'data'         => $message
        );

        //var_dump($fields);

        $headers = array(
            'Authorization:key = AAAA0IN9-xk:APA91bEmxmQNs6_8FEEPZSmnI9j0Wz7p4piEoyvuxZDMCLRDGhLXsaCAea9uJcbtOz6_XsvYvAr5rkPq374vHXwhb_JebzbIDw-Z9TGbVthNcgSFykG1xNLVhgz989MpDtBF3ne12KoN',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
    /**
	* Sending Message From APNS For iOS
	*/
    function send_ios_apns($tokenId, $message, $title)
	{
		ini_set('display_errors','On'); 
		//error_reporting(E_ALL);
		// Change 1 : No braces and no spaces
		$deviceToken= $tokenId;
		//'fe2df8f5200b3eb133d84f73cc3ea4b9065b420f476d53ad214472359dfa3e70'; 
		// Change 2 : If any
		$passphrase = 'teamps'; 
		$ctx = stream_context_create();
		// Change 3 : APNS Cert File name and location.
		stream_context_set_option($ctx, 'ssl', 'local_cert', realpath('assets').'/apns/psnews_apns_cert.pem'); 
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		// Open a connection to the APNS server
		$fp = stream_socket_client( 
		    'ssl://gateway.sandbox.push.apple.com:2195', $err,
		    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
		    exit("Failed to connect: $err $errstr" . PHP_EOL);
		// Create the payload body
		$body['aps'] = array(
		    'alert' => $message,
		    'sound' => 'default'
		    );
		// Encode the payload as JSON
		$payload = json_encode($body);
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		// Close the connection to the server
		fclose($fp);
		if (!$result) 
		    return false;

		return true;
	}

	/**
	 * Delete the record
	 * 1) delete notification
	 * 2) delete image from folder and table
	 * 3) check transactions
	 */
	function delete( $id ) {

		// start the transaction
		$this->db->trans_start();

		// check access
		$this->check_access( DEL );
		
		// delete categories and images
		if ( !$this->ps_delete->delete_noti( $id )) {

			// set error message
			$this->set_flash_msg( 'error', get_msg( 'err_model' ));

			// rollback
			$this->trans_rollback();

			// redirect to list view
			redirect( $this->module_site_url());
		}
			
		/**
		 * Check Transcation Status
		 */
		if ( !$this->check_trans()) {

			$this->set_flash_msg( 'error', get_msg( 'err_model' ));	
		} else {
        	
			$this->set_flash_msg( 'success', get_msg( 'success_noti_delete' ));
		}
		
		redirect( $this->module_site_url());
	}

	/**
	 * Determines if valid input.
	 *
	 * @return     boolean  True if valid input, False otherwise.
	 */
	function is_valid_input( $id = 0 ) 
	{

		$rule = 'required|callback_is_valid_name['. $id  .']';

		$this->form_validation->set_rules( 'message', get_msg( 'message' ), $rule);
		
		if ( $this->form_validation->run() == FALSE ) {
		// if there is an error in validating,

			return false;
		}

		return true;
	}

	/**
	 * Determines if valid name.
	 *
	 * @param      <type>   $name  The  name
	 * @param      integer  $id     The  identifier
	 *
	 * @return     boolean  True if valid name, False otherwise.
	 */
	function is_valid_name( $name, $id = 0 )
	{		
		 $conds['message'] = $name;
			if ( strtolower( $this->Noti->get_one( $id )->message ) == strtolower( $name )) {
			// if the name is existing name for that user id,
				return true;
			} else if ( $this->Noti->exists( ($conds ))) {
			// if the name is existed in the system,
				$this->form_validation->set_message('is_valid_name', get_msg( 'err_dup_name' ));
				return false;
			}
			return true;
	}

	/**
	 * Check category name via ajax
	 *
	 * @param      boolean  $cat_id  The cat identifier
	 */
	function ajx_exists( $id = false )
	{
		// get message
		$name = $_REQUEST['message'];

		if ( $this->is_valid_name( $name, $id )) {
		// if the message is valid,
			
			echo "true";
		} else {
		// if invalid message
			
			echo "false";
		}
	}

}