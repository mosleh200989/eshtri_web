<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Paypal_configs Controller
 */
class Timeslot extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'Time Slots' );
	}

	/**
	 * List down the registered users
	 */
	function add( $id = "1" ) {
		//Get timeslot Config Object
		if ( $this->is_POST()) {
		// if the method is post

			// server side validation
			if ( $this->is_valid_input()) {

				// save user info
				$this->save( "1" );
			}
		}

		$this->data['timeslotnumber'] = $this->Timeslotsnumber->get_one( "1" );
		$this->data['timeslots'] = $this->Timeslots->get_all_by(array('time_slot_number_id' =>"1"))->result();
		$this->load_template( 'timeslot/entry_form',$this->data, true );

	}
	
	/**
	 * Saving Logic
	 * 1) upload image
	 * 2) save category
	 * 3) save image
	 * 4) check transaction status
	 *
	 * @param      boolean  $id  The user identifier
	 */

	function save( $id = false ) {
		// start the transaction
		$this->db->trans_start();
		$logged_in_user = $this->ps_auth->get_user_info();
		
		/** 
		 * Insert Tag Records 
		 */
		$data = array();
        $data['is_new'] = 1;
		// prepare number_slots
		if ( $this->has_data( 'number_slots' )) {
			$data['number_slots'] = $this->get_data( 'number_slots' );
		}		

		// prepare limit_order
		if ( $this->has_data( 'limit_order' )) {
			$data['limit_order'] = $this->get_data( 'limit_order' );
		}

        $this->ps_delete->delete_time_slots('1');
        $slot_counter_total = $this->get_data( 'number_slots' );
        for($j=1; $j<=$slot_counter_total; $j++) {
            $start_time = $this->get_data('start_time' . $j);
            $end_time = $this->get_data('end_time' . $j);

            sleep(1);

            if( $start_time != "" || $end_time != "" ) {
                $slot_data['time_slot_number_id'] = 1;
                $slot_data['start_time'] = $start_time;
                $slot_data['end_time'] = $end_time;
                $slot_data['added_date'] = date("Y-m-d H:i:s");
                $slot_data['added_user_id'] = $logged_in_user->user_id;

                $this->Timeslots->save($slot_data);
            }
        }
		//save Tag
		if ( ! $this->Timeslotsnumber->save( $data, $id )) {
		// if there is an error in inserting user data,	

			// rollback the transaction
			$this->db->trans_rollback();

			// set error message
			$this->data['error'] = get_msg( 'err_model' );
			
			return;
		}

		/** 
		 * Check Transactions 
		 */

		// commit the transaction
		if ( ! $this->check_trans()) {
        	
			// set flash error message
			$this->set_flash_msg( 'error', get_msg( 'err_model' ));
		} else {

			if ( $id ) {
			// if user id is not false, show success_add message
				
				$this->set_flash_msg( 'success', get_msg( 'success_version_edit' ));
			} else {
			// if user id is false, show success_edit message

				$this->set_flash_msg( 'success', get_msg( 'success_version_add' ));
			}
		}

		redirect( site_url('/admin/timeslot/add') );
	}



	/**
	 * Determines if valid input.
	 *
	 * @return     boolean  True if valid input, False otherwise.
	 */
	// function is_valid_input( $id = 0 ) 
	// {
		
	// 	return true;
	// }

	function is_valid_input( $id = 0 ) {
		
		$this->form_validation->set_rules( 'number_slots', get_msg( 'number_slots_no_required' ), 'required');

		if ( $this->form_validation->run() == FALSE ) {
		// if there is an error in validating,
			//echo "error"; die;
			return false;
		}

		return true;
	}

	
	
}
