<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Transactions Controller
 */
class Orders extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'TRANSACTIONS' );
		$this->load->library('pdf');
		// load the mail library
		$this->load->library( 'PS_Mail' );
	}

	/**
	 * List down the registered users
	 */
	function index($shop_id = 0) {
        $langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
        $this->data['action_title'] = "Orders";
		$shop_countSQL = "SELECT COUNT(DISTINCT(`shop_id`)) AS shop, 
            (SELECT COUNT(DISTINCT(`user_id`)) FROM mk_transactions_header) AS customer,
            (SELECT COUNT(DISTINCT(`id`)) FROM mk_transactions_header) AS orders,
            (SELECT COUNT(payment_method) FROM mk_transactions_header WHERE payment_method='paytabs') AS paytabs,
            (SELECT COUNT(payment_method) FROM mk_transactions_header WHERE payment_method='POSTerminal') AS POSTerminal,
            (SELECT COUNT(payment_method) FROM mk_transactions_header WHERE payment_method='COD') AS COD,
            (SELECT COUNT(trans_status_id) FROM mk_transactions_header WHERE trans_status_id=1) AS orderreceive,
            (SELECT COUNT(trans_status_id) FROM mk_transactions_header WHERE trans_status_id=2) AS orderpreparing,
            (SELECT COUNT(trans_status_id) FROM mk_transactions_header WHERE trans_status_id=3) AS orderready,
            (SELECT COUNT(trans_status_id) FROM mk_transactions_header WHERE trans_status_id=4) AS orderontheway,
            (SELECT COUNT(trans_status_id) FROM mk_transactions_header WHERE trans_status_id=5) AS orderdelivered,
            (SELECT COUNT(reject_status) FROM mk_transactions_header WHERE reject_status=1) AS orderrejected
            FROM mk_transactions_header";
        $shop_countResultSet = $this->Acc_period->queryPrepare($shop_countSQL);
		$this->data['summeryReport'] = $shop_countResultSet->row();

        if ($this->input->get("producSearch")) {
            $productsDropdownList = $this->Acc_coa->productsDropdownList($this->input->get());
            echo json_encode($productsDropdownList);
            exit();
        }

        if ($this->input->get("copyProductData")) {
            $attributesDropdownList = $this->importProduct($this->input->get());
            $dataa=array('isError' => false, "message"=>"added");
            echo json_encode($dataa);
            exit();
        }

        if ($this->input->get("createOrderData")) {
            $attributesDropdownList = $this->importProduct($this->input->get());
            $dataa=array('isError' => false, "message"=>"added");
            echo json_encode($dataa);
            exit();
        }

        $this->load_template('orders/list', $this->data, true );
	}

    function importProduct($params=array()){
        $current_date_time = date("Y-m-d H:i:s");
        $shopid=$params['shop_id'];
        $trans_header_id=$params['order_id'];
        $product_id=$params['productid'];

        if($trans_header_id && $product_id && $shopid){
            $product_info=$this->Product->get_one( $product_id );
            $shop=$this->Shop->get_one( $product_id );
            $logged_in_user = $this->ps_auth->get_user_info();

            if($product_info->is_available==1 && $product_info->status==1 && $shopid == $product_info->shop_id) {
                $categoryid = $product_info->cat_id;
                $trans_detail['shop_id'] = $product_info->shop_id;
                $trans_detail['product_id'] = $product_id;
                $trans_detail['product_category_id'] = $categoryid;
                $trans_detail['product_name'] = $product_info->name;
                $trans_detail['product_photo'] = $product_info->thumbnail;
                $trans_detail['original_price'] = $product_info->original_price;

                $trans_detail['price'] = $product_info->unit_price;
                $trans_detail['qty'] = 1;
                $trans_detail['transactions_header_id'] = $trans_header_id;
                $trans_detail['added_date'] = $current_date_time;
                $trans_detail['added_user_id'] = $logged_in_user->user_id;
                $trans_detail['updated_date'] = $current_date_time;
                $trans_detail['currency_short_form'] = $shop->currency_short_form;
                $trans_detail['currency_symbol'] = $shop->currency_symbol;
                $trans_detail['product_unit'] = $product_info->product_unit;
                $trans_detail['product_measurement'] = $product_info->product_measurement;

                $this->Transactiondetail->save($trans_detail);

                //Need to update transaction count table
                $prd_cat_id = $this->Product->get_one($product_id)->cat_id;
                $prd_sub_cat_id = $this->Product->get_one($product_id)->sub_cat_id;

                $trans_count['product_id'] = $product_id;
                $trans_count['shop_id'] = $product_info->shop_id;
                $trans_count['cat_id'] = $prd_cat_id;
                $trans_count['sub_cat_id'] = $prd_sub_cat_id;
                $trans_count['user_id'] = $logged_in_user->user_id;

              $this->Transactioncount->save($trans_count);
            }
        }
    }

    public function order_list()
    {

        $iDisplayStart = $this->input->get("iDisplayStart")? intval($this->input->get("iDisplayStart")):0;
        $iDisplayLength = $this->input->get("iDisplayLength")? intval($this->input->get("iDisplayLength")) :10;
        $sSortDir = $this->input->get("sSortDir_0");
        $iSortingCol = $this->input->get("iSortCol_0")? $this->input->get("iSortCol_0") : null;
        $sSearch = $this->input->get("sSearch") ? $this->input->get("sSearch") : null;
        if($sSearch){
            $sSearch = "% ".$sSearch." %";
        }

        $dataReturns = array();

        $langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
        $sqlWhere = "WHERE mst.id !=0 ";
        $sqlWhere .= ($this->input->get("orderStartDate") && $this->input->get("orderEndDate")) ? ((" AND mst.added_date >= '".$this->input->get("orderStartDate")."'").(" AND mst.added_date <= '".$this->input->get("orderEndDate")."'")) : "";
        $sqlWhere .= ($this->input->get("deliveryStartDate") && $this->input->get("deliveryEndDate")) ? ((" AND mst.delivery_date >= '".$this->input->get("deliveryStartDate")."'").(" AND mst.delivery_date <= '".$this->input->get("deliveryEndDate")."'")) : "";
        $sqlWhere .= $this->input->get("invoiceNo") ? " AND mst.id='" .$this->input->get("invoiceNo"). "'" : "";
        $sqlWhere .= $this->input->get("transStatus") ? " AND mst.trans_status_id='" .$this->input->get("transStatus"). "'" : "";
        $sqlWhere .= $this->input->get("paymentMethod") ? " AND mst.payment_method='" .$this->input->get("paymentMethod"). "'" : "";
        $sqlWhere .= $this->input->get("paymentStatus") =='0' || $this->input->get("paymentStatus") =='1'? " AND mst.payment_status='" .$this->input->get("paymentStatus"). "'" : "";
        $sqlWhere .= $this->input->get("rejection") =='0' || $this->input->get("rejection") =='1'? " AND mst.reject_status='" .$this->input->get("rejection"). "'" : "";
        $sqlWhere .= $this->input->get("delivery_boy") ? " AND mst.assign_to='" .$this->input->get("delivery_boy"). "'" : "";
        $sqlWhere .= $this->input->get("shop") ? " AND mst.shop_id='" .$this->input->get("shop"). "'" : "";
        $sqlWhere .= $this->input->get("customerMobile") ? " AND mst.contact_phone LIKE '%" .$this->input->get("customerMobile"). "%'" : "";

        $sqlWhere .= $this->input->get("sSearch") ? " AND (mst.contact_name LIKE '%".$this->input->get("sSearch")."%' OR mst.contact_phone LIKE '%".$this->input->get("sSearch")."%' OR mst.payment_reference LIKE '%".$this->input->get("sSearch")."%' OR mst.shipping_address_1 LIKE '%".$this->input->get("sSearch")."%' OR mst.shipping_city LIKE '%".$this->input->get("sSearch")."%' OR mst.shipping_phone LIKE '%".$this->input->get("sSearch")."%' OR mst.delivery_date LIKE '%".$this->input->get("sSearch")."%' OR mst.payment_method LIKE '%".$this->input->get("sSearch")."%')" : "";
        $querySQL = "SELECT mst.id, mst.user_id, mst.shop_id, mst.sub_total_amount, mst.discount_amount, mst.coupon_discount_amount, mst.tax_amount, mst.tax_percent, mst.shipping_amount, mst.shipping_tax_percent, mst.shipping_method_amount, mst.shipping_method_name, mst.balance_amount, mst.total_item_amount, mst.total_item_count, mst.contact_name, mst.contact_phone, mst.payment_method, mst.payment_status, mst.added_date, mst.trans_status_id, mst.shipping_first_name, mst.shipping_last_name, mst.shipping_company, mst.shipping_address_1, mst.shipping_address_2, mst.shipping_state, mst.shipping_city, mst.shipping_phone, mst.assign_to, mst.delivery_time_from, mst.delivery_time_to, mst.time_slot_id, mst.delivery_date, mst.reject_status, mst.rejected_by, transStatus.title as transStatusTitle, usr.user_name as deliveryBoyName, usr.user_phone as deliveryBoyPhone, shop.name as shopName FROM mk_transactions_header AS mst LEFT JOIN mk_transactions_status transStatus ON mst.trans_status_id = transStatus.id LEFT JOIN core_users usr ON usr.user_id = mst.assign_to LEFT JOIN mk_shops shop ON shop.id = mst.shop_id ".$sqlWhere." ORDER BY mst.id DESC ";
        $resultSet = $this->Acc_period->queryPrepare($querySQL);
        $totalCount =$resultSet->num_rows();

        $querySQL .= " LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart."";

//        echo $querySQL;
//die();
        $resultSet = $this->Acc_period->queryPrepare($querySQL);
        $serial = 0;

        $actionList = array();

        $actionListConfirm = array();
//        $actionList[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
//        $actionList[]=array('actionName' => 'update', 'actionClass' => 'update', 'titleName' => get_msg( 'update_data'), 'iconName' => 'fa fa-pencil-square-o text-navy');
//        $actionList[]=array('actionName' => 'confirm', 'actionClass' => 'confirm', 'titleName' => get_msg( 'Confirm_Data'), 'iconName' => 'fa fa-check-circle text-warning');
//        $actionList[]=array('actionName' => 'delete', 'actionClass' => 'delete', 'titleName' => get_msg( 'Delete_Data'), 'iconName' => 'fa fa-trash text-danger');
//        $actionListConfirm[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
//        $actionListConfirm[]=array('actionName' => 'index', 'actionClass' => 'print', 'titleName' => get_msg( 'Print_Report'), 'iconName' => 'fa fa-print text-navy');


        foreach ($resultSet->result() as $row)
        {
            $serial++;
            $statusLabel="";
            if ($row->trans_status_id == 1) {
                $statusLabel='<span class="badge badge-danger">'.get_msg(''.$row->transStatusTitle.'').'</span>';
            } else if ($row->trans_status_id == 2) {
                $statusLabel='<span class="badge badge-info">'.get_msg(''.$row->transStatusTitle.'').'</span>';
             } else if ($row->trans_status_id == 3) {
                $statusLabel='<span class="badge badge-success">'.get_msg(''.$row->transStatusTitle.'').'</span>';
             } else {
                $statusLabel='<span class="badge badge-primary">'.get_msg(''.$row->transStatusTitle.'').'</span>';
             }

            $payment_status='';
            $reject_status='';
            if($row->reject_status ==1){
                $reject_status=' - <span class="badge badge-danger">'.get_msg('rejected').'</span>';
                $statusLabel=' - <span class="badge badge-danger">'.get_msg('rejected').'</span>';
            }

            if($row->payment_status ==1){
                $payment_status='<span class="btn btn-sm btn-success">'.get_msg('paid').$reject_status.'</span>';
            }else{
                $payment_status='<span class="badge badge-danger">'.get_msg('unpaid').$reject_status.'</span>';
            }


            $assign='';
            if(!$row->assign_to){
                $assign='<a class="pull-left btn btn-sm btn-success" href="'.site_url("admin/orders/assign/" . $row->id).'">'.get_msg('assign').'</a>';
             }else if($row->trans_status_id !=5){
                $assign='<a class="pull-left btn btn-sm btn-warning" href="'.site_url("admin/orders/assign/" . $row->id).'">'.get_msg('re_assign').'</a>';
             }else if ($row->trans_status_id ==5 && $this->session->userdata( 'user_id' ) == "usr75023528330eed4cc55f0e1bdd463cfe") {
                $assign='<a class="pull-left btn btn-sm btn-warning" href="'.site_url("admin/orders/assign/" . $row->id).'">'.get_msg('re_assign').'</a>';
            }
            if($row->reject_status ==1){
                $assign='';
            }
            $accounting_status='';
            if($row->trans_status_id==5 && $row->accounting_status==0) {
                $accounting_status='<span class="col-md-6 no-padding"><a href="" referenceid="'.$row->id.'" class="confirm-reference" data-title="Confirm_Data"><i class="fa fa-check-circle text-warning"></i></a>&nbsp;</span>';
            }
            $token="?order_id=".$row->id."&mobile_no=".$row->contact_phone."&lang=ar";
            $detail="";
            if($row->trans_status_id == 1){
                $detail='<a style="margin-left:10px; margin-right:10px" class="pull-left btn btn-sm btn-warning" target="_blank" href="'.site_url("shareordertoconfirm" . $token).'"><i class="fa fa-share" aria-hidden="true"></i></a>&nbsp;';
            }
            $detail.='&nbsp;<a class="pull-right btn btn-sm btn-primary" target="_blank" href="'.site_url("admin/orders/detail/" . $row->id).'">'.get_msg('details').'</a>';
            $orderedit='';
            if($row->trans_status_id !=5){
                $orderedit='<a class="pull-left btn btn-sm btn-warning" target="_blank" href="'.site_url("admin/orders/orderedit/" . $row->id).'">'.get_msg('edit').'</a>';
            }else if ($row->trans_status_id ==5 && $this->session->userdata( 'user_id' ) == "usr75023528330eed4cc55f0e1bdd463cfe") {
                $orderedit='<a class="pull-left btn btn-sm btn-warning" target="_blank" href="'.site_url("admin/orders/orderedit/" . $row->id).'">'.get_msg('edit').'</a>';
            }

            $printDetailsPdf='<a class="pull-right btn btn-sm btn-primary" target="_blank" href="'.site_url("admin/orders/printDetailsPdf/" . $row->id).'"><i class="fa fa-print" aria-hidden="true"></i></a>';
            $shop_total=($row->sub_total_amount + $row->tax_amount);
            $dataReturns[]=array(
                'DT_RowId' => $row->id,
                'DataRejected' => (($row->reject_status ==1) ? true: false),
                'accessibleUrl' => (($row->confirm_status == "Draft") ? $actionList : $actionListConfirm),
                'activeStatus' => get_msg( 'Active'),
                0 => $serial,
                1 => sprintf("%07d", $row->id),
                2 =>  '<a class="shopname" href="'.site_url("admin/dashboard/index/" . $row->shop_id).'">'.$row->shopName.'</a>',
                3 => $row->contact_name . "( ".get_msg('contact').": " . $row->contact_phone . " )",
                4 => $row->shipping_city,
                5 => $statusLabel,
                6 => number_format($shop_total,2),
                7 => $row->added_date,
                8 => $row->delivery_date? $row->delivery_date." (".$row->delivery_time_from." - ".$row->delivery_time_to.")" :"",
                9 => get_msg(''.$row->payment_method.''),
                10 => $payment_status,
                11 => $row->assign_to ? '<a class="drivingname" target="_blank" href="'.site_url("admin/delivery_boys/delivery_boys_details/" . $row->assign_to).'">'.$row->deliveryBoyName.'-'.$row->deliveryBoyPhone.'</a>' :"",
                12 => $accounting_status,
                13 => $detail,
                14 => $printDetailsPdf,
                15 => $orderedit
            );
        }

        $gridData=array();
        if ($totalCount == 0) {
            $gridData=array('iTotalRecords' => 0, 'iTotalDisplayRecords' =>0, 'aaData'=> array());
            echo json_encode($gridData);
            exit();
        }

        $gridData=array('iTotalRecords' => $totalCount, 'iTotalDisplayRecords' =>$totalCount, 'aaData'=> $dataReturns);
        echo json_encode($gridData);
        exit();
    }
	/**
	 * Searches for the first match.
	 */
	function search($status_id = 0) {
		

		// breadcrumb urls
		$this->data['action_title'] = get_msg( 'trans_search' );
		
		// condition with search term
		if($this->input->post('submit') != NULL ){

			$conds = array( 'searchterm' => $this->searchterm_handler( $this->input->post( 'searchterm' )));

			// condition passing date
			$conds['date'] = $this->input->post( 'date' );

			// no publish filter
			$conds['no_publish_filter'] = 1;

			if($this->input->post('searchterm') != "") {
				$conds['searchterm'] = $this->input->post('searchterm');
				$this->data['searchterm'] = $this->input->post('searchterm');
				$this->session->set_userdata(array("searchterm" => $this->input->post('searchterm')));
			} else {
				
				$this->session->set_userdata(array("searchterm" => NULL));
			}

			if($this->input->post('date') != "") {
				$conds['date'] = $this->input->post('date');
				$this->data['date'] = $this->input->post('date');
				$this->session->set_userdata(array("date" => $this->input->post('date')));
			} else {
				
				$this->session->set_userdata(array("date" => NULL));
			}
		
			if($this->input->post('trans_status_id') != "") {
				$conds['trans_status_id'] = $this->input->post('trans_status_id');
				$this->data['trans_status_id'] = $this->input->post('trans_status_id');
				$this->session->set_userdata(array("trans_status_id" => $this->input->post('trans_status_id')));
			} else {
				
				$this->session->set_userdata(array("trans_status_id" => NULL));
			}
		
			if($this->input->post('shop_id') != "" && $this->input->post('shop_id') != "0") {
				$conds['shop_id'] = $this->input->post('shop_id');
				$this->data['shop_id'] = $this->input->post('shop_id');
				$this->session->set_userdata(array("shop_id" => $this->input->post('shop_id')));
			} else {
				
				$this->session->set_userdata(array("shop_id" => NULL));
			}
		
		} else {
			//$conds['no_publish_filter'] = 1;
			
			if($this->session->userdata('trans_status_id') != NULL){
				
				$this->data['trans_status_id'] = $this->session->userdata('trans_status_id');
				$conds['trans_status_id'] = $this->session->userdata('trans_status_id');

			} 

			if($this->session->userdata('shop_id') != NULL){
				
				$this->data['shop_id'] = $this->session->userdata('shop_id');
				$conds['shop_id'] = $this->session->userdata('shop_id');

			}
			//read from session value
			if($this->session->userdata('searchterm') != NULL){
				//echo "7";die;
				$conds['searchterm'] = $this->session->userdata('searchterm');
				$this->data['searchterm'] = $this->session->userdata('searchterm');
			}
			if($this->session->userdata('date') != NULL){
				$conds['date'] = $this->session->userdata('date');
				$this->data['date'] = $this->session->userdata('date');
			}

		
		}
			// $selected_shop_id = $this->session->userdata('selected_shop_id');
			// $shop_id = $selected_shop_id['shop_id'];

			// $conds['shop_id'] = $shop_id;
			// pagination
			$this->data['rows_count'] = $this->Transactionheader->count_all_by( $conds );

			// search data
			$this->data['transactions'] = $this->Transactionheader->get_all_by( $conds, $this->pag['per_page'], $this->uri->segment( 4 ) );

			//$this->data['selected_shop_id'] = $shop_id;
			parent::orders_index();
			// load add list
			//$this->load_template('orders/list', $this->data, true );
			//parent::search();
		}

	function assign($id='')
	{
		$conds = array( 'register_role_id' => 5 );
		$conds['status'] = 1;
		$conds['is_banned'] = 0;
		// get rows count
		$this->data['rows_count'] = $this->User->count_all_by($conds);

		// get users
		$this->data['users'] = $this->User->get_all_by($conds);
		$this->data['transaction'] = $this->Transactionheader->get_one( $id );
		// load index logic
		$this->load_template('orders/assign', $this->data, true );
	}	

	function orderedit($id='')
	{
		
		$conds['id'] = $id;
		// get rows count
		$this->data['rows_count'] = $this->Transactionheader->count_all_by($conds);

		// get users
		$this->data['transaction'] = $this->Transactionheader->get_one( $id );
		// load index logic
		$this->load_template('orders/orderedit', $this->data, true );
	}

     function createorder($id='')
    {
        
        $conds['user_id'] = $id;
        $this->data['user'] = $this->User->get_one_by($conds);
        $this->data['users'] = $this->User->get_all_by($conds);
        // get rows count
        $this->data['rows_count'] = $this->Transactionheader->count_all_by($conds);

        // get users
        $this->data['transaction'] = $this->Transactionheader->get_one( $id );
        $this->data['shops'] = $this->Shop->get_all_by(array());

        $this->data['userAddress'] = $this->UserAddress->get_one_by( array( 'user_id' => $id, 'is_default' => 1));
        $today=date('Y-m-d');
        $nextdayDate = date('Y-m-d', strtotime($today.' +1 day'));
        $this->data['nextday'] = date('Y-m-d', strtotime($today.' +1 day'));

        $this->data['timeslotsdetails'] = $this->Timeslotsdetails->get_all_by(array('slot_date' => $nextdayDate));
        // $delivery_date          = $timeslotsdetails->slot_date;
        // $delivery_time_from     = $timeslotsdetails->start_time;
        // $delivery_time_to       = $timeslotsdetails->end_time;

        // load index logic
        $this->load_template('orders/createorder', $this->data, true );
    }


    public function creaorderubmit(){
        header('Content-Type: application/json');  
        $rules = array(
            array(
                    'field' => 'shipping_first_name',
                    'label' => get_msg('name'),
                    'rules' => 'required'
            ),array(
                    'field' => 'shipping_address_1',
                    'label' => get_msg('Address'),
                    'rules' => 'required'
            ),array(
                    'field' => 'shipping_phone',
                    'label' => get_msg('Phone'),
                    'rules' => 'required'
            ),array(
                    'field' => 'shop_id',
                    'label' => get_msg('shop'),
                    'rules' => 'required'
            ),array(
                    'field' => 'importproduct',
                    'label' => get_msg('product'),
                    'rules' => 'required'
            ),array(
                    'field' => 'user_id',
                    'label' => get_msg('user_id'),
                    'rules' => 'required'
            )
            
        );

        $this->form_validation->set_data($this->input->post());
        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules( $rules );

        if ( $this->form_validation->run() == FALSE ) {
            $arr = array('isError' => true, 'message' =>  validation_errors());    
            echo json_encode( $arr );
            exit;
        }



            $payment_method = "";
            $paypal_result = 0;
            $stripe_result = 0;
            $cod_result = 0;
                //User Using COD 
            $payment_method = "COD";
            $cod_result = 1;

            $mobile=$this->input->post('shipping_phone');
             if($mobile && preg_match('/^(009665|\+9665)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/', $mobile)){
                $mobile=$mobile;
             }else{
                $mobile=ltrim($mobile, '0');
                if (strlen($mobile)<11) {
                  $mobile="+966".$mobile;
                }else{
                   $mobile=$mobile;
                }
             }
            $user_id=$this->input->post('user_id');
            $conds = array(
                "user_id" => $user_id
            );
             $allAddress = $this->UserAddress->get_all_by($conds)->result();
             foreach ($allAddress as $singleAddress) {
                 $address_dataNew = array(
                     "is_default"=> 0
                 );
                 $this->UserAddress->save($address_dataNew, $singleAddress->id);
             }

        $address_data = array(
            "user_id"=> $this->input->post('user_id'), 
            "receiverName"=> $this->input->post('shipping_first_name'), 
            "mobileNo"=> $mobile, 
            "email"=> $this->input->post('shipping_email'), 
            "address"=> $this->input->post('shipping_address_1'), 
            "city"=> $this->input->post('shipping_address_2'), 
            // "latitute"=> $this->input->post('latitute'),   
            // "lontitude"=> $this->input->post('lontitude'),     
            // "landmark"=> $this->input->post('landmark'),   
            "is_default"=> 1

            );

        if ( !$this->UserAddress->save($address_data, $this->input->post('default_add_id'))) {
            $arr = array('isError' => true, 'message' =>  get_msg( 'err_address_add' ));    
            echo json_encode( $arr );
            exit;
        }

        $UserAddress = $this->UserAddress->get_one_by( array( 'user_id' => $this->input->post('user_id'), 'is_default' => 1));
            
            if($UserAddress->id ==null || !$UserAddress->id){
                $arr = array('isError' => true, 'message' =>  get_msg( 'delivery_address_not_added' ));    
                echo json_encode( $arr );
                exit;
            }

            if( $paypal_result == 1 || $stripe_result == 1 || $cod_result == 1 || $bank_result == 1 || $paytabs_result = 1 || $POSTerminal_result = 1) {
                
                $this->db->trans_start();
                //First Time
                $transaction_row_count = $this->Transactionheader->count_all();
                $current_date_month = date("Ym");
                $current_date_time = date("Y-m-d H:i:s"); 
                $conds['code'] = $current_date_month;
                $trans_code_checking =  $this->Code->get_one_by($conds)->code;
                $id = false;
                if($trans_code_checking == "") {
                    //New record for this year--mm, need to insert as inside the core_code_generator table
                    $data['type']  =  "transaction";
                    $data['code']  =  $today = date("Ym"); ;
                    $data['count'] = $transaction_row_count + 1;
                    $data['added_user_id'] = $this->input->post( 'user_id' );
                    $data['added_date'] = date("Y-m-d H:i:s"); 
                    $data['updated_date'] = date("Y-m-d H:i:s"); 
                    $data['updated_user_id'] = 0;
                    $data['updated_flag'] = 0;

                    if( !$this->Code->save($data, $id) ) {
                        $this->db->trans_rollback();
                        $arr = array('isError' => true, 'message' =>  get_msg( 'err_model' ));
                        echo json_encode( $arr );
                        exit;
                    }

                    // get inserted id
                    if ( !$id ) $id = $data['id']; 

                    if($id) {
                        $trans_code = $this->Code->get_one($id)->code;
                    }
                } else {
                    //record is already exist so just need to update for count field only
                    $data['count'] = $transaction_row_count + 1;

                    $core_code_generator_id =  $this->Code->get_one_by($conds)->id;

                    if( !$this->Code->save($data, $core_code_generator_id) ) {
                        // rollback the transaction
                        $this->db->trans_rollback();
                        $arr = array('isError' => true, 'message' =>  get_msg( 'err_model' ));
                        echo json_encode( $arr );
                        exit;
                    }

                    $conds['id'] = $core_code_generator_id;
                    $trans_code =  $this->Code->get_one_by($conds)->code . ($transaction_row_count + 1);


                }
                $shipping_method_amount = 15.00;
                
                $shopinfo = $this->Shop->get_one_by(array('id' => $this->input->post('shop_id')));
                $product = $this->Product->get_one($this->input->post( 'importproduct'));
                $delivery_date          = "";
                $delivery_time_from     = "";
                $delivery_time_to       = "";
                $time_slot_id           = "";
                if($this->input->post( 'time_slot_id')) {
                    $timeslotsdetails = $this->Timeslotsdetails->get_one_by(array('id' => $this->input->post('time_slot_id')));
                    $delivery_date          = $timeslotsdetails->slot_date;
                    $delivery_time_from     = $timeslotsdetails->start_time;
                    $delivery_time_to       = $timeslotsdetails->end_time;
                    $time_slot_id           = $this->input->post('time_slot_id');

                }
                 $shipping_latitute     = '';
                 $shipping_lontitude        = '';
                 if($UserAddress->id){
                    $shipping_first_name = $UserAddress->receiverName;
                    $shipping_address_1 = $UserAddress->address;
                    $shipping_address_2 = $UserAddress->city;
                    $shipping_city      = $UserAddress->city;
                    $shipping_email     = $UserAddress->email;
                    $shipping_phone     = $UserAddress->mobileNo;
                    $shipping_latitute      = $UserAddress->latitute;
                    $shipping_lontitude     = $UserAddress->lontitude;
                 }else{
                    $shipping_first_name = $this->input->post( 'shipping_first_name');
                    $shipping_address_1 = $this->input->post( 'shipping_address_1');
                    $shipping_address_2 = $this->input->post( 'shipping_address_2');
                    $shipping_city      = $this->input->post( 'shipping_address_2');
                    $shipping_email     = $this->input->post( 'shipping_email');
                    $shipping_phone     = $this->input->post( 'shipping_phone');
                 }
                 
                 $tax_val=0.00;
                 if($shopinfo->overall_tax_value){
                    $tax_val=$product->unit_price * $shopinfo->overall_tax_value;
                 }
                 
                //Need to save inside transaction header table 
                $trans_header = array(
                    'user_id'               => $this->input->post( 'user_id'),
                    'shop_id'               => $this->input->post( 'shop_id' ),
                    'sub_total_amount'      => $product->unit_price,
                    'tax_amount'            => $tax_val,
                    'shipping_amount'       => 0,
                    // 'balance_amount'         => $this->input->post( 'balance_amount' ),
                    'balance_amount'        => ($product->unit_price + ($tax_val + $shipping_method_amount) ),
                    'total_item_amount'     => $product->unit_price,
                    'total_item_count'      => 1,
                    'contact_name'          => $shipping_first_name,
                    'contact_phone'         => $shipping_phone ,
                    'payment_method'        => $payment_method,
                    'trans_status_id'       => 1,
                    'discount_amount'       => 0,
                    'coupon_discount_amount'=> 0,
                    'trans_code'            => $trans_code,
                    'added_date'            => $current_date_time,
                    'added_user_id'         => $this->input->post( 'user_id' ),
                    'updated_date'          => $current_date_time,
                    'updated_user_id'       => "0",
                    'updated_flag'          => "0",
                    'currency_symbol'       => $shopinfo->currency_symbol,
                    'currency_short_form'   => $shopinfo->currency_short_form,
                    // 'billing_first_name'    => $this->input->post( 'billing_first_name'),
                    // 'billing_last_name'      => $this->input->post( 'billing_last_name'),
                    // 'billing_company'        => $this->input->post( 'billing_company'),
                    // 'billing_address_1'      => $this->input->post( 'billing_address_1'),
                    // 'billing_address_2'      => $this->input->post( 'billing_address_2'),
                    // 'billing_country'        => $this->input->post( 'billing_country'),
                    // 'billing_state'          => $this->input->post( 'billing_state'),
                    // 'billing_city'           => $this->input->post( 'billing_city'),
                    // 'billing_postal_code'    => $this->input->post( 'billing_postal_code'),
                    // 'billing_email'          => $this->input->post( 'billing_email'),
                    // 'billing_phone'          => $this->input->post( 'billing_phone'),
                     'shipping_first_name'   => $shipping_first_name,
                    // 'shipping_last_name' => $this->input->post( 'shipping_last_name'),
                    // 'shipping_company'       => $this->input->post( 'shipping_company'),
                     'shipping_address_1'   => $shipping_address_1,
                     'shipping_address_2'   => $shipping_address_2,
                    // 'shipping_country'       => $this->input->post( 'shipping_country'),
                    // 'shipping_state'     => $this->input->post( 'shipping_state'),
                     'shipping_city'            => $shipping_city,
                    // 'shipping_postal_code'   => $this->input->post( 'shipping_postal_code'),
                     'shipping_email'       => $shipping_email,
                     'shipping_phone'       => $shipping_phone,
                     'shipping_lat'         => $shipping_latitute,
                     'shipping_lng'         => $shipping_lontitude,
                     'shop_lat'             => $shopinfo->lat,
                     'shop_lng'             => $shopinfo->lng,
                    'shipping_tax_percent'  => 0,
                    'tax_percent'           => $shopinfo->overall_tax_label,
                    'shipping_method_amount' => $shipping_method_amount,
                    'shipping_method_name'   => "Per_kilo_cost",
                    'memo'                   => "", 
                    'is_zone_shipping'       => 1,
                    'delivery_date'         => $delivery_date,
                    'delivery_time_from'    => $delivery_time_from,
                    'delivery_time_to'      => $delivery_time_to,
                    'time_slot_id'          => $time_slot_id,

                );

                $trans_header_id = false;
                $inserted=$this->Transactionheader->saveOrder($trans_header);
                if( $inserted ==null) {
                    // rollback the transaction
                    $this->db->trans_rollback();
                    $arr = array('isError' => true, 'message' =>  get_msg( 'err_model' ));
                    echo json_encode( $arr );
                    exit;
                } 

                $trans_header_id = $inserted; 

                //$trans_details = $this->input->post( 'details' );
                $totalItem = 0;
                $totalItemAmountNoTax = 0.0;
                $sub_total_amount = 0.0;
                $discount_total_amount = 0.0;
                $originalprice_total_amount = 0.0;
                $tax_amount = 0.0;

                    if($product->is_available==1 && $product->status==1 && $this->input->post( 'shop_id' ) == $shopinfo->id) {
                        $categoryid = $product->cat_id;
                        // print_r($trans_details);die;
                        $trans_detail['shop_id'] = $product->shop_id;
                        $trans_detail['product_id'] = $product->id;
                        $trans_detail['product_category_id'] = $categoryid;
                        $trans_detail['product_name'] = $product->name;
                        $trans_detail['product_photo'] = $product->thumbnail;
                        $trans_detail['price'] = $product->unit_price;
                        $trans_detail['original_price'] = $product->original_price;
                        $trans_detail['product_color_name'] = "";
                        $trans_detail['qty'] = 1;
                        $trans_detail['discount_value'] = 0.00;
                        $trans_detail['discount_percent'] = 0.00;
                        $trans_detail['discount_amount'] = 0.00;
                        $trans_detail['transactions_header_id'] = $trans_header_id;
                        $trans_detail['added_date'] = $current_date_time;
                        $trans_detail['added_user_id'] = $this->input->post('user_id');
                        $trans_detail['updated_date'] = $current_date_time;
                        $trans_detail['updated_user_id'] = "0";
                        $trans_detail['updated_flag'] = "0";

                        $trans_detail['currency_short_form'] = $shopinfo->currency_short_form;
                        $trans_detail['currency_symbol'] = $shopinfo->currency_symbol;
                        $trans_detail['product_unit'] = $product->product_unit;
                        $trans_detail['product_measurement'] = $product->product_measurement;
                        $trans_detail['shipping_cost'] = $product->shipping_cost;

                        if (!$this->Transactiondetail->save($trans_detail)) {
                            // if error in saving transaction detail,
                            $this->db->trans_rollback();
                            $arr = array('isError' => true, 'message' =>  get_msg( 'err_model' ));
                            echo json_encode( $arr );
                            exit;
                        }

                        //Need to update transaction count table
                        $prd_cat_id = $product->cat_id;
                        $prd_sub_cat_id = $product->sub_cat_id;

                        $trans_count['product_id'] = $product->id;
                        $trans_count['shop_id'] = $product->shop_id;
                        $trans_count['cat_id'] = $prd_cat_id;
                        $trans_count['sub_cat_id'] = $prd_sub_cat_id;
                        $trans_count['user_id'] = $this->input->post('user_id');


                        $qtyDtls = 1;
                        $originalPriceDtls = $product->original_price;
                        $discountPercentDtls = 0.0;
                        $price = $originalPriceDtls;
                        $discount_amount = 0.0;
                        if ($discountPercentDtls > 0) {
                            $discount_amount = ((0.0 / 100) * $originalPriceDtls);
                            $discountPriceDtls = ($originalPriceDtls - ((0.0 / 100) * $originalPriceDtls));
                            $price = $discountPriceDtls;
                        }

                        $itemSubtotal = $originalPriceDtls * $qtyDtls;

                            $totalItem += $qtyDtls;
                            $totalItemAmountNoTax += $itemSubtotal;

                            $itemDiscountAmount = $discount_amount * $qtyDtls;
                            $discount_total_amount += $itemDiscountAmount;
                            $sub_total_amount += $price * $qtyDtls;
                            $itemOriginalAmount = $product->original_price * $qtyDtls;
                            $originalprice_total_amount += $itemOriginalAmount;



                        if (!$this->Transactioncount->save($trans_count)) {
                            // if error in saving review rating,
                            $this->db->trans_rollback();
                            $arr = array('isError' => true, 'message' =>  get_msg( 'err_model' ));
                            echo json_encode( $arr );
                            exit;

                        }

                    }
            

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $arr = array('isError' => true, 'message' =>  get_msg( 'err_model' ));
                    echo json_encode( $arr );
                    exit;
                } else {
                    $total_item_amount=$sub_total_amount;
                    

                    $tax_amount=(($shopinfo->overall_tax_value / 100) * $total_item_amount);
                    $master_data = array(
                        'total_item_count'=> $totalItem,
                        'sub_total_amount'=> $sub_total_amount, // after discount
                        'discount_amount'=> $discount_total_amount,
                        'coupon_discount_amount'=> 0.0,
                        'tax_amount'=> $tax_amount,
                        'shipping_method_amount'=> $shipping_method_amount,
                        'total_item_amount'=> $originalprice_total_amount, // without discount
                        'balance_amount'=> $total_item_amount + $shipping_method_amount + $tax_amount-0.0,
                    );

                    // save data
                    $this->Transactionheader->save( $master_data, $trans_header_id );
                    $status_data = array( 'transactions_header_id'=> $trans_header_id, 'transactions_status_id'=> 1 );
                    // save data
                    $this->Transactionstatustracking->save($status_data);

                    
                        $orderCount = $timeslotsdetails->order_count + 1;
                        $slot_data['order_count'] = $orderCount;
                        if ($orderCount >= $timeslotsdetails->accept_order_total) {
                            $slot_data['available'] = 0;
                        }

                        // save data
                        $this->Timeslotsdetails->save($slot_data, $timeslotsdetails->id);
                    
                    
                    $this->db->trans_commit();
                }

               // $trans_header_obj = $this->Transactionheader->get_one($trans_header_id);
                
                //redirect(site_url('admin/orders/orderedit/'.$trans_header_id));
                 $arr = array('isError' => false, 'message' =>  get_msg('save_successfully'), 'site_url' =>  site_url('admin/orders/orderedit/'.$trans_header_id));    
                echo json_encode( $arr );
                exit;

        }

        $arr = array('isError' => false, 'message' =>  get_msg('save_successfully'));    
        echo json_encode( $arr );
        exit;
        
    }
	/**
	* Update the existing one
	*/
	function edit( $id ) {

		// load user
		$this->data['transaction'] = $this->Transactionheader->get_one( $id );

		redirect(site_url('admin/transactions/'));
	}

	/**
	 	* Update the existing one
		*/
	function update() {
		
		$id = $this->input->post('trans_header_id');
		$assign_to = $this->input->post('assign_to');

		$assign_to_data = array( 'assign_to'=> $assign_to );
			
		// save data
		$this->Transactionheader->save( $assign_to_data, $id ); 
		redirect(site_url('admin/orders/'));

	}

//    update order
    function editordersubmit() {

        $id = $this->input->post('transactions_header_id');
        $rules = array(
            array(
                'field' => 'transactions_header_id',
                'label' => get_msg('transactions_header_id'),
                'rules' => 'required'
            )

        );

        $this->form_validation->set_data($this->input->post());
        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules( $rules );

        if ( $this->form_validation->run() == FALSE ) {
            $arr = array('isError' => true, 'message' =>  validation_errors());
            echo json_encode( $arr );
            exit;
        }

        $totalItem = 0;
        $totalItemAmountNoTax = 0.0;
        $sub_total_amount = 0.0;
        $discount_total_amount = 0.0;
        $originalprice_total_amount = 0.0;
        $tax_amount = 0.0;
        foreach($this->input->post('idDtls') as $key =>$sinleDetails) {
            if($this->input->post('idDtls')[$key]){
                $qtyDtls= $this->input->post('qtyDtls')[$key];
                $originalPriceDtls = $this->input->post('originalPriceDtls')[$key];
                $discountPercentDtls = $this->input->post('discountPercentDtls')[$key];
                $price=$originalPriceDtls;
                $discount_amount=0.0;
                if($discountPercentDtls>0){
                    $discount_amount=(($this->input->post('discountPercentDtls')[$key] / 100) * $originalPriceDtls);
                    $discountPriceDtls = ($originalPriceDtls - (($this->input->post('discountPercentDtls')[$key] / 100) * $originalPriceDtls));
                    $price=$discountPriceDtls;
                }

                $stockDtls = $this->input->post('stockDtls')[$key];
                $itemSubtotal=$originalPriceDtls * $qtyDtls;
                if($stockDtls=='1'){
                    $totalItem += $qtyDtls;
                    $totalItemAmountNoTax += $itemSubtotal;

                    $itemDiscountAmount=$discount_amount * $qtyDtls;
                    $discount_total_amount += $itemDiscountAmount;
                    $sub_total_amount +=$price * $qtyDtls;
                    $itemOriginalAmount=$this->input->post('originalPriceDtls')[$key] * $qtyDtls;
                    $originalprice_total_amount += $itemOriginalAmount;
                }
                $dataDtls = array(
                    'original_price' => $this->input->post('originalPriceDtls')[$key],
                    'price' => $price,
                    'discount_amount' => $discount_amount,
                    'discount_percent' => $discountPercentDtls,
                    'discount_value' => $discountPercentDtls /100,
                    'qty' => $qtyDtls,
                    'stock_status' => ''.$stockDtls.'',
                );
                $this->Transactiondetail->save($dataDtls, $this->input->post('idDtls')[$key]);
            }
        }


        $total_item_amount=$sub_total_amount;
        if($this->input->post('coupon_discount_amount') >0){
            $total_item_amount=$total_item_amount - $this->input->post('coupon_discount_amount');
        }

        $tax_amount=(($this->input->post('tax_percent') / 100) * $total_item_amount);
        $master_data = array(
            'total_item_count'=> $totalItem,
            'sub_total_amount'=> $sub_total_amount, // after discount
            'discount_amount'=> $discount_total_amount,
            'coupon_discount_amount'=> $this->input->post('coupon_discount_amount'),
            'tax_amount'=> $tax_amount,
            'shipping_method_amount'=> $this->input->post('shipping_method_amount'),
            'total_item_amount'=> $originalprice_total_amount, // without discount
            'balance_amount'=> $total_item_amount + $this->input->post('shipping_method_amount') + $tax_amount-$this->input->post('coupon_discount_amount'),
        );

        // save data
        $this->Transactionheader->save( $master_data, $id );
        redirect(site_url('admin/orders/orderedit/'.$id.''));

    }
	/**
	* Sending Message From FCM For Android
	*/
	function send_android_fcm( $registatoin_ids, $message) 
    {
    	//Google cloud messaging GCM-API url
    	$url = 'https://fcm.googleapis.com/fcm/send';
    	$fields = array(
    	    'registration_ids' => $registatoin_ids,
    	    'data' => $message,
    	);
    	// Update your Google Cloud Messaging API Key
    	//define("GOOGLE_API_KEY", "AIzaSyCCwa8O4IeMG-r_M9EJI_ZqyybIawbufgg");
    	define("GOOGLE_API_KEY", $this->Backend_config->get_one('be1')->fcm_api_key);  	
    		
    	$headers = array(
    	    'Authorization: key=' . GOOGLE_API_KEY,
    	    'Content-Type: application/json'
    	);
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_POST, true);
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);	
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    	$result = curl_exec($ch);				
    	if ($result === FALSE) {
    	    die('Curl failed: ' . curl_error($ch));
    	}
    	curl_close($ch);

    	return $result;
    }

	/**
	* View transaction Detail
	*/
	function detail($id)
	{
		// breadcrumb urls
		$this->data['action_title'] = get_msg( 'trans_detail' );


        $conds = array( 'register_role_id' => 5 );
        $conds['status'] = 1;
        $conds['is_banned'] = 0;
        // get rows count
//        $this->data['rows_count'] = $this->User->count_all_by($conds);

        // get users
       // $this->data['users'] = $this->User->get_all_by($conds);
        $transaction = $this->Transactionheader->get_one( $id );
        $this->data['assigned'] =array();
        if($transaction->assign_to){
            $conds['user_id'] = $transaction->assign_to;
            $this->data['assigned'] = $this->User->get_one_by($conds);
        }
        $this->data['transaction'] = $transaction;
		$this->load_template('orders/detail', $this->data, true );
	}

	function printDetailsPdf($id='')
	{
		//$this->load->library('pdf');
// ini_set('display_errors', 1);
		$this->data['action_title'] = get_msg( 'trans_detail' );

		$detail = $this->Transactionheader->get_one( $id );
		$this->data['transaction'] = $detail;
		//$this->load_template('orders/detail', $this->data, true );
		$html = $this->load->view('backend/orders/printdetail',$this->data, true);  

		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8']);
		if($this->session->userdata('language_code')=="ar"){
		$mpdf->SetDirectionality('rtl');
		}
				//$mpdf = new \Mpdf\Mpdf();
		$mpdf->autoScriptToLang = true;
		$mpdf->autoLangToFont = true;
		$mpdf->WriteHTML($html);
		$mpdf->setFooter('{PAGENO} / {nb}');
		$mpdf->Output('order-no-'.$id.'.pdf', 'I');
		//$mpdf->Output();
		// $canvas = $this->pdf->get_canvas();
	 //    $this->pdf->loadHtml($html);
	 //    // $customPaper = array(0,0,570,570);
	 //    //$this->pdf->set_paper($customPaper);

	 //    $this->pdf->setPaper('A4','portrait');//landscape
	 //    $this->pdf->render();
	 //    $this->pdf->stream("abc.pdf", array('Attachment'=>0));
	    //'Attachment'=>0 for view and 'Attachment'=>1 for download file      
	}

    function printSummeryPdf($id='')
    {
        //$this->load->library('pdf');
// ini_set('display_errors', 1);
        $this->data['action_title'] = get_msg( 'trans_detail' );

        $detail = $this->Transactionheader->get_one( $id );
        $this->data['transaction'] = $detail;
        //$this->load_template('orders/detail', $this->data, true );
        $html = $this->load->view('backend/orders/printsummery',$this->data, true);

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8']);
        if($this->session->userdata('language_code')=="ar"){
            $mpdf->SetDirectionality('rtl');
        }
        //$mpdf = new \Mpdf\Mpdf();
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;
        $mpdf->WriteHTML($html);
        $mpdf->setFooter('{PAGENO} / {nb}');
        $mpdf->Output('order-no-'.$id.'.pdf', 'I');
        //$mpdf->Output();
        // $canvas = $this->pdf->get_canvas();
        //    $this->pdf->loadHtml($html);
        //    // $customPaper = array(0,0,570,570);
        //    //$this->pdf->set_paper($customPaper);

        //    $this->pdf->setPaper('A4','portrait');//landscape
        //    $this->pdf->render();
        //    $this->pdf->stream("abc.pdf", array('Attachment'=>0));
        //'Attachment'=>0 for view and 'Attachment'=>1 for download file
    }

    function cancelorder() {
        header('Content-Type: application/json');
        $rules = array(
            array(
                'field' => 'id',
                'label' => get_msg('id'),
                'rules' => 'required'
            ),array(
                'field' => 'cancelableStatus',
                'label' => get_msg('reject_order'),
                'rules' => 'required'
            )

        );

        $this->form_validation->set_data($this->input->post());
        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules( $rules );

        if ( $this->form_validation->run() == FALSE ) {
            $arr = array('isError' => true, 'message' =>  validation_errors());
            echo json_encode( $arr );
            exit;
        }
        $logged_in_user = $this->ps_auth->get_user_info();

        $conds['id'] = $this->input->post("id");

        $_data = $this->Transactionheader->get_one_by($conds);
        if (!$_data->id) {
            $data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
            echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            exit;
        }

        if ($_data->trans_status_id==5) {
            $data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
            echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            exit;
        }
        if ($_data->payment_status==1) {
            $data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
            echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            exit;
        }


        $rejectStatus=1;
        if($_data->reject_status==1){
            $rejectStatus=0;
        }

        $masterDtls = array(
            'reject_status' =>$rejectStatus,
            'rejected_by' =>"eShtri",
            'cancel_reason' =>"",
        );
        if($this->input->post("id") && $this->Transactionheader->save($masterDtls, $this->input->post("id"))){
            $arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));
            echo json_encode( $arr );
            exit;
        }else{
            $arr = array('isError' => true, 'message' =>  get_msg('update_failed_please_try_again'));
            echo json_encode( $arr );
            exit;
        }
    }
	/**
	 * Saving Logic
	 * 1) upload image
	 * 2) save attribute
	 * 3) save image
	 * 4) check transaction status
	 *
	 * @param      boolean  $id  The user identifier
	 */
	function save( $id  = false, $status_id = 0 ) {

		// save Transaction

		$data['trans_status_id'] = $status_id;

		if ( ! $this->Transactionheader->save( $data, $id )) {
			// if there is an error in inserting user data,	
				
				// rollback the transaction
			$this->db->trans_rollback();

				// set error message
			$this->data['error'] = get_msg( 'err_model' );
				
			return;
		}

			
		// commit the transaction
		if ( ! $this->check_trans()) {
        	
			// set flash error message
			$this->set_flash_msg( 'error', get_msg( 'err_model' ));
		} else {

			if ( $id ) {
			// if user id is not false, show success_add message
				
				// Sending Email To user
				$status_title = $this->Transactionstatus->get_one($status_id)->title;
				$to_who = "user";
				$subject = get_msg('order_status_subject') . $status_title;
				if ( !send_transaction_order_emails( $id, $to_who, $subject )) {

					$this->set_flash_msg( 'error', get_msg( 'err_email_not_send_to_user' ));
				
				}
				
				$this->set_flash_msg( 'success', get_msg( 'success_trans_edit' ));
			}
		}


		redirect(site_url() . "/admin/transactions/detail/" . $id);
	}

	function filter_from_dashboard($status_id) {
		
		$this->session->set_userdata("trans_status_id", $status_id);

		redirect(site_url() . "/admin/transactions/search");

	}

	/**
	 * Delete the record
	 * 1) delete category
	 * 2) delete image from folder and table
	 * 3) check transactions
	 */
	function delete( $id ) {

		// start the transaction
		$this->db->trans_start();

		// check access
		$this->check_access( DEL );

		// delete categories and images
		$enable_trigger = true; 
		
		if ( !$this->ps_delete->delete_transaction( $id, $enable_trigger )) {

			// set error message
			$this->set_flash_msg( 'error', get_msg( 'err_model' ));

			// rollback
			$this->trans_rollback();

			// redirect to list view
			redirect( $this->module_site_url());
		}
			
		/**
		 * Check Transcation Status
		 */
		if ( !$this->check_trans()) {

			$this->set_flash_msg( 'error', get_msg( 'err_model' ));	
		} else {
        	
			$this->set_flash_msg( 'success', get_msg( 'success_trans_delete' ));
		}
		
		redirect( $this->module_site_url());
	}



	function confirmaccountingdata() {
		header('Content-Type: application/json');  
		$rules = array(
			array(
					'field' => 'id',
					'label' => get_msg('id'),
					'rules' => 'required'
			),array(
					'field' => 'confirmStatus',
					'label' => get_msg('confirmStatus'),
					'rules' => 'required'
			)
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$logged_in_user = $this->ps_auth->get_user_info();

		$conds['id'] = $this->input->post("id");
		$_data = $this->Transactionheader->get_one_by($conds);
		if (!$_data->id) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		
		if ($_data->trans_status_id !=5) {
			$data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		if ($_data->balance_amount==0) {
			$data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		if ($_data->accounting_status==1) {
			$data = array('isError' => true, 'message' =>  get_msg('accounting_already_done'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		if ($_data->payment_status==0) {
			$data = array('isError' => true, 'message' =>  get_msg('payment_not_completed'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$period_data = $this->Acc_period->get_one_by(array('is_current' =>1));
		if (!$period_data->is_current) {
			$data = array('isError' => true, 'message' =>  get_msg('Period_already_closed'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$returnStatus=0;
		if($_data->payment_method=='COD' || $_data->payment_method=='POSTerminal'){
			$returnStatus=$this->cashOnDeliveryAccounting($_data, $period_data);
		}else if ($_data->payment_method=='paytabs') {
			$returnStatus=$this->payTabsAccounting($_data, $period_data);
		}
		
		if($returnStatus){
			$arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('update_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}
	}

	function cashOnDeliveryAccounting($orderDetails, $period_data){
		// order completed status starts
		if($orderDetails->trans_status_id==5){
			$order_id=$orderDetails->id;

		$deliveryBoyDetails=$this->User->get_one_by(array('user_id' =>$orderDetails->assign_to));
		$deliveryCommissionDetails=$this->Acc_deliveryboy_charge->get_one_by(array('driver_id' =>$orderDetails->assign_to));
		$shopCommissionDetails=$this->Acc_comission_seller->get_one_by(array('shop_id' =>$orderDetails->shop_id));
		$shopDetails=$this->Shop->get_one_by(array('id' =>$orderDetails->shop_id));
		$receiveAbleHead = $this->Acc_coa->get_one_by(array('journals_type' =>'Account_receivable'));
        $deliveryFeeCommissionHead = $this->Acc_coa->get_one_by(array('journals_type' =>'DeliveryFeeCommission'));
        $salesCommissionHead = $this->Acc_coa->get_one_by(array('journals_type' =>'SalesCommission'));
        $pettyCashHead = $this->Acc_coa->get_one_by(array('journals_type' =>'PettyCash'));
        $creditCardBankHead = $this->Acc_coa->get_one_by(array('journals_type' =>'Bank', 'is_card_default' =>'1'));
        $payAbleHead = $this->Acc_coa->get_one_by(array('journals_type' =>'Account_Payable'));
		$gateWayCostHead = $this->Acc_coa->get_one_by(array('journals_type' =>'PaymentGateWayFees'));

        $deliveryFee=$orderDetails->shipping_method_amount;
        $deliveryboyType=$deliveryCommissionDetails->contract_type;
        $deliveryboyCommissionRate=$deliveryCommissionDetails->order_comission;

        $ourDeliveryFeeRevinue=$deliveryFee;
        if($deliveryboyType=="DeliveryBoy"){
        	$ourDeliveryFeeRevinue = ($deliveryboyCommissionRate / 100) * $deliveryFee;
        }

		$shopCommissionRate=$shopCommissionDetails->amount_percentage;
        $salesAmount=$orderDetails->sub_total_amount;

        $ourRevinueOnSale=0.00;
        if($salesAmount && $shopCommissionRate>0){ 
        	$ourRevinueOnSale = ($shopCommissionRate / 100) * $salesAmount;
        }

	
		// cash and pos tarminal starts
		if($ourDeliveryFeeRevinue>0){ // voucher master for delivery fees
		$Mstdata = array(
			'entity_type' => "Income_Voucher",
			'service_type' => "DeliveryFeeCommission",
			'caption' => get_msg('comission_Earnings')." - ".date("Y-m-d H:i:s"),
			'acc_period_id' => $period_data->id,
			'trans_date' => date("Y-m-d H:i:s"),
			'reference_code' => $order_id,
			'total_amount' => $ourDeliveryFeeRevinue,
			'narration' => 'Total amount='.$orderDetails->balance_amount.', Tax='.$orderDetails->tax_amount.', Discount='.$orderDetails->discount_amount.', Shipping cost='.$orderDetails->shipping_method_amount.', Product price without tax='.$orderDetails->sub_total_amount,
			'stake_holder_type' => $deliveryboyType,
			'stakeholder_id' => $deliveryCommissionDetails->driver_id,
			'stakeholder_name' => $deliveryBoyDetails->user_name,
			'confirm_status' => "Draft",
			'approve_status' => "Draft",
			'active_status' => "Active",
		);
		$inserted=$this->Acc_vcr_mst->accSave($Mstdata);
		


		$receiveAbleDtls = array(
			'vcr_mst_id' => $inserted,
			'trans_date' => date("Y-m-d H:i:s"),
			'narration' => 'Shipping cost='.$orderDetails->shipping_method_amount,
			'acc_coa_id' => $receiveAbleHead->id,
			'dr_amount' => $ourDeliveryFeeRevinue,
			'cr_amount' => 0.00,
			'drcr_type' => 'Debit',
			'stake_holder_type' => $deliveryboyType,
			'stakeholder_id' => $deliveryCommissionDetails->driver_id,
			'stakeholder_name' => $deliveryBoyDetails->user_name,
			'service_type' => "DeliveryFeeCommission",
		);
		$this->Acc_vcr_dtl->save($receiveAbleDtls);

		$serviceRevenueDtls = array(
			'vcr_mst_id' => $inserted,
			'trans_date' => date("Y-m-d H:i:s"),
			'narration' => 'Shipping cost='.$orderDetails->shipping_method_amount,
			'acc_coa_id' => $deliveryFeeCommissionHead->id,
			'dr_amount' => 0.00,
			'cr_amount' => $ourDeliveryFeeRevinue,
			'drcr_type' => 'Credit',
			'stake_holder_type' => $deliveryboyType,
			'stakeholder_id' => $deliveryCommissionDetails->driver_id,
			'stakeholder_name' => $deliveryBoyDetails->user_name,
			'service_type' => "DeliveryFeeCommission",
		);
		$this->Acc_vcr_dtl->save($serviceRevenueDtls);
		}
		// voucher for comission from saller
		if($ourRevinueOnSale >0){
		$Mstdata = array(
			'entity_type' => "Income_Voucher",
			'service_type' => "SalesCommission",
			'caption' => get_msg('comission_Earnings')." - ".date("Y-m-d H:i:s"),
			'acc_period_id' => $period_data->id,
			'trans_date' => date("Y-m-d H:i:s"),
			'reference_code' => $order_id,
			'total_amount' => $ourRevinueOnSale,
			'narration' => 'Total amount='.$orderDetails->balance_amount.', Tax='.$orderDetails->tax_amount.', Discount='.$orderDetails->discount_amount.', Shipping cost='.$orderDetails->shipping_method_amount.', Product price without tax='.$orderDetails->sub_total_amount,
			'stake_holder_type' => "Seller",
			'stakeholder_id' => $shopDetails->id,
			'stakeholder_name' => $shopDetails->name,
			'confirm_status' => "Draft",
			'approve_status' => "Draft",
			'active_status' => "Active",
		);
		$inserted=$this->Acc_vcr_mst->accSave($Mstdata);
		


		$receiveAbleDtls = array(
			'vcr_mst_id' => $inserted,
			'trans_date' => date("Y-m-d H:i:s"),
			'narration' => 'Total amount='.$orderDetails->balance_amount.', Tax='.$orderDetails->tax_amount.', Product price without tax='.$orderDetails->sub_total_amount,
			'acc_coa_id' => $receiveAbleHead->id,
			'dr_amount' => $ourRevinueOnSale,
			'cr_amount' => 0.00,
			'drcr_type' => 'Debit',
			'stake_holder_type' => "Seller",
			'stakeholder_id' => $shopDetails->id,
			'stakeholder_name' => $shopDetails->name,
			'service_type' => "SalesCommission",
		);
		$this->Acc_vcr_dtl->save($receiveAbleDtls);

		$serviceRevenueDtls = array(
			'vcr_mst_id' => $inserted,
			'trans_date' => date("Y-m-d H:i:s"),
			'narration' => 'Total amount='.$orderDetails->balance_amount.', Tax='.$orderDetails->tax_amount.', Product price without tax='.$orderDetails->sub_total_amount,
			'acc_coa_id' => $salesCommissionHead->id,
			'dr_amount' => 0.00,
			'cr_amount' => $ourRevinueOnSale,
			'drcr_type' => 'Credit',
			'stake_holder_type' => "Seller",
			'stakeholder_id' => $shopDetails->id,
			'stakeholder_name' => $shopDetails->name,
			'service_type' => "SalesCommission",
		);
		$this->Acc_vcr_dtl->save($serviceRevenueDtls);
		
		}
		// pos tarminal
		if($orderDetails->payment_method =="POSTerminal"){
			$POSTerminalRateDetails=$this->Acc_paymentgateways_fees->get_one_by(array('id' =>2));
				$POSTerminalCostRate=$POSTerminalRateDetails->cost_rate;
		        //$salesAmount=$orderDetails->sub_total_amount;

		        $ourCostOnPOSTerminal=0.00;
		        if($orderDetails->balance_amount && $POSTerminalCostRate>0){ 
		        	$ourCostOnPOSTerminal = ($POSTerminalCostRate / 100) * $orderDetails->balance_amount;
		        }
				// paytabs expenses comission
				if($ourCostOnPOSTerminal>0){
				$Mstdata = array(
					'entity_type' => "Expense_Voucher",
					'service_type' => "PaymentGateWayFees",
					'caption' => get_msg('POSTerminalFees')." - ".date("Y-m-d H:i:s"),
					'acc_period_id' => $period_data->id,
					'trans_date' => date("Y-m-d H:i:s"),
					'reference_code' => $order_id,
					'total_amount' => $ourCostOnPOSTerminal,
					'narration' => 'Total amount='.$orderDetails->balance_amount,
					'stake_holder_type' => "PaymentGateWay",
					'stakeholder_id' => $POSTerminalCostRate->id,
					'stakeholder_name' => $POSTerminalCostRate->name,
					'confirm_status' => "Draft",
					'approve_status' => "Draft",
					'active_status' => "Active",
				);
				$inserted=$this->Acc_vcr_mst->accSave($Mstdata);

				$gateWayCostDtls = array(
					'vcr_mst_id' => $inserted,
					'trans_date' => date("Y-m-d H:i:s"),
					'narration' => 'POSTerminal cost='.$ourCostOnPOSTerminal.' Rate '.$POSTerminalCostRate.'% On Total= '.$orderDetails->balance_amount,
					'acc_coa_id' => $gateWayCostHead->id,
					'dr_amount' => $ourCostOnPOSTerminal,
					'cr_amount' => 0.00,
					'drcr_type' => 'Debit',
					'stake_holder_type' => "PaymentGateWay",
					'stakeholder_id' => $POSTerminalCostRate->id,
					'stakeholder_name' => $POSTerminalCostRate->name,
					'service_type' => "PaymentGateWayFees",
				);
				$this->Acc_vcr_dtl->save($gateWayCostDtls);


				$Mstdata = array(
					'entity_type' => "journal_voucher",
					'service_type' => "PettyCash",
					'caption' => get_msg('PettyCash')." - ".date("Y-m-d H:i:s"),
					'acc_period_id' => $period_data->id,
					'trans_date' => date("Y-m-d H:i:s"),
					'reference_code' => $order_id,
					'total_amount' => $orderDetails->balance_amount,
					'narration' => 'Total amount='.$orderDetails->balance_amount,
					'stake_holder_type' => $deliveryboyType,
					'stakeholder_id' => $deliveryCommissionDetails->driver_id,
					'stakeholder_name' => $deliveryBoyDetails->user_name,
					'confirm_status' => "Draft",
					'approve_status' => "Draft",
					'active_status' => "Active",
				);
				$inserted=$this->Acc_vcr_mst->accSave($Mstdata);

				$pettyCashDtls = array(
					'vcr_mst_id' => $inserted,
					'trans_date' => date("Y-m-d H:i:s"),
					'narration' => 'POSTerminal cost='.$ourCostOnPOSTerminal.' Rate '.$POSTerminalCostRate.'% On Total= '.$orderDetails->balance_amount,
					'acc_coa_id' => $pettyCashHead->id,
					'dr_amount' => 0.00,
					'cr_amount' => $orderDetails->balance_amount,
					'drcr_type' => 'Credit',
					'stake_holder_type' => $deliveryboyType,
					'stakeholder_id' => $deliveryCommissionDetails->driver_id,
					'stakeholder_name' => $deliveryBoyDetails->user_name,
					'service_type' => "PettyCash",
				);
				$this->Acc_vcr_dtl->save($pettyCashDtls);

				$pettyCashToBankDtls = array(
					'vcr_mst_id' => $inserted,
					'trans_date' => date("Y-m-d H:i:s"),
					'narration' => 'POSTerminal cost='.$ourCostOnPOSTerminal.' Rate '.$POSTerminalCostRate.'% On Total= '.$orderDetails->balance_amount,
					'acc_coa_id' => $creditCardBankHead->id,
					'dr_amount' => $orderDetails->balance_amount,
					'cr_amount' => 0.00,
					'drcr_type' => 'Debit',
					'stake_holder_type' => $deliveryboyType,
					'stakeholder_id' => $deliveryCommissionDetails->driver_id,
					'stakeholder_name' => $deliveryBoyDetails->user_name,
					'service_type' => "PettyCash",
				);
				$this->Acc_vcr_dtl->save($pettyCashToBankDtls);

		        }
		}
		
		$payment_confirm_data = array( 'accounting_status'=> 1); //1=done , 0=not done
		$this->Transactionheader->save($payment_confirm_data, $order_id);
		} // trans_status_id 5 check end

	} // cash on delivery ends




	function payTabsAccounting($orderDetails, $period_data){
		// order completed status starts
		if($orderDetails->trans_status_id==5){
			$order_id=$orderDetails->id;

		$deliveryBoyDetails=$this->User->get_one_by(array('user_id' =>$orderDetails->assign_to));
		$deliveryCommissionDetails=$this->Acc_deliveryboy_charge->get_one_by(array('driver_id' =>$orderDetails->assign_to));
		$shopCommissionDetails=$this->Acc_comission_seller->get_one_by(array('shop_id' =>$orderDetails->shop_id));
		$shopDetails=$this->Shop->get_one_by(array('id' =>$orderDetails->shop_id));
		$receiveAbleHead = $this->Acc_coa->get_one_by(array('journals_type' =>'Account_receivable'));
        $deliveryFeeCommissionHead = $this->Acc_coa->get_one_by(array('journals_type' =>'DeliveryFeeCommission'));
        $salesCommissionHead = $this->Acc_coa->get_one_by(array('journals_type' =>'SalesCommission'));
        $pettyCashHead = $this->Acc_coa->get_one_by(array('journals_type' =>'PettyCash'));
        $payAbleHead = $this->Acc_coa->get_one_by(array('journals_type' =>'Account_Payable'));
		$gateWayCostHead = $this->Acc_coa->get_one_by(array('journals_type' =>'PaymentGateWayFees'));
		$cashInTransitHead = $this->Acc_coa->get_one_by(array('journals_type' =>'Cash_In_Transit_Receiving'));
		$paytabsRateDetails=$this->Acc_paymentgateways_fees->get_one_by(array('id' =>1));

       // $deliveryFee=$orderDetails->shipping_method_amount;
        // $deliveryboyType=$deliveryCommissionDetails->contract_type;
        // $deliveryboyCommissionRate=$deliveryCommissionDetails->order_comission;

        $ourDeliveryFeeRevinue=$orderDetails->shipping_method_amount;
        // if($deliveryboyType=="DeliveryBoy"){
        // 	$ourDeliveryFeeRevinue = ($deliveryboyCommissionRate / 100) * $deliveryFee;
        // }

		$shopCommissionRate=$shopCommissionDetails->amount_percentage;
        $salesAmount=$orderDetails->sub_total_amount;

        $ourRevinueOnSale=0.00;
        if($salesAmount && $shopCommissionRate>0){ 
        	$ourRevinueOnSale = ($shopCommissionRate / 100) * $salesAmount;
        }

	
		$paytabsCostRate=$paytabsRateDetails->cost_rate;
		$ourCostOnPayTabs=0.00;
		if($orderDetails->balance_amount && $paytabsCostRate>0){ 
			$ourCostOnPayTabs = ($paytabsCostRate / 100) * $orderDetails->balance_amount;
		}
		
		// voucher master for delivery fees
		if($orderDetails->shipping_method_amount>0){
			$Mstdata = array(
				'entity_type' => "Income_Voucher",
				'service_type' => "DeliveryFeeCommission",
				'caption' => get_msg('comission_Earnings')." - ".date("Y-m-d H:i:s"),
				'acc_period_id' => $period_data->id,
				'trans_date' => date("Y-m-d H:i:s"),
				'reference_code' => $order_id,
				'total_amount' => $ourDeliveryFeeRevinue,
				'narration' => 'Total amount='.$orderDetails->balance_amount.', Tax='.$orderDetails->tax_amount.', Discount='.$orderDetails->discount_amount.', Shipping cost='.$orderDetails->shipping_method_amount.', Product price without tax='.$orderDetails->sub_total_amount,
				'stake_holder_type' => $deliveryboyType,
				'stakeholder_id' => $deliveryCommissionDetails->driver_id,
				'stakeholder_name' => $deliveryBoyDetails->user_name,
				'confirm_status' => "Draft",
				'approve_status' => "Draft",
				'active_status' => "Active",
			);
			$inserted=$this->Acc_vcr_mst->accSave($Mstdata);

			if($ourDeliveryFeeRevinue>0){
				$serviceRevenueDtls = array(
					'vcr_mst_id' => $inserted,
					'trans_date' => date("Y-m-d H:i:s"),
					'narration' => 'Shipping cost='.$orderDetails->shipping_method_amount,
					'acc_coa_id' => $deliveryFeeCommissionHead->id,
					'dr_amount' => 0.00,
					'cr_amount' => $ourDeliveryFeeRevinue,
					'drcr_type' => 'Credit',
					'stake_holder_type' => $deliveryboyType,
					'stakeholder_id' => $deliveryCommissionDetails->driver_id,
					'stakeholder_name' => $deliveryBoyDetails->user_name,
					'service_type' => "DeliveryFeeCommission",
				);
			$this->Acc_vcr_dtl->save($serviceRevenueDtls);
			}
			}
			// voucher for comission from saller
			if($ourRevinueOnSale >0){
			$Mstdata = array(
				'entity_type' => "Income_Voucher",
				'service_type' => "SalesCommission",
				'caption' => get_msg('comission_Earnings')." - ".date("Y-m-d H:i:s"),
				'acc_period_id' => $period_data->id,
				'trans_date' => date("Y-m-d H:i:s"),
				'reference_code' => $order_id,
				'total_amount' => $ourRevinueOnSale,
				'narration' => 'Total amount='.$orderDetails->balance_amount.', Tax='.$orderDetails->tax_amount.', Discount='.$orderDetails->discount_amount.', Shipping cost='.$orderDetails->shipping_method_amount.', Product price without tax='.$orderDetails->sub_total_amount,
				'stake_holder_type' => "Seller",
				'stakeholder_id' => $shopDetails->id,
				'stakeholder_name' => $shopDetails->name,
				'confirm_status' => "Draft",
				'approve_status' => "Draft",
				'active_status' => "Active",
			);
			$inserted=$this->Acc_vcr_mst->accSave($Mstdata);
			
			$receiveAbleDtls = array(
				'vcr_mst_id' => $inserted,
				'trans_date' => date("Y-m-d H:i:s"),
				'narration' => 'Total amount='.$orderDetails->balance_amount.', Tax='.$orderDetails->tax_amount.', Product price without tax='.$orderDetails->sub_total_amount,
				'acc_coa_id' => $receiveAbleHead->id,
				'dr_amount' => $ourRevinueOnSale,
				'cr_amount' => 0.00,
				'drcr_type' => 'Debit',
				'stake_holder_type' => "Seller",
				'stakeholder_id' => $shopDetails->id,
				'stakeholder_name' => $shopDetails->name,
				'service_type' => "SalesCommission",
			);
			$this->Acc_vcr_dtl->save($receiveAbleDtls);

			$serviceRevenueDtls = array(
				'vcr_mst_id' => $inserted,
				'trans_date' => date("Y-m-d H:i:s"),
				'narration' => 'Total amount='.$orderDetails->balance_amount.', Tax='.$orderDetails->tax_amount.', Product price without tax='.$orderDetails->sub_total_amount,
				'acc_coa_id' => $salesCommissionHead->id,
				'dr_amount' => 0.00,
				'cr_amount' => $ourRevinueOnSale,
				'drcr_type' => 'Credit',
				'stake_holder_type' => "Seller",
				'stakeholder_id' => $shopDetails->id,
				'stakeholder_name' => $shopDetails->name,
				'service_type' => "SalesCommission",
			);
			$this->Acc_vcr_dtl->save($serviceRevenueDtls);
			}

			// paytabs expenses comission
			if($ourCostOnPayTabs>0){
			$Mstdata = array(
				'entity_type' => "Expense_Voucher",
				'service_type' => "PaymentGateWayFees",
				'caption' => get_msg('PaytabsFees')." - ".date("Y-m-d H:i:s"),
				'acc_period_id' => $period_data->id,
				'trans_date' => date("Y-m-d H:i:s"),
				'reference_code' => $order_id,
				'total_amount' => $ourCostOnPayTabs,
				'narration' => 'Total amount='.$orderDetails->balance_amount,
				'stake_holder_type' => "PaymentGateWay",
				'stakeholder_id' => $paytabsRateDetails->id,
				'stakeholder_name' => $paytabsRateDetails->name,
				'confirm_status' => "Draft",
				'approve_status' => "Draft",
				'active_status' => "Active",
			);
			$inserted=$this->Acc_vcr_mst->accSave($Mstdata);

			$gateWayCostDtls = array(
				'vcr_mst_id' => $inserted,
				'trans_date' => date("Y-m-d H:i:s"),
				'narration' => 'Paytab cost='.$ourCostOnPayTabs.' Rate '.$paytabsCostRate.'% On Total= '.$orderDetails->balance_amount,
				'acc_coa_id' => $gateWayCostHead->id,
				'dr_amount' => $ourCostOnPayTabs,
				'cr_amount' => 0.00,
				'drcr_type' => 'Debit',
				'stake_holder_type' => "PaymentGateWay",
				'stakeholder_id' => $paytabsRateDetails->id,
				'stakeholder_name' => $paytabsRateDetails->name,
				'service_type' => "PaymentGateWayFees",
			);
			$this->Acc_vcr_dtl->save($gateWayCostDtls);

			}

			// cash on transit
			if($orderDetails->balance_amount>0){
			$Mstdata = array(
				'entity_type' => "Receive_Voucher",
				'service_type' => "PaymentGateWayFees",
				'caption' => get_msg('PaytabsFees')." - ".date("Y-m-d H:i:s"),
				'acc_period_id' => $period_data->id,
				'trans_date' => date("Y-m-d H:i:s"),
				'reference_code' => $order_id,
				'total_amount' => $orderDetails->balance_amount,
				'narration' => 'Total amount='.$orderDetails->balance_amount,
				'stake_holder_type' => "PaymentGateWay",
				'stakeholder_id' => $paytabsRateDetails->id,
				'stakeholder_name' => $paytabsRateDetails->name,
				'confirm_status' => "Draft",
				'approve_status' => "Draft",
				'active_status' => "Active",
			);
			$inserted=$this->Acc_vcr_mst->accSave($Mstdata);

			$cashInTransitDtls = array(
				'vcr_mst_id' => $inserted,
				'trans_date' => date("Y-m-d H:i:s"),
				'narration' => 'Online paid total='.$orderDetails->balance_amount,
				'acc_coa_id' => $cashInTransitHead->id,
				'dr_amount' => $orderDetails->balance_amount,
				'cr_amount' => 0.00,
				'drcr_type' => 'Debit',
				'stake_holder_type' => "PaymentGateWay",
				'stakeholder_id' => $paytabsRateDetails->id,
				'stakeholder_name' => $paytabsRateDetails->name,
				'service_type' => "PaymentGateWayFees",
			);
			$this->Acc_vcr_dtl->save($cashInTransitDtls);
				
			$pettyCashDtls = array(
				'vcr_mst_id' => $inserted,
				'trans_date' => date("Y-m-d H:i:s"),
				'narration' => 'Online paid tota='.$orderDetails->balance_amount,
				'acc_coa_id' => $pettyCashHead->id,
				'dr_amount' => 0.00,
				'cr_amount' => $orderDetails->balance_amount,
				'drcr_type' => 'Credit',
				'stake_holder_type' => $deliveryboyType,
				'stakeholder_id' => $deliveryCommissionDetails->driver_id,
				'stakeholder_name' => $deliveryBoyDetails->user_name,
				'service_type' => "PettyCash",
			);
			$this->Acc_vcr_dtl->save($pettyCashDtls);
			}

			$payment_confirm_data = array( 'accounting_status'=> 1); //1=done , 0=not done
			$this->Transactionheader->save($payment_confirm_data, $order_id);


		}// trans_status_id 5 check end

	}// paytabs accounting ends



}
