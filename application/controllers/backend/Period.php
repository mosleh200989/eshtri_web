<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users crontroller for BE_USERS table
 */
class Period extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'Acc_period' );
	}

	/**
	* Load Notification Sending Form
	*/
	function index() {
		$this->data['action_title'] = get_msg('chart_of_accounts');
		// for showing data
		if($this->input->get("showAction")){
			$conds['id'] = $this->input->get("id");
			$_data = $this->Acc_period->get_one_by($conds);
			if (!$_data->id) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$data = array('isError' => false, 'defaultInstance' =>  $_data);    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		// for editing data
		if($this->input->get("editData")){
			$conds['id'] = $this->input->get("id");
			$_data = $this->Acc_period->get_one_by($conds);
			if (!$_data->id) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$data = array('isError' => false, 'defaultInstance' =>  $_data);    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$this->load_template('period/list', $this->data, true );
	}

	
	 public function periodlist()
	 {
 
		 $iDisplayStart = $this->input->get("iDisplayStart")? intval($this->input->get("iDisplayStart")):0;
		 $iDisplayLength = $this->input->get("iDisplayLength")? intval($this->input->get("iDisplayLength")) :10;
		 $sSortDir = $this->input->get("sSortDir_0");
		 $iSortingCol = $this->input->get("iSortCol_0")? $this->input->get("iSortCol_0") : null;
		 $sSearch = $this->input->get("sSearch") ? $this->input->get("sSearch") : null;
		 if($sSearch){
			 $sSearch = "% ".$sSearch." %";
		 }
		 
		 $dataReturns = array();
	 
		 $langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
		 $sqlWhere = $this->input->get("sSearch") ? " AND caption LIKE '%".$this->input->get("sSearch")."%'" : "";
		 $querySQL = "SELECT * FROM acc_period where id >0 ".$sqlWhere." ORDER BY id ASC LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart."";
		 $resultSet = $this->Acc_period->queryPrepare($querySQL);
		 $totalCount = $resultSet->num_rows();
		 $serial = 0;
			 $total = 0;
			 
		 $actionList = array();
		 
			 $actionListConfirm = array();
			 $actionList[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
			 $actionList[]=array('actionName' => 'update', 'actionClass' => 'update', 'titleName' => get_msg( 'update_data'), 'iconName' => 'fa fa-pencil-square-o text-navy');
			 $actionList[]=array('actionName' => 'confirm', 'actionClass' => 'confirm', 'titleName' => get_msg( 'Confirm_Data'), 'iconName' => 'fa fa-check-circle text-warning');
			//  $actionList[]=array('actionName' => 'delete', 'actionClass' => 'delete', 'titleName' => get_msg( 'Delete_Data'), 'iconName' => 'fa fa-trash text-danger');
			 $actionListConfirm[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
			 
				 
		 foreach ($resultSet->result() as $row)
		 {
			 $serial++;
			 $iscurrent="".get_msg('Upcoming')."";
			if($row->is_current==1){
				$iscurrent="".get_msg('OnGoing')."";
			}else if($row->is_current=='0'){
				$iscurrent="".get_msg('completed')."";
			}
			 $dataReturns[]=array('DT_RowId' => $row->id, 'accessibleUrl' => (($row->is_current == "1") ? $actionListConfirm : $actionList),'activeStatus' => get_msg( 'Active'), 0 => $serial, 1 => $row->caption,2 => get_msg(''.$row->period_type.''),3 => $row->period_start_dt,4 => $row->period_close_dt,5 => $iscurrent,6 => "");
		 }
		 
		 $gridData=array();
		 if ($totalCount == 0) {
			 $gridData=array('iTotalRecords' => 0, 'iTotalDisplayRecords' =>0, 'aaData'=> array());
			 echo json_encode($gridData);
			 exit();
		 }
 
		 $gridData=array('iTotalRecords' => $totalCount, 'iTotalDisplayRecords' =>$totalCount, 'aaData'=> $dataReturns);
		 echo json_encode($gridData);
		 exit();
	 }
	function writesubmit(){
		header('Content-Type: application/json');  
		$rules = array(
			array(
					'field' => 'period_start_dt',
					'label' => get_msg('Period_Date'),
					'rules' => 'required'
			),
			array(
					'field' => 'period_close_dt',
					'label' => get_msg('Period_Date'),
					'rules' => 'required'
			),
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}

		$period_start_dt = $this->input->post('period_start_dt');
		$period_close_dt = $this->input->post('period_close_dt');
		$remark = $this->input->post('remark');

		$orderdate = explode('-', $period_start_dt);
		$year = $orderdate[0];
		$month   = $str = ltrim($orderdate[1], '0');
		$checkStartDate=$this->db
		->select('*', false)
		->from('acc_period')
		->where('period_start_dt <=', $period_start_dt)
		->where('period_close_dt >=', $period_start_dt)
		->order_by('id', 'asc')
		->get();
		if($checkStartDate->num_rows() > 0){
			$arr = array('isError' => true, 'message' =>  get_msg('data_already_exist'));    
			echo json_encode( $arr );
			exit;
		}
		$checkCloseDate=$this->db
		->select('*', false)
		->from('acc_period')
		->where('period_start_dt >=', $period_close_dt)
		->where('period_close_dt <=', $period_close_dt)
		->order_by('id', 'asc')
		->get();
		if($checkCloseDate->num_rows() > 0){
			$arr = array('isError' => true, 'message' =>  get_msg('data_already_exist'));    
			echo json_encode( $arr );
			exit;
		}
		$data = array(
		'period_month' => $month,
		'period_year' => $year,
		'period_start_dt' => $period_start_dt,
		'period_close_dt' => $period_close_dt,
		'period_type' => "Customize",
		'remark' => $remark,
		'active_status' => "Active",
		'caption' => $period_start_dt." - ".$period_close_dt,
		);

		$this->User->save($user_data,$user_id);


		if($this->Acc_period->save($data)){
			$arr = array('isError' => false, 'message' =>  get_msg('save_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}
	}
	function editsubmit(){
		header('Content-Type: application/json');  

		// for showing data
		if($this->input->post("confirmStatus")){
			$conds['id'] = $this->input->post("id");
			$_data = $this->Acc_period->get_one_by($conds);
			if (!$_data) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			if ($_data->is_current) {
				$data = array('isError' => true, 'message' =>  get_msg('no_action'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$data = array('isError' => false, 'defaultInstance' =>  $_data);    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		$rules = array(
			array(
					'field' => 'period_start_dt',
					'label' => get_msg('Period_Date'),
					'rules' => 'required'
			),
			array(
					'field' => 'period_close_dt',
					'label' => get_msg('Period_Date'),
					'rules' => 'required'
			),
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}

		$id = $this->input->post('id');
		if(!$id){
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}

		$checkdata=$this->Acc_period->get_one_by( array( 'id' => $id));
		if (!$checkdata && $checkdata->id !=$id)
		{
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}
				
		$period_start_dt = $this->input->post('period_start_dt');
		$period_close_dt = $this->input->post('period_close_dt');
		$remark = $this->input->post('remark');

		$orderdate = explode('-', $period_start_dt);
		$year = $orderdate[0];
		$month   = $str = ltrim($orderdate[1], '0');

		$checkStartDate=$this->db
		->select('*', false)
		->from('acc_period')
		->where('period_start_dt <=', $period_start_dt)
		->where('period_close_dt >=', $period_start_dt)
		->where('id !=', $id)
		->order_by('id', 'asc')
		->get();
		if($checkStartDate->num_rows() > 0){
			$arr = array('isError' => true, 'message' =>  get_msg('data_already_exist'));    
			echo json_encode( $arr );
			exit;
		}
		$checkCloseDate=$this->db
		->select('*', false)
		->from('acc_period')
		->where('period_start_dt >=', $period_close_dt)
		->where('period_close_dt <=', $period_close_dt)
		->where('id !=', $id)
		->order_by('id', 'asc')
		->get();
		if($checkCloseDate->num_rows() > 0){
			$arr = array('isError' => true, 'message' =>  get_msg('data_already_exist'));    
			echo json_encode( $arr );
			exit;
		}
		$data = array(
		'period_month' => $month,
		'period_year' => $year,
		'period_start_dt' => $period_start_dt,
		'period_close_dt' => $period_close_dt,
		'period_type' => "Customize",
		'remark' => $remark,
		'active_status' => "Active",
		'caption' => $period_start_dt." - ".$period_close_dt,
		);
		
		
		if($this->Acc_period->save($data, $id)){
			$arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}
	}

	function confirmsubmit() {
		header('Content-Type: application/json');  
		$rules = array(
			array(
					'field' => 'id',
					'label' => get_msg('id'),
					'rules' => 'required'
			),array(
					'field' => 'confirmStatus',
					'label' => get_msg('confirmStatus'),
					'rules' => 'required'
			)
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$logged_in_user = $this->ps_auth->get_user_info();

		$conds['id'] = $this->input->post("id");
		$_data = $this->Acc_period->get_one_by($conds);
		if (!$_data->id) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		if ($_data->is_current==1) {
			$data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}


		$periodconds['is_current'] = 1;
		$previousPeriod = $this->Acc_period->get_one_by($periodconds);

		$ledgerNew="";
        $messages = '';
        $totalMessages = '';
        $totalError = 0;
        $totalSuccess = 0;
        $ledgerListOld = array();
        $ledgerListNew = array();

        $currentDrRevenue=0.0;
        $currentCrRevenue=0.0;
        $currentDrExpense=0.0;
        $currentCrExpense=0.0;

		
		$revenueSQL = "SELECT SUM(acc_ledger.current_dr) AS current_dr, SUM(acc_ledger.current_cr) AS current_cr FROM acc_ledger LEFT JOIN acc_period ON acc_ledger.acc_period_id = acc_period.id LEFT JOIN acc_coa ON acc_coa.id = acc_ledger.acc_coa_id WHERE acc_period.is_current=1 AND acc_coa.accounts_type='Revenue'";
		$revenueresultSet = $this->Acc_period->queryPrepare($revenueSQL);
		$revenue =$revenueresultSet->row();

		$expenseSQL = "SELECT SUM(acc_ledger.current_dr) AS current_dr, SUM(acc_ledger.current_cr) AS current_cr FROM acc_ledger LEFT JOIN acc_period ON acc_ledger.acc_period_id = acc_period.id LEFT JOIN acc_coa ON acc_coa.id = acc_ledger.acc_coa_id WHERE acc_period.is_current=1 AND acc_coa.accounts_type='Expense'";
		$expenseresultSet = $this->Acc_period->queryPrepare($expenseSQL);
		$expense =$expenseresultSet->row();

		$currentDrRevenue = $revenue->current_dr ? $revenue->current_dr : 0.0;
        $currentCrRevenue = $revenue->current_cr ? $revenue->current_cr : 0.0;
        $currentDrExpense = $expense->current_dr ? $expense->current_dr : 0.0;
		$currentCrExpense = $expense->current_cr ? $expense->current_cr : 0.0;
		
		$totalSuccess = 0;
		if (!$previousPeriod->id) {
			$coaSQL = "SELECT * FROM acc_coa WHERE acc_coa_flag='HED'";
			$coaresultSet = $this->Acc_period->queryPrepare($coaSQL);
			foreach ($coaresultSet->result() as $accCoa)
			{
				$ledgerNew = array(
					'caption' => $accCoa->caption." - ".$_data->caption,
					'acc_period_id' => $_data->id,
					'acc_coa_id' => $accCoa->id,
					'is_current' => 1,
				);
				$totalSuccess = $totalSuccess + 1;
				$this->Acc_ledger->save($ledgerNew);
			}
		}else{
			$ledgerSQL = "SELECT * FROM acc_ledger WHERE acc_period_id=".$previousPeriod->id." AND is_current=1";
			$ledgerList = $this->Acc_period->queryPrepare($ledgerSQL);
			foreach ($ledgerList->result() as $ledger)
			{
				$oldLedger=$ledger;
				$oldLedger->is_current=0;
				$oldLedger->closing_dr=$ledger->current_dr;
				$oldLedger->closing_cr=$ledger->current_cr;

				$ledgerACCSQL = "SELECT * FROM acc_coa WHERE id=".$ledger->acc_coa_id."";
				$ledgerACCresultSet = $this->Acc_period->queryPrepare($ledgerACCSQL);
				$ledgerACC =$ledgerACCresultSet->row();

				$periodSQL = "SELECT * FROM acc_period WHERE id=".$ledger->acc_period_id."";
				$periodresultSet = $this->Acc_period->queryPrepare($periodSQL);
				$period =$periodresultSet->row();

				$ledgerNew = array();
				$ledgerNew['caption']=$ledgerACC->caption." - ".$period->caption;
				$ledgerNew['acc_period_id']=$_data->id;
				$ledgerNew['acc_coa_id']=$ledgerACC->id;
				$ledgerNew['is_current']=1;
				if($ledgerACC->journals_type=="Retained_Earnings"){
					$ledgerNew['opening_dr']=$ledger->closing_dr;
					$ledgerNew['opening_cr']=$ledger->closing_cr;
					$ledgerNew['current_dr']=$ledger->closing_dr + ($currentDrExpense - $currentDrRevenue);
					$ledgerNew['current_cr']=$ledger->closing_cr + ($currentCrExpense - $currentCrRevenue);
                }else{
					$ledgerNew['opening_dr']=$ledger->closing_dr;
					$ledgerNew['opening_cr']=$ledger->closing_cr;
					$ledgerNew['current_dr']=$ledger->closing_dr;
					$ledgerNew['current_cr']=$ledger->closing_cr;
				}
				$totalSuccess = $totalSuccess + 1;
				$this->Acc_ledger->save($oldLedger, $oldLedger->id);
				$this->Acc_ledger->save($ledgerNew);
			}
		}

		if ($totalSuccess) {
			if ($previousPeriod->id) {
				$previousPeriodData = array();
				$previousPeriodData['is_current']=0;
				$previousPeriodData['is_close']=1;
				$this->Acc_period->save($previousPeriodData, $previousPeriod->id);
			}
			$defaultInstance = array();
			$defaultInstance['is_current']=1;
			$defaultInstance['is_close']=0;
			$this->Acc_period->save($defaultInstance, $_data->id);

			$arr = array('isError' => false, 'message' =>  get_msg('Data_Inserted_Successfully'));    
			echo json_encode( $arr );
			exit;
        }else{
			if (!$previousPeriod->id) {
				$defaultInstance = array();
				$defaultInstance['is_current']=1;
				$defaultInstance['is_close']=0;
				$this->Acc_period->save($defaultInstance, $_data->id);
			}else{
				$arr = array('isError' => true, 'message' =>  get_msg('Data_Inserted_failed'));    
				echo json_encode( $arr );
				exit;
			}
			
		}
	}

	function recreateledger(){
		header('Content-Type: application/json');
		$rules = array(
			array(
					'field' => 'recreateLedger',
					'label' => get_msg('confirmStatus'),
					'rules' => 'required'
			)
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$periodconds['is_current'] = 1;
		$currentPeriod = $this->Acc_period->get_one_by($periodconds);
		$ledgerNew="";
        $messages = '';
        $totalMessages = '';
        $totalError = 0;
        $totalSuccess = 0;
        $ledgerListOld = array();
        $ledgerListNew = array();
        if (!$currentPeriod->id) {
            $data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$coaSQL = "SELECT * FROM acc_coa WHERE acc_coa_flag='HED'";
		$coaresultSet = $this->Acc_period->queryPrepare($coaSQL);
		foreach ($coaresultSet->result() as $accCoa)
		{
			$ledgerconds['acc_coa_id'] = $accCoa->id;
			$ledgerconds['acc_period_id'] = $currentPeriod->id;
			$accLedger = $this->Acc_ledger->get_one_by($ledgerconds);
			if (!$accLedger->id){
			$ledgerNew = array(
				'caption' => $accCoa->caption." - ".$currentPeriod->caption,
				'acc_period_id' => $currentPeriod->id,
				'acc_coa_id' => $accCoa->id,
				'is_current' => 1,
			);
			$totalSuccess = $totalSuccess + 1;
			$this->Acc_ledger->save($ledgerNew);
			}
		}
		if (!$totalSuccess) {
            $data = array('isError' => true, 'message' =>  get_msg('No_saving_data_found'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}else{
			$data = array('isError' => false, 'message' =>  get_msg('Data_Inserted_Successfully'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

	}
}