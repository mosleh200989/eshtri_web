<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users crontroller for BE_USERS table
 */
class Orderfees extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'Acc_period' );
	}

	/**
	* Load Notification Sending Form
	*/
	function index() {
		$this->data['action_title'] = get_msg('order_fees');
		// for showing data
		if($this->input->get("showAction")){
			$conds['id'] = $this->input->get("id");
			$defaultInstance = $this->Acc_delivery_fees->get_one_by($conds);
			if (!$defaultInstance->id) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$isExtraCharge="".get_msg('no')."";
			if($defaultInstance->isExtraCharge==1){
				$isExtraCharge="".get_msg('yes')."";
			}
            $shopName="";
            if ($defaultInstance->scope=='shop') {
                $shopconds['id'] = $defaultInstance->shop_id;
                $shopInfo = $this->Shop->get_one_by($shopconds);
                $shopName=$shopInfo->name;
            }
			$data = array('isError' => false, 'defaultInstance' => $defaultInstance, 'isExtraCharge' => $isExtraCharge, 'shop_name' =>  $shopName);
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		// for editing data
		if($this->input->get("editData")) {
            $conds['id'] = $this->input->get("id");
            $defaultInstance = $this->Acc_delivery_fees->get_one_by($conds);
            if (!$defaultInstance->id) {
                $data = array('isError' => true, 'message' => get_msg('notfound_message'));
                echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                exit;
            }
            $shopName="";
            if ($defaultInstance->scope=='shop') {
                $shopconds['id'] = $defaultInstance->shop_id;
                $shopInfo = $this->Shop->get_one_by($shopconds);
                $shopName=$shopInfo->name;
            }
			$data = array('isError' => false, 'defaultInstance' =>  $defaultInstance, 'shop_name' =>  $shopName );
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		// for loadOfferData data
		if($this->input->get("loadOfferData")) {
            $scope = $this->input->get("scope");
            $shop_id = $this->input->get("shop_id");
            if($scope=='1'){
                $offerBy="shop";
                $offerDescOne = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 1, 'offerBy' => $offerBy, 'shop_id' => $shop_id));
                $offerDescTwo = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 2, 'offerBy' => $offerBy, 'shop_id' => $shop_id));
                $offerDescThree = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 3, 'offerBy' => $offerBy, 'shop_id' => $shop_id));
                $offerDescFour = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 4, 'offerBy' => $offerBy, 'shop_id' => $shop_id));
                $offerDescFive = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 5, 'offerBy' => $offerBy, 'shop_id' => $shop_id));
            }else{
                $offerBy="global";
                $offerDescOne = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 1, 'offerBy' => $offerBy));
                $offerDescTwo = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 2, 'offerBy' => $offerBy));
                $offerDescThree = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 3, 'offerBy' => $offerBy));
                $offerDescFour = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 4, 'offerBy' => $offerBy));
                $offerDescFive = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 5, 'offerBy' => $offerBy));
            }

			$data = array('isError' => false, 'offerDescOne' =>  $offerDescOne, 'offerDescTwo' =>  $offerDescTwo, 'offerDescThree' =>  $offerDescThree, 'offerDescFour' =>  $offerDescFour, 'offerDescFive' =>  $offerDescFive);
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		$this->load_template('acc_delivery_fees/list', $this->data, true );
	}

	
	 public function tablelist()
	 {
 
		 $iDisplayStart = $this->input->get("iDisplayStart")? intval($this->input->get("iDisplayStart")):0;
		 $iDisplayLength = $this->input->get("iDisplayLength")? intval($this->input->get("iDisplayLength")) :10;
		 $sSortDir = $this->input->get("sSortDir_0");
		 $iSortingCol = $this->input->get("iSortCol_0")? $this->input->get("iSortCol_0") : null;
		 $sSearch = $this->input->get("sSearch") ? $this->input->get("sSearch") : null;
		 if($sSearch){
			 $sSearch = "% ".$sSearch." %";
		 }
		 
		 $dataReturns = array();
	 
		 $langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
		 $sqlWhere = $this->input->get("sSearch") ? " AND initial_amount LIKE '%".$this->input->get("sSearch")."%'" : "";
         $sqlWhere .= $this->input->get("scope") =='1' || $this->input->get("shop_id") !=null? " AND shop_id='" .$this->input->get("shop_id"). "'" : "";
         $sqlWhere .= $this->input->get("scope") =='0' ? " AND scope='global'" : "";
         $sqlWhere .= $this->input->get("scope") =='1'? " AND scope='shop'" : "";
		 $querySQL = "SELECT * FROM acc_delivery_fees where id >0 ".$sqlWhere." ORDER BY id ASC LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart."";
		 $resultSet = $this->Acc_period->queryPrepare($querySQL);
		 $totalCount = $resultSet->num_rows();
		 $serial = 0;
			 $total = 0;
			 
		 $actionList = array();
		 
			 $actionListConfirm = array();
			 $actionList[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
			 $actionList[]=array('actionName' => 'update', 'actionClass' => 'update', 'titleName' => get_msg( 'update_data'), 'iconName' => 'fa fa-pencil-square-o text-navy');
			 $actionList[]=array('actionName' => 'confirm', 'actionClass' => 'confirm', 'titleName' => get_msg( 'Confirm_Data'), 'iconName' => 'fa fa-check-circle text-warning');
			//  $actionList[]=array('actionName' => 'delete', 'actionClass' => 'delete', 'titleName' => get_msg( 'Delete_Data'), 'iconName' => 'fa fa-trash text-danger');
			 $actionListConfirm[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
			 
				 
		 foreach ($resultSet->result() as $row)
		 {
			 $serial++;
			 $iscurrent="".get_msg('no')."";
			if($row->is_current==1){
				$iscurrent="".get_msg('OnGoing')."";
			}
			$isExtraCharge="".get_msg('no')."";
			if($row->isExtraCharge==1){
				$isExtraCharge="".get_msg('yes')."";
			}
			 $dataReturns[]=array('DT_RowId' => $row->id, 'accessibleUrl' => (($row->is_current == "1") ? $actionListConfirm : $actionList),'activeStatus' => get_msg( 'Active'),'shop_id' => $row->shop_id,'scope' => $row->scope,
			 0 => $serial, 
			 1 => $row->initial_amount,
			 2 => $isExtraCharge,
			 3 => $row->per_kilo_amount,
			 4 => $row->maximum_amount,
			 5 => $row->extraChargeOutCityIfCash,
			 6 => $row->date_created,
			 7 => $iscurrent,
			 8 => "");
		 }
		 
		 $gridData=array();
		 if ($totalCount == 0) {
			 $gridData=array('iTotalRecords' => 0, 'iTotalDisplayRecords' =>0, 'aaData'=> array());
			 echo json_encode($gridData);
			 exit();
		 }
 
		 $gridData=array('iTotalRecords' => $totalCount, 'iTotalDisplayRecords' =>$totalCount, 'aaData'=> $dataReturns);
		 echo json_encode($gridData);
		 exit();
	 }
	function writesubmit(){
		header('Content-Type: application/json');  
		$rules = array(
			array(
					'field' => 'initial_amount',
					'label' => get_msg('initial_amount'),
					'rules' => 'required'
			),
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$logged_in_user = $this->ps_auth->get_user_info();
		$shop_id="";
		if($this->input->post("scope") =='1'){
            $scope='shop';
            if($this->input->post("shop_for_cost") ==null || !$this->input->post("shop_for_cost")){
                $arr = array('isError' => true, 'message' =>  get_msg('shop_required'));
                echo json_encode( $arr );
                exit;
            }else{
                $shop_id=$this->input->post("shop_for_cost");
            }
        }else if ($this->input->post("scope") =='0'){
            $scope='global';
        }else{
            $arr = array('isError' => true, 'message' =>  get_msg('scope_required'));
            echo json_encode( $arr );
            exit;
        }
		$data = array(
            'scope' => $scope,
            'shop_id' => $shop_id,
            'initial_amount' => $this->input->post("initial_amount"),
            'maximum_amount' => $this->input->post("maximum_amount"),
            'extraChargeOutCityIfCash' => $this->input->post("extraChargeOutCityIfCash"),
            'isExtraCharge' => $this->input->post("isExtraCharge"),
            'per_kilo_amount' => $this->input->post("per_kilo_amount"),
            'remark' => $this->input->post('remark'),
            'created_by' => $logged_in_user->user_id,
		);

		if($this->Acc_delivery_fees->save($data)){
			$arr = array('isError' => false, 'message' =>  get_msg('save_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}
	}
	function editsubmit(){
		header('Content-Type: application/json');  

		$rules = array(
			array(
					'field' => 'initial_amount',
					'label' => get_msg('initial_amount'),
					'rules' => 'required'
			),
			array(
				'field' => 'id',
				'label' => get_msg('id'),
				'rules' => 'required'
			),
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$id = $this->input->post('id');
		if(!$id){
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}

		$checkdata=$this->Acc_delivery_fees->get_one_by( array( 'id' => $id));
		if (!$checkdata->id)
		{
			$arr = array('isError' => true, 'message' =>  get_msg('data_not_found'));    
			echo json_encode( $arr );
			exit;
		}
		$logged_in_user = $this->ps_auth->get_user_info();
        $shop_id="";
		$data = array(
            'initial_amount' => $this->input->post("initial_amount"),
            'maximum_amount' => $this->input->post("maximum_amount"),
            'extraChargeOutCityIfCash' => $this->input->post("extraChargeOutCityIfCash"),
            'isExtraCharge' => $this->input->post("isExtraCharge"),
            'per_kilo_amount' => $this->input->post("per_kilo_amount"),
            'remark' => $this->input->post('remark'),
            'updated_by' => $logged_in_user->user_id,
		);
		if($this->Acc_delivery_fees->save($data, $id)){
			$arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}

	}

	function confirmsubmit() {
		header('Content-Type: application/json');  
		$rules = array(
			array(
					'field' => 'id',
					'label' => get_msg('id'),
					'rules' => 'required'
			),array(
					'field' => 'confirmStatus',
					'label' => get_msg('confirmStatus'),
					'rules' => 'required'
			)
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$logged_in_user = $this->ps_auth->get_user_info();

		$conds['id'] = $this->input->post("id");
		$defaultInstance = $this->Acc_delivery_fees->get_one_by($conds);
		if (!$defaultInstance->id) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		if ($defaultInstance->is_current==1) {
			$data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
            if($this->input->post("scope") =='shop'){
                $feesSQL = "SELECT * FROM acc_delivery_fees WHERE is_current='1' AND scope='shop' AND shop_id='".$this->input->post("shop_id")."'";
            }else{
                $feesSQL = "SELECT * FROM acc_delivery_fees WHERE is_current='1' AND scope='global'";
            }

			$resultSet = $this->Acc_period->queryPrepare($feesSQL);
			foreach ($resultSet->result() as $row)
			{
				$updateArr = array(
					'is_current' => 0,
				);
				$this->Acc_delivery_fees->save($updateArr, $row->id);
			}

			$newupdateArr = array(
				'is_current' => 1,
			);
			
			if($this->Acc_delivery_fees->save($newupdateArr, $this->input->post("id"))){
				$arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));    
				echo json_encode( $arr );
				exit;
			}else{
				$arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));    
				echo json_encode( $arr );
				exit;
			}
		
	}

	function shippingCostcalculation()
	{
		header('Content-Type: application/json');  
		//https://eshtri.net/admin/orderfees/shippingCostcalculation/?shop_id=shop98ef587bb29242db042804cf245b7d0a&user_id=usr06db64ac1397e509fcb6d311ef89064b

		$shopid=$this->input->get("shop_id");
		$userid=$this->input->get("user_id");
		$default_fee=15;
		if($shopid && $userid){
			$delivery_fees = $this->Acc_delivery_fees->get_one_by( array( 'is_current' => 1));
			if($delivery_fees->initial_amount){
				$default_fee=$delivery_fees->initial_amount;
				if($delivery_fees->isExtraCharge==1 && $delivery_fees->per_kilo_amount){
					$per_kilo_charge=$delivery_fees->per_kilo_amount;
					$shop = $this->Shop->get_one_by( array( 'id' => $shopid));
					$userAddress = $this->UserAddress->get_one_by( array( 'user_id' => $userid, 'is_default' => 1));
					if($userAddress->latitute && $userAddress->lontitude && $shop->lat && $shop->lng){
						$lat1=$userAddress->latitute;
						$lon1=$userAddress->lontitude;
						$lat2=$shop->lat;
						$lon2=$shop->lng;
						$unit="K";
						//$totalDistance=distance($lat1, $lon1, $lat2, $lon2, $unit); 
						$totalDistance=GetDrivingDistance($lat1, $lon1, $lat2, $lon2, $unit); 
						$totalExtraCharge=$totalDistance * $per_kilo_charge;
						$default_fee = $default_fee + $totalExtraCharge;
				
					}

				}
			}
		}
		
		$data=array("success"=>"1", "msg"=>"", "shippingcost"=>round($default_fee,0,PHP_ROUND_HALF_EVEN), "totalDistance"=>$totalDistance);
		echo json_encode($data);
	}
	

	function generateTimeSlot()
	{
		header('Content-Type: application/json');  
		//https://eshtri.net/admin/orderfees/generateTimeSlot
		$start=date('Y-m-d');
		$end = date('Y-m-d', strtotime(end($dates).' +300 day'));
		$datesArr=getDatesFromRange($start, $end);
		foreach ($datesArr as $singleDate) {
			$timeslotsDetails = $this->Timeslotsdetails->get_one_by( array('slot_date' => $singleDate));
			$countfound=false;
			if($timeslotsDetails && $timeslotsDetails->id){
				$timeslotsDetailsCount=$this->db->select('*')->from('mk_time_slots_details')->where('slot_date', $singleDate)->where('order_count is NOT NULL', NULL, FALSE)->get()->row();
				if($timeslotsDetailsCount && $timeslotsDetailsCount->id){
					$countfound=true;
				}
			}
			if(!$countfound){
				$this->Timeslotsdetails->delete_by(array('slot_date' => $singleDate));
				$timeslotsnumber = $this->Timeslotsnumber->get_one_by( array( 'id' => 1));
				$timeslots = $this->Timeslots->get_all_by(array('time_slot_number_id' => 1))->result();
				foreach ($timeslots as $singleSlot) {
					$slotArr = array(
						'slot_date' => $singleDate, 
						'start_time' => $singleSlot->start_time, 
						'end_time' => $singleSlot->end_time, 
						'available' => 1, 
						'accept_order_total' => $timeslotsnumber->limit_order, 
					);
					$this->Timeslotsdetails->save($slotArr);
				}
			}
			
			
		}

		$data=array("success"=>"1", "msg"=>"");
		echo json_encode($data);
	}

    function writeofferssubmit(){
        header('Content-Type: application/json');
        $rules = array(
            array(
                'field' => 'freeFirstCustomer',
                'label' => get_msg('freeFirstCustomer'),
                'rules' => 'required'
            ),

        );

        $this->form_validation->set_data($this->input->post());
        $this->form_validation->set_error_delimiters('', '<br/>');
        $this->form_validation->set_rules( $rules );

        if ( $this->form_validation->run() == FALSE ) {
            $arr = array('isError' => true, 'message' =>  validation_errors());
            echo json_encode( $arr );
            exit;
        }
//
//        is_current	offerType	offerBy	shop_id	freeFirstCustomer	freeFirstDate	percentDiscountAll
//        percentDiscountAmnt	ifExceedAmount	eligibleFreeAmount
//        date_created	last_updated	created_by	updated_by	remark

        $logged_in_user = $this->ps_auth->get_user_info();
        $shop_id="";
        if($this->input->post("offerscope") =='1'){
            $offerBy='shop';
            if($this->input->post("shop_for_offers") ==null || !$this->input->post("shop_for_offers")){
                $arr = array('isError' => true, 'message' =>  get_msg('shop_required'));
                echo json_encode( $arr );
                exit;
            }else{
                $shop_id=$this->input->post("shop_for_offers");
            }
        }else if ($this->input->post("offerscope") =='0'){
            $offerBy='global';
        }else{
            $arr = array('isError' => true, 'message' =>  get_msg('scope_required'));
            echo json_encode( $arr );
            exit;
        }

        // offer one
        $is_current=0;
        if($this->input->post("isDefaultTrue")==1){
            $is_current=1;
        }
        $data1 = array(
            'is_current' => $is_current,
            'offerType' => 1,
            'offerBy' => $offerBy,
            'shop_id' => $shop_id,
            'scope' => 'unique'
        );
        $offerDescOne = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 1, 'offerBy' => $offerBy));
        $this->Acc_delivery_fee_offers->save($data1, $offerDescOne->id);

        // offer two
        $is_current=0;
        if($this->input->post("isDefaultTrue")==2){
            $is_current=1;
        }
        $data2 = array(
            'is_current' => $is_current,
            'offerType' => 2,
            'offerBy' => $offerBy,
            'shop_id' => $shop_id,
            'scope' => 'unique',
            'freeFirstCustomer' => $this->input->post("freeFirstCustomer"),
            'freeFirstDate' => $this->input->post("freeFirstDate"),
        );
        $offerDescTwo = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 2, 'offerBy' => $offerBy));
        $this->Acc_delivery_fee_offers->save($data2, $offerDescTwo->id);

        // offer three
        $is_current=0;
        if($this->input->post("isDefaultTrue")==3){
            $is_current=1;
        }
        $data3 = array(
            'is_current' => $is_current,
            'offerType' => 3,
            'offerBy' => $offerBy,
            'shop_id' => $shop_id,
            'scope' => 'unique',
            'percentDiscountAll' => $this->input->post("percentDiscountAll"),
        );
        $offerDescThree = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 3, 'offerBy' => $offerBy));
        $this->Acc_delivery_fee_offers->save($data3, $offerDescThree->id);

        // offer four
        $is_current=0;
        if($this->input->post("isDefaultTrue")==4){
            $is_current=1;
        }
        $data4 = array(
            'is_current' => $is_current,
            'offerType' => 4,
            'offerBy' => $offerBy,
            'shop_id' => $shop_id,
            'scope' => 'unique',
            'percentDiscountAmnt' => $this->input->post("percentDiscountAmnt"),
            'ifExceedAmount' => $this->input->post("ifExceedAmount"),
        );
        $offerDescFour = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 4, 'offerBy' => $offerBy));
        $this->Acc_delivery_fee_offers->save($data4, $offerDescFour ->id);

        // offer five
        $is_current=0;
        if($this->input->post("isEligibleFreeAmountTrue")==5){
            $is_current=1;
        }
        $data5 = array(
            'is_current' => $is_current,
            'offerType' => 5,
            'offerBy' => $offerBy,
            'shop_id' => $shop_id,
            'scope' => 'no_unique',
            'eligibleFreeAmount' => $this->input->post("eligibleFreeAmount"),
        );
        $offerDescFive = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 5, 'offerBy' => $offerBy));
        $this->Acc_delivery_fee_offers->save($data5, $offerDescFive ->id);
//
//        if($this->Acc_delivery_fees->save($data)){
            $arr = array('isError' => false, 'message' =>  get_msg('save_successfully'), 'data'=>$this->input->post());
            echo json_encode( $arr );
            exit;
//        }else{
//            $arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));
//            echo json_encode( $arr );
//            exit;
//        }
    }
}