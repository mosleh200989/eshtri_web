<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users crontroller for BE_USERS table
 */
class Chart_of_accounts extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'Acc_coa' );
	}

	/**
	* Load Notification Sending Form
	*/
	function index() {
		$langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
		$this->data['action_title'] = "Chart of Accounts";
		$this->data['journalsTypeList'] = $this->Acc_coa->enumListLanguage("journalsType");
		$this->data['rows_count'] = $this->Acc_coa_locale_view->count_all_by( $conds );
		$this->data['accCoa'] = $this->Acc_coa_locale_view->get_all_by( $conds , $this->pag['per_page'], $this->uri->segment( 4 ) );
		// for showing data
		if($this->input->get("showAction")){
			header('Content-Type: application/json');  
			$conds['id'] = $this->input->get("id");
			$defaultInstance = $this->Acc_coa->get_one_by($conds);
			if (!$defaultInstance) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$condss['coa_id'] = $defaultInstance->id;
			$condss['language_enum'] = $langEnumKey;
			$locale = $this->Acc_coa_locale->get_one_by($condss);
			if (!$locale) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$condsss['coa_id'] = $defaultInstance->acc_coa_id;
			$condsss['language_enum'] = $langEnumKey;
			$localeParent = $this->Acc_coa_locale->get_one_by($condsss);

			$data = array('isError' => false, 'defaultInstance' =>  $defaultInstance, 'caption'=>$locale->caption, 'accCoa'=>$localeParent->caption ? $localeParent->caption:"", 'accCoaFlag'=>get_msg($defaultInstance->acc_coa_flag), 'journalsType'=>get_msg($defaultInstance->journals_type));    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		// for editing data
		if($this->input->get("editData")){
			header('Content-Type: application/json');  
			$conds['id'] = $this->input->get("id");
			$defaultInstance = $this->Acc_coa->get_one_by($conds);
			if (!$defaultInstance) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			
			$condsss['coa_id'] = $defaultInstance->acc_coa_id;
			$condsss['language_enum'] = $langEnumKey;
			$localeParent = $this->Acc_coa_locale->get_one_by($condsss);

			$accconds['coa_id']=$defaultInstance->id;
			$accCoalanguage = $this->Acc_coa_locale->get_all_by( $accconds , $this->pag['per_page'], $this->uri->segment( 4 ) )->result();
			$data = array(
				'isError' => false, 
				'defaultInstance' =>  $defaultInstance, 
				'codePrefix'=>substr($defaultInstance->code,0,4), 
				'codeSuffix'=>substr($defaultInstance->code,4,7),
				'accCoaId'=>$defaultInstance->acc_coa_id ? $defaultInstance->acc_coa_id:"",
				'accCoaText'=>$defaultInstance->acc_coa_id ? $localeParent->caption:"",
				'journalsType'=>$defaultInstance->journals_type,
				'accCoaFlagText'=>get_msg($defaultInstance->acc_coa_flag),
				'language'=>$accCoalanguage,
				);    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		// for editing data
		if($this->input->get("addAction")){
			header('Content-Type: application/json');  
			$conds['id'] = $this->input->get("id");
			$defaultInstance = $this->Acc_coa->get_one_by($conds);
			if (!$defaultInstance) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
			$condss['coa_id'] = $defaultInstance->id;
			$condss['language_enum'] = $langEnumKey;
			$locale = $this->Acc_coa_locale->get_one_by($condss);
			if (!$locale) {
				$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}

			if ($defaultInstance->acc_coa_flag == "CLS") {
                $defaultInstance->acc_coa_flag = "GRP";
            } else if ($defaultInstance->acc_coa_flag == "GRP") {
				$defaultInstance->acc_coa_flag = "HED";
            }

			$accconds['coa_id']=$defaultInstance->id;
			$accCoalanguage = $this->Acc_coa_locale->get_all_by( $accconds , $this->pag['per_page'], $this->uri->segment( 4 ) )->result();
			$data = array(
				'isError' => false, 
				'defaultInstance' =>  $defaultInstance, 
				'caption' =>  $locale->caption,
				'codePrefix'=>substr($defaultInstance->code,0,4), 
				'codeSuffix'=>substr($defaultInstance->code,4,7),
				'accCoaId'=>$defaultInstance->acc_coa_id ? $defaultInstance->acc_coa_id:"",
				'accCoaText'=>$defaultInstance->acc_coa_id ? $localeParent->caption:"",
				'journalsType'=>$defaultInstance->journals_type,
				'accCoaFlagText'=>get_msg($defaultInstance->acc_coa_flag),
				'language'=>$accCoalanguage,
				);    
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		if($this->input->get("reloadaccCoa")){
			header('Content-Type: application/json');  
			$coaOptionGroupList = $this->Acc_coa->coaOptionGroup();
			$data = array(
				'isError' => false, 
				'obj' =>  $coaOptionGroupList
				); 
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		
		}
		$this->load_template('chart_of_accounts/list', $this->data, true );
	}

	function listpagination()
	{
		
		$dataReturns = array();
		$langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
		$querySQL = "SELECT * FROM acc_coa_locale_view where locale_language_enum ='".$langEnumKey."' ORDER BY code ASC";
		$resultSet = $this->Acc_period->queryPrepare($querySQL);
		$totalCount = $resultSet->num_rows();
		$serial = 0;
		$total = 0;
			
		$identifyFlag = null;
		$listIcon = "";
		foreach ($resultSet->result() as $row)
		{
			$identifyFlag=$row->acc_coa_flag;
			if ($identifyFlag=="HED"){
                $listIcon = '&nbsp;&nbsp;<i class="fa fa-list text-success viewData"></i>&nbsp;&nbsp;<i class="fa fa-pencil-square-o text-navy text-success editData"></i>&nbsp;&nbsp;<i class="fa fa-trash text-navy text-danger deleteData"></i>';
            }elseif($identifyFlag=="CLS"){
                $listIcon = '&nbsp;&nbsp;<i class="fa fa-list text-success viewData"></i></i>&nbsp;&nbsp;<i class="fa fa-pencil-square-o text-navy text-success editData"></i>';
            }else{
                $listIcon = '&nbsp;&nbsp;<i class="fa fa-list text-success viewData"></i></i>&nbsp;&nbsp;<i class="fa fa-plus-square-o text-success addData">&nbsp;&nbsp;<i class="fa fa-pencil-square-o text-navy text-success editData"></i>';
            }
			$dataReturns[]=array('id' => $row->coa_oid, 'parent' => ($row->parent_id ? $row->parent_id : '#'),'text' => $row->code." - ".$row->caption."&nbsp;&nbsp;".$listIcon);
			$listIcon = "";
		}
		echo json_encode($dataReturns);
		exit();
	}


	/**
	 * Create new one
	 */
	function write() {
		header('Content-Type: application/json');  
		$rules = array(
			array(
					'field' => 'codePrefix',
					'label' => get_msg('codePrefix'),
					'rules' => 'required'
			),array(
					'field' => 'codeSuffix',
					'label' => get_msg('codeSuffix'),
					'rules' => 'required'
			),array(
				'field' => 'accCoaFlag',
				'label' => get_msg('accCoaFlag'),
				'rules' => 'required'
			)
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}

		$conds['code'] = $this->input->post("codePrefix")."".$this->input->post("codeSuffix");
		$checkData = $this->Acc_coa->get_one_by($conds);
		if ($checkData->id) {
			$data = array('isError' => true, 'message' =>  get_msg('condition_mismatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		$accCoaconds['id'] = $this->input->post("accCoa");
		$accCoa = $this->Acc_coa->get_one_by($accCoaconds);
		if (!$accCoa->id) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$logged_in_user = $this->ps_auth->get_user_info();

		$data = array(
			'code' => $this->input->post("codePrefix")."".$this->input->post("codeSuffix"),
			'caption' => $this->input->post("codePrefix")."".$this->input->post("codeSuffix"),
			'accounts_type' => $accCoa->accounts_type,
			'acc_coa_id' => $this->input->post("accCoa"),
			'is_card_default' => $this->input->post("isCardDefault"),
			'acc_coa_flag' => $this->input->post("accCoaFlag"),
			'is_taxable' => $this->input->post("isTaxable"),
			'journals_type' => $this->input->post("journals_type"),
			'remark' => $this->input->post("remark"),
			'date_created' =>date("Y-m-d H:i:s"),
			'created_by' => $logged_in_user->user_id,
			'last_updated' =>date("Y-m-d H:i:s"),
			'active_status' => "Active",
		);
		$inserted=$this->Acc_coa->accSave($data);
		
		foreach($this->input->post('languageEnum') as $key =>$sinleDetails) { 
			if($this->input->post('languageEnum')[$key] && $this->input->post('caption')[$key]){
				$dataDtls = array(
				'coa_id' => $inserted,
				'caption' => $this->input->post('caption')[$key],
				'language_enum' => $this->input->post('languageEnum')[$key],
				'date_created' =>date("Y-m-d H:i:s"),
				'created_by' => $logged_in_user->user_id,
				'last_updated' =>date("Y-m-d H:i:s"),
				'active_status' => "Active",
				);
				$this->Acc_coa_locale->save($dataDtls);
			}
		}
	
		$arr = array('isError' => false, 'message' =>  get_msg('save_successfully'));    
		echo json_encode( $arr );
		exit;
	}


	/**
	 * Update one
	 */
	function editsubmit() {
		header('Content-Type: application/json');  
		$rules = array(
			array(
					'field' => 'codePrefix',
					'label' => get_msg('codePrefix'),
					'rules' => 'required'
			),array(
					'field' => 'codeSuffix',
					'label' => get_msg('codeSuffix'),
					'rules' => 'required'
			),array(
				'field' => 'accCoaFlag',
				'label' => get_msg('accCoaFlag'),
				'rules' => 'required'
			)
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$conds['id'] = $this->input->post("id");
		$defaultInstance = $this->Acc_coa->get_one_by($conds);
		if (!$defaultInstance->id) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		$condssss['code'] = $this->input->post("codePrefix")."".$this->input->post("codeSuffix");
		$checkData = $this->Acc_coa->get_one_by($condssss);
		if ($checkData->id && $checkData->id !=$this->input->post("id")) {
			$data = array('isError' => true, 'message' =>  get_msg('condition_mismatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}


		$accCoaconds['id'] = $this->input->post("accCoa");
		$accCoa = $this->Acc_coa->get_one_by($accCoaconds);
		if (!$accCoa->id) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$logged_in_user = $this->ps_auth->get_user_info();

		$data = array(
			'code' => $this->input->post("codePrefix")."".$this->input->post("codeSuffix"),
			'caption' => $this->input->post("codePrefix")."".$this->input->post("codeSuffix"),
			'accounts_type' => $accCoa->accounts_type,
			'acc_coa_id' => $this->input->post("accCoa"),
			'is_card_default' => $this->input->post("isCardDefault"),
			'acc_coa_flag' => $this->input->post("accCoaFlag"),
			'is_taxable' => $this->input->post("isTaxable"),
			'journals_type' => $this->input->post("journals_type"),
			'remark' => $this->input->post("remark"),
			'last_updated' =>date("Y-m-d H:i:s"),
		);
		$inserted=$this->Acc_coa->accSave($data, $this->input->post("id"));
		$deleteconds['coa_id'] = $this->input->post("id");
		$this->Acc_coa_locale->delete_all_records($deleteconds);
		foreach($this->input->post('languageEnum') as $key =>$sinleDetails) { 
			if($this->input->post('languageEnum')[$key] && $this->input->post('caption')[$key]){
				$dataDtls = array(
				'coa_id' => $this->input->post("id"),
				'caption' => $this->input->post('caption')[$key],
				'language_enum' => $this->input->post('languageEnum')[$key],
				'date_created' =>date("Y-m-d H:i:s"),
				'created_by' => $logged_in_user->user_id,
				'last_updated' =>date("Y-m-d H:i:s"),
				'active_status' => "Active",
				);
				$this->Acc_coa_locale->save($dataDtls);
			}
		}
	
		$arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));    
		echo json_encode( $arr );
		exit;
	}

	function remove(){
		$rules = array(
			array(
					'field' => 'id',
					'label' => get_msg('id'),
					'rules' => 'required'
			)
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$conds['id'] = $this->input->post("id");
		$_data = $this->Acc_coa->get_one_by($conds);
		if (!$_data->id) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message').$this->input->post("id"));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		$Acc_ledgerconds['acc_coa_id'] = $this->input->post("id");
		$ledger_data = $this->Acc_ledger->get_one_by($Acc_ledgerconds);
		if ($ledger_data->id) {
			$data = array('isError' => true, 'message' =>  get_msg('data_already_in_use'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$deleteconds['coa_id'] = $this->input->post("id");
		$this->Acc_coa_locale->delete_all_records($deleteconds);
		
		$deleteMstconds['id'] = $this->input->post("id");
		$this->Acc_coa->delete_all_records($deleteMstconds);
		$arr = array('isError' => false, 'message' =>  get_msg('deleted_successfully'));    
		echo json_encode( $arr );
		exit;

	}

}