<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users crontroller for BE_USERS table
 */
class Delivery_boys extends BE_Controller {

	/**
	 * Constructs required variables
	 */
	function __construct() {
		parent::__construct( MODULE_CONTROL, 'USERS' );
	}

	/**
	 * List down the registered users
	 */
	function index() {

		//system users filter
		$conds = array( 'register_role_id' => 5 );
		$conds['status'] = 1;
		// get rows count
		$this->data['rows_count'] = $this->User->count_all_by($conds);

		// get users
		$this->data['users'] = $this->User->get_all_by($conds, $this->pag['per_page'], $this->uri->segment( 4 ) );

		// load index logic
		parent::delivery_users_index();
	}

	function due_fees_reports(){
        $langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
        $this->data['action_title'] = "Orders";
        $shop_countSQL = "SELECT COUNT(DISTINCT(`shop_id`)) AS shop, 
            (SELECT COUNT(DISTINCT(`user_id`)) FROM mk_transactions_header) AS customer,
            (SELECT COUNT(DISTINCT(`id`)) FROM mk_transactions_header) AS orders,
            (SELECT COUNT(payment_method) FROM mk_transactions_header WHERE payment_method='paytabs') AS paytabs,
            (SELECT COUNT(payment_method) FROM mk_transactions_header WHERE payment_method='POSTerminal') AS POSTerminal,
            (SELECT COUNT(payment_method) FROM mk_transactions_header WHERE payment_method='COD') AS COD,
            (SELECT COUNT(trans_status_id) FROM mk_transactions_header WHERE trans_status_id=1) AS orderreceive,
            (SELECT COUNT(trans_status_id) FROM mk_transactions_header WHERE trans_status_id=2) AS orderpreparing,
            (SELECT COUNT(trans_status_id) FROM mk_transactions_header WHERE trans_status_id=3) AS orderready,
            (SELECT COUNT(trans_status_id) FROM mk_transactions_header WHERE trans_status_id=4) AS orderontheway,
            (SELECT COUNT(trans_status_id) FROM mk_transactions_header WHERE trans_status_id=5) AS orderdelivered,
            (SELECT COUNT(reject_status) FROM mk_transactions_header WHERE reject_status=1) AS orderrejected
            FROM mk_transactions_header";
        $shop_countResultSet = $this->Acc_period->queryPrepare($shop_countSQL);
        $this->data['summeryReport'] = $shop_countResultSet->row();

        $this->load_template('delivery_boys/due_fees_reports', $this->data, true );
    }

    public function due_fees_reports_list()
    {

        $iDisplayStart = $this->input->get("iDisplayStart")? intval($this->input->get("iDisplayStart")):0;
        $iDisplayLength = $this->input->get("iDisplayLength")? intval($this->input->get("iDisplayLength")) :10;
        $sSortDir = $this->input->get("sSortDir_0");
        $iSortingCol = $this->input->get("iSortCol_0")? $this->input->get("iSortCol_0") : null;
        $sSearch = $this->input->get("sSearch") ? $this->input->get("sSearch") : null;
        if($sSearch){
            $sSearch = "% ".$sSearch." %";
        }

        $dataReturns = array();

        $langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
        $sqlWhere = "WHERE mst.id !=0 ";
        $sqlWhere .= ($this->input->get("orderStartDate") && $this->input->get("orderEndDate")) ? ((" AND mst.added_date >= '".$this->input->get("orderStartDate")."'").(" AND mst.added_date <= '".$this->input->get("orderEndDate")."'")) : "";
        $sqlWhere .= ($this->input->get("deliveryStartDate") && $this->input->get("deliveryEndDate")) ? ((" AND mst.delivery_date >= '".$this->input->get("deliveryStartDate")."'").(" AND mst.delivery_date <= '".$this->input->get("deliveryEndDate")."'")) : "";
        $sqlWhere .= $this->input->get("invoiceNo") ? " AND mst.id='" .$this->input->get("invoiceNo"). "'" : "";
        $sqlWhere .= $this->input->get("transStatus") ? " AND mst.trans_status_id='" .$this->input->get("transStatus"). "'" : "";
        $sqlWhere .= $this->input->get("paymentMethod") ? " AND mst.payment_method='" .$this->input->get("paymentMethod"). "'" : "";
        $sqlWhere .= $this->input->get("paymentStatus") =='0' || $this->input->get("paymentStatus") =='1'? " AND mst.payment_status='" .$this->input->get("paymentStatus"). "'" : "";
        $sqlWhere .= $this->input->get("rejection") =='0' || $this->input->get("rejection") =='1'? " AND mst.reject_status='" .$this->input->get("rejection"). "'" : "";
        $sqlWhere .= $this->input->get("delivery_boy") ? " AND mst.assign_to='" .$this->input->get("delivery_boy"). "'" : "";
        $sqlWhere .= $this->input->get("shop") ? " AND mst.shop_id='" .$this->input->get("shop"). "'" : "";
        $sqlWhere .= $this->input->get("customerMobile") ? " AND mst.contact_phone LIKE '%" .$this->input->get("customerMobile"). "%'" : "";

        $sqlWhere .= $this->input->get("sSearch") ? " AND (mst.contact_name LIKE '%".$this->input->get("sSearch")."%' OR mst.contact_phone LIKE '%".$this->input->get("sSearch")."%' OR mst.payment_reference LIKE '%".$this->input->get("sSearch")."%' OR mst.shipping_address_1 LIKE '%".$this->input->get("sSearch")."%' OR mst.shipping_city LIKE '%".$this->input->get("sSearch")."%' OR mst.shipping_phone LIKE '%".$this->input->get("sSearch")."%' OR mst.delivery_date LIKE '%".$this->input->get("sSearch")."%' OR mst.payment_method LIKE '%".$this->input->get("sSearch")."%')" : "";
        $querySQL = "SELECT mst.id, mst.user_id, mst.shop_id, mst.sub_total_amount, mst.discount_amount, mst.coupon_discount_amount, mst.tax_amount, mst.tax_percent, mst.shipping_amount, mst.shipping_tax_percent, mst.shipping_method_amount, mst.shipping_method_name, mst.balance_amount, mst.total_item_amount, mst.total_item_count, mst.contact_name, mst.contact_phone, mst.payment_method, mst.payment_status, mst.added_date, mst.trans_status_id, mst.shipping_first_name, mst.shipping_last_name, mst.shipping_company, mst.shipping_address_1, mst.shipping_address_2, mst.shipping_state, mst.shipping_city, mst.shipping_phone, mst.assign_to, mst.delivery_time_from, mst.delivery_time_to, mst.time_slot_id, mst.delivery_date, mst.reject_status, mst.rejected_by, transStatus.title as transStatusTitle, usr.user_name as deliveryBoyName, usr.user_phone as deliveryBoyPhone, shop.name as shopName FROM mk_transactions_header AS mst LEFT JOIN mk_transactions_status transStatus ON mst.trans_status_id = transStatus.id LEFT JOIN core_users usr ON usr.user_id = mst.assign_to LEFT JOIN mk_shops shop ON shop.id = mst.shop_id ".$sqlWhere." ORDER BY mst.id DESC ";
        $resultSet = $this->Acc_period->queryPrepare($querySQL);
        $totalCount =$resultSet->num_rows();

        $querySQL .= " LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart."";

//        echo $querySQL;
//die();
        $resultSet = $this->Acc_period->queryPrepare($querySQL);
        $serial = 0;

        $actionList = array();

        $actionListConfirm = array();
//        $actionList[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
//        $actionList[]=array('actionName' => 'update', 'actionClass' => 'update', 'titleName' => get_msg( 'update_data'), 'iconName' => 'fa fa-pencil-square-o text-navy');
//        $actionList[]=array('actionName' => 'confirm', 'actionClass' => 'confirm', 'titleName' => get_msg( 'Confirm_Data'), 'iconName' => 'fa fa-check-circle text-warning');
//        $actionList[]=array('actionName' => 'delete', 'actionClass' => 'delete', 'titleName' => get_msg( 'Delete_Data'), 'iconName' => 'fa fa-trash text-danger');
//        $actionListConfirm[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
//        $actionListConfirm[]=array('actionName' => 'index', 'actionClass' => 'print', 'titleName' => get_msg( 'Print_Report'), 'iconName' => 'fa fa-print text-navy');


        foreach ($resultSet->result() as $row)
        {
            $serial++;
            $statusLabel="";
            if ($row->trans_status_id == 1) {
                $statusLabel='<span class="badge badge-danger">'.get_msg(''.$row->transStatusTitle.'').'</span>';
            } else if ($row->trans_status_id == 2) {
                $statusLabel='<span class="badge badge-info">'.get_msg(''.$row->transStatusTitle.'').'</span>';
            } else if ($row->trans_status_id == 3) {
                $statusLabel='<span class="badge badge-success">'.get_msg(''.$row->transStatusTitle.'').'</span>';
            } else {
                $statusLabel='<span class="badge badge-primary">'.get_msg(''.$row->transStatusTitle.'').'</span>';
            }

            $payment_status='';
            $reject_status='';
            if($row->reject_status ==1){
                $reject_status=' - <span class="badge badge-danger">'.get_msg('rejected').'</span>';
                $statusLabel=' - <span class="badge badge-danger">'.get_msg('rejected').'</span>';
            }

            if($row->payment_status ==1){
                $payment_status='<span class="btn btn-sm btn-success">'.get_msg('paid').$reject_status.'</span>';
            }else{
                $payment_status='<span class="badge badge-danger">'.get_msg('unpaid').$reject_status.'</span>';
            }


            $assign='';
            if(!$row->assign_to){
                $assign='<a class="pull-left btn btn-sm btn-success" href="'.site_url("admin/orders/assign/" . $row->id).'">'.get_msg('assign').'</a>';
            }else if($row->trans_status_id !=5){
                $assign='<a class="pull-left btn btn-sm btn-warning" href="'.site_url("admin/orders/assign/" . $row->id).'">'.get_msg('re_assign').'</a>';
            }else if ($row->trans_status_id ==5 && $this->session->userdata( 'user_id' ) == "usr75023528330eed4cc55f0e1bdd463cfe") {
                $assign='<a class="pull-left btn btn-sm btn-warning" href="'.site_url("admin/orders/assign/" . $row->id).'">'.get_msg('re_assign').'</a>';
            }
            if($row->reject_status ==1){
                $assign='';
            }
            $accounting_status='';
            if($row->trans_status_id==5 && $row->accounting_status==0) {
                $accounting_status='<span class="col-md-6 no-padding"><a href="" referenceid="'.$row->id.'" class="confirm-reference" data-title="Confirm_Data"><i class="fa fa-check-circle text-warning"></i></a>&nbsp;</span>';
            }
            $detail='<a class="pull-right btn btn-sm btn-primary" target="_blank" href="'.site_url("admin/orders/detail/" . $row->id).'">'.get_msg('details').'</a>';
            $orderedit='';
            if($row->trans_status_id !=5){
                $orderedit='<a class="pull-left btn btn-sm btn-warning" target="_blank" href="'.site_url("admin/orders/orderedit/" . $row->id).'">'.get_msg('edit').'</a>';
            }else if ($row->trans_status_id ==5 && $this->session->userdata( 'user_id' ) == "usr75023528330eed4cc55f0e1bdd463cfe") {
                $orderedit='<a class="pull-left btn btn-sm btn-warning" target="_blank" href="'.site_url("admin/orders/orderedit/" . $row->id).'">'.get_msg('edit').'</a>';
            }

            $printDetailsPdf='<a class="pull-right btn btn-sm btn-primary" target="_blank" href="'.site_url("admin/orders/printDetailsPdf/" . $row->id).'"><i class="fa fa-print" aria-hidden="true"></i></a>';
            $shop_total=($row->sub_total_amount + $row->tax_amount);
            $dataReturns[]=array(
                'DT_RowId' => $row->id,
                'DataRejected' => (($row->reject_status ==1) ? true: false),
                'accessibleUrl' => (($row->confirm_status == "Draft") ? $actionList : $actionListConfirm),
                'activeStatus' => get_msg( 'Active'),
                0 => $serial,
                1 => sprintf("%07d", $row->id),
                2 =>  '<a class="shopname" href="'.site_url("admin/dashboard/index/" . $row->shop_id).'">'.$row->shopName.'</a>',
                3 => $row->contact_name . "( ".get_msg('contact').": " . $row->contact_phone . " )",
                4 => $row->shipping_city,
                5 => $statusLabel,
                6 => number_format($shop_total,2),
                7 => $row->added_date,
                8 => $row->delivery_date? $row->delivery_date." (".$row->delivery_time_from." - ".$row->delivery_time_to.")" :"",
                9 => get_msg(''.$row->payment_method.''),
                10 => $payment_status,
                11 => $row->deliveryBoyName."-".$row->deliveryBoyPhone,
                12 => $accounting_status,
                13 => $detail,
                14 => $printDetailsPdf,
                15 => $orderedit
            );
        }

        $gridData=array();
        if ($totalCount == 0) {
            $gridData=array('iTotalRecords' => 0, 'iTotalDisplayRecords' =>0, 'aaData'=> array());
            echo json_encode($gridData);
            exit();
        }

        $gridData=array('iTotalRecords' => $totalCount, 'iTotalDisplayRecords' =>$totalCount, 'aaData'=> $dataReturns);
        echo json_encode($gridData);
        exit();
    }

	/**
	 * Searches for the first match in system users
	 */
	function search() {

		// breadcrumb urls
		$data['action_title'] = get_msg( 'user_search' );

		// condition with search term
		if($this->input->post('submit') != NULL ){

			$conds = array( 'searchterm' => $this->searchterm_handler( $this->input->post( 'searchterm' )));

			if($this->input->post('searchterm') != "") {
				$conds['searchterm'] = $this->input->post('searchterm');
				$this->data['searchterm'] = $this->input->post('searchterm');
				$this->session->set_userdata(array("searchterm" => $this->input->post('searchterm')));
			} else {
				
				$this->session->set_userdata(array("searchterm" => NULL));
			}
		} else {
			//read from session value
			if($this->session->userdata('searchterm') != NULL){
				$conds['searchterm'] = $this->session->userdata('searchterm');
				$this->data['searchterm'] = $this->session->userdata('searchterm');
			}
		}
		$conds['system_role_id'] = 5;
		$conds['status'] = 1;
		
		$this->data['rows_count'] = $this->User->count_all_by( $conds );

		$this->data['users'] = $this->User->get_all_by( $conds, $this->pag['per_page'], $this->uri->segment( 4 ));
		
		parent::system_users_search();
	}

    /**
     * Create the user
     */
    function delivery_boys_details($user_id) {

        // breadcrumb
        $this->data['action_title'] = get_msg( 'user_add' );
        $this->data['user'] = $this->User->get_one( $user_id );
        $this->data['user_id'] = $user_id;
        $conds = array();
        if ( $user_id) {
            $conds['user_id'] = $user_id;
        }
        $this->data['user_locations'] = $this->User_location->get_all_by( $conds)->result();
        // echo "<pre>";
        // print_r($this->data['user_locations']);
        // echo "</pre>";
        // die();
        // call add logic
        $this->load_template('delivery_boys/delivery_boys_details', $this->data, true );
    }

    function delivery_boys_maps() {
        $conds = array();
        if($this->input->get("showPolyline")){
            $conds['user_id'] = $this->input->get("user_id");
            $mapdata = $this->User_location->get_all_by( $conds)->result();
            $data = array('isError' => false, 'mapdata' => $mapdata);    
            echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            exit;
        }
    }

	/**
	 * Create the user
	 */
	function add() {

		// breadcrumb
		$this->data['action_title'] = get_msg( 'user_add' );

		// call add logic
		parent::deliveryboyadd();
	}

	/**
	 * Update the user
	 */
	function edit( $user_id ) {
		// breadcrumb
		$this->data['action_title'] = get_msg( 'user_edit' );

		// load user
		$this->data['user'] = $this->User->get_one( $user_id );

		// call update logic
		parent::deliveryboyedit( $user_id );
	}

	
	/**
	 * Saving User Info logic
	 *
	 * @param      boolean  $user_id  The user identifier
	 */
	function save( $user_id = false ) {
		
		
		// prepare user object and permission objects
		$user_data = array();
		// save user_id
		if ( $this->has_data( 'user_id' )) {
			$user_data['user_id'] = $this->get_data( 'user_id' );
		}
		// save username
		if ( $this->has_data( 'user_name' )) {
			$user_data['user_name'] = $this->get_data( 'user_name' );
		}

		// save user email
		if( $this->has_data( 'user_email' )) {
			$user_data['user_email'] = $this->get_data( 'user_email' );
		}

		// save user phone
		if( $this->has_data( 'user_phone' )) {
			$user_data['user_phone'] = $this->get_data( 'user_phone' );
		}
		// save user contract type
		if( $this->has_data( 'contract_type' )) {
			$user_data['contract_type'] = $this->get_data( 'contract_type' );
		}
		// save user contract type
		if( $this->has_data( 'commission' )) {
			$user_data['commission'] = $this->get_data( 'commission' );
		}
		// save password if exists or not empty
		if ( $this->has_data( 'user_password' ) 
			&& !empty( $this->get_data( 'user_password' ))) {
			$user_data['user_password'] = md5( $this->get_data( 'user_password' ));
		}

		
		$user_data['role_id'] = 5;
			

		if($user_data['role_id'] == 1){
			$is_shop_admin = 1;
			$shop_id = $this->input->post('shop_id');
		} else {
			$is_shop_admin = 0;
			$shop_id = 0;
		}

		$user_data['is_shop_admin'] = $is_shop_admin;
		
		$permissions = ( $this->get_data( 'permissions' ) != false )? $this->get_data( 'permissions' ): array();
		
		$permissions['module_id'] = 5;
			// save data
			if ( ! $this->User->save_user( $user_data, $permissions, $user_id )) {
			// if there is an error in inserting user data,	

				$this->set_flash_msg( 'error', get_msg( 'err_model' ));
			} 
		

			redirect( $this->module_site_url());
		
}

	/**
	 * Determines if valid input.
	 *
	 * @return     boolean  True if valid input, False otherwise.
	 */
	function is_valid_input( $user_id = 0 ) {
		
		$email_rule = 'required|valid_email|callback_is_valid_email['. $user_id  .']';
		$rule = 'required';

		$this->form_validation->set_rules( 'user_email', get_msg( 'user_email' ), $email_rule);
		$this->form_validation->set_rules( 'user_name', get_msg( 'user_name' ), $rule );
		
		$user = $this->User->get_one( $user_id );

		if ( !$user->user_is_sys_admin ) {
		// if updated user is not system admin,
			
			$this->form_validation->set_rules( 'permissions[]', get_msg( 'allowed_modules'), $rule );
		}
		
		if ( $user_id == 0 ) {
		// password is required if new user
			
			$this->form_validation->set_rules( 'user_password', get_msg( 'user_password' ), $rule );
			$this->form_validation->set_rules( 'conf_password', get_msg( 'conf_password' ), $rule .'|matches[user_password]' );
		}


		return true;
	}

	/**
	 * Determines if valid email.
	 *
	 * @param      <type>   $email  The user email
	 * @param      integer  $user_id     The user identifier
	 *
	 * @return     boolean  True if valid email, False otherwise.
	 */
	function is_valid_email( $email, $user_id = 0 )
	{		

		if ( strtolower( $this->User->get_one( $user_id )->user_email ) == strtolower( $email )) {
		// if the email is existing email for that user id,
			
			return true;
		} else if ( $this->User->exists( array( 'user_email' => $_REQUEST['user_email'] ))) {
		// if the email is existed in the system,

			$this->form_validation->set_message('is_valid_email', get_msg( 'err_dup_email' ));
			return false;
		}

		return true;
	}

	/**
	 * Ajax Exists
	 *
	 * @param      <type>  $user_id  The user identifier
	 */
	function ajx_exists( $user_id = null )
	{
		$user_email = $_REQUEST['user_email'];
		
		if ( $this->is_valid_email( $user_email, $user_id )) {
		// if the user email is valid,
			
			echo "true";
		} else {
		// if the user email is invalid,

			echo "false";
		}
	}

	/**
	 * Delete the record
	 * 1) delete registered user
	 * 2) check transactions
	 */
	function delete( $user_id ) {

		// start the transaction
		$this->db->trans_start();

		// check access
		$this->check_access( DEL );

		$conds['user_id'] = $user_id;

		$this->User_shop->delete_by( $conds );
		// delete users
		if ( !$this->ps_delete->delete_user( $user_id )) {

			// set error message
			$this->set_flash_msg( 'error', get_msg( 'err_model' ));

			// rollback
			$this->trans_rollback();

			// redirect to list view
			redirect( $this->module_site_url());
		}
			
		/**
		 * Check Transcation Status
		 */
		if ( !$this->check_trans()) {

			$this->set_flash_msg( 'error', get_msg( 'err_model' ));	
		} else {
        	
			$this->set_flash_msg( 'success', get_msg( 'success_user_delete' ));
		}
		
		redirect( $this->module_site_url());
	}

	/**
	 * Ban the user
	 *
	 * @param      integer  $user_id  The user identifier
	 */
	function ban( $user_id = 0 )
	{
		$this->check_access( BAN );
		
		$data = array( 'is_banned' => 1 );
			
		if ( $this->User->save( $data, $user_id )) {
			echo 'true';
		} else {
			echo 'false';
		}
	}
	
	/**
	 * Unban the user
	 *
	 * @param      integer  $user_id  The user identifier
	 */
	function unban( $user_id = 0 )
	{
		$this->check_access( BAN );
		
		$data = array( 'is_banned' => 0 );
			
		if ( $this->User->save( $data, $user_id )) {
			echo 'true';
		} else {
			echo 'false';
		}
	}
}
