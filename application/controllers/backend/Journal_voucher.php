<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users crontroller for BE_USERS table
 */
class Journal_voucher extends BE_Controller {

	/**
	 * Construt required variables
	 */
	function __construct() {

		parent::__construct( MODULE_CONTROL, 'Acc_coa' );
	}

	/**
	* Load Notification Sending Form
	*/
	function index() {
		$langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
		$this->data['action_title'] = "Journal Voucher";
		$this->data['currentPeriod'] = $this->Acc_coa->currentPeriod();
		$this->data['drcrTypeList'] = $this->Acc_coa->enumListLanguage("drcrType");
		$this->data['stakeHolderTypeList'] = $this->Acc_coa->enumListLanguage("stakeHolderType");
		$this->data['confirmStatusList'] = $this->Acc_coa->enumListLanguage("confirmStatus");
		$this->data['accPeriodList'] = $this->Acc_coa->allClosedPeriodOptions();
		$this->data['coaOptionGroup'] = $this->Acc_coa->coaOptionGroup();
		$this->data['coaClass'] = $this->Acc_coa->coaOptionDropDown(null, "CLS", null);
	// for showing data
	if($this->input->get("showAction")){
		$conds['id'] = $this->input->get("id");
		$_data = $this->Acc_vcr_mst->get_one_by($conds);
		if (!$_data) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$condss['id'] = $_data->acc_period_id;
		$accperiod = $this->Acc_period->get_one_by($condss);
		$detailsQuery="SELECT dtl.*, coa_local.caption AS coaCaption FROM acc_vcr_mst AS mst LEFT JOIN acc_vcr_dtl dtl ON mst.id = dtl.vcr_mst_id LEFT JOIN acc_coa coa ON dtl.acc_coa_id = coa.id LEFT JOIN acc_coa coa_grp ON coa.acc_coa_id = coa_grp.id LEFT JOIN acc_coa_locale coa_local ON coa_local.coa_id = coa.id where coa_local.language_enum ='".$langEnumKey."' AND dtl.vcr_mst_id=".$this->input->get("id")."";
		$resultSet = $this->Acc_period->queryPrepare($detailsQuery);
		$accVcrDtls=$resultSet->result();
		$data = array('isError' => false, 'defaultInstance' =>  $_data, 'acc_period'=>$accperiod->caption, 'stakeHolderType'=>get_msg($_data->stake_holder_type), 'activeStatus'=>get_msg($_data->active_status), 'accVcrDtls'=>$accVcrDtls);    
		echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
		exit;
	}
	// for editing data
	if($this->input->get("editData")){
		$conds['id'] = $this->input->get("id");
		$_data = $this->Acc_vcr_mst->get_one_by($conds);
		if (!$_data) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		if ($_data->confirm_status != "Draft") {
			$data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$condss['id'] = $_data->acc_period_id;
		$accperiod = $this->Acc_period->get_one_by($condss);
		$detailsQuery="SELECT dtl.*, coa_local.caption AS coaCaption FROM acc_vcr_mst AS mst LEFT JOIN acc_vcr_dtl dtl ON mst.id = dtl.vcr_mst_id LEFT JOIN acc_coa coa ON dtl.acc_coa_id = coa.id LEFT JOIN acc_coa coa_grp ON coa.acc_coa_id = coa_grp.id LEFT JOIN acc_coa_locale coa_local ON coa_local.coa_id = coa.id where coa_local.language_enum ='".$langEnumKey."' AND dtl.vcr_mst_id=".$this->input->get("id")."";
		$resultSet = $this->Acc_period->queryPrepare($detailsQuery);
		$accVcrDtlsResult=$resultSet->result();
		$accVcrDtls = array();
		$accCoa="";
		$drAmountReadonly = '';
        $crAmountReadonly = '';
		foreach($accVcrDtlsResult as $dtl){
			if ($dtl->drcr_type == "Debit") {
				$drAmountReadonly = '';
				$crAmountReadonly = 'readonly';
			}else{
				$drAmountReadonly = 'readonly';
				$crAmountReadonly = '';
			}
			$accVcrDtls[]=array('id'=>$dtl->id,'accCoaId'=>$dtl->acc_coa_id,'accCoaText'=>$dtl->coaCaption,'stakeHolderType'=> ($dtl->stake_holder_type ? $dtl->stake_holder_type : ''),'stakeholderId'=> ($dtl->stakeholder_id ? $dtl->stakeholder_id : ''),'stakeholderName'=> ($dtl->stakeholder_name ? $dtl->stakeholder_name : ''),'stakeholderText'=> ($dtl->stakeholder_name ? $dtl->stakeholder_name : ''), 'drAmount'=>$dtl->dr_amount,'crAmount'=>$dtl->cr_amount,'drcrType'=>$dtl->drcr_type,'narration'=>$dtl->narration, 'drAmountReadonly'=> $drAmountReadonly, 'crAmountReadonly'=> $crAmountReadonly);
			
		}

		$data = array('isError' => false, 'defaultInstance' =>  $_data, 'acc_period'=>$accperiod, 'stakeHolderType'=>get_msg($_data->stake_holder_type), 'activeStatus'=>get_msg($_data->active_status), 'accVcrDtls'=>$accVcrDtls, 'accCoa'=>$accCoa);    
		echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
		exit;
	}
	if ($this->input->get("stakeholderSearch")) {
		$stakHldrDropdownList = $this->Acc_coa->stakHldrDropdownList($this->input->get());
		echo json_encode($stakHldrDropdownList);
		exit();
	}
	if ($this->input->get("stakeholderSearchRpt")) {
		$stakHldrDropdownList = $this->Acc_coa->stakHldrDropdownList($this->input->get());
		echo json_encode($stakHldrDropdownList);
		exit();
	}
		$this->load_template('journal_voucher/list', $this->data, true );
	}

	public function voucher_list()
	{

		$iDisplayStart = $this->input->get("iDisplayStart")? intval($this->input->get("iDisplayStart")):0;
		$iDisplayLength = $this->input->get("iDisplayLength")? intval($this->input->get("iDisplayLength")) :10;
		$sSortDir = $this->input->get("sSortDir_0");
		$iSortingCol = $this->input->get("iSortCol_0")? $this->input->get("iSortCol_0") : null;
		$sSearch = $this->input->get("sSearch") ? $this->input->get("sSearch") : null;
		if($sSearch){
			$sSearch = "% ".$sSearch." %";
		}
		
		$dataReturns = array();
	
		$langEnumKey = $this->session->userdata('language_code') ? $this->session->userdata('language_code') : 'ar';
		$sqlWhere = "AND coa_local.language_enum ='".$langEnumKey."'";
		$sqlWhere .= ($this->input->get("trans_date") && $this->input->get("trans_date_to")) ? ((" AND mst.trans_date >= '".$this->input->get("trans_date")."'").(" AND mst.trans_date <= '".$this->input->get("trans_date_to")."'")) : "";
		$sqlWhere .= $this->input->get("confirm_status") ? " AND mst.confirm_status='" .$this->input->get("confirm_status"). "'" : "";
		$sqlWhere .= $this->input->get("stake_holder_type") ? " AND mst.stake_holder_type='".$this->input->get("stake_holder_type")."'" : "";
		$sqlWhere .= $this->input->get("stake_holder_name") ? " AND mst.stakeholder_name='" .$this->input->get("stake_holder_name"). "'" : "";

		$sqlWhere .= $this->input->get("acc_period") ? (" AND period.id=" . $this->input->get("acc_period")) : '';
		// $sqlWhere .= $this->input->get("acc_coa") ? (" AND  dtl.acc_coa_id=" .$this->input->get("acc_coa")) : '';
		// $sqlWhere .= $this->input->get("coa_class") ? (" AND coa_grp.acc_coa_id=" .$this->input->get("coa_class")) : '';
		$sqlWhere .= $this->input->get("sSearch") ? " AND mst.code LIKE '%".$this->input->get("sSearch")."%' " : "";
		$querySQL = "SELECT mst.id, mst.code AS code, mst.trans_date AS trans_date, mst.stakeholder_name AS stakeholder, period.caption AS period, mst.total_amount AS amount, 
		(SELECT group_concat(coa_local.caption, ', ')
        FROM acc_vcr_dtl dtl
                 LEFT JOIN acc_coa coa ON dtl.acc_coa_id = coa.id
                 LEFT JOIN acc_coa coa_grp ON coa.acc_coa_id = coa_grp.id
                 LEFT JOIN acc_coa_locale coa_local ON coa_local.coa_id = coa.id
        where mst.entity_type = 'Journal_Voucher'
        AND coa_local.language_enum ='".$langEnumKey."'      
          and dtl.vcr_mst_id = mst.id) AS Accounts2,
		mst.confirm_status AS confirm_status 
		FROM acc_vcr_mst AS mst 
		LEFT JOIN acc_period period on mst.acc_period_id = period.id 
		where mst.entity_type = 'Journal_Voucher' 
		ORDER BY mst.id LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart."";
		$resultSet = $this->Acc_period->queryPrepare($querySQL);
		$totalCount = $resultSet->num_rows();
		$serial = 0;
			$total = 0;
			
		$actionList = array();
			$actionListConfirm = array();
			$actionList[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
			$actionList[]=array('actionName' => 'update', 'actionClass' => 'update', 'titleName' => get_msg( 'update_data'), 'iconName' => 'fa fa-pencil-square-o text-navy');
			$actionList[]=array('actionName' => 'confirm', 'actionClass' => 'confirm', 'titleName' => get_msg( 'Confirm_Data'), 'iconName' => 'fa fa-check-circle text-warning');
			$actionList[]=array('actionName' => 'delete', 'actionClass' => 'delete', 'titleName' => get_msg( 'Delete_Data'), 'iconName' => 'fa fa-trash text-danger');
			$actionListConfirm[]=array('actionName' => 'show', 'actionClass' => 'show', 'titleName' => get_msg( 'Read_Data'), 'iconName' => 'fa fa-info-circle text-navy');
			$actionListConfirm[]=array('actionName' => 'index', 'actionClass' => 'print', 'titleName' => get_msg( 'Print_Report'), 'iconName' => 'fa fa-print text-navy');
			
		foreach ($resultSet->result() as $row)
		{
			$serial++;
			$dataReturns[]=array('DT_RowId' => $row->id, 'accessibleUrl' => (($row->confirm_status == "Draft") ? $actionList : $actionListConfirm),'activeStatus' => get_msg( 'Active'), 0 => $serial, 1 => $row->period,2 => $row->trans_date,3 => $row->stakeholder,4 => $row->Accounts2,5 => $row->amount,6 => "");
		}
		
		$gridData=array();
		if ($totalCount == 0) {
			$gridData=array('iTotalRecords' => 0, 'iTotalDisplayRecords' =>0, 'aaData'=> array());
			echo json_encode($gridData);
			exit();
		}

		$gridData=array('iTotalRecords' => $totalCount, 'iTotalDisplayRecords' =>$totalCount, 'aaData'=> $dataReturns);
		echo json_encode($gridData);
		exit();
	}

	



	/**
	 * Create new one
	 */
	function write() {
		header('Content-Type: application/json');  
		$rules = array(
			array(
					'field' => 'trans_date',
					'label' => get_msg('trans_date'),
					'rules' => 'required'
			),array(
					'field' => 'acc_period_id',
					'label' => get_msg('Period_Date'),
					'rules' => 'required'
			)
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}

		if ( $this->input->post('drAmountSumInput') != $this->input->post('crAmountSumInput') ) {
			$data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$data = array(
		'entity_type' => "Journal_Voucher",
		'caption' => get_msg('Journal_Voucher')." - ".$this->input->post('trans_date'),
		'acc_period_id' => $this->input->post('acc_period_id'),
		'trans_date' => $this->input->post('trans_date'),
		'reference_code' => $this->input->post('reference_code'),
		'narration' => $this->input->post('narration'),
		'confirm_status' => "Draft",
		'active_status' => "Active",
		);
		$inserted=$this->Acc_vcr_mst->accSave($data);
		
	
		$drAmount = 0.0;
		$crAmount = 0.0;
		foreach($this->input->post('drAmount') as $key =>$sinleDetails) { 
			//if($this->input->post('drAmount')[$key]){
				$drAmount += $this->input->post('drAmount')[$key];
				$crAmount += $this->input->post('crAmount')[$key];
				$dataDtls = array(
				'vcr_mst_id' => $inserted,
				'trans_date' => $this->input->post('trans_date'),
				'narration' => $this->input->post('narrationDtls')[$key],
				'acc_coa_id' => $this->input->post('accCoaId')[$key],
				'dr_amount' => $this->input->post('drAmount')[$key],
				'cr_amount' => $this->input->post('crAmount')[$key],
				'drcr_type' => $this->input->post('drcrType')[$key],
				'stake_holder_type' => $this->input->post('stakeHolderType')[$key],
				'stakeholder_id' => $this->input->post('stakeholderId')[$key],
				'stakeholder_name' => $this->input->post('stakeholderName')[$key],
				);
				$this->Acc_vcr_dtl->save($dataDtls);
			//}
		}
		
		$masterDtls = array(
			'total_amount' =>$drAmount
		);
		if($inserted && $this->Acc_vcr_mst->save($masterDtls, $inserted)){
			$arr = array('isError' => false, 'message' =>  get_msg('save_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('save_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}
	}

	function editsubmit() {
		header('Content-Type: application/json');  
		$rules = array(
			array(
				'field' => 'trans_date',
				'label' => get_msg('trans_date'),
				'rules' => 'required'
			),array(
					'field' => 'acc_period_id',
					'label' => get_msg('Period_Date'),
					'rules' => 'required'
			)
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$conds['id'] = $this->input->post("id");
		$_data = $this->Acc_vcr_mst->get_one_by($conds);
		if (!$_data->id) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$drAmount = 0.0;
		$crAmount = 0.0;
		if ( $this->input->post('drAmountSumInput') != $this->input->post('crAmountSumInput') ) {
			$data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$data = array(
		'entity_type' => "Journal_Voucher",
		'caption' => get_msg('Journal_Voucher')." - ".$this->input->post('trans_date'),
		'acc_period_id' => $this->input->post('acc_period_id'),
		'trans_date' => $this->input->post('trans_date'),
		'reference_code' => $this->input->post('reference_code'),
		'narration' => $this->input->post('narration'),
		'confirm_status' => "Draft",
		'active_status' => "Active",
		);
		$inserted=$this->Acc_vcr_mst->save($data, $this->input->post("id"));
		
	
		$drAmount = 0.0;

		foreach($this->input->post('drAmount') as $key =>$sinleDetails) { 
				$drAmount += $this->input->post('drAmount')[$key];
				$crAmount += $this->input->post('crAmount')[$key];
				$dataDtls = array(
				'vcr_mst_id' => $this->input->post("id"),
				'trans_date' => $this->input->post('trans_date'),
				'narration' => $this->input->post('narrationDtls')[$key],
				'acc_coa_id' => $this->input->post('accCoaId')[$key],
				'dr_amount' => $this->input->post('drAmount')[$key],
				'cr_amount' => $this->input->post('crAmount')[$key],
				'drcr_type' => $this->input->post('drcrType')[$key],
				'stake_holder_type' => $this->input->post('stakeHolderType')[$key],
				'stakeholder_id' => $this->input->post('stakeholderId')[$key],
				'stakeholder_name' => $this->input->post('stakeholderName')[$key],
				);
				$this->Acc_vcr_dtl->save($dataDtls, $this->input->post('idDtls')[$key]);
			
		}
		
		$masterDtls = array(
			'total_amount' =>$drAmount
		);
		if($this->input->post("id") && $this->Acc_vcr_mst->save($masterDtls, $this->input->post("id"))){
			$arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('update_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}
	}

	function confirmdata() {
		header('Content-Type: application/json');  
		$rules = array(
			array(
					'field' => 'id',
					'label' => get_msg('id'),
					'rules' => 'required'
			),array(
					'field' => 'confirmStatus',
					'label' => get_msg('confirmStatus'),
					'rules' => 'required'
			)
			
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$logged_in_user = $this->ps_auth->get_user_info();

		$conds['id'] = $this->input->post("id");
		$_data = $this->Acc_vcr_mst->get_one_by($conds);
		if (!$_data->id) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		
		if ($_data->total_amount==0) {
			$data = array('isError' => true, 'message' =>  get_msg('condition_missmatch'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		$periodconds['id'] = $_data->acc_period_id;
		$period_data = $this->Acc_period->get_one_by($periodconds);
		if (!$period_data->is_current) {
			$data = array('isError' => true, 'message' =>  get_msg('Period_already_closed'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		$dtlconds['vcr_mst_id'] = $this->input->post("id");
		$acc_vcr_dtlData = $this->Acc_vcr_dtl->get_all_by( $dtlconds , false, false );
		
		if ($acc_vcr_dtlData->num_rows() <2) {
			$data = array('isError' => true, 'message' =>  get_msg('Invalid_data'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		foreach($acc_vcr_dtlData->result() as $vcrDtl) { 
			$ledgerconds['acc_coa_id'] = $vcrDtl->acc_coa_id;
			$ledgerconds['acc_period_id'] = $_data->acc_period_id;
			$ledger_data = $this->Acc_ledger->get_one_by($ledgerconds);
			if($ledger_data->id){
				if($vcrDtl->voucher_status == "Disbursed"){
					$data = array('isError' => true, 'message' =>  get_msg('Voucher_Already_Disbursed'));
					echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
					exit;
                }else {
					$vcrDtldata = array('voucher_status' => "Disbursed");
					$this->Acc_vcr_dtl->save($vcrDtldata, $vcrDtl->id);
				}
				$ledgerdata = array(
					'current_dr' => $ledger_data->current_dr + $vcrDtl->dr_amount,
					'current_cr' => $ledger_data->current_cr + $vcrDtl->cr_amount
				);
				$this->Acc_ledger->save($ledgerdata, $ledger_data->id);
				
			}else{
				$data = array('isError' => true, 'message' =>  get_msg('Ledger_not_Found'));
				echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
				exit;
			}
		}

		$masterDtls = array(
			'confirm_status' =>"Confirm",
			'approve_status' =>"Approved",
			'approved_by' =>$logged_in_user->user_id,
			'last_updated' =>date("Y-m-d H:i:s"),
		);
		if($this->input->post("id") && $this->Acc_vcr_mst->save($masterDtls, $this->input->post("id"))){
			$arr = array('isError' => false, 'message' =>  get_msg('updated_successfully'));    
			echo json_encode( $arr );
			exit;
		}else{
			$arr = array('isError' => true, 'message' =>  get_msg('update_failed_please_try_again'));    
			echo json_encode( $arr );
			exit;
		}
	}



	function remove(){
		$rules = array(
			array(
					'field' => 'id',
					'label' => get_msg('id'),
					'rules' => 'required'
			)
		);

		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules( $rules );

		if ( $this->form_validation->run() == FALSE ) {
			$arr = array('isError' => true, 'message' =>  validation_errors());    
			echo json_encode( $arr );
			exit;
		}
		$conds['id'] = $this->input->post("id");
		$_data = $this->Acc_vcr_mst->get_one_by($conds);
		if (!$_data->id) {
			$data = array('isError' => true, 'message' =>  get_msg('notfound_message'));
			echo json_encode( $data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$deleteconds['vcr_mst_id'] = $this->input->post("id");
		$this->Acc_vcr_dtl->delete_all_records($deleteconds);
		
		$deleteMstconds['id'] = $this->input->post("id");
		$this->Acc_vcr_mst->delete_all_records($deleteMstconds);
		$arr = array('isError' => false, 'message' =>  get_msg('deleted_successfully'));    
		echo json_encode( $arr );
		exit;

	}

}