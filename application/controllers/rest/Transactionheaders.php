<?php
require_once( APPPATH .'libraries/REST_Controller.php' );
require_once( APPPATH .'libraries/braintree_lib/autoload.php' );
require_once( APPPATH .'libraries/stripe_lib/autoload.php' );



/**
 * REST API for Transaction Header
 */
class Transactionheaders extends API_Controller
{

	/**
	 * Constructs Parent Constructor
	 */
	function __construct()
	{
		parent::__construct( 'Transactionheader' );
	}

	/**
	 * Default Query for API
	 * @return [type] [description]
	 */
	function default_conds()
	{
		$conds = array();

		if ( $this->is_get ) {
		// if is get record using GET method
			$conds['order_by'] = 1;
			$conds['order_by_field'] = "added_date";
			$conds['order_by_type'] = "asc";
		}

		return $conds;
	}

	/**
	 * Convert Object
	 */
	function convert_object( &$obj )
	{
		// call parent convert object
		parent::convert_object( $obj );

		$this->ps_adapter->convert_transaction_header( $obj );
		
	}


	/**
	* When user submit transaction from app
	{
  "user_id" : "c4ca4238a0b923820dcc509a6f75849b",
 "shop_id" : "",
  "sub_total_amount" : "0",
  "discount_amount" : "500",
 "coupon_discount_amount" : "10",
 "tax_amount" : "12",
 "shipping_amount" : "10",
  "balance_amount"  : "5500", 
 "total_item_amount" : "100",
  "contact_name" : "Luiz",
  "contact_phone" : "777777",
  "is_cod" : "1",
 "is_paypal" : "0",
 "is_stripe" : "0",
 "payment_method_nonce" : "tok_1EA98KL6UpNJN4PNmb9atCPx",
  "trans_status_id" : "1",
  "currency_symbol" : "Kyats",
  "currency_short_form" : "Ks",
 "billing_first_name" : "B First Name",
  "billing_last_name" : "B Last Name",
  "billing_company" : "B Company",
  "billing_address_1" : "B Add 1",
  "billing_address_2" : "B Add 2",
  "billing_country"  : "B country",
  "billing_state" : "b state",
  "billing_city" : "b city",
  "billing_postal_code" : "b postal code",
  "billing_email" : "b email",
  "billing_phone" : "b phone",
 "shipping_first_name" : "s first name",
 "shipping_last_name" : "s last name",
 "shipping_company" : "s com",
 "shipping_address_1" : "s add 1",
 "shipping_address_2" : "s add 2",
 "shipping_country" : "s country", 
 "shipping_state" : "s state",
 "shipping_city" : "s city",
 "shipping_postal_code" : "s postal code",
 "shipping_email" : "s email",
 "shipping_phone" : "s phone",
 "shipping_tax_percent" : "",
 "tax_percent" : "",
 "shipping_method_amount" : "",
 "shipping_method_name" : "",
 "memo" : "",
  "details" : [
    {
  "shop_id":"1", 
    "product_id":"prd02174016d69d1d8e40d8b875a186944c",
    "product_name":"NoteBook 3",
    "product_attribute_id":"",
    "product_attribute_name":"",
  "product_attribute_price" : "", 
    "price":"460",
   "original_price":"460",
    "discount_price":"0",
    "discount_amount":"0",
    "qty":"1",
    "discount_value":"0",
    "discount_percent":"0",
    "currency_short_form":"USD",
    "currency_symbol":"$"
    },
  {
  "shop_id":"1",
    "product_id":"prd05f4aaabd3ec274ca406e1613ad83160",
    "product_name":"SD Card 2",
    "product_attribute_id":"",
    "product_attribute_name":"",
  "product_attribute_price" : "", 
    "price":"80",
    "original_price":"80",
    "discount_price":"0",
    "discount_amount":"0",
    "qty":"1",
    "discount_value":"0",
    "discount_percent":"0",
    "currency_short_form":"USD",
    "currency_symbol":"$"
    }
  ]
}

*/
	function submit_post() 
	{	

		/*

		$trans_details = $this->post( 'details' );

		$fail_trans = array();

		$fail_not_available_products = array();

		$fail_delete_products = array();

		$fail_price_change_products = array();

		$fail_trans = $this->ps_adapter->transaction_checking($trans_details);

		//print_r($fail_trans);

		//[0] - is for delete products
		if(count($fail_trans[0]) > 0) {

			// Rule 1 : Delete Product Checking
			for($g = 0; $g<count($fail_trans[0]); $g++) {

				$fail_delete_products[] = $fail_trans[0][$g];

			}


		} else if( count($fail_trans[1]) > 0 ) {

			// Rule 2 : Avaiable Product Checking
			for($j = 0; $j<count($fail_trans[1]); $j++) {

				$fail_not_available_products[] = $fail_trans[1][$j];

			} 


		} else if( count($fail_trans[2]) > 0 ) {

			// Rile 3 : Price Checking
			for($h = 0; $h<count($fail_trans[2]); $h++) {

				$fail_price_change_products[] = $fail_trans[2][$h];

			}

		}

		$fail_delete_products = array_unique($fail_delete_products);


		if( count($fail_delete_products) > 0 ) {


			$prds = $this->Product->get_all_in($fail_delete_products)->result();
			$this->custom_fail_response( $prds,true, "Product is deleted from the system." );

		} else if (count($fail_not_available_products) > 0) {

			$prds = $this->Product->get_all_in($fail_not_available_products)->result();
			$this->custom_fail_response( $prds,true, "Product is not available from the system." );

		} else if (count($fail_price_change_products) > 0) {

			$prds = $this->Product->get_all_in($fail_price_change_products)->result();
			$this->custom_fail_response( $prds,true, "Product Price is has been changed." );

		} else {

		*/	

			$payment_method = "";
			$paypal_result = 0;
			$stripe_result = 0;
			$cod_result = 0;

			if($this->post( 'is_paypal' ) == 1) {

				//User using Paypal to submit the transaction
				$payment_method = "PAYPAL";

				$shop_info = $this->Shop->get_one($this->post( 'shop_id' ));


				$gateway = new Braintree_Gateway([
				  'environment' => trim($shop_info->paypal_environment),
				  'merchantId' => trim($shop_info->paypal_merchant_id),
				  'publicKey' => trim($shop_info->paypal_public_key),
				  'privateKey' => trim($shop_info->paypal_private_key)
				]);

				$result = $gateway->transaction()->sale([
				  'amount' 			   => $this->post( 'balance_amount' ),
				  'paymentMethodNonce' => $this->post( 'payment_method_nonce' ),
				  'options' => [
				    'submitForSettlement' => True
				  ]
				]);

				if($result->success == 1) {
				
					$paypal_result = $result->success;
				
				} else {

					$this->error_response( get_msg( 'paypal_transaction_failed' ) );
				
				}



			} else if($this->post( 'is_stripe' ) == 1) {

				//User using Stripe to submit the transaction
				 $payment_method = "STRIPE";
				 $shop_info = $this->Shop->get_one($this->post( 'shop_id' ));

				try {
				
					# set stripe test key
					\Stripe\Stripe::setApiKey( trim($shop_info->stripe_secret_key) );
					
					$charge = \Stripe\Charge::create(array(
				    	"amount" 	  => $this->post( 'balance_amount' ) * 100, // amount in cents, so need to multiply with 100 .. $amount * 100
				    	"currency"    => trim($shop_info->currency_short_form),
				    	"source"      => $this->post( 'payment_method_nonce' ),
				    	"description" => get_msg('order_desc')
				    ));
				    
				    if( $charge->status == "succeeded" )
				    {
				    	$stripe_result = 1;
				    } else {
				    	$this->error_response( get_msg( 'stripe_transaction_failed' ) );
				    }
					
				} 

				catch(exception $e) {
				  	
				 	$this->error_response( get_msg( 'stripe_transaction_failed' ) );
				    
				 }


			} else if($this->post( 'is_paytabs' ) == 1) {
 			//$property_id=$request['rentproperty_id'];
	        $postal_code="23213";$country="SAU";$postal_code_shipping="23213";$country_shipping="SAU";
	        $products_per_title="Property Title";$currency="SAR";$site_url="https://www.eshtri.net";
	        $return_url = "https://www.eshtri.net/paymentsuccessurl";$cms_with_version="API USING PHP";
	        $shipping_address_2="Riyadh";
				//User Using COD 
				$payment_method = "paytabs";


				$paytabs_result = 1;

			}else if($this->post( 'is_POSTerminal' ) == 1) {

				//User Using COD 
				$payment_method = "POSTerminal";


				$POSTerminal_result = 1;

			}else if($this->post( 'is_cod' ) == 1) {

				//User Using COD 
				$payment_method = "COD";


				$cod_result = 1;

			} else if ($this->post( 'is_bank' ) == 1) {

				//User Using COD 
				$payment_method = "BANK";


				$bank_result = 1;

			} else {

				//Not selected to payment 
				$this->error_response( get_msg( 'payment_not_select' ) );
			}

			$UserAddress = $this->UserAddress->get_one_by( array( 'user_id' => $this->post( 'user_id'), 'is_default' => 1));
			if($UserAddress->id ==null || !$UserAddress->id){
				$this->error_response( get_msg( 'delivery_address_not_added' ) );
			}
			// if($this->post( 'shipping_method_amount') >50) {
	 	// 		$this->error_response( get_msg( 'currently_we_have_no_agent_to_deliver_in_your_delivery_location' ) );
	 	// 	} 
			if( $paypal_result == 1 || $stripe_result == 1 || $cod_result == 1 || $bank_result == 1 || $paytabs_result = 1 || $POSTerminal_result = 1) {
				


				$this->db->trans_start();

				//First Time
		 		$transaction_row_count = $this->Transactionheader->count_all();
		 		$current_date_month = date("Ym");
		 		$current_date_time = date("Y-m-d H:i:s"); 
		 		// print_r($current_date_month);die;
		 		$conds['code'] = $current_date_month;
		 		// print_r($conds);die;
		 		$trans_code_checking =  $this->Code->get_one_by($conds)->code;
		 		// print_r($trans_code_checking);die;
		 		$id = false;


		 		if($trans_code_checking == "") {
		 			//New record for this year--mm, need to insert as inside the core_code_generator table
					$data['type']  =  "transaction";
			 		$data['code']  =  $today = date("Ym"); ;
			 		$data['count'] = $transaction_row_count + 1;
			 		$data['added_user_id'] = $this->post( 'user_id' );
			 		$data['added_date'] = date("Y-m-d H:i:s"); 
			 		$data['updated_date'] = date("Y-m-d H:i:s"); 
			 		$data['updated_user_id'] = 0;
			 		$data['updated_flag'] = 0;

		 			if( !$this->Code->save($data, $id) ) {
						// rollback the transaction
						$this->db->trans_rollback();
						$this->error_response( get_msg( 'err_model' ));
		 			}

		 			// get inserted id
					if ( !$id ) $id = $data['id']; 

					if($id) {
						$trans_code = $this->Code->get_one($id)->code;
					}


					
		 		} else {
		 			//record is already exist so just need to update for count field only
		 			$data['count'] = $transaction_row_count + 1;

		 			$core_code_generator_id =  $this->Code->get_one_by($conds)->id;

		 			if( !$this->Code->save($data, $core_code_generator_id) ) {
						// rollback the transaction
						$this->db->trans_rollback();
						$this->error_response( get_msg( 'err_model' ));
		 			}

		 			$conds['id'] = $core_code_generator_id;
		 			$trans_code =  $this->Code->get_one_by($conds)->code . ($transaction_row_count + 1);


		 		}

		 		if( $this->post( 'shipping_method_amount') <50) {
		 			$shipping_method_amount = 15.00;
		 		}else if( $this->post( 'shipping_method_amount') >50) {
                    $shipping_method_amount = 50.00;
                }else {
		 			$shipping_method_amount = $this->post( 'shipping_method_amount');
		 		}
                $shopinfo = $this->Shop->get_one_by(array('id' => $this->post('shop_id')));
                $delivery_date			= "";
                $delivery_time_from	    = "";
                $delivery_time_to		= "";
                $time_slot_id		 	= "";
                if($this->post( 'shipping_method_amount') <50) {
                    // $UserAddress = $this->UserAddress->get_one_by( array( 'user_id' => $this->post( 'user_id'), 'is_default' => 1));
                    $timeslotsdetails = $this->Timeslotsdetails->get_one_by(array('id' => $this->post('time_slot_id')));
                    $delivery_date			= $timeslotsdetails->slot_date;
                    $delivery_time_from	    = $timeslotsdetails->start_time;
                    $delivery_time_to		= $timeslotsdetails->end_time;
                    $time_slot_id		 	= $this->post('time_slot_id');

                }
				 $shipping_latitute		= '';
				 $shipping_lontitude		= '';
				 if($UserAddress->id){
					$shipping_first_name = $UserAddress->receiverName;
					$shipping_address_1	= $UserAddress->address;
					$shipping_address_2	= $UserAddress->landmark;
					$shipping_city		= $UserAddress->city;
					$shipping_email		= $UserAddress->email;
					$shipping_phone		= $UserAddress->mobileNo;
					$shipping_latitute		= $UserAddress->latitute;
					$shipping_lontitude		= $UserAddress->lontitude;
				 }else{
					$shipping_first_name = $this->post( 'shipping_first_name');
					$shipping_address_1	= $this->post( 'shipping_address_1');
					$shipping_address_2	= $this->post( 'shipping_address_2');
					$shipping_city		= $this->post( 'shipping_city');
					$shipping_email		= $this->post( 'shipping_email');
					$shipping_phone		= $this->post( 'shipping_phone');
				 }
		 		//Need to save inside transaction header table 
				$trans_header = array(
		 			'user_id' 				=> $this->post( 'user_id'),
		 			'shop_id'				=> $this->post( 'shop_id' ),
		 			'sub_total_amount' 		=> $this->post( 'sub_total_amount' ),
		 			'tax_amount' 			=> $this->post( 'tax_amount' ),
		 			'shipping_amount' 		=> $this->post( 'shipping_amount' ),
		 			// 'balance_amount' 		=> $this->post( 'balance_amount' ),
		 			'balance_amount' 		=> ($this->post( 'sub_total_amount' ) + ($this->post( 'tax_amount' ) + $shipping_method_amount + ($shipping_method_amount * $this->post( 'shipping_tax_percent'))) ),
		 			'total_item_amount' 	=> $this->post( 'total_item_amount' ),
		 			'total_item_count' 	    => $this->post( 'total_item_count' ),
		 			'contact_name' 		    => $this->post( 'contact_name' ),
		 			'contact_phone' 		=> $this->post( 'contact_phone' ),
		 			'payment_method' 		=> $payment_method,
		 			'trans_status_id' 		=> 1,
		 			'discount_amount'       => $this->post( 'discount_amount'),
		 			'coupon_discount_amount'=> $this->post( 'coupon_discount_amount'),
		 			'trans_code'            => $trans_code,
		 			'added_date'            => $current_date_time,
		 			'added_user_id'         => $this->post( 'user_id' ),
		 			'updated_date'          => $current_date_time,
		 			'updated_user_id'       => "0",
		 			'updated_flag'          => "0",
		 			'currency_symbol'       => $this->post( 'currency_symbol'),
		 			'currency_short_form'   => $this->post( 'currency_short_form'),
		 			// 'billing_first_name'    => $this->post( 'billing_first_name'),
					// 'billing_last_name'		=> $this->post( 'billing_last_name'),
					// 'billing_company'		=> $this->post( 'billing_company'),
					// 'billing_address_1'		=> $this->post( 'billing_address_1'),
					// 'billing_address_2'		=> $this->post( 'billing_address_2'),
					// 'billing_country' 		=> $this->post( 'billing_country'),
					// 'billing_state'			=> $this->post( 'billing_state'),
					// 'billing_city'			=> $this->post( 'billing_city'),
					// 'billing_postal_code'	=> $this->post( 'billing_postal_code'),
					// 'billing_email'			=> $this->post( 'billing_email'),
					// 'billing_phone'			=> $this->post( 'billing_phone'),
					 'shipping_first_name'   => $shipping_first_name,
					// 'shipping_last_name'	=> $this->post( 'shipping_last_name'),
					// 'shipping_company'		=> $this->post( 'shipping_company'),
					 'shipping_address_1'	=> $shipping_address_1,
					 'shipping_address_2'	=> $shipping_address_2,
					// 'shipping_country' 		=> $this->post( 'shipping_country'),
					// 'shipping_state'		=> $this->post( 'shipping_state'),
					 'shipping_city'			=> $shipping_city,
					// 'shipping_postal_code'	=> $this->post( 'shipping_postal_code'),
					 'shipping_email'		=> $shipping_email,
					 'shipping_phone'		=> $shipping_phone,
					 'shipping_lat'			=> $shipping_latitute,
					 'shipping_lng'			=> $shipping_lontitude,
					 'shop_lat'				=> $shopinfo->lat,
					 'shop_lng'				=> $shopinfo->lng,
					'shipping_tax_percent'  => $this->post( 'shipping_tax_percent'),
					'tax_percent'  			=> $this->post( 'tax_percent'),
					'shipping_method_amount' => $shipping_method_amount,
					'shipping_method_name'   => $this->post( 'shipping_method_name'),
					'memo'   				 => $this->post( 'memo'),
					'is_zone_shipping'		 => $this->post('is_zone_shipping'),
					'delivery_date'			=> $delivery_date,
					'delivery_time_from'	=> $delivery_time_from,
					'delivery_time_to'		=> $delivery_time_to,
					'time_slot_id'		 	=> $time_slot_id,

		 		);

				$trans_header_id = false;
				$inserted=$this->Transactionheader->saveOrder($trans_header);
				if( $inserted ==null) {
					// rollback the transaction
					$this->error_response( get_msg( 'err_model' ) );
				} 

				$trans_header_id = $inserted; 

				$trans_details = $this->post( 'details' );
                $totalItem = 0;
                $totalItemAmountNoTax = 0.0;
                $sub_total_amount = 0.0;
                $discount_total_amount = 0.0;
                $originalprice_total_amount = 0.0;
                $tax_amount = 0.0;
				for($i=0; $i<count($trans_details); $i++) 
				{
					$product_info =  $this->Product->get_one($trans_details[$i]['product_id']);
					if($product_info->is_available==1 && $product_info->status==1 && $this->post( 'shop_id' ) == $trans_details[$i]['shop_id']) {
                        $categoryid = $product_info->cat_id;
                        // print_r($trans_details);die;
                        $trans_detail['shop_id'] = $trans_details[$i]['shop_id'];
                        $trans_detail['product_id'] = $trans_details[$i]['product_id'];
                        $trans_detail['product_category_id'] = $categoryid;
                        $trans_detail['product_name'] = $trans_details[$i]['product_name'];
                        $trans_detail['product_photo'] = $product_info->thumbnail;
                        if(strpos($trans_details[$i]['product_attribute_price'], "#") !== false){
                            $attribute_price_arr=explode("#",$trans_details[$i]['product_attribute_price']);
                            $attribute_id_arr=explode("#",$trans_details[$i]['product_attribute_id']);
                            $trans_detail['product_attribute_id'] = $attribute_id_arr[count($attribute_id_arr)-1];
                            $trans_detail['product_attribute_price'] = $attribute_price_arr[count($attribute_price_arr)-1];
                            $trans_detail['price'] = $trans_details[$i]['original_price'];
                        }else{
                            $trans_detail['product_attribute_id'] = $trans_details[$i]['product_attribute_id'];
                            $trans_detail['product_attribute_price'] = $trans_details[$i]['product_attribute_price'];
                            $trans_detail['price'] = $trans_details[$i]['unit_price'];
                        }

                        $trans_detail['product_attribute_name'] = $trans_details[$i]['product_attribute_name'];

                        $trans_detail['original_price'] = $trans_details[$i]['original_price'];
                        $trans_detail['product_color_id'] = $trans_details[$i]['product_color_id'];
                        $trans_detail['product_color_code'] = $trans_details[$i]['product_color_code'];
                        $trans_detail['product_color_name'] = "";
                        if ($trans_details[$i]['product_color_id']) {
                            $color_info = $this->Color->get_one($trans_details[$i]['product_color_id']);
                            if ($color_info->color_value && $color_info->color_name) {
                                $trans_detail['product_color_name'] = $color_info->color_name;
                            }
                        }


                        $trans_detail['qty'] = $trans_details[$i]['qty'];
                        $trans_detail['discount_value'] = $trans_details[$i]['discount_value'];
                        $trans_detail['discount_percent'] = $trans_details[$i]['discount_percent'];
                        $trans_detail['discount_amount'] = $trans_details[$i]['discount_amount'];
                        $trans_detail['transactions_header_id'] = $trans_header_id;
                        $trans_detail['added_date'] = $current_date_time;
                        $trans_detail['added_user_id'] = $this->post('user_id');
                        $trans_detail['updated_date'] = $current_date_time;
                        $trans_detail['updated_user_id'] = "0";
                        $trans_detail['updated_flag'] = "0";
                        $trans_detail['currency_short_form'] = $trans_details[$i]['currency_short_form'];
                        $trans_detail['currency_symbol'] = $trans_details[$i]['currency_symbol'];
                        $trans_detail['product_unit'] = $trans_details[$i]['product_unit'];
                        $trans_detail['product_measurement'] = $trans_details[$i]['product_measurement'];
                        $trans_detail['shipping_cost'] = $trans_details[$i]['shipping_cost'];

                        if (!$this->Transactiondetail->save($trans_detail)) {
                            // if error in saving transaction detail,
                            $this->db->trans_rollback();
                            $this->error_response(get_msg('err_model'));
                        }

                        //Need to update transaction count table
                        $prd_cat_id = $this->Product->get_one($trans_details[$i]['product_id'])->cat_id;
                        $prd_sub_cat_id = $this->Product->get_one($trans_details[$i]['product_id'])->sub_cat_id;

                        $trans_count['product_id'] = $trans_details[$i]['product_id'];
                        $trans_count['shop_id'] = $trans_details[$i]['shop_id'];
                        $trans_count['cat_id'] = $prd_cat_id;
                        $trans_count['sub_cat_id'] = $prd_sub_cat_id;
                        $trans_count['user_id'] = $this->post('user_id');


                        $qtyDtls = $trans_details[$i]['qty'];
                        $originalPriceDtls = $trans_details[$i]['original_price'];
                        $discountPercentDtls = $trans_details[$i]['discount_percent'];
                        $price = $originalPriceDtls;
                        $discount_amount = 0.0;
                        if ($discountPercentDtls > 0) {
                            $discount_amount = (($trans_details[$i]['discount_percent'] / 100) * $originalPriceDtls);
                            $discountPriceDtls = ($originalPriceDtls - (($trans_details[$i]['discount_percent'] / 100) * $originalPriceDtls));
                            $price = $discountPriceDtls;
                        }

                        $itemSubtotal = $originalPriceDtls * $qtyDtls;

                            $totalItem += $qtyDtls;
                            $totalItemAmountNoTax += $itemSubtotal;

                            $itemDiscountAmount = $discount_amount * $qtyDtls;
                            $discount_total_amount += $itemDiscountAmount;
                            $sub_total_amount += $price * $qtyDtls;
                            $itemOriginalAmount = $trans_details[$i]['original_price'] * $qtyDtls;
                            $originalprice_total_amount += $itemOriginalAmount;



                        if (!$this->Transactioncount->save($trans_count)) {
                            // if error in saving review rating,
                            $this->db->trans_rollback();
                            $this->error_response(get_msg('err_model'));

                        }

                        //    $device_token=$this->post( 'device_token');
                        // $user_id = $this->post( 'user_id');
                        // $user_data['device_token'] = $device_token;
                        // if( !$this->User->save($user_data, $user_id) ) {
                        // 	// rollback the transaction
                        // 	$this->db->trans_rollback();
                        // 	$this->error_response( get_msg( 'err_model' ));
                        // 	}


                    }
				}

				
				

				if ($this->db->trans_status() === FALSE) {
		        	$this->db->trans_rollback();
		        	$this->error_response( get_msg( 'err_model' ) );
		    	} else {
                    $total_item_amount=$sub_total_amount;
                    if($this->post( 'coupon_discount_amount') >0){
                        $total_item_amount=$total_item_amount - $this->post( 'coupon_discount_amount');
                    }

                    $tax_amount=(($this->post( 'tax_percent') / 100) * $total_item_amount);
                    $master_data = array(
                        'total_item_count'=> $totalItem,
                        'sub_total_amount'=> $sub_total_amount, // after discount
                        'discount_amount'=> $discount_total_amount,
                        'coupon_discount_amount'=> $this->post( 'coupon_discount_amount'),
                        'tax_amount'=> $tax_amount,
                        'shipping_method_amount'=> $shipping_method_amount,
                        'total_item_amount'=> $originalprice_total_amount, // without discount
                        'balance_amount'=> $total_item_amount + $shipping_method_amount + $tax_amount-$this->post( 'coupon_discount_amount'),
                    );

                    // save data
                    $this->Transactionheader->save( $master_data, $trans_header_id );




					$status_data = array( 'transactions_header_id'=> $trans_header_id, 'transactions_status_id'=> 1 );
					// save data
					$this->Transactionstatustracking->save($status_data);

                    if($this->post( 'shipping_method_amount') <50) {
                        $orderCount = $timeslotsdetails->order_count + 1;
                        $slot_data['order_count'] = $orderCount;
                        if ($orderCount >= $timeslotsdetails->accept_order_total) {
                            $slot_data['available'] = 0;
                        }

                        // save data
                        $this->Timeslotsdetails->save($slot_data, $timeslotsdetails->id);
                    }
					// paytabs starts
					$status=array();
					if($payment_method=="paytabs"){
						$merchant_email='info@eshtri.net';
						$secret_key='ZVhfg4c0GwHn6JJqoCoU3wWQSYvgshQsd7HtcUuTqchwV0XkDlfHSbxKa31ClR5yZN2bHw57ImEDmCX0YdHQoeByiCbDugbt3eFL';
						$merchant_id='10063345';

						$params=array('merchant_email'=>$merchant_email,
						             'merchant_id'=>$merchant_id,
						             'secret_key'=>$secret_key,
						             'payment_reference'=>'');
						             
						$this->load->library('paytabs',$params);
						$unit_price=$total_item_amount + $shipping_method_amount + $tax_amount-$this->post( 'coupon_discount_amount');
						if($shipping_email==null || $shipping_email==""){
							$shipping_email="sales@eshtri.net";
						}
						if($shipping_first_name==null || $shipping_first_name==""){
							$shipping_first_name="customer name";
						}
						if($shipping_first_name==null || $shipping_first_name==""){
							$shipping_first_name="customer name";
						}
						if($cc_phone_number==null || $cc_phone_number==""){
							$cc_phone_number="966";
						}
						if(strlen($shipping_phone) >9){
				            $formattedNumber=str_replace("966", "", $shipping_phone);
				            $formattedNumber=str_replace('+', "", $formattedNumber);
				            $shipping_phone=intval($formattedNumber);
				        }else if(strlen($shipping_phone) <9){
				        	$shipping_phone="536176364";
				        }
				        if($shipping_address_1==null || $shipping_address_1==""){
							$shipping_address_1="customer address";
						}
						if($shipping_address_2==null || $shipping_address_2==""){
							$shipping_address_2="customer state";
						}
						 if($shipping_city==null || $shipping_city==""){
							$shipping_city="customer city";
						}
                        $url="https://secure.paytabs.sa/payment/request";
                        $data=array(
                            "profile_id" => 58545,
                            "tran_type" => "sale",
                            "tran_class" => "ecom",
                            "cart_description" => "Order nnumber-".$trans_header_id."",
                            "cart_id" => "Order-".$trans_header_id."",
                            "cart_currency" => "SAR",
                            "cart_amount" => $unit_price,
                            "callback" => "https://www.eshtri.net",
                            "return" => "https://www.eshtri.net/paymentsuccessurl/transection/".$trans_header_id."",
                            "customer_details" => array(
                                "name"=>"".$shipping_first_name."",
                                "email"=>"".$shipping_email."",
                                "phone_number"=>"966".$shipping_phone."",
                                "street1"=>"".$shipping_address_1."",
                                "city"=>"".$shipping_city."",
                                "state"=>"".$shipping_address_2."",
                                "country"=>"".$country."",
                                "ip"=>"127.0.0.1"

                            ),
                        );
                        $postdata = json_encode($data);

                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: SDJN9JL69Z-JBDDDMB9GW-JG9DWZGNKB'));
                        $result = curl_exec($ch);
                        curl_close($ch);

                        $response=json_decode($result);
//						$postdata=array(
//					            "merchant_email" => "info@eshtri.net",
//					            'secret_key' => "ZVhfg4c0GwHn6JJqoCoU3wWQSYvgshQsd7HtcUuTqchwV0XkDlfHSbxKa31ClR5yZN2bHw57ImEDmCX0YdHQoeByiCbDugbt3eFL",
//					            'title' => "Order no=".$trans_header_id."",
//					            'cc_first_name' => "".$shipping_first_name."",
//					            'cc_last_name' => "".$shipping_first_name."",
//					            'email' => "".$shipping_email."",
//					            'cc_phone_number' => "".$cc_phone_number."",
//					            'phone_number' => "".$shipping_phone."",
//					            'billing_address' => "".$shipping_address_1."",
//					            'city' => "".$shipping_city."",
//					            'state' => "".$shipping_address_2."",
//					            'postal_code' => "".$postal_code."",
//					            'country' => "".$country."",
//					            'address_shipping' => "".$shipping_address_1."",
//					            'city_shipping' => "".$shipping_city."",
//					            'state_shipping' => "".$shipping_address_2."",
//					            'postal_code_shipping' => "".$postal_code."",
//					            'country_shipping' => "".$country."",
//					            'currency' => "SAR",
//					            "products_per_title"=> $totalItem,
//					            "unit_price"=> "".$unit_price."",
//					            'quantity' => "1",
//					            'other_charges' => "0",
//					            'amount' => "".$unit_price."",
//					            'discount'=>"0",
//					            "msg_lang" => "arabic",
//					            "reference_no" => "".$trans_header_id."",
//					            "site_url" => "https://www.eshtri.net",
//					            'return_url' => "https://www.eshtri.net/paymentsuccessurl/transection/".$trans_header_id."",
//					            "cms_with_version" => "API USING PHP",
//					            //"payment_type" => "mada"
//					        );
//
//				         $response= $this->paytabs->create_pay_page($postdata);
				        // echo $response->payment_url;   
				         
				         if($response->tran_ref){
//				             $status=array('success'=>1, 'url'=>$response->payment_url, 'result' =>$response->result, 'p_id' =>$response->p_id);
				             $payment_data['payment_reference']=$response->tran_ref;
								// save data
							$this->Transactionheader->save($payment_data, $trans_header_id);
				          }

					}
					// Paytabs ends


					$this->db->trans_commit();
				}

				$trans_header_obj = $this->Transactionheader->get_one($trans_header_id);

				//Sending Email to shop
				$to_who = "shop";
				$subject = get_msg('order_receive_subject');

				if ( !send_transaction_order_emails( $trans_header_id, $to_who, $subject )) {

					//$this->error_response( get_msg( 'err_email_not_send_to_shop' ));
				
				}

				//Sending Email To user
				$to_who = "user";
				$subject = get_msg('order_receive_subject');
				
				if ( !send_transaction_order_emails( $trans_header_id, $to_who, $subject )) {

					//$this->error_response( get_msg( 'err_email_not_send_to_user' ));
				
				}

				//Sending Email to Eshtri
				$to_who = "eShtri";
				$subject = get_msg('order_receive_subject');

				if ( !send_transaction_order_emails( $trans_header_id, $to_who, $subject )) {

					//$this->error_response( get_msg( 'err_email_not_send_to_shop' ));
				
				}
				if($payment_method=="paytabs"){
					if($response->tran_ref){
						$trans_header_obj->payment_url=$response->redirect_url;
						$trans_header_obj->p_id=$response->tran_ref;
					}
				}

				$this->convert_object($trans_header_obj);
				$this->custom_response($trans_header_obj);
				
		}


	}

    function sendsomething_post()
    {

        $user_id=$this->post( 'user_id');
        //$shipping_address_id=$this->post( 'shipping_address_id');
       // $pickup_address_id=$this->post( 'pickup_address_id');
        $memo=$this->post( 'memo');
        $paypal_result = 0;
        $stripe_result = 0;
        $payment_method = "COD";
        $cod_result = 1;

       // $UserPickAddress = $this->UserAddress->get_one_by( array( 'user_id' => $user_id, 'id' => $pickup_address_id));
       // $UserDropAddress = $this->UserAddress->get_one_by( array( 'user_id' => $user_id, 'id' => $shipping_address_id));
      //  if($UserDropAddress->id ==null || !$UserDropAddress->id){
      //      $this->error_response( get_msg( 'delivery_address_not_added' ) );
      //  }
        // if($this->post( 'shipping_method_amount') >50) {
        // 		$this->error_response( get_msg( 'currently_we_have_no_agent_to_deliver_in_your_delivery_location' ) );
        // 	}
        if($cod_result == 1) {
            $this->db->trans_start();

            //First Time
            $transaction_row_count = $this->Transactionheader->count_all();
            $current_date_month = date("Ym");
            $current_date_time = date("Y-m-d H:i:s");
            // print_r($current_date_month);die;
            $conds['code'] = $current_date_month;
            // print_r($conds);die;
            $trans_code_checking =  $this->Code->get_one_by($conds)->code;
            // print_r($trans_code_checking);die;
            $id = false;


            if($trans_code_checking == "") {
                //New record for this year--mm, need to insert as inside the core_code_generator table
                $data['type']  =  "transaction";
                $data['code']  =  $today = date("Ym"); ;
                $data['count'] = $transaction_row_count + 1;
                $data['added_user_id'] = $this->post( 'user_id' );
                $data['added_date'] = date("Y-m-d H:i:s");
                $data['updated_date'] = date("Y-m-d H:i:s");
                $data['updated_user_id'] = 0;
                $data['updated_flag'] = 0;

                if( !$this->Code->save($data, $id) ) {
                    // rollback the transaction
                    $this->db->trans_rollback();
                    $this->error_response( get_msg( 'err_model' ));
                }

                // get inserted id
                if ( !$id ) $id = $data['id'];

                if($id) {
                    $trans_code = $this->Code->get_one($id)->code;
                }

            } else {
                //record is already exist so just need to update for count field only
                $data['count'] = $transaction_row_count + 1;

                $core_code_generator_id =  $this->Code->get_one_by($conds)->id;

                if( !$this->Code->save($data, $core_code_generator_id) ) {
                    // rollback the transaction
                    $this->db->trans_rollback();
                    $this->error_response( get_msg( 'err_model' ));
                }

                $conds['id'] = $core_code_generator_id;
                $trans_code =  $this->Code->get_one_by($conds)->code . ($transaction_row_count + 1);


            }


            $shipping_method_amount = 15.00;
            $delivery_date			= "";
            $delivery_time_from	    = "";
            $delivery_time_to		= "";
            $time_slot_id		 	= "";
                // $UserAddress = $this->UserAddress->get_one_by( array( 'user_id' => $this->post( 'user_id'), 'is_default' => 1));

                $delivery_date			= date("Y-m-d H:i:s");
                $delivery_time_from	    = "";
                $delivery_time_to		= "";
                $time_slot_id		 	= "";


            $shipping_latitute		= '';
            $shipping_lontitude		= '';
                // $shipping_first_name = $UserDropAddress->receiverName;
                // $shipping_address_1	= $UserDropAddress->address;
                // $shipping_address_2	= $UserDropAddress->landmark;
                // $shipping_city		= $UserDropAddress->city;
                // $shipping_email		= $UserDropAddress->email;
                // $shipping_phone		= $UserDropAddress->mobileNo;
                // $shipping_latitute		= $UserDropAddress->latitute;
                // $shipping_lontitude		= $UserDropAddress->lontitude;

            //Need to save inside transaction header table
            $trans_header = array(
                'user_id' 				=> $user_id,
                'sub_total_amount' 		=> 0,
                'tax_amount' 			=> 0,
                'shipping_amount' 		=> 0,
                'balance_amount' 		=> 15,
                'total_item_amount' 	=> $this->post( 'total_item_amount'),
                'total_item_count' 	    => 0,
                'contact_name' 		    => $shipping_first_name,
                'contact_phone' 		=> $shipping_phone,
                'payment_method' 		=> $payment_method,
                'trans_status_id' 		=> 1,
                'discount_amount'       => 0,
                'coupon_discount_amount'=> 0,
                'trans_code'            => $trans_code,
                'added_date'            => $current_date_time,
                'added_user_id'         => $user_id,
                'updated_date'          => $current_date_time,
                'updated_user_id'       => "0",
                'updated_flag'          => "0",
                'currency_symbol'       => "SAR",
                'currency_short_form'   => "SAR",
                'billing_first_name'    => $this->post( 'billing_first_name'),
                'billing_last_name'	    => $this->post( 'billing_last_name'),
                'billing_address_1'	    => $this->post( 'billing_address_1'),
                'billing_address_2'	    => $this->post( 'billing_address_2'),
                'billing_city'			=> $this->post( 'billing_city'),
                'billing_email'		    => $this->post( 'billing_email'),
                'billing_phone'		    => $this->post( 'billing_phone'),
                'shipping_first_name'   => $this->post( 'shipping_first_name'),
                'shipping_address_1'	=> $this->post( 'shipping_address_1'),
                'shipping_address_2'	=> $this->post( 'shipping_address_2'),
                'shipping_city'			=> $this->post( 'shipping_city'),
                'shipping_email'		=> $this->post( 'shipping_email'),
                'shipping_phone'		=> $this->post( 'shipping_phone'),
                'shipping_lat'			=> $this->post( 'shipping_lat'),
                'shipping_lng'			=> $this->post( 'shipping_lng'),
                'shop_lat'				=> $this->post( 'shop_lat'),
                'shop_lng'				=> $this->post( 'shop_lng'),
                'shipping_tax_percent'  => 0,
                'tax_percent'  			=> 0,
                'shipping_method_amount' => $shipping_method_amount,
                'shipping_method_name'   => "Zone Shipping",
                'memo'   				 => $memo,
                'is_zone_shipping'		 => 1,
                'delivery_date'			=> $this->post( 'delivery_date'),
                'delivery_time_from'	=> $this->post( 'delivery_time_from'),
                'delivery_time_to'		=> $delivery_time_to,
                'time_slot_id'		 	=> $time_slot_id,
                'is_hot_item'		 	=> $this->post( 'is_hot_item'),
                'order_cost_policy'		=> $this->post( 'order_cost_policy'),
                'order_cost_estimated'	=> $this->post( 'order_cost_estimated'),
                'delivery_fees_policy'	=> $this->post( 'delivery_fees_policy'),
                'order_type'	        => $this->post( 'order_type'),
                'address_type'	        => $this->post( 'address_type'),

            );
            $trans_header_id = false;
            $inserted=$this->Transactionheader->saveOrder($trans_header);
            if( $inserted ==null) {
                // rollback the transaction
                $this->error_response( get_msg( 'err_model' ) );
            }
            $trans_header_id = $inserted;

            // product details start
            $trans_details = $this->post( 'details' );
            if(count($trans_details)>0) {
                $totalItem = 0;
                for ($i = 0; $i < count($trans_details); $i++) {
                    $trans_detail['product_name'] = $trans_details[$i]['product_name'];
                    $trans_detail['qty'] = $trans_details[$i]['qty'];
                    $trans_detail['price'] = 0;
                    $trans_detail['original_price'] = 0;
                    $trans_detail['discount_value'] =0;
                    $trans_detail['discount_percent'] = 0;
                    $trans_detail['discount_amount'] =0;
                    $trans_detail['transactions_header_id'] = $trans_header_id;
                    $trans_detail['added_date'] = $current_date_time;
                    $trans_detail['added_user_id'] = $this->post('user_id');
                    $trans_detail['updated_date'] = $current_date_time;
                    $trans_detail['updated_user_id'] = "0";
                    $trans_detail['updated_flag'] = "0";
                    $trans_detail['currency_short_form'] ='SAR';
                    $trans_detail['currency_symbol'] ='SAR';
                    $trans_detail['shipping_cost'] = 0;
                    $totalItem = $totalItem + $trans_details[$i]['qty'];
                    if (!$this->Transactiondetail->save($trans_detail)) {
                        // if error in saving transaction detail,
                        $this->db->trans_rollback();
                        $this->error_response(get_msg('err_model'));
                    }
                }

                $master_data = array(
                    'total_item_count' => $totalItem
                );
                // update qty data
                $this->Transactionheader->save($master_data, $trans_header_id);
            }
            // product details ends

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->error_response( get_msg( 'err_model' ) );
            } else {
                $status_data = array( 'transactions_header_id'=> $trans_header_id, 'transactions_status_id'=> 1 );
                // save data
                $this->Transactionstatustracking->save($status_data);
                $this->db->trans_commit();
            }
            $trans_header_obj = $this->Transactionheader->get_one($trans_header_id);

            //Sending Email to Eshtri
            $to_who = "eShtri";
            $subject = get_msg('order_receive_subject');

            if ( !send_transaction_order_emails( $trans_header_id, $to_who, $subject )) {
                //$this->error_response( get_msg( 'err_email_not_send_to_shop' ));
            }
            $this->convert_object($trans_header_obj);
            $this->custom_response($trans_header_obj);
        }

    }

		// function paytabsPayment($value='')
		// {
			
		// }
	function changeorderstatus_post(){

		//error_reporting(0);

		$order_id=$this->post( 'id');
		$order_status_id=$this->post( 'trans_status_id');
		$contract_type=$this->post( 'contract_type');
        //$this->custom_response( $this->post() ) ;
		$conds = array(
			"id" => $order_id,
			// "trans_status_id" => $trans_status_id
		);
		$orderdata = $this->Transactionheader->get_one_by($conds);


		if(!$order_id || !$order_status_id || $orderdata->trans_status_id==5){
			$this->error_response( get_msg( 'order_already_completed' ) );
			exit;
		}
		

		//$this->db->trans_start();
		if($order_status_id && $order_id){
		    if($contract_type=="DeliveryBoy" && $order_status_id=='2'){
                if ($orderdata->trans_status_id && $order_status_id != 5) {
                    $order_status_id = 2;
                }
                $status_data = array('transactions_header_id' => $order_id, 'transactions_status_id' => $order_status_id);
                // save data
                $this->Transactionstatustracking->save($status_data);


                $order_status_id = 3;
                $status_data = array('transactions_header_id' => $order_id, 'transactions_status_id' => $order_status_id);
                // save data
                $this->Transactionstatustracking->save($status_data);

                $order_status_id = 4;
                $status_data = array('transactions_header_id' => $order_id, 'transactions_status_id' => $order_status_id);
                // save data
                $this->Transactionstatustracking->save($status_data);


                $assign_to_data = array('trans_status_id' => $order_status_id);
                $this->Transactionheader->save($assign_to_data, $order_id);

                $userinfo=$this->User->get_one_by( array( 'user_id' => $orderdata->user_id));
                if($userinfo->device_token){
                    $this->ps_adapter->send_android_fcm($userinfo->device_token, get_msg( 'order_status_changed' ), get_msg( 'your_order_is_on_the_way' )." - ".$order_id."", "/MyOrderDetails", $order_id, "");
                }

            }else {
                if ($orderdata->trans_status_id && $order_status_id != 5) {
                    $order_status_id = $orderdata->trans_status_id + 1;
                }
                $assign_to_data = array('trans_status_id' => $order_status_id);
                $this->Transactionheader->save($assign_to_data, $order_id);
                $status_data = array('transactions_header_id' => $order_id, 'transactions_status_id' => $order_status_id);
                // save data
                $this->Transactionstatustracking->save($status_data);

                $userinfo=$this->User->get_one_by( array( 'user_id' => $orderdata->user_id));
                if($userinfo->device_token){
                    $message="";
                    if($order_status_id==2){
                        $message=get_msg('your_order_is_preparing')." - ".$order_id."" ;
                    }
                    if($order_status_id==3){
                        $message=get_msg('your_order_is_ready')." - ".$order_id."" ;
                    }
                    if($order_status_id==4){
                        $message=get_msg('your_order_is_on_the_way')." - ".$order_id."" ;
                    }
                    if($order_status_id==5){
                        $message=get_msg('your_order_is_delivered')." - ".$order_id."" ;
                    }
                    $this->ps_adapter->send_android_fcm($userinfo->device_token, get_msg( 'order_status_changed' ), $message, "/MyOrderDetails", $order_id, "");
                }

            }
		}

		// order completed status starts
		if($order_status_id==5){
            $payment_confirm_data = array( 'payment_status'=> 1);
            $this->Transactionheader->save( $payment_confirm_data, $order_id );
			$platform_name = $this->post('platform_name');
		if ( !$platform_name ) {
			$this->custom_response( get_msg('Required_Platform') ) ;
		}
		
		//$user_id = $this->post('user_id');

		if($platform_name == "ios") {
			
			$uploaddir = 'uploads/';
			
			$path_parts = pathinfo( $_FILES['pic']['name'] );
			$filename = $path_parts['filename'] . date( 'YmdHis' ) .'.'. $path_parts['extension'];
			
			if (move_uploaded_file($_FILES['pic']['tmp_name'], $uploaddir . $filename)) {
			   $payment_confirm_data = array( 'shop_invoic_no' => $filename );
				   if ( $this->Transactionheader->save( $payment_confirm_data, $order_id ) ) {
					   	$this->success_response( get_msg( 'success_status_changed'));
				   } else {
					   	$this->error_response( get_msg('file_na') );
				   }
			   
			} else {
			   $this->error_response( get_msg('file_na') );
				
			}
			
		} else {
			
			$uploaddir = 'uploads/';
			
			$path_parts = pathinfo( $_FILES['file']['name'] );
			$filename = $path_parts['filename'] . date( 'YmdHis' ) .'.'. $path_parts['extension'];
			
			if (move_uploaded_file($_FILES['file']['tmp_name'], $uploaddir . $filename)) {
			   $payment_confirm_data = array('shop_invoic_no' => $filename );
				   if ( $this->Transactionheader->save( $payment_confirm_data, $order_id ) ) {
					   	$this->success_response( get_msg( 'success_status_changed'));

				   } else {
					   	$this->error_response( get_msg('file_na') );
				   }
			   
			} else {
			   $this->error_response( get_msg('file_na') );
				
			}
		}


		// $payment_confirm_data = array( 'payment_status'=> 1, 'shop_invoic_no'=>$this->post( 'shop_invoic_no')); //1=paid , 0=unpaid
		// $this->Transactionheader->save($payment_confirm_data, $order_id);
		}
	

//if(isset( $this->input->post("signature"))){
		// $img = $this->input->post("signature");
		// $img = str_replace('data:image/png;base64,', '', $img);
		// $img = str_replace(' ', '+', $img);
		// $image = base64_decode($img);
		// if ($image) {
		// 	header('Content-Type: bitmap; charset=utf-8');
		// 	$path="/uploads/signature/" . uniqid() . '.png';
		// 	$filelocation = ".".$path;
		// 	file_put_contents($filelocation, $image);
		// 	$order_id=$this->input->post("id");
		// 	$Signaturedata = array(
		// 		'order_id' => $this->input->post("id"),
		// 		'signature' => $path
		// 	);
	
		// 	$this->Signature->save($Signaturedata);
		// }
	   // }
	   //$this->db->trans_commit();
	   

		$this->success_response( get_msg( 'success_status_changed'));
		//echo json_encode($data);
	}

	function ordercancel_post()
    {
        $trans_header_id=$this->post( 'id');
        $cancel_by=$this->post( 'cancel_by');
        $cancel_reason=$this->post( 'cancel_reason');

        $trans_header_obj = $this->Transactionheader->get_one($trans_header_id);
        if($trans_header_obj->id ==null || !$trans_header_obj->id){
			$this->error_response( get_msg( 'order_not_available' ) );
		}

		if($trans_header_obj->trans_status_id !=1){
			$this->error_response( get_msg( 'order_can_not_cancel_now' ) );
		}

        $dataToSubmit = array("reject_status" => 1, "rejected_by" => $cancel_by, "cancel_reason" => $cancel_reason);
        $this->Transactionheader->save($dataToSubmit, $trans_header_id);


        if($this->Transactionheader->save($dataToSubmit, $trans_header_id)) {
        $obj = $this->Transactionheader->get_one($trans_header_id);
		if ( isset( $obj->time_slot_id )) {
			$tmp_time_slot = $this->Timeslotsdetails->get_one( $obj->time_slot_id );
			$obj->time_slot = $tmp_time_slot;
		}
		
		// shop object
		if ( isset( $obj->shop_id )) {
			$tmp_shop = $this->Shop->get_one( $obj->shop_id );

			$this->ps_adapter->convert_shop( $tmp_shop );

			$obj->shop = $tmp_shop;
		}

		$all_traking_status = $this->Transactionstatus->get_all_by()->result();
		foreach ($all_traking_status as $key => $value) {
			$status_conds['transactions_header_id'] = $obj->id;
			$status_conds['transactions_status_id'] = $value->id;
			$trakingInfo = $this->Transactionstatustracking->get_one_by( $status_conds );
			$all_traking_status[$key]->title=get_msg(''.$value->title.'');
			if($trakingInfo->id){
				$all_traking_status[$key]->hasUpdate=1;
				$all_traking_status[$key]->changed_date=$trakingInfo->date_created;
			}else{
				$all_traking_status[$key]->hasUpdate=0;
				$all_traking_status[$key]->changed_date=null;
			}
			
		}
		$obj->traking_status = $all_traking_status;
		$obj->all_detail = $this->Transactiondetail->get_all_by(array('transactions_header_id' =>$obj->id))->result();
		$this->custom_response($obj);
		}else{
		$this->error_response( get_msg( 'order_can_not_cancel_now' ) );
		}
    }	

	function orderLatePaymentPaytabs_post()
    {
        $trans_header_id=$this->post( 'id');
      
        $trans_header_obj = $this->Transactionheader->get_one($trans_header_id);
        if($trans_header_obj->id ==null || !$trans_header_obj->id){
			$this->error_response( get_msg( 'order_not_available' ) );
		}

		if($trans_header_obj->trans_status_id ==5 || $trans_header_obj->reject_status ==1 || $trans_header_obj->payment_method !="paytabs"){
			$this->error_response( get_msg( 'you_can_not_pay_now' ) );
		}

		// paytabs starts
		$status=array();
		if($trans_header_obj->payment_method=="paytabs" && $trans_header_obj->balance_amount >0){
			 $postal_code="23213";$country="SAU";$postal_code_shipping="23213";$country_shipping="SAU";
	        $products_per_title="Property Title";$currency="SAR";$site_url="https://www.eshtri.net";
	        $return_url = "https://www.eshtri.net/paymentsuccessurl";$cms_with_version="API USING PHP";
	        $shipping_address_2="Riyadh";
			$merchant_email='info@eshtri.net';
			$secret_key='ZVhfg4c0GwHn6JJqoCoU3wWQSYvgshQsd7HtcUuTqchwV0XkDlfHSbxKa31ClR5yZN2bHw57ImEDmCX0YdHQoeByiCbDugbt3eFL';
			$merchant_id='10063345';

			$params=array('merchant_email'=>$merchant_email,
			             'merchant_id'=>$merchant_id,
			             'secret_key'=>$secret_key,
			             'payment_reference'=>'');
			             
			$this->load->library('paytabs',$params);
			$unit_price=$trans_header_obj->balance_amount;
			$shipping_email="sales@eshtri.net";
			if($trans_header_obj->shipping_email !=null){
				$shipping_email=$trans_header_obj->shipping_email;
			}
			$shipping_first_name="customer name";
			if($trans_header_obj->shipping_first_name !=null){
				$shipping_first_name="customer name";
			}
			
		
				$cc_phone_number="966";
			$shipping_phone=intval('530000000');
			if(strlen($trans_header_obj->shipping_phone) >9){
	            $formattedNumber=str_replace("966", "", $trans_header_obj->shipping_phone);
	            $formattedNumber=str_replace('+', "", $formattedNumber);
	            $shipping_phone=intval($formattedNumber);
	        }else if(strlen($trans_header_obj->shipping_phone) <9){
	        	$shipping_phone="530000000";
	        }
	        $shipping_address_1="customer address";
	        if($trans_header_obj->shipping_address_1 !=null){
				$shipping_address_1=$trans_header_obj->shipping_address_1;
			}
			$shipping_address_2="customer state";
			if($trans_header_obj->shipping_address_2 !=null){
				$shipping_address_2=$trans_header_obj->shipping_address_2;
			}
			$shipping_city="customer city";
			 if($trans_header_obj->shipping_city !=null){
				$shipping_city=$trans_header_obj->shipping_city;
			}
            $url="https://secure.paytabs.sa/payment/request";
            $data=array(
                "profile_id" => 58545,
                "tran_type" => "sale",
                "tran_class" => "ecom",
                "cart_description" => "Order nnumber-".$trans_header_obj->id."",
                "cart_id" => "Order-".$order_id."",
                "cart_currency" => "SAR",
                "cart_amount" => $unit_price,
                "callback" => "https://www.eshtri.net",
                "return" => "https://www.eshtri.net/paymentsuccessurl/transection/".$trans_header_obj->id."",
                "customer_details" => array(
                    "name"=>"".$shipping_first_name."",
                    "email"=>"".$shipping_email."",
                    "phone_number"=>"966".$shipping_phone."",
                    "street1"=>"".$shipping_address_1."",
                    "city"=>"".$shipping_city."",
                    "state"=>"".$shipping_address_2."",
                    "country"=>"".$country."",
                    "ip"=>"127.0.0.1"

                ),
            );
            $postdata = json_encode($data);

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: SDJN9JL69Z-JBDDDMB9GW-JG9DWZGNKB'));
            $result = curl_exec($ch);
            curl_close($ch);

            $response=json_decode($result);
//			$postdata=array(
//		            "merchant_email" => "info@eshtri.net",
//		            'secret_key' => "ZVhfg4c0GwHn6JJqoCoU3wWQSYvgshQsd7HtcUuTqchwV0XkDlfHSbxKa31ClR5yZN2bHw57ImEDmCX0YdHQoeByiCbDugbt3eFL",
//		            'title' => "Order no=".$trans_header_obj->id."",
//		            'cc_first_name' => "".$shipping_first_name."",
//		            'cc_last_name' => "".$shipping_first_name."",
//		            'email' => "".$shipping_email."",
//		            'cc_phone_number' => "".$cc_phone_number."",
//		            'phone_number' => "".$shipping_phone."",
//		            'billing_address' => "".$shipping_address_1."",
//		            'city' => "".$shipping_city."",
//		            'state' => "".$shipping_address_2."",
//		            'postal_code' => "".$postal_code."",
//		            'country' => "".$country."",
//		            'address_shipping' => "".$shipping_address_1."",
//		            'city_shipping' => "".$shipping_city."",
//		            'state_shipping' => "".$shipping_address_2."",
//		            'postal_code_shipping' => "".$postal_code."",
//		            'country_shipping' => "".$country."",
//		            'currency' => "SAR",
//		            "products_per_title"=> $trans_header_obj->total_item_count,
//		            "unit_price"=> "".$unit_price."",
//		            'quantity' => "1",
//		            'other_charges' => "0",
//		            'amount' => "".$unit_price."",
//		            'discount'=>"0",
//		            "msg_lang" => "arabic",
//		            "reference_no" => "".$trans_header_obj->id."",
//		            "site_url" => "https://www.eshtri.net",
//		            'return_url' => "https://www.eshtri.net/paymentsuccessurl/transection/".$trans_header_obj->id."",
//		            "cms_with_version" => "API USING PHP",
//		            "payment_type" => "mada"
//		        );
//
//	         $response= $this->paytabs->create_pay_page($postdata);
	        // echo $response->payment_url;   
	         
	         if($response->tran_ref){
	            // $status=array('success'=>1, 'url'=>$response->payment_url, 'result' =>$response->result, 'p_id' =>$response->p_id);
	             $payment_data['payment_reference']=$response->tran_ref;
					// save data

				if($this->Transactionheader->save($payment_data, $trans_header_obj->id)) {
		        $obj = $this->Transactionheader->get_one($trans_header_obj->id);
				$obj->payment_url=$response->redirect_url;
				$obj->p_id=$response->tran_ref;
				
				
				if ( isset( $obj->time_slot_id )) {
					$tmp_time_slot = $this->Timeslotsdetails->get_one( $obj->time_slot_id );
					$obj->time_slot = $tmp_time_slot;
				}
				
				// shop object
				if ( isset( $obj->shop_id )) {
					$tmp_shop = $this->Shop->get_one( $obj->shop_id );

					$this->ps_adapter->convert_shop( $tmp_shop );

					$obj->shop = $tmp_shop;
				}

				$all_traking_status = $this->Transactionstatus->get_all_by()->result();
				foreach ($all_traking_status as $key => $value) {
					$status_conds['transactions_header_id'] = $obj->id;
					$status_conds['transactions_status_id'] = $value->id;
					$trakingInfo = $this->Transactionstatustracking->get_one_by( $status_conds );
					$all_traking_status[$key]->title=get_msg(''.$value->title.'');
					if($trakingInfo->id){
						$all_traking_status[$key]->hasUpdate=1;
						$all_traking_status[$key]->changed_date=$trakingInfo->date_created;
					}else{
						$all_traking_status[$key]->hasUpdate=0;
						$all_traking_status[$key]->changed_date=null;
					}
					
				}
				$obj->traking_status = $all_traking_status;
				$obj->all_detail = $this->Transactiondetail->get_all_by(array('transactions_header_id' =>$obj->id))->result();
				$this->custom_response($obj);
				}else{
				$this->error_response( get_msg( 'you_can_not_pay_now' ) );
				}


	          }

		}
		// Paytabs ends
        
    }

    function changePaymentMethod_post()
    {
        $trans_header_id=$this->post( 'id');
        $payment_method=$this->post( 'payment_method');

        $trans_header_obj = $this->Transactionheader->get_one($trans_header_id);
        if($trans_header_obj->id ==null || !$trans_header_obj->id){
			$this->error_response( get_msg( 'order_not_available' ) );
		}

		if($trans_header_obj->trans_status_id ==null || $trans_header_obj->trans_status_id==5){
			$this->error_response( get_msg( 'you_are_not_athorized_to_update_this' ) );
		}
		// if($assign_to_id != $trans_header_obj->assign_to){
		// 	$this->error_response( get_msg( 'you_are_not_athorized_to_update_this' ) );
		// }
 
        $dataToSubmit = array("payment_method" => $payment_method);
        $this->Transactionheader->save($dataToSubmit, $trans_header_id);
		$this->success_response( get_msg( 'success_payment_method_changed'));
    }
	function stock_post()
    {
        $trans_details_id=$this->post( 'id');
        $trans_header_id=$this->post( 'transactions_header_id');
       // $assign_to_id=$this->post( 'assign_to');
        $stock_status=$this->post( 'stock_status');

        $trans_header_obj = $this->Transactionheader->get_one($trans_header_id);
        if($trans_header_obj->id ==null || !$trans_header_obj->id){
			$this->error_response( get_msg( 'order_not_available' ) );
		}
		// if($assign_to_id != $trans_header_obj->assign_to){
		// 	$this->error_response( get_msg( 'you_are_not_athorized_to_update_this' ) );
		// }
        $Transactiondetail = $this->Transactiondetail->get_one_by( array( 'id' => $trans_details_id));
 		if($trans_header_obj->id ==null || !$trans_header_obj->id){
			$this->error_response( get_msg( 'order_not_available' ) );
		}
        $dataToSubmit = array("stock_status" => $stock_status);
        $this->Transactiondetail->save($dataToSubmit, $trans_details_id);

        $totalItem = 0;
        $totalItemAmountNoTax = 0.0;
        $sub_total_amount = 0.0;
        $discount_amount = 0.0;
        $tax_amount = 0.0;
        $all_detail = $this->Transactiondetail->get_all_by(array('transactions_header_id' =>$trans_header_id))->result();
        foreach($all_detail as $sinleDetails) {
                $qtyDtls= $sinleDetails->qty;
                $originalPriceDtls = $sinleDetails->original_price;
                $discountPercentDtls = $sinleDetails->discount_percent;
                $price=$originalPriceDtls;
                $discount_amount=0.0;
                if($discountPercentDtls>0){
                    $discount_amount=(($sinleDetails->discount_percent / 100) * $originalPriceDtls);
                    $originalPriceDtls = ($originalPriceDtls - (($sinleDetails->discount_percent / 100) * $originalPriceDtls));
                    $price=$originalPriceDtls;
                }

                $stockDtls = $sinleDetails->stock_status;
                $itemSubtotal=$originalPriceDtls * $qtyDtls;
                if($stockDtls=='1') {
                    $totalItem += $qtyDtls;
                    $totalItemAmountNoTax += $itemSubtotal;
                    $sub_total_amount +=$itemSubtotal;
                    $discount_amount += $discount_amount;
                }

        }

        $total_item_amount=$sub_total_amount - $discount_amount;
        if($trans_header_obj->coupon_discount_amount >0){
            $total_item_amount=$total_item_amount - $trans_header_obj->coupon_discount_amount;
        }

        $tax_amount=(($trans_header_obj->tax_percent / 100) * $total_item_amount);
        $master_data = array(
            'total_item_count'=> $totalItem,
            'sub_total_amount'=> $sub_total_amount,
            'discount_amount'=> $discount_amount,
            'tax_amount'=> $tax_amount,
            'shipping_method_amount'=> $trans_header_obj->shipping_method_amount,
            'total_item_amount'=> $total_item_amount,
            'balance_amount'=> $total_item_amount + $trans_header_obj->shipping_method_amount + $tax_amount,
        );

        // save data
        $this->Transactionheader->save($master_data, $trans_header_id);

		$this->success_response( get_msg( 'success_stock_status_changed'));

    }
	function checkedunchecked_post()
    {
        $trans_details_id=$this->post( 'id');
        $trans_header_id=$this->post( 'transactions_header_id');
       // $assign_to_id=$this->post( 'assign_to');
        $check_status=$this->post( 'check_status');

        $trans_header_obj = $this->Transactionheader->get_one($trans_header_id);
        if($trans_header_obj->id ==null || !$trans_header_obj->id){
			$this->error_response( get_msg( 'order_not_available' ) );
		}
		// if($assign_to_id != $trans_header_obj->assign_to){
		// 	$this->error_response( get_msg( 'you_are_not_athorized_to_update_this' ) );
		// }
        $Transactiondetail = $this->Transactiondetail->get_one_by( array( 'id' => $trans_details_id));
 		if($trans_header_obj->id ==null || !$trans_header_obj->id){
			$this->error_response( get_msg( 'order_not_available' ) );
		}
        $dataToSubmit = array("check_status" => $check_status);
        $this->Transactiondetail->save($dataToSubmit, $trans_details_id);
        $this->success_response( get_msg( 'success_check_status_changed'));

    }

    function orderaccept_post()
    {
        $trans_header_id=$this->post( 'id');
        $assign_to_id=$this->post( 'assign_to');

        $trans_header_obj = $this->Transactionheader->get_one($trans_header_id);
        if($trans_header_obj->id ==null || !$trans_header_obj->id){
			$this->error_response( get_msg( 'order_not_available' ) );
		}
		if($trans_header_obj->assign_to !=null){
			$this->error_response( get_msg( 'order_already_accepted' ) );
		}
        
        $dataToSubmit = array("assign_to" => $assign_to_id);
        $this->Transactionheader->save($dataToSubmit, $trans_header_id);
		$this->success_response( get_msg( 'success_order_assigned'));

    }

    function orderreject_post()
    {
        $trans_header_id=$this->post( 'id');
        $assign_to_id=$this->post( 'assign_to');

  //       $trans_header_obj = $this->Transactionheader->get_one($trans_header_id);
  //       if($trans_header_obj->id ==null || !$trans_header_obj->id){
		// 	$this->error_response( get_msg( 'order_not_available' ) );
		// }
		// if($trans_header_obj->assign_to !=null){
		// 	$this->error_response( get_msg( 'order_already_accepted' ) );
		// }
        
  //       $dataToSubmit = array("assign_to" => $assign_to_id);
  //       $this->Transactionheader->save($dataToSubmit, $trans_header_id);
		// $this->success_response( get_msg( 'success_order_assigned'));

    }

 	function deliveryboyorder_post()
    {
        $delivery_boy_id=$this->post( 'd_id');
        $conds = array(
			"assign_to" => $delivery_boy_id
		);
		$data = $this->Transactionheader->get_all_by($conds)->result();

		$this->custom_response( $data );

    }
	function checking_post()
	{

		$trans_details = $this->post( 'details' );

		$fail_trans = array();

		$fail_not_available_products = array();

		$fail_delete_products = array();

		$fail_price_change_products = array();

		$fail_trans = $this->ps_adapter->transaction_checking($trans_details);


		//[0] - is for delete products
		if(count($fail_trans[0]) > 0) {

			// Rule 1 : Delete Product Checking
			for($g = 0; $g<count($fail_trans[0]); $g++) {

				$fail_delete_products[] = $fail_trans[0][$g];

			}


		} else if( count($fail_trans[1]) > 0 ) {

			// Rule 2 : Avaiable Product Checking
			for($j = 0; $j<count($fail_trans[1]); $j++) {

				$fail_not_available_products[] = $fail_trans[1][$j];

			} 


		} else if( count($fail_trans[2]) > 0 ) {

			// Rile 3 : Price Checking
			for($h = 0; $h<count($fail_trans[2]); $h++) {

				$fail_price_change_products[] = $fail_trans[2][$h];

			}

		}

		$fail_delete_products = array_unique($fail_delete_products);


		if( count($fail_delete_products) > 0 ) {


			$prds = $this->Product->get_all_in($fail_delete_products)->result();
			$this->custom_fail_response( $prds,true, "Product is deleted from the system." );

		} else if (count($fail_not_available_products) > 0) {

			$prds = $this->Product->get_all_in($fail_not_available_products)->result();
			$this->custom_fail_response( $prds,true, "Product is not available from the system." );

		} else if (count($fail_price_change_products) > 0) {

			$prds = $this->Product->get_all_in($fail_price_change_products)->result();
			$this->custom_fail_response( $prds,true, "Product Price is has been changed." );

		} else {
			echo "ok!!!!";
		}

	}


	function stripe_checking_get()
	{

		\Stripe\Stripe::setApiKey("sk_test_lxHim6W6aJAjb4jjAtfviY0t");
		try {
		  \Stripe\Charge::all();
		  echo "TLS 1.2 supported, no action required.";
		} catch (\Stripe\Error\ApiConnection $e) {
		  echo "TLS 1.2 is not supported. You will need to upgrade your integration.";
		}

	}




}
