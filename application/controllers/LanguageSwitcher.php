<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LanguageSwitcher extends CI_Controller
{
    public function __construct() {
        parent::__construct(); 
        $this->load->library('session');   
        $this->load->helper('url'); 
    }
 
    function switchLang($language = "") {
        try {
             $language=$this->uri->segment(3);
            if($language){
                $language = $language;
            }else{
                $language = "arabic";
            }
            
            $this->session->set_userdata('site_lang', $language);
            if($language=='english'){
                $langcode="en";
                $this->session->set_userdata('user_language_id', "lang4d0a0f9ccf889b9fa0642a76368c5bdf");
            }else{
                $langcode="ar";
                $this->session->set_userdata('user_language_id', "langfe7b3532541cb580ead1145ae8cf5eb2");
            }
            $langcode = ($langcode != "") ? $langcode : "ar";
            $this->session->set_userdata('code_lang', $langcode);
            
            redirect($_SERVER['HTTP_REFERER']);
        }catch (exception $e) {
            redirect("/");
        }

    }
}
