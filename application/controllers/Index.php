<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Index extends FE_Controller {


	public function __construct()
	{
		// parent::__construct();

		parent::__construct( NO_AUTH_CONTROL, 'REGISTER' );
        $this->load->library('session');
        $language=$this->session->userdata('site_lang');
        $language = ($language != "") ? $language : "arabic";
        $this->session->set_userdata('site_lang', $language);

        $langcode=$this->session->userdata('code_lang');
        $langcode = ($langcode != "") ? $langcode : "ar";
        $this->session->set_userdata('code_lang', $langcode);
	}

	/**
	 * Redirec to home page
	 */
	function index() {
//	    $url="https://secure.paytabs.sa/payment/request";
//        $data=array(
//            "profile_id" => 58545,
//            "tran_type" => "sale",
//            "tran_class" => "ecom",
//            "cart_description" => "Order nnumber-660",
//            "cart_id" => "Order-660",
//            "cart_currency" => "SAR",
//            "cart_amount" => 100,
//            "callback" => "https://www.eshtri.net",
//            "return" => "https://www.eshtri.net/paymentsuccessurl/transection/660"
//        );
//        $postdata = json_encode($data);
//
//        $ch = curl_init($url);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: SDJN9JL69Z-JBDDDMB9GW-JG9DWZGNKB'));
//        $result = curl_exec($ch);
//        curl_close($ch);
//        echo "<pre>";
//        echo print_r(json_decode($result));
//        echo "</pre>";


//
//            "merchant_email" => "info@eshtri.net",
//            'secret_key' => "ZVhfg4c0GwHn6JJqoCoU3wWQSYvgshQsd7HtcUuTqchwV0XkDlfHSbxKa31ClR5yZN2bHw57ImEDmCX0YdHQoeByiCbDugbt3eFL",
//            'title' => "Order no=".$trans_header_id."",
//            'cc_first_name' => "".$shipping_first_name."",
//            'cc_last_name' => "".$shipping_first_name."",
//            'email' => "".$shipping_email."",
//            'cc_phone_number' => "".$cc_phone_number."",
//            'phone_number' => "".$shipping_phone."",
//            'billing_address' => "".$shipping_address_1."",
//            'city' => "".$shipping_city."",
//            'state' => "".$shipping_address_2."",
//            'postal_code' => "".$postal_code."",
//            'country' => "".$country."",
//            'address_shipping' => "".$shipping_address_1."",
//            'city_shipping' => "".$shipping_city."",
//            'state_shipping' => "".$shipping_address_2."",
//            'postal_code_shipping' => "".$postal_code."",
//            'country_shipping' => "".$country."",
//            'currency' => "SAR",
//            "products_per_title"=> $totalItem,
//            "unit_price"=> "".$unit_price."",
//            'quantity' => "1",
//            'other_charges' => "0",
//            'amount' => "".$unit_price."",
//            'discount'=>"0",
//            "msg_lang" => "arabic",
//            "reference_no" => "".$trans_header_id."",
//            "site_url" => "https://www.eshtri.net",
//            'return_url' => "https://www.eshtri.net/paymentsuccessurl/transection/".$trans_header_id."",
//            "cms_with_version" => "API USING PHP",
        //"payment_type" => "mada"
//        return json_decode($this->post(PAYPAGE_URL, $values));
       // $response= $this->paytabs->create_pay_page($postdata);
        // echo $response->payment_url;

//        if($response->response_code == 4012){
//            $status=array('success'=>1, 'url'=>$response->payment_url, 'result' =>$response->result, 'p_id' =>$response->p_id);
//            $payment_data['payment_reference']=$response->p_id;
//            // save data
//            $this->Transactionheader->save($payment_data, $trans_header_id);
//        }

        $current_user_id=$this->session->userdata('current_user_id');
        $current_user_mobile=$this->session->userdata('current_user_mobile');
        $this->data['bodyclass'] = "outside_shop basicpages homepage";
        $this->data['page_name'] = "website_home";
        $this->data['redirect_url'] = site_url('/');
        $this->session->set_userdata('selected_front_shop_id', "shop4f302930cd7fecfc9b60a4b240a5c736");
        $shop_id= $this->session->userdata('selected_front_shop_id');
        $this->data['shop_id'] = $shop_id;
        $this->data['is_shop_dashboard'] = true;
        $this->data['discountproducts'] = $this->Product->get_all_by( array("is_discount"=>1, "is_available"=>1, "shop_id"=>$shop_id));
        $this->data['featuredproducts'] = $this->Product->get_all_by( array("is_featured"=>1, "is_available"=>1, "shop_id"=>$shop_id));
        $this->data['newproduct'] = $this->Product->get_all_by( array("order_by"=>1, "order_by_field"=>"added_date", "order_by_type"=>"asc", "shop_id"=>$shop_id) , 20, 0 );
        $this->data['popularproduct'] = $this->Product->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>$shop_id) , 50, 0 );
        $this->data['popularcategory'] = $this->Category->get_all_by( array("order_by"=>1, "order_by_field"=>"touch_count", "order_by_type"=>"asc", "shop_id"=>$shop_id));
        $current_time = $this->session->userdata('current_time');
        if($current_time){
            $nowtime=$current_time;
        }else{
            $nowtime=date("Ymdgis");
            $this->session->set_userdata('current_time', $nowtime);
        }
        $this->data['carttime'] = $nowtime;
        $transaction=array();
        if($nowtime){
            $transaction=$this->Cartheader->get_one_by(array('time' => $nowtime, 'shop_id' => $shop_id));
        }
        $this->data['transaction'] = $transaction;
        $this->load_front_template( 'home',$this->data, true );
//		$this->load->view('front/home');
	}
	function areas() {
		$this->load->view('front/areas');
	}	

	function contactus() {
		$this->load->view('front/contactus');
	}
	function app() {
		$this->load->view('front/app');
	}

	function privacy() {
		$this->load->view('front/privacy');
	}
	function terms() {
		$this->load->view('front/terms');
	}
	function policies() {
		$this->load->view('front/policies');
	}
	function aboutus() {
		$this->load->view('front/aboutus');
	}

	function partnership() {
		$this->load->view('front/partnership');
	}

	function printOrderDetailsPdf()
	{
		$this->data['action_title'] = get_msg( 'trans_detail' );
		$id=$this->input->get('order_id');
		$detail = $this->Transactionheader->get_one( $id );

//http://localhost/eshtri/printOrderDetailsPdf?order_id=46&driver_id=usr2dd0cbb424a797c6e888618b4e2d34ac
		$delivery_boy_id=$this->input->get( 'driver_id');
		if($detail->assign_to != $delivery_boy_id){
			return;
			die();
		}

		$this->data['transaction'] = $detail;
		$html = $this->load->view('backend/orders/printdetail',$this->data, true);  

		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8']);
		if($this->session->userdata('language_code')=="ar"){
		$mpdf->SetDirectionality('rtl');
		}
		$mpdf->autoScriptToLang = true;
		$mpdf->autoLangToFont = true;
		$mpdf->WriteHTML($html);
		$mpdf->Output('order-no-'.$id.'.pdf', 'D');   
	}
	function userbymobile(){
		header('Content-Type: application/json');
		$status=array('message' => get_msg( 'err_user_not_exist') ,'code'=>'','userexist'=>0, 'status'=>0);

		if (!$this->input->get('user_phone')){
			$status=array('message' => get_msg( 'err_user_not_exist') ,'code'=>'','userexist'=>0, 'status'=>0);
			print json_encode($status);
			exit;
		} 
		$user=$this->db->select('*')->from('core_users')->where('user_phone', $this->input->get('user_phone'))->get()->row();


		$mobile=$this->input->get('user_phone');


		if($user){
            //sendtexttomobile($mobile, $msg); //global function for send sms
			$status=array('message' => 'User Exist', 'code'=>$user->code,'userid'=>$user->user_id,'usertype'=>$user->role_id,'usermobile'=>$user->user_phone, 'userexist'=>1,'status'=>1,'user'=>$user);
		}else{
            //sendtexttomobile($mobile, $msg); //global function for send sms

			$status=array('message' => 'User Not Exist', 'code'=>'', 'userexist'=>0,'status'=>0);
		}
		print json_encode($status);
	}

	function createOrupdateuser(){
		$status=array('message' => get_msg( 'err_user_register') ,'code'=>'','userexist'=>0, 'status'=>0, 'userid'=>"", 'usermobile'=>"");
		if (!$this->input->get('user_phone')){
			$status=array('message' => get_msg( 'err_user_register') ,'code'=>'','userexist'=>0, 'status'=>0, 'userid'=>"", 'usermobile'=>"");
			print json_encode($status);
			exit;
		} 


		$user=$this->db->select('*')->from('core_users')->where('user_phone', $this->input->get('user_phone'))->get()->row();
		$conds['user_phone'] = $this->input->get('user_phone');
		$user_datas = $this->User->get_one_by($conds);
		$user_id = $user_datas->user_id;
		if(!$user){
			$status=array('message' => get_msg( 'err_user_register') , 'userid'=>"", 'usermobile'=>"",'usertype'=>"",'userinfo'=>"",'status'=>0);

		}else{
 				//User already exist in DB
 				//$code = $this->input->get('code');
 				//if($code !="1006"){
			$user_data = array(
				'status'    => 1
			);
			$this->User->save($user_data,$user_id);
 				//}
			
			$status=array('message' => get_msg( 'new_user_register'), 'usermobile'=>$user->user_phone, 'userid'=>$user->user_id,'usertype'=>$user->role_id,'userinfo'=>$user,'status'=>1);
		}


		print json_encode($status);
	}


	function deliveryboyorder()
	{
		header('Content-Type: application/json');
		$delivery_boy_id=$this->input->get( 'd_id');
		$trans_status_id=$this->input->get('trans_status_id');
		$conds = array(
			"assign_to" => $delivery_boy_id,
		);
		if($trans_status_id=='5'){
//			$conds["trans_status_id"]='5';
//			$data = $this->Transactionheader->get_all_by($conds)->result();
            $querySQL="SELECT mk_transactions_header.*, mk_shops.name as shop_name FROM mk_transactions_header LEFT JOIN mk_shops ON mk_transactions_header.shop_id = mk_shops.id WHERE mk_transactions_header.assign_to='$delivery_boy_id' AND mk_transactions_header.trans_status_id ='5' ORDER BY mk_transactions_header.id DESC";
            $resultSet = $this->Transactionheader->queryPrepare($querySQL);
            $data = $resultSet->result();
		}else{
			$querySQL="SELECT mk_transactions_header.*, mk_shops.name as shop_name FROM mk_transactions_header LEFT JOIN mk_shops ON mk_transactions_header.shop_id = mk_shops.id WHERE mk_transactions_header.assign_to='$delivery_boy_id' AND mk_transactions_header.trans_status_id !='5' AND mk_transactions_header.reject_status=0";
			$resultSet = $this->Transactionheader->queryPrepare($querySQL);
			$data = $resultSet->result();
		}
		
		print json_encode(array(
			"data" => $data
		));

	}	

	function unassignedorder()
	{
		header('Content-Type: application/json');
		$delivery_boy_id=$this->input->get( 'driver_id');
		$deliveryBoyDetails=$this->User->get_one_by(array('user_id' =>$delivery_boy_id));
		$conds = array(
			"assign_to" => $delivery_boy_id,
		);
		if($deliveryBoyDetails->role_id !='5'){
			print json_encode(array(
			"data" => false
		));
		}else{
            $querySQL="SELECT mk_transactions_header.*, mk_shops.name as shop_name FROM mk_transactions_header LEFT JOIN mk_shops ON mk_transactions_header.shop_id = mk_shops.id WHERE mk_transactions_header.trans_status_id !='5' AND mk_transactions_header.reject_status=0 AND mk_transactions_header.assign_to IS NULL  ORDER BY mk_transactions_header.id ASC";
            $resultSet = $this->Transactionheader->queryPrepare($querySQL);
            $data = $resultSet->result();
//			$querySQL="SELECT * FROM mk_transactions_header WHERE trans_status_id !=5 AND reject_status=0 AND assign_to IS NULL";
//			$resultSet = $this->Transactionheader->queryPrepare($querySQL);
//			$data = $resultSet->result();
		}
		
		print json_encode(array(
			"data" => $data
		));

	}	
	function deliveryboysingleorder()
	{
		header('Content-Type: application/json');
		$orderid=$this->input->get( 'orderid');
		// $trans_status_id=$this->input->get( 'trans_status_id');
		$conds = array(
			"id" => $orderid,
			// "trans_status_id" => $trans_status_id
		);
		$orderdata = $this->Transactionheader->get_one_by($conds);

		$condsshop['id'] = $orderdata->shop_id;
		$shop_detail =  $this->Shop->get_one_by($condsshop);
		$orderdata->shop_name=$shop_detail->name;
		
		$querySQL="SELECT mk_transactions_detail.* FROM mk_transactions_detail 
		WHERE mk_transactions_detail.transactions_header_id='$orderid' ORDER BY mk_transactions_detail.product_category_id DESC";
		$resultSet = $this->Transactiondetail->queryPrepare($querySQL);
		
		$all_detail = $resultSet->result();
		$orderdata->product_orders=$all_detail;

		// $condss['transactions_header_id'] = $orderid;
		// $all_detail =  $this->Transactiondetail->get_all_by( $condss )->result();
		// $orderdata->product_orders=$all_detail;

		$consAddres['is_default'] = 1;
		$consAddres['user_id'] = $orderdata->user_id;
		$Addres_detail =  $this->UserAddress->get_one_by($consAddres);
		$orderdata->current_address=$Addres_detail;
		//current_address
		print json_encode(array(
			"data" => $orderdata
		));

	}

	function orderdetails()
	{
		header('Content-Type: application/json');
		$sale_id=$this->input->get( 'sale_id');
		$conds['transactions_header_id'] = $sale_id;
		$all_detail =  $this->Transactiondetail->get_all_by( $conds )->result();
		
		print json_encode(array(
			"data" => $all_detail
		));

	}
	function orders()
	{
		header('Content-Type: application/json');
		$delivery_boy_id=$this->input->get( 'd_id');
		$conds = array(
			"assign_to" => $delivery_boy_id
		);
		$data = $this->Transactionheader->get_all_by($conds)->result();
		print json_encode(array(
			"data" => $data
		));

	}
	function markdelivered(){

		//error_reporting(0);

		$order_id=$this->input->get("id");
		$order_status_id=$this->input->get("trans_status_id");

		$conds = array(
			"id" => $order_id,
			// "trans_status_id" => $trans_status_id
		);
		$orderdata = $this->Transactionheader->get_one_by($conds);


		if(!$order_id || !$order_status_id || $orderdata->trans_status_id==5){
			$conds = array(
				"id" => $order_id,
				// "trans_status_id" => $trans_status_id
			);
			$orderdata = $this->Transactionheader->get_one_by($conds);
	
			$condsshop['id'] = $orderdata->shop_id;
			$shop_detail =  $this->Shop->get_one_by($condsshop);
			$orderdata->shop_details=$shop_detail->name;
			
			$querySQL="SELECT mk_transactions_detail.*, (SELECT core_images.img_path FROM core_images WHERE core_images.img_type='product' 
			AND core_images.img_parent_id=mk_transactions_detail.product_id ORDER BY core_images.is_default ASC LIMIT 1) as default_photo FROM mk_transactions_detail 
			WHERE mk_transactions_detail.transactions_header_id='$order_id'";
			$resultSet = $this->Transactiondetail->queryPrepare($querySQL);
			
			$all_detail = $resultSet->result();
			$orderdata->product_orders=$all_detail;
	
			$consAddres['is_default'] = 1;
			$consAddres['user_id'] = $orderdata->user_id;
			$Addres_detail =  $this->UserAddress->get_one_by($consAddres);
			$orderdata->current_address=$Addres_detail;
			//current_address
			print json_encode(array(
				"data" => $orderdata
			));
			exit;
		}
		

		//$this->db->trans_start();
		if($order_status_id && $order_id){
			if($orderdata->trans_status_id && $order_status_id !=5){
				$order_status_id=$orderdata->trans_status_id+1;
			}
			$assign_to_data = array( 'trans_status_id'=> $order_status_id);
			$this->Transactionheader->save($assign_to_data, $order_id);
			$status_data = array( 'transactions_header_id'=> $order_id, 'transactions_status_id'=> $order_status_id);
			// save data
			$this->Transactionstatustracking->save($status_data);
		}
		// save data
		if($order_status_id==5){
		// AccVcrMst vcrMst = new AccVcrMst();
  //       AccVcrDtl receiveAbleTotal = new AccVcrDtl();
  //       AccVcrDtl serviceRevenue = new AccVcrDtl();
		$orderDetails=$this->Transactionheader->get_one_by(array('id' =>$order_id));
		$deliveryBoyDetails=$this->User->get_one_by(array('user_id' =>$orderDetails->assign_to));
		$deliveryCommissionDetails=$this->Acc_deliveryboy_charge->get_one_by(array('driver_id' =>$orderDetails->assign_to));
		$shopCommissionDetails=$this->Acc_comission_seller->get_one_by(array('shop_id' =>$orderDetails->shop_id));
		$shopDetails=$this->Shop->get_one_by(array('id' =>$orderDetails->shop_id));
		$period_data = $this->Acc_period->get_one_by(array('is_current' =>1));
		$receiveAbleHead = $this->Acc_coa->get_one_by(array('journals_type' =>'Account_receivable'));
        $deliveryFeeCommissionHead = $this->Acc_coa->get_one_by(array('journals_type' =>'DeliveryFeeCommission'));
        $salesCommissionHead = $this->Acc_coa->get_one_by(array('journals_type' =>'SalesCommission'));

        $deliveryFee=$orderDetails->shipping_method_amount;
        $deliveryboyType=$deliveryCommissionDetails->contract_type;
        $deliveryboyCommissionRate=$deliveryCommissionDetails->order_comission;

        $ourDeliveryFeeRevinue=$deliveryFee;
        if($deliveryboyType=="DeliveryBoy"){
        	$ourDeliveryFeeRevinue = ($deliveryboyCommissionRate / 100) * $deliveryFee;
        }

		$shopCommissionRate=$shopCommissionDetails->amount_percentage;
        $salesAmount=$orderDetails->sub_total_amount;

        $ourRevinueOnSale=0.00;
        if($salesAmount && $shopCommissionRate>0){ 
        	$ourRevinueOnSale = ($shopCommissionRate / 100) * $salesAmount;
        }

        // voucher master for delivery fees
     if($ourDeliveryFeeRevinue>0){
		$Mstdata = array(
			'entity_type' => "Income_Voucher",
			'service_type' => "DeliveryFeeCommission",
			'caption' => get_msg('comission_Earnings')." - ".date("Y-m-d H:i:s"),
			'acc_period_id' => $period_data->id,
			'trans_date' => date("Y-m-d H:i:s"),
			'reference_code' => $order_id,
			'total_amount' => $ourDeliveryFeeRevinue,
			'narration' => 'Total amount='.$orderDetails->balance_amount.', Tax='.$orderDetails->tax_amount.', Discount='.$orderDetails->discount_amount.', Shipping cost='.$orderDetails->shipping_method_amount.', Product price without tax='.$orderDetails->sub_total_amount,
			'stake_holder_type' => $deliveryboyType,
			'stakeholder_id' => $deliveryCommissionDetails->driver_id,
			'stakeholder_name' => $deliveryBoyDetails->user_name,
			'confirm_status' => "Draft",
			'approve_status' => "Draft",
			'active_status' => "Active",
		);
		$inserted=$this->Acc_vcr_mst->accSave($Mstdata);
		


		$receiveAbleDtls = array(
			'vcr_mst_id' => $inserted,
			'trans_date' => date("Y-m-d H:i:s"),
			'narration' => 'Shipping cost='.$orderDetails->shipping_method_amount,
			'acc_coa_id' => $receiveAbleHead->id,
			'dr_amount' => $ourDeliveryFeeRevinue,
			'cr_amount' => 0.00,
			'drcr_type' => 'Debit',
			'stake_holder_type' => $deliveryboyType,
			'stakeholder_id' => $deliveryCommissionDetails->driver_id,
			'stakeholder_name' => $deliveryBoyDetails->user_name,
			'service_type' => "DeliveryFeeCommission",
		);
		$this->Acc_vcr_dtl->save($receiveAbleDtls);

		$serviceRevenueDtls = array(
			'vcr_mst_id' => $inserted,
			'trans_date' => date("Y-m-d H:i:s"),
			'narration' => 'Shipping cost='.$orderDetails->shipping_method_amount,
			'acc_coa_id' => $deliveryFeeCommissionHead->id,
			'dr_amount' => 0.00,
			'cr_amount' => $ourDeliveryFeeRevinue,
			'drcr_type' => 'Credit',
			'stake_holder_type' => $deliveryboyType,
			'stakeholder_id' => $deliveryCommissionDetails->driver_id,
			'stakeholder_name' => $deliveryBoyDetails->user_name,
			'service_type' => "DeliveryFeeCommission",
		);
		$this->Acc_vcr_dtl->save($serviceRevenueDtls);
		}
		// voucher for comission from saller
		if($ourRevinueOnSale >0){
		$Mstdata = array(
			'entity_type' => "Income_Voucher",
			'service_type' => "SalesCommission",
			'caption' => get_msg('comission_Earnings')." - ".date("Y-m-d H:i:s"),
			'acc_period_id' => $period_data->id,
			'trans_date' => date("Y-m-d H:i:s"),
			'reference_code' => $order_id,
			'total_amount' => $ourRevinueOnSale,
			'narration' => 'Total amount='.$orderDetails->balance_amount.', Tax='.$orderDetails->tax_amount.', Discount='.$orderDetails->discount_amount.', Shipping cost='.$orderDetails->shipping_method_amount.', Product price without tax='.$orderDetails->sub_total_amount,
			'stake_holder_type' => "Seller",
			'stakeholder_id' => $shopDetails->id,
			'stakeholder_name' => $shopDetails->name,
			'confirm_status' => "Draft",
			'approve_status' => "Draft",
			'active_status' => "Active",
		);
		$inserted=$this->Acc_vcr_mst->accSave($Mstdata);
		


		$receiveAbleDtls = array(
			'vcr_mst_id' => $inserted,
			'trans_date' => date("Y-m-d H:i:s"),
			'narration' => 'Total amount='.$orderDetails->balance_amount.', Tax='.$orderDetails->tax_amount.', Product price without tax='.$orderDetails->sub_total_amount,
			'acc_coa_id' => $receiveAbleHead->id,
			'dr_amount' => $ourRevinueOnSale,
			'cr_amount' => 0.00,
			'drcr_type' => 'Debit',
			'stake_holder_type' => "Seller",
			'stakeholder_id' => $shopDetails->id,
			'stakeholder_name' => $shopDetails->name,
			'service_type' => "SalesCommission",
		);
		$this->Acc_vcr_dtl->save($receiveAbleDtls);

		$serviceRevenueDtls = array(
			'vcr_mst_id' => $inserted,
			'trans_date' => date("Y-m-d H:i:s"),
			'narration' => 'Total amount='.$orderDetails->balance_amount.', Tax='.$orderDetails->tax_amount.', Product price without tax='.$orderDetails->sub_total_amount,
			'acc_coa_id' => $salesCommissionHead->id,
			'dr_amount' => 0.00,
			'cr_amount' => $ourRevinueOnSale,
			'drcr_type' => 'Credit',
			'stake_holder_type' => "Seller",
			'stakeholder_id' => $shopDetails->id,
			'stakeholder_name' => $shopDetails->name,
			'service_type' => "SalesCommission",
		);
		$this->Acc_vcr_dtl->save($serviceRevenueDtls);
		}
	}

//if(isset( $this->input->post("signature"))){
		// $img = $this->input->post("signature");
		// $img = str_replace('data:image/png;base64,', '', $img);
		// $img = str_replace(' ', '+', $img);
		// $image = base64_decode($img);
		// if ($image) {
		// 	header('Content-Type: bitmap; charset=utf-8');
		// 	$path="/uploads/signature/" . uniqid() . '.png';
		// 	$filelocation = ".".$path;
		// 	file_put_contents($filelocation, $image);
		// 	$order_id=$this->input->post("id");
		// 	$Signaturedata = array(
		// 		'order_id' => $this->input->post("id"),
		// 		'signature' => $path
		// 	);
	
		// 	$this->Signature->save($Signaturedata);
		// }
	   // }
	   //$this->db->trans_commit();
	   $conds = array(
			"id" => $order_id,
			// "trans_status_id" => $trans_status_id
		);
		$orderdata = $this->Transactionheader->get_one_by($conds);

		$condsshop['id'] = $orderdata->shop_id;
		$shop_detail =  $this->Shop->get_one_by($condsshop);
		$orderdata->shop_details=$shop_detail->name;
		
		$querySQL="SELECT mk_transactions_detail.*, (SELECT core_images.img_path FROM core_images WHERE core_images.img_type='product' 
		AND core_images.img_parent_id=mk_transactions_detail.product_id ORDER BY core_images.is_default ASC LIMIT 1) as default_photo FROM mk_transactions_detail 
		WHERE mk_transactions_detail.transactions_header_id='$order_id'";
		$resultSet = $this->Transactiondetail->queryPrepare($querySQL);
		
		$all_detail = $resultSet->result();
		$orderdata->product_orders=$all_detail;

		$consAddres['is_default'] = 1;
		$consAddres['user_id'] = $orderdata->user_id;
		$Addres_detail =  $this->UserAddress->get_one_by($consAddres);
		$orderdata->current_address=$Addres_detail;
		//current_address
		print json_encode(array(
			"data" => $orderdata
		));
		exit;


		//echo json_encode($data);
	}

		// function paytabs()
		// {
		// 	$payment_method="paytabs";
		// 	if($payment_method=="paytabs"){
		// 		$merchant_email='info@eshtri.net';
		// 		$secret_key='ZVhfg4c0GwHn6JJqoCoU3wWQSYvgshQsd7HtcUuTqchwV0XkDlfHSbxKa31ClR5yZN2bHw57ImEDmCX0YdHQoeByiCbDugbt3eFL';
		// 		$merchant_id='10063345';

		// 		$params=array('merchant_email'=>$merchant_email,
		// 		             'merchant_id'=>$merchant_id,
		// 		             'secret_key'=>$secret_key);
				             
		// 		$this->load->library('paytabs',$params);
		// 		$unit_price=138.5;
		// 		$trans_header_id=5;
		// 		$shipping_first_name="shaza";
		// 		$shipping_address_1="جدة -النسيم-عمرو الاوسي";
		// 		$shipping_city="Jeddah";
		// 		$shipping_address_2="Makkah";
		// 		$shipping_email="";
		// 		$postal_code="23213";$country="SAU";
		// 		$postal_code_shipping="23213";
		// 		$country_shipping="SAU";
	 //        		$currency="SAR";
	 //        		$site_url="https://www.eshtri.net";
		// 		if($shipping_email==null || $shipping_email==""){
		// 			$shipping_email="mosleh.engineer@gmail.com";
		// 		}
		// 		$cc_phone_number="966";
		// 		$shipping_phone="531534038";
		// 		 // if($request['bil_phone']){
		//    //          $phone_number=$request['bil_phone'];
		//    //          $formattedNumber=str_replace($phone_number, "", $request['formattedNumber']);
		//    //          $formattedNumber=str_replace('+', "", $formattedNumber);
		//    //          $cc_phone_number=$formattedNumber;
		//    //      }
		// 		$postdata=array(
		//             "merchant_email" => "info@eshtri.net",
		//             'secret_key' => "ZVhfg4c0GwHn6JJqoCoU3wWQSYvgshQsd7HtcUuTqchwV0XkDlfHSbxKa31ClR5yZN2bHw57ImEDmCX0YdHQoeByiCbDugbt3eFL",
		//             'title' => "Order no=".$trans_header_id."",
		//             'cc_first_name' => "".$shipping_first_name."",
		//             'cc_last_name' => "".$shipping_first_name."",
		//             'email' => "".$shipping_email."",
		//             'cc_phone_number' => "".$cc_phone_number."",
		//             'phone_number' => "".$shipping_phone."",
		//             'billing_address' => "".$shipping_address_1."",
		//             'city' => "".$shipping_city."",
		//             'state' => "".$shipping_address_2."",
		//             'postal_code' => "".$postal_code."",
		//             'country' => "".$country."",
		//             'address_shipping' => "".$shipping_address_1."",
		//             'city_shipping' => "".$shipping_city."",
		//             'state_shipping' => "".$shipping_address_2."",
		//             'postal_code_shipping' => "".$postal_code."",
		//             'country_shipping' => "".$country."",
		//             'currency' => "SAR",
		//             "products_per_title"=> "total items: 11",
		//             "unit_price"=> "".$unit_price."",
		//             'quantity' => "1",
		//             'other_charges' => "0",
		//             'amount' => "".$unit_price."",
		//             'discount'=>"0",
		//             "msg_lang" => "arabic",
		//             "reference_no" => "".$trans_header_id."",
		//             "site_url" => "https://www.eshtri.net",
		//             'return_url' => "https://www.eshtri.net/paymentsuccessurl/transection/".$trans_header_id."",
		//             "cms_with_version" => "API USING PHP",
		//             "payment_type" => "mada"
		//         );
		//          $response= $this->paytabs->create_pay_page($postdata);
		//         // echo $response->payment_url;   
		//          $status=array();
		//          if($response->response_code == 4012){
		//              $status=array('success'=>1, 'url'=>$response->payment_url, 'result' =>$response->result, 'p_id' =>$response->p_id);
		//           }
		//           // echo "<pre>";
		//           // print_r($response);
		//           // echo "</pre>";

		// 	}
		// }

		// function paymentsuccessurl start
		function paymentsuccessurl()
		{
			$trans_header_id=$this->uri->segment(3);
			$orderDetails = $this->Transactionheader->get_one($trans_header_id);
			// payment_method paytabs start
			if($orderDetails->id && $orderDetails->payment_method=="paytabs"){
				$payment_reference=$orderDetails->payment_reference;
				//10130870
//					$merchant_email='info@eshtri.net';
//				$secret_key='ZVhfg4c0GwHn6JJqoCoU3wWQSYvgshQsd7HtcUuTqchwV0XkDlfHSbxKa31ClR5yZN2bHw57ImEDmCX0YdHQoeByiCbDugbt3eFL';
//				$merchant_id='10063345';
//
//				$params=array('merchant_email'=>$merchant_email,
//				             'merchant_id'=>$merchant_id,
//				             'secret_key'=>$secret_key,
//				             'payment_reference'=>$payment_reference);
//
//				$this->load->library('paytabs',$params);
//				$postdata=array(
//		            "merchant_email" => "info@eshtri.net",
//		            'secret_key' => "ZVhfg4c0GwHn6JJqoCoU3wWQSYvgshQsd7HtcUuTqchwV0XkDlfHSbxKa31ClR5yZN2bHw57ImEDmCX0YdHQoeByiCbDugbt3eFL",
//		            'payment_reference' => "".$payment_reference."",
//
//		        );
		
//		         $response= $this->paytabs->verify_payment($postdata);
		         // paytabs  verified stutas start
                $url="https://secure.paytabs.sa/payment/request";
                $data=array(
                    "profile_id" => 58545,
                    "tran_ref" => $payment_reference,
                );
                $postdata = json_encode($data);

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: SDJN9JL69Z-JBDDDMB9GW-JG9DWZGNKB'));
                $result = curl_exec($ch);
                curl_close($ch);

                $response=json_decode($result);

				 if($response->payment_result->response_status == "A"){
					$payment_confirm_data = array( 'payment_status'=> 1); //1=paid , 0=unpaid
					$this->Transactionheader->save($payment_confirm_data, $trans_header_id);

                    $customerDetails=$this->User->get_one_by(array('user_id' =>$orderDetails->user_id));

                    if($customerDetails->device_token){
                        $device_ids = array();
                        $device_ids[]=$customerDetails->device_token;
                        $this->send_android_fcm($device_ids, get_msg( 'online_payment_success' ), get_msg( 'site_name' ));
                        $this->send_ios_apns($customerDetails->device_token, get_msg( 'online_payment_success' ), get_msg( 'site_name' ));
                    }

						header("Location: https://www.eshtri.net/paymentverifysuccessurl");
		 	            die();
				  }else{
				      header("Location: https://www.eshtri.net/paymentunsuccessurl");
		 	        die();
				  }
		          // paytabs  verified stutas ends
			}
			
						print '<div class="container"><center><h2>ارجوك انتظر......</h2></center></div><style>.container {
              height: 100%;
              width: 100%;
              display: flex;
              position: fixed;
              align-items: center;
              justify-content: center;
            }
            div.clsbtn a {
                width: 100px;
                height: 100px;
                position: absolute;
                right: 50%;
                top: 50%;
                margin-top: 100px;
                margin-right: -50px;
                background-color: #ffffff;
                border-radius: 50px;
                opacity: 1;
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                -webkit-box-shadow: 0px 0px 30px 0px rgba(247, 149, 29, 0.5);
                -moz-box-shadow: 0px 0px 30px 0px rgba(247, 149, 29, 0.5);
                box-shadow: 0px 0px 30px 0px rgba(247, 149, 29, 0.5);
            }
            div.clsbtn a:hover > span {
                background-color: #2faee0;
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
            }
            div.clsbtn a > span.left {
                transform: rotate(45deg);
                transform-origin: center;
            }
            div.clsbtn a > span {
                background-color: #f5a700;
                display: block;
                height: 12px;
                border-radius: 6px;
                position: relative;
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                position: absolute;
                top: 50%;
                margin-top: -6px;
                left: 18px;
                width: 64px;
                display: -webkit-box;
                display: -moz-box;
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
                -webkit-justify-content: space-between;
                justify-content: space-between;
                -moz-justify-content: space-between;
                -ms-justify-content: space-between;
            }
            div.clsbtn a > span.right {
                transform: rotate(-45deg);
                transform-origin: center;
            }
            div.clsbtn a:hover > span.right .circle-left {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                margin-left: 52px;
            }
            div.clsbtn a > span.right .circle-left {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                margin-left: 0;
            }
            div.clsbtn a:hover > span span {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                background-color: #008ac9;
            }
            div.clsbtn a > span span {
                display: block;
                background-color: #ed7f00;
                width: 12px;
                height: 12px;
                border-radius: 6px;
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                position: absolute;
                left: 0;
                top: 0;
            }
            div.clsbtn a:hover > span.right .circle-right {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                margin-left: 0;
            }
            div.clsbtn a > span.right .circle-right {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                margin-left: 52px;
            }
                        </style><div class="clsbtn">
              <a href="#">
                <span class="left">
                  <span class="circle-left"></span>
                  <span class="circle-right"></span>
                </span>
                <span class="right">
                  <span class="circle-left"></span>
                  <span class="circle-right"></span>
                </span>
              </a>
            </div>';
			// payment_method paytabs end	
		}
		
		function paymentunsuccessurl()
		{
		    print '<div class="container"><center><h2>ارجوك انتظر......</h2></center></div><style>.container {
              height: 100%;
              width: 100%;
              display: flex;
              position: fixed;
              align-items: center;
              justify-content: center;
            }
            div.clsbtn a {
                width: 100px;
                height: 100px;
                position: absolute;
                right: 50%;
                top: 50%;
                margin-top: 100px;
                margin-right: -50px;
                background-color: #ffffff;
                border-radius: 50px;
                opacity: 1;
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                -webkit-box-shadow: 0px 0px 30px 0px rgba(247, 149, 29, 0.5);
                -moz-box-shadow: 0px 0px 30px 0px rgba(247, 149, 29, 0.5);
                box-shadow: 0px 0px 30px 0px rgba(247, 149, 29, 0.5);
            }
            div.clsbtn a:hover > span {
                background-color: #2faee0;
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
            }
            div.clsbtn a > span.left {
                transform: rotate(45deg);
                transform-origin: center;
            }
            div.clsbtn a > span {
                background-color: #f5a700;
                display: block;
                height: 12px;
                border-radius: 6px;
                position: relative;
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                position: absolute;
                top: 50%;
                margin-top: -6px;
                left: 18px;
                width: 64px;
                display: -webkit-box;
                display: -moz-box;
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
                -webkit-justify-content: space-between;
                justify-content: space-between;
                -moz-justify-content: space-between;
                -ms-justify-content: space-between;
            }
            div.clsbtn a > span.right {
                transform: rotate(-45deg);
                transform-origin: center;
            }
            div.clsbtn a:hover > span.right .circle-left {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                margin-left: 52px;
            }
            div.clsbtn a > span.right .circle-left {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                margin-left: 0;
            }
            div.clsbtn a:hover > span span {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                background-color: #008ac9;
            }
            div.clsbtn a > span span {
                display: block;
                background-color: #ed7f00;
                width: 12px;
                height: 12px;
                border-radius: 6px;
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                position: absolute;
                left: 0;
                top: 0;
            }
            div.clsbtn a:hover > span.right .circle-right {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                margin-left: 0;
            }
            div.clsbtn a > span.right .circle-right {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                margin-left: 52px;
            }
                        </style><div class="clsbtn">
              <a href="#">
                <span class="left">
                  <span class="circle-left"></span>
                  <span class="circle-right"></span>
                </span>
                <span class="right">
                  <span class="circle-left"></span>
                  <span class="circle-right"></span>
                </span>
              </a>
            </div>';
		}
		function paymentverifysuccessurl()
		{
		    print '<div class="container"><center><h2>ارجوك انتظر......</h2></center></div><style>.container {
              height: 100%;
              width: 100%;
              display: flex;
              position: fixed;
              align-items: center;
              justify-content: center;
            }
            div.clsbtn a {
                width: 100px;
                height: 100px;
                position: absolute;
                right: 50%;
                top: 50%;
                margin-top: 100px;
                margin-right: -50px;
                background-color: #ffffff;
                border-radius: 50px;
                opacity: 1;
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                -webkit-box-shadow: 0px 0px 30px 0px rgba(247, 149, 29, 0.5);
                -moz-box-shadow: 0px 0px 30px 0px rgba(247, 149, 29, 0.5);
                box-shadow: 0px 0px 30px 0px rgba(247, 149, 29, 0.5);
            }
            div.clsbtn a:hover > span {
                background-color: #2faee0;
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
            }
            div.clsbtn a > span.left {
                transform: rotate(45deg);
                transform-origin: center;
            }
            div.clsbtn a > span {
                background-color: #f5a700;
                display: block;
                height: 12px;
                border-radius: 6px;
                position: relative;
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                position: absolute;
                top: 50%;
                margin-top: -6px;
                left: 18px;
                width: 64px;
                display: -webkit-box;
                display: -moz-box;
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
                -webkit-justify-content: space-between;
                justify-content: space-between;
                -moz-justify-content: space-between;
                -ms-justify-content: space-between;
            }
            div.clsbtn a > span.right {
                transform: rotate(-45deg);
                transform-origin: center;
            }
            div.clsbtn a:hover > span.right .circle-left {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                margin-left: 52px;
            }
            div.clsbtn a > span.right .circle-left {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                margin-left: 0;
            }
            div.clsbtn a:hover > span span {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                background-color: #008ac9;
            }
            div.clsbtn a > span span {
                display: block;
                background-color: #ed7f00;
                width: 12px;
                height: 12px;
                border-radius: 6px;
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                position: absolute;
                left: 0;
                top: 0;
            }
            div.clsbtn a:hover > span.right .circle-right {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                margin-left: 0;
            }
            div.clsbtn a > span.right .circle-right {
                transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1);
                margin-left: 52px;
            }
                        </style><div class="clsbtn">
              <a href="#">
                <span class="left">
                  <span class="circle-left"></span>
                  <span class="circle-right"></span>
                </span>
                <span class="right">
                  <span class="circle-left"></span>
                  <span class="circle-right"></span>
                </span>
              </a>
            </div>';
		}

    function send_android_fcm( $registatoin_ids, $message, $title)
    {
        $n = array(
            "body"  => $message,
            "title" => $title,
            "text"  => "Click me to open an Activity!",
            "sound" => "warning"
        );
        $message = array
        (
            'message'   => $message,
            'title'     => $title,
            'subtitle'  => 'This is a subtitle. subtitle',
            'tickerText'    => 'Ticker text here...Ticker text here...Ticker text here',
            'vibrate'   => 1,
            'largeIcon' => 'large_icon',
            'smallIcon' => 'small_icon'
        );
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            /*'to'             => $tokens,*/
            'registration_ids' => $registatoin_ids,
            'priority'     => "high",
            'notification' => $n,
            'data'         => $message
        );

        //var_dump($fields);
//        $api2='AIzaSyAjp5OfeOhMfejWHUjy0TVtdwhTlc_NYxs';
        $api1='AAAA0IN9-xk:APA91bEmxmQNs6_8FEEPZSmnI9j0Wz7p4piEoyvuxZDMCLRDGhLXsaCAea9uJcbtOz6_XsvYvAr5rkPq374vHXwhb_JebzbIDw-Z9TGbVthNcgSFykG1xNLVhgz989MpDtBF3ne12KoN';
        $headers = array(
            'Authorization:key = '.$api1.'',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
            //  curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }
    function send_ios_apns($tokenId, $message, $title)
    {
        ini_set('display_errors','On');
        //error_reporting(E_ALL);
        // Change 1 : No braces and no spaces
        $deviceToken= $tokenId;
        //'fe2df8f5200b3eb133d84f73cc3ea4b9065b420f476d53ad214472359dfa3e70';
        // Change 2 : If any
        $passphrase = 'teamps';
        $ctx = stream_context_create();
        // Change 3 : APNS Cert File name and location.
        stream_context_set_option($ctx, 'ssl', 'local_cert', realpath('assets').'/apns/psnews_apns_cert.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        // Open a connection to the APNS server
        $fp = stream_socket_client(
            'ssl://gateway.sandbox.push.apple.com:2195', $err,
            $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        // Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default'
        );
        // Encode the payload as JSON
        $payload = json_encode($body);
        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
        // Close the connection to the server
        fclose($fp);
        if (!$result)
            return false;

        return true;
    }
		// function paymentsuccessurl end


		// function paytabstest()
		// {
			
		// 	header("Location: https://www.eshtri.net/eshtri/paymentsuccessurl/transection/5");
		// 	die();
		// }
	function deliveryboyterms(){
		$q = $this->db->query("select * from `pageapp` where id=3"); 


		$data["responce"] = true;     
		$data['data'] = $q->result();


		echo json_encode($data,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);  
	}

	function settings()
	{
		header('Content-Type: application/json');


		//$checkuser=$this->User->get_one_by( array( 'user_phone' => '966531534038'));
		$settings='{"success":true,"data":{"app_name":"\u0645\u0642\u0635\u0648\u0631\u0629","enable_stripe":"0","default_tax":"5","default_currency":"SAR","enable_paypal":"0","main_color":"#15255a","main_dark_color":"#15255a","second_color":"#043832","second_dark_color":"#ccccdd","accent_color":"#8c98a8","accent_dark_color":"#9999aa","scaffold_dark_color":"#2c2c2c","scaffold_color":"#fafafa","google_maps_key":"AIzaSyAT07iMlfZ9bJt1gmGj9KhJDLFY8srI6dA","mobile_language":"ar","app_version":"1.0.0","enable_version":"1","currency_right":"1","distance_unit":"km"},"message":"Settings retrieved successfully"}';
	//echo json_encode($settings,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);  
		// $prd_conds['user_id'] = $checkuser->user_id;
		// $prd_conds['is_default'] = 1;
		// $default_address = $this->UserAddress->get_one_by( $prd_conds );

		//$checkuser->default_address = $default_address;
	//echo json_encode($checkuser,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);  
		echo $settings;
	}    

	function userAllDeliveryAddress()
	{
		header('Content-Type: application/json');
		$user_id=$this->input->get( 'user_id');
		$conds = array(
			"user_id" => $user_id
		);
		$data = $this->UserAddress->get_all_by($conds)->result();
		print json_encode(array(
			"data" => $data
		));
	}

    function shareordertoconfirm()
    {
        // AAAAdnoaKhs:APA91bF31-_EfboVJfZwJnAAZ2UUxosDYkRWPsbet6FpzkD8O6bok51020h5XzFZXlAdSGmMnO5RzWju1Lc_HOSPOqJsaHaLrISRtESFv_xdojm_GlVtqapEp7NxV2Uc7-x4KcZ4d6mu
        $order_id=$this->input->get( 'order_id');
        $mobile_no=$this->input->get( 'mobile_no');
        $lang=$this->input->get( 'lang');
        $this->load->library('session');
        $language=$this->session->userdata('site_lang');
        $lang_key="ar";
        if($lang=="en"){
            $lang_key="en";
        }

        if($lang_key){
            $condsd['symbol'] = $lang_key;
            $language = $this->Language->get_one_by($condsd);
            if($language->id !=null){
                $this->session->set_userdata('language_code', $language->symbol);
                $this->session->set_userdata( 'user_language_id', $language->id );
            }
        }

        $conds['id'] = $order_id;
        // get rows count
        $this->data['rows_count'] = $this->Transactionheader->count_all_by($conds);

        // get users
        $transaction = $this->Transactionheader->get_one( $order_id );
        if($transaction->id && $transaction->trans_status_id == 1 && $transaction->contact_phone == $mobile_no){
            $this->data['transaction'] = $transaction;
        }else{
            redirect(site_url('/'));
        }
        // load index logic
        $this->load_old_template('orders/orderconfirmbycustomer', $this->data, true );
    }

    function deleteordertoconfirm()
    {
//        $token="AAAAdnoaKhs:APA91bF31-_EfboVJfZwJnAAZ2UUxosDYkRWPsbet6FpzkD8O6bok51020h5XzFZXlAdSGmMnO5RzWju1Lc_HOSPOqJsaHaLrISRtESFv_xdojm_GlVtqapEp7NxV2Uc7-x4KcZ4d6mu";
        $order_id=$this->input->get( 'order_id');
        $order_details_id=$this->input->get( 'order_details_id');
//        $token_id=$this->input->get( 'token_id');
        $isincreaseQty=$this->input->get( 'increaseQtyData');
        if($order_id && $order_details_id){
            $transaction = $this->Transactionheader->get_one( $order_id );
            if($transaction->id){
                $orderproduct=$this->Transactiondetail->get_one_by(array("transactions_header_id"=>$order_id, "id"=>$order_details_id));
                if($orderproduct->id){

                    if($isincreaseQty){
                        $qty=$this->input->get( 'qty');
                        if($qty>0) {
                            $dataDtls = array(
                                'qty' => $qty,
                            );
                            $this->Transactiondetail->save($dataDtls, $order_details_id);
                        }
                    }else{
                        $rows_count = $this->Transactiondetail->count_all_by(array("transactions_header_id"=>$order_id, "stock_status"=>1));
                        if($rows_count>1){
                            $this->Transactiondetail->delete_all_records(array("transactions_header_id"=>$order_id, "id"=>$order_details_id));
                        }

                    }


                    $totalItem = 0;
                    $totalItemAmountNoTax = 0.0;
                    $sub_total_amount = 0.0;
                    $discount_total_amount = 0.0;
                    $originalprice_total_amount = 0.0;
                    $tax_amount = 0.0;
                    $orderdetails=$this->Transactiondetail->get_all_by(array("transactions_header_id"=>$order_id));
                    foreach($orderdetails->result() as $sinleDetails) {
                        if($sinleDetails->id){
                            $qtyDtls= $sinleDetails->qty;
                            $originalPriceDtls = $sinleDetails->original_price;
                            $discountPercentDtls = $sinleDetails->discount_percent;
                            $price=$originalPriceDtls;
                            $discount_amount=0.0;
                            if($discountPercentDtls>0){
                                $discount_amount=(($sinleDetails->discount_percent / 100) * $originalPriceDtls);
                                $discountPriceDtls = ($originalPriceDtls - (($sinleDetails->discount_percent / 100) * $originalPriceDtls));
                                $price=$discountPriceDtls;
                            }

                            $stockDtls = $sinleDetails->stock_status;
                            $itemSubtotal=$originalPriceDtls * $qtyDtls;
                            if($stockDtls=='1'){
                                $totalItem += $qtyDtls;
                                $totalItemAmountNoTax += $itemSubtotal;

                                $itemDiscountAmount=$discount_amount * $qtyDtls;
                                $discount_total_amount += $itemDiscountAmount;
                                $sub_total_amount +=$price * $qtyDtls;
                                $itemOriginalAmount=$sinleDetails->original_price * $qtyDtls;
                                $originalprice_total_amount += $itemOriginalAmount;
                            }
                        }
                    }


                    $total_item_amount=$sub_total_amount;
                    if($transaction->coupon_discount_amount >0){
                        $total_item_amount=$total_item_amount - $transaction->coupon_discount_amount;
                    }

                    $tax_amount=(($transaction->tax_percent / 100) * $total_item_amount);
                    $master_data = array(
                        'total_item_count'=> $totalItem,
                        'sub_total_amount'=> $sub_total_amount, // after discount
                        'discount_amount'=> $discount_total_amount,
                        'coupon_discount_amount'=> $transaction->coupon_discount_amount,
                        'tax_amount'=> $tax_amount,
                        'shipping_method_amount'=> $transaction->shipping_method_amount,
                        'total_item_amount'=> $originalprice_total_amount, // without discount
                        'balance_amount'=> $total_item_amount + $transaction->shipping_method_amount + $tax_amount-$transaction->coupon_discount_amount,
                    );

                    // save data
                    $this->Transactionheader->save( $master_data, $transaction->id);


                }

            }

        }
        if($isincreaseQty){
            $transaction = $this->Transactionheader->get_one( $order_id );
            $arr = array('isError' => false, 'message' =>  "updated", 'data' =>   $transaction);
            echo json_encode( $arr );
            exit;
        }else{
            redirect(site_url('shareordertoconfirm?order_id='.$transaction->id.'&mobile_no='.$transaction->contact_phone));
        }

    }

    function confirmorderbycustomer(){
        $postdata=$this->input->post();
        $order_id=$postdata["transactions_header_id"];
        $payment_method=$postdata["trans_payment_method"];
        if($order_id && $payment_method) {
            $master_data = array(
                'payment_method' => $payment_method,
                'trans_status_id' => 2,
            );
            // save data
            $this->Transactionheader->save($master_data, $order_id);

            // paytabs starts
            $status=array();
            if($payment_method=="paytabs"){
                $postal_code="23213";$country="SAU";$postal_code_shipping="23213";$country_shipping="SAU";
                $products_per_title="Property Title";$currency="SAR";$site_url="https://www.eshtri.net";
                $return_url = "https://www.eshtri.net/paymentsuccessurl";$cms_with_version="API USING PHP";
                $shipping_address_2="Riyadh";

                $merchant_email='info@eshtri.net';
                $secret_key='ZVhfg4c0GwHn6JJqoCoU3wWQSYvgshQsd7HtcUuTqchwV0XkDlfHSbxKa31ClR5yZN2bHw57ImEDmCX0YdHQoeByiCbDugbt3eFL';
                $merchant_id='10063345';

                $params=array('merchant_email'=>$merchant_email,
                    'merchant_id'=>$merchant_id,
                    'secret_key'=>$secret_key,
                    'payment_reference'=>'');

                $this->load->library('paytabs',$params);
                $transaction = $this->Transactionheader->get_one( $order_id );
                $unit_price=$transaction->balance_amount;
                $shipping_email=$transaction->shipping_email;
                $shipping_first_name=$transaction->shipping_first_name;
                $shipping_phone=$transaction->shipping_phone;
                $shipping_address_1=$transaction->shipping_address_1;
                $shipping_address_2=$transaction->shipping_address_2;
                $shipping_city=$transaction->shipping_city;
                $totalItem=$transaction->total_item_count;
                if($shipping_email==null || $shipping_email==""){
                    $shipping_email="sales@eshtri.net";
                }
                if($shipping_first_name==null || $shipping_first_name==""){
                    $shipping_first_name="customer name";
                }
                if($shipping_first_name==null || $shipping_first_name==""){
                    $shipping_first_name="customer name";
                }
                $cc_phone_number="966";
                if(strlen($shipping_phone) >9){
                    $formattedNumber=str_replace("966", "", $shipping_phone);
                    $formattedNumber=str_replace('+', "", $formattedNumber);
                    $shipping_phone=intval($formattedNumber);
                }else if(strlen($shipping_phone) <9){
                    $shipping_phone="536176364";
                }
                if($shipping_address_1==null || $shipping_address_1==""){
                    $shipping_address_1="customer address";
                }
                if($shipping_address_2==null || $shipping_address_2==""){
                    $shipping_address_2="customer state";
                }
                if($shipping_city==null || $shipping_city==""){
                    $shipping_city="customer city";
                }
                $url="https://secure.paytabs.sa/payment/request";
                $data=array(
                    "profile_id" => 58545,
                    "tran_type" => "sale",
                    "tran_class" => "ecom",
                    "cart_description" => "Order nnumber-".$order_id."",
                    "cart_id" => "Order-".$order_id."",
                    "cart_currency" => "SAR",
                    "cart_amount" => $unit_price,
                    "callback" => "https://www.eshtri.net",
                    "return" => "https://www.eshtri.net/paymentsuccessurl/transection/".$order_id."",
                    "customer_details" => array(
                        "name"=>"".$shipping_first_name."",
                        "email"=>"".$shipping_email."",
                        "phone_number"=>"966".$shipping_phone."",
                        "street1"=>"".$shipping_address_1."",
                        "city"=>"".$shipping_city."",
                        "state"=>"".$shipping_address_2."",
                        "country"=>"".$country."",
                        "ip"=>"127.0.0.1"

                    ),
                );
                $postdata = json_encode($data);

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: SDJN9JL69Z-JBDDDMB9GW-JG9DWZGNKB'));
                $result = curl_exec($ch);
                curl_close($ch);

                $response=json_decode($result);
//                $postdata=array(
//                    "merchant_email" => "info@eshtri.net",
//                    'secret_key' => "ZVhfg4c0GwHn6JJqoCoU3wWQSYvgshQsd7HtcUuTqchwV0XkDlfHSbxKa31ClR5yZN2bHw57ImEDmCX0YdHQoeByiCbDugbt3eFL",
//                    'title' => "Order no=".$order_id."",
//                    'cc_first_name' => "".$shipping_first_name."",
//                    'cc_last_name' => "".$shipping_first_name."",
//                    'email' => "".$shipping_email."",
//                    'cc_phone_number' => "".$cc_phone_number."",
//                    'phone_number' => "".$shipping_phone."",
//                    'billing_address' => "".$shipping_address_1."",
//                    'city' => "".$shipping_city."",
//                    'state' => "".$shipping_address_2."",
//                    'postal_code' => "".$postal_code."",
//                    'country' => "".$country."",
//                    'address_shipping' => "".$shipping_address_1."",
//                    'city_shipping' => "".$shipping_city."",
//                    'state_shipping' => "".$shipping_address_2."",
//                    'postal_code_shipping' => "".$postal_code."",
//                    'country_shipping' => "".$country."",
//                    'currency' => "SAR",
//                    "products_per_title"=> $totalItem,
//                    "unit_price"=> "".$unit_price."",
//                    'quantity' => "1",
//                    'other_charges' => "0",
//                    'amount' => "".$unit_price."",
//                    'discount'=>"0",
//                    "msg_lang" => "arabic",
//                    "reference_no" => "".$order_id."",
//                    "site_url" => "https://www.eshtri.net",
//                    'return_url' => "https://www.eshtri.net/paymentsuccessurl/transection/".$order_id."",
//                    "cms_with_version" => "API USING PHP",
//                    //"payment_type" => "mada"
//                );
//
//                $response= $this->paytabs->create_pay_page($postdata);
                // echo $response->payment_url;

                if($response->tran_ref){
                   // $status=array('success'=>1, 'url'=>$response->payment_url, 'result' =>$response->result, 'p_id' =>$response->p_id);
                    $payment_data['payment_reference']=$response->tran_ref;
                    // save data
                    $this->Transactionheader->save($payment_data, $order_id);
                }

            }
            $trans_header_obj = $this->Transactionheader->get_one($order_id);
            if($payment_method=="paytabs"){
                if($response->tran_ref){
                    $trans_header_obj->payment_url=$response->redirect_url;
                    $trans_header_obj->p_id=$response->tran_ref;
                    redirect($response->redirect_url);
                }else{
                    redirect(site_url('/'));
                }

            }else{
                $trans_header_obj->payment_url=$site_url;

            }
            redirect(site_url('/'));
            // Paytabs ends
            // $arr = array('isError' => false, 'message' =>  "updated", 'orderdata' =>   $trans_header_obj);
            // echo json_encode( $arr );
            // exit;

        }

        redirect(site_url('/'));
    }
//    function editordersubmit() {
//
//        $id = $this->input->post('transactions_header_id');
//        $rules = array(
//            array(
//                'field' => 'transactions_header_id',
//                'label' => get_msg('transactions_header_id'),
//                'rules' => 'required'
//            )
//
//        );
//
//        $this->form_validation->set_data($this->input->post());
//        $this->form_validation->set_error_delimiters('', '<br/>');
//        $this->form_validation->set_rules( $rules );
//
//        if ( $this->form_validation->run() == FALSE ) {
//            $arr = array('isError' => true, 'message' =>  validation_errors());
//            echo json_encode( $arr );
//            exit;
//        }
//
//        $totalItem = 0;
//        $totalItemAmountNoTax = 0.0;
//        $sub_total_amount = 0.0;
//        $discount_total_amount = 0.0;
//        $originalprice_total_amount = 0.0;
//        $tax_amount = 0.0;
//        foreach($this->input->post('idDtls') as $key =>$sinleDetails) {
//            if($this->input->post('idDtls')[$key]){
//                $qtyDtls= $this->input->post('qtyDtls')[$key];
//                $originalPriceDtls = $this->input->post('originalPriceDtls')[$key];
//                $discountPercentDtls = $this->input->post('discountPercentDtls')[$key];
//                $price=$originalPriceDtls;
//                $discount_amount=0.0;
//                if($discountPercentDtls>0){
//                    $discount_amount=(($this->input->post('discountPercentDtls')[$key] / 100) * $originalPriceDtls);
//                    $discountPriceDtls = ($originalPriceDtls - (($this->input->post('discountPercentDtls')[$key] / 100) * $originalPriceDtls));
//                    $price=$discountPriceDtls;
//                }
//
//                $stockDtls = $this->input->post('stockDtls')[$key];
//                $itemSubtotal=$originalPriceDtls * $qtyDtls;
//                if($stockDtls=='1'){
//                    $totalItem += $qtyDtls;
//                    $totalItemAmountNoTax += $itemSubtotal;
//
//                    $itemDiscountAmount=$discount_amount * $qtyDtls;
//                    $discount_total_amount += $itemDiscountAmount;
//                    $sub_total_amount +=$price * $qtyDtls;
//                    $itemOriginalAmount=$this->input->post('originalPriceDtls')[$key] * $qtyDtls;
//                    $originalprice_total_amount += $itemOriginalAmount;
//                }
//                $dataDtls = array(
//                    'original_price' => $this->input->post('originalPriceDtls')[$key],
//                    'price' => $price,
//                    'discount_amount' => $discount_amount,
//                    'discount_percent' => $discountPercentDtls,
//                    'discount_value' => $discountPercentDtls /100,
//                    'qty' => $qtyDtls,
//                    'stock_status' => ''.$stockDtls.'',
//                );
//                $this->Transactiondetail->save($dataDtls, $this->input->post('idDtls')[$key]);
//            }
//        }
//
//
//        $total_item_amount=$sub_total_amount;
//        if($this->input->post('coupon_discount_amount') >0){
//            $total_item_amount=$total_item_amount - $this->input->post('coupon_discount_amount');
//        }
//
//        $tax_amount=(($this->input->post('tax_percent') / 100) * $total_item_amount);
//        $master_data = array(
//            'total_item_count'=> $totalItem,
//            'sub_total_amount'=> $sub_total_amount, // after discount
//            'discount_amount'=> $discount_total_amount,
//            'coupon_discount_amount'=> $this->input->post('coupon_discount_amount'),
//            'tax_amount'=> $tax_amount,
//            'shipping_method_amount'=> $this->input->post('shipping_method_amount'),
//            'total_item_amount'=> $originalprice_total_amount, // without discount
//            'balance_amount'=> $total_item_amount + $this->input->post('shipping_method_amount') + $tax_amount-$this->input->post('coupon_discount_amount'),
//        );
//
//        // save data
//        $this->Transactionheader->save( $master_data, $id );
//        redirect(site_url('admin/orders/orderedit/'.$id.''));
//
//    }
}
