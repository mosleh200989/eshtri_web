<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PanaceaSoft Authentication
 */
class PS_Adapter {

	// codeigniter instance
	protected $CI;

	// login user
	protected $login_user_id;

	/**
	 * Constructor
	 */
	function __construct()
	{
		// get CI instance
		$this->CI =& get_instance();
	}

	/**
	 * Sets the login user.
	 */
	function set_login_user_id( $user_id )
	{
		$this->login_user_id = $user_id;
	}

	/**
	 * Send Firebase Notification.
	 */
    function send_android_fcm($receiverToken, $title, $message, $screen, $order_id, $chat_with)
     {
        //Google cloud messaging GCM-API url
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'to' => $receiverToken,
            'collapse_key' => 'type_a',
            'priority' => 'high',
            'notification' => array('title' => $title,'body' => $message),
            'data' => array(
                'title' => $title,
                'body' => $message,
                'sound' => 'default',
                'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
                'screen' => $screen,
                'order_id' => $order_id,
                'chat_with' => $chat_with,
            ),
        );

        $headers = array(
            'Authorization: key=AAAAYsFkyRw:APA91bGrKHArC-U1PK-rR4iVteLz7xQd2UBcX_1-Ris1HF_uTQDberY1NdXvgoLz63WDW8HswJs7ERNMCkQRc8j19T2BXrQMwE35tmVwA0yRAVECHiZYF2QB0LFpqIOCnMANeko0qQYi',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);

        return $result;
     }


	/**
	 * Sets the login user.
	 */
	function get_login_user_id()
	{
		return $this->login_user_id;
	}

    /**
     * get default caption/name via language.
     */
    function getDefaultCaption($caption, $captionAlt) {
        if (!$caption && !$captionAlt) {
            return '';
        }
        return ($this->CI->session->userdata('language_code') == "en") ? ($captionAlt ? $captionAlt : $caption) : ($caption ? $caption : $captionAlt);
    }
	/**
	 * Gets the default photo.
	 *
	 * @param      <type>  $id     The identifier
	 * @param      <type>  $type   The type
	 */
	function get_default_photo( $id, $type )
	{
		$default_photo = "";

		// get all images
		$img = $this->CI->Image->get_all_by( array( 'img_parent_id' => $id, 'img_type' => $type ))->result();

		if ( count( $img ) > 0 ) {
		// if there are images for wallpaper,
			
			$default_photo = $img[0];
		} else {
		// if no image, return empty object

			$default_photo = $this->CI->Image->get_empty_object();
		}

		return $default_photo;
	}

	/**
	 * Gets the default photo.
	 *
	 * @param      <type>  $id     The identifier
	 * @param      <type>  $type   The type
	 */
	function get_default_photo_for_gallery( $id, $type )
	{
		$default_photo = "";
		$conds['img_parent_id'] = $id;
		$conds['img_type'] = $type;

		// get all images
		$img = $this->CI->Image->get_all_by($conds)->result();
		
		if ( count( $img ) == 1 ) {
			// if there are images for gallery,
			$default_photo = $img[0];
			
		} elseif ( count( $img ) > 1 ) {
			$conds['is_default'] = "1";
			$image = $this->CI->Image->get_all_by($conds)->result();
			// if there are images for gallery,
			if(count($image) != 0) {
				$default_photo = $image[0];
			} else {
				$default_photo = $img[0];
			}

		} else {
			// if no image, return empty object
			$default_photo = $this->CI->Image->get_empty_object();
		}

		return $default_photo;
	}


	/**
	 * Customize category object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_category( &$obj )
	{
        $obj->name = $this->getDefaultCaption($obj->name, $obj->name_alt);
		// added_date timestamp string
		$obj->added_date_str = ago( $obj->added_date );

		// set default photo
		$obj->default_photo = $this->get_default_photo( $obj->id, 'category' );

		// set default icon 
		$obj->default_icon = $this->get_default_photo( $obj->id, 'category-icon' );
	}

	/**
	 * Customize sub category object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_sub_category( &$obj )
	{
        $obj->name = $this->getDefaultCaption($obj->name, $obj->name_alt);
		// added_date timestamp string
		$obj->added_date_str = ago( $obj->added_date );
		
		// set default photo
		$obj->default_photo = $this->get_default_photo( $obj->id, 'sub_category' );

		// set default icon 
		$obj->default_icon = $this->get_default_photo( $obj->id, 'subcat_icon' );
	}

	/**
	 * Customize product object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_product( &$obj , $need_return = false)
	{


        $obj->name = $this->getDefaultCaption($obj->name, $obj->name_alt);
        $obj->name_alt = $this->CI->session->userdata('language_code');

//        $querySQL="SELECT mk_shops.name as shop_name, mk_shops.comissionpln as comission_plan, mk_shops.currency_symbol as currency_symbol, mk_shops.currency_short_form as currency_short_form, acc_comission_seller.amount_percentage as comission_rate FROM `acc_comission_seller` INNER JOIN mk_shops ON acc_comission_seller.shop_id=mk_shops.id WHERE mk_shops.id='".$obj->shop_id."'";
//        $resultSet = $this->CI->Acc_period->queryPrepare($querySQL);
//        $comissionInfo=$resultSet->row();
        //$obj->shop_name=$comissionInfo->shop_name;
        $obj->comission_plan="comissionIncldNot";
        $obj->comission_rate="20";

        //$obj->currency_symbol = $comissionInfo->currency_symbol;

        //$obj->currency_short_form = $comissionInfo->currency_short_form;
        $percentage=0;
        if($obj->commission_plan){
            $querySQL="SELECT id, amount_percentage, shop_id FROM `acc_comission_seller` WHERE shop_id='".$obj->shop_id."' AND id=$obj->commission_plan";
            $resultSet = $this->CI->Acc_period->queryPrepare($querySQL);
            $comission_info=$resultSet->row();

            $num_unit_price = $obj->unit_price;
            $num_original_price = $obj->original_price;


            if($comission_info->amount_percentage && $comission_info->amount_percentage>0){
                $percentage = $comission_info->amount_percentage;
                $num_unit_price += $num_unit_price * ($percentage / 100);
                $num_unit_price = round($num_unit_price, 1);
                $num_unit_price = sprintf('%0.2f', $num_unit_price);
                $obj->unit_price=$num_unit_price;


                $num_original_price += $num_original_price * ($percentage / 100);
                $num_original_price = round($num_original_price, 1);
                $num_original_price = sprintf('%0.2f', $num_original_price);
                $obj->original_price=$num_original_price;
            }
        }
		//Transaction Status
		if(isset($obj->trans_status)) {

			if($obj->trans_status != "") {
				$obj->trans_status = $obj->trans_status;
			} else {
				$obj->trans_status = "";
			}

		} else {
			$obj->trans_status = "";
		}
		

		// set default photo
		$obj->default_photo = $this->get_default_photo_for_gallery( $obj->id, 'product' );

		// set default photo
		$condss['img_parent_id'] = $obj->id;
		$condss['img_type'] = 'product';

		// get all images
		$img = $this->CI->Image->get_all_by($condss)->result();
		$obj->all_photo = $img;
		// category object
		if ( isset( $obj->cat_id )) {
			$tmp_category = $this->CI->Category->get_one( $obj->cat_id );

			$this->convert_category( $tmp_category );

			$obj->category = $tmp_category;
		}

		// Sub Category Object
		if ( isset( $obj->sub_cat_id )) {
			$tmp_sub_category = $this->CI->Subcategory->get_one( $obj->sub_cat_id );

			$this->convert_sub_category( $tmp_sub_category );

			$obj->sub_category = $tmp_sub_category;
		}

		$conds['product_id'] = $obj->id;
		// Colors Object 
		$color_count = $this->CI->Color->count_all_by( $conds );

		if ( $color_count > 0 ) {
			$tmp_colors = $this->CI->Color->get_all_by( $conds )->result();
			$obj->colors = $tmp_colors;
		} else {
			$color_dummy[] = $this->CI->Color->get_empty_object();
			$obj->colors = $color_dummy;
		}

		// Spec Object 
		$spec_count = $this->CI->Specification->count_all_by( $conds );

		

		if ( $spec_count > 0 ) {
			$tmp_spec = $this->CI->Specification->get_all_by( $conds )->result();
			$obj->specs = $tmp_spec;
		} else {
			$spec_dummy[] = $this->CI->Specification->get_empty_object();
			$obj->specs = $spec_dummy;
		}

		//Need to check for Like and Favourite
		$obj->is_liked = 0;
		$obj->is_favourited = 0;
		
		if($this->get_login_user_id() != "") {
			//Need to check for Fav
			$conds['product_id'] = $obj->id;
			$conds['user_id']    = $this->get_login_user_id();
		
			// checking for like product by user
			$like_id = $this->CI->Like->get_one_by($conds)->id;
			$obj->is_liked = 0;
			if($like_id != "") {
				$obj->is_liked = 1;
			} else {
				$obj->is_liked = 0;
			}


			$fav_id = $this->CI->Favourite->get_one_by($conds)->id;
			$obj->is_favourited = 0;
			if($fav_id != "") {
				$obj->is_favourited = 1;
			} else {
				$obj->is_favourited = 0;
			}

		} else if($obj->login_user_id_post != "") {
			$conds['product_id'] = $obj->id;
			$conds['user_id']    = $obj->login_user_id_post;

			// checking for like product by user
			$like_id = $this->CI->Like->get_one_by($conds)->id;
			$obj->is_liked = 0;
			if($like_id != "") {
				$obj->is_liked = 1;
			} else {
				$obj->is_liked = 0;
			}

			$fav_id = $this->CI->Favourite->get_one_by($conds)->id;
			$obj->is_favourited = 0;
			if($fav_id != "") {
				$obj->is_favourited = 1;
			} else {
				$obj->is_favourited = 0;
			}

		}

		
		unset($obj->login_user_id_post);

		$obj->is_liked = $obj->is_liked;
		$obj->is_favourited = $obj->is_favourited;

		// like count
	    $obj->like_count = $this->CI->Like->count_all_by(array("product_id" => $obj->id));

	    // fav count
		//$obj->favourite_count =  $this->CI->Favourite->count_all_by(array("product_id" => $obj->id));

	    // image count 
		$obj->image_count =  $this->CI->Image->count_all_by(array("img_parent_id" => $obj->id));

		// touch count
		//$obj->touch_count =  $this->CI->Touch->count_all_by(array("type_id" => $obj->id, "type_name" => "product"));

		// Comment count
		$obj->comment_header_count =  $this->CI->Commentheader->count_all_by(array("product_id" => $obj->id));

		$shopinf=$this->CI->Shop->get_one( $obj->shop_id );
        $obj->currency_symbol = $shopinf->currency_symbol;

        $obj->currency_short_form = $shopinf->currency_short_form;
        $obj->shop_name=$shopinf->shop_name;
		//Discount Checking 
		$conds['product_id'] = $obj->id;
		$discount_id = $this->CI->ProductDiscount->get_one_by( $conds )->discount_id;

		if($discount_id != "") { 
			$discount_percent = $this->CI->Discount->get_one( $discount_id )->percent;

			$obj->discount_amount = $obj->original_price * $discount_percent;

			$obj->discount_percent = $discount_percent * 100;

			$obj->discount_value = $discount_percent;


		} else {


			$obj->discount_amount = 0;

			$obj->discount_percent = 0;

			$obj->discount_value = 0;

		}

		// Attribute Object
		// Get Header Object First
		$att_header_count = $this->CI->Attribute->count_all_by( $conds );

		if ( $att_header_count > 0 ) {
			for($i = 0; $i < $att_header_count; $i++) {
				//Need to check for that header got details or not
				$tmp_header = $this->CI->Attribute->get_all_by( $conds )->result();
				$att_conds['header_id'] = $tmp_header[$i]->id;
				$att_header_count_from_detail = $this->CI->Attributedetail->count_all_by( $att_conds );

				if( $att_header_count_from_detail > 0 ) {
					//if got details, need to put those details data at one header

					$tmp_detail = $this->CI->Attributedetail->get_all_by( $att_conds )->result();
					$obj->attributes_header[$i] = $tmp_header[$i];

                    if($obj->commission_plan && $percentage >0){
                        $alldetails=array();
                        foreach ($tmp_detail as $singleattri){
                            $additional_price=$singleattri->additional_price;
                            $additional_price += $additional_price * ($percentage / 100);
                            $additional_price = round($additional_price, 1);
                            $additional_price = sprintf('%0.2f', $additional_price);
                            $singleattri->additional_price=$additional_price;
                            $alldetails[]=$singleattri;
                        }
                        $obj->attributes_header[$i]->attributes_detail = $alldetails;
                    }else{
                        $obj->attributes_header[$i]->attributes_detail = $tmp_detail;
                    }


				} else {
					$att_detail_dummy[] = $this->CI->Attributedetail->get_empty_object();

					$obj->attributes_header[$i] = $tmp_header[$i];
					$obj->attributes_header[$i]->attributes_detail = $att_detail_dummy;
				}
			}

		} else {
			$header_dummy[] = $this->CI->Attribute->get_empty_object();
			$obj->attributes_header = $header_dummy;

			$att_detail_dummy[] = $this->CI->Attributedetail->get_empty_object();
			$obj->attributes_header[0]->attributes_detail = $att_detail_dummy;
		}

		//rating details 
		
		// $obj->like_count = $this->CI->Like->count_all_by(array("product_id" => $obj->id));

		
		$total_rating_count = 0;
		$total_rating_value = 0;

		$five_star_count = 0;
		$five_star_percent = 0;

		$four_star_count = 0;
		$four_star_percent = 0;

		$three_star_count = 0;
		$three_star_percent = 0;

		$two_star_count = 0;
		$two_star_percent = 0;

		$one_star_count = 0;
		$one_star_percent = 0;


		

		//Rating Total how much ratings for this product
		$conds_rating['product_id'] = $obj->id;
		$total_rating_count = $this->CI->Rate->count_all_by($conds_rating);
		$sum_rating_value = $this->CI->Rate->sum_all_by($conds_rating)->result()[0]->rating;

		//Rating Value such as 3.5, 4.3 and etc
		if($total_rating_count > 0) {
			$total_rating_value = number_format((float) ($sum_rating_value  / $total_rating_count), 1, '.', '');
		} else {
			$total_rating_value = 0;
		}

		//For 5 Stars rating

		$conds_five_star_rating['rating'] = 5;
		$conds_five_star_rating['product_id'] = $obj->id;
		$five_star_count = $this->CI->Rate->count_all_by($conds_five_star_rating);
		if($total_rating_count > 0) {
			$five_star_percent = number_format((float) ((100 / $total_rating_count) * $five_star_count), 1, '.', '');
		} else {
			$five_star_percent = 0;
		}

		//For 4 Stars rating
		$conds_four_star_rating['rating'] = 4;
		$conds_four_star_rating['product_id'] = $obj->id;
		$four_star_count = $this->CI->Rate->count_all_by($conds_four_star_rating);
		if($total_rating_count > 0) {
			$four_star_percent = number_format((float) ((100 / $total_rating_count) * $four_star_count), 1, '.', '');
		} else {
			$four_star_percent = 0;
		}


		//For 3 Stars rating
		$conds_three_star_rating['rating'] = 3;
		$conds_three_star_rating['product_id'] = $obj->id;
		$three_star_count = $this->CI->Rate->count_all_by($conds_three_star_rating);
		if($total_rating_count > 0) {
			$three_star_percent = number_format((float) ((100 / $total_rating_count) * $three_star_count), 1, '.', '');
		} else {
			$three_star_percent = 0;
		}


		//For 2 Stars rating
		$conds_two_star_rating['rating'] = 2;
		$conds_two_star_rating['product_id'] = $obj->id;
		$two_star_count = $this->CI->Rate->count_all_by($conds_two_star_rating);

		if($total_rating_count > 0) {
			$two_star_percent = number_format((float) ((100 / $total_rating_count) * $two_star_count), 1, '.', '');
		} else {
			$two_star_percent = 0;
		}

		//For 1 Stars rating
		$conds_one_star_rating['rating'] = 1;
		$conds_one_star_rating['product_id'] = $obj->id;
		$one_star_count = $this->CI->Rate->count_all_by($conds_one_star_rating);

		if($total_rating_count > 0) {
		$one_star_percent = number_format((float) ((100 / $total_rating_count) * $one_star_count), 1, '.', '');
		} else {
			$one_star_percent = 0;
		}


		$rating_std = new stdClass();
		@$rating_std->five_star_count = $five_star_count; 
		$rating_std->five_star_percent = $five_star_percent;

		$rating_std->four_star_count = $four_star_count;
		$rating_std->four_star_percent = $four_star_percent;

		$rating_std->three_star_count = $three_star_count;
		$rating_std->three_star_percent = $three_star_percent;

		$rating_std->two_star_count = $two_star_count;
		$rating_std->two_star_percent = $two_star_percent;

		$rating_std->one_star_count = $one_star_count;
		$rating_std->one_star_percent = $one_star_percent;

		$rating_std->total_rating_count = $total_rating_count;
		$rating_std->total_rating_value = $total_rating_value;


		$obj->rating_details = $rating_std;

		if($need_return)
		{
			return $obj;
		} 

	}

	/**
	 * Customize collection object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_collection( &$obj )
	{
		

		$conds['collection_id'] = $obj->id;

		// set default photo
		$obj->default_photo = $this->get_default_photo( $obj->id, 'collection' );

		$collection_id = $this->CI->get_collection_id();

		$count_product = 0;
		if($collection_id == "")
		{
			$count_product_collection = $this->CI->Api->get_one_by( array( 'api_constant' => GET_ALL_COLLECTIONS ) )->count;
		} else {
			$count_product = $this->CI->Productcollection->count_all_by( $conds );
		}


		if ( $count_product_collection > 0 ) {

			for($i = 0; $i < $count_product_collection ; $i++) {
				

				$tmp_collection = $this->CI->Productcollection->get_all_collections( $conds )->result();


				if(isset($tmp_collection[$i]->id)) {

					$prd_conds['id'] = $tmp_collection[$i]->id;
					$prd_conds['delete_flag'] = 0;

					$tmp_product = $this->CI->Product->get_one_by( $prd_conds );
					$obj->products[] = $this->convert_product($tmp_product, true);
				}

			}

		}

	}

	/**
	 * Customize noti object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_noti( &$obj )
	{
		
		
		if($this->get_login_user_id() != "") {
			$noti_user_data = array(
	        	"noti_id" => $obj->id,
	        	"user_id" => $this->get_login_user_id()
	    	);
			if ( !$this->CI->Notireaduser->exists( $noti_user_data )) {
				$obj->is_read = 0;
			} else {
				$obj->is_read = 1;
			}
		} 
		
		// set default photo
		$obj->default_photo = $this->get_default_photo( $obj->id, 'noti' );
	}

	/**
	 * Customize user object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_user( &$obj )
	{
		// country object
		if ( isset( $obj->country_id )) {
			$tmp_country = $this->CI->Country->get_one( $obj->country_id );

			$this->convert_country( $tmp_country );

			$obj->country = $tmp_country;
		}

		// city object
		if ( isset( $obj->city_id )) {
			$tmp_city = $this->CI->City->get_one( $obj->city_id );

			$this->convert_city( $tmp_city );

			$obj->city = $tmp_city;
		}
		
		$prd_conds['user_id'] = $obj->user_id;
		$prd_conds['is_default'] = 1;
		$default_address = $this->CI->UserAddress->get_one_by( $prd_conds );

		$obj->default_address = $default_address;

		$all_UserAddress = $this->CI->UserAddress->get_all_by( array( 'user_id' => $obj->user_id))->result();
		$obj->all_address = $all_UserAddress;

		if($this->CI->input->get('shop_id')){
	     	$obj->shipping_cost=$this->get_user_shipping_cost($obj->user_id, $this->CI->input->get('shop_id'));
	     }

	}

	/**
	 * Customize about object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_about( &$obj )
	{
		// set default photo
		$obj->default_photo = $this->get_default_photo( $obj->about_id, 'about' );

	}

	/**
	 * Checking for transaction
	 *
	 * 
	 */
	function transaction_checking( $trans_details = array())
	{
		$failed_records = array();

		$failed_price = array();

		$failed_available = array();

		$failed_delete = array();


		for($i=0; $i<count($trans_details); $i++) 
		{
			
			// Rule 1 : Need to check the product whether delete or not?
			//$product_is_delete = $this->CI->Product_delete->get_one($trans_details[$i]['product_id'])->product_id;
			$product_is_delete = $this->CI->Delete_history->get_one($trans_details[$i]['product_id'])->product_id;
			if($product_is_delete != "")
			{
				//delete_flag '1' is deleted
				$failed_delete[$i] = $trans_details[$i]['product_id'];

			} else {

				//Rule 2 : Need to check availability of the product
				$product_is_available = $this->CI->Product->get_one($trans_details[$i]['product_id'])->is_available;
				if($product_is_available == 0)
				{
					//is_available '0' is No More Stock
					$failed_available[$i] = $trans_details[$i]['product_id'];
				} else {

					// Rule 3 : Need to check the product price 
					$product_unit_price = $this->CI->Product->get_one($trans_details[$i]['product_id'])->unit_price;

					if($product_unit_price != $trans_details[$i]['price']) 
					{
						
						//Price not same
						$failed_price[$i] = $trans_details[$i]['product_id'];

					} else {
						//Product Price is same but attribute price is not same
						$att_additional_price = $this->CI->Attributedetail->get_one($trans_details[$i]['product_attribute_id'])->additional_price;

						if($att_additional_price != $trans_details[$i]['product_attribute_price'])  {

							//att price not same
							$failed_price[$i] = $trans_details[$i]['product_id'];

						}

					}

				}

			}

		}
		
		$failed_records[] = array_unique($failed_price);
		$failed_records[] = array_unique($failed_available);
		$failed_records[] = array_unique($failed_delete);

		//print_r($failed_records); die;

		return $failed_records;
	}

	/*
	 * Customize tag object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_tag( &$obj )
	{
        $obj->name = $this->getDefaultCaption($obj->name, $obj->name_alt);
		// set default photo
		$obj->default_photo = $this->get_default_photo( $obj->id, 'tag' );

		// set default icon 
		$obj->default_icon = $this->get_default_photo( $obj->id, 'tag-icon' );

	}

	/*
	 * Customize shop object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_shop( &$obj )
	{
		// set default photo
		$obj->name = $this->getDefaultCaption($obj->name, $obj->name_alt);
		$obj->default_photo = $this->get_default_photo( $obj->id, 'shop' );

		// set default icon
		$obj->default_icon = $this->get_default_photo( $obj->id, 'shop-icon' );

		// touch count
		$obj->touch_count =  $this->CI->Touch->count_all_by(array("type_id" => $obj->id, "type_name" => "shop"));
		$obj->comission_plan =  $this->CI->Acc_comission_seller->get_one_by(array("shop_id" => $obj->id));

        //rating details


        $total_rating_count = 0;
        $total_rating_value = 0;

        $five_star_count = 0;
        $five_star_percent = 0;

        $four_star_count = 0;
        $four_star_percent = 0;

        $three_star_count = 0;
        $three_star_percent = 0;

        $two_star_count = 0;
        $two_star_percent = 0;

        $one_star_count = 0;
        $one_star_percent = 0;




        //Rating Total how much ratings for this product
        $conds_rating['shop_id'] = $obj->id;
        $total_rating_count = $this->CI->Shop_rate->count_all_by($conds_rating);
        $sum_rating_value = $this->CI->Shop_rate->sum_all_by($conds_rating)->result()[0]->rating;

        //Rating Value such as 3.5, 4.3 and etc
        if($total_rating_count > 0) {
            $total_rating_value = number_format((float) ($sum_rating_value  / $total_rating_count), 1, '.', '');
        } else {
            $total_rating_value = 0;
        }

        //For 5 Stars rating

        $conds_five_star_rating['rating'] = 5;
        $conds_five_star_rating['shop_id'] = $obj->id;
        $five_star_count = $this->CI->Shop_rate->count_all_by($conds_five_star_rating);
        if($total_rating_count > 0) {
            $five_star_percent = number_format((float) ((100 / $total_rating_count) * $five_star_count), 1, '.', '');
        } else {
            $five_star_percent = 0;
        }

        //For 4 Stars rating
        $conds_four_star_rating['rating'] = 4;
        $conds_four_star_rating['shop_id'] = $obj->id;
        $four_star_count = $this->CI->Shop_rate->count_all_by($conds_four_star_rating);
        if($total_rating_count > 0) {
            $four_star_percent = number_format((float) ((100 / $total_rating_count) * $four_star_count), 1, '.', '');
        } else {
            $four_star_percent = 0;
        }


        //For 3 Stars rating
        $conds_three_star_rating['rating'] = 3;
        $conds_three_star_rating['shop_id'] = $obj->id;
        $three_star_count = $this->CI->Shop_rate->count_all_by($conds_three_star_rating);
        if($total_rating_count > 0) {
            $three_star_percent = number_format((float) ((100 / $total_rating_count) * $three_star_count), 1, '.', '');
        } else {
            $three_star_percent = 0;
        }


        //For 2 Stars rating
        $conds_two_star_rating['rating'] = 2;
        $conds_two_star_rating['shop_id'] = $obj->id;
        $two_star_count = $this->CI->Shop_rate->count_all_by($conds_two_star_rating);

        if($total_rating_count > 0) {
            $two_star_percent = number_format((float) ((100 / $total_rating_count) * $two_star_count), 1, '.', '');
        } else {
            $two_star_percent = 0;
        }

        //For 1 Stars rating
        $conds_one_star_rating['rating'] = 1;
        $conds_one_star_rating['shop_id'] = $obj->id;
        $one_star_count = $this->CI->Shop_rate->count_all_by($conds_one_star_rating);

        if($total_rating_count > 0) {
            $one_star_percent = number_format((float) ((100 / $total_rating_count) * $one_star_count), 1, '.', '');
        } else {
            $one_star_percent = 0;
        }


        $rating_std = new stdClass();
        @$rating_std->five_star_count = $five_star_count;
        $rating_std->five_star_percent = $five_star_percent;

        $rating_std->four_star_count = $four_star_count;
        $rating_std->four_star_percent = $four_star_percent;

        $rating_std->three_star_count = $three_star_count;
        $rating_std->three_star_percent = $three_star_percent;

        $rating_std->two_star_count = $two_star_count;
        $rating_std->two_star_percent = $two_star_percent;

        $rating_std->one_star_count = $one_star_count;
        $rating_std->one_star_percent = $one_star_percent;

        $rating_std->total_rating_count = $total_rating_count;
        $rating_std->total_rating_value = $total_rating_value;


        $obj->rating_details = $rating_std;

	}

	/*
	 * Customize feed object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_feed( &$obj )
	{
		// set default photo
		$obj->default_photo = $this->get_default_photo_for_gallery( $obj->id, 'feed' );

		// shop object
		if ( isset( $obj->shop_id )) {
			$tmp_shop = $this->CI->Shop->get_one( $obj->shop_id );

			$this->convert_shop( $tmp_shop );

			$obj->shop = $tmp_shop;
		}
		
	}

	/**
	 * Customize tag object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_rating( &$obj )
	{
		// set user object
		if ( isset( $obj->user_id )) {
			$tmp_user = $this->CI->User->get_one( $obj->user_id );

			$this->convert_user( $tmp_user );

			$obj->user = $tmp_user;
		}
	}


	/*
	 * Customize Transaction Header object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_transaction_header( &$obj )
	{
		// set total qty
		//$conds['trans_header_id'] = $obj->id;

		//$rst = $this->CI->Transactionheader->get_product_count_from_transaction($conds)->result();


		//$obj->total_item_qty = $rst[0]->total;
		if ( isset( $obj->time_slot_id )) {
			$tmp_time_slot = $this->CI->Timeslotsdetails->get_one( $obj->time_slot_id );
			$obj->time_slot = $tmp_time_slot;
		}

		if ( isset( $obj->product_id )) {
			$obj->default_photo = $this->get_default_photo_for_gallery( $obj->product_id, 'product' );
		}
		
		// shop object
		if ( isset( $obj->shop_id )) {
			$tmp_shop = $this->CI->Shop->get_one( $obj->shop_id );

			$this->convert_shop( $tmp_shop );

			$obj->shop = $tmp_shop;
		}

		$all_traking_status = $this->CI->Transactionstatus->get_all_by()->result();
		foreach ($all_traking_status as $key => $value) {
			$status_conds['transactions_header_id'] = $obj->id;
			$status_conds['transactions_status_id'] = $value->id;
			$trakingInfo = $this->CI->Transactionstatustracking->get_one_by( $status_conds );
			$all_traking_status[$key]->title=get_msg(''.$value->title.'');
			if($trakingInfo->id){
				$all_traking_status[$key]->hasUpdate=1;
				$all_traking_status[$key]->changed_date=$trakingInfo->date_created;
			}else{
				$all_traking_status[$key]->hasUpdate=0;
				$all_traking_status[$key]->changed_date=null;
			}
			
		}
				$obj->traking_status = $all_traking_status;
		
	}	

	function convert_transaction_status( &$obj )
	{
		// set total qty
		//$conds['trans_header_id'] = $obj->id;

		//$rst = $this->CI->Transactionheader->get_product_count_from_transaction($conds)->result();


		//$obj->total_item_qty = $rst[0]->total;
		if ( isset( $obj->product_id )) {
			$obj->default_photo = $this->get_default_photo_for_gallery( $obj->product_id, 'product' );
		}
		// shop object
		if ( isset( $obj->shop_id )) {
			$tmp_shop = $this->CI->Shop->get_one( $obj->shop_id );

			$this->convert_shop( $tmp_shop );

			$obj->shop = $tmp_shop;
		}

		
		
	}

	/*
	 * Customize Transaction Detail object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_transaction_detail( &$obj )
	{

		if ( isset( $obj->product_id )) {
			$obj->default_photo = $this->get_default_photo_for_gallery( $obj->product_id, 'product' );
		}
		// shop object
		if ( isset( $obj->shop_id )) {
			$tmp_shop = $this->CI->Shop->get_one( $obj->shop_id );

			$this->convert_shop( $tmp_shop );

			$obj->shop = $tmp_shop;
		}

		if ( isset( $obj->transactions_header_id )) {
			
				$transactions_header_conds['id'] = $obj->transactions_header_id;
		

			$tmp_Transactionheaders = $this->CI->Transactionheader->get_one_by( $transactions_header_conds );

			
				if ( isset( $obj->transactions_header_id )) {
					$driver_conds['user_id'] = $tmp_Transactionheaders->assign_to;
					$tmp_driver = $this->CI->User->get_one_by( $driver_conds );
					$obj->delivery_boy_id = $tmp_driver->user_id;
				}

				$all_traking_status = $this->CI->Transactionstatus->get_all_by()->result();
				foreach ($all_traking_status as $key => $value) {
					$status_conds['transactions_header_id'] = $obj->transactions_header_id;
					$status_conds['transactions_status_id'] = $value->id;
					$trakingInfo = $this->CI->Transactionstatustracking->get_one_by( $status_conds );
					$all_traking_status[$key]->title=get_msg(''.$value->title.'');
					if($trakingInfo->id){
						$all_traking_status[$key]->hasUpdate=1;
						$all_traking_status[$key]->changed_date=$trakingInfo->date_created;
					}else{
						$all_traking_status[$key]->hasUpdate=0;
						$all_traking_status[$key]->changed_date=null;
					}
					
				}
				$obj->traking_status = $all_traking_status;
			
		}
		
	}


	/**
	 * Customize shipping method object
	 *
	 * @param      <type>  $obj    The object
	 */
	function convert_shipping_method( &$obj )
	{
		
		// added_date timestamp string
		//$obj->added_date_str = ago( $obj->added_date );
		
		// set default photo
		$obj->currency_symbol = $this->CI->Shop->get_one($obj->shop_id)->currency_symbol;

		// set default icon 
		$obj->currency_short_form = $this->CI->Shop->get_one($obj->shop_id)->currency_short_form;
	}

	// For Shipping Zone API

	function convert_shipping_zones_method( &$obj )
	{

		

	}

	// For Country object api

	function convert_country( &$obj )
	{

	}

	// For City object api

	function convert_city( &$obj )
	{

	}


	function get_user_shipping_cost( $user_id = null, $shop_id = null) 
	{

//		$default_fee=15;
//
//			$delivery_fees = $this->CI->Acc_delivery_fees->get_one_by( array( 'is_current' => 1));
//			if($delivery_fees->initial_amount){
//
//				$default_fee=$delivery_fees->initial_amount;
//				if($delivery_fees->isExtraCharge==1 && $delivery_fees->per_kilo_amount){
//
//					$per_kilo_charge=$delivery_fees->per_kilo_amount;
//					//if($shop_id && $user_id){
//
//					$shop = $this->CI->Shop->get_one_by( array( 'id' => $shop_id));
//					$userAddress = $this->CI->UserAddress->get_one_by( array( 'user_id' => $user_id, 'is_default' => 1));
//					if($userAddress->latitute && $userAddress->lontitude && $shop->lat && $shop->lng){
//
//						$lat1=$userAddress->latitute;
//						$lon1=$userAddress->lontitude;
//						$lat2=$shop->lat;
//						$lon2=$shop->lng;
//						$unit="K";
//						$totalDistance=GetDrivingDistance($lat1, $lon1, $lat2, $lon2, $unit);
//						$totalExtraCharge=$totalDistance * $per_kilo_charge;
//						$default_fee = $default_fee + $totalExtraCharge;
//
//					}
//				//}
//
//				}
//			}
//
//
//
//		$shipping_cost = round($default_fee,0,PHP_ROUND_HALF_EVEN);
//		if($shipping_cost>50 || $shipping_cost==15){
//			$shipping_cost=62.5;
//		}
	//	return $shipping_cost;



        $shop_id = $shop_id;
        $userid = $user_id;
        $default_fee=15;
        $shop = $this->CI->Shop->get_one_by( array( 'id' => $shop_id));
        if($shop->is_shop_shipping_cost =='1'){

            if($shop_id=="shop4f302930cd7fecfc9b60a4b240a5c736"){
                // shop shipping starts here
                $default_fee = 0;
                $userAddress = $this->CI->UserAddress->get_one_by(array('user_id' => $userid, 'is_default' => 1));
                if ($userAddress->latitute && $userAddress->lontitude && $shop->lat && $shop->lng) {
                    $lat1 = $userAddress->latitute;
                    $lon1 = $userAddress->lontitude;
                    $lat2 = $shop->lat;
                    $lon2 = $shop->lng;
                    $unit = "K";
                    $totalDistance = GetDrivingDistance($lat1, $lon1, $lat2, $lon2, $unit);
                    if($totalDistance>50){
                        $default_fee = 50;
                    }else{
                        $default_fee = 0;
                    }
                }

                $shipping_cost = round($default_fee, 0, PHP_ROUND_HALF_EVEN);
                return $shipping_cost;
            }else {
                // shop shipping starts here
                $delivery_fees = $this->CI->Acc_delivery_fees->get_one_by(array('is_current' => 1, 'scope' => 'shop', 'shop_id' => $shop_id));
                $outsideCity = 0;
                if ($delivery_fees->initial_amount) {
                    //$totatt=3;
                    $default_fee = $delivery_fees->initial_amount;
                    if ($delivery_fees->isExtraCharge == 1 && $delivery_fees->per_kilo_amount) {
                        //$totatt=4;
                        $per_kilo_charge = $delivery_fees->per_kilo_amount;
                        //if($shopid && $userid){
                        //$totatt=5;

                        $userAddress = $this->CI->UserAddress->get_one_by(array('user_id' => $userid, 'is_default' => 1));
                        if ($userAddress->latitute && $userAddress->lontitude && $shop->lat && $shop->lng) {
                            //$totatt=6;
                            $lat1 = $userAddress->latitute;
                            $lon1 = $userAddress->lontitude;
                            $lat2 = $shop->lat;
                            $lon2 = $shop->lng;
                            $unit = "K";
                            $totalDistance = GetDrivingDistance($lat1, $lon1, $lat2, $lon2, $unit);
                            $totalExtraCharge = $totalDistance * $per_kilo_charge;
                            if ($totalDistance > 50) {
                                $default_fee = 50;
                                $outsideCity = 1;
                            } else {
                                $default_fee = $default_fee + $totalExtraCharge;
                            }


                        }
                        //}

                    }
                }


                $shipping_cost = round($default_fee, 0, PHP_ROUND_HALF_EVEN);
                return $shipping_cost;
            }
            // shop shipping ends here
        }else {
            // global shipping starts here
            $default_fee=15.00;
            $delivery_fees = $this->CI->Acc_delivery_fees->get_one_by(array('is_current' => 1, 'scope' => 'global'));
            $outsideCity = 0;
            if ($delivery_fees->initial_amount) {
                //$totatt=3;
                $default_fee = $delivery_fees->initial_amount;
                if ($delivery_fees->isExtraCharge == 1 && $delivery_fees->per_kilo_amount) {
                    //$totatt=4;
                    $per_kilo_charge = $delivery_fees->per_kilo_amount;
                    //if($shopid && $userid){
                    //$totatt=5;

                    $userAddress = $this->CI->UserAddress->get_one_by(array('user_id' => $userid, 'is_default' => 1));
                    if ($userAddress->latitute && $userAddress->lontitude && $shop->lat && $shop->lng) {
                        //$totatt=6;
                        $lat1 = $userAddress->latitute;
                        $lon1 = $userAddress->lontitude;
                        $lat2 = $shop->lat;
                        $lon2 = $shop->lng;
                        $unit = "K";
                        $totalDistance = GetDrivingDistance($lat1, $lon1, $lat2, $lon2, $unit);
                        $totalExtraCharge = $totalDistance * $per_kilo_charge;
                        if ($totalDistance > 50) {
                            $default_fee = 50;
                            $outsideCity = 1;
                        } else {
                            $default_fee = $default_fee + $totalExtraCharge;
                        }

                    }
                    //}

                } else {
                    $userAddress = $this->CI->UserAddress->get_one_by(array('user_id' => $userid, 'is_default' => 1));
                    if ($userAddress->latitute && $userAddress->lontitude && $shop->lat && $shop->lng) {
                        //$totatt=6;
                        $lat1 = $userAddress->latitute;
                        $lon1 = $userAddress->lontitude;
                        $lat2 = $shop->lat;
                        $lon2 = $shop->lng;
                        $unit = "K";
                        $totalDistance = GetDrivingDistance($lat1, $lon1, $lat2, $lon2, $unit);
                        if ($totalDistance > 50) {
                            $default_fee = 50;
                            $outsideCity = 1;
                        } else {
                            $default_fee = $default_fee;
                        }

                    }


                }
            }


            $shipping_cost = round($default_fee, 0, PHP_ROUND_HALF_EVEN);

           return $shipping_cost;
        }

	}



}