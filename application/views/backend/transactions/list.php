<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" class="table-responsive animated fadeInRight">
			
	<div class="card-body table-responsive p-0">
  
  		<table class="table m-0 table-striped">
		<?php $count = $this->uri->segment(4) or $count = 0; ?>

			<?php if ( !empty( $transactions ) && count( $transactions->result()) > 0 ): ?>
	<tr>
		<th><?php echo get_msg('no'); ?></th>
		<th><?php echo get_msg('invoice'); ?></th>
		<th><?php echo get_msg('customer_phone'); ?></th>
		<th><?php echo get_msg('status_label'); ?></th>
		<th><?php echo get_msg('total_items'); ?></th>
		<th><?php echo get_msg('total_store_price'); ?></th>
		<th><?php echo get_msg('place_on'); ?></th>
		<th><?php echo get_msg('delivery_date'); ?></th>
		<th><?php echo get_msg('assign_to'); ?></th>
		<?php if ( $this->ps_auth->has_access( DEL )): ?>
				
				<th><span class="th-title"><?php echo get_msg('btn_delete')?></span></th>
			
			<?php endif; ?>
		<th><?php echo get_msg('details'); ?></th>
				<th><?php echo get_msg('print'); ?></th>
	</tr>
			<tbody style="font-size: 16px;">
				<?php foreach($transactions->result() as $transaction): 
						$shop = $this->Shop->get_one($transaction->shop_id);

                    $statusLabel="";
                    if($transaction->reject_status == 1){
                        $statusLabel=' - <span class="badge badge-danger">'.get_msg('rejected').'</span>';
                    }
					?>
					
		  					
			  					<tr <?php if ($transaction->reject_status == 1) { ?> class="rejectedOrder" <?php } ?>>
			  						<td><?php echo ++$count;?></td>
			  						<td><?php echo sprintf("%07d", $transaction->id);?></td>
			  						<td style="width: 35%;">
			  							<?php
											echo $transaction->contact_name . "( ".get_msg('contact').": " . $transaction->contact_phone . " )";
										?>
									</td>
									<td style="width: 10%;">
										<?php if ($transaction->trans_status_id == 1) { ?>
							                <span class="badge badge-danger">
							                  <?php echo get_msg(''.$this->Transactionstatus->get_one( $transaction->trans_status_id )->title.$statusLabel.''); ?>
							                </span>
							            <?php } elseif ($transaction->trans_status_id == 2) { ?>
							                <span class="badge badge-info">
							                  <?php echo get_msg(''.$this->Transactionstatus->get_one( $transaction->trans_status_id )->title.$statusLabel.''); ?>
							                </span>
							            <?php } elseif ($transaction->trans_status_id == 3) { ?>
							                <span class="badge badge-success">
							                  <?php echo get_msg(''.$this->Transactionstatus->get_one( $transaction->trans_status_id )->title.$statusLabel.''); ?>
							                </span>
							            <?php } else { ?>
							                <span class="badge badge-primary">
							                  <?php echo get_msg(''.$this->Transactionstatus->get_one( $transaction->trans_status_id )->title.$statusLabel.''); ?>
							                </span>
							            <?php } ?>
									</td>
									<td>
										<?php
											$detail_count = 0;
											$conds['transactions_header_id'] = $transaction->id;
											$detail_count =  $this->Transactiondetail->count_all_by( $conds );
											echo "<small style='padding: 0 50px'>" . $detail_count . " ".get_msg('Items')." </small>";
										 ?>
									</td>
									<td>
										<?php
											 echo  ($transaction->sub_total_amount + $transaction->tax_amount); 
										 ?>
									</td>
									<td>
										<?php echo $transaction->added_date; ?>
									</td>
									<td>
										<?php if($transaction->time_slot_id  && $transaction->shipping_method_amount <50){
												$time_slotinfo=$this->Timeslotsdetails->get_one($transaction->time_slot_id);
												print $time_slotinfo->slot_date."(".$time_slotinfo->start_time."-".$time_slotinfo->end_time.")";
											} ?>
									</td>
									
									<td>
										<?php if($transaction->assign_to){?>
										<?php
												$userinfo=$this->User->get_one($transaction->assign_to);
												print $userinfo->user_name."-".$userinfo->user_phone;?>
											<?php } ?>
									</td>
									
									<td>
										<?php  
						                  if( $this->session->userdata( 'user_id' ) == "usr75023528330eed4cc55f0e1bdd463cfe" || $this->session->userdata( 'user_id' ) == "usr0fe366b0474ecd77faa4c73b6e00eaf6") { // mosleh_dev || Abdul Aziz
						                ?>
										<a herf='#' class='btn-delete' data-toggle="modal" data-target="#reportsmodal" id="<?php echo "$transaction->id";?>">
											<i class='fa fa-trash-o'></i>
										</a>
										<?php } ?>
									</td>
									<td>
                                        <?php if ($transaction->reject_status == 0) { ?>
                                            <a class="pull-right btn btn-sm btn-primary" href="<?php echo $module_site_url . "/detail/" . $transaction->id;?>">
                                            <?php echo get_msg('details'); ?>
                                        </a> <?php } ?>

									</td>
									<td>
                                        <?php if ($transaction->reject_status == 0) { ?>
                                            <a class="pull-right btn btn-sm btn-primary" target="_blank" href="<?php echo $module_site_url . "/printDetailsPdf/" . $transaction->id;?>">
                                                <i class="fa fa-print" aria-hidden="true"></i>
                                            </a> <?php } ?>

									</td>
								</tr>
						
						
			
			<?php endforeach; ?>
			</tbody>
			<?php else: ?>
					
				<?php $this->load->view( $template_path .'/partials/no_data' ); ?>

			<?php endif; ?>
		</table>
	</div>
</div>
<script>
	// Delete Trigger
	$('.btn-delete').click(function(){
	
		// get id and links
		var id = $(this).attr('id');
		var btnYes = $('.btn-yes').attr('href');
		var btnNo = $('.btn-no').attr('href');

		// modify link with id
		$('.btn-yes').attr( 'href', btnYes + id );
		$('.btn-no').attr( 'href', btnNo + id );
	});
</script>
<?php
	// Delete Confirm Message Modal
	$data = array(
		'title' => get_msg( 'delete_trans_label' ),
		'message' =>  get_msg( 'trans_yes_all_message' ),
		'no_only_btn' => get_msg( 'cat_no_only_label' )
	);
	
	$this->load->view( $template_path .'/components/report_delete_confirm_modal', $data );
?>
<style>
    tr.rejectedOrder{
        background: #403f3f!important;
        color: white;
    }
</style>
