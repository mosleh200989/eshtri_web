<?php $this->load->view( $template_path .'/partials/nav' ); ?>

<div class="container-fluid">
  <div class="content-wrapper"  style="background-color: #ffff;padding-left: 30px;">
  		<div class="row">
			<div class="col-12 col-md-3 sidebar teamps-sidebar-open">
				
				<?php $this->load->view( $template_path .'/partials/sidebar' ); ?>
			</div>
			

			<div class="col-12 col-md-12 main teamps-sidebar-push">

				<?php 
					// load breadcrumb
					show_breadcrumb( $action_title );

					// show flash message
					flash_msg();
				?>
  				
					<?php
	$attributes = array( 'id' => 'import-form', 'enctype' => 'multipart/form-data');
	echo form_open( '', $attributes);
?>
	
<section class="content animated fadeInRight">
	<div class="card card-info">
	    <div class="card-header">
	        <h3 class="card-title"><?php echo get_msg('shops')?></h3>
	    </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
             	<div class="col-md-6">
     		       <div class="card card-info">
          			<div class="card-header">
			        	<h3 class="card-title"><?php echo get_msg('Export_from')?></h3>
			      	</div>
			      	<div class="card-body">
					<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('shops')?>
						</label>

						<?php
							$options=array();
							$selected_shop_id = $this->session->userdata('selected_shop_id');
    						$shop_id = $selected_shop_id['shop_id'];
                        $detailsQuery="SELECT * FROM mk_shops";
                        $shops = $this->Acc_period->queryPrepare($detailsQuery);
								foreach($shops->result() as $shop) {
									if($shop_id != $shop->id){
										$options[$shop->id]=$shop->name;
									}
								}

							echo form_dropdown(
								'shop',
								$options,
								set_value( 'shop',"", false ),
								'class="form-control form-control-sm mr-3" id="shop"'
							);
						?>

					</div>
					<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('Prd_search_cat')?>
						</label>
						<select name="cat_id" class="form-control form-control-sm mr-3 valid" id="cat_id" aria-invalid="false">
						<?php
							$options=array();
							print '<option value="0">'.get_msg('Prd_search_cat').'</option>';
						?>
						</select>
					</div>
					<div class="form-group">
						<label> 
							<?php echo get_msg('Prd_search_subcat')?>
						</label>
						<?php
							$conds['cat_id'] = $selected_cat_id;
							$options=array();
							$options[0]=get_msg('Prd_search_subcat');
							echo form_dropdown(
								'sub_cat_id',
								$options,
								set_value( 'sub_cat_id', "", false ),
								'class="form-control form-control-sm mr-3" id="sub_cat_id"'
							);
						?>
					</div>
					</div>
					</div>
                  	</div>
                  	<!--  col-md-6  -->
                  		<div class="col-md-6">
                  		<div class="card card-info">
                  			<div class="card-header">
					        	<h3 class="card-title"><?php echo get_msg('Export_to')?></h3>
					      	</div>
					      	<div class="card-body">
                  			<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('Prd_search_cat')?>
						</label>
						<select name="cat_id_to" class="form-control form-control-sm mr-3 valid" id="cat_id_to" aria-invalid="false">
						<?php
							$options=array();
							$conds['shop_id'] = $selected_shop_id['shop_id'];
							print '<option value="0">'.get_msg('Prd_search_cat').'</option>';
							$categories = $this->Category->get_all_by($conds);
							foreach($categories->result() as $cat) {
							print '<option value="'.$cat->id.'">'.$cat->name.'</option>';
									
							}
						?>
						</select>
					</div>
					<div class="form-group">
						<label> 
							<?php echo get_msg('Prd_search_subcat')?>
						</label>
						<?php
							$conds['cat_id'] = $selected_cat_id;
							$options=array();
							$options[0]=get_msg('Prd_search_subcat');
							echo form_dropdown(
								'sub_cat_id_to',
								$options,
								set_value( 'sub_cat_id_to', "", false ),
								'class="form-control form-control-sm mr-3" id="sub_cat_id_to"'
							);
						?>
					</div>
                  		</div>
					</div>
                  	</div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.card-body -->

		<div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
				<?php echo get_msg('btn_import')?>
			</button>

			<a href="<?php echo $module_site_url; ?>" class="btn btn-sm btn-primary">
				<?php echo get_msg('btn_cancel')?>
			</a>
        </div>
       
    </div>
    <!-- card info -->
</section>
<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>
<script>
	function jqvalidate() {

		$('#product-form').validate({
			rules:{
				cat_id: {
		       		indexCheck : "",
		       		blankCheck : "",
		      	},
		      	sub_cat_id: {
		       		indexCheck : ""
		      	},
		      	
			},
			messages:{
				cat_id:{
					blankCheck : "<?php echo get_msg( 'f_item_cat_required' ) ;?>",
			       	indexCheck: "<?php echo get_msg('f_item_cat_required'); ?>"
			    },
			    sub_cat_id:{
			       indexCheck: "<?php echo get_msg('f_item_subcat_required'); ?>"
			    },
			    
			},

			submitHandler: function(form) {
		        if ($("#product-form").valid()) {
		            form.submit();
		        }
		    }

		});
		
	}

	function runAfterJQ() {
		$('#shop').on('change', function() {
			var shopId = $(this).val();
			$.ajax({
				url: '<?php echo $module_site_url . '/get_all_categories/';?>' + shopId,
				method: 'GET',
				dataType: 'JSON',
				success:function(data){
					$('#cat_id').html("");
					$.each(data, function(i, obj){
					    $('#cat_id').append('<option value="'+ obj.id +'">' + obj.name+ '</option>');
					});
					$('#cat_id').trigger('change');
				}
			});
		});

		$('#cat_id').on('change', function() {
			var catId = $(this).val();
			$.ajax({
				url: '<?php echo $module_site_url . '/get_all_sub_categories/';?>' + catId,
				method: 'GET',
				dataType: 'JSON',
				success:function(data){
					$('#sub_cat_id').html("");
					$.each(data, function(i, obj){
					    $('#sub_cat_id').append('<option value="'+ obj.id +'">' + obj.name+ '</option>');
					});
					$('#sub_cat_id').trigger('change');
				}
			});
		});

	$('#cat_id_to').on('change', function() {
			var catId = $(this).val();
			$.ajax({
				url: '<?php echo $module_site_url . '/get_all_sub_categories/';?>' + catId,
				method: 'GET',
				dataType: 'JSON',
				success:function(data){
					$('#sub_cat_id').html("");
					$.each(data, function(i, obj){
					    $('#sub_cat_id_to').append('<option value="'+ obj.id +'">' + obj.name+ '</option>');
					});
					$('#sub_cat_id_to').trigger('change');
				}
			});
		});

	}



</script>