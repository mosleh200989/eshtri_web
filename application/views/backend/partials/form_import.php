<?php $this->load->view( $template_path .'/partials/nav' ); ?>

<div class="container-fluid">
  <div class="content-wrapper"  style="background-color: #ffff;padding-left: 30px;">
  		<div class="row">
			<div class="col-12 col-md-3 sidebar teamps-sidebar-open">
				
				<?php $this->load->view( $template_path .'/partials/sidebar' ); ?>
			</div>
			

			<div class="col-12 col-md-12 main teamps-sidebar-push">

				<?php 
					// load breadcrumb
					show_breadcrumb( $action_title );

					// show flash message
					flash_msg();
				?>
  				
					<?php
	$attributes = array( 'id' => 'import-form', 'enctype' => 'multipart/form-data');
	echo form_open( '', $attributes);
?>
	
<section class="content animated fadeInRight">
	<div class="card card-info">
	    <div class="card-header">
	        <h3 class="card-title"><?php echo get_msg('shops')?></h3>
	    </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
             	<div class="col-md-6">
					<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('shops')?>
						</label>

						<?php
							$options=array();
								foreach($shops->result() as $shop) {
									$options[$shop->id]=$shop->name;
							}

							echo form_dropdown(
								'shop',
								$options,
								set_value( 'shop',"", false ),
								'class="form-control form-control-sm mr-3" id="cat_id"'
							);
						?>

					</div>



                  	</div>


                  	<!--  col-md-6  -->

            </div>
            <!-- /.row -->
        </div>
        <!-- /.card-body -->

		<div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
				<?php echo get_msg('btn_import')?>
			</button>

			<a href="<?php echo $module_site_url; ?>" class="btn btn-sm btn-primary">
				<?php echo get_msg('btn_cancel')?>
			</a>
        </div>
       
    </div>
    <!-- card info -->
</section>
				

	
	

<?php echo form_close(); ?>
			
			</div>
		</div>
	</div>
</div>
