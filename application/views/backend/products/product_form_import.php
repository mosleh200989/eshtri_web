
<?php
	$attributes = array( 'id' => 'product-form', 'enctype' => 'multipart/form-data');
	echo form_open( '', $attributes);
?>
<section class="content animated fadeInRight">
			
	<div class="card card-info">
      	<div class="card-header">
        	<h3 class="card-title"><?php echo get_msg('prd_info')?></h3>
      	</div>

    <form role="form">
        <div class="card-body">
        	<div class="row">
        		<div class="col-md-6">
        			<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('shops')?>
						</label>
						<select name="shop_id" class="form-control form-control-sm mr-3 valid" id="shop_id" aria-invalid="false">
						<?php
                            $detailsQuery="SELECT * FROM mk_shops";
                            $shops = $this->Acc_period->queryPrepare($detailsQuery);
							$options=array();
							print '<option value="0">'.get_msg('shop').'</option>';
							foreach($shops->result() as $shop) {
								print '<option  value="'.$shop->id.'">'.$shop->name.'</option>';
							}
						?>
						</select>
					</div>
					
					<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('Prd_search_cat')?>
						</label>
						<select name="cat_id" class="form-control form-control-sm mr-3 valid" id="cat_id" aria-invalid="false">
						<?php
							$options=array();
							$conds['shop_id'] = $selected_shop_id;
							print '<option value="0">'.get_msg('Prd_search_cat').'</option>';
							$categories = $this->Category->get_all_by($conds);
							foreach($categories->result() as $cat) {
								if($product && $product->cat_id && $product->cat_id == $cat->id){
									print '<option selected data-keyword="'.$cat->search_tag.'" value="'.$cat->id.'">'.$cat->name.'</option>';
								}else{
									print '<option data-keyword="'.$cat->search_tag.'" value="'.$cat->id.'">'.$cat->name.'</option>';
								}
									
							}
						?>
						</select>
					</div>

					<div class="form-group">
						<label> 
							<?php echo get_msg('Prd_search_subcat')?>
						</label>

						<?php
							if(isset($product)) {
								$options=array();
								$options[0]=get_msg('Prd_search_subcat');
								$conds['cat_id'] = $product->cat_id;
								$sub_cat = $this->Subcategory->get_all_by($conds);
								foreach($sub_cat->result() as $subcat) {
									$options[$subcat->id]=$subcat->name;
								}
								echo form_dropdown(
									'sub_cat_id',
									$options,
									set_value( 'sub_cat_id', show_data( @$product->sub_cat_id), false ),
									'class="form-control form-control-sm mr-3" id="sub_cat_id"'
								);

							} else {
								$conds['cat_id'] = $selected_cat_id;
								$options=array();
								$options[0]=get_msg('Prd_search_subcat');

								echo form_dropdown(
									'sub_cat_id',
									$options,
									set_value( 'sub_cat_id', show_data( @$product->sub_cat_id), false ),
									'class="form-control form-control-sm mr-3" id="sub_cat_id"'
								);
							}
							
						?>

					</div>
            	</div>
    		</div>
    		
			</div>
		<div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary" style="margin-top: 3px;">
				<?php echo get_msg('btn_save')?>
			</button>
        </div>

	</div>
</section>
<?php echo form_close(); ?>
<script>

	function jqvalidate() {

		$('#product-form').validate({
			rules:{
				cat_id: {
		       		indexCheck : "",
		       		blankCheck : "",
		      	},
		      	sub_cat_id: {
		       		indexCheck : ""
		      	},
		      	
			},
			messages:{
				cat_id:{
					blankCheck : "<?php echo get_msg( 'f_item_cat_required' ) ;?>",
			       	indexCheck: "<?php echo get_msg('f_item_cat_required'); ?>"
			    },
			    sub_cat_id:{
			       indexCheck: "<?php echo get_msg('f_item_subcat_required'); ?>"
			    },
			    
			},

			submitHandler: function(form) {
		        if ($("#product-form").valid()) {
		            form.submit();
		        }
		    }

		});
		
	}

	function runAfterJQ() {

		  
		$('#cat_id').on('change', function() {

				var value = $('option:selected', this).text().replace(/Value\s/, '');
				var defaultKeyword = $('option:selected', this).attr('data-keyword');
				if(defaultKeyword){
					$('#search_tag').val(defaultKeyword);
				}
				var catId = $(this).val();
				// alert("adfasd"+ catId);
				 
				$.ajax({
					url: '<?php echo $module_site_url . '/get_all_sub_categories/';?>' + catId,
					method: 'GET',
					dataType: 'JSON',
					success:function(data){
						$('#sub_cat_id').html("");
						$.each(data, function(i, obj){
						    $('#sub_cat_id').append('<option value="'+ obj.id +'">' + obj.name+ '</option>');
						});
						$('#name').val($('#name').val() + " ").blur();
						$('#sub_cat_id').trigger('change');
					}
				});
			});


	
	}



</script>