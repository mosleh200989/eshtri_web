<?php
	$selected_shop_id = $this->session->userdata('selected_shop_id');
	$shop_id = $selected_shop_id['shop_id'];

?>
<center><h2><?php echo get_msg('total_showing')?> : <?php echo $rows_count;?></h2></center>
<div class="table-responsive animated fadeInRight">

	<table class="table m-0 table-striped">
		<tr>
			<th><?php echo get_msg('no'); ?></th>
			<th><?php echo get_msg('product_image'); ?></th>
			<th><?php echo get_msg('product_name'); ?></th>
			<th><?php echo get_msg('name_alt'); ?></th>
			<th><?php echo get_msg('shop_code'); ?></th>
			<th><?php echo get_msg('barcode'); ?></th>
			<th><?php echo get_msg('cat_name'); ?></th>
			<!-- <th><?php echo get_msg('subcat_name'); ?></th> -->
			<th><?php echo get_msg('unit_price')  . '(' . $this->Shop->get_one($shop_id)->currency_symbol . ')'; ?></th>
			<th><?php echo get_msg('original_price')  . '(' . $this->Shop->get_one($shop_id)->currency_symbol . ')'; ?></th>
			<th><?php echo get_msg('pricewithcomission'); ?></th>
			<th><?php echo get_msg('created_by'); ?></th>

			<?php if ( $this->ps_auth->has_access( EDIT )): ?>
				
				<th><span class="th-title"><?php echo get_msg('btn_edit')?></span></th>
			
			<?php endif; ?>
			
			<?php if ( $this->ps_auth->has_access( DEL )): ?>
				
				<th><span class="th-title"><?php echo get_msg('btn_delete')?></span></th>
			
			<?php endif; ?>
			
			<?php if ( $this->ps_auth->has_access( PUBLISH )): ?>
				
				<th><span class="th-title"><?php echo get_msg('availablibily')?></span></th>
				<th><span class="th-title"><?php echo get_msg('btn_publish')?></span></th>
			
			<?php endif; ?>

		</tr>
		
	
	<?php $count = $this->uri->segment(4) or $count = 0; ?>

	<?php if ( !empty( $products ) && count( $products->result()) > 0 ): ?>

		<?php foreach($products->result() as $product): ?>
			
			<tr id="row_<?php echo $product->id;?>">
				<td><?php echo ++$count;?></td>
				<td><?php if($product->thumbnail !=null) { ?>
						<img src="<?php echo site_url( '/'); ?>uploads/small/<?php echo $product->thumbnail;?>" width="80" height="80">
						<?php } else { ?>
						<img src="<?php echo site_url( '/'); ?>files/Icon-Small.png" width="80" height="80">
					<?php } ?>
				</td>
				<td id="row_<?php echo $product->id;?>_name"><?php echo $product->name;?></td>
                <td id="row_<?php echo $product->id;?>_namealt"><?php echo $product->name_alt;?></td>
				<td id="row_<?php echo $product->id;?>_shopcode"><?php echo $product->shop_code;?></td>
				<td id="row_<?php echo $product->id;?>_code"><?php echo $product->code;?></td>
				<td><?php echo $this->Category->get_one( $product->cat_id )->name; ?></td>
				<!-- <td><?php //echo $this->Subcategory->get_one( $product->sub_cat_id )->name; ?></td> -->
				<td>
                <?php if($product->is_featured == 1 ) { ?>
                <span class="fa fa-diamond" style="color:red;"></span>&nbsp;
                <?php } ?>
                    <?php

						$unit_price = $product->unit_price;

						$unit_price = round($unit_price, 2) ;

						echo $unit_price;

				?></td>
				<td id="row_<?php echo $product->id;?>_originalprice">
                    <?php
						$original_price = $product->original_price;
						$original_price = round($original_price, 2) ;
						echo $original_price;
				?></td>
                <td><?php echo addvatandcomission($product->unit_price, $product->id, $product->commission_plan);?></td>
                <td><?php echo $this->User->get_one( $product->added_user_id )->user_name; ?></td>
				<?php if ( $this->ps_auth->has_access( EDIT )): ?>
			
					<td>
						<a href='<?php echo $module_site_url .'/edit/'. $product->id; ?>'>
							<i class='fa fa-pencil-square-o'></i>
						</a>
                        <a style="width: 50px;display: none;" title="<?php echo get_msg('save'); ?>" id="save_button<?php echo $product->id;?>" class="button" onclick="save_row('<?php echo $product->id;?>')"><i class='fa fa-check-circle'></i></a>
                        <a style="width: 50px; " title="<?php echo get_msg('table_edit'); ?>" id="edit_button<?php echo $product->id;?>" class="button" onclick="edit_row('<?php echo $product->id;?>')"><i class='fa fa-pencil-square-o'></i></a>
					</td>
				
				<?php endif; ?>
				
				<?php if ( $this->ps_auth->has_access( DEL )): ?>
					
					<td>
				
						<a herf='#' class='btn-delete' data-toggle="modal" data-target="#myModal" id="<?php echo "$product->id";?>">
							<i class='fa fa-trash-o'></i>
						</a>
						
					</td>
				
				<?php endif; ?>
				
				<?php if ( $this->ps_auth->has_access( PUBLISH )): ?>
					
					<td>
						<?php if ( @$product->is_available== 1): ?>
							<button class="btn btn-sm btn-success unavailable" id='<?php echo $product->id;?>'>
							<?php echo get_msg('available'); ?></button>
						<?php else:?>
							<button class="btn btn-sm btn-danger available" id='<?php echo $product->id;?>'>
							<?php echo get_msg('notavailable'); ?></button><?php endif;?>
					</td>
				
				<?php endif; ?>

				<?php if ( $this->ps_auth->has_access( PUBLISH )): ?>
					
					<td>
						<?php if ( @$product->status== 1): ?>
							<button class="btn btn-sm btn-success unpublish" id='<?php echo $product->id;?>'>
							<?php echo get_msg('btn_yes'); ?></button>
						<?php else:?>
							<button class="btn btn-sm btn-danger publish" id='<?php echo $product->id;?>'>
							<?php echo get_msg('btn_no'); ?></button><?php endif;?>
					</td>
				
				<?php endif; ?>

			</tr>

		<?php endforeach; ?>

		<?php else: ?>
				
			<?php $this->load->view( $template_path .'/partials/no_data' ); ?>

		<?php endif; ?>

	</table>
</div>


<script>
    function edit_row(no)
    {
        document.getElementById("edit_button"+no).style.display="none";
        document.getElementById("save_button"+no).style.display="inline";

        var product_name=document.getElementById("row_"+no+"_name");
        var product_name_data=product_name.innerHTML;
        product_name.innerHTML="<input class='form-control' type='text' id='product_name_text"+no+"' value='"+product_name_data+"'>";

        var product_namealt=document.getElementById("row_"+no+"_namealt");
        var product_namealt_data=product_namealt.innerHTML;
        product_namealt.innerHTML="<input class='form-control' type='text' id='product_namealt_text"+no+"' value='"+product_namealt_data+"'>";

        var product_shopcode=document.getElementById("row_"+no+"_shopcode");
        var product_shopcode_data=product_shopcode.innerHTML;
        product_shopcode.innerHTML="<input class='form-control' type='text' id='product_shopcode_text"+no+"' value='"+product_shopcode_data+"'>";

        var product_code=document.getElementById("row_"+no+"_code");
        var product_code_data=product_code.innerHTML;
        product_code.innerHTML="<input class='form-control' type='text' id='product_code_text"+no+"' value='"+product_code_data+"'>";

        var product_originalprice=document.getElementById("row_"+no+"_originalprice");
        var product_originalprice_data=product_originalprice.innerHTML;
        product_originalprice.innerHTML="<input class='form-control' type='text' id='product_originalprice_text"+no+"' value='"+product_originalprice_data+"'>";
    }

    function save_row(no)
    {
        var request_val={};
        request_val['tableedit'] = true;
        request_val['id'] = no;

        var product_name_val=document.getElementById("product_name_text"+no).value;
        document.getElementById("row_"+no+"_name").innerHTML=product_name_val;
        request_val['name']=product_name_val;

        var product_namealt_val=document.getElementById("product_namealt_text"+no).value;
        document.getElementById("row_"+no+"_namealt").innerHTML=product_namealt_val;
        request_val['name_alt']=product_namealt_val;

        var product_shopcode_val=document.getElementById("product_shopcode_text"+no).value;
        document.getElementById("row_"+no+"_shopcode").innerHTML=product_shopcode_val;
        request_val['shop_code']=product_shopcode_val;

        var product_code_val=document.getElementById("product_code_text"+no).value;
        document.getElementById("row_"+no+"_code").innerHTML=product_code_val;
        request_val['code']=product_code_val;

        var product_originalprice_val=document.getElementById("product_originalprice_text"+no).value;
        document.getElementById("row_"+no+"_originalprice").innerHTML=product_originalprice_val;
        request_val['original_price']=product_originalprice_val;

        document.getElementById("edit_button"+no).style.display="inline";
        document.getElementById("save_button"+no).style.display="none";



            var confirmDel = confirm("<?php echo get_msg('are_you_sure')?> "+no+" ?");
            if (confirmDel == true) {
                jQuery.ajax({
                    type: 'POST',
                    dataType: 'JSON',
                    data: request_val,
                    url: "<?php echo site_url('/admin/products/tableedit');?>",
                    success: function (data, textStatus) {
                        confirmNormalMessages(data.isError, data.message, false);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        serverErrorToast(errorThrown);
                    }
                });
            }

        //
        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });
        // $.post({
        //     url: '{!! URL::route('updatelookup') !!}',
        //     data: request_val,
        //     success: function (response) {
        //     },
        //     error: function () {
        //     }
        // });

    }
</script>