<script>
	<?php if ( $this->config->item( 'client_side_validation' ) == true ): ?>

	function jqvalidate() {

		$('#product-form').validate({
			rules:{
				name:{
					blankCheck : "",
					minlength: 3,
					remote: "<?php echo $module_site_url .'/ajx_exists/'.@$product->id; ?>"
				},
				name_alt:{
					blankCheck : "",
					minlength: 3,
				},
				original_price:{
					blankCheck : "",
					indexCheck : ""
				},
				cat_id: {
		       		indexCheck : "",
		       		blankCheck : "",
		      	},
		      	sub_cat_id: {
		       		indexCheck : ""
		      	},
		      	
			},
			messages:{
				name:{
					blankCheck : "<?php echo get_msg( 'err_Prd_name' ) ;?>",
					minlength: "<?php echo get_msg( 'err_Prd_len' ) ;?>",
					remote: "<?php echo get_msg( 'err_Prd_exist' ) ;?>."
				},
				name_alt:{
					blankCheck : "<?php echo get_msg( 'err_Prd_name' ) ;?>",
					minlength: "<?php echo get_msg( 'err_Prd_len' ) ;?>"
				},
				original_price:{
					blankCheck : "<?php echo get_msg( 'price_required' ) ;?>",
					minCheck : "<?php echo get_msg( 'price_required' ) ;?>",
				},
				cat_id:{
					blankCheck : "<?php echo get_msg( 'f_item_cat_required' ) ;?>",
			       	indexCheck: "<?php echo get_msg('f_item_cat_required'); ?>"
			    },
			    sub_cat_id:{
			       indexCheck: "<?php echo get_msg('f_item_subcat_required'); ?>"
			    },
			    
			},

			submitHandler: function(form) {
		        if ($("#product-form").valid()) {
		            form.submit();
		        }
		    }

		});
		
		jQuery.validator.addMethod("indexCheck",function( value, element ) {
			
			   if(value == 0) {
			    	return false;
			   } else {
			    	return true;
			   };
			   
		});
		jQuery.validator.addMethod("minCheck",function( value, element ) {
			
			   if(value <1 || value=='0') {
			    	return false;
			   } else {
			    	return true;
			   };
			   
		});
			jQuery.validator.addMethod("blankCheck",function( value, element ) {
			
			   if(value == "") {
			    	return false;
			   } else {
			   	 	return true;
			   }
		});

	}

	<?php endif; ?>
	function runAfterJQ() {


		$('.delete-img').click(function(e){
			e.preventDefault();

			// get id and image
			var id = $(this).attr('id');

			// do action
			var action = '<?php echo $module_site_url .'/delete_cover_photo/'; ?>' + id + '/<?php echo @$product->id; ?>';
			console.log( action );
			$('.btn-delete-image').attr('href', action);
			
		});
		  
		$('#cat_id').on('change', function() {

				var value = $('option:selected', this).text().replace(/Value\s/, '');
				var defaultKeyword = $('option:selected', this).attr('data-keyword');
				if(defaultKeyword){
					$('#search_tag').val(defaultKeyword);
				}
				var catId = $(this).val();
				// alert("adfasd"+ catId);
				 
				$.ajax({
					url: '<?php echo $module_site_url . '/get_all_sub_categories/';?>' + catId,
					method: 'GET',
					dataType: 'JSON',
					success:function(data){
						$('#sub_cat_id').html("");
						$.each(data, function(i, obj){
						    $('#sub_cat_id').append('<option value="'+ obj.id +'">' + obj.name+ '</option>');
						});
						$('#name').val($('#name').val() + " ").blur();
						$('#sub_cat_id').trigger('change');
					}
				});
			});

		// colorpicker
		$('.my-colorpicker2').colorpicker()

		// add input colorpicker and count colorpicker
     	$(document).ready(function () {

     		var edit_product_check = $('#edit_product').val();


     		if(edit_product_check == 0) {
     			//new product
     			var counter = 2;
     		} else {
     			//edit product
     			var counter =  parseInt($('#color_total_existing').val())+2;
     		}
     		$('#color_total_existing').val(counter);

      		$("#addColor").click(function () {
      			
				
      		 	var newTextBoxDiv = $(document.createElement('div'))
	     		.attr("id", "colorgroupfield"+counter)
	     		.attr("class", 'row');

	     		newTextBoxDiv.after().html(
	      		'<div class="col-md-6"><input placeholder="<?php echo get_msg( 'color_name' ) ;?>" class="form-control form-control-sm mt-1" type="text" name="color_name' + counter + 
	      		'" id="color_name' + counter + '" value="" ></div><div class="col-md-6"><div id="colorvalue' + counter + '" class="input-group my-colorpicker2 colorpicker-element"><input class="form-control form-control-sm mt-1" type="text" name="colorvalue' + counter + 
	      		'" id="colorvalue' + counter + '" placeholder="<?php echo get_msg( 'color_code' ) ;?>" value="" ><div class="input-group-addon mt-1"><i></i></div></div></div>');

	      		newTextBoxDiv.appendTo("#color-picker-group");
				$('#colorvalue'+counter).colorpicker({});
				counter++;

				$( ".CounterTextBoxDiv" ).remove();
				var newCounterTextBoxDiv = $(document.createElement('div'))
	     		.attr("id", 'CounterTextBoxDiv' + counter);

	     		newCounterTextBoxDiv.after().html(
	      		'<input type="hidden" name="color_total" id="color_total" value=" '+ counter +'" >');

	      		newCounterTextBoxDiv.appendTo(".my-colorpicker2");

	      		

      		});
      	});

      	// add specification
      	$(document).ready(function () {
			<?php 
			if(!@$website_url){?>
				$("#website_url").change(function() {
	      			var url=$(this).val();
	      			var href=$("#picture-download-link").attr("href");
	      			$("#picture-download-link").attr("href", href+"?website_url="+url);
	      		});
			<?php }?>
					
      		
     		//add new product
      		var edit_product_check = $('#edit_product').val();


     		if(edit_product_check == 0) {
     			//new product
     			var counter = 2;
     		} else {
     			//edit product
     			var counter =  parseInt($('#spec_total_existing').val())+2;
     		}

      		$('#spec_total_existing').val(counter);

      		$('#addspec').click(function () {

      			var newTextBoxDiv = $(document.createElement('div'))
	     		.attr("class",'col-md-6',"id", 'TextBoxDiv' + counter);

	     		newTextBoxDiv.after().html(
	      		'<div class="form-group"><label>Title : '+counter+'</label><input class="form-control form-control-sm" type="text" name="prd_spec_title' + counter + 
	      		'" id="prd_spec_title' + counter + '" value="" ></div><div class="form-group"><label>Description : '+counter+'</label><textarea class="form-control" cols="40" rows="5" name="prd_spec_desc' + counter + 
	      		'" id="prd_spec_desc' + counter + '" ></textarea></div>');
    
	      		newTextBoxDiv.appendTo("#spec_data1");
	      		counter++;
	      		
	      		$( "#CounterTextBoxDiv" ).remove();
				var newCounterTextBoxDiv = $(document.createElement('div'))
	     		.attr("id", 'CounterTextBoxDiv' + counter);

	     		newCounterTextBoxDiv.after().html(
	      		'<input type="hidden" name="spec_total" id="spec_total" value=" '+ counter +'" >');

	      		newCounterTextBoxDiv.appendTo("#spec_data1");

	      		
      		});
      	});

 		$('input[name="original_price"]').change(function(e) {

        	if(  $("#discount_percent").val() ) {
				var result = parseFloat($("#original_price").val()) - ( parseFloat($("#original_price").val()) * parseFloat($("#discount_percent").val()) );

				$("#discount_label").text( '( Unit Price : ' + result + ' )' );
				$("#unit_price").val(result);

			} else {
				$("#discount_label").text( '( Unit Price : ' + $("#original_price").val() + ' )' );
				$("#unit_price").val($("#original_price").val());
			}

		});

		$('[data-toggle="tooltip"]').tooltip(); 
	
	}


    function xParseFloat(x) {
        var amount = x.replace(',', '.');
        amount = amount.replace(/[^0-9.]/, '');
        if (amount === '') {
            return false;
        } else {
            return parseFloat(amount);
        }
    }

    function getAmount() {
        var amount = document.getElementById('sum').value;
        return xParseFloat(amount);
    }

    function getVat() {
        var amount = document.getElementById('vat').value;
        return xParseFloat(amount);
    }

    function getOperation() {
        return document.getElementById('formactv').checked ? 'exclude' : 'add';
    }

    function calculatorSubmit() {
        var amount = getAmount();
        if (amount === false || isNaN(amount) || !isFinite(amount)) {
            return false;
        }
        var vat = getVat();
        if (vat === false || isNaN(vat) || !isFinite(vat)) {
            return false;
        }
        var operation = getOperation();
        var result;
        if (operation === 'exclude') {
            result = amount - amount / (1 + vat / 100);
        } else if (operation === 'add') {
            result =  amount * (1 + vat / 100);
        }
        addResults(amount, vat, operation, result);
    }

    function toCurrencyString(x) {
        return (Math.round(x*100)/100).toFixed(2)
    }

    function resultBlock(caption, value) {
        return '<div class="result-block">' +
            caption + '<br/>' + value +
            '</div>'
    }

    function addResults(amount, vat, operation, result) {
        amount = toCurrencyString(amount);
        vat = toCurrencyString(vat);
        result = toCurrencyString(result);
        if(operation === 'add'){
            document.getElementById('original_price').value=toCurrencyString(parseFloat(result));
            document.getElementById('unit_price').value=toCurrencyString(parseFloat(result));
        }else{
            document.getElementById('original_price').value=toCurrencyString(parseFloat(amount) - parseFloat(result));
            document.getElementById('unit_price').value=toCurrencyString(parseFloat(amount) - parseFloat(result));
        }

        return true;
    }
    $(document).on("keydown", ":input:not(textarea)", function(event) {
        return event.key != "Enter";
    });
</script>
<?php 
	// replace cover photo modal
	$data = array(
		'title' => get_msg('upload_photo'),
		'img_type' => 'product',
		'img_parent_id' => @$product->id
	);

	$this->load->view( $template_path .'/components/photo_upload_modal', $data );

	// delete cover photo modal
	$this->load->view( $template_path .'/components/delete_cover_photo_modal' ); 
?>