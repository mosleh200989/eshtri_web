
<div class="row">
    <div class="col-md-12">
        <div class="card" style="padding: 10px 35px 10px 35px;">
            <div class="invoice p-3 mb-3">
                <!-- title row -->

                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped" border="1">
                            <thead>
                            <tr>
                                <th><?php echo get_msg('no'); ?></th>
                                <th><?php echo get_msg('name'); ?></th>
                                <th><?php echo get_msg('name_alt'); ?></th>
                                <th><?php echo get_msg('barcode'); ?></th>
                                <th><?php echo get_msg('unit_price'); ?></th>
                                <th><?php echo get_msg('original_price'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no=1;
                            $this->load->library('zend');
                            //load in folder Zend
                            $this->zend->load('Zend/Barcode');
                            //generate barcode


                            foreach($products->result() as $product):

                                ?>

                                <tr>
                                    <td width="20"><?=$no?></td>
                                    <td align="center"> <?php print $product->name;?> </td>
                                    <td align="center"><?php echo $product->name_alt; ?></td>
                                    <?php
//                                    if($product->code) {
//                                    $file = Zend_Barcode::draw('code128', 'image', array('text' => $product->code), array());
//                                    $code = time() . $product->code;
//                                    $barcodeRealPath = APPPATH . '/cache/' . $code . '.png';
//                                    $barcodePath = APPPATH . '/cache/';
//
//                                    header('Content-Type: image/png');
//                                    $store_image = imagepng($file, $barcodeRealPath);
//                                        $barcode=$barcodePath . $code . '.png';
//                                    echo '<td align="center"><img src="'.$barcode.'"></td>';
//                                    }else{
//                                        echo '<td align="center">'.$product->code.'</td>';
//                                    }

                                    ?>
                                    <td align="center">
                                    <?php if($product->thumbnail){?><img src="<?php echo site_url( '/'); ?>uploads/small/<?php echo $product->thumbnail; ?>" style="max-width: 150px; max-height: 130px" ><?php } ?></td>
                                    <td>
                                        <?php
                                        $unit_price = $product->unit_price;
                                        $unit_price = round($unit_price, 2);
                                        print $unit_price;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $original_price = $product->original_price;
                                        $original_price = round($original_price, 2);
                                        print $original_price;
                                        ?>
                                    </td>
<!--                                    <td></td>-->
<!--                                    <td></td>-->

                                </tr>
                                <?php $no++; ?>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>


            </div>

        </div>
    </div>
</div>
<style type="text/css" media="print">
    @page {
        size: 8.5in 11in;
        margin: 2%;
        margin-header: 5mm;
        margin-footer: 15mm;
    }
</style>