<?php 
if(@$product->id==null){ ?>
<div class="card card-info">
      	<div class="card-header">
        	<h3 class="card-title"><?php echo get_msg('product_import_panel')?></h3>
      	</div>

    <form role="form">
        <div class="card-body">
        	<div class="row">
        		<div class="col-md-6">
        			<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('website_url')?>
						</label>

						<?php echo form_input( array(
							'name' => 'website_url',
							'value' => set_value( 'website_url', show_data( @$website_url), false ),
							'class' => 'form-control form-control-sm',
							'placeholder' => get_msg('pls_website_url'),
							'id' => 'website_url'
						)); ?>

					</div>
					<div class="card-footer">
			            <button type="submit" class="btn btn-sm btn-primary" style="margin-top: 3px;">
							<?php echo get_msg('import')?>
						</button>
        			</div>
        		</div>
        	</div>
        </div>
    </form>
</div>
<?php } ?>
