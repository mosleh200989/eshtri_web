<div class="container-fluid" style="padding: 10px 30px 10px 30px;">
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox cc-box">
                   <?php include 'filter_form.php';?>
                    <div class="ibox-title">
                        <div class="ct-btn">
                            <a href="#" class="btn btn-primary btn-sm form-bg" id="btn-icon" data-toggle="modal" data-target="#accVcrMstModalFormId">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>

                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="tabs-containers">
                            <div class="table-responsive tblc">
                                <table class="table table-stripped table-bordered" id="list-table">
                                    <thead>
                                        <tr>
                                            <th><?php echo get_msg('sl')?></th>
                                            <th><?php echo get_msg('period')?></th>
                                            <th><?php echo get_msg('trans_date')?></th>
                                            <th><?php echo get_msg('stakeholder')?></th>
                                            <th><?php echo get_msg('accounts')?></th>
                                            <th><?php echo get_msg('amount')?></th>
                                            <th><?php echo get_msg('action')?></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'entry_form.php';?>
<?php include 'acc_view.php';?> 

 <script>
   
        $(document).ready(function () {
            var saveBtn = $('#save-btn');
            var resetBtn = $('#reset-btn');
            var listTable = $('#list-table');
            var defaultForm = $('#defaultForm');
            var periodStartDt = "<?php echo $currentPeriod['periodStartDt'];?>";
           var periodCloseDt = "<?php echo $currentPeriod['periodCloseDt'];?>";

            $("#show-btn").on("click", function (e) {
                listTable.DataTable().ajax.reload();
                e.preventDefault();
            });
            
            resetFormByBtn(resetBtn, defaultForm, "<?php echo get_msg('payment_voucher')?>", "<?php echo get_msg('save')?>");
            resetBtn.click(function () {
                // resetMasterForm();
            });
            var startDate = new Date(periodStartDt);
            var endDate = new Date(periodCloseDt);
            
            $('#transDate').datepicker({
              format: 'yyyy-mm-dd'
            }).datepicker("setDate",'now');
            $('#transDate').datepicker('setStartDate', startDate);
            $('#transDate').datepicker('setEndDate', endDate);

            $("#defaultModalOnClickBtn").on("click", function (e) {
                $("#accVcrMstModalFormId").modal('show');
                // resetMasterForm();
                // resetDetailForm();
                $("#accVcrDtls-table > tbody").html("");
                $('#drAmountSum').html('0');
                $('#crAmountSum').html('0');
                e.preventDefault();
            });

            defaultForm.validate({
                lang: langCode,
                ignore: [],
                rules: {
                    acc_coa: {
                        required: true
                    },
                    acc_period_id: {
                        required: true
                    },
                    stake_holder_type: {
                        required: true
                    },
                    stakeholder_id: {
                        required: true
                    },
                    trans_date: {
                        required: true
                    },
                    amount: {
                        required: true
                    },
                },
                submitHandler: function (form) {
                    var url;
                    if ($('#hiddenUpId').val() == '') {
                        url = "<?php echo site_url('/admin/payment_voucher/write');?>";
                    } else {
                        url = "<?php echo site_url('/admin/payment_voucher/editsubmit');?>";
                    }
                    formAfterSubmit(saveBtn, "<?php echo get_msg('processing')?>....")
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: defaultForm.serialize(),
                        success: function (data) {

                            if (data.isError == true) {
                                saveBtn.children().removeClass('fa-spin');
                                saveBtn.contents().first().replaceWith("<?php echo get_msg('save')?>");
                                saveBtn.prop('disabled', false);
                                $('#defaultMsgDiv').html(createMessageDiv("error-msg", data.message));
                            } else {
                                $('#defaultMsgDiv').html(createMessageDiv("success-msg", data.message));
                                saveBtn.children().removeClass('fa-spin');
                                saveBtn.contents().first().replaceWith("<?php echo get_msg('save')?>");
                                saveBtn.prop('disabled', false);
                                listTable.DataTable().ajax.reload();
                                // resetMasterForm();
                                // resetDetailForm();
                                $('#drAmountSum').html('0');
                                $('#crAmountSum').html('0');
                                $("#accVcrDtls-table > tbody").html("");
                            }
                        },
                        failure: function (data) {
                        }
                    })
                }
            });

            listTable.dataTable({
                language: {
                    processing: "<?php echo get_msg('processing')?>...",
                    search: "<?php echo get_msg('search')?>",
                    lengthMenu: "<?php echo get_msg('display _menu_records_per_page')?>",
                    zeroRecords: "<?php echo get_msg('nothing_found_sorry')?>",
                    info: "<?php echo get_msg('showing_page_page_of_pages')?>",
                    infoEmpty: "<?php echo get_msg('no_records_available')?>",
                    infoFiltered: "<?php echo get_msg('filtered_from_max_total_records')?>",
                    paginate: {
                        first: "<?php echo get_msg('first')?>",
                        previous: "<?php echo get_msg('previous')?>",
                        next: "<?php echo get_msg('next')?>",
                        last: "<?php echo get_msg('last')?>"
                    },
                },
                "processing": true,
                "serverSide": true,
                "bAutoWidth": false,
                "iDisplayLength": 25,
                "order": [0, "desc"],
                "sAjaxSource": "<?php echo site_url('/admin/payment_voucher/voucher_list');?>",
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    if (aData.DT_RowId == undefined) {
                        return true;
                    }
                    $('td:eq(6)', nRow).html(getActionButtons(nRow, aData));
                    return nRow;
                },
                "fnServerParams": function (aoData) {
                    aoData.push({"name": "listAction", "value": true},
                        {"name": "trans_date", "value": $('#transDateRpt').val()},
                        {"name": "trans_date_to", "value": $('#transDateToRpt').val()},
                        {"name": "acc_period", "value": $('#accPeriodRpt').val()},
                        {"name": "confirm_status", "value": $('#confirmStatusRpt').val()},
                        {"name": "stake_holder_type", "value": $('#accVcrDtls_stakeHolderTypeRpt').val()},
                        {"name": "stakeholder_name", "value": $('#accVcrDtls_stakeholderRpt').val()},
                        {"name": "acc_coa", "value": $('#accCoaRpt').val()},
                        {"name": "coa_class", "value": $('#coaClassRpt').val()},
                        );
                },
                "aoColumns": [
                    null,
                    null,
                    null,
                    {"bSortable": false},
                    {"bSortable": false},
                    null,
                    {"bSortable": false},
                ]
            });

            listTable.on('click', 'a.show-reference', function (e) {
                var control = this;
                var referenceId = $(control).attr('referenceid');
                jQuery.ajax({
                    type: 'GET',
                    dataType: 'JSON',
                    data: {id: referenceId, showAction: true},
                    url: "<?php echo site_url('/admin/payment_voucher');?>",
                    success: function (data, textStatus) {
                        if (data.isError == false) {
                            $("#view-accPeriod").html("<strong>" + (data.defaultInstance.acc_period_id != null ? data.acc_period : '') + "</strong>");
                            $("#view-transDate").html("<strong>" + ((data.defaultInstance.trans_date != null) ? (getFormattingDate(new Date(data.defaultInstance.trans_date))) : '') + "</strong>");
                            $("#view-chequeDate").html("<strong>" + ((data.defaultInstance.cheque_date != null) ? (getFormattingDate(new Date(data.defaultInstance.cheque_date))) : '') + "</strong>");
                            $("#view-referenceCode").html("<strong>" + (data.defaultInstance.reference_code != null) ? (data.defaultInstance.reference_code) : '' + "</strong>");
                            $("#view-stakeHolderType").html("<strong>" + (data.defaultInstance.stake_holder_type != null ? data.stakeHolderType : '') + "</strong>");
                            $("#view-sourceStakeholder").html("<strong>" + (data.defaultInstance.stakeholder_name != null ? data.defaultInstance.stakeholder_name : '') + "</strong>");
                            $("#view-cashHead").html("<strong>" + (data.defaultInstance.cash_head != null ? data.cashHead : '') + "</strong>");
                            $("#view-chequeNo").html("<strong>" + (data.defaultInstance.cheque_no != null ? data.defaultInstance.cheque_no : '') + "</strong>");
                            $("#view-totalAmount").html("<strong>" + (data.defaultInstance.total_amount != null) ? (data.defaultInstance.total_amount) : '' + "</strong>");
                            $("#view-narration").html("<strong>" + (data.defaultInstance.narration != null) ? (data.defaultInstance.narration) : '' + "</strong>");
                            $("#accVcrDtlsTableView > tbody").html("");
                            rowNumber = 1;
                            if (data.accVcrDtls != null) {
                                $.each(data.accVcrDtls, function (key, value) {
                                    detailInTableView(rowNumber++, value.id, value.acc_coa_id, value.coaCaption, value.sourceStakeholderText, value.dr_amount, value.cr_amount);
                                });
                            }
                            $('#view-activeStatus').html('<h5>' + (data.activeStatus ? data.activeStatus.name : '') + '</h5>');
                            $('#accVcrMstModalViewId').modal('show');
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        serverErrorToast(errorThrown);
                    }
                });
                e.preventDefault();
            });

            listTable.on('click', 'a.update-reference', function (e) {
                var control = this;
                var referenceId = $(control).attr('referenceid');
                jQuery.ajax({
                    type: 'GET',
                    dataType: 'JSON',
                    data: {id: referenceId, editData: true},
                    url: "<?php echo site_url('/admin/payment_voucher');?>",
                    success: function (data, textStatus) {
                        if (data.isError == false) {
                            $('#defaultMsgDiv').html('');
                            $('#hiddenUpId').val(data.defaultInstance.id);
                            $("#accPeriod").val(data.defaultInstance.acc_period_id != null ? '' + data.acc_period.id : '');
                            $("#accPeriodText").val(data.defaultInstance.acc_period_id != null ? '' + data.acc_period.caption : '');
                            $("#stakeHolderType").val(data.defaultInstance.stake_holder_type != null ? '' + data.defaultInstance.stake_holder_type : '');
                            var transDate = data.defaultInstance.trans_date;
                            $('#transDate').datepicker('setDate', ((transDate != null) ? (new Date(transDate)) : ''));
                            var chequeDate = data.defaultInstance.cheque_date;
                            $('#chequeDate').datepicker('setDate', ((chequeDate != null) ? (new Date(chequeDate)) : ''));
                            $("#referenceCode").val((data.defaultInstance.reference_code != null) ? (data.defaultInstance.reference_code) : '');
                            $("#narration").val((data.defaultInstance.narration != null) ? (data.defaultInstance.narration) : '');
                            $("#stakeholder").empty().append('<option value="' + (data.defaultInstance.stakeholder_id!= null ? data.defaultInstance.stakeholder_id : '') + '">' + (data.defaultInstance.stakeholder_name) + '</option>').val(data.defaultInstance.stakeholder_id != null ? data.defaultInstance.stakeholder_id  : '').trigger('change');
                            $("#select2-stakeholder-container").html(data.defaultInstance.stakeholder_name);
                           // $("#payMethod").val(data.defaultInstance.payMethod != null ? '' + data.defaultInstance.payMethod.name : '');
                            $("#chequeNo").val((data.defaultInstance.cheque_no != null) ? (data.defaultInstance.cheque_no) : '');
                            $("#amount").val((data.defaultInstance.total_amount != null) ? (data.defaultInstance.total_amount) : '');
                            // $("#accCoa").select2('val', (data.accCoa != null) ? (data.accCoa) : '');
                            $("#accCoa").select2('val',(data.accCoa));
                            $('#accVcrMstModalFormId').modal('show');
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        serverErrorToast(errorThrown);
                    }
                });
                e.preventDefault();
            });

            listTable.on('click', 'a.confirm-reference', function (e) {
                var selectRow = $(this).parents('tr');
                var data = selectRow.find("td:eq(2)").text();
                var confirmDel = confirm("<?php echo get_msg('are_you_sure?')?>" + data + ' ??');
                if (confirmDel == true) {
                    var control = this;
                    var referenceId = $(control).attr('referenceid');
                    jQuery.ajax({
                        type: 'POST',
                        dataType: 'JSON',
                        data: {id: referenceId, confirmStatus: 'true'},
                        url: "<?php echo site_url('/admin/expense_voucher/confirmdata');?>",
                        success: function (data, textStatus) {
                            if (data.isError == false) {
                            listTable.DataTable().ajax.reload();
                            }else{
                                alert(data.message);
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            serverErrorToast(errorThrown);
                        }
                    });
                }
                e.preventDefault();
            });

            listTable.on('click', 'a.activate-reference,a.delete-reference', function (e) {
                var selectRow = $(this).parents('tr');
                var data = selectRow.find("td:eq(1)").text();
                var referenceClass = $(this).attr("class");
                var confirmDel;
                if (referenceClass == 'activate-reference') {
                    confirmDel = confirm("<?php echo get_msg('are_you_sure_want_to_change_status')?>" + data + ' ?');
                } else if (referenceClass == 'delete-reference') {
                    confirmDel = confirm("<?php echo get_msg('are_you_sure_want_to_remove')?>" + data + ' ?');
                }
                if (confirmDel == true) {
                    var control = this;
                    var referenceId = $(control).attr('referenceid');
                    jQuery.ajax({
                        type: 'POST',
                        dataType: 'JSON',
                        data: {id: referenceId, referenceClass: referenceClass, deleteClass: 'mainClass'},
                        url: "<?php echo site_url('/admin/payment_voucher/remove');?>",
                        success: function (data, textStatus) {
                            deleteMessages(data.isError, data.message, listTable, selectRow);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            serverErrorToast(errorThrown);
                        }
                    });
                }
                e.preventDefault();
            });

            $("#stakeHolderType").change(function(){
                $("#stakeholder").select2({
                ajax: {
                    url: "<?php echo site_url('/admin/expense_voucher');?>",
                    dataType: 'JSON',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page,
                            stakeHolderType: $('#stakeHolderType').val(),
                            stakeholderSearch: 'true'
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: "<?php echo get_msg('stakeholder')?>...",
                allowClear: true,
                width: '100%',
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });
            });
            
        
            $("#accVcrDtls_stakeHolderTypeRpt").change(function(){
                $("#accVcrDtls_stakeholderRpt").select2({
                ajax: {
                    url: "<?php echo site_url('/admin/expense_voucher');?>",
                    dataType: 'JSON',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page,
                            stakeHolderType: $('#accVcrDtls_stakeHolderTypeRpt').val(),
                            stakeholderSearch: 'true'
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: "<?php echo get_msg('stakeholder')?>...",
                allowClear: true,
                width: '100%',
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });
            });

         });
         function formatRepoRpt(repo) {
            if (repo.loading) {
                return repo.text;
            }
            return "<div class='row slt-srs m-0'>" +
                "<div class='col-md-10 sc-con pl-3 pr-1'>" +
                "<span class='scsm-gp'>" +
                "<span class='sc-code lft'>" + repo.caption + '</span>' +
                '</span>' +
                "</div>" +
                "</div>";
        }

        function formatRepoSelectionRpt(repo) {
            if (repo.id === '' || repo.phone== undefined) {
                return "<?php echo get_msg('stakeholder')?>...";
            }
            $('#carriedByCaption').val(repo.caption);
            return repo.caption;
        }

        function formatRepo(repo) {
            if (repo.loading) {
                return repo.text;
            }
            return "<div class='row slt-srs m-0'><div class='col-md-2 thumbnail sc-img p-0'>" + repo.imgsrc + "</div><div class='col-md-10 sc-con pl-3 pr-1'><span class='scsm-gp'><span class='sc-code lft'>" + repo.caption + '</span><span class="sc-phn rit">' + repo.phone + '</span></span><span class="scsm-gp"><span class="sc-capt lft">' + repo.phone + '</span></span><span class="scsm-gp"><span class="sc-eml lft">' + repo.email + "</span></span></div></div>";
        }

        function formatRepoSelection(repo) {
            if (repo.id === '' || repo.phone== undefined) {
                return "<?php echo get_msg('stakeholder')?>...";
            }
            $('#carriedByCaption').val(repo.caption);
            return repo.phone + ' ' + repo.caption;
        }
        function detailInTableView(rowNumber, id, accCoaId, accCoaText, bankAccountText, drAmount, crAmount) {
            $('#accVcrDtlsTableView > tbody').prepend('<tr><td>' + rowNumber + '</td><td>' + nullCheck(accCoaText) + '</td><td>' + nullCheck(bankAccountText) + '</td><td>' + nullCheck(drAmount) + '</td><td>' + nullCheck(crAmount) + '</td></tr>');
        }

    </script>