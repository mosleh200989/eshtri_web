<div class="modal fade common-form" id="accVcrMstModalViewId">
    <div class="modal-dialog modal-xl">
        <div class="modal-content form-bg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <div class="modal-body">
                <div class="modal-area">
                    <div class="form-horizontal form-body-md col-md-12 column-two-form  common-form custom-form-view">
                        <div class="text-center panel panel-default mb-0 crowlr-4">
                            <h3 id="headerTitle" class="m-0 p-1"><?php echo get_msg('payment_voucher')?></h3>
                        </div>

                        <div id="defaultMsgDiv"></div>

                        <div class="column-two-form-down form-view">

                            <div class="row crowmlr-8">
                                <div class="col-md-6 column-two p-1">
                                    <div class="panel panel-default">
                                        <fieldset>
                                            <legend><?php echo get_msg('payment_voucher')?></legend>

                                            <div class="form-group">

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('acc_period')?>:</label>
                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-accPeriod"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('trans_date')?>:</label>
                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-transDate"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('stakeholder_type')?>:</label>
                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-stakeHolderType"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('stakeholder')?>:</label>
                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-sourceStakeholder"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('reference_code')?>:</label>
                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-referenceCode"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="col-md-6 column-two p-1">
                                    <div class="panel panel-default">
                                        <fieldset>
                                            <legend><?php echo get_msg('payment_voucher')?></legend>

                                            <div class="form-group">

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('total_amount')?>: </label>
                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-totalAmount"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('cheque_no')?>:</label>
                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-chequeNo"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('cheque_date')?> :</label>
                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-chequeDate"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('narration')?>:</label>
                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-narration"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include 'dtls_view.php';?>

                        <div class="row  crowmlr-8">
                            <div class="col-md-12 p-1">
                                <div class="panel panel-default mb-0">
                                    <div class="text-right p-2">
                                        <button type="button" class="btn btn-warning" data-dismiss="modal"><?php echo get_msg('cancel')?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
