<div class="ibox-content">
    <div class="tabs-containers">
        <div class="table-responsive tblc">
            <table class="table table-stripped table-bordered"
                   id="accVcrDtlsTableView">
                    <thead>
                        <th><?php echo get_msg('sl')?></th>
                        <th><?php echo get_msg('acc_coa')?></th>
                        <th><?php echo get_msg('stakeholder')?></th>
                        <th><?php echo get_msg('dr_amount')?></th>
                        <th><?php echo get_msg('cr_amount')?></th>
                    </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div>
</div>
