<form class="form-horizontal form-body-md col-md-12 column-two-form common-form report-form-area" id="defaultForm">
    <div class="column-two-form-down report-form">

        <div class="row crowmlr-8">
            <input type="hidden" id="hiddenUpId" name="id"/>

            <div class="col-md-12 column-one p-1">
                <div class="panel panel-default">
                    <div class="form-group">
                        <div class="row">

                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"> <?php echo get_msg('acc_period')?>: &nbsp; </label>
                                    <div class="col-sm-8 fmi_box">
                                      <select class="form-control selectTwo select2 input-sm" name="acc_period" id="accPeriodRpt">
                                      <option value=""><?php echo get_msg('acc_period')?></option>
                                      <?php
                                        foreach ($accPeriodList as $dataVal)
                                        {
                                        echo '<option value="'.$dataVal->id.'">'.$dataVal->caption.'</option>';
                                        }
                                        ?>
                                      </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"><?php echo get_msg('accounts')?>: &nbsp;</label>
                                    <div class="col-sm-8 fmi_box">
                                      <select class="form-control selectTwo select2 input-sm" name="acc_coa" id="accCoaRpt">
                                      <option value=""><?php echo get_msg('accounts')?>...</option>
                                            <?php
                                            foreach ($coaOptionGroup as $optionGroup)
                                            {?>
                                                <optgroup label="<?php echo $optionGroup["code"]." - ".$optionGroup["caption"];?>">
                                                    <?php
                                                    foreach ($optionGroup["options"] as $options)
                                                    {?>
                                                        <option value="<?php echo $options["id"];?>"><?php echo $options["code"]." - ".$options["caption"];?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </optgroup>
                                        <?php
                                            }
                                            ?>
                                      </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"> <?php echo get_msg('accounts_class')?>: </label>
                                    <div class="col-sm-8 fmi_box">
                                      <select class="form-control selectTwo select2 input-sm" name="coa_class" id="coaClassRpt">
                                      <option value=""><?php echo get_msg('accounts_class')?></option>
                                          <?php
                                        foreach ($coaClass as $coa)
                                        {
                                            echo '<option value="'.$coa->coa_oid.'">'.$coa->code.' - '.$coa->caption.'</option>';
                                        }
                                        ?>
                                      </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row crowmlr-8">
            <div class="col-md-12 p-1">
                <div class="panel panel-default mb-0 report-btn">
                    <div class="text-right">
                        <button type="button" class="btn btn-default mb-0" data-toggle="tooltip"
                                id="reset-btn"><?php echo get_msg('reset')?></button>
                        <button type="submit" class="btn btn-primary mb-0" data-toggle="tooltip"
                                id="save-btn"><?php echo get_msg('show')?><i
                                class="fa fa-recycle"></i></button>
                    </div>
                </div>
            </div>
        </div>

    </div>

</form>
