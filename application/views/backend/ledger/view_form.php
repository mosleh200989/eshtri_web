<div class="modal fade common-form" id="accLedgerModalViewId">
    <div class="modal-dialog modal-md">
        <div class="modal-content form-bg">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">×</button>
            </div>

            <div class="modal-body">
                <div class="modal-area">
                    <div class="form-horizontal form-body-md col-md-12 column-one-form  common-form custom-form-view">
                        <div class="text-center panel panel-default mb-0 crowlr-4">
                            <h3 id="headerTitle" class="m-0 p-1"><?php echo get_msg('ledger')?></h3>
                        </div>

                        <div id="defaultMsgDiv"></div>

                        <div class="column-one-form-down form-view">

                            <div class="row crowmlr-8">
                                <div class="col-md-12 column-one p-1">
                                    <div class="panel panel-default">
                                        <fieldset>
                                          <div class="form-group">

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('code')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-code"></span>
                                                    </div>
                                                </div>
                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('accPeriod')?>  :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-accPeriod"></span>
                                                    </div>
                                                </div>
                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('accCoa')?>  :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-accCoa"></span>
                                                    </div>
                                                </div>
                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('accPeriod')?><g:message
                                                            code="app.controller.accPeriod.isCurrent"
                                                            default="Is Current"/> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-isCurrent"></span>
                                                    </div>
                                                </div>
                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('openingDR')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-openingDr"></span>
                                                    </div>
                                                </div>
                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('openingCR')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-openingCr"></span>
                                                    </div>
                                                </div>
                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('currentDR')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-currentDr"></span>
                                                    </div>
                                                </div>
                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('currentCR')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-currentCr"></span>
                                                    </div>
                                                </div>
                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('closingDR')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-closingDr"></span>
                                                    </div>
                                                </div>
                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span class="vmiddle"><?php echo get_msg('closingCR')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-closingCr"></span>
                                                    </div>
                                                </div>

                                          </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row  crowmlr-8">
                            <div class="col-md-12 p-1">
                                <div class="panel panel-default mb-0">
                                    <div class="text-right p-2">
                                        <button type="button" class="btn btn-warning"
                                                data-dismiss="modal"><?php echo get_msg('cancel')?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>