<div class="container-fluid" style="padding: 10px 30px 10px 30px;">
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox cc-box">
                    <?php include 'filter_form.php';?>

                    <div class="ibox-content">
                        <div class="tabs-containers">
                            <div class="table-responsive tblc">
                                <table class="table table-stripped table-bordered" id="list-table">
                                    <thead>
                                        <tr>
                                            <th><?php echo get_msg('SL')?></th>
                                            <th><?php echo get_msg('Code')?></th>
                                            <th><?php echo get_msg('period')?></th>
                                            <th><?php echo get_msg('Accounts')?></th>
                                            <th><?php echo get_msg('isCurrent')?></th>
                                            <th><?php echo get_msg('openingDR')?></th>
                                            <th><?php echo get_msg('openingCR')?></th>
                                            <th><?php echo get_msg('closingDR')?></th>
                                            <th><?php echo get_msg('closingCR')?></th>
                                            <th><?php echo get_msg('currentDR')?></th>
                                            <th><?php echo get_msg('currentCR')?></th>
                                            <th><?php echo get_msg('Action')?></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <?php include 'view_form.php';?>
 <script>
        $(document).ready(function () {

            var listTable = $('#list-table');
            $("#save-btn").on("click", function (e) {
                listTable.DataTable().ajax.reload();
                e.preventDefault();
            });
            $("#reset-btn").on("click", function (e) {
                resetFormValue($('#defaultForm'));
                $('#status').select2('val', '');
                $('#priority').select2('val', '');
                $('#category').select2('val', '');
                $('#assignedTo').select2('val', '');
                $('#reportFormatEnum').select2('val', '');
                e.preventDefault();
            });
            listTable.dataTable({
                language: {
                    processing: "<?php echo get_msg('processing')?>...",
                    search: "<?php echo get_msg('search')?>",
                    lengthMenu: "<?php echo get_msg('display_menu_records_per_page')?>",
                    zeroRecords: "<?php echo get_msg('nothing_found_sorry')?>",
                    info: "<?php echo get_msg('showing_page_page_of_pages')?>",
                    infoEmpty: "<?php echo get_msg('no_records_available')?>",
                    infoFiltered: "<?php echo get_msg('filtered_from_max_total_records')?>",
                    paginate: {
                        first: "<?php echo get_msg('first')?>",
                        previous: "<?php echo get_msg('previous')?>",
                        next: "<?php echo get_msg('next')?>",
                        last: "<?php echo get_msg('last')?>"
                    },
                },
                "processing": true,
                "serverSide": true,
                "bAutoWidth": false,
                "iDisplayLength": 50,
                "order": [0, "desc"],
                "sAjaxSource": "<?php echo site_url('/admin/ledger/ledger_list');?>",
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    if (aData.DT_RowId == undefined) {
                        return true;
                    }
                    $('td:eq(11)', nRow).html(getActionButtons(nRow, aData));
                    return nRow;
                },
                "fnServerParams": function (aoData) {
                    aoData.push(
                        {"name": "listAction", "value": true},
                        {"name": "acc_period", "value": $('#accPeriodRpt').val()},
                        {"name": "acc_coa", "value": $('#accCoaRpt').val()},
                        {"name": "coa_class", "value": $('#coaClassRpt').val()},
                    );
                },
                "aoColumns": [
                    null,
                    null,
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                    {"bSortable": false},
                ]
            });

            listTable.on('click', 'a.show-reference', function (e) {
                var control = this;
                var referenceId = $(control).attr('referenceid');
                jQuery.ajax({
                    type: 'GET',
                    dataType: 'JSON',
                    data: {id: referenceId, showAction: true},
                    url: "<?php echo site_url('/admin/ledger');?>",
                    success: function (data, textStatus) {
                        console.log(data);
                        if (data.isError == false) {
                            $("#view-code").html("<strong>" + (data.defaultInstance.code != null) ? (data.defaultInstance.code) : '' + "</strong>");
                            $("#view-accPeriod").html("<strong>" + (data.defaultInstance.acc_period_id != null ? data.accPeriod : '') + "</strong>");
                            $("#view-accCoa").html("<strong>" + (data.defaultInstance.acc_coa_id != null ? data.accCoa : '') + "</strong>");
                            $("#view-isCurrent").html("<strong>" + (data.defaultInstance.is_current != null) ? (data.isCurrent) : '' + "</strong>");
                            $("#view-openingDr").html("<strong>" + (data.defaultInstance.opening_dr != null) ? (data.defaultInstance.opening_dr) : '' + "</strong>");
                            $("#view-openingCr").html("<strong>" + (data.defaultInstance.opening_cr != null) ? (data.defaultInstance.opening_cr) : '' + "</strong>");
                            $("#view-currentDr").html("<strong>" + (data.defaultInstance.current_dr != null) ? (data.defaultInstance.current_dr) : '' + "</strong>");
                            $("#view-currentCr").html("<strong>" + (data.defaultInstance.current_cr != null) ? (data.defaultInstance.current_cr) : '' + "</strong>");
                            $("#view-closingDr").html("<strong>" + (data.defaultInstance.closing_dr != null) ? (data.defaultInstance.closing_dr) : '' + "</strong>");
                            $("#view-closingCr").html("<strong>" + (data.defaultInstance.closing_cr != null) ? (data.defaultInstance.closing_cr) : '' + "</strong>");
                            // $('#view-activeStatus').html('<h5>' + (data.activeStatus ? data.activeStatus : '') + '</h5>');
                            $('#accLedgerModalViewId').modal('show');
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        serverErrorToast(errorThrown);
                    }
                });
                e.preventDefault();
            });

            // $('#report-btn').on("click", function (e) {
            //     window.open("${g.createLink(controller: 'accLedger', action: 'index')}?printReport=true&" + $("#defaultForm").serialize());
            //     e.preventDefault();
            // });

        });

    </script>