<?php
	$attributes = array('id' => 'user-form');
	echo form_open( 'admin/registered_users/save', $attributes );
?>

<div class="container-fluid">
	<div class="col-12"  style="padding: 30px 20px 20px 20px;">
		<div class="card earning-widget">
		    <div class="card-header" style="border-top: 2px solid red;">
				<h5><?php echo get_msg('user_info')?></h5>
			</div>

		<div id="perm_err" class="alert alert-danger fade in" style="display: none">
			<label for="permissions[]" class="error"></label>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		</div>
		
		<div class="card-body">	
			<div class="row">
				<div class="col-6">
						<div class="form-group">
							<label><?php echo get_msg('user_mobile')?></label>

							<?php echo form_input(array(
								'name' => 'user_phone',
								'value' => set_value( 'user_phone', show_data( @$user->user_phone ), false ),
								'class' => 'form-control form-control-sm',
								'placeholder' => get_msg('user_phone'),
								'id' => 'user_phone',
								'maxlength' => '10',
								'type' => 'number'
							)); ?>

						</div>
						<div class="form-group">
							<label><?php echo get_msg('user_name')?></label>

							<?php echo form_input(array(
								'name' => 'user_name',
								'value' => set_value( 'user_name', show_data( @$user->user_name ), false ),
								'class' => 'form-control form-control-sm',
								'placeholder' => get_msg('user_name'),
								'id' => 'name'
							)); ?>

						</div>
						
						<div class="form-group">
							<label><?php echo get_msg('user_email')?></label>

							<?php echo form_input(array(
								'name' => 'user_email',
								'value' => set_value( 'user_email', show_data( @$user->user_email ), false ),
								'class' => 'form-control form-control-sm',
								'placeholder' => get_msg('user_email'),
								'id' => 'user_email'
							)); ?>

						</div>
						
						<?php if ( @$user->user_is_sys_admin == false ): ?>


						
						<div class="form-group">
							<label><?php echo get_msg('role')?></label>
							<select class="form-control form-control-sm" name='role_id' id='role_id'>
								<?php
									foreach($this->Role->get_all()->result() as $role){
										if($role->role_id==4){
										echo "<option value='".$role->role_id."'>".$role->role_desc."</option>";
										}
									}
								?>
							</select>
						</div>

						<?php endif; ?>
				</div>
				

			</div>
		</div>

		<div class="card-footer">
			<button type="submit" class="btn btn-sm btn-primary"><?php echo get_msg('btn_save')?></button>
			<a href="<?php echo $module_site_url; ?>" class="btn btn-sm btn-primary"><?php echo get_msg('btn_cancel')?></a>
		</div>
	</div>
    <!-- card info -->
</div>


<?php echo form_close(); ?>