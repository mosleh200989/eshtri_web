<script>
	<?php if ( $this->config->item( 'client_side_validation' ) == true ): ?>

	function jqvalidate() {

		$('#detail_form').validate({


			rules:{
				name:{
					blankCheck : "",
					minlength: 3,
					remote: {
		                url: '<?php echo $module_site_url .'/ajx_exists/'.@$attdetail->id; ?>',
		                type: 'get',
		                data: {
		                    header_id: function () {
			                    return $("#header_id").val();
			                    return $("#name").css({"color": "red"});
			                }

		                } 

		            }
				},
				additional_price:{
					blankCheck : ""
				}
			},
			messages:{
				name:{
					blankCheck : "<?php echo get_msg( 'err_detail_name' ) ;?>",
					minlength: "<?php echo get_msg( 'err_detail_len' ) ;?>",
					remote:  "<?php echo get_msg( 'err_detail_exist' ) ;?>",
				},
				additional_price:{
					blankCheck : "<?php echo get_msg( 'err_price' ) ;?>"
				}
			}


		});
		// custom validation
		jQuery.validator.addMethod("blankCheck",function( value, element ) {
			
			   if(value == "") {
			    	return false;
			   } else {
			   	
			    	return true;
			   };
		})

		$('input[name="additional_price"]').keyup(function(e)
        {
		  if (/[^\d.-]/g.test(this.value))
		  {
		    // Filter non-digits from input value.
		    this.value = this.value.replace(/[^\d.-]/g, '');
		  }
		});
		
	}

	<?php endif; ?>

    function xParseFloat(x) {
        var amount = x.replace(',', '.');
        amount = amount.replace(/[^0-9.]/, '');
        if (amount === '') {
            return false;
        } else {
            return parseFloat(amount);
        }
    }

    function getAmount() {
        var amount = document.getElementById('sum').value;
        return xParseFloat(amount);
    }

    function getVat() {
        var amount = document.getElementById('vat').value;
        return xParseFloat(amount);
    }

    function getOperation() {
        return document.getElementById('formactv').checked ? 'exclude' : 'add';
    }

    function calculatorSubmit() {
        var amount = getAmount();
        if (amount === false || isNaN(amount) || !isFinite(amount)) {
            return false;
        }
        var vat = getVat();
        if (vat === false || isNaN(vat) || !isFinite(vat)) {
            return false;
        }
        var operation = getOperation();
        var result;
        if (operation === 'exclude') {
            result = amount - amount / (1 + vat / 100);
        } else if (operation === 'add') {
            result =  amount * (1 + vat / 100);
        }
        addResults(amount, vat, operation, result);
    }

    function toCurrencyString(x) {
        return (Math.round(x*100)/100).toFixed(2)
    }

    function resultBlock(caption, value) {
        return '<div class="result-block">' +
            caption + '<br/>' + value +
            '</div>'
    }

    function addResults(amount, vat, operation, result) {
        amount = toCurrencyString(amount);
        vat = toCurrencyString(vat);
        result = toCurrencyString(result);
        if(operation === 'add'){
            document.getElementById('price').value=toCurrencyString(parseFloat(result));
        }else{
            document.getElementById('price').value=toCurrencyString(parseFloat(amount) - parseFloat(result));
        }

        return true;
    }
</script>