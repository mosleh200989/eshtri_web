<?php
	$attributes = array( 'id' => 'detail_form', 'enctype' => 'multipart/form-data');
	echo form_open( '', $attributes);
?>

	<hr/>
	<br>

	<section class="content animated fadeInRight">
		<div class="card card-info">
		    <div class="card-header">
		        <h3 class="card-title"><?php echo get_msg('att_detail_info')?></h3>
		    </div>
	        <!-- /.card-header -->
	        <div class="card-body">
	            <div class="row">
	             	<div class="col-md-6">
						<div class="form-group">
							<label> <span style="font-size: 17px; color: red;">*</span>
								<?php echo get_msg('selected_product_name'); ?> : 
								<input type="hidden" id="product_id" name="product_id" value="<?php echo $this->Product->get_one($product_id)->id; ?>">
								<?php 
									$name = $this->Product->get_one($product_id)->name;
									echo $name;
								?>
							</label>
						</div>

						<div class="form-group">
							<label> <span style="font-size: 17px; color: red;">*</span>
								<?php echo get_msg('selected_attribute_header'); ?> : 
								<input type="hidden" id="header_id" name="header_id" value="<?php echo $this->Attribute->get_one($header_id)->id; ?>">
								<?php 

									$name = $this->Attribute->get_one($header_id)->name;
									echo $name;

								?>
							</label>
						</div>

						<div class="form-group">
							<label> <span style="font-size: 17px; color: red;">*</span>
								<?php echo get_msg('att_detail_name')?>
								<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('att_name_tooltips')?>">
									<i class="fa fa-info-circle"></i>
								</a>
							</label>
							<br>
							

							<?php echo form_input( array(
								'name' => 'name',
								'value' => set_value( 'name', show_data( @$attdetail->name), false ),
								'class' => 'form-control form-control-sm',
								'placeholder' => get_msg('pls_att_name'),
								'id' => 'name'
							)); ?>

						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label> <span style="font-size: 17px; color: red;">*</span>
								<?php echo get_msg('att_price')?>
								<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('att_name_tooltip')?>">
									<i class="fa fa-info-circle"></i>
								</a>
							</label>
							<br>
							
							<i><label><?php echo get_msg('attribute_detail_extra_price'); ?></label></i>

							<?php echo form_input( array(
								'name' => 'additional_price',
								'value' => set_value( 'price', show_data( @$attdetail->additional_price), false ),
								'class' => 'form-control form-control-sm',
								'placeholder' => get_msg('att_price'),
								'id' => 'price'
							)); ?>

						</div>

						<div class="form-group">
							<label> <span style="font-size: 17px; color: red;">*</span>
								<?php echo get_msg('price_type')?>
							</label>
								<?php
							$options=array();
							$options['additional_price']=get_msg('additional_price');
							$options['individul_price']=get_msg('individul_price');
							
							echo form_dropdown(
								'price_type',
								$options,
								set_value( 'price_type', show_data( @$attdetail->price_type), false ),
								'class="form-control form-control-sm mr-3" id="price_type"'
							);
						?>
						</div>
                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('ordering')?>
                                <a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('ordering_tooltips')?>">
									<span class='glyphicon glyphicon-info-sign menu-icon'>
                                </a>
                            </label>

                            <?php echo form_input( array(
                                'name' => 'display_serial',
                                'value' => set_value( 'ordering', show_data( @$attdetail->display_serial ), false ),
                                'class' => 'form-control form-control-sm',
                                'placeholder' => get_msg( 'ordering' ),
                                'id' => 'display_serial',
                                'type' => 'number'
                            )); ?>
                        </div>
                        <section id="calculator-form" style="background: #c9e66f; padding: 10px; font-weight: bold;">
                            <h1><?php echo get_msg('Vat_Calculator')?></h1>
                            <div class="amount form-group">
                                <?php echo get_msg('Amount')?>: <input type="number" name="sum" id="sum" step="0.01" min="0" value="">
                            </div>
                            <div class="form-group">
                                <input type="radio" name="action" value="v" id="formactv" checked=""><label for="formactv"> <?php echo get_msg('exclude_VAT')?> </label>
                                <input type="radio" name="action" value="n" id="formactn"><label for="formactn"> <?php echo get_msg('add_VAT')?> </label>
                                <input type="number" size="6" min="0" step="0.01" name="vat" id="vat" value="15">%
                                <input type="button" class="calculatebutton" value="<?php echo get_msg('Calculate')?>" onclick="calculatorSubmit();return false;">
                            </div>
                            <div class="clearfix"></div>
                            <div id="results">

                            </div>
                        </section>
					</div>
				</div>
			</div>	

			<div class="card-footer">
				<input type="hidden" id="product_id" name="header" value="$this->Product->get_one($product_id)->id">
				<button type="submit" class="btn btn-sm btn-primary">
					<?php echo get_msg('btn_save')?>
				</button>

				<a href="<?php echo site_url() . '/admin/attributedetails/index/'.$header_id . '/'.$product_id ?>" class="btn btn-sm btn-primary">
						<?php echo get_msg('btn_cancel')?>
				</a>
			</div>
		</div>
	</section>	

<?php echo form_close(); ?>