<div class="row crowmlr-8 master-detail">

    <div class="col-md-12 column-one p-1">
        <div class="panel panel-default">
            <fieldset class="mt-0">
                <legend><?php echo get_msg('receive_voucher')?></legend>

                <div class="form-group">
                    <div class="row m-0">
                        <div class="col-lg-4 column-three pl-1 pr-1">
                            <div class="row mb-1">
                                <label class="col-sm-4 control-label"><?php echo get_msg('acc_coa')?><span class="text-warning">*</span> : &nbsp </label>
                                <div class="col-sm-8 fmi_box">
                                    <select class="form-control selectTwo input-sm" name="acc_coa" id="accVcrDtls_accCoa">
                                    <option value=""><?php echo get_msg('accounts')?>...</option>
                                            <?php
                                            foreach ($coaOptionGroupList as $optionGroup)
                                            {?>
                                                <optgroup label="<?php echo $optionGroup["code"]." - ".$optionGroup["caption"];?>">
                                                    <?php
                                                    foreach ($optionGroup["options"] as $options)
                                                    {?>
                                                        <option value="<?php echo $options["id"];?>"><?php echo $options["code"]." - ".$options["caption"];?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </optgroup>
                                        <?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-8">
                            <div class="row">

                                <div class="col-lg-3 column-three pl-1 pr-1">
                                    <div class="row mb-1">
                                        <label class="col-sm-4 control-label"><?php echo get_msg('tax_percent')?><span class="text-warning">*</span> :</label>
                                        <div class="col-sm-8 fmi_box">
                                            <input type="number" id="accVcrDtls_taxPercent" name="acc_vcr_dtls_tax_percent" class="form-control input-sm" value="0">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 column-three pl-1 pr-1">
                                    <div class="row mb-1">
                                        <label class="col-sm-4 control-label"><?php echo get_msg('amount')?><span class="text-warning">*</span> :</label>
                                        <div class="col-sm-8 fmi_box">
                                            <input type="number" id="accVcrDtls_drCrAmount" name="acc_vcr_dtls_dr_cr_amount"
                                                   class="form-control input-sm">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="col-lg-12 pl-1 pr-1 float-right">
                            <div class="text-right p-2">
                                <button type="button" class="btn btn-default"
                                        data-toggle="tooltip"
                                        id="reset-accVcrDtls-btn"><?php echo get_msg('reset')?></button>
                                <button type="button" class="btn btn-primary"
                                        data-toggle="tooltip"
                                        id="add-accVcrDtls-btn"><?php echo get_msg('add')?>... <i
                                        class="fa fa-recycle"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="ibox-content p-1">
                <div class="tabs-containers">

                    <div class="table-responsive tblc max-height-200 min-height-200 sub-master-tbl">
                        <table class="table table-stripped table-bordered"
                               id="accVcrDtls-table">
                            <thead>
                            <th><?php echo get_msg('sl')?></th>
                            <th><?php echo get_msg('acc_coa')?></th>
                            <th><?php echo get_msg('dr_amount')?></th>
                            <th><?php echo get_msg('cr_amount')?></th>
                            <th><?php echo get_msg('tax_percent')?></th>
                            <th><?php echo get_msg('tax_amount')?></th>
                            <th><?php echo get_msg('action')?></th>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="2"></td>
                                <td class="text-navy"><strong id="drAmountSum">0</strong></td>
                                <td class="text-navy"><strong id="crAmountSum">0</strong></td>
                                <td class="text-navy"><strong id="taxPercentSum"></strong></td>
                                <td class="text-navy"><strong id="taxAmountSum">0</strong></td>
                                <td></td>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
            </div>
        </fieldset>
        </div>
    </div>

</div>

