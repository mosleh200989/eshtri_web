<div class='row my-3'>
	<div class='col-6'>
		<?php
			$attributes = array('class' => 'form-inline');
			echo form_open( $module_site_url .'/search/'.$product_id, $attributes);
		?>
			
		<div class="form-group" style="padding-right: 2px;">

			<?php echo form_input(array(
				'name' => 'searchterm',
				'value' => set_value( 'searchterm' ),
				'class' => 'form-control form-control-sm',
				'placeholder' => 'Search'
			)); ?>

	  	</div>

		<div class="form-group" style="padding-right: 3px;">
			<input type="hidden" name="product_id" id="product_id" value="<?php echo $product_id ?>">
		  	<button type="submit" class="btn btn-sm btn-primary">
		  		<?php echo get_msg( 'btn_search' )?>
		  	</button>
	  	</div>

	  	<div class="form-group">
		  	<a href="<?php echo $module_site_url .'/index' .'/' . $product_id; ?>" class="btn btn-sm btn-primary">
				<?php echo get_msg( 'btn_reset' ); ?>
			</a>
		</div>
		
		<?php echo form_close(); ?>

	</div>
    <div class='col-3'>
        <div class="row mb-1">
            <label class="col-sm-2 control-label"><?php echo get_msg('attributes')?>:</label>
            <div class="col-sm-6 fmi_box">
                <select class="form-control select2 selectTwo input-sm" name="attributessearch" id="attributessearch">
                    <option value=""><?php echo get_msg('attributes')?></option>
                </select>
            </div>
            <div class="col-sm-2">
                <a href='#' class='btn btn-sm btn-primary pull-right' id="import-attributes">
                    <span class='fa fa-plus'></span>
                    <?php echo get_msg( 'import' )?>
                </a>
            </div>
        </div>
    </div>
	<div class='col-3'>
		<a href='<?php echo $module_site_url .'/add' .'/' . $product_id;?>' class='btn btn-sm btn-primary pull-right'>
			<span class='fa fa-plus'></span> 
			<?php echo get_msg( 'att_add' )?>
		</a>
	</div>

</div>

<script>
    <?php
    $selected_shop_id = $this->session->userdata('selected_shop_id');
    $shop_id = $selected_shop_id['shop_id'];
    ?>
    $(document).ready(function () {
        function FetchData() {
            $("#attributessearch").select2({
                ajax: {
                    url: "<?php echo site_url('/admin/attributes');?>",
                    dataType: 'JSON',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page,
                            shop_id: "<?=$shop_id?>",
                            attributesSearch: 'true'
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: "<?php echo get_msg('attributes')?>...",
                allowClear: true,
                width: '100%',
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });


            $(document.body).on("click","#import-attributes",function(e){
                var attributeid = $("#attributessearch").val();
                var shopId = "<?=$shop_id?>";
                jQuery.ajax({
                    type: 'GET',
                    dataType: 'JSON',
                    data: {shop_id: shopId, attributeid:attributeid, product_id:'<?=$product_id?>', copyAttributeData: true},
                    url: "<?php echo site_url('/admin/attributes');?>",
                    success: function (data, textStatus) {
                        if (data.isError == false) {
                            confirmNormalMessages(data.isError, data.message, true);
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        serverErrorToast(errorThrown);
                    }
                });
                e.preventDefault();
            });
            // do something
        }
        setTimeout(FetchData, 3000);

        function formatRepo(repo) {
            if (repo.loading) {
                return repo.text;
            }
            return "<div class='row slt-srs m-0'><div class='col-md-10 sc-con pl-3 pr-1'><span class='scsm-gp'><span class='sc-code lft'>" + repo.caption + "</span></span></div></div>";
        }

        function formatRepoSelection(repo) {
            if (repo.id === '') {
                return "<?php echo get_msg('attributes')?>...";
            }
            return repo.caption;
        }
    });

</script>