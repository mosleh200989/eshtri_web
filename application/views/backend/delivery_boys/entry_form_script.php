<script>

	<?php if ( $this->config->item( 'client_side_validation' ) == true ): ?>
	
	function jqvalidate() {
		$('#deliveryboy-form').validate({
			rules:{
				user_name:{
					required: true,
					minlength: 4
				},
				user_email:{
					required: true,
					email: true,
					remote: '<?php echo $module_site_url ."/ajx_exists/". @$user->user_id ; ?>'
				},
				<?php if ( !isset( $user )): ?>
				user_password:{
					required: true,
					minlength: 4
				},
				conf_password:{
					required: true,
					equalTo: '#user_password'
				},
				<?php endif; ?>
				commission:{
					required: true,
				},
				user_phone:{
					required: true,
					minlength: 10,
					maxlength: 10,
				},
			},
			messages:{
				user_name:{
					required: "<?php echo get_msg( 'err_user_name_blank' ); ?>",
					minlength: "<?php echo get_msg( 'err_user_name_len' ); ?>"
				},
				user_phone:{
					required: "<?php echo get_msg( 'err_user_phone_blank' ); ?>",
					minlength: "<?php echo get_msg( 'err_user_phone_len' ); ?>",
					maxlength: "<?php echo get_msg( 'err_user_phone_len' ); ?>"
				},
				user_email:{
					required: "<?php echo get_msg( 'err_user_email_blank' ); ?>",
					email: "<?php echo get_msg( 'err_user_email_invalid' ); ?>",
					remote: "<?php echo get_msg( 'err_user_email_exist' ); ?>"
				},
				commission:{
					required: "<?php echo get_msg( 'err_user_commission_blank' ); ?>",
				},
				<?php if ( !isset( $user )): ?>
				user_password:{
					required: "<?php echo get_msg( 'err_user_pass_blank' ); ?>",
					minlength: "<?php echo get_msg( 'err_user_pass_len' ); ?>"
				},
				conf_password:{
					required: "<?php echo get_msg( 'err_user_pass_conf_blank' ); ?>",
					equalTo: "<?php echo get_msg( 'err_user_pass_conf_not_match' ); ?>"
				},
				<?php endif; ?>
			}
		});
	}

	$(document).ready(function() {
		$("#contract_type").change(function() {
			if($(this).val()=="DeliveryBoy"){
				$("#lbl_eShtri_commission").show();
				$("#lbl_employee_commission").hide();
			}else{
				$("#lbl_eShtri_commission").hide();
				$("#lbl_employee_commission").show();
			}
		});
	});	
	<?php endif; ?>

</script>
