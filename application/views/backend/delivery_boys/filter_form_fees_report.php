<form class="form-horizontal form-body-md col-md-12 column-two-form common-form report-form-area" id="defaultFormRpt">
    <div class="column-two-form-down report-form">
        <div class="row crowmlr-8">
            <div class="col-md-12 column-one p-1">
                <div class="panel panel-default">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"><?php echo get_msg('order_date')?> :</label>

                                    <div class="col-sm-8 fmi_box input-daterange input-group pickDate">
                                        <input type="text" id="orderStartDateRpt" name="orderStartDateRpt" class="form-control input-sm" autocomplete="off" onkeydown="return false" />
                                        <span class="input-group-addon"><?php echo get_msg('to')?></span>
                                        <input type="text" id="orderEndDateToRpt" name="orderStartDateRpt" class="form-control input-sm" autocomplete="off" onkeydown="return false" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"><?php echo get_msg('delivery_date')?> :</label>

                                    <div class="col-sm-8 fmi_box input-daterange input-group pickDate">
                                        <input type="text" id="deliveryStartDateRpt" name="deliveryStartDateRpt" class="form-control input-sm" autocomplete="off" onkeydown="return false" />
                                        <span class="input-group-addon"><?php echo get_msg('to')?></span>
                                        <input type="text" id="deliveryEndDateRpt" name="deliveryEndDateRpt" class="form-control input-sm" autocomplete="off" onkeydown="return false" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"><?php echo get_msg('invoice_no')?> :</label>
                                    <div class="col-sm-8 fmi_box">
                                        <input type="text" id="invoiceNoRpt" name="invoiceNo" class="form-control input-sm" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"><?php echo get_msg('customer_mobile')?> :</label>
                                    <div class="col-sm-8 fmi_box">
                                        <input type="text" id="customerMobileRpt" name="customerMobileRpt" class="form-control input-sm" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"><?php echo get_msg('status_label')?> :</label>
                                    <div class="col-sm-8 fmi_box">
                                      <select class="form-control selectTwo select2 input-sm" name="transStatusRpt" id="transStatusRpt">
                                      <option value=""><?php echo get_msg('status_label')?></option>
                                        <?php
                                        foreach ($this->Transactionstatus->get_all()->result() as $status) {
                                            echo '<option value="'.$status->id.'">'.get_msg($status->title).'</option>';
                                        }
                                        ?>
                                      </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"><?php echo get_msg('rejection')?> :</label>
                                    <div class="col-sm-8 fmi_box">
                                        <select class="form-control selectTwo select2 input-sm" name="rejectionRpt" id="rejectionRpt">
                                            <?php
                                            echo '<option value="0">'.get_msg("not_rejected").'</option>';
                                            echo '<option value="1">'.get_msg("rejected").'</option>';
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"><?php echo get_msg('payment_method')?> :</label>
                                    <div class="col-sm-8 fmi_box">
                                        <select class="form-control selectTwo select2 input-sm" name="paymentMethodRpt" id="paymentMethodRpt">
                                            <option value=""><?php echo get_msg('payment_method')?></option>
                                            <?php
                                                echo '<option value="paytabs">'.get_msg("paytabs").'</option>';
                                                echo '<option value="POSTerminal">'.get_msg("POSTerminal").'</option>';
                                                echo '<option value="COD">'.get_msg("COD").'</option>';
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"><?php echo get_msg('payment_status')?> :</label>
                                    <div class="col-sm-8 fmi_box">
                                        <select class="form-control selectTwo select2 input-sm" name="paymentStatusRpt" id="paymentStatusRpt">
                                            <option value=""><?php echo get_msg('payment_status')?></option>
                                            <?php
                                            echo '<option value="1">'.get_msg("paid").'</option>';
                                            echo '<option value="0">'.get_msg("unpaid").'</option>';
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"><?php echo get_msg('Delivery_Boy')?>:</label>
                                    <div class="col-sm-8 fmi_box">
                                      <select class="form-control input-sm" name="delivery_boyRpt" id="delivery_boyRpt">
                                      <option value=""><?php echo get_msg('Delivery_Boy')?></option>
                                      </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"><?php echo get_msg('shop')?>:</label>
                                    <div class="col-sm-8 fmi_box">
                                        <select class="form-control select2 selectTwo input-sm" name="shopRpt" id="shopRpt">
                                            <option value=""><?php echo get_msg('shop')?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row crowmlr-8">
            <div class="col-md-12 p-1">
                <div class="panel panel-default mb-0 report-btn">
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary mb-0" data-toggle="tooltip" id="show-btn"><?php echo get_msg('show')?>...<i class="fa fa-recycle"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
