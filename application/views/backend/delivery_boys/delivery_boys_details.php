<style>
  html,
body,
#map {
  width: 100%;
  height: 100%;
}
</style>
<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjp5OfeOhMfejWHUjy0TVtdwhTlc_NYxs"
      
    ></script>
    <!-- <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>  -->
<script>
  var map = null;
var infowindow = new google.maps.InfoWindow();
var bounds = new google.maps.LatLngBounds();

//The list of points to be connected
var markers = [
<?php 
         $prev_lat="";
         $prev_lng="";
        foreach ($user_locations as $location) {
            if($prev_lat != $location->latitude){
                print "{ lat: ".$location->latitude.", lng: ".$location->longitude.", title: '".$location->created_at."' },";
            }
           
            $prev_lat=$location->latitude;
            $prev_lng=$location->longitude;
        }
        ?>
        ];


//    var map;
function initialize() {
  var mapOptions = {
    center: new google.maps.LatLng(
      parseFloat(markers[0].lat),
      parseFloat(markers[0].lng)),
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var service = new google.maps.DirectionsService();

  var infoWindow = new google.maps.InfoWindow();
  map = new google.maps.Map(document.getElementById("map"), mapOptions);
  var lat_lng = new Array();

  var marker = new google.maps.Marker({
    position: map.getCenter(),
    map: map,
    draggable: false
  });
  bounds.extend(marker.getPosition());
  // google.maps.event.addListener(marker, "click", function(evt) {
  //   infowindow.setContent("coord:" + marker.getPosition().toUrlValue(6));
  //   infowindow.open(map, marker);
  // });
  for (var i = 0; i < markers.length; i++) {
    if ((i + 1) < markers.length) {
      var src = new google.maps.LatLng(parseFloat(markers[i].lat),
        parseFloat(markers[i].lng));
      createMarker(src, markers[i].title);

      var des = new google.maps.LatLng(parseFloat(markers[i + 1].lat),
        parseFloat(markers[i + 1].lng));
      createMarker(des, markers[i + 1].title);
      //  poly.setPath(path);
      service.route({
        origin: src,
        destination: des,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
      }, function(result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          var path = new google.maps.MVCArray();
          var poly = new google.maps.Polyline({
            map: map,
            strokeColor: '#F3443C'
          });
          for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
            path.push(result.routes[0].overview_path[i]);
          }
          poly.setPath(path);
          map.fitBounds(bounds);
        }
      });
    }
  }
}


function createMarker(latLng, title) {
  var marker = new google.maps.Marker({
    position: latLng,
    map: map,
    draggable: false,
    title:title
  });
  bounds.extend(marker.getPosition());
  marker['infowindow'] = new google.maps.InfoWindow({
   content: title
  });
  google.maps.event.addListener(marker, 'mouseover', function() {
        this['infowindow'].open(map, this);
    });
  // google.maps.event.addListener(marker, "click", function(evt) {
  //   infowindow.setContent("coord:" + this.getPosition().toUrlValue(6));
  //   infowindow.open(map, this);
  // });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div class="container-fluid" style="padding: 10px 30px 10px 30px;">
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div id="map"></div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    #map{height: 500px; max-width: 600}
</style>