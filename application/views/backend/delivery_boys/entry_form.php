<?php
	$attributes = array('id' => 'deliveryboy-form');
	echo form_open( '', $attributes );
?>

<div class="container-fluid">
	<div class="col-12"  style="padding: 30px 20px 20px 20px;">
		<?php flash_msg(); ?>
		<div class="card earning-widget">
		    <div class="card-header" style="border-top: 2px solid red;">
		        <h3 class="card-title"><?php echo get_msg('user_info')?></h3>
		    </div>

		<div id="perm_err" class="alert alert-danger fade in" style="display: none">
			<label for="permissions[]" class="error"></label>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		</div>
			
		<div class="card-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('user_name')?></label>

						<?php echo form_input(array(
							'name' => 'user_name',
							'value' => set_value( 'user_name', show_data( @$user->user_name ), false ),
							'class' => 'form-control form-control-sm',
							'placeholder' => get_msg('user_name'),
							'id' => 'name'
						)); ?>

					</div>
					
					<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('user_email')?></label>

						<?php echo form_input(array(
							'name' => 'user_email',
							'value' => set_value( 'user_email', show_data( @$user->user_email ), false ),
							'class' => 'form-control form-control-sm',
							'placeholder' => get_msg('user_email'),
							'id' => 'user_email'
						)); ?>

					</div>
					<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('user_phone')?></label>

						<?php echo form_input(array(
							'name' => 'user_phone',
							'value' => set_value( 'user_phone', show_data( @$user->user_phone ), false ),
							'class' => 'form-control form-control-sm',
							'placeholder' => get_msg('user_phone'),
							'id' => 'user_phone'
						)); ?>

					</div>
					<?php if ( @$user->user_is_sys_admin == false ): ?>

					<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('user_password')?></label>

						<?php echo form_input(array(
							'type' => 'password',
							'name' => 'user_password',
							'value' => set_value( 'user_password' ),
							'class' => 'form-control form-control-sm',
							'placeholder' => get_msg('user_password'),
							'id' => 'user_password'
						)); ?>
					</div>
								
					<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('conf_password')?></label>
						
						<?php echo form_input(array(
							'type' => 'password',
							'name' => 'conf_password',
							'value' => set_value( 'conf_password' ),
							'class' => 'form-control form-control-sm',
							'placeholder' => get_msg('conf_password'),
							'id' => 'conf_password'
						)); ?>
					</div>
					
					<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('role_label')?></label>

						<?php 
							$options = array();
							foreach($this->Role->get_all()->result() as $role) {
								if($role->role_id==5){
									$options[$role->role_id] = $role->role_desc;
								}
								
							}

							echo form_dropdown(
								'role_id',
								$options,
								set_value(5),
								'class="form-control form-control-sm" id="role_id" disabled'
							);
						?>
					</div>

					<div class="form-group">
						<label> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('contract_type_label')?></label>

						<?php 
							$options = array();
							$options["DeliveryBoy"] = get_msg('non_employee_label');
							$options["Employee"] = get_msg('employee_label');
								

							echo form_dropdown(
								'contract_type',
								$options,
								set_value( 'contract_type', show_data( @$user->contract_type ), false ),
								'class="form-control form-control-sm" id="contract_type"'
							);
						?>
					</div>
						<div class="form-group">
						<label id="lbl_eShtri_commission"> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('eShtri_commission')?></label>
						<label id="lbl_employee_commission" style="display: none"> <span style="font-size: 17px; color: red;">*</span>
							<?php echo get_msg('employee_commission')?></label>

						<?php echo form_input(array(
							'name' => 'commission',
							'value' => set_value( 'commission', show_data( @$user->commission ), false ),
							'class' => 'form-control form-control-sm',
							'placeholder' => get_msg('commission'),
							'type' => 'number',
							'id' => 'commission'
						)); ?>

					</div>

					<?php endif; ?>
				</div>
			


			</div>
		</div>
		
		<div class="card-footer">
	        <button type="submit" class="btn btn-sm btn-primary">
				<?php echo get_msg('btn_save')?>
			</button>

			<a href="<?php echo $module_site_url; ?>" class="btn btn-sm btn-primary">
				<?php echo get_msg('btn_cancel')?>
			</a>
	    </div>
	       
	</div>
    <!-- card info -->
	</div>
</div>

<?php echo form_close(); ?>

