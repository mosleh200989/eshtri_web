
    <div class="modal fade common-form" id="accVcrMstModalFormId">
        <div class="modal-dialog modal-xxl">
            <div class="modal-content form-bg">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="modal-area">
                        <form class="form-horizontal form-body-md col-md-12 column-two-form common-form"
                              id="defaultForm">
                            <div class="text-center panel panel-default mb-0 crowlr-4">
                                <h3 id="headerTitle" class="m-0 p-1"><?php echo get_msg('expense_voucher')?></h3>
                            </div>

                            <div id="defaultMsgDiv"></div>

                            <div class="column-two-form-down">

                                <div class="row crowmlr-8">
                                    <input type="hidden" id="hiddenUpId" name="id"/>

                                    <div class="col-md-6 column-two p-1">
                                        <div class="panel panel-default">
                                            <fieldset>
                                                <legend><?php echo get_msg('expense_voucher')?></legend>

                                                <div class="form-group">
                                                    <!-- <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('code')?>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                            <input type="text" id="code" name="code" readonly
                                                                   class="form-control input-sm">
                                                        </div>
                                                    </div> -->

                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('acc_period')?><span
                                                                class="text-warning">*</span> : &nbsp</label>
                                                        <div class="col-sm-8 fmi_box">
                                                        
                                                            <input type="hidden" id="accPeriod" name="acc_period_id"
                                                                   value="<?php echo $currentPeriod['optionKey'];?>">
                                                            <input type="text" id="accPeriodText" name="acc_period_text"
                                                                   value="<?php echo $currentPeriod['periodStartDt'].' - '.$currentPeriod['periodCloseDt'];?>" readonly
                                                                   class="form-control input-sm">
                                                        </div>
                                                    </div>

                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('trans_date')?><span
                                                                class="text-warning">*</span> :</label>
                                                        <div class="col-sm-8 fmi_box">
                                                                   <input type="text" id="transDate" name="trans_date" class="form-control pickDate input-sm" autocomplete="off" onkeydown="return false">
                                                        </div>
                                                    </div>

                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('bearer_type')?>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                            <select class="form-control selectTwo select2 input-sm" name="stake_holder_type" id="stakeHolderType">
                                                                <option value=""><?php echo get_msg('bearer_type')?></option>
                                                                <?php
                                                                foreach ($stakeHolderTypeList as $item)
                                                                {
                                                                echo '<option value="'.$item.'">'.get_msg($item).'</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('stakeholder')?>: &nbsp</label>
                                                        <div class="col-sm-8 fmi_box">
                                                            <select class="form-control input-sm" name="stakeholder_id" id="stakeholder">
                                                               
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('reference_code')?>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                            <input type="text" id="referenceCode" name="reference_code"
                                                                   class="form-control input-sm">
                                                        </div>
                                                    </div>

                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>

                                    <div class="col-md-6 column-two p-1">
                                        <div class="panel panel-default">
                                            <fieldset>
                                                <legend><?php echo get_msg('expense_voucher')?></legend>

                                                <div class="form-group">

                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('acc_coa')?>
                                                          <span class="text-warning">*</span> : &nbsp
                                                        </label>

                                                        <div class="col-sm-8 fmi_box">
                                                            <select class="form-control selectTwo select2 input-sm" name="acc_coa" id="accCoa">
                                                            <option value=""><?php echo get_msg('acc_coa')?></option>
                                                                <?php
                                                                foreach ($coaOptionGroup as $coa)
                                                                {
                                                                echo '<option value="'.$coa->coa_oid.'">'.$coa->code.'-'.$coa->caption.'</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('cheque_card_no')?>:</label>

                                                        <div class="col-sm-8">
                                                            <input type="text" id="chequeNo" name="cheque_no" class="form-control input-sm">
                                                        </div>
                                                    </div>

                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('cheque_date')?>:</label>

                                                        <div class="col-sm-8">
                                                            <input type="text" id="chequeDate" name="cheque_date" class="form-control pickDate input-sm" autocomplete="off" onkeydown="return false">
                                                        </div>
                                                    </div>

                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label">
                                                            <?php echo get_msg('narration')?>:
                                                        </label>

                                                        <div class="col-sm-8 fmi_box">
                                                            <textarea id="narration" name="narration"
                                                              class="form-control input-sm"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>

                          <?php include 'dtls_form.php';?>

                            <div class="row crowmlr-8">
                                <div class="col-md-12 p-1">
                                    <div class="panel panel-default mb-0">
                                        <div class="text-right p-2">
                                            <button type="button" class="btn btn-default" data-toggle="tooltip"
                                                    id="reset-btn"><?php echo get_msg('reset')?></button>
                                            <button type="submit" class="btn btn-primary" data-toggle="tooltip"
                                                    id="save-btn"><?php echo get_msg('save')?>... <i
                                                    class="fa fa-recycle"></i></button>
                                            <button type="button" class="btn btn-warning cancel-btn"
                                                    data-dismiss="modal"><?php echo get_msg('cancel')?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
