
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/jsTree/jstree.min.css'); ?>">
    <script src="<?php echo base_url( 'assets/plugins/jsTree/jstree.min.js' ); ?>"></script>
<div class="container-fluid" style="padding: 10px 30px 10px 30px;">
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <?php 
// echo "<pre>";
// print_r($accCoa);
// echo "</pre>";
        ?>
        <div class="row">
            <div class="col-4">
                <div class="ibox cc-box">
                    <div class="ibox-content">
                        <div class="tabs-containers">
                            <div class="panel-body p-10-over">
                                <div class="form-body-md col-md-12">
                                    <div class="coa-tree ">
                                        <div class="row m-0">
                                            <div class="col-md-4 btn btn-primary btn-outline">
                                                <h4 class="text-center"><?php echo get_msg('chartTreeView')?> <i
                                                        class="fa fa-cog fa-spin"></i>
                                                </h4>

                                                <div class="col-expand-btn">
                                                <?php
                                                if($rows_count){ ?>
                                                        <input class="form-group-sm btn btn-info" type="button"
                                                            value="<?php echo get_msg('collapse')?>"
                                                            onclick="$('#menuTree').jstree('close_all');">
                                                        <input class="form-group-sm btn btn-primary " type="button"
                                                            value="<?php echo get_msg('expand')?>"
                                                            onclick="$('#menuTree').jstree('open_all');">
                                                <?php }else{ ?>
                                                        <input class="form-group-sm btn btn-warning" type="button"
                                                            value="<?php echo get_msg('create_chart_of_account')?>"
                                                            id="createCoa">
                                                <?php } ?>
                                                </div>
                                            </div>

                                            <div class="col-md-8 btn btn-primary btn-outline">
                                                <div class="row">
                                                    <div class="col-sm-12 control-label">
                                                        <h4><?php echo get_msg('search')?></h4>
                                                    </div>

                                                    <div class="col-sm-12 fmi_box">
                                                        <input type="text" id="menuTree_q" name="menuTree_q"
                                                            class="form-control input-sm">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div id="menuTree">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'entry_form.php';?>
 <?php include 'acc_coa_view.php';?>
<script>
        $(document).ready(function () {
            $(".onlybank").hide();
            var saveBtn = $('#save-btn');
            var resetBtn = $('#reset-btn');
            var listTable = $('#list-table');
            var defaultForm = $('#defaultForm');
            resetFormByBtn(resetBtn, defaultForm, "<?php echo get_msg('chart_of_accounts')?>", "<?php echo get_msg('save')?>");
            resetBtn.click(function () {
                resetMasterForm();
                // resetDetailForm();
            });

            var tree = $("#menuTree");
            tree.jstree({
                'core': {
                    'data': {
                        "url": "<?php echo site_url('/admin/chart_of_accounts/listpagination');?>?lazy",
                        "data": function (node) {
                            return {"listAction": true, "id": node.id};
                        },
                        "dataType": "json" // needed only if you do not supply JSON headers
                    }
                },
                'plugins': ["contextmenu", 'types', 'dnd', "search"],
                'types': {
                    'default': {
                        'icon': 'fa fa-folder text-navy'
                    }
                }
            }).delegate(".viewData, .addData, .editData, .deleteData", "click", function (e) {
                var referenceId = $(this).parents('li').attr('id');
                var bodyText = $(this).parents("a").text().trim();
                var refClass = "listRef";

                if ($(this).hasClass('viewData')) {
                    viewData(referenceId);
                } else if ($(this).hasClass('addData')) {
                    addData(referenceId);
                } else if ($(this).hasClass('editData')) {
                    editData(referenceId);
                } else if ($(this).hasClass('deleteData')) {
                    var confirmDel = confirm("Are you sure want to delete ??");
                    if (confirmDel == true) {
                        deleteData(referenceId);
                    }
                }
            });
            ;
            var to = false;
            $('#menuTree_q').keyup(function () {
                if (to) {
                    clearTimeout(to);
                }
                to = setTimeout(function () {
                    var v = $('#menuTree_q').val();
                    tree.jstree(true).search(v);
                }, 250);
            });

            defaultForm.validate({
                lang: langCode,
                ignore: [],
                rules: {
                    codePrefix: {
                        required: true,
                        number : tree
                    },
                    codeSuffix: {
                        required: true,
                        number : tree
                    },
                    accCoaFlag: {
                        required: true
                    }
                },
                submitHandler: function (form) {
                    var url;
                    if ($('#hiddenUpId').val() == '') {
                        url = "<?php echo site_url('/admin/chart_of_accounts/write');?>";
                    } else {
                        url = "<?php echo site_url('/admin/chart_of_accounts/editsubmit');?>";
                    }
                    formAfterSubmit(saveBtn, "<?php echo get_msg('processing')?>")
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: defaultForm.serialize(),
                        success: function (data) {
                            formSubmitReturnMsg(saveBtn, data, listTable, defaultForm, "<?php echo get_msg('chart_of_accounts')?>", "<?php echo get_msg('save')?>");
                            if (data.isError == true) {
                                // $('#menuTree').jstree(true).refresh();
                                $('#menuTree').jstree(true).reload(true);
                            }
                        },
                        failure: function (data) {
                        }
                    })
                }
            });


            $(".reload-accCoa").on("click", function (e) {
                jQuery.ajax({
                    type: 'GET',
                    dataType: 'JSON',
                    data: {reloadaccCoa: 'true'},
                    url: "<?php echo site_url('/admin/chart_of_accounts');?>",
                    success: function (data, textStatus) {
                       // console.log(data);
                        // if (data.isError == false) {
                        //     $("#accCoa").select2("destroy");
                        //     $("#accCoa option").remove();
                        //     $("#accCoa").append($("<option></option>").attr("value", '').text("<?php echo get_msg('accCoa')?>"));
                        //     $.each(data.obj, function (i, item) {
                        //         $("#accCoa")
                        //             .append($("<option></option>")
                        //                 .attr("value", item.optionKey)
                        //                 .text(item.optionValue));
                        //     });
                        //     $("#accCoa").select2();
                        // }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        serverErrorToast(errorThrown);
                    }
                });
                e.preventDefault();
            });

            $("#createCoa").on("click", function (e) {
                // jQuery.ajax({
                //     type: 'POST',
                //     dataType: 'JSON',
                //     data: {createCoa: 'true'},
                //     url: "<?php echo site_url('/admin/chart_of_accounts');?>",
                //     success: function (data, textStatus) {
                //         showToastMessageOnly(data.isError, data.message);
                //         setTimeout('locationReload()', 2000);
                //     },
                //     error: function (XMLHttpRequest, textStatus, errorThrown) {
                //         serverErrorToast(errorThrown);
                //     }
                // });
                e.preventDefault();
            });

        });

        function resetMasterForm() {
            $("#accCoa").select2('val', '');
        }

        function addData(referenceId) {
            jQuery.ajax({
                type: 'GET',
                dataType: 'JSON',
                data: {addAction: true, id: referenceId},
                url: "<?php echo site_url('/admin/chart_of_accounts');?>",
                success: function (data, textStatus) {
                    if (data.isError == false) {
                        $('#defaultMsgDiv').html('');
                        $('#hiddenUpId').val('');
                        // $("#caption").val("");
                        // $("#captionAlt").val("");
                        $("#remark").val("");
                        $("#accCoa").val(data.defaultInstance.id);
                        // $("#accCoaText").val(data.obj.defaultInstance.caption);
                        $("#accCoaText").val(data.caption);
                        $("#accCoaFlag").val(data.defaultInstance.acc_coa_flag);
                        $("#accCoaFlagText").val(data.accCoaFlagText);
                        $("#codePrefix").val(data.codePrefix);
                        $("#tableAdd > tbody").html("");
                        var rowNumber =1;
                        $.each(data.language, function (key, value) {
                            addDetailInAddTable(rowNumber, value.language_enum, value.language_enum);
                            rowNumber = rowNumber + 1;
                        });

                        $("#accCoaModalFormId").modal('show');
                    } else {
                        alert(data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    serverErrorToast(errorThrown);
                }
            });
        }

        function editData(referenceId) {
            jQuery.ajax({
                type: 'GET',
                dataType: 'JSON',
                data: {editData: true, id: referenceId},
                url: "<?php echo site_url('/admin/chart_of_accounts');?>",
                success: function (data, textStatus) {
                    console.log(data);
                    if (data.isError == false) {
                        $('#defaultMsgDiv').html('');
                        $('#hiddenUpId').val(data.defaultInstance.id);
                        // $("#caption").val(data.obj.defaultInstance.caption);
                        $("#codePrefix").val(''+data.codePrefix);
                        $("#codeSuffix").val(''+data.codeSuffix);
                        // $("#captionAlt").val(data.obj.defaultInstance.captionAlt);
                        $("#remark").val(data.defaultInstance.remark);
                        $("#accCoa").val(data.defaultInstance.acc_coa_id);
                        // $("#accCoaText").val(data.obj.defaultInstance.accCoaText);
                        $("#accCoaFlag").val(data.defaultInstance.acc_coa_flag);
                        $("#journalsType").val(data.journalsType);
                        if(data.journalsType=="Bank"){
                            $(".onlybank").show();
                        }else{
                            $(".onlybank").hide();
                        }
                        if(data.journalsType=="Bank" && data.defaultInstance.is_card_default==true){
                            $("#isCardTrue").prop("checked", true);
                        }else{
                            $("#isCardFalse").prop("checked", true);
                        }
                        $("#accCoaFlagText").val(data.accCoaFlagText);
                        $("#accCoaText").val(data.defaultInstance.caption);
                        $("#tableAdd > tbody").html("");
                        var rowNumber =1;
                        $.each(data.language, function (key, value) {
                            editDetailInAddTable(value.id, rowNumber, value.language_enum, value.language_enum, value.caption);
                            rowNumber = rowNumber + 1;
                        });

                        $("#accCoaModalFormId").modal('show');
                    } else {
                        alert(data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    serverErrorToast(errorThrown);
                }
            });
        }

        function deleteData(referenceId) {
            jQuery.ajax({
                type: 'POST',
                dataType: 'JSON',
                data: {deleteClass: 'mainClass', referenceClass: 'delete-reference', id: referenceId},
                url: "<?php echo site_url('/admin/chart_of_accounts/remove');?>",
                success: function (data, textStatus) {
                    if (data.isError == false) {
                        $('#menuTree').jstree(true).refresh();
                        $('#headerTitle').html('Data remove successfully...');
                    } else {
                        alert(data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    serverErrorToast(errorThrown);
                }
            });
        }

        function viewData(referenceId) {
            jQuery.ajax({
                type: 'GET',
                dataType: 'JSON',
                data: {showAction: true, id: referenceId},
                url: "<?php echo site_url('/admin/chart_of_accounts');?>",
                success: function (data, textStatus) {
                    if (data.isError == false) {
                        $("#view-code").html("<strong>" + (data.defaultInstance.code != null) ? (data.defaultInstance.code) : '' + "</strong>");
                        $("#view-caption").html("<strong>" + (data.caption != null) ? (data.caption) : '' + "</strong>");
                        $("#view-captionAlt").html("<strong>" + (data.defaultInstance.caption_alt != null) ? (data.defaultInstance.caption_alt) : '' + "</strong>");
                        $("#view-accCoaFlag").html("<strong>" + (data.defaultInstance.acc_coa_flag != null ? data.accCoaFlag : '') + "</strong>");
                        $("#view-isTaxable").html("<strong>" + (data.defaultInstance.is_taxable != null) ? (data.defaultInstance.is_taxable) : '' + "</strong>");
                        $("#view-accCoa").html("<strong>" + (data.defaultInstance.acc_coa_id != null ? data.accCoa : '') + "</strong>");
                        $("#view-journalsType").html("<strong>" + (data.defaultInstance.journals_type != null ? data.journalsType : '') + "</strong>");
                        $("#view-remark").html("<strong>" + (data.defaultInstance.remark != null) ? (data.defaultInstance.remark) : '' + "</strong>");
                        $('#view-activeStatus').html('<h5>' + (data.defaultInstance.active_status ? data.defaultInstance.active_status : '') + '</h5>');
                        $('#accCoaModalViewId').modal('show');
                    } else {
                        alert(data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    serverErrorToast(errorThrown);
                }
            });
        }

        function addDetailInAddTable(rowOne, rowTwo, rowThree) {
            var languageEnumCol = '<input type="hidden"  name="languageEnum[' + rowOne + ']" class="form-control input-sm languageEnum" value="' + rowTwo + '">';
            var captionCol = '<input type="text"  name="caption[' + rowOne + ']" class="form-control input-sm caption" value="">';
            var actionCol = '<div class="btn-group"><button class="btn btn-white deleteDtlBtn" ><i class="fa fa-floppy-o text-navy"></i></button>';
            $('#tableAdd > tbody').prepend('<tr><td>' + rowOne + languageEnumCol + '</td><td>' + rowThree + '</td><td>' + captionCol + '</td><td>' + actionCol + '</td></tr>');
        }

        function editDetailInAddTable(id,rowOne, rowTwo, rowThree, rowFour) {
            var languageEnumCol = '<input type="hidden"  name="eidtid[' + rowOne + ']" value="' + id + '"><input type="hidden"  name="languageEnum[' + rowOne + ']" class="form-control input-sm languageEnum" value="' + rowTwo + '">';
            var captionCol = '<input type="text"  name="caption[' + rowOne + ']" class="form-control input-sm caption" value="'+rowFour+'">';
            var actionCol = '<div class="btn-group"><button class="btn btn-white deleteDtlBtn" ><i class="fa fa-floppy-o text-navy"></i></button>';
            $('#tableAdd > tbody').prepend('<tr><td>' + rowOne + languageEnumCol + '</td><td>' + rowThree + '</td><td>' + captionCol + '</td><td>' + actionCol + '</td></tr>');
        }

    </script>
    <style>
    .wrapper .jstree-default .jstree-closed > .jstree-ocl {
        background-position: -132px -16px;
        top: 3px;
        left: 4px;
    }
    .wrapper .jstree-default .jstree-icon:empty {
        width: 13px;
        height: 15px;
        position: relative;
    }
    </style>