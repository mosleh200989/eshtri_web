<div class="modal fade common-form" id="accCoaModalFormId">
        <div class="modal-dialog modal-md">
            <div class="modal-content form-bg">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="modal-area">
                        <form class="form-horizontal form-body-md col-md-12 column-one-form common-form"
                              id="defaultForm">
                            <div class="text-center panel panel-default mb-0 crowlr-4">
                                <h3 id="headerTitle" class="m-0 p-1"><?php echo get_msg('chart_of_accounts')?></h3>
                            </div>

                            <div id="defaultMsgDiv"></div>

                            <div class="column-one-form-down">

                                <div class="row crowmlr-8">
                                    <input type="hidden" id="hiddenUpId" name="id"/>

                                    <div class="col-md-12 column-one p-1">
                                        <div class="panel panel-default">
                                            <fieldset>
                                                <legend><?php echo get_msg('chart_of_accounts')?></legend>

                                                <div class="form-group">
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('code')?>  :</label>

                                                        <div class="col-sm-4 fmi_box">
                                                            <input type="text" id="codePrefix" minlength="4" maxlength="4" name="codePrefix" readonly class="form-control input-sm">
                                                        </div>
                                                        <div class="col-sm-4 fmi_box">
                                                            <input type="text" id="codeSuffix" minlength="3" maxlength="3" name="codeSuffix" class="form-control input-sm">
                                                        </div>
                                                    </div>


                                                    <div class="row mb-1"><label
                                                            class="col-sm-4 control-label"><?php echo get_msg('Acc_Coa_Flag')?> <span
                                                                class="text-warning">*</span> :</label>

                                                        <div class="col-sm-8 fmi_box">
                                                            <input type="hidden" id="accCoaFlag" name="accCoaFlag">
                                                            <input type="text" readonly id="accCoaFlagText" class="form-control input-sm">
                                                        </div>
                                                    </div>

                                                    <div class="row mb-1"><label
                                                            class="col-sm-4 control-label"><?php echo get_msg('journals_Type')?> <span
                                                                class="text-warning">*</span> :</label>

                                                        <div class="col-sm-8 fmi_box">
														<select class="form-control selectTwo input-sm" name="journals_type" id="journalsType">
                                                                <option value=""><?php echo get_msg('journalsType')?></option>
                                                                <?php
                                                                foreach ($journalsTypeList as $item)
                                                                {
                                                                echo '<option value="'.$item.'">'.get_msg($item).'</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('is_Taxable')?> <span
                                                                class="text-warning">*</span> :</label>

                                                        <div class="col-sm-8 fmi_box">
                                                            <label class="checkbox-inline i-checks"><input
                                                                    id="isTaxableTrue" type="radio" value="on"
                                                                    name="isTaxable">&nbsp;&nbsp;<?php echo get_msg('yes')?></label>
                                                            <label class="checkbox-inline i-checks"><input
                                                                    id="isTaxableFalse" checked type="radio" value="off"
                                                                    name="isTaxable">&nbsp;&nbsp;<?php echo get_msg('no')?></label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1 onlybank">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('isCreditCardDefault')?> <span
                                                                class="text-warning">*</span> :</label>

                                                        <div class="col-sm-8 fmi_box">
                                                            <label class="checkbox-inline i-checks"><input
                                                                    id="isCardTrue" type="radio" value="on"
                                                                    name="isCardDefault">&nbsp;&nbsp;<?php echo get_msg('yes')?></label>
                                                            <label class="checkbox-inline i-checks"><input
                                                                    id="isCardFalse" checked type="radio" value="off"
                                                                    name="isCardDefault">&nbsp;&nbsp;<?php echo get_msg('no')?></label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1"><label
                                                            class="col-sm-4 control-label"><?php echo get_msg('accCoa')?>  : &nbsp;
                                                             <!-- <a
                                                                href="#"
                                                                target="_blank"><i
                                                                    class="fa fa-plus-square create-accCoa"
                                                                    aria-hidden="true"></i></a>&nbsp; <a href="#"><i
                                                                class="fa fa-refresh reload-accCoa"
                                                                aria-hidden="true"></i></a> -->
                                                                </label>

                                                        <div class="col-sm-8 fmi_box">
                                                            <input type="hidden" id="accCoa" name="accCoa">
                                                            <input type="text" readonly id="accCoaText" class="form-control input-sm">
                                                        </div>
                                                    </div>

                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('remark')?>  :</label>

                                                        <div class="col-sm-8 fmi_box">
                                                            <textarea id="remark" name="remark"
                                                                      class="form-control input-sm"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>

                                <div class=" table-responsive">
                                    <table class="table  table-bordered" id="tableAdd">
                                        <thead>
                                        <tr>
                                            <th class="text-navy" style="width: 15%"><?php echo get_msg('serial')?></th>
                                            <th class="text-navy" style="width: 30%"><?php echo get_msg('languageName')?> :</th>
                                            <th class="text-navy" style="width: 40%"><?php echo get_msg('InputLanguage')?></th>
                                            <th class="text-navy" style="width: 15%"><?php echo get_msg('action')?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                            </div>


                            <div class="row crowmlr-8">
                                <div class="col-md-12 p-1">
                                    <div class="panel panel-default mb-0">
                                        <div class="text-right p-2">
                                            <button type="button" class="btn btn-default" data-toggle="tooltip"
                                                    id="reset-btn"><?php echo get_msg('reset')?></button>
                                            <button type="submit" class="btn btn-primary" data-toggle="tooltip"
                                                    id="save-btn"><?php echo get_msg('save')?>... <i
                                                    class="fa fa-recycle"></i></button>
                                            <button type="button" class="btn btn-warning"
                                                    data-dismiss="modal"><?php echo get_msg('cancel')?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>