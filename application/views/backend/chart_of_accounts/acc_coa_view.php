<div class="modal fade common-form" id="accCoaModalViewId">
    <div class="modal-dialog modal-md">
        <div class="modal-content form-bg">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">×</button>
            </div>

            <div class="modal-body">
                <div class="modal-area">
                    <div class="form-horizontal form-body-md col-md-12 column-one-form  common-form custom-form-view">
                        <div class="text-center panel panel-default mb-0 crowlr-4">
                            <h3 id="headerTitle" class="m-0 p-1"><?php echo get_msg('chart_of_accounts')?></h3>
                        </div>

                        <div id="defaultMsgDiv"></div>

                        <div class="column-one-form-down form-view">

                            <div class="row crowmlr-8">
                                <div class="col-md-12 column-one p-1">
                                    <div class="panel panel-default">
                                        <fieldset>
                                            <legend><?php echo get_msg('chart_of_accounts')?></legend>

                                            <div class="form-group">
                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span
                                                            class="vmiddle"><?php echo get_msg('code')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-code"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span
                                                            class="vmiddle"><?php echo get_msg('caption')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-caption"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span
                                                            class="vmiddle"><?php echo get_msg('captionAlt')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-captionAlt"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span
                                                            class="vmiddle"><?php echo get_msg('accCoaFlag')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-accCoaFlag"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span
                                                            class="vmiddle"><?php echo get_msg('journalsType')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-journalsType"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span
                                                            class="vmiddle"><?php echo get_msg('isTaxable')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-isTaxable"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span
                                                            class="vmiddle"><?php echo get_msg('accCoa')?>  :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-accCoa"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span
                                                            class="vmiddle"><?php echo get_msg('remark')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-remark"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row  crowmlr-8">
                            <div class="col-md-12 p-1">
                                <div class="panel panel-default mb-0">
                                    <div class="text-right p-2">
                                        <button type="button" class="btn btn-warning"
                                                data-dismiss="modal"><?php echo get_msg('cancel')?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
