<?php
	$attributes = array( 'id' => 'timeslot-form', 'enctype' => 'multipart/form-data');
	echo form_open( '', $attributes);
?>

<div class="container-fluid">
    <div class="col-sm-5"  style="padding: 30px 20px 20px 20px;">
    	<div class="card earning-widget">
	    	<div class="card-header" style="border-top: 2px solid red;">
	    		<h3 class="card-title"><?php echo get_msg('time_slots_number')?></h3>
			</div>	
	        <!-- /.card-header -->
	        <div class="card-body">
	            <div class="row">
	             	<div class="col-md-12">
	             		<div class="card earning-widget">
	             			<div class="card-header">
	             				<div class="row">
								<div class="col-md-6">
			            		<div class="form-group">
			                   		<label>
										<?php echo get_msg('total_slot')?>
										<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('total_slot')?>">
											<span class='glyphicon glyphicon-info-sign menu-icon'>
										</a>
									</label>

									<?php echo form_input( array(
										'name' => 'number_slots',
										'value' => set_value( 'number_slots', show_data( @$timeslotnumber->number_slots ), false ),
										'class' => 'form-control form-control-sm',
										'placeholder' => get_msg( 'total_slot' ),
										'id' => 'number_slots'
									)); ?>
			              		</div>
			              		</div>
			              		<div class="col-md-6">
			              		<div class="form-group">
			                   		<label>
										<?php echo get_msg('limit_order')?>
										<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('limit_order')?>">
											<span class='glyphicon glyphicon-info-sign menu-icon'>
										</a>
									</label>

									<?php echo form_input( array(
										'name' => 'limit_order',
										'value' => set_value( 'limit_order', show_data( @$timeslotnumber->limit_order ), false ),
										'class' => 'form-control form-control-sm',
										'placeholder' => get_msg( 'limit_order' ),
										'id' => 'limit_order'
									)); ?>
			              		</div>
			              		</div>
			              		</div>
	             			</div>
	             			<div class="card-body">
                                <div id="spec_data">
							<?php 
							
							$loopnumber=6;
							if($timeslotnumber->number_slots>0){
								$loopnumber=intval($timeslotnumber->number_slots);
							}
							
							$slotdata = $this->Timeslots->get_all_by(array("time_slot_number_id" => $timeslotnumber->id));?>




    <?php

    $spec_data = $slotdata->result();
    $specs_count = count($spec_data);

    //counter need to plus one for edit default
    $spec_data_count = $specs_count + 1;
    ?>




        <?php
        $i = 0;
        if (!empty($slotdata) && count($slotdata->result()) > 0): ?>
            <?php
			if (isset($spec_data)) {
                $specs_count = $specs_count;
            } else {
                $specs_count = 0;
            }
		?>
            <input type="hidden" id="slot_total_existing" name="slot_total_existing" value="<?php echo $specs_count; ?>">
       <?php
            foreach( $spec_data as $spec ):
            $i++;
            ?>


                <div class="input-daterangee">
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <?php echo get_msg('start_time') . " : " . $i?>
                        </label>

                        <?php echo form_input( array(
                            'name' => 'start_time' . $i,
                            'value' => $spec->start_time,
                            'class' => 'form-control pickTime form-control-sm',
                            'id' => 'start_time' . $i,
                        )); ?>

                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <?php echo get_msg('end_time') . " : "  .$i?>
                        </label>
                        <?php echo form_input( array(
                            'name' => 'end_time' . $i,
                            'value' => $spec->end_time,
                            'class' => 'form-control pickTime form-control-sm',
                            'placeholder' => get_msg('prd_specification'),
                            'id' => 'start_time' . $i,
                        )); ?>

                    </div>
                </div>
                </div>
                </div>

        <?php
            if($timeslotnumber->number_slots <=$i){
                break;
            }
            ?>
        <?php endforeach; ?>

                                <?php else: ?>
                                <?php for ($i=0; $i <$loopnumber ; $i++) {  ?>
                                <div class="input-daterangee">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>
                                                    <?php echo get_msg('start_time')?>
                                                    <a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('start_time')?>">
											<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                    </a>
                                                </label>
                                                <?php echo form_input( array(
                                                'name' => 'start_time'.$i,
                                                'class' => 'form-control pickTime form-control-sm',
                                                'autocomplete' => 'off',
                                                'onkeydown' => 'return false',
                                                'placeholder' => get_msg( 'start_time' ),
                                                'id' => 'start_time'.$i
                                                )); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>
                                                    <?php echo get_msg('end_time')?>
                                                    <a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('end_time')?>">
											<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                    </a>
                                                </label>

                                                <?php echo form_input( array(
                                                'name' => 'end_time'.$i,
                                                'class' => 'form-control pickTime form-control-sm',
                                                'autocomplete' => 'off',
                                                'onkeydown' => 'return false',
                                                'placeholder' => get_msg( 'end_time' ),
                                                'id' => 'end_time'.$i
                                                )); ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <?php } ?>
                                <?php endif; ?>
            </div>
	             			</div>
	             		</div>

	             		

							
	              		

	                </div>
	                <!-- col-md-6 -->
	            </div>
	            <!-- /.row -->
	        </div>
	        <!-- /.card-body -->

			<div class="card-footer">
	            <button type="submit" class="btn btn-sm btn-primary">
					<?php echo get_msg('btn_save')?>
				</button>

				<a href="<?php echo $module_site_url; ?>" class="btn btn-sm btn-primary">
					<?php echo get_msg('btn_cancel')?>
				</a>
	        </div>
	    </div>
       
    </div>
</div>
<!-- card info -->

<?php echo form_close(); ?>
<?php
	$this->load->view( $template_path .'/timeslot/entry_form_script');
?>