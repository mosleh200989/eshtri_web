<script>
function runAfterJQ() {
		// Publish Trigger
		$(document).delegate('.available','click',function(){
			
			// get button and id
			var btn = $(this);
			var id = $(this).attr('id');

			// Ajax Call to publish
			$.ajax({
				url: "<?php echo $module_site_url .'/ajx_available/'; ?>" + id,
				method: 'GET',
				success: function( msg ) {
					if ( msg == 'true' )
						btn.addClass('unavailable').addClass('btn-success')
							.removeClass('available').removeClass('btn-danger')
							.html("<?php echo get_msg( 'available' ); ?>");
					else
						alert( "<?php echo get_msg( 'err_available' ); ?>" );
				}
			});
		});
		
		// Unpublish Trigger
		$(document).delegate('.unavailable','click',function(){

			// get button and id
			var btn = $(this);
			var id = $(this).attr('id');
			// Ajax call to unpublish
			$.ajax({
				url: "<?php echo $module_site_url .'/ajx_unavailable/'; ?>" + id,
				method: 'GET',
				success: function( msg ){
					if ( msg == 'true' )
						btn.addClass('available').addClass('btn-danger')
							.removeClass('unavailable').removeClass('btn-success')
							.html("<?php echo get_msg( 'notavailable' ); ?>");
					else
						alert( "<?php echo get_msg( 'err_available' ); ?>" );
				}
			});

	});
}

</script>

<?php
	// Delete Confirm Message Modal
	$data = array(
		'title' => get_msg( 'delete_prd_label' ),
		'message' =>  get_msg( 'prd_yes_all_message' ),
		'no_only_btn' => get_msg( 'prd_no_only_label' )
	);
	
	$this->load->view( $template_path .'/components/delete_confirm_modal', $data );
?>