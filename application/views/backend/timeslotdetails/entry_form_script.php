<script>
	<?php if ( $this->config->item( 'client_side_validation' ) == true ): ?>

	function jqvalidate() {

		$('#product-form').validate({
			rules:{
				number_slots:{
					blankCheck : "",
					minlength: 1,
				},
				limit_order:{
					blankCheck : "",
					minlength: 1,
				},
				
		      	
			},
			messages:{
				number_slots:{
					blankCheck : "<?php echo get_msg( 'err_number_slots' ) ;?>",
					minlength: "<?php echo get_msg( 'err_number_slots_length' ) ;?>",
				},
				limit_order:{
					blankCheck : "<?php echo get_msg( 'err_limit_order' ) ;?>",
					minlength: "<?php echo get_msg( 'err_limit_order_len' ) ;?>"
				},
				
			    
			},

			submitHandler: function(form) {
		        if ($("#timeslot-form").valid()) {
		            form.submit();
		        }
		    }

		});
		
		jQuery.validator.addMethod("indexCheck",function( value, element ) {
			
			   if(value == 0) {
			    	return false;
			   } else {
			    	return true;
			   };
			   
		});
		jQuery.validator.addMethod("minCheck",function( value, element ) {
			
			   if(value <1 || value=='0') {
			    	return false;
			   } else {
			    	return true;
			   };
			   
		});
			jQuery.validator.addMethod("blankCheck",function( value, element ) {
			
			   if(value == "") {
			    	return false;
			   } else {
			   	 	return true;
			   }
		});

	}

	<?php endif; ?>
	function runAfterJQ() {

		// colorpicker
		$('.my-colorpicker2').colorpicker()

		// add input colorpicker and count colorpicker
     	$(document).ready(function () {

     		var edit_product_check = $('#edit_product').val();


     		if(edit_product_check == 0) {
     			//new product
     			var counter = 2;
     		} else {
     			//edit product
     			var counter =  parseInt($('#color_total_existing').val())+2;
     		}
     		$('#color_total_existing').val(counter);

      		$("#addColor").click(function () {
      			
				
      		 	var newTextBoxDiv = $(document.createElement('div'))
	     		.attr("id", "colorgroupfield"+counter)
	     		.attr("class", 'row');

	     		newTextBoxDiv.after().html(
	      		'<div class="col-md-6"><input placeholder="<?php echo get_msg( 'color_name' ) ;?>" class="form-control form-control-sm mt-1" type="text" name="color_name' + counter + 
	      		'" id="color_name' + counter + '" value="" ></div><div class="col-md-6"><div id="colorvalue' + counter + '" class="input-group my-colorpicker2 colorpicker-element"><input class="form-control form-control-sm mt-1" type="text" name="colorvalue' + counter + 
	      		'" id="colorvalue' + counter + '" placeholder="<?php echo get_msg( 'color_code' ) ;?>" value="" ><div class="input-group-addon mt-1"><i></i></div></div></div>');

	      		newTextBoxDiv.appendTo("#color-picker-group");
				$('#colorvalue'+counter).colorpicker({});
				counter++;

				$( ".CounterTextBoxDiv" ).remove();
				var newCounterTextBoxDiv = $(document.createElement('div'))
	     		.attr("id", 'CounterTextBoxDiv' + counter);

	     		newCounterTextBoxDiv.after().html(
	      		'<input type="hidden" name="color_total" id="color_total" value=" '+ counter +'" >');

	      		newCounterTextBoxDiv.appendTo(".my-colorpicker2");

	      		

      		});
      	});

        // add specification
        $(document).ready(function () {


                //edit product
                var counter =  parseInt($('#slot_total_existing').val())+2;
                var counter =  1;

            $('#slot_total_existing').val(counter);

            $('#number_slots').change(function () {
                $( "#spec_data" ).html("");
                var counter;
                var length =  parseInt($('#number_slots').val());
                for (counter = 1; counter <= length; counter++) {


                var newTextBoxDiv = $(document.createElement('div'))
                    .attr("class",'input-daterangee',"id", 'TextBoxDiv' + counter);

                newTextBoxDiv.after().html(
                    '<div class="row"><div class="col-md-6"><div class="form-group"><label><?php echo get_msg('start_time')?> : '+counter+'</label><input class="form-control pickTime form-control-sm" type="text" name="start_time' + counter +
                    '" id="start_time' + counter + '" value="" ></div></div><div class="col-md-6"><div class="form-group"><label><?php echo get_msg('end_time')?> : '+counter+'</label><input class="form-control pickTime form-control-sm" type="text" name="end_time' + counter +
                    '" id="end_time' + counter + '" ></div></div>');

                newTextBoxDiv.appendTo("#spec_data");
                //counter++;

                $( "#CounterTextBoxDiv" ).remove();
                var newCounterTextBoxDiv = $(document.createElement('div'))
                    .attr("id", 'CounterTextBoxDiv' + counter);

                newCounterTextBoxDiv.after().html(
                    '<input type="hidden" name="slot_total" id="slot_total" value=" '+ counter +'" >');

                newCounterTextBoxDiv.appendTo("#spec_data");

                }


                $('.pickTime').clockpicker({
                    autoclose: true,
                    twelvehour: true
                }).focus(function () {
                    $(this).prop("autocomplete", "off");
                });

            });
        });

 		$('input[name="original_price"]').change(function(e) {

        	if(  $("#discount_percent").val() ) {
				var result = parseFloat($("#original_price").val()) - ( parseFloat($("#original_price").val()) * parseFloat($("#discount_percent").val()) );

				$("#discount_label").text( '( Unit Price : ' + result + ' )' );
				$("#unit_price").val(result);

			} else {
				$("#discount_label").text( '( Unit Price : ' + $("#original_price").val() + ' )' );
				$("#unit_price").val($("#original_price").val());
			}

		});

		$('[data-toggle="tooltip"]').tooltip(); 
	
	}

</script>
<?php 
	// replace cover photo modal
	$data = array(
		'title' => get_msg('upload_photo'),
		'img_type' => 'product',
		'img_parent_id' => @$product->id
	);

	$this->load->view( $template_path .'/components/photo_upload_modal', $data );

	// delete cover photo modal
	$this->load->view( $template_path .'/components/delete_cover_photo_modal' ); 
?>