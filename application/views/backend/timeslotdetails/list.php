

<div class="container-fluid" style="padding: 10px 30px 10px 30px;">
    <div class="table-responsive">
        <table class="table m-0 table-striped">
            <tr>
                <th><?php echo get_msg('no'); ?></th>
                <th><?php echo get_msg('date'); ?></th>
                <th><?php echo get_msg('start_time'); ?></th>
                <th><?php echo get_msg('end_time'); ?></th>
                <th><?php echo get_msg('accept_order_total'); ?></th>
                <th><?php echo get_msg('order_count'); ?></th>

                <?php if ( $this->ps_auth->has_access( PUBLISH )): ?>

                    <th><span class="th-title"><?php echo get_msg('btn_publish')?></span></th>

                <?php endif; ?>

            </tr>


            <?php $count = $this->uri->segment(4) or $count = 0; ?>

            <?php if ($timeslotnumber->is_new==1): ?>
            <button class="btn btn-sm btn-danger generate">
                <?php echo get_msg('generate'); ?></button>

            <?php elseif ( !empty( $timeslots ) && count( $timeslots->result()) > 0 ): ?>

                <?php foreach($timeslots->result() as $timeslot): ?>

                    <tr>
                        <td><?php echo ++$count;?></td>
                        <td><?php echo $timeslot->slot_date;?></td>
                        <td><?php echo $timeslot->start_time;?></td>
                        <td><?php echo $timeslot->end_time;?></td>
                        <td><?php echo $timeslot->accept_order_total;?></td>
                        <td><?php echo $timeslot->order_count;?></td>
                        <?php if ( $this->ps_auth->has_access( PUBLISH )): ?>

                            <td>
                                <?php $date_now = date("Y-m-d");
                                if ($date_now < $timeslot->slot_date) {?>

                                <?php if ( @$timeslot->available == 1): ?>
                                    <button class="btn btn-sm btn-success unavailable" id='<?php echo $timeslot->id;?>'>
                                        <?php echo get_msg('available'); ?></button>
                                <?php else:?>
                                <button class="btn btn-sm btn-danger available" id='<?php echo $timeslot->id;?>'>
                                    <?php echo get_msg('notavailable'); ?></button><?php endif;?>
                                <?php }?>
                            </td>

                        <?php endif; ?>

                    </tr>

                <?php endforeach; ?>

            <?php else: ?>

                <?php $this->load->view( $template_path .'/partials/no_data' ); ?>

            <?php endif; ?>

        </table>
    </div>
</div>

<script>

    // Publish Trigger
    $(document).delegate('.generate','click',function(){

        // get button and id
        var btn = $(this);
        // Ajax Call to publish
        $.ajax({
            url: "<?php echo $module_site_url .'/ajx_date_generate/'; ?>",
            method: 'GET',
            success: function( msg ) {
                if ( msg == true ){
                    location.reload();
                } else {
                    location.reload();
                }
            }
        });
    });
    // Publish Trigger
    $(document).delegate('.publish','click',function(){

        // get button and id
        var btn = $(this);
        var id = $(this).attr('id');

        // Ajax Call to publish
        $.ajax({
            url: "<?php echo $module_site_url .'/ajx_publish/'; ?>" + id,
            method: 'GET',
            success: function( msg ) {
                if ( msg == true ){
                    btn.addClass('unpublish').addClass('btn-success')
                        .removeClass('publish').removeClass('btn-danger')
                        .html('Yes');
                } else {
                    alert( "<?php echo get_msg( 'err_sy888' ); ?>" );
                }
            }
        });
    });

    // Unpublish Trigger
    $(document).delegate('.unpublish','click',function(){

        // get button and id
        var btn = $(this);
        var id = $(this).attr('id');

        // Ajax call to unpublish
        $.ajax({
            url: "<?php echo $module_site_url .'/ajx_unpublish/'; ?>" + id,
            method: 'GET',
            success: function( msg ){
                if ( msg == true )
                    btn.addClass('publish').addClass('btn-danger')
                        .removeClass('unpublish').removeClass('btn-success')
                        .html('No');
                else
                    alert( "<?php echo get_msg( 'err_sy4444s' ); ?>" );
            }
        });
    });

    $('.btn-delete').click(function(){

        // get id and links
        var id = $(this).attr('id');
        var btnYes = $('.btn-yes').attr('href');
        var btnNo = $('.btn-no').attr('href');

        // modify link with id
        $('.btn-yes').attr( 'href', btnYes + id );
        $('.btn-no').attr( 'href', btnNo + id );
    });

</script>
