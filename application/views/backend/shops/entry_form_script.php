<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyBYcg0IU6aU5bAp1PrsWo1LI-PVgh9cQAQ&language=<?php echo $this->session->userdata('language_code'); ?>"></script>
<script>
	<?php if ( $this->config->item( 'client_side_validation' ) == true ): ?>

	function jqvalidate() {
		
		$('#shop-form').validate({
			rules:{
				name:{
					blankCheck : "",
					minlength: 3,
					remote: "<?php echo $module_site_url .'/ajx_exists/'.@$shop->id; ?>"
					
				},
				description:{
					required : true
				},
				shoptag:{
					required : true
				},
                latitude:{
                    required : true
                },
                longitude:{
                    required : true
                },
				cover:{
					required : true
				},
				icon:{
					required : true
				}
			},
			messages:{
				name:{
					blankCheck : "<?php echo get_msg( 'err_shop_name' ) ;?>",
					minlength: "<?php echo get_msg( 'err_shop_len' ) ;?>",
					remote: "<?php echo get_msg( 'err_shop_exist' ) ;?>."
				},
                latitude:{
                    required : "<?php echo get_msg( 'select_map_location' ) ;?>."
                },
                longitude:{
                    required : "<?php echo get_msg( 'select_map_location' ) ;?>."
                },
				cover:{
					required : "<?php echo get_msg( 'err_image_missing' ) ;?>."
				},
				icon:{
					required : "<?php echo get_msg( 'err_icon_missing' ) ;?>."
				}
			}

		});

		// custom validation
		jQuery.validator.addMethod("blankCheck",function( value, element ) {
			
			   if(value == "") {
			    	return false;
			   } else {
			    	return true;
			   }
		})
		

	}
	

	<?php endif; ?>

	$('.delete-img').click(function(e){
			e.preventDefault();

			// get id and image
			var id = $(this).attr('id');

			// do action
			var action = '<?php echo $module_site_url .'/delete_cover_photo/'; ?>' + id + '/<?php echo @$shop->id; ?>';
			console.log( action );
			$('.btn-delete-image').attr('href', action);
			
		});

		$('.delete-shop').click(function(e){
		e.preventDefault();
		var id = $(this).attr('id');
		var image = $(this).attr('image');
		var action = '<?php echo site_url('/admin/shops/delete/');?>';
		$('.btn-delete-shop').attr('href', action + id);
	});
		
	$('#shoptag').change(function(){
		var shop_tag = $(this).val();

    	$('#tagselect').val(shop_tag);
    	
	});

	
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

    		var target = $(e.target).attr("value") // activated tab
    		$('#current_tab').val(target);

	});
		$('input[name="per_order_based_cost"]').keyup(function(e)
	                                {
	  if (/[^\d.-]/g.test(this.value))
	  {
	    // Filter non-digits from input value.
	    this.value = this.value.replace(/[^\d.-]/g, '');
	  }
	  
	});

	$(document).ready(function(){
    $('input[name="shipping"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        if(inputValue=="standard_shipping_enable"){
        	$("#standard_shipping_cost").show();
        	$('input[name="colorRadio"]').prop('checked', true);
        }else{
        	$("#standard_shipping_cost").hide();
        	$('input[name="shipping_cost"]').val("0");
        	$('input[name="colorRadio"]').prop('checked', false);
        }
        
    });
	});





    var marker;
    var map;
    // var var_location = new google.maps.LatLng(23.777176, 90.399452); Dhaka
    var var_location = new google.maps.LatLng(21.5699946,39.3430764);
    var previousZoom = 5;

    function map_init() {
        var var_mapoptions = {
            center: var_location,
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map"),var_mapoptions);


        var input = document.getElementById("pac-input");
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo("bounds", map);



        var markerIcon = {
            url: "<?php echo img_url( 'thumbnail/marker.png'); ?>",
        };


        function placeMarker(location) {
            if (marker == undefined){
                marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    animation: google.maps.Animation.DROP,
                    icon:markerIcon,
                });
            }
            else{
                marker.setPosition(location);
            }
            map.setCenter(location);

        }
        google.maps.event.addListener(autocomplete, "place_changed", function(){
            var place = autocomplete.getPlace();
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(15);
            }


        });

        google.maps.event.addListener(map, "click", function(event)
        {
            placeMarker(event.latLng);

            var lat = parseFloat(event.latLng.lat());
            var lng = parseFloat(event.latLng.lng());
            var latlong = {lat: parseFloat(event.latLng.lat()), lng: parseFloat(event.latLng.lng())};

            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'location': latlong}, function(results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        previousZoom = map.zoom;
                        document.getElementById("longitude").value = event.latLng.lng();
                        document.getElementById("latitude").value = event.latLng.lat();
                        var add = document.getElementById("address2").value = results[0].formatted_address;
                        var enAddress = results[0].address_components;
                        for (ac = 0; ac < enAddress.length; ac++) {
                            if (enAddress[ac].types[0] == "route") { document.getElementById("address1").value = enAddress[ac].long_name; }
                        }
                    } else {
                    }
                } else {
                }
            });

            });
        }
        if(document.getElementById("map")){
            google.maps.event.addDomListener(window, 'load', map_init);
        }


        $('#mapmodals').on('shown.bs.modal', function () {
            $("#address1").val('');
            $("#address2").val('');

            google.maps.event.trigger(map, "resize");

            if ( $("#longitude").val() != "" )
                var_location = new google.maps.LatLng($("#latitude").val(), $("#longitude").val());
            map.setZoom(previousZoom);
            document.getElementById("longitude").value = '';
            document.getElementById("latitude").value = '';
            map.setCenter(var_location);
        });
    </script>

    <?php
        // replace cover photo modal
        $data = array(
            'title' => get_msg('upload_photo'),
            'img_type' => 'shop',
            'img_parent_id' => @$shop->id
        );

        $this->load->view( $template_path .'/components/shop_photo_upload_modal', $data );
        // delete cover photo modal
        $this->load->view( $template_path .'/components/delete_cover_photo_modal' );

        // replace icon photo modal
        $data = array(
            'title' => get_msg('upload_photo'),
            'img_type' => 'shop-icon',
            'img_parent_id' => @$shop->id
        );

        $this->load->view( $template_path .'/components/icon_upload_modal', $data );
        // delete icon photo modal
        $this->load->view( $template_path .'/components/delete_icon_modal' );
    ?>
<style>
    #map{
        height: 600px;
    }
    .pac-card{margin:10px 10px 0 0;border-radius:2px 0 0 2px;box-sizing:border-box;-moz-box-sizing:border-box;outline:none;box-shadow:0 2px 6px rgba(0,0,0,0.3);background-color:#fff;font-family:Roboto}
    #pac-container{padding-bottom:12px;margin-right:12px}
    .pac-controls{display:inline-block;padding:5px 11px}
    .pac-controls label{font-family:Roboto;font-size:13px;font-weight:300}
    #pac-input,#pac-input2{background-color:#fff;font-family:Roboto;font-size:15px;font-weight:300;text-overflow:ellipsis;width:400px;z-index:20}
    #pac-input2:focus,#pac-input:focus{border-color:#4d90fe}
    #title{color:#fff;background-color:#4d90fe;font-size:25px;font-weight:500;padding:6px 12px}
    #target{width:345px}
    #pmap{height:450px}
    .pac-container{background-color:#FFF;position:fixed;display:inline-block;float:left;z-index: 99999999;}
    .modal-header{border-bottom:0}.modal-backdrop{z-index:10}​ .modal{z-index:200;top:10%}
    .modal-header {
        padding: 15px;
        border-bottom: 1px solid #e5e5e5;
    }
    .modal-header:before, .modal-header:after {
        content: " ";
        display: table;
    }
    .modal-header:after {
        clear: both;
    }
    .modal-header .close {
        margin-top: -2px;
    }
    .close {
        float: right;
        font-size: 21px;
        font-weight: bold;
        line-height: 1;
        color: #000;
        text-shadow: 0 1px 0 #fff;
        opacity: 0.2;
        filter: alpha(opacity=20);
    }
    button.close {
        padding: 0;
        cursor: pointer;
        background: transparent;
        border: 0;
        -webkit-appearance: none;
    }
    .modal-title {
        margin: 0;
        line-height: 1.428571429;
        position: absolute;
    }
    h4, .h4 {
        font-size: 18px;
    }

</style>