<div class="container-fluid" style="padding: 10px 30px 10px 30px;">
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <?php 
        // echo "<pre>";
        // print_r($period->result());
        // echo "</pre>";
    ?>
    <div class="row">
        <div class="ibox-title">
            <div class="ct-btn">
                <a href="#" class="btn btn-primary btn-sm form-bg" id="btn-icon" data-toggle="modal" data-target="#accPeriodModalFormId">
                    <i class="fa fa-plus"></i>
                </a>
                <a href="#" id="newcoaInsert" class="btn btn-primary btn-sm form-bg" data-title="<?php echo get_msg('Initialize_New_Ledger')?>"><i class="fa fa-refresh text-navy"></i></a>
            </div>

            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
   </div>

 

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox cc-box">
                <div class="ibox-content">
                    <div class="tabs-containers">
                        <div class="table-responsive tblc">
                            <table class="table table-stripped table-bordered" id="list-table">
                                <thead>
                                    <tr>
                                        <th class="sl-icon">
                                            <?php echo get_msg('SL')?>
                                        </th>
                                        <th class="code-icon">
                                            <?php echo get_msg('caption')?>
                                        </th>
                                        <th>
                                            <?php echo get_msg('Period_Type')?>
                                        </th>
                                        <th>
                                            <?php echo get_msg('Start_date')?>  
                                        </th>
                                        <th>
                                            <?php echo get_msg('Close_Date')?>  
                                        </th>
                                        <th>
                                            <?php echo get_msg('Is_Current')?>
                                        </th>
                                       
                                        <!-- <th>
                                            <?php echo get_msg('Is_Close')?>
                                        </th> -->
                                        <th>
                                            <?php echo get_msg('Action')?>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

<?php include 'entry_form.php';?>
<?php include 'period_view.php';?>

<?php
    // Delete Confirm Message Modal
    // $this->load->view( $template_path .'/period/period_form');

	$this->load->view( $template_path .'/components/delete_confirm_modal', $data );
?>

<script type="text/javascript">
$(document).ready(function() {
    var saveBtn = $('#save-btn');
    var resetBtn = $('#reset-btn');
    var listTable = $('#list-table');
    var defaultForm = $('#defaultForm');
    // listTable.DataTable({
    //     "ajax": {
    //         url : "<?php echo site_url('/admin/period/periodlist');?>",
    //         type : 'GET'
    //     },
    // });
    listTable.dataTable({
                language: {
                    processing: "<?php echo get_msg('processing')?>...",
                    search: "<?php echo get_msg('search')?>",
                    lengthMenu: "<?php echo get_msg('display _menu_records_per_page')?>",
                    zeroRecords: "<?php echo get_msg('nothing_found_sorry')?>",
                    info: "<?php echo get_msg('showing_page_page_of_pages')?>",
                    infoEmpty: "<?php echo get_msg('no_records_available')?>",
                    infoFiltered: "<?php echo get_msg('filtered_from_max_total_records')?>",
                    paginate: {
                        first: "<?php echo get_msg('first')?>",
                        previous: "<?php echo get_msg('previous')?>",
                        next: "<?php echo get_msg('next')?>",
                        last: "<?php echo get_msg('last')?>"
                    },
                },
                "processing": true,
                "serverSide": true,
                "bAutoWidth": false,
                "iDisplayLength": 25,
                "order": [0, "desc"],
                "sAjaxSource": "<?php echo site_url('/admin/period/periodlist');?>",
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    if (aData.DT_RowId == undefined) {
                        return true;
                    }
                    $('td:eq(6)', nRow).html(getActionButtons(nRow, aData));
                    return nRow;
                },
                "fnServerParams": function (aoData) {
                    aoData.push({"name": "listAction", "value": true},
                        // {"name": "trans_date", "value": $('#transDateRpt').val()},
                        // {"name": "trans_date_to", "value": $('#transDateToRpt').val()},
                        // {"name": "acc_period", "value": $('#accPeriodRpt').val()},
                        // {"name": "confirm_status", "value": $('#confirmStatusRpt').val()},
                        // {"name": "stake_holder_type", "value": $('#accVcrDtls_stakeHolderTypeRpt').val()},
                        // {"name": "stakeholder_name", "value": $('#accVcrDtls_stakeholderRpt').val()},
                        // {"name": "acc_coa", "value": $('#accCoaRpt').val()},
                        // {"name": "coa_class", "value": $('#coaClassRpt').val()},
                        );
                },
                "aoColumns": [
                    null,
                    null,
                    null,
                    {"bSortable": false},
                    {"bSortable": false},
                    null,
                    {"bSortable": false},
                ]
            });
    defaultForm.validate({
            lang: langCode,
            ignore: [],
            rules: {
                period_start_dt: {
                    required: true
                },
                period_close_dt: {
                    required: true
                },
            },
            submitHandler: function (form) {
                    var url;
                    if ($('#hiddenUpId').val() == '') {
                        url = "<?php echo site_url('/admin/period/writesubmit');?>";
                    } else {
                        url = "<?php echo site_url('/admin/period/editsubmit');?>";
                    }
                    formAfterSubmit(saveBtn, "<?php echo get_msg('processing')?>")
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: defaultForm.serialize(),
                        success: function (data) {
                            formSubmitReturnMsg(saveBtn, data, listTable, defaultForm, "<?php echo $action_title;?>", "<?php echo get_msg('save')?>");
                            if (data.isError == true) {
                                resetMasterForm();
                            }
                        },
                        failure: function (data) {
                        }
                    })
                    return false;
                }
        });

        listTable.on('click', 'a.show-reference', function (e) {
            var control = this;
            var referenceId = $(control).attr('referenceid');
            jQuery.ajax({
                type: 'GET',
                dataType: 'JSON',
                data: {id: referenceId, showAction: true},
                url: "<?php echo site_url('/admin/period');?>",
                success: function (data, textStatus) {
                    if (data.isError == false) {
                        $("#view-caption").html("<strong>" + (data.defaultInstance.caption != null) ? (data.defaultInstance.caption) : '' + "</strong>");
                        $("#view-periodCloseDt").html("<strong>" + ((data.defaultInstance.period_close_dt != null) ? (getFormattingDate(new Date(data.defaultInstance.period_close_dt))) : '') + "</strong>");
                        $("#view-periodStartDt").html("<strong>" + ((data.defaultInstance.period_start_dt != null) ? (getFormattingDate(new Date(data.defaultInstance.period_start_dt))) : '') + "</strong>");
                        $("#view-isOpen").html("<strong>" + (data.defaultInstance.is_open != null) ? (data.defaultInstance.is_open) : '' + "</strong>");
                        $("#view-isClose").html("<strong>" + (data.defaultInstance.is_close != null) ? (data.defaultInstance.is_close) : '' + "</strong>");
                        $("#view-remark").html("<strong>" + (data.defaultInstance.remark != null) ? (data.defaultInstance.remark) : '' + "</strong>");
                        $('#accPeriodModalViewId').modal('show');
                    } else {
                        alert(data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    serverErrorToast(errorThrown);
                }
            });
            e.preventDefault();
        });

        listTable.on('click', 'a.update-reference', function (e) {
                var control = this;
                var referenceId = $(control).attr('referenceid');
                jQuery.ajax({
                    type: 'GET',
                    dataType: 'JSON',
                    data: {id: referenceId, editData: true},
                    url: "<?php echo site_url('/admin/period');?>",
                    success: function (data, textStatus) {
                        if (data.isError == false) {
                            $('#defaultMsgDiv').html('');
                            $('#hiddenUpId').val(data.defaultInstance.id);
                            var periodCloseDt = data.defaultInstance.period_close_dt;
                            $('#periodCloseDt').datepicker('setDate', ((periodCloseDt != null) ? (new Date(periodCloseDt)) : ''));
                            var periodStartDt = data.defaultInstance.period_start_dt;
                            $('#periodStartDt').datepicker('setDate', ((periodStartDt != null) ? (new Date(periodStartDt)) : ''));
                            $('#isOpen').prop('checked', data.defaultInstance.is_open);
                            $('#isClose').prop('checked', data.defaultInstance.is_close);
                            $("#remark").val((data.defaultInstance.remark != null) ? (data.defaultInstance.remark) : '');
                            $('#accPeriodModalFormId').modal('show');
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        serverErrorToast(errorThrown);
                    }
                });
                e.preventDefault();
            });


            listTable.on('click', 'a.confirm-reference', function (e) {
                var selectRow = $(this).parents('tr');
                var data = selectRow.find("td:eq(1)").text();
                var confirmDel = confirm("<?php echo get_msg('are_you_sure_want_to_open_this_period')?> " + data + ' ??');
                if (confirmDel == true) {
                    var control = this;
                    var referenceId = $(control).attr('referenceid');
                    jQuery.ajax({
                        type: 'POST',
                        dataType: 'JSON',
                        data: {id: referenceId, confirmStatus: 'true'},
                        url: "<?php echo site_url('/admin/period/confirmsubmit');?>",
                        success: function (data, textStatus) {
                            confirmRowMessages(data.isError, data.message, listTable);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            serverErrorToast(errorThrown);
                        }
                    });
                }
                e.preventDefault();
            });

            $('#newcoaInsert').on("click", function (e)  {
                var confirmDel = confirm("<?php echo get_msg('are_you_sure_want_initialize_new_ledger')?> ");
                if (confirmDel == true) {
                    jQuery.ajax({
                        type: 'POST',
                        dataType: 'JSON',
                        data: {recreateLedger: 'true'},
                        url: "<?php echo site_url('/admin/period/recreateledger');?>",
                        success: function (data, textStatus) {
                            confirmRowMessages(data.isError, data.message, listTable);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            serverErrorToast(errorThrown);
                        }
                    });
                }
                e.preventDefault();
            });
            function resetMasterForm() {
                }

});
</script>

