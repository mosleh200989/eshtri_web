
    <div class="modal fade common-form" id="batchaccPeriodModalFormId">
        <div class="modal-dialog modal-md">
            <div class="modal-content form-bg">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="modal-area">
                        <form class="form-horizontal form-body-md col-md-12 column-one-form common-form" id="batchdefaultForm">
                            <div class="text-center panel panel-default mb-0 crowlr-4">
                                <h3 id="batchheaderTitle" class="m-0 p-1"><?php echo get_msg('Period')?></h3>
                            </div>

                            <div id="batchdefaultMsgDiv"></div>

                            <div class="column-one-form-down">

                                <div class="row crowmlr-8">
                                    <input type="hidden" id="batchhiddenUpId" name="id" />

                                    <div class="col-md-12 column-one p-1">
                                        <div class="panel panel-default">
                                            <fieldset>
                                                <legend><?php echo get_msg('Period')?></legend>  

                                                <div class="form-group">
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('Year')?>:</label>

                                                        <div class="col-sm-8 fmi_box">
                                                            <select class="form-control selectTwo select2 input-sm" name="period_year" id="periodYear">
                                                                <option value=""> </option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('Remark')?>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                            <textarea id="batchremark" name="remark" class="form-control input-sm"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row crowmlr-8">
                                <div class="col-md-12 p-1">
                                    <div class="panel panel-default mb-0">
                                        <div class="text-right p-2">
                                            <button type="button" class="btn btn-default" data-toggle="tooltip" id="batchreset-btn"><?php echo get_msg('Reset')?></button>
                                            <button type="button" class="btn btn-primary" data-toggle="tooltip" id="batchsave-btn-monthly"><?php echo get_msg('Monthly_Period')?>... <i
                                                        class="fa fa-recycle"></i></button>
                                            <button type="button" class="btn btn-primary" data-toggle="tooltip" id="batchsave-btn-yearly"><?php echo get_msg('Yearl_Period')?>... <i
                                                        class="fa fa-recycle"></i></button>
                                            <button type="button" class="btn btn-warning" data-dismiss="modal"><?php echo get_msg('Cancel')?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>