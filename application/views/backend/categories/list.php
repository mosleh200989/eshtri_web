<div class="table-responsive animated fadeInRight">
	<table class="table m-0 table-striped">
		<tr>
			<th><?php echo get_msg('no'); ?></th>
			<th><?php echo get_msg('product_image'); ?></th>
			<th><?php echo get_msg('cat_name'); ?></th>
			<th><?php echo get_msg('name_alt'); ?></th>
			
			<?php if ( $this->ps_auth->has_access( EDIT )): ?>
				
				<th><span class="th-title"><?php echo get_msg('btn_edit')?></span></th>
			
			<?php endif; ?>
			
			<?php if ( $this->ps_auth->has_access( DEL )): ?>
				
				<th><span class="th-title"><?php echo get_msg('btn_delete')?></span></th>
			
			<?php endif; ?>
			
			<?php if ( $this->ps_auth->has_access( PUBLISH )): ?>
				
				<th><span class="th-title"><?php echo get_msg('btn_publish')?></span></th>
			
			<?php endif; ?>

		</tr>
		
	
	<?php $count = $this->uri->segment(4) or $count = 0; ?>

	<?php if ( !empty( $categories ) && count( $categories->result()) > 0 ): ?>

		<?php foreach($categories->result() as $category): ?>
			
			<tr>
				<td><?php echo ++$count;?></td>
				<td><?php 
				$imginfo=$this->Image->get_one_by( array( 'img_type' => 'category', 'img_parent_id' => $category->id)); 
				if($imginfo->img_path !=null) { ?>
						<img src="<?php echo site_url( '/'); ?>uploads/small/<?php echo $imginfo->img_path;?>" width="80" height="80">
						<?php } else { ?>
						<img src="<?php echo site_url( '/'); ?>files/Icon-Small.png" width="80" height="80">
					<?php } ?>
				</td>
				<td><?php echo $category->name;?></td>
				<td><?php echo $category->name_alt;?></td>

				<?php if ( $this->ps_auth->has_access( EDIT )): ?>
			
					<td>
						<a href='<?php echo $module_site_url .'/edit/'. $category->id; ?>'>
							<i class='fa fa-pencil-square-o'></i>
						</a>
					</td>
				
				<?php endif; ?>
				
				<?php if ( $this->ps_auth->has_access( DEL )): ?>
					
					<td>
						<?php  
                  if( $this->session->userdata('user_id') == "usr75023528330eed4cc55f0e1bdd463cfe") { // mosleh_dev
                ?>
						<a herf='#' class='btn-delete' data-toggle="modal" data-target="#categorymodal" id="<?php echo "$category->id";?>">
							<i class='fa fa-trash-o'></i>
						</a>
					<?php } ?>
					</td>
				
				<?php endif; ?>
				
				<?php if ( $this->ps_auth->has_access( PUBLISH )): ?>
					
					<td>
						<?php if ( @$category->status == 1): ?>
							<button class="btn btn-sm btn-success unpublish" id='<?php echo $category->id;?>'>
							<?php echo get_msg('btn_yes'); ?></button>
						<?php else:?>
							<button class="btn btn-sm btn-danger publish" id='<?php echo $category->id;?>'>
							<?php echo get_msg('btn_no'); ?></button><?php endif;?>
					</td>
				
				<?php endif; ?>

			</tr>

		<?php endforeach; ?>

	<?php else: ?>
			
		<?php $this->load->view( $template_path .'/partials/no_data' ); ?>

	<?php endif; ?>

</table>
</div>

