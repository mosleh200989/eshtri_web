<script>
function runAfterJQ() {

	$(document).ready(function(){
		
		// Publish Trigger
		

	});
}
</script>

<?php
	// Delete Confirm Message Modal
	$data = array(
		'title' => get_msg( 'delete_location_label' ),
		'message' =>  get_msg( 'location_yes_all_message' ),
		'no_only_btn' => get_msg( 'location_no_only_label' )
	);
	
	$this->load->view( $template_path .'/components/delete_confirm_modal', $data );
?>