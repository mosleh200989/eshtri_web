<div class="row crowmlr-8 master-detail">
    <div class="col-md-12 column-one p-1">
        <div class="panel panel-default">
            <fieldset class="mt-0">
                <legend><?php echo get_msg('journal_voucher')?></legend>
                <div class="form-group">
                    <div class="row m-0">
                        <div class="col-lg-3 column-three pl-1 pr-1">
                            <div class="row mb-1">
                                <label  class="col-sm-4 control-label"><?php echo get_msg('acc_coa')?><span class="text-warning">*</span> : &nbsp </label>
                                <div class="col-sm-8 fmi_box">
                                    <select class="form-control selectTwo select2 input-sm" id="accVcrDtls_accCoa" name="acc_coa">
                                    <option value=""><?php echo get_msg('acc_coa')?>...</option>
                                            <?php
                                            foreach ($coaOptionGroup as $optionGroup)
                                            {?>
                                                <optgroup label="<?php echo $optionGroup["code"]." - ".$optionGroup["caption"];?>">
                                                    <?php
                                                    foreach ($optionGroup["options"] as $options)
                                                    {?>
                                                        <option value="<?php echo $options["id"];?>"><?php echo $options["code"]." - ".$options["caption"];?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </optgroup>
                                        <?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 column-three pl-1 pr-1">
                        <div class="row mb-1">
                            <label class="col-sm-4 control-label"><?php echo get_msg('bearer_type')?>:</label>
                            <div class="col-sm-8 fmi_box">
                                <select class="form-control selectTwo input-sm" id="accVcrDtls_stakeHolderType" name="stake_holder_type">
                                <option value=""><?php echo get_msg('bearer_type')?></option>
                                <?php
                                foreach ($stakeHolderTypeList as $item)
                                {
                                echo '<option value="'.$item.'">'.get_msg($item).'</option>';
                                }
                                ?>
                                </select>
                                </select>
                            </div>
                        </div>
                        </div>

                        <div class="col-lg-4 column-three pl-1 pr-1">
                            <div class="row mb-1">
                                <label class="col-sm-4 control-label"> <?php echo get_msg('stakeholder')?>: &nbsp </label>
                                <div class="col-sm-8 fmi_box">
                                    <select class="form-control  input-sm" id="accVcrDtls_stakeholder" name="stakeholder_id">
                                        <option value=""><?php echo get_msg('stakeholder')?> </option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-3">
                            <div class="row">
                                <div class="col-lg-6 column-three pl-1 pr-1">
                                    <div class="row mb-1">
                                        <label class="col-sm-4 control-label"><?php echo get_msg('dr_cr_type')?><span class="text-warning">*</span>:</label>
                                        <div class="col-sm-8 fmi_box">
                                            <select class="form-control  selectTwo input-sm"  id="accVcrDtls_drcrType" name="dr_cr_type">
                                            <option value=""><?php echo get_msg('dr_cr_type')?></option>
                                            <?php
                                            foreach ($drcrTypeList as $item)
                                            {
                                            echo '<option value="'.$item.'">'.get_msg($item).'</option>';
                                            }
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 column-three pl-1 pr-1">
                                    <div class="row mb-1">
                                        <label class="col-sm-4 control-label"><?php echo get_msg('amount')?><span class="text-warning">*</span> :</label>
                                        <div class="col-sm-8 fmi_box">
                                            <input type="number" id="accVcrDtls_drCrAmount" name="acc_vcr_dtls_dr_cr_amount"
                                                   class="form-control input-sm" min="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 pl-1 pr-1 float-right">
                            <div class="text-right p-2">
                                <button type="button" class="btn btn-default"
                                        data-toggle="tooltip"
                                        id="reset-accVcrDtls-btn"><?php echo get_msg('reset')?></button>
                                <button type="button" class="btn btn-primary"
                                        data-toggle="tooltip"
                                        id="add-accVcrDtls-btn"><?php echo get_msg('add')?>... <i
                                        class="fa fa-recycle"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="ibox-content p-1">
                <div class="tabs-containers">

                    <div class="table-responsive tblc max-height-200 min-height-200 sub-master-tbl">
                        <table class="table table-stripped table-bordered"
                               id="accVcrDtls-table">
                            <thead>
                                <th><?php echo get_msg('sl')?></th>
                                <th><?php echo get_msg('acc_coa')?></th>
                                <th><?php echo get_msg('source_stakeholder')?></th>
                                <th><?php echo get_msg('dr_amountr')?></th>
                                <th><?php echo get_msg('cr_amount')?></th>
                                <th><?php echo get_msg('action')?></th>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="3"></td>
                                <td class="text-navy"> <input type="hidden" id="drAmountSumInput" name="drAmountSumInput" value=""><strong id="drAmountSum">0</strong>
                                </td>
                                <td class="text-navy"><input type="hidden" id="crAmountSumInput" name="crAmountSumInput" value=""><strong id="crAmountSum">0</strong>
                                </td>
                                <td></td>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
            </div>
        </fieldset>
        </div>
    </div>
</div>