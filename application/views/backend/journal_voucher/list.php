<div class="container-fluid" style="padding: 10px 30px 10px 30px;">
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox cc-box">

                   <?php include 'filter_form.php';?>
                    
                    <div class="ibox-title">
                        <div class="ct-btn">
                        <a href="#" id="defaultModalOnClickBtn"
                                   class="btn btn-primary btn-sm form-bg">
                                    <i class="fa fa-plus"></i>
                            </a>
                        </div>

                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                        
                    </div>

                    <div class="ibox-content">
                        <div class="tabs-containers">
                            <div class="table-responsive tblc">
                                <table class="table table-stripped table-bordered" id="list-table">
                                    <thead>
                                        <tr>
                                            <th><?php echo get_msg('sl')?></th>
                                            <th><?php echo get_msg('period')?></th>
                                            <th><?php echo get_msg('trans_date')?></th>
                                            <th><?php echo get_msg('stakeholder')?></th>
                                            <th><?php echo get_msg('accounts')?></th>
                                            <th><?php echo get_msg('amount')?></th>
                                            <th><?php echo get_msg('action')?></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'entry_form.php';?>
<?php include 'journal_view.php';?>



<script>
    var saveBtn = $('#save-btn');
    var resetBtn = $('#reset-btn');
    var listTable = $('#list-table');
    var defaultForm = $('#defaultForm');
    var periodStartDt = "<?php echo $currentPeriod['periodStartDt'];?>";
    var periodCloseDt = "<?php echo $currentPeriod['periodCloseDt'];?>";
        $(document).ready(function () {
            $("#show-btn").on("click", function (e) {
                listTable.DataTable().ajax.reload();
                e.preventDefault();
            });
            resetFormByBtn(resetBtn, defaultForm, "<?php echo get_msg('journal_voucher')?>", "<?php echo get_msg('save')?>");
            resetBtn.click(function () {
                 resetMasterForm();
            });
            
            var startDate = new Date(periodStartDt);
            var endDate = new Date(periodCloseDt);
            $('#transDate').datepicker({
              format: 'yyyy-mm-dd'
            }).datepicker("setDate",'now');
            $('#transDate').datepicker('setStartDate', startDate);
            $('#transDate').datepicker('setEndDate', endDate);


            $("#defaultModalOnClickBtn").on("click", function (e) {
                $("#accVcrMstModalFormId").modal('show');
                // resetMasterForm();
                // resetDetailForm();
                $("#accVcrDtls-table > tbody").html("");
                $('#drAmountSum').html('0');
                $('#crAmountSum').html('0');
                e.preventDefault();
            });

            defaultForm.validate({
                lang: langCode,
                ignore: [],
                rules: {
                    acc_period_id: {
                        required: true
                    },
                    trans_date: {
                        required: true
                    },
                },
                submitHandler: function (form) {
                    var url;
                    if ($('#hiddenUpId').val() == '') {
                        url = "<?php echo site_url('/admin/journal_voucher/write');?>";
                    } else {
                        url = "<?php echo site_url('/admin/journal_voucher/editsubmit');?>";
                    }
                    formAfterSubmit(saveBtn, "<?php echo get_msg('processing')?>....")
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: defaultForm.serialize(),
                        success: function (data) {

                            if (data.isError == true) {
                                saveBtn.children().removeClass('fa-spin');
                                saveBtn.contents().first().replaceWith("<?php echo get_msg('save')?>");
                                saveBtn.prop('disabled', false);
                                $('#defaultMsgDiv').html(createMessageDiv("error-msg", data.message));
                            } else {
                                $('#defaultMsgDiv').html(createMessageDiv("success-msg", data.message));
                                saveBtn.children().removeClass('fa-spin');
                                saveBtn.contents().first().replaceWith("<?php echo get_msg('save')?>");
                                saveBtn.prop('disabled', false);
                                listTable.DataTable().ajax.reload();
                                // resetMasterForm();
                                // resetDetailForm();
                                $('#drAmountSum').html('0');
                                $('#crAmountSum').html('0');
                                $("#accVcrDtls-table > tbody").html("");
                            }
                        },
                        failure: function (data) {
                        }
                    })
                }
            });

            listTable.dataTable({
                language: {
                    processing: "<?php echo get_msg('processing')?>...",
                    search: "<?php echo get_msg('search')?>",
                    lengthMenu: "<?php echo get_msg('display_menu_records_per_page')?>",
                    zeroRecords: "<?php echo get_msg('nothing_found_sorry')?>",
                    info: "<?php echo get_msg('showing_page_page_of_pages')?>",
                    infoEmpty: "<?php echo get_msg('no_records_available')?>",
                    infoFiltered: "<?php echo get_msg('filtered_from_max_total_records')?>",
                    paginate: {
                        first: "<?php echo get_msg('first')?>",
                        previous: "<?php echo get_msg('previous')?>",
                        next: "<?php echo get_msg('next')?>",
                        last: "<?php echo get_msg('last')?>"
                    },
                },
                "processing": true,
                "serverSide": true,
                "bAutoWidth": false,
                "iDisplayLength": 25,
                "order": [0, "desc"],
                "sAjaxSource": "<?php echo site_url('/admin/journal_voucher/voucher_list');?>",
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    if (aData.DT_RowId == undefined) {
                        return true;
                    }
                    $('td:eq(6)', nRow).html(getActionButtons(nRow, aData));
                    return nRow;
                },
                "fnServerParams": function (aoData) {
                    aoData.push({"name": "listAction", "value": true},
                        {"name": "trans_date", "value": $('#transDateRpt').val()},
                        {"name": "trans_date_to", "value": $('#transDateToRpt').val()},
                        {"name": "acc_period", "value": $('#accPeriodRpt').val()},
                        {"name": "confirm_status", "value": $('#confirmStatusRpt').val()},
                        {"name": "stakeHolder_type", "value": $('#accVcrDtls_stakeHolderTypeRpt').val()},
                        {"name": "stakeholder_name", "value": $('#accVcrDtls_stakeholderRpt').val()},
                        {"name": "acc_coa", "value": $('#accCoaRpt').val()},
                        {"name": "coa_class", "value": $('#coaClassRpt').val()}

                    );
                },
                "aoColumns": [
                    null,
                    null,
                    null,
                    {"bSortable": false},
                    {"bSortable": false},
                    null,
                    {"bSortable": false},
                ]
            });

            listTable.on('click', 'a.show-reference', function (e) {
                var control = this;
                var referenceId = $(control).attr('referenceid');
                jQuery.ajax({
                    type: 'GET',
                    dataType: 'JSON',
                    data: {id: referenceId, showAction: true},
                    url: "<?php echo site_url('/admin/journal_voucher');?>",
                    success: function (data, textStatus) {
                        if (data.isError == false) {
                            $("#view-accPeriod").html("<strong>" + (data.defaultInstance.acc_period_id != null ? data.acc_period : '') + "</strong>");
                            $("#view-transDate").html("<strong>" + ((data.defaultInstance.trans_date != null) ? (getFormattingDate(new Date(data.defaultInstance.trans_date))) : '') + "</strong>");
                            $("#view-referenceCode").html("<strong>" + (data.defaultInstance.reference_code != null) ? (data.defaultInstance.reference_code) : '' + "</strong>");
                            $("#view-bankAccount").html("<strong>" + (data.defaultInstance.bank_account != null ? data.bankAccount : '') + "</strong>");
                            // $("#view-stakeHolderType").html("<strong>" + (data.obj.defaultInstance.stakeHolderType != null ? data.obj.stakeHolderType : '') + "</strong>");
                            // $("#view-sourceStakeholder").html("<strong>" + (data.obj.defaultInstance.sourceStakeholder != null ? data.obj.defaultInstance.sourceStakeholder : '') + "</strong>");
                            $("#view-narration").html("<strong>" + (data.defaultInstance.narration != null) ? (data.defaultInstance.narration) : '' + "</strong>");
                            $("#accVcrDtlsTableView > tbody").html("");
                            rowNumber = 1;
                            if (data.accVcrDtls != null) {
                                $.each(data.accVcrDtls, function (key, value) {
                                    detailInTableView(rowNumber++, value.id, value.acc_coa_id, value.coaCaption,  value.sourceStakeholderText, value.dr_amount, value.cr_amount);
                                });
                            }
                            $('#view-activeStatus').html('<h5>' + (data.activeStatus ? data.activeStatus : '') + '</h5>');
                            $('#accVcrMstModalViewId').modal('show');
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        serverErrorToast(errorThrown);
                    }
                });
                e.preventDefault();
            });

            listTable.on('click', 'a.update-reference', function (e) {
                var control = this;
                var referenceId = $(control).attr('referenceid');
                jQuery.ajax({
                    type: 'GET',
                    dataType: 'JSON',
                    data: {id: referenceId, editData: true},
                    url: "<?php echo site_url('/admin/journal_voucher');?>",
                    success: function (data, textStatus) {
                        if (data.isError == false) {
                            $('#defaultMsgDiv').html('');
                            $('#hiddenUpId').val(data.defaultInstance.id);
                            $("#accPeriod").val(data.defaultInstance.acc_period_id != null ? '' + data.acc_period.id : '');
                            $("#accPeriodText").val(data.defaultInstance.acc_period_id != null ? '' + data.acc_period.caption : '');
                            var transDate = data.defaultInstance.trans_date;
                            $('#transDate').datepicker('setDate', ((transDate != null) ? (new Date(transDate)) : ''));
                            $("#referenceCode").val((data.defaultInstance.reference_code != null) ? (data.defaultInstance.reference_code) : '');
                            $("#narration").val((data.defaultInstance.narration != null) ? (data.defaultInstance.narration) : '');
                            // $("#stakeholder").empty().append('<option value="' + (data.obj.stakeholder != null ? data.obj.stakeholder.id : '') + '">' + (data.obj.stakeholderCode + '-' + data.obj.stakeholderCaption) + '</option>').val(data.obj.stakeholder != null ? data.obj.stakeholder.id : '').trigger('change');
                            // $("#select2-stakeholder-container").html(data.obj.stakeholderCode + ' - ' + data.obj.stakeholderCaption);
                            $("#accVcrDtls-table > tbody").html("");
                            if (data.accVcrDtls != null) {
                                $.each(data.accVcrDtls, function (key, value) {
                                    addDetailInTable(rowNumber++, value.id, value.accCoaId, value.accCoaText, value.stakeHolderType,  value.stakeholderId,  value.stakeholderCode,  value.stakeholderName, value.stakeholderText, value.drAmount, value.crAmount,  value.drcrType, value.drAmountReadonly, value.crAmountReadonly);
                                });
                            }
                            onchangebind();
                            // totalAmount();
                            $('#accVcrMstModalFormId').modal('show');
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        serverErrorToast(errorThrown);
                    }
                });
                e.preventDefault();
            });

            listTable.on('click', 'a.confirm-reference', function (e) {
                var selectRow = $(this).parents('tr');
                var data = selectRow.find("td:eq(2)").text();
                var confirmDel = confirm("<?php echo get_msg('are_you_sure?')?>" + data + ' ?');
                if (confirmDel == true) {
                    var control = this;
                    var referenceId = $(control).attr('referenceid');
                    jQuery.ajax({
                        type: 'POST',
                        dataType: 'JSON',
                        data: {id: referenceId, confirmStatus: 'true'},
                        url: "<?php echo site_url('/admin/journal_voucher/confirmdata');?>",
                        success: function (data, textStatus) {
                            confirmRowMessages(data.isError, data.message, listTable);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            serverErrorToast(errorThrown);
                        }
                    });
                }
                e.preventDefault();
            });

            listTable.on('click', 'a.activate-reference,a.delete-reference', function (e) {
                var selectRow = $(this).parents('tr');
                var data = selectRow.find("td:eq(1)").text();
                var referenceClass = $(this).attr("class");
                var confirmDel;
                if (referenceClass == 'activate-reference') {
                    confirmDel = confirm("<?php echo get_msg('are_you_sure_want_to_change_status')?>" + data + ' ?');
                } else if (referenceClass == 'delete-reference') {
                    confirmDel = confirm("<?php echo get_msg('are_you_sure_want_to_remove')?>" + data + ' ?');
                }
                if (confirmDel == true) {
                    var control = this;
                    var referenceId = $(control).attr('referenceid');
                    jQuery.ajax({
                        type: 'POST',
                        dataType: 'JSON',
                        data: {id: referenceId, referenceClass: referenceClass, deleteClass: 'mainClass'},
                        url: "<?php echo site_url('/admin/journal_voucher/remove');?>",
                        success: function (data, textStatus) {
                            deleteMessages(data.isError, data.message, listTable, selectRow);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            serverErrorToast(errorThrown);
                        }
                    });
                }
                e.preventDefault();
            });
           
            

            
        

        });
        

        
    var rowNumber = 1;
    $(document).ready(function () {

        $('#reset-accVcrDtls-btn').click(function () {
            resetDetailForm();
            $("#accVcrDtls-table > tbody").html("");
        });

        $("#add-accVcrDtls-btn").on("click", function (e) {
            var accCoa = $('#accVcrDtls_accCoa').select2('data');
            if (accCoa == '') {
                alert("<?php echo get_msg('please_select_all_mandatory_fields')?>");
                return false
            }
            var drcrType = $('#accVcrDtls_drcrType').val();
            if (drcrType == '') {
                alert("<?php echo get_msg('please_select_all_mandatory_fields')?>");
                return false
            }
            var accCoaId = accCoa[0].id;
            var accCoaText = accCoa[0].text;
            var stakeHolderType = '';
            var stakeholderId = '';
            var stakeholderCode = '';
            var stakeholderName = '';
            var stakeholderText = '';
            var stakeholder = $('#accVcrDtls_stakeholder').select2('data');
            if (stakeholder[0].id != '') {
                var stakeHolderType = $('#accVcrDtls_stakeHolderType').val();
                var stakeholderId = stakeholder[0].id;
                var stakeholderCode = stakeholder[0].phone;
                var stakeholderName = stakeholder[0].caption;
                var stakeholderText =  (stakeholder[0].phone+' - '+ stakeholder[0].caption);
            }
            var drCrAmount = $('#accVcrDtls_drCrAmount').val();
            if (drCrAmount == '' || drCrAmount == '0') {
                alert("<?php echo get_msg('please_select_all_mandatory_fields')?>");
                return false
            }
            var drAmount
            var drAmountReadonly
            var crAmount
            var crAmountReadonly
            if (drcrType == "Debit") {
                drAmount = parseFloat(drCrAmount);
                crAmount = parseFloat('0');
                drAmountReadonly=''
                crAmountReadonly='readonly'
            } else {
                drAmount = parseFloat('0');
                crAmount = parseFloat(drCrAmount);
                drAmountReadonly='readonly'
                crAmountReadonly=''
            }
            if (checkDuplicate(accCoaId) == true) {
                alert(accCoaText + "<?php echo get_msg('data_already_exist')?>....");
                return false;
            }
            addDetailInTable(rowNumber++, '', accCoaId, accCoaText, stakeHolderType, stakeholderId, stakeholderCode, stakeholderName, stakeholderText, drAmount, crAmount, drcrType, drAmountReadonly, crAmountReadonly);
            onchangebind();
            resetDetailForm();
            totalAmount();
            e.preventDefault();
        });
        $("#accVcrDtls_stakeHolderType").change(function(){
                $("#accVcrDtls_stakeholder").select2({
                ajax: {
                    url: "<?php echo site_url('/admin/expense_voucher');?>",
                    dataType: 'JSON',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page,
                            stakeHolderType: $('#accVcrDtls_stakeHolderType').val(),
                            stakeholderSearch: 'true'
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: "<?php echo get_msg('stakeholder')?>...",
                allowClear: true,
                width: '100%',
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });
            });
        $("#accVcrDtls_stakeHolderTypeRpt").change(function(){
                $("#accVcrDtls_stakeholderRpt").select2({
                ajax: {
                    url: "<?php echo site_url('/admin/expense_voucher');?>",
                    dataType: 'JSON',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page,
                            stakeHolderType: $('#accVcrDtls_stakeHolderTypeRpt').val(),
                            stakeholderSearch: 'true'
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: "<?php echo get_msg('stakeholder')?>...",
                allowClear: true,
                width: '100%',
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });
        });
        

    });

        function formatRepoRpt(repo) {
            if (repo.loading) {
                return repo.text;
            }
            return "<div class='row slt-srs m-0'>" +
                "<div class='col-md-10 sc-con pl-3 pr-1'>" +
                "<span class='scsm-gp'>" +
                "<span class='sc-code lft'>" + repo.caption + '</span>' +
                '</span>' +
                "</div>" +
                "</div>";
        }

        function formatRepoSelectionRpt(repo) {
            if (repo.id === '' || repo.phone== undefined) {
                return "<?php echo get_msg('stakeholder')?>...";
            }
            $('#carriedByCaption').val(repo.caption);
            return repo.caption;
        }

        function formatRepo(repo) {
            if (repo.loading) {
                return repo.text;
            }
            return "<div class='row slt-srs m-0'><div class='col-md-2 thumbnail sc-img p-0'>" + repo.imgsrc + "</div><div class='col-md-10 sc-con pl-3 pr-1'><span class='scsm-gp'><span class='sc-code lft'>" + repo.caption + '</span><span class="sc-phn rit">' + repo.phone + '</span></span><span class="scsm-gp"><span class="sc-capt lft">' + repo.phone + '</span></span><span class="scsm-gp"><span class="sc-eml lft">' + repo.email + "</span></span></div></div>";
        }

        function formatRepoSelection(repo) {
            if (repo.id === '' || repo.phone== undefined) {
                return "<?php echo get_msg('stakeholder')?>...";
            }
            $('#carriedByCaption').val(repo.caption);
            return repo.phone + ' ' + repo.caption;
        }




    function detailInTableView(rowNumber, id, accCoaId, accCoaText, bankAccountText, drAmount, crAmount) {
        $('#accVcrDtlsTableView > tbody').prepend('<tr><td>' + rowNumber + '</td><td>' + nullCheck(accCoaText) + '</td><td>' + nullCheck(bankAccountText) + '</td><td>' + nullCheck(drAmount) + '</td><td>' + nullCheck(crAmount) + '</td></tr>');
    }

    function addDetailInTable(rowNumber, id, accCoaId, accCoaText, stakeHolderType, stakeholderId, stakeholderCode, stakeholderName, stakeholderText, drAmount, crAmount, drcrType, drAmountReadonly, crAmountReadonly) {
        var idCol = '<input type="hidden" class="id" name="idDtls[' + rowNumber + ']" value="' + id + '">';
        var accCoaCol = '<input type="hidden"  name="accCoaId[' + rowNumber + ']" class="accCoaId" value="' + nullCheck(accCoaId) + '"><input type="text" readonly  class="form-control input-sm accCoaText" value="' + nullCheck(accCoaText) + '">';
        var stakeholderCol = '<input type="hidden"  name="stakeHolderType[' + rowNumber + ']" class="stakeHolderType" value="' + nullCheck(stakeHolderType) + '">' +
            '<input type="hidden"  name="stakeholderId[' + rowNumber + ']" class="stakeholderId" value="' + nullCheck(stakeholderId) + '">' +
            '<input type="hidden"  name="stakeholderCode[' + rowNumber + ']" class="stakeholderCode" value="' + nullCheck(stakeholderCode) + '">' +
            '<input type="hidden"  name="stakeholderName[' + rowNumber + ']" class="stakeholderName" value="' + nullCheck(stakeholderName) + '">' +
            '<input type="text" readonly  class="form-control input-sm stakeholderText" value="' + nullCheck(stakeholderText) + '">';
        var drAmountCol = '<input type="text" '+drAmountReadonly+' name="drAmount[' + rowNumber + ']" class="form-control input-sm drAmount" value="' + nullCheck(drAmount) + '">';
        var crAmountCol = '<input type="text" '+crAmountReadonly+' name="crAmount[' + rowNumber + ']" class="form-control input-sm crAmount" value="' + nullCheck(crAmount) + '">';
        var drcrTypeCol = '<input type="hidden" readonly  name="drcrType[' + rowNumber + ']" class="form-control input-sm drcrType" value="' + nullCheck(drcrType) + '">';
        var actionCol = '<div class="btn-group"><button class="btn btn-white accVcrDtlsDeleteDtlBtn" ><i class="fa fa-trash text-danger"></i></button>';
        $('#accVcrDtls-table > tbody').prepend('<tr><td>' + rowNumber + idCol + drcrTypeCol+ '</td><td>' + accCoaCol + '</td><td>' + stakeholderCol + '</td><td>' + drAmountCol + '</td><td>' + crAmountCol + '</td><td>' + actionCol + '</td></tr>');
           $('[name*="accCoaId[' + rowNumber + ']"]').rules('add', {required: true});
           $('[name*="drAmount[' + rowNumber + ']"]').rules('add', {required: true});
           $('[name*="crAmount[' + rowNumber + ']"]').rules('add', {required: true});
    }

    function checkDuplicate(newItemId) {
        var existingItem;
        var isError = false;
        $("#accVcrDtls-table tbody tr").each(function () {
            existingItem = $(this).find(".accCoaId").val();
            if (newItemId == existingItem) {
                isError = true;
            }
        });
        return isError;
    }

    function onchangebind() {
        $(".drAmount, .crAmount").change(function (e) {
            // var selectRow = $(this).parents('tr');
            // var quantityReg = floatConverter(selectRow.find(".quantityReg").val());
            // var quantitySps = floatConverter(selectRow.find(".quantitySps").val());
            // var unitPriceReg = floatConverter(selectRow.find(".unitPriceReg").val());
            // var unitPriceSpc = floatConverter(selectRow.find(".unitPriceSpc").val());
            // var discount = floatConverter(selectRow.find(".discount").val());
            // selectRow.find(".quantityTotal").val(quantityReg + quantitySps);
            // var lineTotal = (quantityReg * unitPriceReg) + (quantitySps * unitPriceSpc);
            // selectRow.find(".lineTotal").val(lineTotal);
            // selectRow.find(".netTotal").val(lineTotal - discount);
            totalAmount();
            e.preventDefault();
        });

        $("button.accVcrDtlsDeleteDtlBtn").on("click", function (e) {
            var selectRow = $(this).parents('tr');
            var rowId = selectRow.find(".id").val();
            if (!(rowId == '')) {
                var idCol = '<input type="hidden" class="id" name="accVcrDtlsDeleteSet[' + rowNumber + '].id" value="' + rowId + '">';
                $('#accVcrDtls-table > tbody').append('<tr style="visibility:collapse"><td>' + (rowNumber++) + idCol + '</td></tr>');
            }
            selectRow.remove();
            totalAmount();
            e.preventDefault();
        });
    }

    function totalAmount() {
        var drAmountSum = 0;
        var crAmountSum = 0;
        $("#accVcrDtls-table  tbody tr").each(function () {
            drAmountSum += floatConverter($(this).find(".drAmount").val());
            crAmountSum += floatConverter($(this).find(".crAmount").val());
        });
        $('#drAmountSum').html(drAmountSum);
        $('#crAmountSum').html(crAmountSum);
        $('#drAmountSumInput').val(drAmountSum);
        $('#crAmountSumInput').val(crAmountSum);
        if(drAmountSum==crAmountSum){
            saveBtn.prop('disabled', false);
        }else{
            saveBtn.prop('disabled', true);
        }
    }

    function resetMasterForm() {
        $('#hiddenUpId').val('');
        $('#code').val('');
        $('#accPeriod').select2('val', '');
        $('#businessUnit').val('');
        $('#narration').val('');
        $('#referenceCode').val('');
        $('#transDate').datepicker('setDate', new Date());
    }

    function resetDetailForm() {
        $('#accVcrDtls_accCoa').select2('val', '');
        $('#accVcrDtls_stakeHolderType').val('');
        $("#accVcrDtls_stakeholder").empty().append('<option value=""></option>').val('').trigger('change');
        $("#select2-accVcrDtls_stakeholder-container").html('');
        $('#accVcrDtls_drCrAmount').val('');
    }
</script>

