
    <div class="modal fade common-form" id="accPeriodModalFormId">
        <div class="modal-dialog modal-md">
            <div class="modal-content form-bg">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="modal-area">
                        <form class="form-horizontal form-body-md col-md-12 column-one-form common-form"
                              id="defaultForm" autocomplete="off">
                            <div class="text-center panel panel-default mb-0 crowlr-4">
                                <h3 id="headerTitle" class="m-0 p-1"><?php echo get_msg('order_fees')?></h3>
                            </div>

                            <div id="defaultMsgDiv"></div>

                            <div class="column-one-form-down">

                                <div class="row crowmlr-8">
                                    <input type="hidden" id="hiddenUpId" name="id"/>

                                    <div class="col-md-12 column-one p-1">
                                        <div class="panel panel-default">
                                            <fieldset>
                                                <legend><?php echo get_msg('order_fees')?></legend>
                                                <div class="row mb-1">
                                                    <label class="col-sm-4 control-label"><?php echo get_msg('scope')?> <span
                                                                class="text-warning">*</span> :</label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <label class="checkbox-inline i-checks"><input
                                                                    id="scopeTrue" type="radio" value="0"
                                                                    name="scope" checked>&nbsp;&nbsp;<?php echo get_msg('global')?></label>
                                                        <label class="checkbox-inline i-checks"><input
                                                                    id="scopeFalse"  type="radio" value="1"
                                                                    name="scope">&nbsp;&nbsp;<?php echo get_msg('shop')?></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('shop')?>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                            <select class="form-control select2 selectTwo input-sm" name="shop_for_cost" id="shop_for_cost" disabled>
                                                                <option value=""><?php echo get_msg('shop')?></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('initial_amount')?><span class="text-warning">*</span>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                        <input type="number" id="initial_amount" min="0" name="initial_amount" class="form-control input-sm">
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('isExtraCharge')?> <span
                                                                class="text-warning">*</span> :</label>

                                                        <div class="col-sm-8 fmi_box">
                                                            <label class="checkbox-inline i-checks"><input
                                                                    id="isExtraChargeTrue" type="radio" value="1"
                                                                    name="isExtraCharge">&nbsp;&nbsp;<?php echo get_msg('yes')?></label>
                                                            <label class="checkbox-inline i-checks"><input
                                                                    id="isExtraChargeFalse" checked type="radio" value="0"
                                                                    name="isExtraCharge">&nbsp;&nbsp;<?php echo get_msg('no')?></label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('per_kilo_amount')?>  <span class="text-warning">*</span>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                        <input type="number" id="per_kilo_amount" min="0" name="per_kilo_amount" class="form-control input-sm" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('maximum_amount')?><span class="text-warning">*</span>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                            <input type="number" id="maximum_amount" min="0" name="maximum_amount" class="form-control input-sm">
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('extraChargeOutCityIfCash')?>  :</label>

                                                        <div class="col-sm-8 fmi_box">
                                                            <input type="number" id="extraChargeOutCityIfCash" min="0" name="extraChargeOutCityIfCash" class="form-control input-sm">
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('Remark')?>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                            <textarea id="remark" name="remark"
                                                                class="form-control input-sm"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row crowmlr-8">
                                <div class="col-md-12 p-1">
                                    <div class="panel panel-default mb-0">
                                        <div class="text-right p-2">
                                            <button type="button" class="btn btn-default" data-toggle="tooltip"
                                                id="reset-btn"><?php echo get_msg('Reset')?></button>
                                            <button type="submit" class="btn btn-primary" data-toggle="tooltip"
                                                id="save-btn"><?php echo get_msg('Save')?>... <i
                                                    class="fa fa-recycle"></i></button>
                                            <button type="button" class="btn btn-warning cancel-btn"
                                                data-dismiss="modal" id="cancel-btn"><?php echo get_msg('Cancel')?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

