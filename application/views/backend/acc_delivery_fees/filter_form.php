<form class="form-horizontal form-body-md col-md-12 column-two-form common-form report-form-area" id="defaultFormRpt">
    <div class="column-two-form-down report-form">
        <div class="row crowmlr-8">
            <div class="col-md-12 column-one p-1">
                <div class="panel panel-default">

                    <div class="form-group">
                        <div class="row">

                            <div class="col-md-2"><div class="row mb-1">
                                    <label class="col-sm-4 control-label"><?php echo get_msg('scope')?> <span
                                                class="text-warning">*</span> :</label>

                                    <div class="col-sm-8 fmi_box">
                                        <label class="checkbox-inline i-checks"><input
                                                    id="filterscopeTrue" type="radio" value="0"
                                                    name="scope" checked>&nbsp;&nbsp;<?php echo get_msg('global')?></label>
                                        <label class="checkbox-inline i-checks"><input
                                                    id="filterscopeFalse"  type="radio" value="1"
                                                    name="scope">&nbsp;&nbsp;<?php echo get_msg('shop')?></label>
                                    </div>
                                </div></div>
                            <div class="col-md-2">
                                <div class="row mb-1 column-three">
                                    <label class="col-sm-4 control-label"><?php echo get_msg('shop')?>:</label>
                                    <div class="col-sm-8 fmi_box">
                                        <select class="form-control select2 selectTwo input-sm" name="shopRpt" id="shopRpt" disabled>
                                            <option value=""><?php echo get_msg('shop')?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row crowmlr-8">
            <div class="col-md-12 p-1">
                <div class="panel panel-default mb-0 report-btn">
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary mb-0" data-toggle="tooltip" id="show-btn"><?php echo get_msg('show')?>...<i class="fa fa-recycle"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
