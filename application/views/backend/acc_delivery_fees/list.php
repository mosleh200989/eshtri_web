<div class="container-fluid" style="padding: 10px 30px 10px 30px;">
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <ul class="nav nav-tabs" id="myTab">
            <li class="nav-item"><a class="nav-link active" href="#global_delivery_fees" value="global_delivery_fees" data-toggle="tab"><?php echo get_msg('delivery_fee_Information')?></a></li>
            <li class="nav-item"><a class="nav-link" href="#delivery_fees_offer"  value="delivery_fees_offer" data-toggle="tab"><?php echo get_msg('delivery_fee_offers')?></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="global_delivery_fees">
                <div class="row">
                    <div class="ibox-title">
                        <div class="ct-btn">
                            <a href="#" class="btn btn-primary btn-sm form-bg" id="btn-icon" data-toggle="modal" data-target="#accPeriodModalFormId">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>

                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox cc-box">
                            <?php include 'filter_form.php';?>
                            <div class="ibox-content">
                                <div class="tabs-containers">
                                    <div class="table-responsive tblc">
                                        <table class="table table-stripped table-bordered" id="list-table">
                                            <thead>
                                            <tr>
                                                <th class="sl-icon">
                                                    <?php echo get_msg('SL')?>
                                                </th>
                                                <th class="code-icon">
                                                    <?php echo get_msg('initial_amount')?>
                                                </th>
                                                <th>
                                                    <?php echo get_msg('isExtraCharge')?>
                                                </th>
                                                <th>
                                                    <?php echo get_msg('per_kilo_amount')?>
                                                </th>
                                                <th>
                                                    <?php echo get_msg('maximum_amount')?>
                                                </th>
                                                <th>
                                                    <?php echo get_msg('extraChargeOutCityIfCash')?>
                                                </th>
                                                <th>
                                                    <?php echo get_msg('created_date')?>
                                                </th>
                                                <th>
                                                    <?php echo get_msg('is_current')?>
                                                </th>
                                                <th>
                                                    <?php echo get_msg('Action')?>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="delivery_fees_offer">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox cc-box">
                            <div class="ibox-content">
                                <form class="form-horizontal form-body-md col-md-12 column-one-form common-form"
                                      id="offerForm" autocomplete="off">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('scope')?> <span
                                                                    class="text-warning">*</span> :</label>

                                                        <div class="col-sm-8 fmi_box">
                                                            <label class="checkbox-inline i-checks"><input
                                                                        id="offerscopeTrue" type="radio" value="0"
                                                                        name="offerscope" checked>&nbsp;&nbsp;<?php echo get_msg('global')?></label>
                                                            <label class="checkbox-inline i-checks"><input
                                                                        id="offerscopeFalse"  type="radio" value="1"
                                                                        name="offerscope">&nbsp;&nbsp;<?php echo get_msg('shop')?></label>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="row mb-1">
                                                    <label class="col-sm-4 control-label"><?php echo get_msg('shop')?>:</label>
                                                    <div class="col-sm-8 fmi_box">
                                                        <select class="form-control select2 selectTwo input-sm" name="shop_for_offers" id="shop_for_offers" disabled>
                                                            <option value=""><?php echo get_msg('shop')?></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="tabs-containers">
                                    <div class="table-responsive tblc">
                                        <table class="table table-stripped table-bordered" id="offers-table">
                                            <thead>
                                            <tr>
                                                <th class="sl-icon" width="60px">
                                                    <?php echo get_msg('default')?>
                                                </th>
                                                <th class="code-icon">
                                                    <?php echo get_msg('discription')?>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <label class="checkbox-inline i-checks">
                                                        <input
                                                                id="isDefaultTrue0" type="radio" value="0"
                                                                name="isDefaultTrue">
                                                        <input
                                                                id="offerTypeZero" type="hidden" value="0"
                                                                name="offerType">

                                                        &nbsp</label>
                                                </td>
                                                <td><p><?php echo get_msg('no_delivery_offer')?></p></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php
                                                    $defaultChecked='';
                                                    $offerDescOne = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 1, 'offerBy' => 'global'));
                                                    if($offerDescOne->is_current == 1){
                                                        $defaultChecked='checked';
                                                    }
                                                    ?>
                                                    <label class="checkbox-inline i-checks">
                                                        <input
                                                                id="isDefaultTrue1" type="radio" value="1"
                                                                name="isDefaultTrue" <?=$defaultChecked?>>
                                                        <input
                                                                id="offerTypeOne" type="hidden" value="1"
                                                                name="offerTypeOne">

                                                        &nbsp</label>
                                                    </td>
                                                <td><p><?php echo get_msg('delivery_fees_free')?></p></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php
                                                    $defaultChecked='';
                                                    $offerDescOne = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 2, 'offerBy' => 'global'));
                                                    if($offerDescOne->is_current == 1){
                                                        $defaultChecked='checked';
                                                    }
                                                    ?>
                                                    <label class="checkbox-inline i-checks"><input
                                                                id="isDefaultTrue2" type="radio" value="2"
                                                                name="isDefaultTrue" <?=$defaultChecked?>>&nbsp
                                                        <input
                                                                id="offerTypeTwo" type="hidden" value="2"
                                                                name="offerTypeTwo">
                                                    </label>
                                                </td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-sm-1"><p><?php echo get_msg('delivery_fees_free_first')?></p></div>
                                                        <div class="col-sm-1 fmi_box">
                                                            <input type="number" id="freeFirstCustomer" min="0" name="freeFirstCustomer" class="form-control input-sm"  value="<?=$offerDescOne->freeFirstCustomer?>">
                                                        </div>
                                                        <div class="col-sm-1"><p><?php echo get_msg('date')?></p></div>
                                                        <div class="col-sm-1 fmi_box">
                                                            <input type="text" id="freeFirstDate" name="freeFirstDate" class="form-control input-sm" value="<?=$offerDescOne->freeFirstDate?>">
                                                        </div>
                                                    </div>
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php
                                                    $defaultChecked='';
                                                    $offerDescOne = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 3, 'offerBy' => 'global'));
                                                    if($offerDescOne->is_current == 1){
                                                        $defaultChecked='checked';
                                                    }
                                                    ?>
                                                    <label class="checkbox-inline i-checks"><input
                                                                id="isDefaultTrue3" type="radio" value="3"
                                                                name="isDefaultTrue" <?=$defaultChecked?>>&nbsp<input
                                                                id="offerTypeThree" type="hidden" value="3"
                                                                name="offerTypeThree"></label>
                                                </td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-sm-1 fmi_box">
                                                                <input type="number" id="percentDiscountAll" min="0" name="percentDiscountAll" class="form-control input-sm" value="<?=$offerDescOne->percentDiscountAll?>">
                                                        </div>
                                                        <div class="col-sm-11"><p>% <?php echo get_msg('percent_discount_for_all')?></p></div>
                                                    </div>

                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <?php
                                                    $defaultChecked='';
                                                    $offerDescOne = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 4, 'offerBy' => 'global'));
                                                    if($offerDescOne->is_current == 1){
                                                        $defaultChecked='checked';
                                                    }
                                                    ?>
                                                    <label class="checkbox-inline i-checks"><input
                                                                id="isDefaultTrue4" type="radio" value="4"
                                                                name="isDefaultTrue" <?=$defaultChecked?>>&nbsp<input
                                                                id="offerTypeFour" type="hidden" value="4"
                                                                name="offerTypeFour"></label>
                                                </td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-sm-1 fmi_box">
                                                            <input type="number" id="percentDiscountAmnt" min="0" name="percentDiscountAmnt" class="form-control input-sm" value="<?=$offerDescOne->percentDiscountAmnt?>">
                                                        </div>
                                                        <div class="col-sm-2"><p>% <?php echo get_msg('percent_discount_if_order_exceed_amount')?></p></div>
                                                        <div class="col-sm-1 fmi_box">
                                                            <input type="number" id="ifExceedAmount" min="0" name="ifExceedAmount" class="form-control input-sm" value="<?=$offerDescOne->ifExceedAmount?>">
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <?php
                                                    $defaultChecked='';
                                                    $offerDescOne = $this->Acc_delivery_fee_offers->get_one_by( array( 'offerType' => 5, 'offerBy' => 'global'));
                                                    if($offerDescOne->is_current == 1){
                                                        $defaultChecked='checked';
                                                    }
                                                    ?>
                                                    <label class="checkbox-inline i-checks"><input
                                                                id="isDefaultTrue5" type="checkbox" value="5"
                                                                name="isEligibleFreeAmountTrue" <?=$defaultChecked?> >&nbsp<input
                                                                id="offerTypeFive" type="hidden" value="5"
                                                                name="offerTypeFive"></label>
                                                </td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-sm-2"><p> <?php echo get_msg('delivey_free_if_amount_exceed_eligible_free_shipping')?></p></div>
                                                        <div class="col-sm-1 fmi_box">
                                                            <input type="number" id="eligibleFreeAmount" min="0" name="eligibleFreeAmount" class="form-control input-sm" value="<?=$offerDescOne->eligibleFreeAmount?>">
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                                    <div class="row crowmlr-8">
                                        <div class="col-md-12 p-1">
                                            <div class="panel panel-default mb-0">
                                                <div class="text-right p-2">

                                                    <button type="submit" class="btn btn-primary" data-toggle="tooltip"
                                                            id="offer-save-btn"><?php echo get_msg('Save')?>... <i
                                                                class="fa fa-recycle"></i></button>
                                                    <button type="button" class="btn btn-warning cancel-btn"
                                                            data-dismiss="modal" id="offer-cancel-btn"><?php echo get_msg('Cancel')?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


</div>
</div>

<?php include 'entry_form.php';?>
<?php include 'period_view.php';?>

<script type="text/javascript">
$(document).ready(function() {
    var saveBtn = $('#save-btn');
    var resetBtn = $('#reset-btn');
    var listTable = $('#list-table');
    var defaultForm = $('#defaultForm');
    var offerForm = $('#offerForm');
    $("#show-btn").on("click", function (e) {
        listTable.DataTable().ajax.reload();
        e.preventDefault();
    });
    listTable.dataTable({
                language: {
                    processing: "<?php echo get_msg('processing')?>...",
                    search: "<?php echo get_msg('search')?>",
                    lengthMenu: "<?php echo get_msg('display _menu_records_per_page')?>",
                    zeroRecords: "<?php echo get_msg('nothing_found_sorry')?>",
                    info: "<?php echo get_msg('showing_page_page_of_pages')?>",
                    infoEmpty: "<?php echo get_msg('no_records_available')?>",
                    infoFiltered: "<?php echo get_msg('filtered_from_max_total_records')?>",
                    paginate: {
                        first: "<?php echo get_msg('first')?>",
                        previous: "<?php echo get_msg('previous')?>",
                        next: "<?php echo get_msg('next')?>",
                        last: "<?php echo get_msg('last')?>"
                    },
                },
                "processing": true,
                "serverSide": true,
                "bAutoWidth": false,
                "iDisplayLength": 25,
                "order": [0, "desc"],
                "sAjaxSource": "<?php echo site_url('/admin/orderfees/tablelist');?>",
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    if (aData.DT_RowId == undefined) {
                        return true;
                    }
                    $('td:eq(8)', nRow).html(getOfferActionButtons(nRow, aData));
                    return nRow;
                },
                "fnServerParams": function (aoData) {
                    aoData.push({"name": "listAction", "value": true},
                        {"name": "scope", "value": $('input[name="scope"]:checked').val()},
                        {"name": "shop_id", "value": $('#shopRpt').val()},
                        // {"name": "trans_date_to", "value": $('#transDateToRpt').val()},
                        // {"name": "acc_period", "value": $('#accPeriodRpt').val()},
                        // {"name": "confirm_status", "value": $('#confirmStatusRpt').val()},
                        // {"name": "stake_holder_type", "value": $('#accVcrDtls_stakeHolderTypeRpt').val()},
                        // {"name": "stakeholder_name", "value": $('#accVcrDtls_stakeholderRpt').val()},
                        // {"name": "acc_coa", "value": $('#accCoaRpt').val()},
                        // {"name": "coa_class", "value": $('#coaClassRpt').val()},
                        );
                },
                "aoColumns": [
                    null,
                    null,
                    null,
                    {"bSortable": false},
                    {"bSortable": false},
                    null,
                    null,
                    null,
                    {"bSortable": false},
                ]
            });
            defaultForm.validate({
            lang: langCode,
            ignore: [],
            rules: {
                initial_amount: {
                    required: true,
                    number: true,
                    min:0,
                },
                per_kilo_amount: {
                    required: function(element) {
                        return $('#isTaxableTrue').is(':checked')
                    },
                    number:true,
                    max:100,
                    min:0,
                },
                scope: {
                    required: function(element) {
                        return $('#scopeFalse').is(':checked')
                    }
                },
            },
            submitHandler: function (form) {
                    var url;
                    if ($('#hiddenUpId').val() == '') {
                        url = "<?php echo site_url('/admin/orderfees/writesubmit');?>";
                    } else {
                        url = "<?php echo site_url('/admin/orderfees/editsubmit');?>";
                    }
                    formAfterSubmit(saveBtn, "<?php echo get_msg('processing')?>")
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: defaultForm.serialize(),
                        success: function (data) {
                            formSubmitReturnMsg(saveBtn, data, listTable, defaultForm, "<?php echo $action_title;?>", "<?php echo get_msg('save')?>");
                            if (data.isError == true) {
                                resetMasterForm();
                            }
                        },
                        failure: function (data) {
                        }
                    })
                    return false;
                }
        });

    offerForm.validate({
        lang: langCode,
        ignore: [],
        rules: {
            freeFirstCustomer: {
                required: true,
                number: true,
                min:0,
            },
        },
        submitHandler: function (form) {
            var url;
                url = "<?php echo site_url('/admin/orderfees/writeofferssubmit');?>";
            formAfterSubmit(saveBtn, "<?php echo get_msg('processing')?>")
            $.ajax({
                url: url,
                type: 'POST',
                dataType: "json",
                data: offerForm.serialize(),
                success: function (data) {
                    confirmNormalMessages(data.isError, data.message, false);
                },
                failure: function (data) {
                }
            })
            return false;
        }
    });

        listTable.on('click', 'a.show-reference', function (e) {
            var control = this;
            var referenceId = $(control).attr('referenceid');
            jQuery.ajax({
                type: 'GET',
                dataType: 'JSON',
                data: {id: referenceId, showAction: true},
                url: "<?php echo site_url('/admin/orderfees');?>",
                success: function (data, textStatus) {
                    if (data.isError == false) {
                        $("#view-initial_amount").html("<strong>" + (data.defaultInstance.initial_amount != null) ? (data.defaultInstance.initial_amount) : '' + "</strong>");
                        $("#view-isExtraCharge").html("<strong>" + (data.defaultInstance.isExtraCharge != null) ? (data.isExtraCharge) : '' + "</strong>");

                        if(data.defaultInstance.scope=='shop'){
                            $("#view-scope").html("<strong>" + (data.defaultInstance.scope != null) ? ("<?php echo get_msg('shop')?>")  : '' + "</strong>");
                            $("#view-shop_id").html("<strong>" + (data.defaultInstance.shop_id != null) ? (data.shop_name)  : '' + "</strong>");
                            $("#view-shop_id").show();
                        }else{
                            $("#view-shop_id").hide();
                            $("#view-shop_id").html("");
                            $("#view-scope").html("<strong>" + (data.defaultInstance.scope != null) ? ("<?php echo get_msg('global')?>")  : '' + "</strong>");
                        }

                        if(data.defaultInstance.isExtraCharge=='1'){
                            $("#view-per_kilo_amount").html("<strong>" + (data.defaultInstance.per_kilo_amount != null) ? (data.defaultInstance.per_kilo_amount)  : '' + "</strong>");
                            $("#view-per_kilo_amount").show();
                        }else{
                            $("#view-per_kilo_amount").hide();
                            $("#view-per_kilo_amount").html("");
                        }
                        $("#view-maximum_amount").html("<strong>" + (data.defaultInstance.maximum_amount != null) ? (data.defaultInstance.maximum_amount) : '' + "</strong>");
                        $("#view-extraChargeOutCityIfCash").html("<strong>" + (data.defaultInstance.extraChargeOutCityIfCash != null) ? (data.defaultInstance.extraChargeOutCityIfCash) : '' + "</strong>");
                        $("#view-remark").html("<strong>" + (data.defaultInstance.remark != null) ? (data.defaultInstance.remark) : '' + "</strong>");
                        $('#accPeriodModalViewId').modal('show');
                    } else {
                        alert(data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    serverErrorToast(errorThrown);
                }
            });
            e.preventDefault();
        });

        listTable.on('click', 'a.update-reference', function (e) {
                var control = this;
                var referenceId = $(control).attr('referenceid');
                jQuery.ajax({
                    type: 'GET',
                    dataType: 'JSON',
                    data: {id: referenceId, editData: true},
                    url: "<?php echo site_url('/admin/orderfees');?>",
                    success: function (data, textStatus) {
                        if (data.isError == false) {
                            $('#defaultMsgDiv').html('');
                            $('#hiddenUpId').val(data.defaultInstance.id);
                            $('#initial_amount').val(data.defaultInstance.initial_amount);
                            $('#maximum_amount').val(data.defaultInstance.maximum_amount);
                            $('#extraChargeOutCityIfCash').val(data.defaultInstance.extraChargeOutCityIfCash);
                            if(data.defaultInstance.scope=='shop'){
                                $("#scopeFalse").prop("checked", true);
                                $('#shop_for_cost').val(data.defaultInstance.shop_id);
                                $('#shop_for_cost').select2("val",data.defaultInstance.shop_id);
                                $('#shop_for_cost').select2("data",data.defaultInstance.shop_id);
                                $("#shop_for_cost").empty().append('<option value="' + (data.defaultInstance.shop_id != null ? data.defaultInstance.shop_id : '') + '">' + (data.shop_name) + '</option>').val(data.defaultInstance.shop_id != null ? data.defaultInstance.shop_id : '').trigger('change');
                                $("#select2-shop_for_cost-container").html(data.shop_name);
                                $('#shop_for_cost').prop('disabled', false);
                            }else{
                                $("#scopeTrue").prop("checked", true);
                                $('#shop_for_cost').prop('disabled', true);
                                $('#shop_for_cost').val("");
                            }
                            if(data.defaultInstance.isExtraCharge=='1'){
                                $("#isExtraChargeTrue").prop("checked", true);
                                $('#per_kilo_amount').val(data.defaultInstance.per_kilo_amount);
                                $('#per_kilo_amount').prop('disabled', false);
                            }else{
                                $("#isExtraChargeFalse").prop("checked", true);
                                $('#per_kilo_amount').prop('disabled', true);
                                $('#per_kilo_amount').val("");
                            }

                            $("#remark").val((data.defaultInstance.remark != null) ? (data.defaultInstance.remark) : '');
                            $('#accPeriodModalFormId').modal('show');
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        serverErrorToast(errorThrown);
                    }
                });
                e.preventDefault();
            });


            listTable.on('click', 'a.confirm-reference', function (e) {
                var selectRow = $(this).parents('tr');
                var data = selectRow.find("td:eq(1)").text();
                var confirmDel = confirm("<?php echo get_msg('are_you_sure_want_to_active_this_fees')?> " + data + ' ??');
                if (confirmDel == true) {
                    var control = this;
                    var referenceId = $(control).attr('referenceid');
                    var scope = $(control).attr('scope');
                    var shop_id = $(control).attr('shop_id');
                    jQuery.ajax({
                        type: 'POST',
                        dataType: 'JSON',
                        data: {id: referenceId, scope:scope, shop_id:shop_id, confirmStatus: 'true'},
                        url: "<?php echo site_url('/admin/orderfees/confirmsubmit');?>",
                        success: function (data, textStatus) {
                            confirmRowMessages(data.isError, data.message, listTable);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            serverErrorToast(errorThrown);
                        }
                    });
                }
                e.preventDefault();
            });

            function resetMasterForm() {
                }
                clickDsbl('#offerscopeTrue', '#shop_for_offers');
                clickDsbl('#offerscopeFalse', '#shop_for_offers');

                clickDsbl('#scopeTrue', '#shop_for_cost');
                clickDsbl('#scopeFalse', '#shop_for_cost');

                clickDsbl('#filterscopeTrue', '#shopRpt');
                clickDsbl('#filterscopeFalse', '#shopRpt');

                clickDsbl('#isExtraChargeTrue', '#per_kilo_amount');
                clickDsbl('#isExtraChargeFalse', '#per_kilo_amount');
                function clickDsbl($clickId, $dsblfiel) {
                $($clickId).on('change', function () {
                    $($dsblfiel).val("");
                    if ($(this).is(':checked') && $(this).val()=='1') {
                        $($dsblfiel).prop('disabled', false);
                    } else {
                        $($dsblfiel).prop('disabled', true);
                    }
                });
            }

});
$(document).ready(function () {
    function FetchData() {
        $("#shop_for_cost").select2({
            ajax: {
                url: "<?php echo site_url('/admin/expense_voucher');?>",
                dataType: 'JSON',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                        stakeHolderType: "Seller",
                        stakeholderSearch: 'true'
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: "<?php echo get_msg('shop')?>...",
            allowClear: true,
            width: '100%',
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
        $("#shop_for_offers").select2({
            ajax: {
                url: "<?php echo site_url('/admin/expense_voucher');?>",
                dataType: 'JSON',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                        stakeHolderType: "Seller",
                        stakeholderSearch: 'true'
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: "<?php echo get_msg('shop')?>...",
            allowClear: true,
            width: '100%',
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });

        $(document.body).on("change","#shop_for_offers",function(){
            var shopId = $(this).val();
            jQuery.ajax({
                type: 'GET',
                dataType: 'JSON',
                data: {shop_id: shopId, scope:$('input[name="offerscope"]:checked').val(), loadOfferData: true},
                url: "<?php echo site_url('/admin/orderfees');?>",
                success: function (data, textStatus) {
                    if (data.isError == false) {
                        if(data.offerDescOne.is_current==1){
                            $("#isDefaultTrue1").prop("checked", true);
                        }
                        if(data.offerDescTwo.is_current==1){
                            $("#isDefaultTrue2").prop("checked", true);
                        }
                        if(data.offerDescThree.is_current==1){
                            $("#isDefaultTrue3").prop("checked", true);
                        }
                        if(data.offerDescFour.is_current==1){
                            $("#isDefaultTrue4").prop("checked", true);
                        }
                        if(data.offerDescFive.is_current==1){
                            $("#isDefaultTrue5").prop("checked", true);
                        }
                        $('#freeFirstCustomer').val(data.offerDescTwo.freeFirstCustomer);
                        $('#freeFirstDate').val(data.offerDescTwo.freeFirstDate);
                        $('#percentDiscountAll').val(data.offerDescThree.percentDiscountAll);
                        $('#percentDiscountAmnt').val(data.offerDescFour.percentDiscountAmnt);
                        $('#ifExceedAmount').val(data.offerDescFour.ifExceedAmount);
                        $('#eligibleFreeAmount').val(data.offerDescFive.eligibleFreeAmount);
                    } else {
                        alert(data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    serverErrorToast(errorThrown);
                }
            });

        });
        $("#shopRpt").select2({
            ajax: {
                url: "<?php echo site_url('/admin/expense_voucher');?>",
                dataType: 'JSON',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                        stakeHolderType: "Seller",
                        stakeholderSearch: 'true'
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: "<?php echo get_msg('shop')?>...",
            allowClear: true,
            width: '100%',
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
        // do something
    }
    setTimeout(FetchData, 3000);

    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        return "<div class='row slt-srs m-0'><div class='col-md-2 thumbnail sc-img p-0'>" + repo.imgsrc + "</div><div class='col-md-10 sc-con pl-3 pr-1'><span class='scsm-gp'><span class='sc-code lft'>" + repo.caption + '</span><span class="sc-phn rit">' + repo.phone + '</span></span><span class="scsm-gp"><span class="sc-capt lft">' + repo.phone + '</span></span><span class="scsm-gp"><span class="sc-eml lft">' + repo.email + "</span></span></div></div>";
    }

    function formatRepoSelection(repo) {
        if (repo.id === '' || repo.phone== undefined) {
            return "<?php echo get_msg('stakeholder')?>...";
        }
        $('#carriedByCaption').val(repo.caption);
        return repo.phone + ' ' + repo.caption;
    }
});

</script>

