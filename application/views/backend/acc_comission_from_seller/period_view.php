<div class="modal fade common-form" id="accPeriodModalViewId">
    <div class="modal-dialog modal-md">
        <div class="modal-content form-bg">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">×</button>
            </div>

            <div class="modal-body">
                <div class="modal-area">
                    <div class="form-horizontal form-body-md col-md-12 column-one-form  common-form custom-form-view">
                        <div class="text-center panel panel-default mb-0 crowlr-4">
                            <h3 id="headerTitle" class="m-0 p-1"><?php echo get_msg('comission_from_seller')?></h3>
                        </div>

                        <div id="defaultMsgDiv"></div>

                        <div class="column-one-form-down form-view">

                            <div class="row crowmlr-8">
                                <div class="col-md-12 column-one p-1">
                                    <div class="panel panel-default">
                                        <fieldset>
                                            <legend><?php echo get_msg('comission_from_seller')?></legend>

                                            <div class="form-group">
                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span
                                                            class="vmiddle"><?php echo get_msg('shop_name')?>:
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-shop_id"></span>
                                                    </div>
                                                </div>                                    
                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span
                                                            class="vmiddle"><?php echo get_msg('comission_percentage')?>:
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-amount_percentage"></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1 cbwm0">
                                                    <label class="col-sm-4 control-label"><span
                                                            class="vmiddle"><?php echo get_msg('Remark')?> :
                                                    </label>

                                                    <div class="col-sm-8 fmi_box">
                                                        <span class="fview" id="view-remark"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row  crowmlr-8">
                            <div class="col-md-12 p-1">
                                <div class="panel panel-default mb-0">
                                    <div class="text-right p-2">
                                        <button type="button" class="btn btn-warning cancel-btn" id="cancel-btn"
                                            data-dismiss="modal"><?php echo get_msg('Cancel')?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
