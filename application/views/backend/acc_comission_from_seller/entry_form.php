
    <div class="modal fade common-form" id="accPeriodModalFormId">
        <div class="modal-dialog modal-md">
            <div class="modal-content form-bg">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="modal-area">
                        <form class="form-horizontal form-body-md col-md-12 column-one-form common-form"
                              id="defaultForm" autocomplete="off">
                            <div class="text-center panel panel-default mb-0 crowlr-4">
                                <h3 id="headerTitle" class="m-0 p-1"><?php echo get_msg('comission_from_seller')?></h3>
                            </div>

                            <div id="defaultMsgDiv"></div>

                            <div class="column-one-form-down">

                                <div class="row crowmlr-8">
                                    <input type="hidden" id="hiddenUpId" name="id"/>

                                    <div class="col-md-12 column-one p-1">
                                        <div class="panel panel-default">
                                            <fieldset>
                                                <legend><?php echo get_msg('comission_from_seller')?></legend>

                                                <div class="form-group">
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('shops')?><span class="text-warning">*</span>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                            <select class="form-control selectTwo input-sm" id="shop_id" name="shop_id">
                                                            <option value=""><?php echo get_msg('select_shop')?></option>
                                                            <?php
                                                            foreach ($sellerList['items'] as $item)
                                                            {
                                                            echo '<option value="'.$item['id'].'">'.$item['caption'].' - '.$item['phone'].'</option>';
                                                            }
                                                            ?>
                                                            </select>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('comission_percentage')?><span class="text-warning">*</span>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                        <input type="number" id="amount_percentage" min="0" name="amount_percentage" class="form-control input-sm">
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('Remark')?>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                            <textarea id="remark" name="remark"
                                                                class="form-control input-sm"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row crowmlr-8">
                                <div class="col-md-12 p-1">
                                    <div class="panel panel-default mb-0">
                                        <div class="text-right p-2">
                                            <button type="button" class="btn btn-default" data-toggle="tooltip"
                                                id="reset-btn"><?php echo get_msg('Reset')?></button>
                                            <button type="submit" class="btn btn-primary" data-toggle="tooltip"
                                                id="save-btn"><?php echo get_msg('Save')?>... <i
                                                    class="fa fa-recycle"></i></button>
                                            <button type="button" class="btn btn-warning cancel-btn"
                                                data-dismiss="modal" id="cancel-btn"><?php echo get_msg('Cancel')?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

