<div class="container-fluid" style="padding: 10px 30px 10px 30px;">
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <?php 
        // echo "<pre>";
        // print_r($sellerList['items']);
        // echo "</pre>";
    ?>
    <div class="row">
        <div class="ibox-title">
            <div class="ct-btn">
                <a href="#" class="btn btn-primary btn-sm form-bg" id="btn-icon" data-toggle="modal" data-target="#accPeriodModalFormId">
                    <i class="fa fa-plus"></i>
                </a>
            </div>

            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
   </div>

 

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox cc-box">
                <?php include 'filter_form.php';?>
                <div class="ibox-content">
                    <div class="tabs-containers">
                        <div class="table-responsive tblc">
                            <table class="table table-stripped table-bordered" id="list-table">
                                <thead>
                                    <tr>
                                        <th class="sl-icon">
                                            <?php echo get_msg('SL')?>
                                        </th>
                                        <th class="code-icon">
                                            <?php echo get_msg('shop_name')?>
                                        </th>
                                        <th>
                                            <?php echo get_msg('shop_address')?>
                                        </th>
                                        <th>
                                            <?php echo get_msg('comission_percentage')?>
                                        </th>
                                        <th>
                                            <?php echo get_msg('created_by')?>  
                                        </th>
                                        <th>
                                            <?php echo get_msg('created_date')?>  
                                        </th>
                                         <th> <?php echo get_msg('updated_date')?></th> 
                                         <th> <?php echo get_msg('updated_by')?></th> 
                                        <th>
                                            <?php echo get_msg('Action')?>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

<?php include 'entry_form.php';?>
<?php include 'period_view.php';?>

<script type="text/javascript">
$(document).ready(function() {
    var saveBtn = $('#save-btn');
    var resetBtn = $('#reset-btn');
    var listTable = $('#list-table');
    var defaultForm = $('#defaultForm');
    var offerForm = $('#offerForm');
    $("#show-btn").on("click", function (e) {
        listTable.DataTable().ajax.reload();
        e.preventDefault();
    });
    var defaultForm = $('#defaultForm');
            listTable.dataTable({
                language: {
                    processing: "<?php echo get_msg('processing')?>...",
                    search: "<?php echo get_msg('search')?>",
                    lengthMenu: "<?php echo get_msg('display _menu_records_per_page')?>",
                    zeroRecords: "<?php echo get_msg('nothing_found_sorry')?>",
                    info: "<?php echo get_msg('showing_page_page_of_pages')?>",
                    infoEmpty: "<?php echo get_msg('no_records_available')?>",
                    infoFiltered: "<?php echo get_msg('filtered_from_max_total_records')?>",
                    paginate: {
                        first: "<?php echo get_msg('first')?>",
                        previous: "<?php echo get_msg('previous')?>",
                        next: "<?php echo get_msg('next')?>",
                        last: "<?php echo get_msg('last')?>"
                    },
                },
                "processing": true,
                "serverSide": true,
                "bAutoWidth": false,
                "iDisplayLength": 25,
                "order": [0, "desc"],
                "sAjaxSource": "<?php echo site_url('/admin/comission_from_seller/tablelist');?>",
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    if (aData.DT_RowId == undefined) {
                        return true;
                    }
                    $('td:eq(8)', nRow).html(getOfferActionButtons(nRow, aData));
                    return nRow;
                },
                "fnServerParams": function (aoData) {
                    aoData.push({"name": "listAction", "value": true},
                        // {"name": "trans_date", "value": $('#transDateRpt').val()},
                        // {"name": "trans_date_to", "value": $('#transDateToRpt').val()},
                        // {"name": "acc_period", "value": $('#accPeriodRpt').val()},
                        // {"name": "confirm_status", "value": $('#confirmStatusRpt').val()},
                        {"name": "shop_id", "value": $('#shopRpt').val()},
                        // {"name": "stakeholder_name", "value": $('#accVcrDtls_stakeholderRpt').val()},
                        // {"name": "acc_coa", "value": $('#accCoaRpt').val()},
                        // {"name": "coa_class", "value": $('#coaClassRpt').val()},
                        );
                },
                "aoColumns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    {"bSortable": false},
                    {"bSortable": false},
                    null,
                    {"bSortable": false},
                ]
            });
            defaultForm.validate({
            lang: langCode,
            ignore: [],
            rules: {
                shop_id: {
                    required: true
                },
                amount_percentage: {
                    required: true,
                    number: true,
                    min:0,
                }
            },
            submitHandler: function (form) {
                    var url;
                    if ($('#hiddenUpId').val() == '') {
                        url = "<?php echo site_url('/admin/comission_from_seller/writesubmit');?>";
                    } else {
                        url = "<?php echo site_url('/admin/comission_from_seller/editsubmit');?>";
                    }
                    formAfterSubmit(saveBtn, "<?php echo get_msg('processing')?>")
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: defaultForm.serialize(),
                        success: function (data) {
                            formSubmitReturnMsg(saveBtn, data, listTable, defaultForm, "<?php echo $action_title;?>", "<?php echo get_msg('save')?>");
                            if (data.isError == true) {
                                resetMasterForm();
                            }
                        },
                        failure: function (data) {
                        }
                    })
                    return false;
                }
        });

        listTable.on('click', 'a.show-reference', function (e) {
            var control = this;
            var referenceId = $(control).attr('referenceid');
            jQuery.ajax({
                type: 'GET',
                dataType: 'JSON',
                data: {id: referenceId, showAction: true},
                url: "<?php echo site_url('/admin/comission_from_seller');?>",
                success: function (data, textStatus) {
                    if (data.isError == false) {
                        $("#view-shop_id").html("<strong>" + (data.defaultInstance.shop_id != null) ? (data.shopdata.name) : '' + "</strong>");
                        $("#view-amount_percentage").html("<strong>" + (data.defaultInstance.amount_percentage != null) ? (data.defaultInstance.amount_percentage) : '' + "</strong>");
                        $("#view-remark").html("<strong>" + (data.defaultInstance.remark != null) ? (data.defaultInstance.remark) : '' + "</strong>");
                        $('#accPeriodModalViewId').modal('show');
                    } else {
                        alert(data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    serverErrorToast(errorThrown);
                }
            });
            e.preventDefault();
        });

        listTable.on('click', 'a.update-reference', function (e) {
                var control = this;
                var referenceId = $(control).attr('referenceid');
                jQuery.ajax({
                    type: 'GET',
                    dataType: 'JSON',
                    data: {id: referenceId, editData: true},
                    url: "<?php echo site_url('/admin/comission_from_seller');?>",
                    success: function (data, textStatus) {
                        if (data.isError == false) {
                            $('#defaultMsgDiv').html('');
                            $('#hiddenUpId').val(data.defaultInstance.id);
                            $('#shop_id').val(data.defaultInstance.shop_id);
                            $('#amount_percentage').val(data.defaultInstance.amount_percentage);
                            $("#remark").val((data.defaultInstance.remark != null) ? (data.defaultInstance.remark) : '');
                            $('#accPeriodModalFormId').modal('show');
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        serverErrorToast(errorThrown);
                    }
                });
                e.preventDefault();
            });

    listTable.on('click', 'a.confirm-reference', function (e) {
        var selectRow = $(this).parents('tr');
        var data = selectRow.find("td:eq(1)").text();
        var confirmDel = confirm("<?php echo get_msg('are_you_sure_want_to_make_default_this_comission')?> " + data + ' ??');
        if (confirmDel == true) {
            var control = this;
            var referenceId = $(control).attr('referenceid');
            var shop_id = $(control).attr('shop_id');
            jQuery.ajax({
                type: 'POST',
                dataType: 'JSON',
                data: {id: referenceId, shop_id:shop_id, confirmStatus: 'true'},
                url: "<?php echo site_url('/admin/comission_from_seller/confirmsubmit');?>",
                success: function (data, textStatus) {
                    confirmRowMessages(data.isError, data.message, listTable);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    serverErrorToast(errorThrown);
                }
            });
        }
        e.preventDefault();
    });
            function resetMasterForm() {
                }

});


</script>

