
    <div class="modal fade common-form" id="accPeriodModalFormId">
        <div class="modal-dialog modal-md">
            <div class="modal-content form-bg">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="modal-area">
                        <form class="form-horizontal form-body-md col-md-12 column-one-form common-form"
                              id="defaultForm" autocomplete="off">
                            <div class="text-center panel panel-default mb-0 crowlr-4">
                                <h3 id="headerTitle" class="m-0 p-1"><?php echo get_msg('delivery_boy_commission')?></h3>
                            </div>

                            <div id="defaultMsgDiv"></div>

                            <div class="column-one-form-down">

                                <div class="row crowmlr-8">
                                    <input type="hidden" id="hiddenUpId" name="id"/>

                                    <div class="col-md-12 column-one p-1">
                                        <div class="panel panel-default">
                                            <fieldset>
                                                <legend><?php echo get_msg('delivery_boy_commission')?></legend>

                                                <div class="form-group">
                                                     <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('contract_type_label')?><span class="text-warning">*</span>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                            <select class="form-control selectTwo input-sm" id="contract_type" name="contract_type">
                                                            <option value=""><?php echo get_msg('contract_type_label')?></option>
                                                            <?php
                                                            foreach ($contractTypeList as $item)
                                                            {
                                                            echo '<option value="'.$item.'">'.get_msg($item).'</option>';
                                                            }
                                                            ?>
                                                            </select>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('btn_delivery_boys')?><span class="text-warning">*</span>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                            <select class="form-control selectTwo input-sm" id="delivery_boy" name="delivery_boy">
                                                            <option value=""><?php echo get_msg('btn_delivery_boys')?></option>
                                                            </select>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('monthly_salary')?><span class="text-warning">*</span>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                        <input type="number" id="monthly_salary" min="0" name="monthly_salary" class="form-control input-sm" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('order_comission')?><span class="text-warning">*</span>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                        <input type="number" id="order_comission" min="0" name="order_comission" class="form-control input-sm" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <label class="col-sm-4 control-label"><?php echo get_msg('Remark')?>:</label>
                                                        <div class="col-sm-8 fmi_box">
                                                            <textarea id="remark" name="remark"
                                                                class="form-control input-sm"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row crowmlr-8">
                                <div class="col-md-12 p-1">
                                    <div class="panel panel-default mb-0">
                                        <div class="text-right p-2">
                                            <button type="button" class="btn btn-default" data-toggle="tooltip"
                                                id="reset-btn"><?php echo get_msg('Reset')?></button>
                                            <button type="submit" class="btn btn-primary" data-toggle="tooltip"
                                                id="save-btn"><?php echo get_msg('Save')?>... <i
                                                    class="fa fa-recycle"></i></button>
                                            <button type="button" class="btn btn-warning cancel-btn"
                                                data-dismiss="modal" id="cancel-btn"><?php echo get_msg('Cancel')?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

