<div class="container-fluid" style="padding: 10px 30px 10px 30px;">
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <?php 
        // echo "<pre>";
        // print_r($period->result());
        // echo "</pre>";
    ?>
    <div class="row">
        <div class="ibox-title">
            <div class="ct-btn">
                <a href="#" class="btn btn-primary btn-sm form-bg" id="btn-icon" data-toggle="modal" data-target="#accPeriodModalFormId">
                    <i class="fa fa-plus"></i>
                </a>
            </div>

            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
   </div>

 

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox cc-box">
                <div class="ibox-content">
                    <div class="tabs-containers">
                        <div class="table-responsive tblc">
                            <table class="table table-stripped table-bordered" id="list-table">
                                <thead>
                                    <tr>
                                        <th class="sl-icon">
                                            <?php echo get_msg('SL')?>
                                        </th>
                                        <th class="code-icon">
                                            <?php echo get_msg('contract_type')?>
                                        </th>
                                        <th>
                                            <?php echo get_msg('monthly_salary')?>
                                        </th>
                                        <th>
                                            <?php echo get_msg('order_comission')?>  
                                        </th>
                                        <th>
                                            <?php echo get_msg('created_date')?>  
                                        </th>
                                         <th> <?php echo get_msg('user_name')?></th> 
                                         <th> <?php echo get_msg('user_email')?></th> 
                                         <th> <?php echo get_msg('user_phone')?></th> 
                                        <th>
                                            <?php echo get_msg('Action')?>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

<?php include 'entry_form.php';?>
<?php include 'period_view.php';?>

<script type="text/javascript">
$(document).ready(function() {
    var saveBtn = $('#save-btn');
    var resetBtn = $('#reset-btn');
    var listTable = $('#list-table');
    var defaultForm = $('#defaultForm');
            listTable.dataTable({
                language: {
                    processing: "<?php echo get_msg('processing')?>...",
                    search: "<?php echo get_msg('search')?>",
                    lengthMenu: "<?php echo get_msg('display _menu_records_per_page')?>",
                    zeroRecords: "<?php echo get_msg('nothing_found_sorry')?>",
                    info: "<?php echo get_msg('showing_page_page_of_pages')?>",
                    infoEmpty: "<?php echo get_msg('no_records_available')?>",
                    infoFiltered: "<?php echo get_msg('filtered_from_max_total_records')?>",
                    paginate: {
                        first: "<?php echo get_msg('first')?>",
                        previous: "<?php echo get_msg('previous')?>",
                        next: "<?php echo get_msg('next')?>",
                        last: "<?php echo get_msg('last')?>"
                    },
                },
                "processing": true,
                "serverSide": true,
                "bAutoWidth": false,
                "iDisplayLength": 25,
                "order": [0, "desc"],
                "sAjaxSource": "<?php echo site_url('/admin/deliveryboycommission/tablelist');?>",
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    if (aData.DT_RowId == undefined) {
                        return true;
                    }
                    $('td:eq(8)', nRow).html(getActionButtons(nRow, aData));
                    return nRow;
                },
                "fnServerParams": function (aoData) {
                    aoData.push({"name": "listAction", "value": true},
                        // {"name": "trans_date", "value": $('#transDateRpt').val()},
                        // {"name": "trans_date_to", "value": $('#transDateToRpt').val()},
                        // {"name": "acc_period", "value": $('#accPeriodRpt').val()},
                        // {"name": "confirm_status", "value": $('#confirmStatusRpt').val()},
                        // {"name": "stake_holder_type", "value": $('#accVcrDtls_stakeHolderTypeRpt').val()},
                        // {"name": "stakeholder_name", "value": $('#accVcrDtls_stakeholderRpt').val()},
                        // {"name": "acc_coa", "value": $('#accCoaRpt').val()},
                        // {"name": "coa_class", "value": $('#coaClassRpt').val()},
                        );
                },
                "aoColumns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    {"bSortable": false},
                    {"bSortable": false},
                    null,
                    {"bSortable": false},
                ]
            });
            defaultForm.validate({
            lang: langCode,
            ignore: [],
            rules: {
                contract_type: {
                    required: true
                },
                delivery_boy: {
                    required: true
                },
                monthly_salary: {
                    required: function(element) {
                        return $('#contract_type').val()=='Employee' ? true:false;
                    },
                    number: true,
                    min:0,
                },
                order_comission: {
                    required: function(element) {
                        return $('#contract_type').val()=='DeliveryBoy' ? true:false;
                    },
                    number:true,
                    max:100,
                    min:0,
                },
            },
            submitHandler: function (form) {
                    var url;
                    if ($('#hiddenUpId').val() == '') {
                        url = "<?php echo site_url('/admin/deliveryboycommission/writesubmit');?>";
                    } else {
                        url = "<?php echo site_url('/admin/deliveryboycommission/editsubmit');?>";
                    }
                    formAfterSubmit(saveBtn, "<?php echo get_msg('processing')?>")
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: defaultForm.serialize(),
                        success: function (data) {
                            formSubmitReturnMsg(saveBtn, data, listTable, defaultForm, "<?php echo $action_title;?>", "<?php echo get_msg('save')?>");
                            if (data.isError == true) {
                                resetMasterForm();
                            }
                        },
                        failure: function (data) {
                        }
                    })
                    return false;
                }
        });

        listTable.on('click', 'a.show-reference', function (e) {
            var control = this;
            var referenceId = $(control).attr('referenceid');
            jQuery.ajax({
                type: 'GET',
                dataType: 'JSON',
                data: {id: referenceId, showAction: true},
                url: "<?php echo site_url('/admin/deliveryboycommission');?>",
                success: function (data, textStatus) {
                    
                    if (data.isError == false) {
                        $("#view-contract_type").html("<strong>" + (data.defaultInstance.contract_type != null) ? (data.contractType) : '' + "</strong>");
                        $("#view-delivery_boy").html("<strong>" + (data.defaultInstance.delivery_boy != null) ? (data.userdata.user_name) : '' + "</strong>");
                        $("#view-monthly_salary").html("<strong>" + (data.defaultInstance.monthly_salary != null) ? (data.defaultInstance.monthly_salary) : '' + "</strong>");
                        $("#view-order_comission").html("<strong>" + (data.defaultInstance.order_comission != null) ? (data.defaultInstance.order_comission) : '' + "</strong>");
                        $("#view-remark").html("<strong>" + (data.defaultInstance.remark != null) ? (data.defaultInstance.remark) : '' + "</strong>");
                        $('#accPeriodModalViewId').modal('show');
                    } else {
                        alert(data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    serverErrorToast(errorThrown);
                }
            });
            e.preventDefault();
        });

        listTable.on('click', 'a.update-reference', function (e) {
                var control = this;
                var referenceId = $(control).attr('referenceid');
                jQuery.ajax({
                    type: 'GET',
                    dataType: 'JSON',
                    data: {id: referenceId, editData: true},
                    url: "<?php echo site_url('/admin/deliveryboycommission');?>",
                    success: function (data, textStatus) {
                        if (data.isError == false) {
                            $('#defaultMsgDiv').html('');
                            $('#hiddenUpId').val(data.defaultInstance.id);
                            $('#contract_type').val(data.defaultInstance.contract_type);
                            $('#monthly_salary').val(data.defaultInstance.monthly_salary);
                            $('#order_comission').val(data.defaultInstance.order_comission);
                            $("#delivery_boy").empty().append('<option selected value="'+data.userdata.user_id +'">' + (data.userdata.user_phone + '-' + data.userdata.user_name) + '</option>').val(data.userdata.user_id).trigger('change');
                            if (data.defaultInstance.contract_type=='DeliveryBoy') {
                                $("#monthly_salary").prop('disabled', true);
                                $("#order_comission").prop('disabled', false);
                            } else if(data.defaultInstance.contract_type=='Employee'){
                                $("#monthly_salary").prop('disabled', false);
                                $("#order_comission").prop('disabled', true);
                            }

                            $("#remark").val((data.defaultInstance.remark != null) ? (data.defaultInstance.remark) : '');
                            $('#accPeriodModalFormId').modal('show');
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        serverErrorToast(errorThrown);
                    }
                });
                e.preventDefault();
            });


            function resetMasterForm() {
                }


        $("#contract_type").change(function(){
            var contractType=$('#contract_type').val();
            $("#monthly_salary").val("");
            $("#order_comission").val("");
            if (contractType=='DeliveryBoy') {
                $("#monthly_salary").prop('disabled', true);
                $("#order_comission").prop('disabled', false);
            } else if(contractType=='Employee'){
                $("#monthly_salary").prop('disabled', false);
                $("#order_comission").prop('disabled', true);
            }
                $("#delivery_boy").select2({
                ajax: {
                    url: "<?php echo site_url('/admin/expense_voucher');?>",
                    dataType: 'JSON',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page,
                            stakeHolderType: $('#contract_type').val(),
                            stakeholderSearch: 'true'
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: "<?php echo get_msg('btn_delivery_boys')?>...",
                allowClear: true,
                width: '100%',
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });
            });


            function formatRepo(repo) {
                if (repo.loading) {
                    return repo.text;
                }
                return "<div class='row slt-srs m-0'><div class='col-md-2 thumbnail sc-img p-0'>" + repo.imgsrc + "</div><div class='col-md-10 sc-con pl-3 pr-1'><span class='scsm-gp'><span class='sc-code lft'>" + repo.caption + '</span><span class="sc-phn rit">' + repo.phone + '</span></span><span class="scsm-gp"><span class="sc-capt lft">' + repo.phone + '</span></span><span class="scsm-gp"><span class="sc-eml lft">' + repo.email + "</span></span></div></div>";
            }

            function formatRepoSelection(repo) {
                if (repo.id === '' || repo.phone== undefined) {
                    return "<?php echo get_msg('btn_delivery_boys')?>...";
                }
                return repo.phone + ' ' + repo.caption;
            }

});
</script>

