<div class="container-fluid" style="padding: 10px 30px 10px 30px;">
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <?php 
        // echo "<pre>";
        // print_r($period->result());
        // echo "</pre>";
    ?>
    <div class="row">
        <div class="ibox-title">
            <div class="ct-btn">
                <a href="#" class="btn btn-primary btn-sm form-bg" id="btn-icon" data-toggle="modal" data-target="#accPeriodModalFormId">
                    <i class="fa fa-plus"></i>
                </a>
            </div>

            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
   </div>

 

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox cc-box">
                <div class="ibox-content">
                    <div class="tabs-containers">
                        <div class="table-responsive tblc">
                            <table class="table table-stripped table-bordered" id="list-table">
                                <thead>
                                    <tr>
                                        <th class="sl-icon">
                                            <?php echo get_msg('SL')?>
                                        </th>
                                        <th class="code-icon">
                                            <?php echo get_msg('gate_way_name')?>
                                        </th>
                                        <th>
                                            <?php echo get_msg('cost_rate')?>
                                        </th>
                                        <th>
                                            <?php echo get_msg('return_rate')?>
                                        </th>
                                        <th>
                                            <?php echo get_msg('liability')?>  
                                        </th>
                                        <th>
                                            <?php echo get_msg('created_date')?>  
                                        </th>
                                        <th>
                                            <?php echo get_msg('Action')?>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

<?php include 'entry_form.php';?>
<?php include 'period_view.php';?>

<script type="text/javascript">
$(document).ready(function() {
    var saveBtn = $('#save-btn');
    var resetBtn = $('#reset-btn');
    var listTable = $('#list-table');
    var defaultForm = $('#defaultForm');
    listTable.dataTable({
                language: {
                    processing: "<?php echo get_msg('processing')?>...",
                    search: "<?php echo get_msg('search')?>",
                    lengthMenu: "<?php echo get_msg('display _menu_records_per_page')?>",
                    zeroRecords: "<?php echo get_msg('nothing_found_sorry')?>",
                    info: "<?php echo get_msg('showing_page_page_of_pages')?>",
                    infoEmpty: "<?php echo get_msg('no_records_available')?>",
                    infoFiltered: "<?php echo get_msg('filtered_from_max_total_records')?>",
                    paginate: {
                        first: "<?php echo get_msg('first')?>",
                        previous: "<?php echo get_msg('previous')?>",
                        next: "<?php echo get_msg('next')?>",
                        last: "<?php echo get_msg('last')?>"
                    },
                },
                "processing": true,
                "serverSide": true,
                "bAutoWidth": false,
                "iDisplayLength": 25,
                "order": [0, "desc"],
                "sAjaxSource": "<?php echo site_url('/admin/paymentgatewayfees/tablelist');?>",
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    if (aData.DT_RowId == undefined) {
                        return true;
                    }
                    $('td:eq(6)', nRow).html(getActionButtons(nRow, aData));
                    return nRow;
                },
                "fnServerParams": function (aoData) {
                    aoData.push({"name": "listAction", "value": true},
                        // {"name": "trans_date", "value": $('#transDateRpt').val()},
                        // {"name": "trans_date_to", "value": $('#transDateToRpt').val()},
                        // {"name": "acc_period", "value": $('#accPeriodRpt').val()},
                        // {"name": "confirm_status", "value": $('#confirmStatusRpt').val()},
                        // {"name": "stake_holder_type", "value": $('#accVcrDtls_stakeHolderTypeRpt').val()},
                        // {"name": "stakeholder_name", "value": $('#accVcrDtls_stakeholderRpt').val()},
                        // {"name": "acc_coa", "value": $('#accCoaRpt').val()},
                        // {"name": "coa_class", "value": $('#coaClassRpt').val()},
                        );
                },
                "aoColumns": [
                    null,
                    null,
                    null,
                    {"bSortable": false},
                    {"bSortable": false},
                    null,
                    {"bSortable": false},
                ]
            });
            defaultForm.validate({
            lang: langCode,
            ignore: [],
            rules: {
                cost_rate: {
                    required: true,
                    number: true,
                    min:0,
                },
            },
            submitHandler: function (form) {
                    var url;
                    if ($('#hiddenUpId').val() == '') {
                        url = "<?php echo site_url('/admin/paymentgatewayfees/writesubmit');?>";
                    } else {
                        url = "<?php echo site_url('/admin/paymentgatewayfees/editsubmit');?>";
                    }
                    formAfterSubmit(saveBtn, "<?php echo get_msg('processing')?>")
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: defaultForm.serialize(),
                        success: function (data) {
                            formSubmitReturnMsg(saveBtn, data, listTable, defaultForm, "<?php echo $action_title;?>", "<?php echo get_msg('save')?>");
                            if (data.isError == true) {
                                resetMasterForm();
                            }
                        },
                        failure: function (data) {
                        }
                    })
                    return false;
                }
        });

        listTable.on('click', 'a.show-reference', function (e) {
            var control = this;
            var referenceId = $(control).attr('referenceid');
            jQuery.ajax({
                type: 'GET',
                dataType: 'JSON',
                data: {id: referenceId, showAction: true},
                url: "<?php echo site_url('/admin/paymentgatewayfees');?>",
                success: function (data, textStatus) {
                    if (data.isError == false) {
                        $("#view-name").html("<strong>" + (data.defaultInstance.name != null) ? (data.defaultInstance.name) : '' + "</strong>");
                        $("#view-liability").html("<strong>" + (data.defaultInstance.liability_by != null) ? (data.liabilityBy) : '' + "</strong>");
                       $("#view-cost_rate").html("<strong>" + (data.defaultInstance.cost_rate != null) ? (data.defaultInstance.cost_rate) : '' + "</strong>");
                       $("#view-return_rate").html("<strong>" + (data.defaultInstance.return_rate != null) ? (data.defaultInstance.return_rate) : '' + "</strong>");
                        $("#view-remark").html("<strong>" + (data.defaultInstance.remark != null) ? (data.defaultInstance.remark) : '' + "</strong>");
                        $('#accPeriodModalViewId').modal('show');
                    } else {
                        alert(data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    serverErrorToast(errorThrown);
                }
            });
            e.preventDefault();
        });

        listTable.on('click', 'a.update-reference', function (e) {
                var control = this;
                var referenceId = $(control).attr('referenceid');
                jQuery.ajax({
                    type: 'GET',
                    dataType: 'JSON',
                    data: {id: referenceId, editData: true},
                    url: "<?php echo site_url('/admin/paymentgatewayfees');?>",
                    success: function (data, textStatus) {
                        if (data.isError == false) {
                            console.log(data.defaultInstance);
                            $('#defaultMsgDiv').html('');
                            $('#hiddenUpId').val(data.defaultInstance.id);
                            $('#name').val(data.defaultInstance.name);
                            $('#cost_rate').val(data.defaultInstance.cost_rate);
                            $('#return_rate').val(data.defaultInstance.return_rate);
                            if(data.defaultInstance.liability_by=='1'){
                                $("#liabilityEshtri").prop("checked", true);
                            }else{
                                $("#liabilityShop").prop("checked", true);
                            }

                            $("#remark").val((data.defaultInstance.remark != null) ? (data.defaultInstance.remark) : '');
                            $('#accPeriodModalFormId').modal('show');
                        } else {
                            alert(data.message);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        serverErrorToast(errorThrown);
                    }
                });
                e.preventDefault();
            });


            function resetMasterForm() {
                }
            //     clickDsbl('#isExtraChargeTrue', '#per_kilo_amount');
            //     clickDsbl('#isExtraChargeFalse', '#per_kilo_amount');
            //     function clickDsbl($clickId, $dsblfiel) {
            //     $($clickId).on('change', function () {
            //         $($dsblfiel).val("");
            //         if ($(this).is(':checked') && $(this).val()=='1') {
            //             $($dsblfiel).prop('disabled', false);
            //         } else {
            //             $($dsblfiel).prop('disabled', true);
            //         }
            //     });
            // }

});
</script>

