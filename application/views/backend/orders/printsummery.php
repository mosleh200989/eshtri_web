
<div class="row">
	<div class="col-md-12">
		<div class="card" style="padding: 10px 35px 10px 35px;">
<div class="invoice p-3 mb-3">
  	<!-- title row -->
  	  	<div class="row">
    	<div class="col-12" style="text-align: center">
      		<center >
      			<img src="<?php echo site_url('files/logo-en.png');?>">
<!--       			<p>6471 King Abdullah Rd, AR Ruwais District
				Jeddah, Saudi Arabia 23213</p>
				<p>Mobile: 0536176364</p> -->
      		</center>
    	</div>
    <!-- /.col -->
  	</div>
  	<hr>
  	<div class="row">
    	<div class="col-12">
    		<b><?php echo get_msg('invoice'); ?>: <?php 
	  echo '<span style="color:red; font-size:20px">'.sprintf("%03d", $transaction->id).'</span>';?></b><br>
      		<h4>
        	<?php echo get_msg('place_on'); ?>
        	<small class="float-right"><?php echo get_msg('date'); ?>: <?php echo $transaction->added_date; ?></small>
      		</h4>
      			  <?php 

      			  if($transaction->time_slot_id && $transaction->shipping_method_amount <50){
	   $timeslotinfo=$this->Timeslotsdetails->get_one($transaction->time_slot_id); ?>
	  <b><?php echo get_msg('delivery_time'); ?> : <?php echo $timeslotinfo->slot_date." (".$timeslotinfo->start_time." - ".$timeslotinfo->end_time.")"; ?></b>
	<?php } ?>	

    	</div>
    <!-- /.col -->
  	</div><br>
  <!-- info row -->
	<div class="row invoice-info">
	<div class="col-sm-4 invoice-col">
	<b><u><?php echo get_msg('shipping_address'); ?></u></b> <br>
	  	<address>

		    <?php echo get_msg('name'); ?>: <?php echo $transaction->contact_name; ?><br>
		    <?php echo get_msg('address1'); ?>: <?php echo $transaction->shipping_address_1?><br>
		    <?php echo get_msg('address2'); ?>: <?php echo $transaction->shipping_address_2?><br>
		    <?php echo get_msg('phone'); ?>: <?php echo $transaction->contact_phone;?><br>
	 	 </address>
	</div>
	<!-- /.col -->

	<!-- /.col -->

	<!-- /.col -->
	</div>
	<div class="row">
		<div class="col-12 table-responsive">

		    	<?php 
					$conds['transactions_header_id'] = $transaction->id;
					$all_detail =  $this->Transactiondetail->get_all_by( $conds );
                    $outOfStockAmount=0;
                    $outOfStockAmountWithTax=0;
                    $no=1;
					foreach($all_detail->result() as $transaction_detail):

				?>


                        <?php
                        if($transaction_detail->stock_status==1){
                            $itemSubTotal=$transaction_detail->qty * $transaction_detail->price;
                            if($transaction_detail->stock_status==0){
                                $outOfStockAmount +=$itemSubTotal;
                                $outOfStockAmountWithTax += $itemSubTotal+ (($transaction->tax_percent / 100) * $itemSubTotal);
                            }

                        }else {
                            $itemSubTotal=$transaction_detail->qty * $transaction_detail->price;
                            if($transaction_detail->stock_status==0){
                                $outOfStockAmount +=$itemSubTotal;
                                $outOfStockAmountWithTax += $itemSubTotal+ (($transaction->tax_percent / 100) * $itemSubTotal);
                            }

                        }

                        ?>


					<?php endforeach; ?>

		</div>
	<!-- /.col -->
	</div>

	<div class="row">
        <!-- accepted payments column -->
        <!-- /.col -->
        <div class="ccol-6777">

            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <td width="100%">
                            <p><?php echo get_msg('trans_payment_method'); ?><?php echo get_msg(''.$transaction->payment_method.''); ?></p>
                            <?php if($transaction->payment_status ==1){
                                $payment_status='<span class="btn btn-sm btn-success">'.get_msg('paid').'</span>';
                            }else{
                                $payment_status='<span class="badge badge-danger">'.get_msg('unpaid').'</span>';
                            } ?>
                            <p><?php echo get_msg('payment_status'); ?>: <?php echo $payment_status; ?></p>

                            <p> <?php echo get_msg('trans_memo'); ?> <?php echo $transaction->memo; ?></p>
                        </td>
                    </tr>
                </table>

            </div>
            <br>
            <br>
          <div class="table-responsive">
          	<table class="table">
          		<tr>

          			<td width="100%">
          				<table class="table" border="1">

              	<tr>
	                <th><?php echo get_msg('trans_total_balance_amount'); ?></th>
	                <td>
	                	
	                	<?php 

	                	//balance_amount = total_item_amount - coupon_discont + (overall_tax + shipping_cost + shipping_tax (based on shipping cost)) 

	                	echo  ($transaction->sub_total_amount + ($transaction->tax_amount + $transaction->shipping_method_amount + ($transaction->shipping_method_amount * $transaction->shipping_tax_percent)) );  
	                	echo " ". $transaction->currency_symbol;
	                	?>
	                </td>
              	</tr>
            </table>
          			</td>
          		</tr>
          	</table>
            
          </div>
        </div>
        <!-- /.col -->
    </div>
</div>

</div>
	</div>
</div>
<style type="text/css" media="print">
	@page {
	size: 8.5in 11in; 
	margin: 2%; 
	margin-header: 5mm; 
	margin-footer: 15mm; 
}
</style>