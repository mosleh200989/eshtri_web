
<div class="container-fluid" style="padding: 10px 30px 10px 30px;">
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox cc-box">
                    <?php include 'filter_form.php';?>
                    <br>
                    <div class="report-lebel text-center">
                        <h1>
                            <span class="label label-default"><?php echo get_msg('total_shop'); ?> <span class="badge"><?=$summeryReport->shop?></span></span>
                            <span class="label label-primary"><?php echo get_msg('unique_customer'); ?> <span class="badge"><?=$summeryReport->customer?></span></span>
                            <span class="label label-success"><?php echo get_msg('total_orders'); ?> <span class="badge"><?=$summeryReport->orders?></span></span>
                            <span class="label label-default"><?php echo get_msg('OrderReceived'); ?> <span class="badge"><?=$summeryReport->orderreceive?></span></span>
                            <span class="label label-primary"><?php echo get_msg('Preparing'); ?> <span class="badge"><?=$summeryReport->orderpreparing?></span></span>
                            <span class="label label-warning"><?php echo get_msg('Ready'); ?> <span class="badge"><?=$summeryReport->orderready?></span></span>
                            <span class="label label-info"><?php echo get_msg('OntheWay'); ?> <span class="badge"><?=$summeryReport->orderontheway?></span></span>
                            <span class="label label-success"><?php echo get_msg('Delivered'); ?> <span class="badge"><?=$summeryReport->orderdelivered?></span></span>
                            <span class="label label-danger"><?php echo get_msg('rejected'); ?> <span class="badge"><?=$summeryReport->orderrejected?></span></span>
                            <span class="label label-info"><?php echo get_msg('paytabs'); ?> <span class="badge"><?=$summeryReport->paytabs?></span></span>
                            <span class="label label-warning"><?php echo get_msg('POSTerminal'); ?> <span class="badge"><?=$summeryReport->POSTerminal?></span></span>
                            <span class="label label-danger"><?php echo get_msg('COD'); ?> <span class="badge"><?=$summeryReport->COD?></span></span>
                        </h1>
                    </div>
                    <br>
                    <div class="ibox-content">
                        <div class="tabs-containers">
                            <div class="table-responsive tblc">
                                <table class="table table-stripped table-bordered" id="list-table">
                                    <thead>
                                    <tr>
                                        <th><?php echo get_msg('no'); ?></th>
                                        <th><?php echo get_msg('invoice'); ?></th>
                                        <th><?php echo get_msg('shop_name'); ?></th>
                                        <th><?php echo get_msg('customer_phone'); ?></th>
                                        <th><?php echo get_msg('city'); ?></th>
                                        <th><?php echo get_msg('status_label'); ?></th>
                                        <th><?php echo get_msg('total_store_price'); ?></th>
                                        <th><?php echo get_msg('place_on'); ?></th>
                                        <th><?php echo get_msg('delivery_date'); ?></th>
                                        <th><?php echo get_msg('payment_method'); ?></th>
                                        <th><?php echo get_msg('payment_status'); ?></th>
                                        <th><?php echo get_msg('assign_to'); ?></th>
                                        <th><?php echo get_msg('Accounting'); ?></th>
                                        <th><?php echo get_msg('details'); ?></th>
                                        <th><?php echo get_msg('print'); ?></th>
                                        <th><?php echo get_msg('edit'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    var saveBtn = $('#save-btn');
    var resetBtn = $('#reset-btn');
    var listTable = $('#list-table');
    var defaultForm = $('#defaultForm');
    $(document).ready(function () {
        $("#show-btn").on("click", function (e) {
            listTable.DataTable().ajax.reload();
            e.preventDefault();
        });
        resetFormByBtn(resetBtn, defaultForm, "<?php echo get_msg('orders')?>", "<?php echo get_msg('save')?>");
        resetBtn.click(function () {
            resetMasterForm();
        });

        // var startDate = new Date(periodStartDt);
        // var endDate = new Date(periodCloseDt);
        // $('#transDate').datepicker({
        //     format: 'yyyy-mm-dd'
        // }).datepicker("setDate",'now');
        // $('#transDate').datepicker('setStartDate', startDate);
        // $('#transDate').datepicker('setEndDate', endDate);


        $("#defaultModalOnClickBtn").on("click", function (e) {
            $("#accVcrMstModalFormId").modal('show');
            // resetMasterForm();
            // resetDetailForm();
            $("#accVcrDtls-table > tbody").html("");
            $('#drAmountSum').html('0');
            $('#crAmountSum').html('0');
            e.preventDefault();
        });


        listTable.dataTable({
            language: {
                processing: "<?php echo get_msg('processing')?>...",
                search: "<?php echo get_msg('search')?>",
                lengthMenu: "<?php echo get_msg('display_menu_records_per_page')?>",
                zeroRecords: "<?php echo get_msg('nothing_found_sorry')?>",
                info: "<?php echo get_msg('showing_page_page_of_pages')?>",
                infoEmpty: "<?php echo get_msg('no_records_available')?>",
                infoFiltered: "<?php echo get_msg('filtered_from_max_total_records')?>",
                paginate: {
                    first: "<?php echo get_msg('first')?>",
                    previous: "<?php echo get_msg('previous')?>",
                    next: "<?php echo get_msg('next')?>",
                    last: "<?php echo get_msg('last')?>"
                },
            },
            "processing": true,
            "serverSide": true,
            "bAutoWidth": false,
            "iDisplayLength": 25,
            "order": [0, "desc"],
            "sAjaxSource": "<?php echo site_url('/admin/orders/order_list');?>",
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                if (aData.DataRejected) {
                    $(nRow).addClass('rejectedOrder');
                }
                if (aData.DT_RowId == undefined) {
                    return true;
                }
                // $('td:eq(6)', nRow).html(getActionButtons(nRow, aData));
                return nRow;
            },
            "fnServerParams": function (aoData) {
                aoData.push({"name": "listAction", "value": true},
                    {"name": "orderStartDate", "value": $('#orderStartDateRpt').val()},
                    {"name": "orderEndDate", "value": $('#orderEndDateToRpt').val()},
                    {"name": "deliveryStartDate", "value": $('#deliveryStartDateRpt').val()},
                    {"name": "deliveryEndDate", "value": $('#deliveryEndDateRpt').val()},
                    {"name": "invoiceNo", "value": $('#invoiceNoRpt').val()},
                    {"name": "customerMobile", "value": $('#customerMobileRpt').val()},
                    {"name": "transStatus", "value": $('#transStatusRpt').val()},
                    {"name": "rejection", "value": $('#rejectionRpt').val()},
                    {"name": "paymentMethod", "value": $('#paymentMethodRpt').val()},
                    {"name": "paymentStatus", "value": $('#paymentStatusRpt').val()},
                    {"name": "delivery_boy", "value": $('#delivery_boyRpt').val()},
                    {"name": "shop", "value": $('#shopRpt').val()},

                );
            },
            "aoColumns": [
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
            ]
        });

    });



    var rowNumber = 1;

    $(document).ready(function () {
        function FetchData() {
        $("#delivery_boyRpt").select2({
            ajax: {
                url: "<?php echo site_url('/admin/expense_voucher');?>",
                dataType: 'JSON',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                        stakeHolderType: "Employee_DeliveryBoy",
                        stakeholderSearch: 'true'
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: "<?php echo get_msg('Delivery_Boy')?>...",
            allowClear: true,
            width: '100%',
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });


            $("#shopRpt").select2({
                ajax: {
                    url: "<?php echo site_url('/admin/expense_voucher');?>",
                    dataType: 'JSON',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page,
                            stakeHolderType: "Seller",
                            stakeholderSearch: 'true'
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: "<?php echo get_msg('shop')?>...",
                allowClear: true,
                width: '100%',
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });
            // do something
        }
        setTimeout(FetchData, 3000);




    });

    function formatRepoRpt(repo) {
        if (repo.loading) {
            return repo.text;
        }
        return "<div class='row slt-srs m-0'>" +
            "<div class='col-md-10 sc-con pl-3 pr-1'>" +
            "<span class='scsm-gp'>" +
            "<span class='sc-code lft'>" + repo.caption + '</span>' +
            '</span>' +
            "</div>" +
            "</div>";
    }

    function formatRepoSelectionRpt(repo) {
        if (repo.id === '' || repo.phone== undefined) {
            return "<?php echo get_msg('stakeholder')?>...";
        }
        $('#carriedByCaption').val(repo.caption);
        return repo.caption;
    }

    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        return "<div class='row slt-srs m-0'><div class='col-md-2 thumbnail sc-img p-0'>" + repo.imgsrc + "</div><div class='col-md-10 sc-con pl-3 pr-1'><span class='scsm-gp'><span class='sc-code lft'>" + repo.caption + '</span><span class="sc-phn rit">' + repo.phone + '</span></span><span class="scsm-gp"><span class="sc-capt lft">' + repo.phone + '</span></span><span class="scsm-gp"><span class="sc-eml lft">' + repo.email + "</span></span></div></div>";
    }

    function formatRepoSelection(repo) {
        if (repo.id === '' || repo.phone== undefined) {
            return "<?php echo get_msg('stakeholder')?>...";
        }
        $('#carriedByCaption').val(repo.caption);
        return repo.phone + ' ' + repo.caption;
    }


    function checkDuplicate(newItemId) {
        var existingItem;
        var isError = false;
        $("#accVcrDtls-table tbody tr").each(function () {
            existingItem = $(this).find(".accCoaId").val();
            if (newItemId == existingItem) {
                isError = true;
            }
        });
        return isError;
    }


    function resetMasterForm() {
        $('#hiddenUpId').val('');
        $('#code').val('');
        $('#accPeriod').select2('val', '');
        $('#businessUnit').val('');
        $('#narration').val('');
        $('#referenceCode').val('');
        $('#transDate').datepicker('setDate', new Date());
    }

    function resetDetailForm() {
        $('#accVcrDtls_accCoa').select2('val', '');
        $('#accVcrDtls_stakeHolderType').val('');
        $("#accVcrDtls_stakeholder").empty().append('<option value=""></option>').val('').trigger('change');
        $("#select2-accVcrDtls_stakeholder-container").html('');
        $('#accVcrDtls_drCrAmount').val('');
    }
</script>

<style>
    .label {
        display: inline;
        padding: .2em .6em .3em;
        font-size: 63%;
        font-weight: 700;
        line-height: 2;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25em;

    }
    .label .badge{
        background: #375750;
        color: white;
        font-weight: bold;
    }
    .label-default {
        background-color: #181a1b;
    }
    tr.rejectedOrder{
        background: #403f3f!important;
        color: white;
    }
    td a.shopname{
        background: #017cff;
        padding: 2px 5px;
        border-radius: 5px;
        color: white;
        font-weight: bold;
    }
    bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
        vertical-align: middle;
    }
</style>