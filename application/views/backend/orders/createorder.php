
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <form action="<?php echo site_url('/admin/orders/editordersubmit');?>" method="post" id="defaultForm">
            <div class="card" style="padding: 10px 35px 10px 35px;">
    <div class="invoice p-3 mb-3">
        <!-- title row -->
        <div class="row">
            <div class="col-12">
                <h4>
                <?php echo get_msg('trans_detail'); ?>
                <small class="float-right"><?php echo get_msg('date'); ?>: <?php echo date("Y-m-d H:i:s"); ?></small>
                </h4>
            </div>

        <!-- /.col -->
        </div>
      <!-- info row -->
        <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
           <b><u><?php echo get_msg('shipping_address'); ?></u></b> <br><br>
            <address>
                <input type="hidden" name="user_id" id="user_id" value="<?=$user->user_id?>">
                <input type="hidden" name="default_add_id" id="default_add_id" value="<?=$userAddress->id?>">
                <?php echo get_msg('name'); ?>: <input type="text" name="shipping_first_name" id="shipping_first_name" value="<?=$userAddress->receiverName ? $userAddress->receiverName : $user->user_name?>"><br>
                <?php echo get_msg('address1'); ?>: <input type="text" name="shipping_address_1" id="shipping_address_1" value="<?=$userAddress->address?>"><br>
                <?php echo get_msg('city'); ?>: <input type="text" name="shipping_address_2" id="shipping_address_2" value="<?=$userAddress->city?>"><br>
                <?php echo get_msg('phone'); ?>: <input type="text" name="shipping_phone" id="shipping_phone" value="<?=$userAddress->mobileNo ? $userAddress->mobileNo : $user->user_phone?>"><br>
                <?php echo get_msg('email'); ?>: <input type="text" name="shipping_email" id="shipping_email" value="<?=$userAddress->email ? $userAddress->email : $user->user_email?>">
             </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
        
        </div>
        <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <div class='col-12'>
                    <div class="row mb-1">
                        <label class="col-sm-2 control-label"><?php echo get_msg('delivery_date')?>:</label>
                        <div class="col-sm-6 fmi_box">
                           <input type="text" class="form-control input-sm" name="delivery_date" id="delivery_date" value="<?=$nextday?>">
                        </div>
                    </div>
                </div>
                <div class='col-12'>
                    <div class="row mb-1">
                        <label class="col-sm-2 control-label"><?php echo get_msg('delivery_time')?>:</label>
                        <div class="col-sm-6 fmi_box">
                            <select class="form-control input-sm" name="time_slot_id" id="time_slot_id">
                               <?php 
                                 foreach($timeslotsdetails->result() as $timeslots): ?>
                                <option value="<?=$timeslots->id?>"><?=$timeslots->start_time." - ".$timeslots->end_time?></option>
                                <?php 
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class='col-12'>
                    <div class="row mb-1">
                        <label class="col-sm-2 control-label"><?php echo get_msg('shop')?>:</label>
                        <div class="col-sm-6 fmi_box">
                            <select class="form-control input-sm" name="shop_id" id="shop_id">
                                <?php 
                                 foreach($shops->result() as $shop): ?>
                                <option value="<?=$shop->id?>"><?=$shop->name?></option>
                                <?php 
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class='col-12'>
                    <div class="row mb-1">
                        <label class="col-sm-2 control-label"><?php echo get_msg('product')?>:</label>
                        <div class="col-sm-6 fmi_box">
                            <select class="form-control select2 selectTwo input-sm" name="importproduct" id="importproduct">
                                <option value=""><?php echo get_msg('product')?></option>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <a href='#' class='btn btn-sm btn-primary pull-right' id="import-product">
                                <span class='fa fa-plus'></span>
                                <?php echo get_msg( 'import' )?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 table-responsive">
                
                    <input type="hidden" class="transactions_header_id" name="transactions_header_id" value="<?=$transaction->id?>">
                    <input type="hidden" class="tax_percent" name="tax_percent" value="<?=$transaction->tax_percent?>">
                    <input type="hidden" class="coupon_discount_amount" name="coupon_discount_amount" value="<?=$transaction->coupon_discount_amount?>">
              <table class="table table-striped">
                <thead>
                    <tr>
                        <th><?php echo get_msg('no'); ?></th>
                        <th><?php echo get_msg('Prd_name'); ?></th>
                        <th width="70"><?php echo get_msg('Prd_price'); ?></th>

                        <th width="70"><?php echo get_msg('Prd_dis_price'); ?></th>
                        <th width="70"><?php echo get_msg('Prd_dis'); ?></th>
                        <th width="70"><?php echo get_msg('Prd_qty'); ?></th>
                        <th><?php echo get_msg('Prd_amt'); ?></th>
                        <th width="150"><?php echo get_msg('stock'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $conds['transactions_header_id'] = $transaction->id;
                        $all_detail =  $this->Transactiondetail->get_all_by( $conds );
                        $rowNumber=0;
                        $outOfStockAmount=0;
                        $outOfStockAmountWithTax=0;
                        $no=1;
                        foreach($all_detail->result() as $transaction_detail):

                    ?>
                    <tr>
                        <td width="20"><?=$no?></td>
                        <td>
                            <?php
                            $att_name_info  = explode("#", $transaction_detail->product_attribute_name);
                            $att_price_info = explode("#", $transaction_detail->product_attribute_price);
                            $att_info_str = "";
                            $att_flag = 0;
                            if( count($att_name_info[0]) > 0 ) {

                                //loop attribute info
                                for($k = 0; $k < count($att_name_info); $k++) {

                                    if($att_name_info[$k] != "") {
                                        $att_flag = 1;
                                        $att_info_str .= $att_name_info[$k] . " : " . $att_price_info[$k] . "(". $transaction->currency_symbol ."),";

                                    }
                                }

                            } else {
                                $att_info_str = "";
                            }

                            $att_info_str = rtrim($att_info_str, ",");


                            if( $att_flag == 1 ) {

                                echo $this->Product->get_one($transaction_detail->product_id)->name .'<br>' . $att_info_str  . '<br>' ;

                            } else {

                                echo $this->Product->get_one($transaction_detail->product_id)->name . '<br>';

                            }


                            if ($transaction_detail->product_color_id != "") {

                                echo "".get_msg('color').":";

                                $color_value =  $this->Color->get_one($transaction_detail->product_color_id)->color_value . '}';


                                }

                            ?>

                            <div style="background-color:<?php echo  $this->Color->get_one($transaction_detail->product_color_id)->color_value ; ?>; width: 20px; height: 20px; margin-top: -20px; margin-left: 50px;">
                            </div>

                            <?php echo "".get_msg('product_unit')." : " . $transaction_detail->product_measurement . " " . $transaction_detail->product_unit; ?> <br>
                            <?php echo "".get_msg('shipping_cost')." : " . $transaction_detail->shipping_cost ." ". $transaction->currency_symbol; ?>

                            <input type="hidden" class="id" name="idDtls[<?=$rowNumber?>]" value="<?=$transaction_detail->id?>">


                        </td>
                        <td> <input type="number" min="0" step="any" class="originalPrice form-control" name="originalPriceDtls[<?=$rowNumber?>]" value="<?=$transaction_detail->original_price?>"><?php echo " ". $transaction->currency_symbol; ?></td>
                         <td><input type="number" min="0" step="any" class="originalPrice form-control" name="discountPriceDtls[<?=$rowNumber?>]" value="<?=$transaction_detail->price?>"><?php echo " ". $transaction->currency_symbol; ?></td>
                        <td><input type="number" min="0" step="any" class="discountPercent form-control" name="discountPercentDtls[<?=$rowNumber?>]" value="<?=$transaction_detail->discount_percent?>"> <?php echo "-" . $transaction_detail->discount_amount . $transaction->currency_symbol . " (" .$transaction_detail->discount_percent . "% ".get_msg('off').")"; ?></td>
                        <td> <input type="number" min="0" step="any" class="qty form-control" name="qtyDtls[<?=$rowNumber?>]" value="<?=$transaction_detail->qty?>"></td>


                        <td>
                            <?php
                            $itemSubTotal=$transaction_detail->qty * $transaction_detail->price;
                            if($transaction_detail->stock_status==0){
                                $outOfStockAmount +=$itemSubTotal;
                                $outOfStockAmountWithTax += $itemSubTotal+ (($transaction->tax_percent / 100) * $itemSubTotal);
                            }

                                echo $itemSubTotal  ." ". $transaction->currency_symbol;
                            ?>
                        </td>
                        <td>
                            <select class="stock form-control" name="stockDtls[<?=$rowNumber?>]" >
                                <option value="1" <?=$transaction_detail->stock_status==1? "selected" :""?>><?php echo get_msg('in_stock')?></option>
                                <option value="0" <?=$transaction_detail->stock_status==0? "selected" :""?>><?php echo get_msg('out_of_stock')?></option>
                            </select>
                        </td>
                    </tr>
                            <?php 
                            $no++;
                            $rowNumber++; ?>
                        <?php endforeach; ?>
                </tbody>
              </table>
            </div>
        <!-- /.col -->
        </div>

        <div class="row">
            <!-- accepted payments column -->

            <div class="col-6">
                 <br>
              <p><?php echo get_msg('trans_payment_method'); ?><?php echo $transaction->payment_method; ?></p>

              <p> <?php echo get_msg('trans_memo'); ?> <?php echo $transaction->memo; ?></p>
            </div>

            <!-- /.col -->
            <div class="col-6">

                <table class="table">
                    <?php
                    if($outOfStockAmount>0){ ?>
                    <tr>
                        <th><?php echo get_msg('out_of_stock_amount'); ?> (-):</th>
                        <td><span style="color:red"><b><?php echo $outOfStockAmount . " : ".$outOfStockAmountWithTax." ".$transaction->currency_symbol; ?></b></span></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <th><?php echo get_msg('trans_coupon_discount_amount'); ?></th>
                        <td><?php echo $transaction->coupon_discount_amount . " ". $transaction->currency_symbol; ?></td>
                    </tr>

                    <tr>
                        <th style="width:50%"><?php echo get_msg('trans_item_sub_total'); ?></th>
                        <td><?php echo $transaction->sub_total_amount . " ". $transaction->currency_symbol; ?></td>
                    </tr>
                    <tr>
                        <th><?php echo get_msg('trans_overall_tax'); ?> <?php echo "(" . $transaction->tax_percent . "%)"  ?> : (+)</th>
                        <td><?php echo $transaction->tax_amount . " ". $transaction->currency_symbol; ?></td>
                    </tr>

                    <tr>
                        <th style="width:50%"><?php echo get_msg('sub_total_with_tax'); ?></th>
                        <td><span style="color:red"><b><?php echo  ($transaction->sub_total_amount + $transaction->tax_amount )  . " ". $transaction->currency_symbol; ?></b></span></td>
                    </tr>
                    <tr>
                        <th><?php echo get_msg('trans_shipping_cost'); ?>: (+)</th>
                        <td><input type="number" min="0" class="shippingCost form-control" name="shipping_method_amount" value="<?=$transaction->shipping_method_amount?>"><?php echo " ". $transaction->currency_symbol; ?></td>
                    </tr>

                    <tr>
                        <th><?php echo get_msg('trans_shipping_tax'); ?> <?php echo "(" . $transaction->shipping_tax_percent . ")"  ?>% : (+)</th>
                        <td><?php echo $transaction->shipping_method_amount * $transaction->shipping_tax_percent . " ". $transaction->currency_symbol; ?></td>
                    </tr>


                    <tr>
                        <th><?php echo get_msg('trans_total_balance_amount'); ?></th>
                        <td>

                            <?php

                            //balance_amount = total_item_amount - coupon_discont + (overall_tax + shipping_cost + shipping_tax (based on shipping cost))

                            echo  ($transaction->sub_total_amount + ($transaction->tax_amount + $transaction->shipping_method_amount + ($transaction->shipping_method_amount * $transaction->shipping_tax_percent)) );
                            echo " ". $transaction->currency_symbol;
                            ?>
                        </td>
                    </tr>
                </table>
                  <input type="submit" value="Submit" class="btn btn-block btn-primary">
              
              </div>
            </div>
            <!-- /.col -->
        </div>
    </div>

        </div>
        </form>
        </div>
        <div class="col-md-2"></div>
    </div>

    <script>
        <?php
        $shop_id = $transaction->shop_id;
        $order_id = $transaction->id;
        ?>
        $(document).ready(function () {
            function FetchData() {
                $("#importproduct").select2({
                    ajax: {
                        url: "<?php echo site_url('/admin/orders');?>",
                        dataType: 'JSON',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term, // search term
                                page: params.page,
                                shop_id: ""+$("#shop_id").val()+"",
                                producSearch: 'true'
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.items,
                                pagination: {
                                    more: (params.page * 30) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },
                    placeholder: "<?php echo get_msg('product')?>...",
                    allowClear: true,
                    width: '100%',
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    minimumInputLength: 1,
                    templateResult: formatRepo,
                    templateSelection: formatRepoSelection
                });


                // $(document.body).on("change","#import-product",function(e){
                //     var productid = $("#importproduct").val();
                //     var shopId = $("#shop_id").val();
                //     jQuery.ajax({
                //         type: 'GET',
                //         dataType: 'JSON',
                //         data: {shop_id: shopId, productid:productid, copyProductData: true},
                //         url: "<?php echo site_url('/admin/orders');?>",
                //         success: function (data, textStatus) {
                //             if (data.isError == false) {
                //                 confirmNormalMessages(data.isError, data.message, true);
                //             } else {
                //                 alert(data.message);
                //             }
                //         },
                //         error: function (XMLHttpRequest, textStatus, errorThrown) {
                //             serverErrorToast(errorThrown);
                //         }
                //     });
                //     e.preventDefault();
                // });

                $(document.body).on("click","#import-product",function(e){
                    var productid = $("#importproduct").val();
                    var first_name = $("#shipping_first_name").val();
                    var address_1 = $("#shipping_address_1").val();
                    var shipping_phone = $("#shipping_phone").val();
                    var shopId = $("#shop_id").val();
                    if(!first_name || !address_1 || !shipping_phone){
                        alert("Please add delivery address");
                    }else{

                    var url = "<?php echo site_url('/admin/orders/creaorderubmit');?>";
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: $("#defaultForm").serialize(),
                        success: function (data) {
                            if (data.isError == false) {
                                confirmNormalMessages(data.isError, data.message, true);
                                if(data.site_url){
                                    window.location.href = data.site_url;
                                }
                                
                            } else {
                                alert(data.message);
                            }
                        },
                        failure: function (data) {
                        }
                    })

                    }
                    
                    e.preventDefault();
                });
                // do something
            }
            setTimeout(FetchData, 3000);

            function formatRepo(repo) {
                if (repo.loading) {
                    return repo.text;
                }
                return "<div class='row slt-srs m-0'><div class='col-md-10 sc-con pl-3 pr-1'><span class='scsm-gp'><span class='sc-code lft'>" + repo.caption + "</span></span></div></div>";
            }

            function formatRepoSelection(repo) {
                if (repo.id === '') {
                    return "<?php echo get_msg('product')?>...";
                }
                return repo.caption;
            }
        });

    </script>