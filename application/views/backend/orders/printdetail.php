
<div class="row">
	<div class="col-md-12">
		<div class="card" style="padding: 10px 35px 10px 35px;">
<div class="invoice p-3 mb-3">
  	<!-- title row -->
  	  	<div class="row">
    	<div class="col-12" style="text-align: center">
      		<center >
      			<img src="<?php echo site_url('files/logo-en.png');?>">
<!--       			<p>6471 King Abdullah Rd, AR Ruwais District
				Jeddah, Saudi Arabia 23213</p>
				<p>Mobile: 0536176364</p> -->
      		</center>
    	</div>
    <!-- /.col -->
  	</div>
  	<hr>
  	<div class="row">
    	<div class="col-12">
    		<b><?php echo get_msg('invoice'); ?>: <?php 
	  echo '<span style="color:red; font-size:20px">'.sprintf("%03d", $transaction->id).'</span>';?></b><br>
      		<h4>
        	<?php echo get_msg('place_on'); ?>
        	<small class="float-right"><?php echo get_msg('date'); ?>: <?php echo $transaction->added_date; ?></small>
      		</h4>
      			  <?php 

      			  if($transaction->time_slot_id && $transaction->shipping_method_amount <50){
	   $timeslotinfo=$this->Timeslotsdetails->get_one($transaction->time_slot_id); ?>
	  <b><?php echo get_msg('delivery_time'); ?> : <?php echo $timeslotinfo->slot_date." (".$timeslotinfo->start_time." - ".$timeslotinfo->end_time.")"; ?></b>
	<?php } ?>	

      <div class="col-sm-4 invoice-col">
	  
	  <b><?php echo get_msg('account'); ?>:</b> <?php echo ($transaction->sub_total_amount + $transaction->tax_amount) ." ". $transaction->currency_short_form; ?>
	    <?php if($transaction->shop_id){
	   $shopinfo=$this->Shop->get_one($transaction->shop_id); ?>
	  <br><b><?php echo get_msg('shop'); ?> : <?php echo $shopinfo->name; ?></b>
	<?php } ?>	
	</div>
    	</div>
    <!-- /.col -->
  	</div><br>
  <!-- info row -->
	<div class="row invoice-info">
	<div class="col-sm-4 invoice-col">
	<b><u><?php echo get_msg('shipping_address'); ?></u></b> <br>
	  	<address>

		    <?php echo get_msg('name'); ?>: <?php echo $transaction->contact_name; ?><br>
		    <?php echo get_msg('address1'); ?>: <?php echo $transaction->shipping_address_1?><br>
		    <?php echo get_msg('address2'); ?>: <?php echo $transaction->shipping_address_2?><br>
		    <?php echo get_msg('phone'); ?>: <?php echo $transaction->contact_phone;?><br>
		    <?php echo get_msg('email'); ?>: <?php echo $transaction->shipping_email; ?>
	 	 </address>
	</div>
	<!-- /.col -->
	<div class="col-sm-4 invoice-col">
	  
	</div>
	<!-- /.col -->

	<!-- /.col -->
	</div>
<br><br>
	<div class="row">
		<div class="col-12 table-responsive">
		  <table class="table table-striped" border="1">
		    <thead>
			    <tr>
			    	<th><?php echo get_msg('no'); ?></th>
			      	<th><?php echo get_msg('Prd_name'); ?></th>
			      	<th><?php echo get_msg('Prd_qty'); ?></th>
			      	<th><?php echo get_msg('product_image'); ?></th> 
			      	<th><?php echo get_msg('barcode'); ?></th>
					<th><?php echo get_msg('Prd_price'); ?></th>
					<th><?php echo get_msg('Prd_dis_price'); ?></th> 
					<th><?php echo get_msg('Prd_amt'); ?></th>
			    </tr>
		    </thead>
		    <tbody>
		    	<?php 
					$conds['transactions_header_id'] = $transaction->id;
					$all_detail =  $this->Transactiondetail->get_all_by( $conds );
                    $outOfStockAmount=0;
                    $outOfStockAmountWithTax=0;
                    $no=1;
					foreach($all_detail->result() as $transaction_detail):

				?>

				<tr>
					<td width="20"><?=$no?></td>
					<td align="center">
						<?php 

						
						$att_name_info  = explode("#", $transaction_detail->product_attribute_name);
						$att_price_info = explode("#", $transaction_detail->product_attribute_price);


						$att_info_str = "";
						$att_flag = 0;
						if( $att_name_info && count($att_name_info[0]) > 0 ) {

							//loop attribute info
							for($k = 0; $k < count($att_name_info); $k++) {
								
								if($att_name_info[$k] != "") {
									$att_flag = 1;
									$att_info_str .= $att_name_info[$k] . " : " . $att_price_info[$k] . "(". $transaction->currency_symbol ."),";

								}
							}


						} else {
							$att_info_str = "";
						}

						

						$att_info_str = rtrim($att_info_str, ","); 

						$productinfo=$this->Product->get_one($transaction_detail->product_id);
						if( $att_flag == 1 ) {

							echo $productinfo->name .'<br>' . $att_info_str  . '<br>' ; 

						} else {

							echo $productinfo->name . '<br>';

						}


						if ($transaction_detail->product_color_code != "" && $transaction_detail->product_color_code !=null) {
							echo "".get_msg('color').":";
							} 

						?>
						<?php 
							if($transaction_detail->product_color_name && $transaction_detail->product_color_id != ""){
								print "<span class='colorname'>".$transaction_detail->product_color_name."</span>";
							}
							?>
						<?php 
						if($this->session->userdata('language_code')=="ar" && $transaction_detail->product_color_id != ""){ ?>
						<div class="<?php echo $this->session->userdata('language_code'); ?>" style="background-color:<?php echo $transaction_detail->product_color_code; ?>; width: 20px; height: 20px; margin-top: -20px; margin-right: 50px;"> 
						</div>
						<?php }else if($transaction_detail->product_color_id != ""){ ?>
						<div class="<?php echo $this->session->userdata('language_code'); ?>" style="background-color:<?php echo $transaction_detail->product_color_code; ?>; width: 20px; height: 20px; margin-top: -20px; margin-left: 50px;"> 
						</div>
						<?php }?>

						<?php 
						if($transaction_detail->product_measurement){echo "".get_msg('product_unit')." : " . $transaction_detail->product_measurement . " " . $transaction_detail->product_unit; ?> <br> <?php } ?>
						<?php if($transaction_detail->shipping_cost >0){ echo "".get_msg('shipping_cost')." : " . $transaction_detail->shipping_cost ." ". $transaction->currency_symbol; }?>
					</td>
					<td align="center"><?php echo $transaction_detail->qty ?></td>
					<td align="center"><?php if($productinfo->thumbnail){?><img src="<?php echo site_url( '/'); ?>uploads/small/<?php echo $productinfo->thumbnail; ?>" style="max-width: 150px; max-height: 130px" ><?php } ?></td>
					
					<td align="center">
                        <?php
                        echo $productinfo->code;
                        if($productinfo->shop_code){
                            echo '\n'.'('.$productinfo->shop_code.')';
                        }
                        ?>

                    </td>
					<td align="center"><?php 
					
					echo $transaction_detail->original_price ." ". $transaction->currency_symbol; 
					?></td>
					
				
					<td align="center"><?php 
						
					echo $transaction_detail->price ." ". $transaction->currency_symbol; 
					?>
						
					</td>
					<td align="center">
                        <?php
                        if($transaction_detail->stock_status==1){
                            $itemSubTotal=$transaction_detail->qty * $transaction_detail->price;
                            if($transaction_detail->stock_status==0){
                                $outOfStockAmount +=$itemSubTotal;
                                $outOfStockAmountWithTax += $itemSubTotal+ (($transaction->tax_percent / 100) * $itemSubTotal);
                            }

                            echo $itemSubTotal  ." ". $transaction->currency_symbol;
                        }else {
                            $itemSubTotal=$transaction_detail->qty * $transaction_detail->price;
                            if($transaction_detail->stock_status==0){
                                $outOfStockAmount +=$itemSubTotal;
                                $outOfStockAmountWithTax += $itemSubTotal+ (($transaction->tax_percent / 100) * $itemSubTotal);
                            }
                            echo '<span style="color:red"><b>'.get_msg('out_of_stock').'</b></span>';
                        }
                        $no++;
                        ?>
					</td>
				</tr>

					<?php endforeach; ?>
		    </tbody>
		  </table>
		</div>
	<!-- /.col -->
	</div>

	<div class="row">
        <!-- accepted payments column -->
        <!-- /.col -->
        <div class="ccol-6777">
         

          <div class="table-responsive">
          	<table class="table">
          		<tr>
          			<td width="40%">
                    <p><?php echo get_msg('trans_payment_method'); ?><?php echo get_msg(''.$transaction->payment_method.''); ?></p>
                        <?php if($transaction->payment_status ==1){
                        $payment_status='<span class="btn btn-sm btn-success">'.get_msg('paid').'</span>';
                        }else{
                        $payment_status='<span class="badge badge-danger">'.get_msg('unpaid').'</span>';
                        } ?>
                    <p><?php echo get_msg('payment_status'); ?>: <?php echo $payment_status; ?></p>

          <p> <?php echo get_msg('trans_memo'); ?> <?php echo $transaction->memo; ?></p>
          			</td>
          			<td width="60%">
          				<table class="table" border="1">

                            <?php
                            if($outOfStockAmount>0){ ?>
                                <tr>
                                    <th><?php echo get_msg('out_of_stock_amount'); ?> (-):</th>
                                    <td><span style="color:red"><b><?php echo $outOfStockAmount . " : ".$outOfStockAmountWithTax." ".$transaction->currency_symbol; ?></b></span></td>
                                </tr>
                            <?php } ?>
		        <tr>
		            <th><?php echo get_msg('trans_coupon_discount_amount'); ?></th>
		            <td><?php echo $transaction->coupon_discount_amount . " ". $transaction->currency_symbol;; ?></td>
		        </tr>
		        <tr>
	                <th style="width:57%"><?php echo get_msg('trans_item_sub_total'); ?></th>
	                <td><?php echo $transaction->sub_total_amount . " ". $transaction->currency_symbol; ?></td>
	            </tr>
              	<tr>
	                <th><?php echo get_msg('trans_overall_tax'); ?> <?php echo "(" . $transaction->tax_percent. "%)"  ?> : (+)</th>
	                <td><?php echo $transaction->tax_amount . " ". $transaction->currency_symbol; ?></td>
	            </tr>
              	

	            
				<tr>
	                <th style="width:57%"><?php echo get_msg('sub_total_with_tax'); ?>:</th>
	                <td><span style="color:red"><b><?php echo  ($transaction->sub_total_amount + $transaction->tax_amount )  . " ". $transaction->currency_symbol; ?></b></span></td>
	            </tr>
	            <tr>
	                <th><?php echo get_msg('trans_shipping_cost'); ?>: (+)</th>
	                <td><?php echo $transaction->shipping_method_amount . " ". $transaction->currency_symbol; ?></td>
	            </tr>

	            <tr>
	                <th><?php echo get_msg('trans_shipping_tax'); ?> <?php echo "(" . $transaction->shipping_tax_percent . ")"  ?>% : (+)</th>
	                <td><?php echo $transaction->shipping_method_amount * $transaction->shipping_tax_percent . " ". $transaction->currency_symbol; ?></td>
              	</tr>
            
              
              	<tr>
	                <th><?php echo get_msg('trans_total_balance_amount'); ?></th>
	                <td>
	                	
	                	<?php 

	                	//balance_amount = total_item_amount - coupon_discont + (overall_tax + shipping_cost + shipping_tax (based on shipping cost)) 

	                	echo  ($transaction->sub_total_amount + ($transaction->tax_amount + $transaction->shipping_method_amount + ($transaction->shipping_method_amount * $transaction->shipping_tax_percent)) );  
	                	echo " ". $transaction->currency_symbol;
	                	?>
	                </td>
              	</tr>
            </table>
          			</td>
          		</tr>
          	</table>
            
          </div>
        </div>
        <!-- /.col -->
    </div>
</div>

</div>
	</div>
</div>
<style type="text/css" media="print">
	@page {
	size: 8.5in 11in; 
	margin: 2%; 
	margin-header: 5mm; 
	margin-footer: 15mm; 
}
</style>