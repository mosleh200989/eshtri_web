
<div class="row">
	<div class="col-md-12">
		<div class="card" style="padding: 10px 35px 10px 35px;">
<div class="invoice p-3 mb-3">
  	<!-- title row -->
  	  	<div class="row">
    	<div class="col-12">
      		<center>

      			<img src="<?php echo site_url('files/logo-en.png');?>">
<!--       			<p>6471 King Abdullah Rd, AR Ruwais District
				Jeddah, Saudi Arabia 23213</p>
				<p>Mobile: 0536176364</p> -->
      		</center>
    	</div>
    <!-- /.col -->
  	</div>
  	<hr>
  	<div class="row">
    	<div class="col-12">
      		<h4>
        	<?php echo get_msg('trans_detail'); ?>
        	<small class="float-right"><?php echo get_msg('date'); ?>: <?php echo $transaction->added_date; ?></small>
      		</h4>
    	</div>
    <!-- /.col -->
  	</div>
  <!-- info row -->
	<div class="row invoice-info">
	<div class="col-sm-4 invoice-col">
	<b><u><?php echo get_msg('shipping_address'); ?></u></b> <br><br>
	  	<address>
	  		 

		    <?php echo get_msg('name'); ?>: <?php echo $transaction->contact_name; ?><br>
		    <?php echo get_msg('address1'); ?>: <?php echo $transaction->shipping_address_1?><br>
		    <?php echo get_msg('address2'); ?>: <?php echo $transaction->shipping_address_2?><br>
		    <?php echo get_msg('phone'); ?>: <?php echo $transaction->contact_phone;?><br>
		    <?php echo get_msg('email'); ?>: <?php echo $transaction->shipping_email; ?>
	 	 </address>
	</div>
	<!-- /.col -->
	<div class="col-sm-4 invoice-col actionbtn">
        <div class="row">
            <div class="col-sm-2">
                <a class="pull-right btn btn-sm btn-primary" target="_blank" href="<?php echo $module_site_url . "/printDetailsPdf/" . $transaction->id;?>">
                    <i class="fa fa-print" aria-hidden="true"></i>
                </a>

                <a class="pull-right btn btn-sm btn-primary" style="margin: 0px 5px" target="_blank" href="<?php echo $module_site_url . "/printSummeryPdf/" . $transaction->id;?>">
                    <i class="fa fa-pied-piper-pp" aria-hidden="true"></i>
                </a>
            </div>
            <div class="col-sm-2">
                <a class="btn btn-sm btn-primary cancel-reference" href="#">
                    <?php if($transaction->reject_status==0){
                        ?>
                        <i class="fa fa-trash text-danger" aria-hidden="true"></i>
                    <?php }else{ ?>
                        <i class="fa fa-check-circle text-warning " aria-hidden="true"></i>
                    <?php } ?>
                </a>
            </div>
            <div class="col-sm-8">

                <?php
                if ($assigned) {
                    echo "<p>".get_msg('current_assigned')." : ". $assigned->user_name . "-" . $assigned->user_phone."</p>";
                }
                if($transaction->trans_status_id !=5){
                    $attributes = array('class' => 'form-inline');
                    echo form_open('/admin/orders/update', $attributes);
                    ?>
                    <select  name="assign_to" id="assign_to" class="form-control selectTwo select2 input-sm">
                        <option value=""><?php echo get_msg('stakeholder')?>...</option>
                        <?php if($assigned){ echo "<option value='".$assigned->user_id."' selected>".$assigned->user_name."-".$assigned->user_phone."</option>";}?>
                    </select>
                    <input type="hidden" name="trans_header_id" value=<?php  echo $transaction->id;  ?>>
                    <button type="submit" class="btn btn-sm btn-primary <?php echo $langauge_class; ?>" style="padding : 2px 5px; margin: 5px;"><?php echo get_msg('btn_assign')?></button>
                    <?php echo form_close();
                }else if ($transaction->trans_status_id ==5 && $this->session->userdata( 'user_id' ) == "usr75023528330eed4cc55f0e1bdd463cfe") {

                    $attributes = array('class' => 'form-inline');
                    echo form_open('/admin/orders/update', $attributes);
                    ?>
                    <select  name="assign_to" id="assign_to" class="form-control selectTwo select2 input-sm">
                        <option value=""><?php echo get_msg('stakeholder')?>...</option>
                        <?php if($assigned){ echo "<option value='".$assigned->user_id."' selected>".$assigned->user_name."-".$assigned->user_phone."</option>";}?>
                    </select>
                    <input type="hidden" name="trans_header_id" value=<?php  echo $transaction->id;  ?>>
                    <button type="submit" class="btn btn-sm btn-primary <?php echo $langauge_class; ?>" style="padding : 2px 5px; margin: 5px;"><?php echo get_msg('btn_assign')?></button>
                    <?php echo form_close();
                }?>

            </div>
        </div>

	</div>
	<!-- /.col -->
	<div class="col-sm-4 invoice-col">
	  <b><?php echo get_msg('invoice'); ?> <?php 
	  echo sprintf("%07d", $transaction->id);?></b><br>
	  <?php if($transaction->time_slot_id && $transaction->shipping_method_amount <50){
	   $timeslotinfo=$this->Timeslotsdetails->get_one($transaction->time_slot_id); ?>
	  <b><?php echo get_msg('delivery_time'); ?> : <?php echo $timeslotinfo->slot_date." (".$timeslotinfo->start_time." - ".$timeslotinfo->end_time.")"; ?></b><br>
	<?php } ?>
	  <br>
	
	  <b><?php echo get_msg('account'); ?>:</b> <?php echo $transaction->sub_total_amount ." ". $transaction->currency_short_form; ?>
	</div>
	<!-- /.col -->
	</div>

	<div class="row">
		<div class="col-12 table-responsive">
		  <table class="table table-striped">
		    <thead>
			    <tr>
			    	<th><?php echo get_msg('no'); ?></th>
			      	<th><?php echo get_msg('Prd_name'); ?></th>
			      	<th><?php echo get_msg('product_image'); ?></th>
			      	<th><?php echo get_msg('barcode'); ?></th>
					<th><?php echo get_msg('Prd_price'); ?></th>
					<th><?php echo get_msg('Prd_qty'); ?></th>
					<th><?php echo get_msg('Prd_dis'); ?></th>
					<th><?php echo get_msg('Prd_dis_price'); ?></th> 
					<th><?php echo get_msg('Prd_amt'); ?></th>
			    </tr>
		    </thead>
		    <tbody>
		    	<?php 
					$conds['transactions_header_id'] = $transaction->id;
					$all_detail =  $this->Transactiondetail->get_all_by( $conds );
                    $outOfStockAmount=0;
                    $outOfStockAmountWithTax=0;
                    $no=1;
					foreach($all_detail->result() as $transaction_detail):

				?>
				<tr>
					<td width="20"><?=$no?></td>
					<td>
						<?php 

						
						$att_name_info  = explode("#", $transaction_detail->product_attribute_name);
						$att_price_info = explode("#", $transaction_detail->product_attribute_price);


						$att_info_str = "";
						$att_flag = 0;
						if( $att_name_info && count($att_name_info[0]) > 0 ) {

							//loop attribute info
							for($k = 0; $k < count($att_name_info); $k++) {
								
								if($att_name_info[$k] != "") {
									$att_flag = 1;
									$att_info_str .= $att_name_info[$k] . " : " . $att_price_info[$k] . "(". $transaction->currency_symbol ."),";

								}
							}


						} else {
							$att_info_str = "";
						}

						

						$att_info_str = rtrim($att_info_str, ","); 

						$productinfo=$this->Product->get_one($transaction_detail->product_id);
						if( $att_flag == 1 ) {

							echo $productinfo->name .'<br>' . $att_info_str  . '<br>' ; 

						} else {

							echo $productinfo->name . '<br>';

						}


						if ($transaction_detail->product_color_id != "") {

							echo "".get_msg('color').":";

							// $color_info =  $this->Color->get_one($transaction_detail->product_color_id);
							// $color_value =  $color_info->color_value . '}';
							

							} 

						?>
						<?php 
							if($transaction_detail->product_color_name && $transaction_detail->product_color_id != ""){
								print "<span class='colorname'>".$transaction_detail->product_color_name."</span>";
							}
							?>
						<?php 
								if($this->session->userdata('language_code')=="ar" && $transaction_detail->product_color_id != ""){ ?>
						<div class="<?php echo $this->session->userdata('language_code'); ?>" style="background-color:<?php echo  $transaction_detail->product_color_code ; ?>; width: 20px; height: 20px; margin-top: -20px; margin-right: 50px;"> 
						</div>
						<?php }else if($transaction_detail->product_color_id != ""){ ?>
						<div class="<?php echo $this->session->userdata('language_code'); ?>" style="background-color:<?php echo  $transaction_detail->product_color_code; ?>; width: 20px; height: 20px; margin-top: -20px; margin-left: 34px;"> 
												</div>
						<?php }?>
						<?php echo "".get_msg('product_unit')." : " . $transaction_detail->product_measurement . " " . $transaction_detail->product_unit; ?> <br>
						<?php echo "".get_msg('shipping_cost')." : " . $transaction_detail->shipping_cost ." ". $transaction->currency_symbol; ?>
						

					</td>
					<td><?php if($productinfo->thumbnail){?><img src="<?php echo site_url( '/'); ?>uploads/small/<?php echo $productinfo->thumbnail; ?>" width="100" height="100"><?php } ?></td>
					<td><?php echo $productinfo->code; ?></td>
					<td><?php echo $transaction_detail->original_price ." ". $transaction->currency_symbol; ?></td>
					<td><?php echo $transaction_detail->qty ?></td>
					<td><?php echo "-" . $transaction_detail->discount_amount . $transaction->currency_symbol . " (" .$transaction_detail->discount_percent . "% ".get_msg('off').")"; ?></td>
					<td><?php echo $transaction_detail->price ." ". $transaction->currency_symbol; ?></td>
					<td>
                        <?php
                        if($transaction_detail->stock_status==1){
                            $itemSubTotal=$transaction_detail->qty * $transaction_detail->price;
                            if($transaction_detail->stock_status==0){
                                $outOfStockAmount +=$itemSubTotal;
                                $outOfStockAmountWithTax += $itemSubTotal+ (($transaction->tax_percent / 100) * $itemSubTotal);
                            }

                            echo $itemSubTotal  ." ". $transaction->currency_symbol;
                        }else {
                            $itemSubTotal=$transaction_detail->qty * $transaction_detail->price;
                            if($transaction_detail->stock_status==0){
                                $outOfStockAmount +=$itemSubTotal;
                                $outOfStockAmountWithTax += $itemSubTotal+ (($transaction->tax_percent / 100) * $itemSubTotal);
                            }
                            echo '<span style="color:red"><b>'.get_msg('out_of_stock').'</b></span>';
                        }
                        ?>
					</td>
				</tr>

					<?php 
				$no++;
				endforeach; ?>
		    </tbody>
		  </table>
		</div>
	<!-- /.col -->
	</div>

	<div class="row">
        <!-- accepted payments column -->
       
        <div class="col-6">
        	 <br>
            <p><?php echo get_msg('trans_payment_method'); ?><?php echo get_msg(''.$transaction->payment_method.''); ?></p>
            <?php if($transaction->payment_status ==1){
                $payment_status='<span class="btn btn-sm btn-success">'.get_msg('paid').'</span>';
            }else{
                $payment_status='<span class="badge badge-danger">'.get_msg('unpaid').'</span>';
            } ?>

          <p> <?php echo get_msg('trans_memo'); ?> <?php echo $transaction->memo; ?></p>
        </div>

        <!-- /.col -->
        <div class="col-6">
         

          <div class="table-responsive">
            <table class="table">
                <?php
                if($outOfStockAmount>0){ ?>
                    <tr>
                        <th><?php echo get_msg('out_of_stock_amount'); ?> (-):</th>
                        <td><span style="color:red"><b><?php echo $outOfStockAmount . " : ".$outOfStockAmountWithTax." ".$transaction->currency_symbol; ?></b></span></td>
                    </tr>
                <?php } ?>
		        <tr>
		            <th><?php echo get_msg('trans_coupon_discount_amount'); ?></th>
		            <td><?php echo $transaction->coupon_discount_amount . " ". $transaction->currency_symbol;; ?></td>
		        </tr>

              	<tr>
	                <th style="width:50%"><?php echo get_msg('trans_item_sub_total'); ?></th>
	                <td><?php echo $transaction->sub_total_amount . " ". $transaction->currency_symbol; ?></td>
	            </tr>
              	<tr>
	                <th><?php echo get_msg('trans_overall_tax'); ?> <?php echo "(" . $transaction->tax_percent . "%)"  ?> : (+)</th>
	                <td><?php echo $transaction->tax_amount . " ". $transaction->currency_symbol;; ?></td>
	            </tr>
	            
				<tr>
	                <th style="width:50%"><?php echo get_msg('sub_total_with_tax'); ?></th>
	                <td><span style="color:red"><b><?php echo  ($transaction->sub_total_amount + $transaction->tax_amount )  . " ". $transaction->currency_symbol; ?></b></span></td>
	            </tr>
	            <tr>
	                <th><?php echo get_msg('trans_shipping_cost'); ?>: (+)</th>
	                <td><?php echo $transaction->shipping_method_amount . " ". $transaction->currency_symbol;; ?></td>
	            </tr>

	            <tr>
	                <th><?php echo get_msg('trans_shipping_tax'); ?> <?php echo "(" . $transaction->shipping_tax_percent . ")"  ?>% : (+)</th>
	                <td><?php echo $transaction->shipping_method_amount * $transaction->shipping_tax_percent . " ". $transaction->currency_symbol; ?></td>
              	</tr>
            
              
              	<tr>
	                <th><?php echo get_msg('trans_total_balance_amount'); ?></th>
	                <td>
	                	
	                	<?php 

	                	//balance_amount = total_item_amount - coupon_discont + (overall_tax + shipping_cost + shipping_tax (based on shipping cost)) 

	                	echo  ($transaction->sub_total_amount + ($transaction->tax_amount + $transaction->shipping_method_amount + ($transaction->shipping_method_amount * $transaction->shipping_tax_percent)) );  
	                	echo " ". $transaction->currency_symbol;
	                	?>
	                </td>
              	</tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
    </div>
</div>

</div>
	</div>
</div>
<div id="loader-div">

</div>

<script>
    $("div.actionbtn").on('click', 'a.cancel-reference', function (e) {
        $("#loader-div").show();
        var confirmDel = confirm("<?php echo get_msg('are_you_sure')?> <?=$transaction->id?> ?");
        if (confirmDel == true) {
            var control = this;
            var referenceId ="<?=$transaction->id?>";
            jQuery.ajax({
                type: 'POST',
                dataType: 'JSON',
                data: {id: referenceId, cancelableStatus: 'true'},
                url: "<?php echo site_url('/admin/orders/cancelorder');?>",
                success: function (data, textStatus) {
                    confirmNormalMessages(data.isError, data.message, true);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    serverErrorToast(errorThrown);
                }
            });
        }
        e.preventDefault();
    });


    $(document).ready(function () {
        function FetchData() {
            $("#assign_to").select2({
                ajax: {
                    url: "<?php echo site_url('/admin/expense_voucher');?>",
                    dataType: 'JSON',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page,
                            stakeHolderType: "Employee_DeliveryBoy",
                            stakeholderSearch: 'true'
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: "<?php echo get_msg('Delivery_Boy')?>...",
                allowClear: true,
                width: '100%',
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            // do something
        }
        setTimeout(FetchData, 3000);
    });

    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        return "<div class='row slt-srs m-0'><div class='col-md-2 thumbnail sc-img p-0'>" + repo.imgsrc + "</div><div class='col-md-10 sc-con pl-3 pr-1'><span class='scsm-gp'><span class='sc-code lft'>" + repo.caption + '</span><span class="sc-phn rit">' + repo.phone + '</span></span><span class="scsm-gp"><span class="sc-capt lft">' + repo.phone + '</span></span><span class="scsm-gp"><span class="sc-eml lft">' + repo.email + "</span></span></div></div>";
    }

    function formatRepoSelection(repo) {
        if (repo.id === '' || repo.phone== undefined) {
            return "<?php echo get_msg('stakeholder')?>...";
        }
        return repo.phone + ' ' + repo.caption;
    }

</script>
<style>

    #loader-div {
        display: none;
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(<?php echo site_url('/');?>uploads/Preloader_2.gif) center no-repeat #fff;
    }
</style>