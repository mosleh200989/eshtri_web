
<div class="container-fluid" style="padding: 10px 30px 10px 30px;">
<div class="panel-group card" id="accordion" role="tablist" aria-multiselectable="true" class="table-responsive animated fadeInRight">
			
	<div class="card-body table-responsive p-0">
  
  		<table class="table m-0 table-striped" id="list-table">
	
			<?php $count = $this->uri->segment(4) or $count = 0; ?>

			<?php if ( !empty( $transactions ) && count( $transactions->result()) > 0 ): ?>
	<tr>
		<th><?php echo get_msg('no'); ?></th>
		<th><?php echo get_msg('invoice'); ?></th>
		<th><?php echo get_msg('shop_name'); ?></th>
		<th><?php echo get_msg('customer_phone'); ?></th>
		<th><?php echo get_msg('status_label'); ?></th>
		<th><?php echo get_msg('total_items'); ?></th>
		<th><?php echo get_msg('total_store_price'); ?></th>
		<th><?php echo get_msg('place_on'); ?></th>
		<th><?php echo get_msg('delivery_date'); ?></th>
		<th><?php echo get_msg('payment_method'); ?></th>
		<th><?php echo get_msg('payment_status'); ?></th>
		<th><?php echo get_msg('assign_to'); ?></th>
		<th><?php echo get_msg('assignment'); ?></th>
		<th><?php echo get_msg('confirm_accounting'); ?></th>
		<th><?php echo get_msg('details'); ?></th>
		<th><?php echo get_msg('print'); ?></th>
		<th><?php echo get_msg('edit'); ?></th>
	</tr>
			<tbody style="font-size: 16px;">
				<?php foreach($transactions->result() as $transaction): 
					$shop = $this->Shop->get_one($transaction->shop_id);
					?>
			  					<tr>	
			  						<td><?php echo ++$count;?></td>
			  						<td><?php echo sprintf("%07d", $transaction->id);?></td>
			  						<td style="width: 10%;">
			  							<?php
			  							if($shop){
											 echo $shop->name;
			  							}
										?>
									</td>
			  						<td style="width: 17%;">
			  							<?php
											echo $transaction->contact_name . "( ".get_msg('contact').": " . $transaction->contact_phone . " )";
										?>
									</td>
									<td>
										<?php if ($transaction->trans_status_id == 1) { ?>
							                <span class="badge badge-danger">
							                  <?php echo get_msg(''.$this->Transactionstatus->get_one( $transaction->trans_status_id )->title.''); ?>
							                </span>
							            <?php } elseif ($transaction->trans_status_id == 2) { ?>
							                <span class="badge badge-info">
							                  <?php echo get_msg(''.$this->Transactionstatus->get_one( $transaction->trans_status_id )->title.''); ?>
							                </span>
							            <?php } elseif ($transaction->trans_status_id == 3) { ?>
							                <span class="badge badge-success">
							                  <?php echo get_msg(''.$this->Transactionstatus->get_one( $transaction->trans_status_id )->title.''); ?>
							                </span>
							            <?php } else { ?>
							                <span class="badge badge-primary">
							                	 <?php echo get_msg(''.$this->Transactionstatus->get_one( $transaction->trans_status_id )->title.''); ?>
							                </span>
							            <?php } ?>
									</td>
									<td>
										<?php
											$detail_count = 0;
											$conds['transactions_header_id'] = $transaction->id;
											$detail_count =  $this->Transactiondetail->count_all_by( $conds );
											echo "<small style='padding: 0 50px'>" . $detail_count . " ".get_msg('Items')." </small>";
										 ?>
									</td>
									<td>
										<?php
											 echo  ($transaction->sub_total_amount + $transaction->tax_amount); 
										 ?>
									</td>
									<td>
										<?php echo $transaction->added_date; ?>
									</td>
									<td>
										<?php if($transaction->time_slot_id  && $transaction->shipping_method_amount <50){
												$time_slotinfo=$this->Timeslotsdetails->get_one($transaction->time_slot_id);
												print $time_slotinfo->slot_date."(".$time_slotinfo->start_time."-".$time_slotinfo->end_time.")";
											} ?>
										
									</td>
									<td>
										<?php if($transaction->payment_method){
												print get_msg(''.$transaction->payment_method.'');
											} ?>
									</td>
									<td>
										<?php if($transaction->payment_status ==1){
											print '<span class="btn btn-sm btn-success">'.get_msg('paid').'</span>';
										}if($transaction->payment_method ==1){
                                            print '<span class="badge badge-danger">'.get_msg('unpaid').'</span>';
                                        }else{
											print '<span class="badge badge-danger">'.get_msg('unpaid').'</span>';
										} ?>
									</td>
									<td>
										<?php if($transaction->assign_to){
												$userinfo=$this->User->get_one($transaction->assign_to);
												print $userinfo->user_name."-".$userinfo->user_phone;
											} ?>
									</td>
									<td>
										<?php 
											if(!$transaction->assign_to){

										?>
										<a class="pull-left btn btn-sm btn-success" href="<?php echo $module_site_url . "/assign/" . $transaction->id;?>">
											<?php echo get_msg('assign'); ?>
										</a>
									<?php }else if($transaction->trans_status_id !=5){ 
						
										?>
										<a class="pull-left btn btn-sm btn-warning" href="<?php echo $module_site_url . "/assign/" . $transaction->id;?>">
											<?php echo get_msg('re_assign'); ?>
										</a>
									<?php }else if ($transaction->trans_status_id ==5 && $this->session->userdata( 'user_id' ) == "usr75023528330eed4cc55f0e1bdd463cfe") {
										?>
										<a class="pull-left btn btn-sm btn-warning" href="<?php echo $module_site_url . "/assign/" . $transaction->id;?>">
											<?php echo get_msg('re_assign'); ?>
										</a>
										<?php 	}?>
									</td>
									<td>
										<?php 
										if($transaction->trans_status_id==5 && $transaction->accounting_status==0) {?>
										<span class="col-md-6 no-padding"><a href="" referenceid="<?php echo $transaction->id;?>" class="confirm-reference" data-title="Confirm_Data"><i class="fa fa-check-circle text-warning"></i></a>&nbsp;</span>
									<?php } ?>
									</td>
									<td>
										
										<a class="pull-right btn btn-sm btn-primary" href="<?php echo $module_site_url . "/detail/" . $transaction->id;?>">
											
											<?php echo get_msg('details'); ?>
										</a>
									</td>
									<td>
										<a class="pull-right btn btn-sm btn-primary" target="_blank" href="<?php echo $module_site_url . "/printDetailsPdf/" . $transaction->id;?>">
											<i class="fa fa-print" aria-hidden="true"></i>
										</a>
									</td>
									<td>
										<?php if($transaction->trans_status_id !=5){ 
						
										?>
										<a class="pull-left btn btn-sm btn-warning" href="<?php echo $module_site_url . "/orderedit/" . $transaction->id;?>">
											<?php echo get_msg('edit'); ?>
										</a>
									<?php }else if ($transaction->trans_status_id ==5 && $this->session->userdata( 'user_id' ) == "usr75023528330eed4cc55f0e1bdd463cfe") {
										?>
										<a class="pull-left btn btn-sm btn-warning" href="<?php echo $module_site_url . "/orderedit/" . $transaction->id;?>">
											<?php echo get_msg('edit'); ?>
										</a>
										<?php 	}?>
									</td>
								</tr>
							
						
			
			<?php endforeach; ?>
			</tbody>
			<?php else: ?>
					
				<?php $this->load->view( $template_path .'/partials/no_data' ); ?>

			<?php endif; ?>
		</table>
	</div>

</div>
</div>
<script>
	// Delete Trigger
	$('.btn-delete').click(function(){
	
		// get id and links
		var id = $(this).attr('id');
		var btnYes = $('.btn-yes').attr('href');
		var btnNo = $('.btn-no').attr('href');

		// modify link with id
		$('.btn-yes').attr( 'href', btnYes + id );
		$('.btn-no').attr( 'href', btnNo + id );
	});

	$('#list-table').on('click', 'a.confirm-reference', function (e) {
    var selectRow = $(this).parents('tr');
    var data = selectRow.find("td:eq(1)").text();
    var confirmDel = confirm("<?php echo get_msg('are_you_sure?')?>" + data + ' ?');
    if (confirmDel == true) {
        var control = this;
        var referenceId = $(control).attr('referenceid');
        jQuery.ajax({
            type: 'POST',
            dataType: 'JSON',
            data: {id: referenceId, confirmStatus: 'true'},
            url: "<?php echo site_url('/admin/orders/confirmaccountingdata');?>",
            success: function (data, textStatus) {
                if (data.isError == false) {
                	$(control).hide("fast")
                }else{
                    alert(data.message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                serverErrorToast(errorThrown);
            }
        });
    }
    e.preventDefault();
});
</script>
<?php
	// Delete Confirm Message Modal
	$data = array(
		'title' => get_msg( 'delete_trans_label' ),
		'message' =>  get_msg( 'trans_yes_all_message' ),
		'no_only_btn' => get_msg( 'cat_no_only_label' )
	);
	
	$this->load->view( $template_path .'/components/report_delete_confirm_modal', $data );
?>
