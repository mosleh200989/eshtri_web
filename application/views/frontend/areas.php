    <div id="wrapper">
    <div class="container">
 <?php $this->load->view( '/frontend/partials/topheader'); ?>
 <?php $this->load->view( '/frontend/partials/topbar'); ?>
 <?php $this->load->view( '/frontend/partials/topnav'); ?>
 <div class="container  min-h-full" style="background-color: rgb(247, 248, 250);">
    <div class="mt-6">
        <div class="mt-4 flex justify-between flex-col sm:flex-row">
            <h1 class="font-bold"><?=lang('CityCover')?></h1>
        </div>
        <div class="bg-white rounded-lg shadow my-4 p-4">
            <div class="flex flex-wrap">
                <div class="text-center w-full">
                    <h2 class="wow fadeIn">
                        <?=lang('SoonCover')?>
                    </h2>
                    <a href="#supportedareas" class="areas">
                        <?=lang('Covered')?>
                    </a>
                    <section class="page-section text-center">
    <h1 class="section-title" id="supportedareas">
        <?=lang('Covered')?>
    </h1>
    <div class="container">
        <div class="section-desc">
            <ul class="supported-areas-list">
                    <li>
                        <a href="">
                                <?=lang('RIYADH')?>
                        </a>
                    </li>
                    <li>
                        <a href="">
                                <?=lang('MEDINA')?>
                        </a>
                    </li>
                    <li>
                        <a href="">
                                <?=lang('JEDDAH')?>
                        </a>
                    </li>
                    <li>
                        <a href="">
                                <?=lang('MAKKAH')?>
                        </a>
                    </li>
                
            </ul>
        </div>
    </div>

</section>
                </div>
            </div>
        </div>
    </div>
</div>




<?php $this->load->view( '/frontend/partials/subscription'); ?>
    <?php $this->load->view( '/frontend/partials/aboutcompany'); ?>
    <?php $this->load->view( '/frontend/partials/downloadapp'); ?>
    <?php $this->load->view( '/frontend/partials/footermenu'); ?>
    <?php $this->load->view( '/frontend/partials/copyright'); ?>
    </div>
</div>

   
<style type="text/css">
    .page-banner.page-banner-home {
    /*background: #15355a;*/
    height: 700px;
/*    background-image: url(../matajer/images/Untitled-12.jpg);
    background-position: 100% 66%;
    background-size: cover;
    position: relative;
    overflow: hidden;*/
}
.page-banner-home .banner-slogan, .page-banner-center {
    text-align: center;
    position: relative;
    z-index: 100;
}
.page-banner-2 *, .page-banner-3 *, .page-banner-freelancers * {
    color: #15355a !important;
}
h1.lg-text {
    font-size: 50px;
    font-weight: bold;
    margin-top: 130px;
}
.areas {
    border: 1px solid #fff;
    display: inline-block;
    padding: 20px 40px !important;
    border-radius: 150px;
    font-size: 20px;
}
.page-section {
    padding-top: 48px;
    padding-bottom: 55px;
    border-bottom: 1px solid #edeff2;
}
.page-section {
    position: relative;
}
.text-center {
    text-align: center;
}
h1, .h1 {
    margin-top: 38px;
    margin-bottom: 38px;
    font-size: 33px;
    line-height: 46px;
}
.section-title {
    margin-left: auto;
    margin-right: auto;
}
.section-desc {
    margin-top: -4px;
    margin-left: auto;
    margin-right: auto;
    font-size: 16px;
    line-height: 28px;
}
ul.supported-areas-list {
    padding: 0;
    margin: 0;
    list-style: none;
}
ul.supported-areas-list li {
    width: 20%;
    display: inline-block;
}
ul.supported-areas-list li a {
    font-size: 19px;
    line-height: 45px;
    color: #363b40;
    cursor: pointer;
}
</style>