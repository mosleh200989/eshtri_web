<?php $this->load->view( '/frontend/partials/sidebarcartsummery'); ?>
<?php $this->load->view( '/frontend/partials/sidebarcartdetails'); ?>
<div class="wrapper">
    <!-- Sidebar  -->
    <?php $this->load->view( '/frontend/partials/productcategories'); ?>

    <!-- Page Content  -->
    <div id="content">
        <?php $this->load->view( '/frontend/partials/shoptopheader'); ?>
        <div id="main-content">
                <div class="shopproductlist" id="middlecontent">

                        <?php
                        if (!empty($newproduct) && count($newproduct->result()) > 0): ?>
                            <div class="row">
                                <div class="col-lg-12 shopSectionTexts">
                                    <h2 class="shopUIHeading pt-5 pb-5"><?php echo get_msg('New_Product')?></h2>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-12 shopproductlistup">
                                <div class="owl-carousel owl-theme">
                                    <?php foreach ($newproduct->result() as $newproducts): ?>
                                        <div class="item">
                                            <div class="productOne">
                                                <img src="https://eshtri.net/uploads/<?php echo $newproducts->thumbnail; ?>" class="img-fluid" />
                                                <div class="caption">
                                                    <h5 class="text-center shopText"><?php echo getCaption($newproducts->name, $newproducts->name_alt);?></h5>
                                                    <p class="text-center shopPara"><?php echo get_msg('SAR')?> <?php echo addvatandcomission($newproducts->unit_price, $newproducts->id, $newproducts->commission_plan); ?></p>
                                                </div>
                                                <div class="overlay text">
                                                    <p class="addText"><?php echo get_msg('Add_to_Shopping_Cart')?></p>
                                                    <span>
                                            <a href="<?php echo base_url('shopproducts/productdetails/'.$newproducts->id); ?>" class="btnShowDetails" id="myBtn">
                                                <span ><?php echo get_msg('Details')?></span>
                                                <span >  &gt;</span></a>
                                            </span></div>
                                                <?php
                                                if ($newproducts->status==1 && $newproducts->is_available==1): ?>
                                                    <a href="#" class="btn-cart" data-check-color="<?php echo $newproducts->is_has_color; ?>" data-check-attribute="<?php echo $newproducts->is_has_attribute; ?>"  data-id="<?php echo $newproducts->id; ?>"><button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;<?php echo get_msg('Add_To_Cart')?></button></a>
                                                <?php else: ?>
                                                    <span class="btn btn btn-denger text-center out_of_stock"><?php echo get_msg('out_of_stock')?></span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            </div>
                        <?php endif; ?>

                        <?php
                        if (!empty($discountproducts) && count($discountproducts->result()) > 0): ?>
                        <div class="row">
                            <div class="col-lg-12 shopSectionTexts">
                                <h2 class="shopUIHeading pt-5 pb-5"><?php echo get_msg('Discount')?></h2>
                            </div>
                        </div>


                        <div class="row">
                        <div class="col-lg-12 shopproductlistup">
                            <div class="owl-carousel owl-theme">
                                <?php foreach ($discountproducts->result() as $discount): ?>
                                <div class="item">
                                    <div class="productOne ">

                                        <img src="https://eshtri.net/uploads/<?php echo $discount->thumbnail; ?>" class="img-fluid "/>
                                        <div class="caption">
                                            <h5 class="text-center shopText"><?php echo getCaption($discount->name, $discount->name_alt); ?></h5>
                                            <p class="text-center shopPara"><?php echo get_msg('SAR')?> <?php echo addvatandcomission($discount->unit_price, $discount->id, $discount->commission_plan); ?></p>
                                            <div class="overlay text">
                                                <p class="addText"><?php echo get_msg('Add_to_Shopping_Cart')?></p>
                                                <span>
                                            <a href="<?php echo base_url('shopproducts/productdetails/'.$discount->id); ?>" class="btnShowDetails disabled-link">
                                                <span><?php echo get_msg('Details')?></span>
                                                <span>  &gt;</span></a>
                                                 <?php
                                                 if ($discount->status==1 && $discount->is_available==1): ?>
                                                     <a href="#" class="btn-cart" data-check-color="<?php echo $discount->is_has_color; ?>" data-check-attribute="<?php echo $discount->is_has_attribute; ?>"  data-id="<?php echo $discount->id; ?>"><button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;<?php echo get_msg('Add_To_Cart')?></button></a>
                                                 <?php else: ?>
                                                     <span class="btn btn btn-denger text-center out_of_stock"><?php echo get_msg('out_of_stock')?></span>
                                                 <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <?php endforeach; ?>

                            </div>
                        </div>
                        </div>
                            <?php endif; ?>

                            <?php
                            if (!empty($popularproduct) && count($popularproduct->result()) > 0): ?>
                            <div class="row">
                                <div class="col-lg-12 shopSectionTexts">
                                    <h2 class="shopUIHeading pt-5 pb-5"><?php echo get_msg('Popular')?></h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 shopproductlistup">
                                    <div class="owl-carousel owl-theme">
                                        <?php foreach ($popularproduct->result() as $popularproducts): ?>
                                            <div class="item">
                                                <div class="productOne ">
                                                    <img src="https://eshtri.net/uploads/<?php echo $popularproducts->thumbnail; ?>" class="img-fluid "/>
                                                    <div class="caption">
                                                        <h5 class="text-center shopText"><?php echo getCaption($popularproducts->name, $popularproducts->name_alt); ?></h5>
                                                        <p class="text-center shopPara"><?php echo get_msg('SAR')?> <?php echo addvatandcomission($popularproducts->unit_price, $popularproducts->id, $popularproducts->commission_plan); ?></p>
                                                        <div class="overlay text">
                                                            <p class="addText"><?php echo get_msg('Add_to_Shopping_Cart')?></p>
                                                            <span>
                                            <a href="<?php echo base_url('shopproducts/productdetails/'.$popularproducts->id); ?>" class="btnShowDetails disabled-link">
                                                <span><?php echo get_msg('Details')?></span>
                                                <span>  &gt;</span></a>
                                           </span></div>
                                                        <?php
                                                        if ($popularproducts->status==1 && $popularproducts->is_available==1): ?>
                                                            <a href="#" class="btn-cart" data-check-color="<?php echo $popularproducts->is_has_color; ?>" data-check-attribute="<?php echo $popularproducts->is_has_attribute; ?>"  data-id="<?php echo $popularproducts->id; ?>"><button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;<?php echo get_msg('Add_To_Cart')?></button></a>
                                                        <?php else: ?>
                                                            <span class="btn btn btn-denger text-center out_of_stock"><?php echo get_msg('out_of_stock')?></span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>



                    </div>



                </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>