

<!-- Button trigger modal -->

<!-- Modal -->
<div id="wrapper" class="dashboarddetails">
    <?php $this->load->view('/frontend/partials/topheader'); ?>
    <div class="row">
        <div class="col-md-12">
            <!---------------shop Body------------------>
       <div class="container-fluid dashboard">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <!---Dashboard---->
                            <div class="dashboardSection container">
                                <div class="dashnoardNavigation side-contents">
                                    <div id="sticky-con">
                                    <div class="userDetails">
                                        <div class="userDetails-content">
                                            <i class="fa fa-user-circle-o"  aria-hidden="true"></i>
<!--                                            <img src="./daashboard_files/user.svg" class="img-fluid" style="width: 55%;">-->
                                            <h4 class="userDetails-name">Lorem Ipsum</h4>
                                        </div>
                                    </div>
                                    <div class="dnLink">
                                        <ul>
                                            <li>
                                                <a href="<?php echo base_url('dashboard'); ?>" ><i class="fa fa-user" aria-hidden="true active"></i>  <?php echo get_msg('Dashboard') ?></a>
                                                <a href="<?php echo base_url('orderlist'); ?>" > <i class="fa fa-first-order" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Orders') ?></a>
                                                <a href="<?php echo base_url('address'); ?>" ><i class="fa fa-globe" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Address') ?></a>
                                                <a href="<?php echo base_url('accountdetails'); ?>" ><i class="fa fa-money" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Account_Details') ?></a>
                                                <a href="<?php echo base_url('messages'); ?>" ><i class="fa fa-comments" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Messages') ?></a>
                                                <a href="<?php echo base_url('logout'); ?>" ><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Log_Out') ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                    </div>
                                </div>
                                <div class="dashnoardDetails">
                                    <div class="dashnoardInner">
                                        <div id="img2" style="display: block;">

                                            <?php
                                            if (!empty($orders) && count($orders->result()) > 0){ ?>
                                            <div class="row">
                                                <div class="col-lg-12 mx-auto">
                                                    <!-- List group-->
                                                    <ul class="list-group shadow">
                                                        <!-- list group item-->
                                                        <?php foreach ($orders->result() as $order): ?>
                                                        <li class="list-group-item">
                                                            <!-- Custom content-->
<!--                                                            <div class="media align-items-lg-center flex-column flex-lg-row p-3">-->
                                                                <div class="card card-2">
                                                                    <div class="card-heade headerCard">
                                                                        <h4 class="text-white headers"><span class="fa fa-check red-text pr-3 text-danger"></span> <?php echo get_msg('Order_No')?> : <?php echo $order->id; ?></h4>

                                                                    </div>
                                                                    <div class="backBody">
                                                                        <div class="card-body bg-white inBody">
                                                                            <?php
                                                                            $shopName=$this->Shop->get_one_by( array( 'shop' => $order->shop_id));
                                                                            $statusTitle=$this->Transactionstatus->get_one_by( array( 'transactionstatus' => $order->trans_status_id));
                                                                            ?>
                                                                            <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"> <?php echo get_msg('Shop_Name')?> : </label> <?php echo $shopName->name; ?></h6></h5>
                                                                            <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"><?php echo get_msg('Total_Amount')?> : </label> <?php echo $order->balance_amount; ?></h6></h5>
                                                                            <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"><?php echo get_msg('Status')?> : </label> <?php echo $statusTitle->title; ?></h6></h5>
                                                                            <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"><?php echo get_msg('Submitted_Date')?> : </label> <?php echo $order->added_date; ?></h6></h5>

                                                                            <div class="text-center">
                                                                                <div class="card-heade headerCard">
                                                                                    <a href="<?php echo base_url('/orderlist/orderdetails/'.$order->id); ?>">
                                                                                        <button type="button" class="btn" data-dismiss="modal"><span class="btnText"><?php echo get_msg('View_Details'); ?></span>  </button>
                                                                                    </a>
                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </div>

<!--                                                            </div>-->
                                                <!-- End -->

                                                        </li> <!-- End -->
                                                        <!-- list group item-->
                                                        <?php endforeach; ?>
                                                    </ul> <!-- End -->
                                                </div>
                                            </div>


                                                <?php } else { ?>
                                            <div class="row">
                                                <div class="col-lg-12 ss">
                                                    <img src="https://eshtri.net/uploads/small/6143.jpeg" class="img-fluid">
                                                    <div class="caption">
                                                        <h3 class="text-center">No Orders Found</h3>
                                                        <p class="text-center">You have no orders please start shopping at Sharaf DG and place orders</p>
                                                        <button type="button" class="btn btn-secondary saveBtn">Start Shopping Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                                <?php } ?>
<!--                                            --><?php //endif; ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!---Dashborad--->

                        </div>
                    </div>
                </div>
            </div>


            <!------------------shop Body------------------>
            <div class="clear"></div>

        </div>
    </div>



<style>
    .btnText{
        color: white;
    }
    .allLabel{
        font-weight: normal;
    }
    .headerCard{
        background-color: #15355C;
        /*width: 788px;*/
    }
    .inBody{
        padding: 10px;
        border-radius: 20px 20px !important;
        margin: 5px;

    }
    .backBody{
        padding: 5px;
        border-radius: 20px 20px !important;
        margin-top: 10px;
        background-color: #D9D9D9 !important;
    }
    .headers{
        padding: 10px;
    }
    .headerCard{
        border-radius: 20px 20px !important;
    }
    .dashboarddetails{
        margin-top: 50px;
    }
    #sticky-con {
        position: sticky;
        top: 105px;
        width: 100%;
    }
    .side-contents {
        top: 0;
        left: 0;
        /*width: 100%;*/
        height: 100%;
    }
    .dashnoardDetails{
        height: auto;
    }
</style>
<script>


    /*const myFunction6 = () => {
        document.getElementById("img1").style.display ='block';
        document.getElementById("img2").style.display ='none'
        document.getElementById("img3").style.display ='none'
        document.getElementById("img4").style.display ='none'
        document.getElementById("img5").style.display ='none'

    }

    const myFunction7 = () => {
        document.getElementById("img2").style.display ='block'
        document.getElementById("img3").style.display ='none'
        document.getElementById("img1").style.display ='none'
        document.getElementById("img4").style.display ='none'
        document.getElementById("img5").style.display ='none'
    }

    const myFunction8 = () => {
        document.getElementById("img3").style.display ='block'
        document.getElementById("img2").style.display ='none'
        document.getElementById("img1").style.display ='none'
        document.getElementById("img4").style.display ='none'
        document.getElementById("img5").style.display ='none'
    }
    const myFunction9 = () => {
        document.getElementById("img4").style.display ='block'
        document.getElementById("img3").style.display ='none'
        document.getElementById("img2").style.display ='none'
        document.getElementById("img1").style.display ='none'
        document.getElementById("img5").style.display ='none'
    }
    const myFunction10 = () => {
        document.getElementById("img5").style.display ='block'
        document.getElementById("img4").style.display ='none'
        document.getElementById("img3").style.display ='none'
        document.getElementById("img2").style.display ='none'
        document.getElementById("img1").style.display ='none'
    }
    const myFunction11 = () => {

    }*/

</script>

</div>
<!-------wrapper------->