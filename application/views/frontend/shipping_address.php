

<!-- Button trigger modal -->

<!-- Modal -->
<div id="wrapper" class="shipping_addressdetails">
    <?php $this->load->view( '/frontend/partials/topheader'); ?>
    <div class="container-fluid shipping_address">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="jsx-2001395417 progressIndicator threeSteps">
                        <div class="jsx-2001395417 siteWidthContainer">
                            <ul class="jsx-2001395417 steps">
                                <li class="jsx-2001395417 step active"><?php echo get_msg('Shipping_Address'); ?></li>
                                <li class="jsx-2001395417 step"><?php echo get_msg('confirmation'); ?></li>
                                <li class="jsx-2001395417 step active"><?php echo get_msg('payment'); ?></li>
                                <li class="jsx-2001395417 step"><?php echo get_msg('Order_Placed'); ?></li>
                            </ul>
                        </div>
                    </div>


                    <div id="content" class="jsx-3438394366 siteWidthContainer">
                        <div class="jsx-3438394366 pageWrapper">
                            <div class="jsx-1676968614 container">
                                <div class="jsx-1676968614 addressBook">
                                    <div class="jsx-1676968614 titleRow">
                                        <h2 class="jsx-1676968614 shippingTitle"><?php echo get_msg('Shipping_Address'); ?></h2>
                                    </div>
                                    <div class="jsx-1676968614 addressesWrapper">

                                        <?php
                                        $default_address_id="";
                                        foreach ($useraddress->result() as $address):
                                            ?>
                                            <div class="jsx-1676968614 addressContainer">
                                                <label for="address-102409012" class="jsx-1676968614">
                                                    <input type="radio" name="selectedAddress" id="address-102409012" class="jsx-1676968614" value="<?=$address->id?>" <?=$address->is_default ==1? "checked":""?>>
                                                    <div class="jsx-1023123869 container <?=$address->is_default ==1? "selected":""?> ">
                                                        <?php
                                                        if ($address->is_default ==1):
                                                            $default_address_id=$address->id;
                                                            ?>
                                                            <h2 class="jsx-1023123869 boxHeader"><?php echo get_msg('Primary_Address'); ?></h2>
                                                        <?php endif;?>
                                                        <div class="jsx-1023123869 innerContainer latlng">
                                                            <div class="jsx-1023123869 addressLabelWrapper">
										<span class="jsx-1023123869 addressLabel"><?php echo get_msg('Home'); ?><img src="https://k.nooncdn.com/s/app/2019/noon-checkout/ab9626ae084246fce8e5a868e0d1b08cb5134054/static/images/set_from_map.png" alt="set_on_map" srcset="https://k.nooncdn.com/s/app/2019/noon-checkout/ab9626ae084246fce8e5a868e0d1b08cb5134054/static/images/set_from_map.png 1x, https://k.nooncdn.com/s/app/2019/noon-checkout/ab9626ae084246fce8e5a868e0d1b08cb5134054/static/images/set_from_map@2x.png 2x,https://k.nooncdn.com/s/app/2019/noon-checkout/ab9626ae084246fce8e5a868e0d1b08cb5134054/static/images/set_from_map@3x.png 3x" class="jsx-1023123869">
										</span>
                                                                <div class="jsx-4177912104 wrapperClass addressListToolTip">
                                                                    <div class="jsx-4177912104 tooltipContainer">...<div class="jsx-4177912104 tooltipContentWrapper ">
                                                                            <div class="jsx-4177912104 tooltip">
                                                                                <div class="jsx-4177912104 tooltipText">
                                                                                    <div class="jsx-1023123869 tooltipWrapper">
                                                                                        <button aria-label="Edit" class="jsx-1023123869">
                                                                                            <i class="fa fa-pencil-square-o" aria-hidden="true" style="color: rgb(64, 69, 83); font-size: 13px;"></i>
                                                                                            <span class="jsx-1023123869"><?php echo get_msg('Edit'); ?></span>
                                                                                        </button>
                                                                                        <?php
                                                                                        if ($address->is_default !=1):
                                                                                            ?>
                                                                                            <button aria-label="<?php echo get_msg('Set_as_Default'); ?>" class="jsx-1023123869">
                                                                                                <i class="fa fa-check-circle" aria-hidden="true" style="color: rgb(64, 69, 83); font-size: 13px;"></i>
                                                                                                <span class="jsx-1023123869"><?php echo get_msg('Set_as_Default'); ?></span>
                                                                                            </button>
                                                                                        <?php endif;?>
                                                                                        <button aria-label="Delete" class="jsx-1023123869">
                                                                                            <i class="fa fa-trash-o" style="color: rgb(64, 69, 83); font-size: 13px;" aria-hidden="true"></i>
                                                                                            <span class="jsx-1023123869"><?php echo get_msg('Delete'); ?></span>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="jsx-1023123869 addressDetailsContainer">
                                                                <div class="jsx-1023123869 addressRow">
                                                                    <span class="jsx-1023123869 addressColumn addressColumnLeft"><?php echo get_msg('Name'); ?></span>
                                                                    <span class="jsx-1023123869 addressColumn fullName"><?=$address->receiverName?></span>
                                                                </div>
                                                                <div class="jsx-1023123869 addressRow">
                                                                    <span class="jsx-1023123869 addressColumn addressColumnLeft"><?php echo get_msg('Address'); ?></span>
                                                                    <div class="jsx-1023123869 addressColumn fullAddress"><?=$address->address?> <?=$address->city?> <?=$address->district?></div>
                                                                </div>
                                                                <div class="jsx-1023123869 addressRow">
                                                                    <span class="jsx-1023123869 addressColumn addressColumnLeft"><?php echo get_msg('Phone_Number'); ?></span>
                                                                    <div class="jsx-1023123869 addressColumn phoneNoWrapper verifiedLabel">
                                                                        <span class="jsx-1023123869 phoneNumber"><?=$address->mobileNo?></span>
                                                                        <div class="jsx-1023123869 verificationStatus">
                                                                            <i class="icon icon-radio-checked check" style="color: rgb(64, 69, 83); font-size: 13px;">
                                                                            </i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>
                                        <?php
                                        endforeach;
                                        ?>

                                        <div class="jsx-1676968614 addNewAddressCtr">
                                            <a href="<?php echo site_url('/customerlogin/newaddressmap/'.$current_user_id);?>">
                                                <button aria-label="<?php echo get_msg('Add_a_new_address'); ?>" class="jsx-1676968614 addNewAddress"><?php echo get_msg('Add_a_new_address'); ?></button>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="jsx-1676968614 buttonWrapper">
                                        <button aria-label="Continue" class="jsx-3898435964 ripple primary" id="shipping_continue"><?php echo get_msg('Continue'); ?><div class="jsx-3898435964 active">
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>





                </div>
            </div>
        </div>
    </div>
    <!------------------shop Body------------------>
    <div class="clear"></div>

</div>
<!-------wrapper------->

<style>
    button {
        background-color: transparent;
        cursor: pointer;
    }
    label.jsx-1676968614{
        cursor: pointer;
    }
    .shipping_address{
        margin-top: 65px;
    }
    .progressIndicator.jsx-2001395417 {
        padding: 18px 0px 15px;
        width: 100%;
        background-color: rgb(255, 255, 255);
    }
    .siteWidthContainer {
        padding: 0 15px;
        margin: 0 auto;
        max-width: 1400px;
        box-sizing: border-box;
    }
    ul.steps.jsx-2001395417 {
        display: flex;
        counter-reset: progressCount 0;
        -webkit-box-pack: center;
        justify-content: center;
    }
    li.step.jsx-2001395417 {
        list-style: none;
        display: inline-block;
        position: relative;
        text-align: center;
        font-size: 0.75rem;
        color: rgb(126, 133, 155);
        z-index: 0;
        transition: all 300ms ease-in-out 0s;
    }
    li.step.active.jsx-2001395417 {
        color: rgb(64, 69, 83);
        font-weight: bold;
    }
    .progressIndicator.threeSteps.jsx-2001395417 li.step.jsx-2001395417 {
        flex: 0 1 33.33%;
    }
    li.step.jsx-2001395417::before {
        content: counter(progressCount);
        counter-increment: progressCount 1;
        background-color: rgb(255, 255, 255);
        border-radius: 100%;
        display: block;
        text-align: center;
        margin: 0px auto 5px;
    }
    li.step.jsx-2001395417::before {
        width: 1.667rem;
        height: 1.667rem;
        line-height: 1.667rem;
        border: 0.083rem solid rgb(226, 229, 241);
        padding: 0.103rem;
    }
    li.step.active.jsx-2001395417::before, li.step.completed.jsx-2001395417::before {
        width: 1.667rem;
        height: 1.667rem;
        line-height: 1.667rem;
        border: 0.186rem solid rgb(255, 255, 255);
        padding: 0px;
    }
    li.step.active.jsx-2001395417::before {
        color: rgb(255, 255, 255);
        background-color: rgb(56, 174, 4);
    }
    li.step.jsx-2001395417::after {
        content: "";
        position: absolute;
        width: 100%;
        height: 0.33rem;
        top: 0.85rem;
        background-color: rgb(226, 229, 241);
        border-radius: 5px;
        right: -50%;
        z-index: -1;
    }
    .progressIndicator.threeSteps.jsx-2001395417 li.step.jsx-2001395417:last-child, .progressIndicator.fourSteps.jsx-2001395417 li.step.jsx-2001395417:last-child {
        flex: 1 0 10%;
    }
    @media only screen and (min-width: 768px){
        .siteWidthContainer {
            padding: 0 30px;
        }
        li.step.jsx-2001395417 {
            display: flex;
            white-space: nowrap;
            -webkit-box-align: baseline;
            align-items: baseline;
            font-size: 1rem;
        }
        li.step.jsx-2001395417::before {
            flex: 0 0 1.667rem;
            display: inline-block;
            margin: 0px 5px 5px;
        }
        li.step.active.jsx-2001395417::before {
            margin: 0px 5px 5px;
        }
        li.step.jsx-2001395417::after {
            flex: 1 1 60%;
            display: inline-block;
            position: relative;
            top: unset;
            right: unset;
            margin: 0px 8px 0px 12px;
        }
        .progressIndicator.threeSteps.jsx-2001395417 li.step.jsx-2001395417:last-child, .progressIndicator.fourSteps.jsx-2001395417 li.step.jsx-2001395417:last-child {
            flex: 0 0 10%;
        }
    }
    @media only screen and (min-width: 992px) {
        .siteWidthContainer {
            padding: 0 45px;
        }
    }

 /*address css start*/

    .siteWidthContainer {
        padding: 0 15px;
        margin: 0 auto;
        max-width: 1400px;
        box-sizing: border-box;
    }
    #content.jsx-3438394366 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        width: 100%;
    }
    .container.jsx-1676968614 {
        padding: 20px 0px;
    }
    .addressBook.jsx-1676968614 {
        padding-bottom: 10px;
    }
    .titleRow.jsx-1676968614 {
        display: flex;
        flex-direction: column;
    }
    h2.jsx-1676968614 {
        font-size: 1.66667rem;
        font-weight: bold;
        margin-bottom: 15px;
    }
    .addressContainer.jsx-1676968614 {
        margin-top: 12px;
        cursor: pointer;
        box-sizing: border-box;
    }
    label.jsx-1676968614 [type="radio"].jsx-1676968614 {
        display: none;
    }
    .container.jsx-1023123869 {
        line-height: 1.5;
        border: 1px solid rgb(226, 229, 241);
        box-shadow: rgba(0, 0, 0, 0.04) 0px 3px 2px 0px;
        border-radius: 2px;
        background-color: rgb(255, 255, 255);
        display: flex;
        flex-direction: column;
    }
    .container.selected.jsx-1023123869 {
        border: 1px solid rgb(56, 102, 223);
        box-shadow: rgb(56, 102, 223) 0px 0px 0px 1px, rgba(0, 0, 0, 0.04) 0px 3px 2px 0px;
    }
    .boxHeader.jsx-1023123869 {
        border-bottom: 1px solid rgb(226, 229, 241);
        padding: 10px 15px 9px;
        background: rgb(252, 251, 244);
        font-weight: bold;
        font-size: 1.08333rem;
        height: 41px;
    }
    .innerContainer.jsx-1023123869 {
        padding: 13px 15px 16px;
        flex: 1 1 0%;
    }
    .addressLabelWrapper.jsx-1023123869 {
        font-weight: 600;
        font-size: 1.16666rem;
        display: flex;
        -webkit-box-pack: justify;
        justify-content: space-between;
        margin-bottom: 8px;
    }
    .addressLabel.jsx-1023123869 {
        display: flex;
        -webkit-box-align: center;
        align-items: center;
    }
    .addressLabel.jsx-1023123869 img.jsx-1023123869 {
        margin-left: 4px;
    }
    .wrapperClass.jsx-4177912104 {
        line-height: 1;
        text-align: initial;
    }
    .tooltipContainer.jsx-4177912104 {
        position: relative;
        display: inline-block;
    }
    .tooltipContentWrapper.jsx-4177912104 {
        position: absolute;
        padding-top: calc(100% + 10px);
        top: 0px;
        z-index: 1;
        right: -12px;
        display: none;
        width: 256px;
    }
    .tooltipContainer.jsx-4177912104:hover .tooltipContentWrapper.jsx-4177912104 {
        display: block;
    }

    .addressListToolTip.jsx-4177912104 .tooltipContentWrapper.jsx-4177912104 {
        width: 130px;
    }
    .tooltipContainer.jsx-4177912104 .tooltip.jsx-4177912104 {
        position: relative;
        padding: 14px;
        background-color: rgb(255, 255, 255);
        border: 1px solid rgb(226, 229, 241);
        top: calc(100% + 10px);
        font-size: 12px;
        line-height: 1.4;
        color: rgb(64, 69, 83);
        box-shadow: rgba(0, 0, 0, 0.04) 0px 0px 9px 0px;
    }
    .addressListToolTip.jsx-4177912104 .tooltip.jsx-4177912104 {
        padding: 0px 14px;
        opacity: 1;
    }
    .tooltipWrapper.jsx-1023123869 {
        display: flex;
        flex-direction: column;
    }
    .tooltipWrapper.jsx-1023123869 button.jsx-1023123869 {
        padding: 10px 0px;
        text-align: left;
        display: flex;
        -webkit-box-align: center;
        align-items: center;
        border: 0px;
    }
    .tooltipWrapper.jsx-1023123869 span.jsx-1023123869 {
        margin-left: 6px;
    }
    .tooltipWrapper.jsx-1023123869 button.jsx-1023123869 + button.jsx-1023123869 {
        border-top: 1px solid rgb(226, 229, 241);
    }
    .addressRow.jsx-1023123869 {
        display: flex;
    }
    .addressColumnLeft.jsx-1023123869 {
        min-width: 96px;
        color: rgb(126, 133, 155);
    }
    .fullName.jsx-1023123869 {
        font-weight: 600;
        font-size: 1.16666rem;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .addressRow.jsx-1023123869 + .addressRow.jsx-1023123869 {
        margin-top: 15px;
    }
    .fullAddress.jsx-1023123869 {
        word-break: break-word;
        overflow: hidden;
    }
    .phoneNoWrapper.jsx-1023123869 {
        display: flex;
        -webkit-box-align: center;
        align-items: center;
    }
    .verificationStatus.jsx-1023123869 {
        display: flex;
        flex-direction: row;
        -webkit-box-align: center;
        align-items: center;
        text-transform: uppercase;
        font-size: 0.916666rem;
    }
    .verifiedLabel .check {
        color: rgb(56, 174, 4) !important;
    }
    .buttonWrapper.jsx-1676968614 {
        position: fixed;
        bottom: 0px;
        left: 0px;
        right: 0px;
        z-index: 10;
        background-color: rgb(255, 255, 255);
        padding: 10px 15px 6px;
        box-shadow: rgba(0, 0, 0, 0.3) 0px 0px 6px;
    }
    .ripple.jsx-3898435964 {
        margin: 0px 0px 4px;
        border: none;
        background: none;
        outline: none;
        padding: 17.5px 40px;
        border-radius: 2px;
        text-align: center;
        font-size: 1rem;
        font-weight: bold;
        line-height: 1;
        cursor: pointer;
        position: relative;
        overflow: hidden;
        transform: translate3d(0px, 0px, 0px);
        transition: all 0.2s ease-in 0s;
        width: 100%;
    }
    .primary.jsx-3898435964 {
        background: rgb(56, 102, 223);
        color: rgb(255, 255, 255);
        text-transform: uppercase;
    }
    .primary.jsx-3898435964:hover {
        box-shadow: rgba(0, 0, 0, 0.15) 0px 3px 2px 0px;
    }
    .active.jsx-3898435964 {
        background: transparent;
        position: absolute;
        height: 100%;
        width: 100%;
        left: 0px;
        top: 0px;
        transition: all 0.2s ease-in 0s;
    }
    .primary.jsx-3898435964:active .active.jsx-3898435964 {
        background: rgb(64, 69, 83);
        opacity: 0.2;
    }
    .ripple.jsx-3898435964::after {
        content: "";
        display: block;
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        pointer-events: none;
        background-image: radial-gradient(circle, rgb(0, 0, 0) 10%, transparent 10.01%);
        background-repeat: no-repeat;
        background-position: 50% center;
        transform: scale(10, 10);
        opacity: 0;
        transition: transform 0.5s ease 0s, opacity 1s ease 0s;
    }
    .ripple.jsx-3898435964:active.jsx-3898435964::after {
        transform: scale(0, 0);
        opacity: 0.2;
        transition: all 0s ease 0s;
    }

    .jsx-1023123869.container{
        padding: 0px;
    }



    @media only screen and (min-width: 480px) {
        .titleRow.jsx-1676968614 {
            flex-direction: row;
            -webkit-box-pack: justify;
            justify-content: space-between;
        }
        .addressesWrapper.jsx-1676968614 {
            display: flex;
            flex-wrap: wrap;
            align-content: center;
            margin-left: -12px;
            margin-top: -12px;
        }
        .addressContainer.jsx-1676968614, .addNewAddressCtr.jsx-1676968614 {
            width: 50%;
            padding-left: 12px;
            box-sizing: border-box;
        }
        label.jsx-1676968614, label.jsx-1676968614 > div.jsx-1676968614, .addNewAddress.jsx-1676968614 {
            height: 100%;
            width: 100%;
        }
    }
    @media only screen and (min-width: 768px){
        .siteWidthContainer {
            padding: 0 30px;
        }
        .addressesWrapper.jsx-1676968614 {
            margin-left: -15px;
            margin-top: -15px;
        }
        .addressesWrapper.jsx-1676968614 {
            margin-left: -20px;
            margin-top: -20px;
        }
        .addressContainer.jsx-1676968614, .addNewAddressCtr.jsx-1676968614 {
            width: 100%;
            padding-left: 15px;
            margin-top: 15px;
        }
        .addressDetailsContainer.jsx-1023123869 {
            display: flex;
        }
        .addressRow.jsx-1023123869 {
            width: 33%;
            flex-direction: column;
        }
        .addressRow.jsx-1023123869 + .addressRow.jsx-1023123869 {
            margin: 0px;
            padding-left: 15px;
        }
        .buttonWrapper.jsx-1676968614 {
            position: static;
            background-color: transparent;
            box-shadow: none;
            padding: 30px 0px 0px;
            display: flex;
            -webkit-box-pack: end;
            justify-content: flex-end;
        }
        .ripple.jsx-3898435964 {
            width: unset;
        }
    }
    @media only screen and (min-width: 992px) {
        .siteWidthContainer {
            padding: 0 45px;
        }

        .addressContainer.jsx-1676968614, .addNewAddressCtr.jsx-1676968614 {
            padding-left: 20px;
            margin-top: 20px;
            width: 33.3333%;
        }
        .container.jsx-1023123869 {
            height: 100%;
        }
        .addressDetailsContainer.jsx-1023123869 {
            flex-direction: column;
        }
        .addressRow.jsx-1023123869 {
            width: 100%;
            flex-direction: row;
        }
        .addressRow.jsx-1023123869 + .addressRow.jsx-1023123869 {
            margin-top: 15px;
            padding: 0px;
        }
    }
    @media only screen and (min-width: 1200px) {
        .addressContainer.jsx-1676968614, .addNewAddressCtr.jsx-1676968614 {
            width: 25%;
        }
    }
    @media screen and (max-width: 768px) {
        .buttonWrapper.jsx-1676968614 {
            position: relative;
            margin-top: 28px;
        }
    }
</style>

<script>
    <?php
    if($default_address_id){
        echo 'localStorage.setItem("shipping_add_id", "'.$default_address_id.'");';
    }
    ?>
    jQuery('.addressContainer').click(function(e){
        e.preventDefault();
        jQuery('.jsx-1023123869.container').removeClass("selected");
        jQuery(this).find('.jsx-1023123869.container').addClass("selected");
        jQuery('.jsx-1676968614').prop("checked", false);
        jQuery(this).find('input[type="radio"]').prop("checked", true);
        localStorage.setItem("shipping_add_id", $('input[name="selectedAddress"]:checked').val());
    });

    jQuery('#shipping_continue').click(function(e){
        e.preventDefault();
        if(localStorage.getItem("shipping_add_id")){
            var redirecturl="<?php echo site_url('/checkout/confirmation/'.$current_order_id.'');?>";
            window.location.href = redirecturl;
        }else{
            alert("<?php echo get_msg('Please_select_shipping_address'); ?>");
        }

    });

</script>