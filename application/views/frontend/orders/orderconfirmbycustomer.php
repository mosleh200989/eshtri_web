

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="card" style="padding: 10px 10px 10px 10px;">
            <div class="invoice__title">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="invoice__logo text-center">
                            <img src="https://eshtri.net/assets/front/matajer/images/logo-en.png" alt="woo commerce logo">
                        </div>
                    </div>
                    <div class="col-lg-6 text-right">
                    </div>
                </div>
            </div>
            <br>
            <div class="row invoice__metaInfo mb-4">
                <div class="col-lg-6">
                    <div class="invoice__orderDetails">

                        <strong><u><?php echo get_msg('order_details'); ?></u> </strong><br>
                        <span><strong><?php echo get_msg('Invoice_Number'); ?> :</strong> <?php echo $transaction->trans_code; ?></span><br>
                        <span><strong><?php echo get_msg('Order_Date'); ?> :</strong> <?php echo $transaction->added_date; ?></span><br>
                        <span><strong><?php echo get_msg('Order_ID'); ?> :</strong> <?php echo $transaction->id; ?></span><br>
                        <span> <strong><?php echo get_msg('trans_payment_method'); ?> :</strong> <?php echo get_msg(''.$transaction->payment_method.''); ?></span><br><br>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="invoice__shipping">
                        <strong><u><?php echo get_msg('shipping_address'); ?></u></strong><br>
                        <span><strong><?php echo get_msg('name'); ?></strong>: <?php echo $transaction->shipping_first_name . " " . $transaction->shipping_last_name; ?></span><br>
                        <span><strong><?php echo get_msg('address1'); ?></strong>: <?php echo $transaction->shipping_address_1?></span><br>
                        <span><strong><?php echo get_msg('address2'); ?></strong>: <?php echo $transaction->shipping_address_2?></span><br>
                        <span><strong><?php echo get_msg('phone'); ?></strong>: <?php echo $transaction->shipping_phone;?></span><br>
                        <span><strong><?php echo get_msg('email'); ?></strong>: <?php echo $transaction->shipping_email;?></span>

                    </div>
                </div>
            </div>



            <div class="invoice p-3 mb-3">
                <!-- title row -->

                <div class="row">
                    <div class="col-12 table-responsive">
                        <form action="<?php echo site_url('/confirmorderbycustomer');?>" method="post" id="defaultForm">
                            <input type="hidden" class="transactions_header_id" name="transactions_header_id" value="<?=$transaction->id?>">
                            <input type="hidden" class="tax_percent" name="tax_percent" value="<?=$transaction->tax_percent?>">
                            <input type="hidden" class="coupon_discount_amount" name="coupon_discount_amount" value="<?=$transaction->coupon_discount_amount?>">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th width="70"><?php echo get_msg('product_image'); ?></th>
                                    <th><?php echo get_msg('product_details'); ?></th>
                                    <th width="70"><span class="th-title"><?php echo get_msg('btn_delete')?></span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $conds['transactions_header_id'] = $transaction->id;
                                $all_detail =  $this->Transactiondetail->get_all_by( $conds );
                                $rowNumber=0;
                                $outOfStockAmount=0;
                                $outOfStockAmountWithTax=0;
                                $no=1;
                                foreach($all_detail->result() as $transaction_detail):

                                    ?>
                                    <tr>
                                        <td><?php if($transaction_detail->product_photo){?><img src="<?php echo site_url( '/'); ?>uploads/small/<?php echo $transaction_detail->product_photo; ?>" width="70" height="70"><?php } ?></td>
                                        <td>
                                            <?php
                                            $att_name_info  = explode("#", $transaction_detail->product_attribute_name);
                                            $att_price_info = explode("#", $transaction_detail->product_attribute_price);
                                            $att_info_str = "";
                                            $att_flag = 0;
                                            if( count($att_name_info[0]) > 0 ) {

                                                //loop attribute info
                                                for($k = 0; $k < count($att_name_info); $k++) {

                                                    if($att_name_info[$k] != "") {
                                                        $att_flag = 1;
                                                        $att_info_str .= $att_name_info[$k] . " : " . $att_price_info[$k] . "(". $transaction->currency_symbol ."),";

                                                    }
                                                }

                                            } else {
                                                $att_info_str = "";
                                            }

                                            $att_info_str = rtrim($att_info_str, ",");


                                            if( $att_flag == 1 ) {

                                                echo "<h6>".$this->Product->get_one($transaction_detail->product_id)->name .'</h6>' . $att_info_str  . '<br>' ;

                                            } else {

                                                echo "<h6>".$this->Product->get_one($transaction_detail->product_id)->name . '</h6>';

                                            }


                                            if ($transaction_detail->product_color_id != "") {

                                                echo "".get_msg('color').":";

                                                $color_value =  $this->Color->get_one($transaction_detail->product_color_id)->color_value . '}';


                                            }

                                            ?>

                                            <div style="background-color:<?php echo  $this->Color->get_one($transaction_detail->product_color_id)->color_value ; ?>; width: 20px; height: 20px; margin-top: -20px; margin-left: 50px;">
                                            </div>

                                            <?php
                                            if($transaction_detail->product_measurement && $transaction_detail->product_unit){
                                                echo "".get_msg('product_unit')." : " . $transaction_detail->product_measurement . " " . $transaction_detail->product_unit."<br>";
                                            }
                                            ?>
                                            <?php
                                            if($transaction_detail->shipping_cost>0){
                                                echo "".get_msg('shipping_cost')." : " . $transaction_detail->shipping_cost ." ". $transaction->currency_symbol;} ?>

                                            <input type="hidden" class="id" name="idDtls[<?=$rowNumber?>]" value="<?=$transaction_detail->id?>">
                                            <input type="hidden" min="0" step="any" class="originalPrice form-control" name="originalPriceDtls[<?=$rowNumber?>]" value="<?=$transaction_detail->original_price?>">

                                            <span><strong><?php echo get_msg('price'); ?></strong> <?php echo $transaction_detail->price.$transaction_detail->currency_symbol; ?></span><br>



                                            <div class="input-group item-quantity">
                                                <span><strong><?php echo get_msg('Prd_qty'); ?></strong> </span>
                                                <input data-id="<?php echo $transaction_detail->id;?>"  name="qtyDtls[<?=$rowNumber?>]" value="<?=$transaction_detail->qty?>" type="number" readonly="" class="form-control qty" min="1" max="1593">
                                                <span class="input-group-btn ">
                                <button type="button" value="quantity" class="quantity-right-plus btn qtypluscart" data-type="plus" data-field="">
                                    <span class="fas fa-plus">+</span>
                                </button>
                                <button type="button" value="quantity" class="quantity-left-minus btn qtyminuscart" data-type="minus" data-field="">
                                    <span class="fas fa-minus">-</span>
                                </button>
                            </span>
                                            </div>

                                            <?php
                                            $itemSubTotal=$transaction_detail->qty * $transaction_detail->price;
                                            if($transaction_detail->stock_status==0){
                                                $outOfStockAmount +=$itemSubTotal;
                                                $outOfStockAmountWithTax += $itemSubTotal+ (($transaction->tax_percent / 100) * $itemSubTotal);
                                            }


                                            ?>
                                            <span><strong><?php echo get_msg('sub_total'); ?>:  </strong> <?php echo $itemSubTotal . "" . $transaction->currency_symbol; ?></span><br>
                                            <input type="hidden" class="stock form-control" name="stockDtls[<?=$rowNumber?>]" value="1">
                                        </td>

                                        <td>

                                            <a herf='#' class='btn-delete btn btn-danger' data-toggle="modal" data-target="#myModal" id="<?php echo $transaction_detail->id;?>">
                                                <i style='font-size: 18px;' class='fa fa-trash-o'></i> <?php echo get_msg('btn_delete'); ?>
                                            </a>

                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                    $rowNumber++; ?>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                    </div>
                    <!-- /.col -->
                </div>

                <div class="row">
                    <!-- accepted payments column -->

                    <div class="col-6">
                        <br>
                        <p><?php echo get_msg('trans_payment_method'); ?><?php echo get_msg(''.$transaction->payment_method.''); ?></p>

                        <p> <?php echo get_msg('trans_memo'); ?> <?php echo $transaction->memo; ?></p>
                    </div>

                    <!-- /.col -->
                    <div class="col-6">

                        <table class="table">
                            <?php
                            if($outOfStockAmount>0){ ?>
                                <tr>
                                    <th><?php echo get_msg('out_of_stock_amount'); ?> (-):</th>
                                    <td style="text-align: end;"><span style="color:red"><b><?php echo $outOfStockAmount . " : ".$outOfStockAmountWithTax." ".$transaction->currency_symbol; ?></b></span></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <th><?php echo get_msg('trans_coupon_discount_amount'); ?></th>
                                <td style="text-align: end;"><?php echo $transaction->coupon_discount_amount . " ". $transaction->currency_symbol; ?></td>
                            </tr>

                            <tr>
                                <th style="width:50%"><?php echo get_msg('trans_item_sub_total'); ?></th>
                                <td style="text-align: end;"><?php echo "<span id='sub_total'>".$transaction->sub_total_amount . "</span> ". $transaction->currency_symbol; ?></td>
                            </tr>
                            <tr>
                                <th><?php echo get_msg('trans_overall_tax'); ?> <?php echo "(" . $transaction->tax_percent . "%)"  ?> : (+)</th>
                                <td style="text-align: end;"><?php echo "<span id='total_tax'>".$transaction->tax_amount . "</span> ". $transaction->currency_symbol; ?></td>
                            </tr>

                            <tr>
                                <th style="width:50%"><?php echo get_msg('sub_total_with_tax'); ?></th>
                                <td style="text-align: end;"><span style="color:red"><b><?php echo  "<span id='sub_total_with_tax'>".($transaction->sub_total_amount + $transaction->tax_amount )  . "</span> ". $transaction->currency_symbol; ?></b></span></td>
                            </tr>
                            <tr>
                                <th><?php echo get_msg('trans_shipping_cost'); ?>: (+)</th>
                                <td style="text-align: end;">
                                    <input type="hidden" min="0" class="shippingCost form-control" name="shipping_method_amount" value="<?=$transaction->shipping_method_amount?>"><b><?php echo  $transaction->shipping_method_amount  . " ". $transaction->currency_symbol; ?></b>
                                </td>
                            </tr>

                            <tr>
                                <th><?php echo get_msg('trans_shipping_tax'); ?> <?php echo "(" . $transaction->shipping_tax_percent . ")"  ?>% : (+)</th>
                                <td style="text-align: end;"><?php echo $transaction->shipping_method_amount * $transaction->shipping_tax_percent . " ". $transaction->currency_symbol; ?></td>
                            </tr>


                            <tr>
                                <th><?php echo get_msg('trans_total_balance_amount'); ?></th>
                                <td style="text-align: end;">
                                    <?php
                                    echo  "<span id='total_balance_amount'>".($transaction->sub_total_amount + ($transaction->tax_amount + $transaction->shipping_method_amount + ($transaction->shipping_method_amount * $transaction->shipping_tax_percent)) ). "</span> ";
                                    echo " ". $transaction->currency_symbol;
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th><?php echo get_msg('trans_payment_method'); ?></th>
                                <td>
                                    <input type="hidden"  class="payment_mtd form-control" id="trans_payment_method" name="trans_payment_method" value="<?=$transaction->payment_method?>">
                                    <ul class="thumbnails row payments">
                                        <li class="span4 col-md-4" data-id="paytabs">
                                            <div class="thumbnail card <?=$transaction->payment_method =="paytabs"? "active":""?>">
                                                <div class="caption">
                                                    <h3><?php echo get_msg('paytabs'); ?></h3>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="span4 col-md-4" data-id="POSTerminal">
                                            <div class="thumbnail card <?=$transaction->payment_method =="POSTerminal"? "active":""?>">
                                                <div class="caption">
                                                    <h3><?php echo get_msg('POSTerminal'); ?></h3>

                                                </div>
                                            </div>
                                        </li>
                                        <li class="span4 col-md-4" data-id="COD">
                                            <div  class="thumbnail card <?=$transaction->payment_method =="COD"? "active":""?>">
                                                <div class="caption">
                                                    <h3><?php echo get_msg('COD'); ?></h3>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                        <input type="submit" value="<?php echo get_msg('Confirm'); ?>" class="btn btn-block btn-primary">
                        </form>
                    </div>
                </div>
                <!-- /.col -->
            </div>
        </div>

    </div>
</div>
<div class="col-md-2"></div>
</div>

<script>
    <?php
    $shop_id = $transaction->shop_id;
    $order_id = $transaction->id;
    ?>
</script>
<script>
    function runAfterJQ() {
        $(document).ready(function(){
            // Delete Trigger
            $('.btn-delete').click(function(){
                // get id and links
                var id = $(this).attr('id');
                <?php
                $token="AAAAdnoaKhs:APA91bF31-_EfboVJfZwJnAAZ2UUxosDYkRWPsbet6FpzkD8O6bok51020h5XzFZXlAdSGmMnO5RzWju1Lc_HOSPOqJsaHaLrISRtESFv_xdojm_GlVtqapEp7NxV2Uc7-x4KcZ4d6mu";
                ?>
                var btnYes = "<?php echo site_url('/deleteordertoconfirm?token_id='.$token.'&order_id='.$transaction->id.'&order_details_id=');?>";
                $('.btn-yes').attr( 'href', btnYes + id );
            });

            $(".qty").bind('keyup mouseup', function () {
                var id = $(this).attr('data-id');
                <?php
                $token="AAAAdnoaKhs:APA91bF31-_EfboVJfZwJnAAZ2UUxosDYkRWPsbet6FpzkD8O6bok51020h5XzFZXlAdSGmMnO5RzWju1Lc_HOSPOqJsaHaLrISRtESFv_xdojm_GlVtqapEp7NxV2Uc7-x4KcZ4d6mu";
                ?>
                var qty = $(this).val();
                if(qty>0) {
                    jQuery.ajax({
                        type: 'GET',
                        dataType: 'JSON',
                        data: {order_id: "<?=$transaction->id?>", qty: qty, order_details_id: id, token_id:"<?=$token?>", increaseQtyData: true},
                        url: "<?php echo site_url('/deleteordertoconfirm');?>",
                        success: function (data, textStatus) {
                            if (data.isError == false) {
                                $('#sub_total').html(data.data.sub_total_amount);
                                $('#total_tax').html(data.data.tax_amount);
                                $('#sub_total_with_tax').html(parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount));
                                $('#total_balance_amount').html(parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount));
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            // serverErrorToast(errorThrown);
                        }
                    });
                }


            });
            $('.thumbnails.payments li').click(function(){
                // get id and links
                var id = $(this).attr('data-id');
                $('#trans_payment_method').val(id);
                $('.thumbnails.payments li .card').removeClass("active");
                $(this).find('.card').addClass("active");
            });
            // $(document.body).on("submit","#defaultForm",function(e){
            //     var transaction = "<?=$transaction->id?>";
            //     var payment_method = $("#trans_payment_method").val();
            //         var url = "<?php //echo site_url('/confirmorderbycustomer');?>";
            //         $.ajax({
            //             url: url,
            //             type: 'POST',
            //             dataType: "json",
            //             data: {order_id: "<?=$transaction->id?>", payment_method: payment_method},
            //             success: function (data) {
            //                 if (data.isError == false) {
            //                     // confirmNormalMessages(data.isError, data.message, true);
            //                     if(data.orderdata.payment_url){
            //                         window.location.href = data.orderdata.payment_url;
            //                     }

            //                 } else {
            //                     alert(data.message);
            //                 }
            //             },
            //             failure: function (data) {
            //             }
            //         })

            //     e.preventDefault();
            // });



            //cart item price

            jQuery(".qtyminuscart").click(function(e) {

                // Stop acting like a button
                e.preventDefault();

                // Get the field name
                fieldName = jQuery(this).attr('field');

                // Get its current value
                var currentVal = parseInt(jQuery(this).parents('span').prev('.qty').val());
                var minimumVal =  jQuery(this).parents('span').prev('.qty').attr('min');
                // If it isn't undefined or its greater than 0
                var qty = minimumVal;
                if (!isNaN(currentVal) && currentVal > minimumVal) {
                    // Decrement one
                    qty = currentVal - 1;
                    jQuery(this).parents('span').prev('.qty').val(qty);
                } else {
                    // Otherwise put a 0 there
                    jQuery(this).parents('span').prev('.qty').val(minimumVal);

                }
                var id = jQuery(this).parents('span').prev('.qty').attr('data-id');
                <?php
                $token="AAAAdnoaKhs:APA91bF31-_EfboVJfZwJnAAZ2UUxosDYkRWPsbet6FpzkD8O6bok51020h5XzFZXlAdSGmMnO5RzWju1Lc_HOSPOqJsaHaLrISRtESFv_xdojm_GlVtqapEp7NxV2Uc7-x4KcZ4d6mu";
                ?>

                if(qty>0) {
                    jQuery.ajax({
                        type: 'GET',
                        dataType: 'JSON',
                        data: {order_id: "<?=$transaction->id?>", qty: qty, order_details_id: id, token_id:"<?=$token?>", increaseQtyData: true},
                        url: "<?php echo site_url('/deleteordertoconfirm');?>",
                        success: function (data, textStatus) {
                            if (data.isError == false) {
                                $('#sub_total').html(data.data.sub_total_amount);
                                $('#total_tax').html(data.data.tax_amount);
                                $('#sub_total_with_tax').html(parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount));
                                $('#total_balance_amount').html(parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount));
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            // serverErrorToast(errorThrown);
                        }
                    });
                }

            });

            jQuery('.qtypluscart').click(function(e){
                e.preventDefault();
                // Get the field name
                //fieldName = jQuery(this).attr('field');
                // Get its current value
                var currentVal = parseInt(jQuery(this).parents('span').prev('.qty').val());
                var maximumVal =  jQuery(this).parents('span').prev('.qty').attr('max');


                //alert(maximum);
                // If is not undefined
                if (!isNaN(currentVal)) {
                    if(maximumVal!=0){
                        if(currentVal < maximumVal ){
                            // Increment
                            var qty=currentVal + 1;
                            jQuery(this).parents('span').prev('.qty').val(qty);

                            var id = jQuery(this).parents('span').prev('.qty').attr('data-id');
                            <?php
                            $token="AAAAdnoaKhs:APA91bF31-_EfboVJfZwJnAAZ2UUxosDYkRWPsbet6FpzkD8O6bok51020h5XzFZXlAdSGmMnO5RzWju1Lc_HOSPOqJsaHaLrISRtESFv_xdojm_GlVtqapEp7NxV2Uc7-x4KcZ4d6mu";
                            ?>

                            if(qty>0) {
                                jQuery.ajax({
                                    type: 'GET',
                                    dataType: 'JSON',
                                    data: {order_id: "<?=$transaction->id?>", qty: qty, order_details_id: id, token_id:"<?=$token?>", increaseQtyData: true},
                                    url: "<?php echo site_url('/deleteordertoconfirm');?>",
                                    success: function (data, textStatus) {
                                        if (data.isError == false) {
                                            $('#sub_total').html(data.data.sub_total_amount);
                                            $('#total_tax').html(data.data.tax_amount);
                                            $('#sub_total_with_tax').html(parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount));
                                            $('#total_balance_amount').html(parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount));
                                        }
                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        // serverErrorToast(errorThrown);
                                    }
                                });
                            }
                        }
                    }

                } else {
                    // Otherwise put a 0 there
                    jQuery(this).prev('.qty').val(0);
                }
            });

        });
    }
</script>
<style>
    @media screen and (max-width: 1000px) {
        body#register {
            padding: 0px 0;
        }
        .card {
            margin: 0px 0px 0px 0px;
        }
        .col-6 {
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }
        .table {
            font-size: 10px;
        }
        .thumbnails.payments{}
        .thumbnails.payments li{}
    }
    .table td, .table th {
        vertical-align: middle;
    }
    .input-group {
        position: relative;
        display: flex;
        flex-wrap: wrap;
        align-items: stretch;
        width: 100%;
    }
    .item-quantity input {
        text-align: center;
        padding-left: 5px;
        padding-right: 5px;
        box-shadow: none;
        font-size: 0.875rem;
        font-weight: 800;
        height: 100%;
    }
    .item-quantity {
        width: 110px;
        height: 44px;
    }
    .input-group > .form-control, .input-group > .form-control-plaintext, .input-group > .custom-select, .input-group > .custom-file {
        position: relative;
        flex: 1 1 0%;
        min-width: 0;
        margin-bottom: 0;
    }
    .input-group .form-control {
        height: calc(2.25rem + 6px);
    }
    .input-group > .form-control:not(:last-child), .input-group > .custom-select:not(:last-child) {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }
    .form-control:disabled, .form-control[readonly] {
        background-color: #e9ecef;
        opacity: 1;
    }
    .item-quantity .input-group-btn {
        float: left;
        width: 30px;
    }
    .item-quantity .input-group-btn button {
        display: block;
        height: 21px;
        padding: 0 10px;
        font-size: 10px;
        border: 1px solid #dee2e6;
    }
    .item-quantity .input-group-btn button {
        display: block;
        height: 21px;
        padding: 0 10px;
        font-size: 10px;
        border: 1px solid #dee2e6;
        background-color: #fff;
        color: #000;
    }
    .input-group-btn:not(:first-child)>.btn, .input-group-btn:not(:first-child)>.btn-group {
        margin-top: 0px;
        border-radius: 0;
    }
    .input-group-btn:not(:first-child)>.btn, .input-group-btn:not(:first-child)>.btn-group {
        margin-top: 0px;
        border-radius: 0;
    }
    .input-group-btn:not(:first-child)>.btn-group:first-child, .input-group-btn:not(:first-child)>.btn:first-child {
        margin-left: 0px;
    }

    element.style {
    }
    .item-quantity .input-group-btn {
        float: left;
        width: 30px;
    }
    .input-group-btn {
        position: relative;
        -ms-flex-align: stretch;
        align-items: stretch;
        font-size: 0;
        white-space: nowrap;
    }
    .input-group-addon, .input-group-btn {
        white-space: nowrap;
    }
    .input-group-addon, .input-group-btn {
        display: block;
    }
    .card .btn span{
        padding: 0px;
        margin: 0px 3px;
    }
    .item-quantity .input-group-btn button.qtyminuscart{
        font-size: 15px;
    }
    .payments{
        list-style: none;
    }
    .payments .card{
        margin: 3px;
        text-align: center;
        padding: 10px;
        height: 75px;
        vertical-align: middle;
        cursor: pointer;
    }
    .payments .card.active{
        background-color: #5eb824;
        color: #ffffff;
    }
    .payments .card h3{
        font-size: 15px;
    }
    .payments .card .caption{

    }
    .h6, h6 {
        font-size: 1rem;
        font-size: 13px;
    }
</style>
<?php
// Delete Confirm Message Modal
$data = array(
    'title' => get_msg( 'delete_product_from_list_label' ),
    'message' =>  get_msg( 'product_from_list_delete_confirm_message' ),
    'no_only_btn' => get_msg( 'product_from_list_only_label' )
);

$this->load->view( $template_path .'/components/delete_confirm_modal', $data );
?>