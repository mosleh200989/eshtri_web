

<!-- Button trigger modal -->

<!-- Modal -->
<div id="wrapper" class="shipping_addressdetails">
    <?php $this->load->view( '/frontend/partials/topheader'); ?>
    <div class="row">
        <div class="col-md-12">
            <!---------------shop Body------------------>
            <div class="container-fluid shipping_address">
                <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="jsx-2001395417 progressIndicator threeSteps">
                            <div class="jsx-2001395417 siteWidthContainer">
                                <ul class="jsx-2001395417 steps">
                                    <li class="jsx-2001395417 step completed"><?php echo get_msg('Shipping_Address'); ?></li>
                                    <li class="jsx-2001395417 step completed"><?php echo get_msg('confirmation'); ?></li>
                                    <li class="jsx-2001395417 step active"><?php echo get_msg('payment'); ?></li>
                                    <li class="jsx-2001395417 step"><?php echo get_msg('Order_Placed'); ?></li>
                                </ul>
                            </div>
                        </div>
                        <?php
                        echo "<pre>";
                       // print_r($timeslots);
                        echo "</pre>";
                        ?>
                        <form action="<?php echo site_url('/checkout/checkout_submit');?>" method="post" id="defaultForm">
                            <input type="hidden" id="transactions_header_id" name="transactions_header_id" value="<?=$order_id?>">
                            <input type="hidden" id="user_id" name="user_id" value="<?=$current_user_id?>">
                            <input type="hidden" id="shipping_address_id" name="shipping_address_id" value="">
                            <input type="hidden" id="time_slot_id" name="time_slot_id" value="">
                            <input type="hidden" id="payment_method" name="payment_method" value="">
                            <input type="hidden" id="shipping_method_amount" name="shipping_method_amount" value="15">
                            <div class="jsx-3438394366 pageWrapper">
                                <div class="jsx-1676968614 container" id="timeslot_area">
                                    <div class="row slot-row">
                                        <div class="col-xs-12 ">
                                            <nav>
                                                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                                    <?php
                                                    $i=0;
                                                    foreach ($timeslots as $slots):
                                                    ?>
                                                        <a class="nav-item nav-link <?=$i==0? 'active':''?>" id="nav-<?=$slots['id']?>-tab" data-toggle="tab" href="#nav-<?=$slots['id']?>" role="tab" aria-controls="nav-<?=$slots['id']?>" aria-selected="true"><span class="slotdayname slotheading"><?=$slots['day_name']?></span><span class="slotclass slotheading"><?=$slots['slot_date']?></span></a>
                                                    <?php
                                                        $i++;
                                                    endforeach;
                                                    ?>
                                                </div>
                                            </nav>
                                            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                                                <?php
                                                $i=0;
                                                foreach ($timeslots as $slots):
                                                ?>
                                                <div class="tab-pane fade <?=$i==0? 'show active':''?>" id="nav-<?=$slots['id']?>" role="tabpanel" aria-labelledby="nav-<?=$slots['id']?>-tab">
                                                    <?php
                                                    $i=0;
                                                    foreach ($slots['timeslots'] as $slot):
                                                    ?>
                                                        <div class="card" style="width: 18rem;" data-id="<?=$slot->id?>">
                                                            <div class="card-body">
                                                                <p class="card-text"><?=$slot->start_time?></p>
                                                                <p class="card-text">To</p>
                                                                <p class="card-text"><?=$slot->end_time?></p>
                                                            </div>
                                                        </div>

                                                     <?php
                                                        $i++;
                                                    endforeach;
                                                    ?>
                                                </div>

                                                <?php
                                                $i++;
                                                endforeach;
                                                ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="jsx-1676968614 container" id="payment_method">
                                    <div class="row payment-row">
                                        <div class="col-xs-12 ">
                                            <div class="card payment-card" style="width: 18rem;">
                                                <h5 class="card-title"><?php echo get_msg('trans_payment_method'); ?></h5>
                                                <div class="card-body">
                                                    <ul class="thumbnails row payments">
                                                        <li class="span4 col-md-4" data-id="paytabs">
                                                            <div class="thumbnail card <?=$transaction->payment_method =="paytabs"? "active":""?>">
                                                                <div class="caption">
                                                                    <h3><?php echo get_msg('paytabs'); ?></h3>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="span4 col-md-4" data-id="POSTerminal">
                                                            <div class="thumbnail card <?=$transaction->payment_method =="POSTerminal"? "active":""?>">
                                                                <div class="caption">
                                                                    <h3><?php echo get_msg('POSTerminal'); ?></h3>

                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="span4 col-md-4" data-id="COD">
                                                            <div  class="thumbnail card <?=$transaction->payment_method =="COD"? "active":""?>">
                                                                <div class="caption">
                                                                    <h3><?php echo get_msg('COD'); ?></h3>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                                <div class="jsx-1676968614 buttonWrapper">
                                    <button type="submit" aria-label="Continue" class="jsx-3898435964 ripple primary   "><?php echo get_msg('submit'); ?><div class="jsx-3898435964 active">
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </form>
                                </div>
                            </div>

                        </div>





                    </div>
                </div>
            </div>
            </div>
            <!------------------shop Body------------------>
            <div class="clear"></div>

        </div>
    </div>

</div>
<!-------wrapper------->

<style>
    button {
        background-color: transparent;
        cursor: pointer;
    }
    label.jsx-1676968614{
        cursor: pointer;
    }
    .shipping_address{
        margin-top: 65px;
    }
    .progressIndicator.jsx-2001395417 {
        padding: 18px 0px 15px;
        width: 100%;
        background-color: rgb(255, 255, 255);
    }
    .siteWidthContainer {
        padding: 0 15px;
        margin: 0 auto;
        max-width: 1400px;
        box-sizing: border-box;
    }
    ul.steps.jsx-2001395417 {
        display: flex;
        counter-reset: progressCount 0;
        -webkit-box-pack: center;
        justify-content: center;
    }
    li.step.jsx-2001395417 {
        list-style: none;
        display: inline-block;
        position: relative;
        text-align: center;
        font-size: 0.75rem;
        color: rgb(126, 133, 155);
        z-index: 0;
        transition: all 300ms ease-in-out 0s;
    }
    li.step.active.jsx-2001395417 {
        color: rgb(64, 69, 83);
        font-weight: bold;
    }
    .progressIndicator.threeSteps.jsx-2001395417 li.step.jsx-2001395417 {
        flex: 0 1 33.33%;
    }
    li.step.jsx-2001395417::before {
        content: counter(progressCount);
        counter-increment: progressCount 1;
        background-color: rgb(255, 255, 255);
        border-radius: 100%;
        display: block;
        text-align: center;
        margin: 0px auto 5px;
    }
    li.step.jsx-2001395417::before {
        width: 1.667rem;
        height: 1.667rem;
        line-height: 1.667rem;
        border: 0.083rem solid rgb(226, 229, 241);
        padding: 0.103rem;
    }
    li.step.active.jsx-2001395417::before, li.step.completed.jsx-2001395417::before {
        width: 1.667rem;
        height: 1.667rem;
        line-height: 1.667rem;
        border: 0.186rem solid rgb(255, 255, 255);
        padding: 0px;
    }
    li.step.active.jsx-2001395417::before {
        color: rgb(255, 255, 255);
        background-color: rgb(56, 174, 4);
    }
    li.step.jsx-2001395417::after {
        content: "";
        position: absolute;
        width: 100%;
        height: 0.33rem;
        top: 0.85rem;
        background-color: rgb(226, 229, 241);
        border-radius: 5px;
        right: -50%;
        z-index: -1;
    }
    .progressIndicator.threeSteps.jsx-2001395417 li.step.jsx-2001395417:last-child, .progressIndicator.fourSteps.jsx-2001395417 li.step.jsx-2001395417:last-child {
        flex: 1 0 10%;
    }
    li.step.completed.jsx-2001395417::before {
        font-size: 0.8rem;
        color: rgb(255, 255, 255);
        background-color: rgb(56, 174, 4);
    }
    li.step.active.jsx-2001395417::before, li.step.completed.jsx-2001395417::before {
        width: 1.667rem;
        height: 1.667rem;
        line-height: 1.667rem;
        border: 0.186rem solid rgb(255, 255, 255);
        padding: 0px;
    }
    li.step.completed.jsx-2001395417::after {
        background-color: rgb(56, 174, 4);
    }
    @media only screen and (min-width: 768px){
        .siteWidthContainer {
            padding: 0 30px;
        }
        li.step.jsx-2001395417 {
            display: flex;
            white-space: nowrap;
            -webkit-box-align: baseline;
            align-items: baseline;
            font-size: 1rem;
        }
        li.step.jsx-2001395417::before {
            flex: 0 0 1.667rem;
            display: inline-block;
            margin: 0px 5px 5px;
        }
        li.step.active.jsx-2001395417::before {
            margin: 0px 5px 5px;
        }
        li.step.jsx-2001395417::after {
            flex: 1 1 60%;
            display: inline-block;
            position: relative;
            top: unset;
            right: unset;
            margin: 0px 8px 0px 12px;
        }
        .progressIndicator.threeSteps.jsx-2001395417 li.step.jsx-2001395417:last-child, .progressIndicator.fourSteps.jsx-2001395417 li.step.jsx-2001395417:last-child {
            flex: 0 0 10%;
        }
        li.step.completed.jsx-2001395417::after {
            background-color: rgb(226, 229, 241);
        }
    }
    @media only screen and (min-width: 992px) {
        .siteWidthContainer {
            padding: 0 45px;
        }
    }

 /*address css start*/

    .siteWidthContainer {
        padding: 0 15px;
        margin: 0 auto;
        max-width: 1400px;
        box-sizing: border-box;
    }
    #content.jsx-3438394366 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        width: 100%;
    }
    .container.jsx-1676968614 {
        padding: 20px 0px;
    }
    nav > .nav.nav-tabs{

        border: none;
        color:#fff;
        background:#272e38;
        border-radius:0;

    }
    nav > div a.nav-item.nav-link,
    nav > div a.nav-item.nav-link.active
    {
        border: none;
        padding: 18px 10px;
        color:#fff;
        background:#272e38;
        border-radius:0;
        border: 1px solid #414b58;
    }

    nav > div a.nav-item.nav-link.active:after
    {
        content: "";
        position: relative;
        bottom: -60px;
        left: -10%;
        border: 15px solid transparent;
        border-top-color: #e74c3c ;
    }
    .tab-content{
        background: #fdfdfd;
        line-height: 25px;
        border: 1px solid #ddd;
        border-top:5px solid #e74c3c;
        border-bottom:5px solid #e74c3c;
        padding:30px 25px;
    }

    nav > div a.nav-item.nav-link:hover,
    nav > div a.nav-item.nav-link:focus
    {
        border: none;
        background: #e74c3c;
        color:#fff;
        border-radius:0;
        transition:background 0.20s linear;
    }

    nav > div a.nav-item.nav-link.active {
        border: none;
        background: #e74c3c;
        color: #fff;
        border-radius: 0;
        transition: background 0.20s linear;
    }

    #timeslot_area .tab-pane .card {
        display: inline-block;
        text-align: center;
        border: 1px solid rgba(0,0,0,.125);
        border-radius: 20px;
        width: 185px!important;
        margin: 10px;
        font-weight: bold;
        cursor: pointer;
    }

    #timeslot_area .tab-pane.active .card.active {
        background-color: #47b004;
        color: #fff;
    }

    .slotheading{
        display: block;
    }

    #payment_method .payment-card{
        background: #fdfdfd;
        line-height: 25px;
        border: 1px solid #ddd;
        border-top: 5px solid #e74c3c;
        border-bottom: 5px solid #e74c3c;
        width: 100%!important;
    }
    .slot-row{
        display: block;
    }
    #payment_method .payment-row{
        display: block;
    }
    #payment_method .payment-card .card-title {
        padding: 16px;
        border-bottom: 1px solid #e74c3c;
    }
    .payments{
        list-style: none;
    }
    .payments .card{
        margin: 3px;
        text-align: center;
        padding: 10px;
        height: 75px;
        vertical-align: middle;
        cursor: pointer;
    }
    .payments .card.active{
        background-color: #5eb824;
        color: #ffffff;
    }
    .payments .card h3{
        font-size: 15px;
    }
    .payments .card .caption{

    }

    .buttonWrapper.jsx-1676968614 {
        position: fixed;
        bottom: 0px;
        left: 0px;
        right: 0px;
        z-index: 10;
        background-color: rgb(255, 255, 255);
        padding: 10px 15px 6px;
        box-shadow: rgba(0, 0, 0, 0.3) 0px 0px 6px;
    }
    .ripple.jsx-3898435964 {
        margin: 0px 0px 4px;
        border: none;
        background: none;
        outline: none;
        padding: 17.5px 40px;
        border-radius: 2px;
        text-align: center;
        font-size: 1rem;
        font-weight: bold;
        line-height: 1;
        cursor: pointer;
        position: relative;
        overflow: hidden;
        transform: translate3d(0px, 0px, 0px);
        transition: all 0.2s ease-in 0s;
        width: 100%;
    }
    .primary.jsx-3898435964 {
        background: rgb(56, 102, 223);
        color: rgb(255, 255, 255);
        text-transform: uppercase;
    }
    .primary.jsx-3898435964:hover {
        box-shadow: rgba(0, 0, 0, 0.15) 0px 3px 2px 0px;
    }
    @media only screen and (min-width: 768px){
        .buttonWrapper.jsx-1676968614 {
            position: static;
            background-color: transparent;
            box-shadow: none;
            padding: 30px 0px 0px;
            display: flex;
            -webkit-box-pack: end;
            justify-content: flex-end;
        }
        .ripple.jsx-3898435964 {
            width: unset;
        }
    }

    @media screen and (max-width: 768px) {
        .buttonWrapper.jsx-1676968614 {
            position: relative;
            margin-top: 28px;
        }
    }
</style>

<script>
    document.getElementById("shipping_address_id").value  = localStorage.getItem("shipping_add_id");

    jQuery('#timeslot_area .card').click(function(e){
        e.preventDefault();
        jQuery('#timeslot_area .card').removeClass("active");
        jQuery(this).addClass("active");
        jQuery(this).attr("data-id");

         jQuery('#time_slot_id').val(jQuery(this).attr("data-id"));
        // jQuery(this).find('input[type="radio"]').prop("checked", true);
        // console.log($('input[name="selectedAddress"]:checked').val());

    });
    $('.thumbnails.payments li').click(function(){
        // get id and links
        var id = $(this).attr('data-id');
        $('#payment_method').val(id);
        $('.thumbnails.payments li .card').removeClass("active");
        $(this).children('.card').addClass("active");
    });

     $(document.body).on("submit","#defaultForm",function(e){
         var transaction_id =  $("#transactions_header_id").val();
         var payment_method = $("#payment_method").val();
         var time_slot_id = $("#time_slot_id").val();
         var shipping_address_id = $("#shipping_address_id").val();
         var user_id = $("#user_id").val();
         if(!time_slot_id){
             alert("<?php echo get_msg('please_select_delivery_time'); ?>");
             e.preventDefault();
             return false;
         }
         if(!shipping_address_id){
             alert("<?php echo get_msg('please_select_shipping_address'); ?>");
             e.preventDefault();
             return false;
         }
         if(!payment_method){
             alert("<?php echo get_msg('please_select_payment_method'); ?>");
             e.preventDefault();
             return false;
         }
         if(!transaction_id){
             alert("<?php echo get_msg('order_id_not_found'); ?>");
             e.preventDefault();
             return false;
         }

         if(!user_id){
             alert("<?php echo get_msg('user_not_found'); ?>");
             e.preventDefault();
             return false;
         }
     });

</script>