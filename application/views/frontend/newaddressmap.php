
<!-- Button trigger modal -->

<!-- Modal -->
<div id="wrapper" class="shipping_addressdetails">
    <?php $this->load->view( '/frontend/partials/topheader'); ?>
    <div class="shipping_address">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="map">
                        <div class="mapHeading">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php
                                    if($hasaddress){?>
                                        <a href="<?php echo base_url('customerlogin/shippingaddress/'.$current_user_id); ?>" class="map-heading hidden-xs">
                                            <i class="fa fa-share"></i>
                                            <?php echo get_msg('Back_to_Addresses'); ?>
                                        </a>
                                    <?php }else{?>
                                        <span class="map-heading"><?php echo get_msg('Add_a_new_address'); ?></span>
                                    <?php } ?>
                                </div>
                                <div class="col-md-6">
                                    <a href="<?php echo base_url('shopproducts/shopdashboard/'.$selected_front_shop_id); ?>" class="continue_shopping_link hidden-xs">
                                        <i class="fa fa-share"></i>
                                        <?php echo get_msg('Continue_Shopping'); ?>
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="mapInput">
                            <input type="text" name="text" id="pac-input" class="form-control controls" placeholder="<?php echo get_msg('Add_a_new_address'); ?>">
                        </div>
                        <div id="map" style="height: 300px"></div>
                        <div class="mapbtn">
                            <a href="#" id="mapsubmit"><button type="button" class="btn btn pull-right shadow"><?php echo get_msg('Submit'); ?></button></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!------------------shop Body------------------>
    <div class="clear"></div>

</div>
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjp5OfeOhMfejWHUjy0TVtdwhTlc_NYxs&callback=initAutocomplete&libraries=places&v=weekly"
    defer
></script>

<script>
    function initAutocomplete() {
        var marker;
        const map = new google.maps.Map(document.getElementById("map"), {
            center: { lat: 21.543333, lng: 39.172779 },
            zoom: 13,
            mapTypeId: "roadmap",
        });
        var input = document.getElementById("pac-input");
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo("bounds", map);

        function placeMarker(location) {
            if (marker == undefined){
                marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    animation: google.maps.Animation.DROP,
                    // icon:markerIcon,
                });
            }
            else{
                marker.setPosition(location);
            }
            map.setCenter(location);

        }

        google.maps.event.addListener(autocomplete, "place_changed", function(){
            var place = autocomplete.getPlace();
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(15);
            }
        });

        google.maps.event.addListener(map, "click", function(event)
        {
            placeMarker(event.latLng);

            var lat = parseFloat(event.latLng.lat());
            var lng = parseFloat(event.latLng.lng());
            var latlong = {lat: parseFloat(event.latLng.lat()), lng: parseFloat(event.latLng.lng())};

            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'location': latlong}, function(results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        previousZoom = map.zoom;
                        if (typeof(Storage) !== "undefined") {
                            localStorage.setItem("lng", event.latLng.lng());
                            localStorage.setItem("lat", event.latLng.lat());
                            localStorage.setItem("formatted_address", results[0].formatted_address);
                            var enAddress = results[0].address_components;
                            for (ac = 0; ac < enAddress.length; ac++) {
                                if (enAddress[ac].types[0] == "administrative_area_level_2") {
                                    localStorage.setItem("city", enAddress[ac].long_name);
                                }
                            }
                        }

                    } else {
                    }
                } else {
                }
            });
        });
    }

    jQuery('#mapsubmit').click(function(e){
        e.preventDefault();
        if(localStorage.getItem("lng") && localStorage.getItem("lat")){
            var redirecturl="<?php echo site_url('/customerlogin/newaddressform/'.$current_user_id.'');?>";
            window.location.href = redirecturl;
        }else{
            alert("<?php echo get_msg('Please_select_location_from_map'); ?>");
        }

    });
</script>