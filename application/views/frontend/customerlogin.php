<div id="page">
    <div class="app catalog navOpen chaldal-theme">
        <div class="mui">
            <div class="authDialogs"></div>
        </div>
        <span >
        </span>
        <?php $this->load->view( '/frontend/partials/topheader'); ?>
        <?php //$this->load->view('/frontend/partials/sidebarcartdetails'); ?>
        <?php //$this->load->view('/frontend/partials/headerwrapper'); ?>
        <div class="everythingElseWrapper">
            <section class="bodyTable">
                <div>
                    <div class="mainContainer">
                        <div class="login show" id="login-panel"  style="padding-right: 5px; display: block;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h5 class="modal-title " id="exampleModalLabel">
                                            <div id="login-lbl"><strong><?php echo get_msg('Log_In') ?></strong></div>
                                        </h5>
                                    </div>
                                    <div class="otp-model" id="otp-lbl" style="display: none">
                                        <input type="hidden" name="loginredirect" id="loginredirect" value="<?=$redirect_url?>">
                                        <span id="otp-text">
                                    <?php echo get_msg('You_just_received_phone_message_containing_OTP_Code_on') ?>
                                </span>
                                        <span id="otp-number">xxxxxxx</span>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div id="recptcha-container"></div>
                                            <div id="login-fields">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="<?php echo get_msg('YourName') ?>" id="userName">
                                                </div>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="<?php echo get_msg('MobileNumber') ?>" id="mobileNumber" maxlength="10" onkeypress="return isNumber(event)">
                                                </div>
                                            </div>
                                            <div id="otp-fields" style="display: none">
                                                <div class="input-group">
                                                    <input type="number" class="form-control" placeholder="<?php echo get_msg('OTP') ?>" id="otpId" maxlength="6" onkeypress="return isNumber(event)">
                                                </div>
                                            </div>
                                            <div class="input-group">
                                                <button type="button" id="loginbtn" class="btn btn-success btn-block"><?php echo get_msg('Submit') ?></button>
                                                <button type="button" id="verifybtn" class="btn btn-success btn-block" style="display: none"><?php echo get_msg('Verify') ?></button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    </div>


   
<style type="text/css">
    .modal-header, #login-lbl{
       font-size: 40px;
       padding: 5px 0;
       color: #222;
       font-weight: 300;
       margin-bottom: 0px;
       display: block;

}
    .otp-model,#otp-lbl,#otp-text,#otp-number{
        font-size: 15px;
        padding: 5px 0;
        color: #222;
        font-weight: 300;
        text-align: center !important;
        margin-bottom: 0px;
        display: block;

   }
.page-banner-home .banner-slogan, .page-banner-center {
    text-align: center;
    position: relative;
    z-index: 100;
}
.page-banner-2 *, .page-banner-3 *, .page-banner-freelancers * {
    color: #15355a !important;
}
h1.lg-text {
    font-size: 50px;
    font-weight: bold;
    margin-top: 130px;
}
.areas {
    border: 1px solid #fff;
    display: inline-block;
    padding: 20px 40px !important;
    border-radius: 150px;
    font-size: 20px;
}
.page-section {
    padding-top: 48px;
    padding-bottom: 55px;
    border-bottom: 1px solid #edeff2;
}
.page-section {
    position: relative;
}
.text-center {
    text-align: center;
}
h1, .h1 {
    margin-top: 38px;
    margin-bottom: 38px;
    font-size: 33px;
    line-height: 46px;
}
.section-title {
    margin-left: auto;
    margin-right: auto;
}
.section-desc {
    margin-top: -4px;
    margin-left: auto;
    margin-right: auto;
    font-size: 16px;
    line-height: 28px;
}
ul.supported-areas-list {
    padding: 0;
    margin: 0;
    list-style: none;
}
ul.supported-areas-list li {
    width: 20%;
    display: inline-block;
}
ul.supported-areas-list li a {
    font-size: 19px;
    line-height: 45px;
    color: #363b40;
    cursor: pointer;
}
</style>