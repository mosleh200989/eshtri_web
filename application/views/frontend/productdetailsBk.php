<?php $this->load->view( '/frontend/partials/sidebarcartsummery'); ?>

<!-- Modal -->

<!-- Button trigger modal -->

<!-- Modal -->
<?php $this->load->view( '/frontend/partials/sidebarcartdetails'); ?>

<div id="wrapper">
    <?php $this->load->view( '/frontend/partials/shoptopheader'); ?>

    <div class="row">
        <div class="col-md-1">
            <div class="bs">
                <?php $this->load->view( '/frontend/partials/productcategories'); ?>
                <!--  <div class="shopUiBanner">
                      <img src="img/mainBanner1.jpg" class="img-fluid shopban">
                      <div class="shopProfile">
                          <img src="img/shopBannerLogo.jpg" class="img-fluid">
                      </div>
                  </div>-->
                <div class="clear"></div>
            </div>
        </div>
        <div class="col-md-11">
            <!---------------shop Body------------------>
            <?php $imginfo = $this->Image->get_all_by(array('img_type' => 'product', 'img_parent_id' => $product->id))->result(); ?>
            <div class="productDetail">
                <div class="container">
                    <div class="row">
                        <div class="pimg">

                            <div class="pimgTp2">
                                <div id="first" class="firsts">
                                    <img src="https://eshtri.net/uploads/<?php echo $product->thumbnail; ?>"
                                         class="img-fluid ">
                                </div>
                                <div id="second" class="seconds">
                                    <img src="img/shopproduct1.png" class="img-fluid">
                                    <img role="presentation" alt=""
                                         src="https://eshtri.net/uploads/<?php echo $product->thumbnail; ?>"
                                         class="zoomImg">
                                </div>
                                <div id="third" class="thirds">
                                    <img src="img/shopproduct2.png" class="img-fluid "><img role="presentation" alt=""
                                                                                            src="https://eshtri.net/uploads/<?php echo $product->thumbnail; ?>"
                                                                                            class="zoomImg">
                                </div>
                                <div id="fourth" class="fourths" style="position: relative; overflow: hidden;">
                                    <img src="img/shopproduct3.png" class="img-fluid "><img role="presentation" alt=""
                                                                                            src="https://eshtri.net/uploads/<?php echo $product->thumbnail; ?>"
                                                                                            class="zoomImg">
                                </div>
                            </div>
                            <div class="pimgBt mt-2">
                                <div class="row">
                                    <?php foreach ($imginfo as $sinleimg): ?>
                                        <div class="productInner">
                                            <img src="https://eshtri.net/uploads/<?php echo $sinleimg->img_path; ?>"
                                                 class="img-fluid b1" onclick="myFunction()">
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>

                        <div class="ptext">
                            <div class="productDetails">
                                <h2><?php echo getCaption($product->name, $product->name_alt); ?></h2>
                                <h4><?php echo get_msg('SAR')?> <?php echo addvatandcomission($product->unit_price, $product->id, $product->commission_plan); ?></h4>
                                <!--                                <h5><img  src="https://eshtri.net/uploads/-->
                                <?php //echo $product->thumbnail; ?><!--" class="img-fluid">-->
                                <?php
                                if (!empty($product->is_available) && count($product->is_available == 1) > 0): ?>
                                    <span><?php echo get_msg('In_Stock')?></span>
                                <?php endif; ?>


                                </h5>


                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="input-group  bootstrap-touchspin bootstrap-touchspin-injected">
                                            <input type="number" name="" id="" min="1" class="form-control input-number"
                                                   value="1">
                                        </div>
                                    </div>

                                    <div class="col-lg-9">
                                        <div class="color_chart">
                                            <div class="square" style="background-color: rgb(255, 16, 222);"></div>
                                            <div class="square" style="background-color: rgb(116, 191, 85);"></div>
                                            <div class="square" style="background-color: rgb(116, 191, 105);"></div>
                                            <div class="square" style="background-color: rgb(116, 191, 85);"></div>
                                            <div class="square" style="background-color: rgb(116, 191, 185);"></div>

                                        </div>
                                    </div>
                                </div>


                                <p class="text-justify">
                                <h4><?php echo $product->description; ?></h4>
                                <!--                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,-->
                                </p>
                                <div class="productDetailsbtn">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <?php
                                            if ($product->status==1 && $product->is_available==1): ?>
                                                <a href="#" class="btn-cart" data-check-color="<?php echo $product->is_has_color; ?>" data-check-attribute="<?php echo $product->is_has_attribute; ?>"  data-id="<?php echo $product->id; ?>"><button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;<?php echo get_msg('Add_To_Cart')?></button></a>
                                            <?php else: ?>
                                                <span class="btn btn btn-denger text-center out_of_stock"><?php echo get_msg('out_of_stock')?></span>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                </div>
                                <div class="productDetailssocial">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <a href=""> <i class="bx bxl-facebook-square" id="social-fb"></i></a>
                                            <a href=""><i class="bx bxl-twitter" id="social-tw"></i></a>
                                            <a href=""><i class="bx bxl-linkedin-square" id="social-li"></i></a>
                                            <a href=""><i class="bx bxl-youtube" id="social-yt"></i></a>

                                        </div>

                                    </div>
                                </div>

<!--                                <div class="productDetailssocial">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-lg-12">-->
<!--                                            <a href=""> <i class="bx bxl-facebook-square" id="social-fb"></i></a>-->
<!--                                            <a href=""><i class="bx bxl-twitter" id="social-tw"></i></a>-->
<!--                                            <a href=""><i class="bx bxl-linkedin-square" id="social-li"></i></a>-->
<!--                                            <a href=""><i class="bx bxl-youtube" id="social-yt"></i></a>-->
<!---->
<!--                                        </div>-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->

                            </div>


                        </div>


                    </div>
                </div>

            </div>


            <!------------------shop Body------------------>
            <div class="container-fluid sp">
                <?php
                //$json = file_get_contents('http://eshtri.net/rest/products/related_product_trending/api_key/teamalamathebest/id/' . $product->id . '/shop_id/' . $product->shop_id . '/cat_id/' . $product->cat_id . '/limit/10/offset/0/lang/ar');
               // $obj = json_decode($json);
              //  echo "<pre>";
//                print_r($obj);
              //  echo "</pre>";
                ?>
                <div class="col-lg-12 similarProductsHead">
                    <h2 class=" pt-5 pb-2 spa"><?php echo get_msg('Similar Products')?></h2>
                </div>
                <?php if (!empty($obj) && count($obj) > 0): ?>
                    <div class="col-lg-12 shopproductlistup">
                        <div class="owl-carousel owl-theme owl-loaded owl-drag">
                            <div class="owl-stage-outer">
                                <div class="owl-stage"
                                     style="transform: translate3d(-1865px, 0px, 0px); transition: all 0s ease 0s; width: 5968px;">
                                    <?php foreach ($obj as $similarproducts): ?>
                                    <div class="owl-item cloned" style="width: 363px; margin-right: 10px;">

                                        <div class="item">
                                            <div class="productOne ">
                                                <img src="https://eshtri.net/uploads/<?php echo $similarproducts->thumbnail; ?>" class="img-fluid ">
                                                <div class="caption">
                                                    <h5 class="text-center shopText"><?php echo $similarproducts->name; ?></h5>
                                                    <p class="text-center shopPara"><?php echo get_msg('SAR')?> <?php echo addvatandcomission($similarproducts->unit_price, $similarproducts->id, $similarproducts->commission_plan); ?></p>
                                                    <?php
                                                    if ($similarproducts->status==1 && $similarproducts->is_available==1): ?>
                                                        <a href="#" class="btn-cart" data-check-color="<?php echo $similarproducts->is_has_color; ?>" data-check-attribute="<?php echo $similarproducts->is_has_attribute; ?>"  data-id="<?php echo $similarproducts->id; ?>"><button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;<?php echo get_msg('Add_To_Cart')?></button></a>
                                                    <?php else: ?>
                                                        <span class="btn btn btn-denger text-center out_of_stock"><?php echo get_msg('out_of_stock')?></span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <?php endforeach; ?>

                                </div>
                            </div>
                            <div class="owl-nav">
                                <button type="button" role="presentation" class="owl-prev"><i
                                            class="fa fa-chevron-left"></i></button>
                                <button type="button" role="presentation" class="owl-next"><i
                                            class="fa fa-chevron-right"></i></button>
                            </div>
                            <div class="owl-dots">
                                <button role="button" class="owl-dot active"><span></span></button>
                                <button role="button" class="owl-dot"><span></span></button>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="clear"></div>

            <div class="container p-5 return_button">
                <div class="row">
                    <div class="col-lg-12">
                        <button type="button"
                                class="btn btn-primary d-block mx-auto"><?php echo get_msg('return_home') ?></button>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script>
        function generateCartTable(details){
            $("#carttable tbody").html("");
            $.each(details, function(key,val) {
                if(val.product_photo){
                    var img="uploads/small/"+val.product_photo;
                }else{
                    var img="files/Icon-Small.png";
                }
                var itemSubTotal=parseFloat(val.price) * parseInt(val.qty);
                var itemSubTotal=Number(itemSubTotal).toFixed(2);
                var rowdata='<tr id="tr'+val.id+'"><td><img src="<?php echo site_url('/');?>'+img+'" width="70" height="70"></td>';
                rowdata+='<td><h6>'+val.product_name+'</h6><div style="background-color:; width: 20px; height: 20px; margin-top: -20px; margin-left: 50px;"></div><input type="hidden" class="id" name="idDtls['+key+']" value="'+val.id+'">';
                rowdata+='<input type="hidden" min="0" step="any" class="originalPrice form-control" name="originalPriceDtls['+key+']" value="'+val.price+'"><span><strong><?php echo get_msg('price'); ?></strong> <span id="price'+val.id+'">'+val.price+'</span><?php echo get_msg('SAR'); ?></span><br>';
                rowdata+='<div class="input-group item-quantity"><span><strong><?php echo get_msg('Prd_qty'); ?></strong> </span><input data-id="'+val.id+'" name="qtyDtls['+key+']" value="'+val.qty+'" type="number" readonly="" class="form-control cartqty" min="1" max="1593">';
                rowdata+='<span class="input-group-btn "><button type="button" value="quantity" class="quantity-right-plus btn qtypluscart" data-type="plus" data-field=""><i class="fa fa-plus" aria-hidden="true"></i>';
                rowdata+='</button><button type="button" value="quantity" class="quantity-left-minus btn qtyminuscart" data-type="minus" data-field=""><i class="fa fa-minus" aria-hidden="true"></i></button>';
                rowdata+='</span></div><span><strong><?php echo get_msg('sub_total'); ?> :  </strong> <span id="subtotal'+val.id+'">'+itemSubTotal+'</span></span><br><input type="hidden" class="stock form-control" name="stockDtls['+key+']" value="'+val.stock_status+'">';
                rowdata+='</td><td><a herf="#" class="btn-delete btn btn-danger" id="'+val.id+'"><i style="font-size: 18px;" class="fa fa-trash-o"></i> <?php echo get_msg('btn_delete'); ?> </a></td></tr>';
                if(key==0){
                    $('#carttable tbody').append(rowdata);
                }else{
                    $('#carttable tbody tr:last').after(rowdata);
                }
            });
            runAfterJQ();
        }
        function addbtn() {
            $(document.body).on("click", "a.btn-cart", function (e) {
                var obj = {};
                obj["product_id"] = $(this).attr('data-id');
                var attribute = $(this).attr('data-check-attribute');
                var color = $(this).attr('data-check-color');
                // if (attribute == 1 || color == 1) {
                //     $("#productdetsil").modal('toggle');
                // }else{
                if(obj["product_id"]){
                    var url = "<?php echo site_url('shopproducts/addtocart');?>";
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: {postdata: obj},
                        success: function (data) {
                            if (data.isError == false) {
                                confirmNormalMessages(data.isError, data.message, false);
                                // $("#productdetsil").modal('toggle');
                                // if(data.orderdata.payment_url){
                                //     window.location.href = data.orderdata.payment_url;
                                // }
                                var sub_total_amount=data.data.sub_total_amount;
                                var sub_total_amount=Number(sub_total_amount).toFixed(2);

                                var tax_amount=data.data.tax_amount;
                                var tax_amount=Number(tax_amount).toFixed(2);

                                var sub_total_with_tax=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount);
                                var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

                                var total_balance_amount=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount);
                                var total_balance_amount=Number(total_balance_amount).toFixed(2);

                                $('#sub_total').html(sub_total_amount);
                                $('#total_tax').html(tax_amount);
                                $('#sub_total_with_tax').html(sub_total_with_tax);
                                $('#total_balance_amount').html(total_balance_amount);
                                $('#mycartbalance').html(total_balance_amount);
                                $('#finaltotal').html(total_balance_amount);
                                $('#mycartcount').html("<?php echo get_msg('My_Cart'); ?>("+data.data.total_item_count+")");
                                $('#totalqty').html(data.data.total_item_count);

                                generateCartTable(data.data.details);
                            } else {
                                alert(data.message);
                            }
                        },
                        failure: function (data) {
                        }
                    })
                }
                // }
                e.preventDefault();
            });
        }

        function runAfterJQ() {
            $(document).ready(function(){

                // Delete Trigger
                $('.btn-delete').click(function(){
                    // get id and links
                    var id = $(this).attr('id');

                    jQuery.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {order_id: "<?=$transaction->id?>", order_details_id: id},
                        url: "<?php echo site_url('/shopproducts/plusminusqtycart');?>",
                        success: function (data, textStatus) {
                            if (data.isError == false) {

                                $('#tr'+id).remove();
                                var sub_total_amount=data.data.sub_total_amount;
                                var sub_total_amount=Number(sub_total_amount).toFixed(2);

                                var tax_amount=data.data.tax_amount;
                                var tax_amount=Number(tax_amount).toFixed(2);

                                var sub_total_with_tax=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount);
                                var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

                                var total_balance_amount=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount);
                                var total_balance_amount=Number(total_balance_amount).toFixed(2);

                                $('#sub_total').html(sub_total_amount);
                                $('#total_tax').html(tax_amount);
                                $('#sub_total_with_tax').html(sub_total_with_tax);
                                $('#total_balance_amount').html(total_balance_amount);
                                $('#mycartbalance').html(total_balance_amount);
                                $('#finaltotal').html(total_balance_amount);

                                $('#mycartcount').html("<?php echo get_msg('My_Cart'); ?>("+data.data.total_item_count+")");
                                $('#totalqty').html(data.data.total_item_count);

                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            // serverErrorToast(errorThrown);
                        }
                    });

                });

                jQuery(".qtyminuscart").click(function(e) {

                    // Stop acting like a button
                    e.preventDefault();

                    // Get the field name
                    fieldName = jQuery(this).attr('field');

                    // Get its current value
                    var currentVal = parseInt(jQuery(this).parents('span').prev('.cartqty').val());
                    var minimumVal =  jQuery(this).parents('span').prev('.cartqty').attr('min');
                    // If it isn't undefined or its greater than 0
                    var qty = minimumVal;
                    if (!isNaN(currentVal) && currentVal > minimumVal) {
                        // Decrement one
                        qty = currentVal - 1;
                        jQuery(this).parents('span').prev('.cartqty').val(qty);
                    } else {
                        // Otherwise put a 0 there
                        jQuery(this).parents('span').prev('.cartqty').val(minimumVal);

                    }
                    var id = jQuery(this).parents('span').prev('.cartqty').attr('data-id');
                    if(qty>0) {
                        jQuery.ajax({
                            type: 'POST',
                            dataType: "json",
                            data: {order_id: "<?=$transaction->id?>", qty: qty, order_details_id: id, increaseQtyData: true},
                            url: "<?php echo site_url('/shopproducts/plusminusqtycart');?>",
                            success: function (data, textStatus) {
                                if (data.isError == false) {

                                    var itemprice=$('#price'+id).html();
                                    var subtotal=parseFloat(itemprice) * parseInt(qty);
                                    var subtotal=Number(subtotal).toFixed(2);

                                    $('#subtotal'+id).html(subtotal);;

                                    var sub_total_amount=data.data.sub_total_amount;
                                    var sub_total_amount=Number(sub_total_amount).toFixed(2);

                                    var tax_amount=data.data.tax_amount;
                                    var tax_amount=Number(tax_amount).toFixed(2);

                                    var sub_total_with_tax=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount);
                                    var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

                                    var total_balance_amount=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount);
                                    var total_balance_amount=Number(total_balance_amount).toFixed(2);

                                    $('#sub_total').html(sub_total_amount);
                                    $('#total_tax').html(tax_amount);
                                    $('#sub_total_with_tax').html(sub_total_with_tax);
                                    $('#total_balance_amount').html(total_balance_amount);
                                    $('#mycartbalance').html(total_balance_amount);
                                    $('#finaltotal').html(total_balance_amount);
                                    $('#mycartcount').html("<?php echo get_msg('My_Cart'); ?>("+data.data.total_item_count+")");
                                    $('#totalqty').html(data.data.total_item_count);
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                // serverErrorToast(errorThrown);
                            }
                        });
                    }

                });

                jQuery('.qtypluscart').click(function(e){
                    e.preventDefault();
                    // Get the field name
                    //fieldName = jQuery(this).attr('field');
                    // Get its current value
                    var currentVal = parseInt(jQuery(this).parents('span').prev('.cartqty').val());
                    var maximumVal =  jQuery(this).parents('span').prev('.cartqty').attr('max');

                    //alert(maximum);
                    // If is not undefined
                    if (!isNaN(currentVal)) {
                        if(maximumVal!=0){
                            if(currentVal < maximumVal ){
                                // Increment
                                var qty=currentVal + 1;
                                jQuery(this).parents('span').prev('.cartqty').val(qty);
                                var id = jQuery(this).parents('span').prev('.cartqty').attr('data-id');
                                if(qty>0) {
                                    jQuery.ajax({
                                        type: 'POST',
                                        dataType: "json",
                                        data: {order_id: "<?=$transaction->id?>", qty: qty, order_details_id: id, increaseQtyData: true},
                                        url: "<?php echo site_url('/shopproducts/plusminusqtycart');?>",
                                        success: function (data, textStatus) {
                                            if (data.isError == false) {
                                                var itemprice=$('#price'+id).html();
                                                var subtotal=parseFloat(itemprice) * parseInt(qty);
                                                var subtotal=Number(subtotal).toFixed(2);

                                                $('#subtotal'+id).html(subtotal);

                                                var sub_total_amount=data.data.sub_total_amount;
                                                var sub_total_amount=Number(sub_total_amount).toFixed(2);

                                                var tax_amount=data.data.tax_amount;
                                                var tax_amount=Number(tax_amount).toFixed(2);

                                                var sub_total_with_tax=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount);
                                                var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

                                                var total_balance_amount=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount);
                                                var total_balance_amount=Number(total_balance_amount).toFixed(2);

                                                $('#sub_total').html(sub_total_amount);
                                                $('#total_tax').html(tax_amount);
                                                $('#sub_total_with_tax').html(sub_total_with_tax);
                                                $('#total_balance_amount').html(total_balance_amount);
                                                $('#mycartbalance').html(total_balance_amount);
                                                $('#finaltotal').html(total_balance_amount);
                                                $('#mycartcount').html("<?php echo get_msg('My_Cart'); ?>("+data.data.total_item_count+")");
                                                $('#totalqty').html(data.data.total_item_count);
                                            }
                                        },
                                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                                            // serverErrorToast(errorThrown);
                                        }
                                    });
                                }
                            }
                        }

                    } else {
                        // Otherwise put a 0 there
                        jQuery(this).prev('.cartqty').val(0);
                    }
                });

            });
        }
        addbtn();
        runAfterJQ();
    </script>


    <style>
        * {
            box-sizing: border-box;
        }
      .zoom {
            display: none;
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            overflow: hidden;
            pointer-events: none;
            background: #fff;
            border: 1px solid #ccc;
        }
        .overlay {
            position: absolute;
            bottom: 60px;
            background: rgb(0, 0, 0);
            background: rgba(0, 0, 0, 0.5); /* Black see-through */
            color: #fff;
            width: 100%;
            height: 100%;
            transition: .5s ease;
            opacity: 0;
            color: white;
            font-size: 20px;
            padding: 55px;
            text-align: center;
            /*top: 0;*/
            /*left: 0;*/
            border: 1px solid #CCC;


        }

        .item:hover .overlay {
            opacity: 1;
        }

        /*    .disabled-link{
                cursor: default;
                pointer-events: none;
                text-decoration: none;
                color: grey;
            }*/
        a:hover {
            color: #0056b3;
            text-decoration: none;
        }

        .btnShowDetails {
            display: block;
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            height: 25px;
            background: #fff;
            text-align: center;
            border: 0;
            width: 100%;
            color: #5e5e5e;
            font-weight: 700;
            font-size: 12px;
            padding: 3px;
            text-decoration: none;
            line-height: 20px;
            text-underline: none;
        }

        .overlay.text p.addText {
            height: 100px;
            width: 125px;
            margin: 30px auto 0;
            color: #fff;
            font-size: 22px;
            line-height: 34px;
        }
        .productDetail{
            margin-top: 50px;
        }
        #aaModal {
            margin-top: 67px;
        }
        /* div.item .productOne .overlay.text {
             display: none;



             padding-top: 0;

             background-color: rgba(40,40,40,.75);


             border: 1px solid #CCC;
             -moz-box-sizing: border-box;
             -webkit-box-sizing: border-box;
             box-sizing: border-box;
             -moz-user-select: none;
             -ms-user-select: none;
             -webkit-user-select: none;
             user-select: none;
             padding-bottom: 55px;
         }
         div.item .productOne .overlay {
             position: absolute;
             overflow: hidden;
             color: #fff;
             -moz-box-sizing: border-box;
             -webkit-box-sizing: border-box;
             box-sizing: border-box;
             font-size: 14px;
             border-radius: 4px;
         }*/
    </style>
</div>
<!-------wrapper------->