<?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
<html lang="ar">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="ar" data-lang="ar">
            <?php else: ?>
<html lang="en">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="en" data-lang="en">
            <?php endif ?>
<?php $this->load->view('front/includes/header-menu'); ?>
<div class="page-content">

<div class="wrapper">
        <div class="page-banner page-banner-2 page-banner-home green-top-header">
            <div class="banner-slogan">
                <div class="container" id="z-titles">

                    <div class="termsprivacy-title">
                        <?=lang('Pprivacy')?>
                    </div>

                </div>
            </div>
        </div>


        <div class="page-section" style="padding-bottom: 0">
            <div class="container">
                <p><?=lang('PSprivacy')?>
                </p>
                <section>
                    <h2><?=lang('PSafe')?></h2>
                    <p>
					<?=lang('PsSafe')?>
                    </p>
                </section>
                <section>
                    <h2><?=lang('Ppaymethod')?></h2>
                    <p>
                      <?=lang('PSpaymethod')?>
                    </p>
                </section>

                <section>

                    <h2><?=lang('PShare')?></h2>
                    <p>
                      <?=lang('PsShare')?> 
                    </p>
                </section>
                <section>
                    <h2>
                        <?=lang('Pprem')?> </h2>
                    <p>
                      <?=lang('Psprem')?>
                    </p>
                </section>
                <section>
                    <h2><?=lang('POwner')?></h2>
                    <p>
                      <?=lang('PsOwner')?>
                    </p>
                </section>
                <section>
                    <h2><?=lang('PHow')?></h2>
                    <p>
                       <?=lang('PsHow')?>
                    </p>
                    <p class="last-update"> <?=lang('PUpDate')?> </p>
                </section>

            </div>
        </div>

        <script>
        </script>

    </div>


    </div>

    <!-- footer -->
    <?php $this->load->view('front/includes/footer'); ?>
</body></html>
