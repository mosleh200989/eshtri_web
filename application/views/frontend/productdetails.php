
<div id="page">
    <div class="app catalog navOpen chaldal-theme">
        <div class="mui">
            <div class="authDialogs">
            </div>
        </div>
        <?php $this->load->view('/frontend/partials/sidebarcartdetails'); ?>
        <?php $this->load->view('/frontend/partials/headerwrapper'); ?>
        <div class="primary_shopping_bottom_btn shopping_bottom_btn"><button class="place_order_btn"><?php echo get_msg('Place_Order'); ?></button>
        </div>
        <div class="everythingElseWrapper">
            <section class="bodyTable">
                <div>
                    <div class="mainContainer">
                        <div class="shopproductdetails" id="middlecontent">
                            <?php $imginfo = $this->Image->get_all_by(array('img_type' => 'product', 'img_parent_id' => $product->id))->result(); ?>
                            <!--                product details start-->
                            <div class="productDetail">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="product-details-slider">
                                                <div id="slider" class="flexslider pimgTp">
                                                    <ul class="slides">
                                                        <?php foreach ($imginfo as $sinleimg): ?>
                                                            <li>
                                                                <img src="https://eshtri.net/uploads/<?php echo $sinleimg->img_path; ?>" class="zoomImg" />
                                                            </li>
                                                        <?php endforeach; ?>
                                                        <!-- items mirrored twice, total of 12 -->
                                                    </ul>
                                                </div>
                                                <?php if(count($imginfo)>1){?>
                                                <div id="carousel" class="flexslider">
                                                    <ul class="slides">
                                                        <?php foreach ($imginfo as $sinleimg): ?>
                                                            <li>
                                                                <img src="https://eshtri.net/uploads/<?php echo $sinleimg->img_path; ?>" />
                                                            </li>
                                                        <?php endforeach; ?>
                                                        <!-- items mirrored twice, total of 12 -->
                                                    </ul>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="productDetails">
                                                <section class="right">
                                                    <div class="quantityContainer">
                                                <h2><?php echo getCaption($product->name, $product->name_alt); ?></h2>
                                                <?php if($product->is_discount==1){?>
                                                <div class="discountedPriceSection">
                                                    <div class="discountedPrice">
                                                        <span itemprop="priceCurrency" content="SAR" ><?php echo get_msg('SAR')?></span>
                                                        <span itemprop="price">
                                                            <span><?php echo addvatandcomission($product->unit_price , $product->id, $product->commission_plan);?></span>
                                                        </span>
                                                    </div>
                                                    <div class="fullPrice">
                                                        <span><?php echo get_msg('SAR')?></span>
                                                        <span><?php echo addvatandcomission($product->original_price , $product->id, $product->commission_plan);?></span>
                                                    </div>
                                                    <div class="discount">
                                                        <a class="discount">
                                                           <?php  $discount = $this->Discount->get_one_by( array("id"=>$product->discount_id));  ?>
                                                            <span ><?=$discount->percent * 100?>% <?php echo get_msg('OFF')?></span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <?php }else{ ?>
                                                <h4><?php echo get_msg('SAR')?> <?php echo addvatandcomission($product->unit_price, $product->id, $product->commission_plan); ?></h4>
                                                <?php } ?>
                                                <h5><?php
                                                    if (!empty($product->is_available) && count($product->is_available == 1) > 0): ?>
                                                        <span><?php echo get_msg('In_Stock')?></span>
                                                    <?php endif; ?></h5>
<!--                                                <h5>--><?php //echo get_msg('available_colors')?><!--</h5>-->
<!--                                                <div class="row">-->
<!--                                                    <div class="col-lg-9">-->
<!--                                                        <div class="color_chart">-->
<!--                                                            <div class="square" style="background-color: rgb(255, 16, 222);"></div>-->
<!--                                                            <div class="square" style="background-color: rgb(116, 191, 85);"></div>-->
<!--                                                            <div class="square" style="background-color: rgb(116, 191, 105);"></div>-->
<!--                                                            <div class="square" style="background-color: rgb(116, 191, 85);"></div>-->
<!--                                                            <div class="square" style="background-color: rgb(116, 191, 185);"></div>-->
<!--                                                        </div>-->
<!--                                                    </div>-->
<!--                                                </div>-->
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="input-group  bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input type="number" name="productqtydetails" id="productqtydetails"  min="1"
                                                                   class="form-control input-number" value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="text-justify">
                                                    <?php echo $product->description; ?>
                                                </p>
                                                <div class="productDetailsbtn">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <?php
                                                            if ($product->status=='1' && $product->is_available=='1'): ?>
                                                                <a href="#" class="addButtonWrapper border-radius-small btn-cart" data-check-color="<?php echo $product->is_has_color; ?>" data-check-attribute="<?php echo $product->is_has_attribute; ?>"  data-id="<?php echo $product->id; ?>"><button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;<?php echo get_msg('Add_To_Cart')?></button></a>
                                                            <?php else: ?>
                                                                <span class="btn btn btn-denger text-center out_of_stock"><?php echo get_msg('out_of_stock')?></span>
                                                            <?php endif; ?>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="productDetailssocial">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <a href=""> <i class='bx bxl-facebook-square' id="social-fb"></i></a>
                                                            <a href=""><i class='bx bxl-twitter' id="social-tw"></i></a>
                                                            <a href=""><i class='bx bxl-linkedin-square' id="social-li"></i></a>
                                                            <a href=""><i class='bx bxl-youtube' id="social-yt"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="detailsec shadow-sm">
                                <div class="container-fluid">
                                    <ul class="nav pills pills-secondary shadow-sm">
                                        <li class="nav-item active">
                                            <a class="nav-link active" data-toggle="tab" href="#panel11" role="tab"><?php echo get_msg('Product_Information')?></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#panel12" role="tab"><?php echo get_msg('Product_Details')?></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#panel13" role="tab"><?php echo get_msg('Customer_Review')?></a>
                                        </li>
                                    </ul>
                                    <!-- Tab panels -->
                                    <div class="tab-content pt-0">
                                        <!--Panel 1-->
                                        <div class="tab-pane fade in show active" id="panel11" role="tabpanel">
                                            <br>
                                            <p><?php echo $product->description; ?></p>
                                        </div>
                                        <!--/.Panel 1-->

                                        <!--Panel 2-->
                                        <div class="tab-pane fade" id="panel12" role="tabpanel">
                                            <br>
                                        </div>
                                        <!--/.Panel 2-->

                                        <!--Panel 3-->
                                        <div class="tab-pane fade" id="panel13" role="tabpanel">
                                            <br>
                                        </div>
                                        <!--/.Panel 3-->

                                        <!--Panel 4-->
                                        <div class="tab-pane fade" id="panel14" role="tabpanel">
                                            <br>
                                        </div>
                                        <!--/.Panel 4-->
                                        <div class="tab-pane fade" id="panel15" role="tabpanel">
                                            <br>
                                        </div>
                                        <!--/.Panel 5-->

                                    </div>
                                </div>
                            </div>


                            <!----similar products------>
<!--                            <div class="container-fluid sp">-->
<!--                                <div class="col-lg-12 similarProductsHead">-->
<!--                                    <h2 class=" pt-5 pb-2 spa">--><?php //echo get_msg('Similar Products')?><!--</h2>-->
<!--                                </div>-->
<!---->
<!---->
<!---->
<!--                                <div class="col-lg-12 shopproductlistup">-->
<!--                                    <div class="owl-carousel owl-theme">-->
<!--                                        <div class="item">-->
<!--                                            <div class="productOne ">-->
<!--                                                <img src="https://eshtri.net/uploads/--><?php //echo $product->thumbnail; ?><!--" class="img-fluid "/>-->
<!--                                                <div class="caption">-->
<!--                                                    <h5 class="text-center shopText">Puck Sterilized<br> Cream</h5>-->
<!--                                                    <h6 class="text-center shopText1 ">170g</h6>-->
<!--                                                    <p class="text-center shopPara">BTD - 130.00</p>-->
<!--                                                    <button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;Add To Bag</button>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="item">-->
<!--                                            <div class="productOne ">-->
<!--                                                <img src="https://eshtri.net/uploads/--><?php //echo $product->thumbnail; ?><!--" class="img-fluid "/>-->
<!--                                                <div class="caption">-->
<!--                                                    <h5 class="text-center shopText">Puck Sterilized<br> Cream</h5>-->
<!--                                                    <h6 class="text-center shopText1 ">170g</h6>-->
<!--                                                    <p class="text-center shopPara">BTD - 130.00</p>-->
<!--                                                    <button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;Add To Bag</button>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="item">-->
<!--                                            <div class="productOne ">-->
<!--                                                <img src="https://eshtri.net/uploads/--><?php //echo $product->thumbnail; ?><!--" class="img-fluid "/>-->
<!--                                                <div class="caption">-->
<!--                                                    <h5 class="text-center shopText">Puck Sterilized<br> Cream</h5>-->
<!--                                                    <h6 class="text-center shopText1 ">170g</h6>-->
<!--                                                    <p class="text-center shopPara">BTD - 130.00</p>-->
<!--                                                    <button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;Add To Bag</button>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="item">-->
<!--                                            <div class="productOne ">-->
<!--                                                <img src="https://eshtri.net/uploads/--><?php //echo $product->thumbnail; ?><!--" class="img-fluid "/>-->
<!--                                                <div class="caption">-->
<!--                                                    <h5 class="text-center shopText">Puck Sterilized<br> Cream</h5>-->
<!--                                                    <h6 class="text-center shopText1 ">170g</h6>-->
<!--                                                    <p class="text-center shopPara">BTD - 130.00</p>-->
<!--                                                    <button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;Add To Bag</button>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="item">-->
<!--                                            <div class="productOne ">-->
<!--                                                <img src="https://eshtri.net/uploads/--><?php //echo $product->thumbnail; ?><!--" class="img-fluid "/>-->
<!--                                                <div class="caption">-->
<!--                                                    <h5 class="text-center shopText">Puck Sterilized<br> Cream</h5>-->
<!--                                                    <h6 class="text-center shopText1 ">170g</h6>-->
<!--                                                    <p class="text-center shopPara">BTD - 130.00</p>-->
<!--                                                    <button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;Add To Bag</button>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="item">-->
<!--                                            <div class="productOne ">-->
<!--                                                <img src="https://eshtri.net/uploads/--><?php //echo $product->thumbnail; ?><!--" class="img-fluid "/>-->
<!--                                                <div class="caption">-->
<!--                                                    <h5 class="text-center shopText">Puck Sterilized<br> Cream</h5>-->
<!--                                                    <h6 class="text-center shopText1 ">170g</h6>-->
<!--                                                    <p class="text-center shopPara">BTD - 130.00</p>-->
<!--                                                    <button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;Add To Bag</button>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
                            <!--                product details end-->




                        </div>
                    </div>
                </div></section>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });

            // $('#carousel').flexslider({
            //     animation: "slide",
            //     controlNav: false,
            //     animationLoop: false,
            //     slideshow: false,
            //     itemWidth: 210,
            //     itemMargin: 5,
            //     asNavFor: '#slider'
            // });
            //
            // $('#slider').flexslider({
            //     animation: "slide",
            //     controlNav: false,
            //     animationLoop: false,
            //     slideshow: false,
            //     sync: "#carousel"
            // });
        });

    </script>