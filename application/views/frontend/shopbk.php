<?php $this->load->view( '/frontend/partials/sidebarcartsummery'); ?>

<!-- Modal -->

<!-- Button trigger modal -->

<!-- Modal -->
<?php $this->load->view( '/frontend/partials/sidebarcartdetails'); ?>
<div id="wrapper">
        <?php $this->load->view( '/frontend/partials/shoptopheader'); ?>
    <div class="row">
        <div class="col-md-1">

            <div class="bs">
                <?php $this->load->view( '/frontend/partials/productcategories'); ?>
                <div class="clear"></div>
            </div>
        </div>
        <div class="col-md-11">
            <!---------------shop Body------------------>
            <div class="container-fluid shopproductlist" id="middlecontent">
                <div class="row">
                    <?php
                    if (!empty($newproduct) && count($newproduct->result()) > 0): ?>
                        <div class="col-lg-12 shopSectionTexts">
                            <h2 class="shopUIHeading pt-5 pb-5"><?php echo get_msg('New_Product')?></h2>
                        </div>

                        <div class="col-lg-12 shopproductlistup">
                            <div class="owl-carousel owl-theme">
                                <?php foreach ($newproduct->result() as $newproducts): ?>
                                    <div class="item">
                                        <div class="productOne">
                                            <img src="https://eshtri.net/uploads/<?php echo $newproducts->thumbnail; ?>" class="img-fluid" />
                                            <div class="caption">
                                                <h5 class="text-center shopText"><?php echo getCaption($newproducts->name, $newproducts->name_alt);?></h5>
                                                <p class="text-center shopPara"><?php echo get_msg('SAR')?> <?php echo addvatandcomission($newproducts->unit_price, $newproducts->id, $newproducts->commission_plan); ?></p>
                                            </div>
                                            <div class="overlay text">
                                                <p class="addText"><?php echo get_msg('Add_to_Shopping_Cart')?></p>
                                                <span>
                                            <a href="<?php echo base_url('shopproducts/productdetails/'.$newproducts->id); ?>" class="btnShowDetails" id="myBtn">
                                                <span ><?php echo get_msg('Details')?></span>
                                                <span >  &gt;</span></a>
                                            </span></div>
                                            <?php
                                            if ($newproducts->status==1 && $newproducts->is_available==1): ?>
                                                <a href="#" class="btn-cart" data-check-color="<?php echo $newproducts->is_has_color; ?>" data-check-attribute="<?php echo $newproducts->is_has_attribute; ?>"  data-id="<?php echo $newproducts->id; ?>"><button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;<?php echo get_msg('Add_To_Cart')?></button></a>
                                            <?php else: ?>
                                                <span class="btn btn btn-denger text-center out_of_stock"><?php echo get_msg('out_of_stock')?></span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>

                    <?php endif; ?>

                    <?php
                    if (!empty($discountproducts) && count($discountproducts->result()) > 0): ?>
                        <div class="col-lg-12 shopSectionTexts">
                            <h2 class="shopUIHeading pt-5 pb-5"><?php echo get_msg('Discount')?></h2>
                        </div>



                        <div class="col-lg-12 shopproductlistup">
                            <div class="owl-carousel owl-theme">
                                <?php foreach ($discountproducts->result() as $discount): ?>
                                    <div class="item">
                                        <div class="productOne ">

                                            <img src="https://eshtri.net/uploads/<?php echo $discount->thumbnail; ?>" class="img-fluid "/>
                                            <div class="caption">
                                                <h5 class="text-center shopText"><?php echo getCaption($discount->name, $discount->name_alt); ?></h5>
                                                <p class="text-center shopPara"><?php echo get_msg('SAR')?> <?php echo addvatandcomission($discount->unit_price, $discount->id, $discount->commission_plan); ?></p>
                                                <div class="overlay text">
                                                    <p class="addText"><?php echo get_msg('Add_to_Shopping_Cart')?></p>
                                                    <span>
                                            <a href="<?php echo base_url('shopproducts/productdetails/'.$discount->id); ?>" class="btnShowDetails disabled-link">
                                                <span><?php echo get_msg('Details')?></span>
                                                <span>  &gt;</span></a>
                                                 <?php
                                                 if ($discount->status==1 && $discount->is_available==1): ?>
                                                     <a href="#" class="btn-cart" data-check-color="<?php echo $discount->is_has_color; ?>" data-check-attribute="<?php echo $discount->is_has_attribute; ?>"  data-id="<?php echo $discount->id; ?>"><button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;<?php echo get_msg('Add_To_Cart')?></button></a>
                                                 <?php else: ?>
                                                     <span class="btn btn btn-denger text-center out_of_stock"><?php echo get_msg('out_of_stock')?></span>
                                                 <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php
                    if (!empty($popularproduct) && count($popularproduct->result()) > 0): ?>
                        <div class="col-lg-12 shopSectionTexts">
                            <h2 class="shopUIHeading pt-5 pb-5"><?php echo get_msg('Popular')?></h2>
                        </div>
                        <div class="col-lg-12 shopproductlistup">
                            <div class="owl-carousel owl-theme">
                                <?php foreach ($popularproduct->result() as $popularproducts): ?>
                                    <div class="item">
                                        <div class="productOne ">
                                            <img src="https://eshtri.net/uploads/<?php echo $popularproducts->thumbnail; ?>" class="img-fluid "/>
                                            <div class="caption">
                                                <h5 class="text-center shopText"><?php echo getCaption($popularproducts->name, $popularproducts->name_alt); ?></h5>
                                                <p class="text-center shopPara"><?php echo get_msg('SAR')?> <?php echo addvatandcomission($popularproducts->unit_price, $popularproducts->id, $popularproducts->commission_plan); ?></p>
                                                <div class="overlay text">
                                                    <p class="addText"><?php echo get_msg('Add_to_Shopping_Cart')?></p>
                                                    <span>
                                            <a href="<?php echo base_url('shopproducts/productdetails/'.$popularproducts->id); ?>" class="btnShowDetails disabled-link">
                                                <span><?php echo get_msg('Details')?></span>
                                                <span>  &gt;</span></a>
                                           </span></div>
                                                <?php
                                                if ($popularproducts->status==1 && $popularproducts->is_available==1): ?>
                                                <a href="#" class="btn-cart" data-check-color="<?php echo $popularproducts->is_has_color; ?>" data-check-attribute="<?php echo $popularproducts->is_has_attribute; ?>"  data-id="<?php echo $popularproducts->id; ?>"><button type="button" class="btn btn btn-block"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;<?php echo get_msg('Add_To_Cart')?></button></a>
                                                <?php else: ?>
                                                    <span class="btn btn btn-denger text-center out_of_stock"><?php echo get_msg('out_of_stock')?></span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>


                </div>
            </div>
            <!------------------shop Body------------------>
            <div class="clear"></div>


        </div>
    </div>




    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>
<!--            <p>Some text in the Modal..</p>-->
        </div>

    </div>

    <script>
        function generateCartTable(details){
            $("#carttable tbody").html("");
            $.each(details, function(key,val) {
                if(val.product_photo){
                    var img="uploads/small/"+val.product_photo;
                }else{
                    var img="files/Icon-Small.png";
                }
                var itemSubTotal=parseFloat(val.price) * parseInt(val.qty);
                var itemSubTotal=Number(itemSubTotal).toFixed(2);
                var rowdata='<tr id="tr'+val.id+'"><td><img src="<?php echo site_url('/');?>'+img+'" width="70" height="70"></td>';
                rowdata+='<td><h6>'+val.product_name+'</h6><div style="background-color:; width: 20px; height: 20px; margin-top: -20px; margin-left: 50px;"></div><input type="hidden" class="id" name="idDtls['+key+']" value="'+val.id+'">';
                rowdata+='<input type="hidden" min="0" step="any" class="originalPrice form-control" name="originalPriceDtls['+key+']" value="'+val.price+'"><span><strong><?php echo get_msg('price'); ?></strong> <span id="price'+val.id+'">'+val.price+'</span><?php echo get_msg('SAR'); ?></span><br>';
                rowdata+='<div class="input-group item-quantity"><span><strong><?php echo get_msg('Prd_qty'); ?></strong> </span><input data-id="'+val.id+'" name="qtyDtls['+key+']" value="'+val.qty+'" type="number" readonly="" class="form-control cartqty" min="1" max="1593">';
                rowdata+='<span class="input-group-btn "><button type="button" value="quantity" class="quantity-right-plus btn qtypluscart" data-type="plus" data-field=""><i class="fa fa-plus" aria-hidden="true"></i>';
                rowdata+='</button><button type="button" value="quantity" class="quantity-left-minus btn qtyminuscart" data-type="minus" data-field=""><i class="fa fa-minus" aria-hidden="true"></i></button>';
                rowdata+='</span></div><span><strong><?php echo get_msg('sub_total'); ?> :  </strong> <span id="subtotal'+val.id+'">'+itemSubTotal+'</span></span><br><input type="hidden" class="stock form-control" name="stockDtls['+key+']" value="'+val.stock_status+'">';
                rowdata+='</td><td><a herf="#" class="btn-delete btn btn-danger" id="'+val.id+'"><i style="font-size: 18px;" class="fa fa-trash-o"></i> <?php echo get_msg('btn_delete'); ?> </a></td></tr>';
                if(key==0){
                    $('#carttable tbody').append(rowdata);
                }else{
                    $('#carttable tbody tr:last').after(rowdata);
                }
            });
            runAfterJQ();
        }
        function addbtn() {
            $(document.body).on("click", "a.btn-cart", function (e) {
                var obj = {};
                obj["product_id"] = $(this).attr('data-id');
                var attribute = $(this).attr('data-check-attribute');
                var color = $(this).attr('data-check-color');
                // if (attribute == 1 || color == 1) {
                //     $("#productdetsil").modal('toggle');
                // }else{
                if(obj["product_id"]){
                    var url = "<?php echo site_url('shopproducts/addtocart');?>";
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: {postdata: obj},
                        success: function (data) {
                            if (data.isError == false) {
                                confirmNormalMessages(data.isError, data.message, false);
                                // $("#productdetsil").modal('toggle');
                                // if(data.orderdata.payment_url){
                                //     window.location.href = data.orderdata.payment_url;
                                // }
                                var sub_total_amount=data.data.sub_total_amount;
                                var sub_total_amount=Number(sub_total_amount).toFixed(2);

                                var tax_amount=data.data.tax_amount;
                                var tax_amount=Number(tax_amount).toFixed(2);

                                var sub_total_with_tax=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount);
                                var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

                                var total_balance_amount=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount);
                                var total_balance_amount=Number(total_balance_amount).toFixed(2);

                                $('#sub_total').html(sub_total_amount);
                                $('#total_tax').html(tax_amount);
                                $('#sub_total_with_tax').html(sub_total_with_tax);
                                $('#total_balance_amount').html(total_balance_amount);
                                $('#mycartbalance').html(total_balance_amount);
                                $('#finaltotal').html(total_balance_amount);
                                $('#mycartcount').html("<?php echo get_msg('My_Cart'); ?>("+data.data.total_item_count+")");
                                $('#totalqty').html(data.data.total_item_count);

                                generateCartTable(data.data.details);
                            } else {
                                alert(data.message);
                            }
                        },
                        failure: function (data) {
                        }
                    })
                }
                // }
                e.preventDefault();
            });
        }

        function runAfterJQ() {
            $(document).ready(function(){

                // Delete Trigger
                $('.btn-delete').click(function(){
                    // get id and links
                    var id = $(this).attr('id');

                    jQuery.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {order_id: "<?=$transaction->id?>", order_details_id: id},
                        url: "<?php echo site_url('/shopproducts/plusminusqtycart');?>",
                        success: function (data, textStatus) {
                            if (data.isError == false) {

                                $('#tr'+id).remove();
                                var sub_total_amount=data.data.sub_total_amount;
                                var sub_total_amount=Number(sub_total_amount).toFixed(2);

                                var tax_amount=data.data.tax_amount;
                                var tax_amount=Number(tax_amount).toFixed(2);

                                var sub_total_with_tax=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount);
                                var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

                                var total_balance_amount=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount);
                                var total_balance_amount=Number(total_balance_amount).toFixed(2);

                                $('#sub_total').html(sub_total_amount);
                                $('#total_tax').html(tax_amount);
                                $('#sub_total_with_tax').html(sub_total_with_tax);
                                $('#total_balance_amount').html(total_balance_amount);
                                $('#mycartbalance').html(total_balance_amount);
                                $('#finaltotal').html(total_balance_amount);

                                $('#mycartcount').html("<?php echo get_msg('My_Cart'); ?>("+data.data.total_item_count+")");
                                $('#totalqty').html(data.data.total_item_count);

                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            // serverErrorToast(errorThrown);
                        }
                    });

                });

                jQuery(".qtyminuscart").click(function(e) {

                    // Stop acting like a button
                    e.preventDefault();

                    // Get the field name
                    fieldName = jQuery(this).attr('field');

                    // Get its current value
                    var currentVal = parseInt(jQuery(this).parents('span').prev('.cartqty').val());
                    var minimumVal =  jQuery(this).parents('span').prev('.cartqty').attr('min');
                    // If it isn't undefined or its greater than 0
                    var qty = minimumVal;
                    if (!isNaN(currentVal) && currentVal > minimumVal) {
                        // Decrement one
                        qty = currentVal - 1;
                        jQuery(this).parents('span').prev('.cartqty').val(qty);
                    } else {
                        // Otherwise put a 0 there
                        jQuery(this).parents('span').prev('.cartqty').val(minimumVal);

                    }
                    var id = jQuery(this).parents('span').prev('.cartqty').attr('data-id');
                    if(qty>0) {
                        jQuery.ajax({
                            type: 'POST',
                            dataType: "json",
                            data: {order_id: "<?=$transaction->id?>", qty: qty, order_details_id: id, increaseQtyData: true},
                            url: "<?php echo site_url('/shopproducts/plusminusqtycart');?>",
                            success: function (data, textStatus) {
                                if (data.isError == false) {

                                    var itemprice=$('#price'+id).html();
                                    var subtotal=parseFloat(itemprice) * parseInt(qty);
                                    var subtotal=Number(subtotal).toFixed(2);

                                    $('#subtotal'+id).html(subtotal);;

                                    var sub_total_amount=data.data.sub_total_amount;
                                    var sub_total_amount=Number(sub_total_amount).toFixed(2);

                                    var tax_amount=data.data.tax_amount;
                                    var tax_amount=Number(tax_amount).toFixed(2);

                                    var sub_total_with_tax=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount);
                                    var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

                                    var total_balance_amount=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount);
                                    var total_balance_amount=Number(total_balance_amount).toFixed(2);

                                    $('#sub_total').html(sub_total_amount);
                                    $('#total_tax').html(tax_amount);
                                    $('#sub_total_with_tax').html(sub_total_with_tax);
                                    $('#total_balance_amount').html(total_balance_amount);
                                    $('#mycartbalance').html(total_balance_amount);
                                    $('#finaltotal').html(total_balance_amount);
                                    $('#mycartcount').html("<?php echo get_msg('My_Cart'); ?>("+data.data.total_item_count+")");
                                    $('#totalqty').html(data.data.total_item_count);
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                // serverErrorToast(errorThrown);
                            }
                        });
                    }

                });

                jQuery('.qtypluscart').click(function(e){
                    e.preventDefault();
                    // Get the field name
                    //fieldName = jQuery(this).attr('field');
                    // Get its current value
                    var currentVal = parseInt(jQuery(this).parents('span').prev('.cartqty').val());
                    var maximumVal =  jQuery(this).parents('span').prev('.cartqty').attr('max');

                    //alert(maximum);
                    // If is not undefined
                    if (!isNaN(currentVal)) {
                        if(maximumVal!=0){
                            if(currentVal < maximumVal ){
                                // Increment
                                var qty=currentVal + 1;
                                jQuery(this).parents('span').prev('.cartqty').val(qty);
                                var id = jQuery(this).parents('span').prev('.cartqty').attr('data-id');
                                if(qty>0) {
                                    jQuery.ajax({
                                        type: 'POST',
                                        dataType: "json",
                                        data: {order_id: "<?=$transaction->id?>", qty: qty, order_details_id: id, increaseQtyData: true},
                                        url: "<?php echo site_url('/shopproducts/plusminusqtycart');?>",
                                        success: function (data, textStatus) {
                                            if (data.isError == false) {
                                                var itemprice=$('#price'+id).html();
                                                var subtotal=parseFloat(itemprice) * parseInt(qty);
                                                var subtotal=Number(subtotal).toFixed(2);

                                                $('#subtotal'+id).html(subtotal);

                                                var sub_total_amount=data.data.sub_total_amount;
                                                var sub_total_amount=Number(sub_total_amount).toFixed(2);

                                                var tax_amount=data.data.tax_amount;
                                                var tax_amount=Number(tax_amount).toFixed(2);

                                                var sub_total_with_tax=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount);
                                                var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

                                                var total_balance_amount=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount);
                                                var total_balance_amount=Number(total_balance_amount).toFixed(2);

                                                $('#sub_total').html(sub_total_amount);
                                                $('#total_tax').html(tax_amount);
                                                $('#sub_total_with_tax').html(sub_total_with_tax);
                                                $('#total_balance_amount').html(total_balance_amount);
                                                $('#mycartbalance').html(total_balance_amount);
                                                $('#finaltotal').html(total_balance_amount);
                                                $('#mycartcount').html("<?php echo get_msg('My_Cart'); ?>("+data.data.total_item_count+")");
                                                $('#totalqty').html(data.data.total_item_count);
                                            }
                                        },
                                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                                            // serverErrorToast(errorThrown);
                                        }
                                    });
                                }
                            }
                        }

                    } else {
                        // Otherwise put a 0 there
                        jQuery(this).prev('.cartqty').val(0);
                    }
                });

            });
        }
        addbtn();
        runAfterJQ();
    </script>

    <style>
        * {box-sizing: border-box;}

        .overlay {
            position: absolute;
            bottom:60px;
            background: rgb(0, 0, 0);
            background: rgba(0, 0, 0, 0.5); /* Black see-through */
            color: #fff;
            width: 100%;
            height: 100%;
            transition: .5s ease;
            opacity:0;
            color: white;
            font-size: 20px;
            padding: 55px;
            text-align: center;
            /*top: 0;*/
            /*left: 0;*/
            border: 1px solid #CCC;


        }
        .item:hover .overlay {
            opacity: 1;
        }

        a:hover {
            color: #0056b3;
            text-decoration: none;
        }

        .btnShowDetails {
            display: block;
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            height: 25px;
            background: #fff;
            text-align: center;
            border: 0;
            width: 100%;
            color: #5e5e5e;
            font-weight: 700;
            font-size: 12px;
            padding: 3px;
            text-decoration: none;
            line-height: 20px;
            text-underline: none;
        }
        .overlay.text p.addText {
            margin: 30px auto 0;
            color: #fff;
            font-size: 22px;
            line-height: 34px;
        }


        @media screen and (max-width: 1000px) {
            body#register {
                padding: 0px 0;
            }
            .card {
                margin: 0px 0px 0px 0px;
            }
            .col-6 {
                -ms-flex: 0 0 100%;
                flex: 0 0 100%;
                max-width: 100%;
            }
            .table {
                font-size: 10px;
            }
            .thumbnails.payments{}
            .thumbnails.payments li{}
        }
        .table td, .table th {
            vertical-align: middle;
        }
        .input-group {
            position: relative;
            display: flex;
            flex-wrap: wrap;
            align-items: stretch;
            width: 100%;
        }
        .item-quantity input {
            text-align: center;
            padding-left: 5px;
            padding-right: 5px;
            box-shadow: none;
            font-size: 0.875rem;
            font-weight: 800;
            height: 100%;
        }
        .item-quantity {
            width: 130px;
            height: 44px;
        }
        .btn-delete {
            font-size: 12px;
        }
        .input-group > .form-control, .input-group > .form-control-plaintext, .input-group > .custom-select, .input-group > .custom-file {
            position: relative;
            flex: 1 1 0%;
            min-width: 0;
            margin-bottom: 0;
        }
        .input-group .form-control {
            height: calc(2.25rem + 6px);
        }
        .input-group > .form-control:not(:last-child), .input-group > .custom-select:not(:last-child) {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: #e9ecef;
            opacity: 1;
        }
        .item-quantity .input-group-btn {
            float: left;
            width: 30px;
        }
        .item-quantity .input-group-btn button {
            display: block;
            height: 21px;
            padding: 0 10px;
            font-size: 10px;
            border: 1px solid #dee2e6;
        }
        .item-quantity .input-group-btn button {
            display: block;
            height: 21px;
            padding: 0 10px;
            font-size: 10px;
            border: 1px solid #dee2e6;
            background-color: #fff;
            color: #000;
        }
        .input-group-btn:not(:first-child)>.btn, .input-group-btn:not(:first-child)>.btn-group {
            margin-top: 0px;
            border-radius: 0;
            margin: 0px;
        }
        .input-group-btn:not(:first-child)>.btn, .input-group-btn:not(:first-child)>.btn-group {
            margin-top: 0px;
            border-radius: 0;
        }
        .input-group-btn:not(:first-child)>.btn-group:first-child, .input-group-btn:not(:first-child)>.btn:first-child {
            margin-left: 0px;
        }

        element.style {
        }
        .item-quantity .input-group-btn {
            float: left;
            width: 30px;
        }
        .input-group-btn {
            position: relative;
            -ms-flex-align: stretch;
            align-items: stretch;
            font-size: 0;
            white-space: nowrap;
        }
        .input-group-addon, .input-group-btn {
            white-space: nowrap;
        }
        .input-group-addon, .input-group-btn {
            display: block;
        }
        .card .btn span{
            padding: 0px;
            margin: 0px 3px;
        }
        .payments{
            list-style: none;
        }
        .payments .card{
            margin: 3px;
            text-align: center;
            padding: 10px;
            height: 75px;
            vertical-align: middle;
            cursor: pointer;
        }
        .payments .card.active{
            background-color: #5eb824;
            color: #ffffff;
        }
        .payments .card h3{
            font-size: 15px;
        }
        .payments .card .caption{

        }
        .modal-body form button{
            background-color: #ffffff !important;
        }

        .out_of_stock{
            background-color: transparent;
            border: 0.0625rem solid #dd285d;
            color: #dd285d;
            font-weight: 600;
            border-radius: 0.1875rem;
            width: 100%;
            background: #d5d6d1;
        }
    </style>
</div>
<!-------wrapper------->