
<div id="page">
    <div class="app catalog navOpen chaldal-theme">
        <div class="mui">
            <div class="authDialogs">
            </div>
        </div>
        <?php $this->load->view('/frontend/partials/sidebarcartdetails'); ?>
        <?php $this->load->view('/frontend/partials/headerwrapper'); ?>
        <div class="everythingElseWrapper">
            <div>
            </div>
            <section class="bodyTable">
                <div >
                    <div class="catalogBrowser">
                        <div class="loaded">
                            <div>
                                <section class="bodyWrapper">
<!--                                    <div class="categoryHeader"><ol class="breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList"><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="crumb"><a href="/grocery" itemscope="" itemtype="http://schema.org/Thing" itemprop="item">-->
<!--	<span itemprop="name">Food-->
<!--</span></a><meta itemprop="position" content="1"></li><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="crumb"><a href="/breakfast" itemscope="" itemtype="http://schema.org/Thing" itemprop="item">-->
<!--	<span itemprop="name">Breakfast-->
<!--</span></a><meta itemprop="position" content="2"></li><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="crumb selected"><a href="/local-breakfast" itemscope="" itemtype="http://schema.org/Thing" itemprop="item">-->
<!--	<span itemprop="name">Local Breakfast-->
<!--</span></a><meta itemprop="position" content="3"></li></ol><section><hr class="first">-->
<!--                                            <div class="name">Local Breakfast-->
<!--                                            </div><hr class="last"></section>-->
<!--                                        <div>-->
<!--                                        </div>-->
<!--                                    </div>-->
                                    <div class="categorySection miscCategorySection onlyMiscCategorySection">
                                        <div class="productPane">
                                            <?php if (!empty($products) && count($products->result()) > 0): ?>
                                                <?php foreach ($products->result() as $product):
                                                    $data['product']=$product;
                                                    ?>
                                                    <?php $this->load->view('/frontend/partials/product', $data); ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div></section>
        </div>
    </div>
