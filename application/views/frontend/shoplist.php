<div id="wrapper">
    <div class="container">
 <?php $this->load->view( '/frontend/partials/topheader'); ?>
 <?php $this->load->view( '/frontend/partials/topbar'); ?>
 <?php $this->load->view( '/frontend/partials/topnav'); ?>
    <div class="bs">
        <?php $this->load->view('/frontend/partials/shopcategory'); ?>
        <?php
        if (!empty($feeds) && count($feeds->result()) > 0): ?>
        <button class="openbtn" onclick="openNav()"  id="openbtn">☰ </button>

        <div class="mainBanner">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php foreach ($feeds->result() as $feed): ?>

                        <div class="swiper-slide">
                            <?php
                                $imginfo=$this->Image->get_one_by( array( 'feed' => 'category', 'img_parent_id' => $feed->id));
                                if($imginfo->img_path !=null) { ?>
                                    <img src="https://eshtri.net/uploads/<?php echo $imginfo->img_path;?>" class="img-fluid">
                                <?php } else { ?>
                                    <img src="<?php echo site_url( '/'); ?>files/Icon-Small.png" class="img-fluid">
                                <?php } ?>
                            <div class="clear"></div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <div class="clear"></div>
        <div class="arrival shoplist">
            <div class="container-fluid">
                <div class="row">
                    <?php
                    if (!empty($popularshop) && count($popularshop->result()) > 0): ?>
                        <div class="col-lg-12 shopSectionTexts">
                            <h2 class="shopUIHeading pt-5 pb-5"><?php echo get_msg('All Shop')?></h2>
                        </div>
                        <div class="col-lg-12 shopproductlistup">
                            <ul class="thumbnails row">
                                <?php foreach ($popularshop->result() as $shop): ?>
                                    <li class="span4 item  col-xs-2 col-lg-2">
                                        <div class="thumbnail">
                                            <div class="item">
                                                <div class="productOne">
                                                    <?php
                                                    $default_photo = get_default_photo($shop->id, 'shop');
                                                    if($default_photo->img_path !=null) { ?>
                                                        <img src="https://eshtri.net/uploads/<?php echo $default_photo->img_path;?>" class="img-fluid">
                                                    <?php } else { ?>
                                                        <img src="<?php echo site_url( '/'); ?>files/Icon-Small.png" class="img-fluid">
                                                    <?php } ?>
                                                    <a href="<?php echo base_url('shopproducts/shopdashboard/'.$shop->id); ?>" tabindex="0"><button type="button" class="btn btn" tabindex="0"><?php echo get_msg('Visit_Store') ?></button></a>                                                    <div class="arz">
<!--                                                        <h6 class="text-center">--><?php //echo $shop->name; ?><!--</h6>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>


                                <?php endforeach; ?>
                            </ul>
                        </div>

                    <?php endif; ?>

                </div>


            </div>
        </div>



    <?php $this->load->view( '/frontend/partials/footermenu'); ?>
    <?php $this->load->view( '/frontend/partials/copyright'); ?>
    </div>
</div>
<!-------wrapper------->

<style>
    * {box-sizing: border-box;}
    .thumbnail, .row {
        margin-top: 0;
        margin-bottom: 1rem;
        list-style: none;
    }
    .overlay {
        position: absolute;
        bottom:60px;
        background: rgb(0, 0, 0);
        background: rgba(0, 0, 0, 0.5); /* Black see-through */
        color: #fff;
        width: 100%;
        height: 80%;
        transition: .5s ease;
        opacity:0;
        color: white;
        font-size: 20px;
        padding: 55px;
        text-align: center;
        /*top: 0;*/
        /*left: 0;*/
        border: 1px solid #CCC;


    }
    .item:hover .overlay {
        opacity: 1;
    }
    /*    .disabled-link{
            cursor: default;
            pointer-events: none;
            text-decoration: none;
            color: grey;
        }*/
    a:hover {
        color: #0056b3;
        text-decoration: none;
    }

    .btnShowDetails {
        display: block;
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        height: 25px;
        background: #fff;
        text-align: center;
        border: 0;
        width: 100%;
        color: #5e5e5e;
        font-weight: 700;
        font-size: 12px;
        padding: 3px;
        text-decoration: none;
        line-height: 20px;
        text-underline: none;
    }
    .overlay.text p.addText {
        height: 100px;
        width: 125px;
        margin: 30px auto 0;
        color: #fff;
        font-size: 22px;
        line-height: 34px;
    }

    /*.shop2 .slide {*/
    /*    margin: 0 10px;*/
    /*    position: relative;*/
    /*}*/

    /*.shop2 .slide img {*/
    /*    width: 100%;*/
    /*    height: 130px;*/
    /*}*/

    .shoplist .productOne button {
        position: absolute;
        top: 40px;
        left: 40px;
        display: none;
        background-color: #fff;
        border-radius: 45px;
        font-family: "Poppins";
        font-weight: 700;
        font-size: 14px;
    }

    .shoplist .productOne:hover button {
        display: block;
        transition: 1s;
    }

    .shoplist .productOne::before {
        position: absolute;
        content: "";
        top: 0;
        left: 0;
        background-color: rgba(39, 174, 96, 0.8);
        width: 0;
        height: 0;
        transition: 0.5s;
    }

    .shoplist .productOne:hover::before {
        width: 100%;
        height: 100%;
    }

</style>