                    <?php
                    $attributes = array('id' => 'register-form','enctype' => 'multipart/form-data');
                    echo form_open(site_url("save"), $attributes);
                    ?>

                    <div class="main">

                        <div class="container">
                            <div class="signup-content" style="background-color: white">
                                <div class="signup-form">
                                    <div class="register-form">
                                        <div class="row">
                                            <legend><?php echo get_msg('shop_info')?></legend>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label>
                                                        <span style="font-size: 17px; color: red;">*</span>
                                                        <?php echo get_msg('name_label') ?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip"
                                                           title="<?php echo get_msg('shop_name_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label>

                                                    <input class="form-control" type="text" placeholder="Please enter shop name" name='name' id='name'
                                                           value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>
                                                        <span style="font-size: 17px; color: red;">*</span>
                                                        <?php echo get_msg('description_label') ?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('shop_desc_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label>

                                                    <textarea class="form-control" name="description" placeholder="Description" rows="5"></textarea>
                                                </div>

                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label>
                                                        <span style="font-size: 17px; color: red;">*</span>
                                                        <?php echo get_msg('shop_tags') ?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('shop_tags')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label><br>

                                                    <select class="form-control select2" multiple="multiple" name="shoptag" id="shoptag" style="width: 370px;">
                                                        <?php
                                                        $register="";
                                                        $tags = $this->Tag->get_all()->result();

                                                        $i = 1;
                                                        foreach ($tags as $tag) {

                                                            if($register->id != "") {
                                                                $conds['shop_id'] = $register->id;
                                                            } else {
                                                                $conds['shop_id'] = '0';
                                                            }

                                                            $conds['tag_id'] = $tag->id;


                                                            $shop_tags_id = $this->Shoptag->get_one_by($conds)->tag_id;

                                                            $selected_value = "";
                                                            if($shop_tags_id == "" ) {
                                                                $selected_value = "";
                                                            } else {
                                                                $selected_value = "selected";
                                                            }
                                                            echo $selected_value . $i;
                                                            echo '<option  '.$selected_value.' name="'.$tag->name.'" value="'.$tag->id.'">'.$tag->name.'</option>';

                                                            $i++;
                                                        }

                                                        if($register->id != "") {
                                                            $conds1['shop_id'] = $register->id;
                                                        } else {
                                                            $conds1['shop_id'] = 0;
                                                        }

                                                        $shop_tags_ids = $this->Shoptag->get_all_by($conds1)->result();
                                                        $existing_tag_ids = "";

                                                        foreach ($shop_tags_ids as $shop_tag) {

                                                            $existing_tag_ids .= $shop_tag->tag_id .",";


                                                        }

                                                        ?>
                                                        <input type="hidden"  id="tagselect" name="tagselect">
                                                        <input type="hidden"  id="existing_tagselect" name="existing_tagselect" value="<?php echo $existing_tag_ids; ?>">
                                                    </select>

                                                </div>

                                                <div class="form-group">

                                                    <label><span style="font-size: 17px; color: red;">*</span><?php echo get_msg('shop_cover_photo_label')?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('shop_photo_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label>

                                                    <br>

                                                    <?php echo get_msg('shop_image_recommended_size')?>

                                                    <input class="btn btn-sm" type="file" name="cover" id="cover">
                                                </div>

                                                <!-- Icon  -->
                                                <div class="form-group">

                                                    <label><span style="font-size: 17px; color: red;">*</span><?php echo get_msg('shop_icon_photo_label')?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('shop_photo_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label>

                                                    <br>

                                                    <?php echo get_msg('shop_image_recommended_size_icon')?>

                                                    <input class="btn btn-sm" type="file" name="icon" id="icon">
                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">
                                            <legend><?php echo get_msg('contact_info')?></legend>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label>
                                                        <?php echo get_msg('phone_label') ?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip"
                                                           title="<?php echo get_msg('shop_name_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label>

                                                    <input class="form-control" type="text" placeholder="<?php echo get_msg('phone_label') ?>" name='about_phone1' id='about_phone1' value="">
                                                </div>


                                                <div class="form-group">
                                                    <label>
                                                        <?php echo get_msg('phone_label3') ?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip"
                                                           title="<?php echo get_msg('shop_name_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label>

                                                    <input class="form-control" type="text" placeholder="<?php echo get_msg('phone_label3') ?>" name='about_phone3' id='about_phone3' value="">
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label>
                                                        <?php echo get_msg('phone_label2') ?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip"
                                                           title="<?php echo get_msg('shop_name_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label>

                                                    <input class="form-control" type="text" placeholder="<?php echo get_msg('phone_label2') ?>" name='about_phone2' id='about_phone2' value="">
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><h4><a href="#mapmodals" data-toggle="modal" role="button"> <?php echo get_msg('Map_Location')?> <i class="fa fa-map-marker"></i> </a></h4></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label><?php echo get_msg('address_label1')?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('shop_address_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label>
                                                    <textarea class="form-control" name="address1" id="address1" placeholder="<?php echo get_msg('address_label1')?>" rows="5"></textarea>
                                                </div>

                                                <div class="form-group">
                                                    <label><?php echo get_msg('address_label3')?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('shop_address_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label>
                                                    <textarea class="form-control" name="address3" id="address3" placeholder="<?php echo get_msg('address_label3')?>" rows="5"></textarea>
                                                </div>

                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label><?php echo get_msg('address_label2')?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('shop_address_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label>
                                                    <textarea class="form-control" name="address2" id="address2" placeholder="<?php echo get_msg('address_label2')?>" rows="5"></textarea>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><?php echo get_msg('contact_email_label')?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('shop_email_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label>

                                                    <input class="form-control" type="text" placeholder="Email" name='email' id='email'
                                                           value="">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><?php echo get_msg('about_website_label')?></label>
                                                    <?php
                                                    echo form_input( array(
                                                        'type' => 'text',
                                                        'name' => 'about_website',
                                                        'id' => 'about_website',
                                                        'class' => 'form-control',
                                                        'placeholder' => 'Website',
                                                        'value' => ''
                                                    ));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <legend><?php echo get_msg('user_info')?></legend>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label>
                                                        <span style="font-size: 17px; color: red;">*</span>
                                                        <?php echo get_msg('user_name') ?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip"
                                                           title="<?php echo get_msg('shop_name_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label>

                                                    <input class="form-control" type="text" placeholder="Please enter your name" name='user_name' id='user_name'
                                                           value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>
                                                        <span style="font-size: 17px; color: red;">*</span>
                                                        <?php echo get_msg('user_email_label') ?>
                                                        <a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('shop_desc_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
                                                        </a>
                                                    </label>

                                                    <input class="form-control" type="text" placeholder="Please enter your email" name='user_email' id='user_email'
                                                           value="">
                                                </div>

                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label> <span style="font-size: 17px; color: red;">*</span>
                                                        <?php echo get_msg('user_password')?></label>

                                                    <?php echo form_input(array(
                                                        'type' => 'password',
                                                        'name' => 'user_password',
                                                        'value' => '',
                                                        'class' => 'form-control form-control',
                                                        'placeholder' => 'Password',
                                                        'id' => 'user_password'
                                                    )); ?>
                                                </div>

                                                <div class="form-group">
                                                    <label> <span style="font-size: 17px; color: red;">*</span>
                                                        <?php echo get_msg('conf_password')?></label>

                                                    <?php echo form_input(array(
                                                        'type' => 'password',
                                                        'name' => 'conf_password',
                                                        'value' => '',
                                                        'class' => 'form-control form-control',
                                                        'placeholder' => 'Confirm Password',
                                                        'id' => 'conf_password'
                                                    )); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden"  id="latitude" name="latitude" value="">
                                        <input type="hidden"  id="longitude" name="longitude" value="">
                                        <div class="form-submit">
                                            <input type="submit" value="<?php echo get_msg('btn_save')?>" class="btn btn-outline-primary" name="save" />

                                            <input type="button" value="Reset" class="btn btn-outline-primary" id="reset" name="reset" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                    <div class="modal fade" id="mapmodals">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title"><?php echo get_msg('map_location'); ?></h4>
                                </div>
                                <div class="modal-body">
                                    <input id="pac-input" name="pac-input" class="controls form-control" type="text" placeholder="Search Box">
                                    <div id="map"></div>
                                    <ul id="places"></ul>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="button border" data-dismiss="modal"><?php echo get_msg('close'); ?></button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
                    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyBYcg0IU6aU5bAp1PrsWo1LI-PVgh9cQAQ&language=<?php echo $this->session->userdata('language_code'); ?>"></script>
                    <script type="text/javascript">
                        <?php if ( $this->config->item( 'client_side_validation' ) == true ): ?>

                        function jqvalidate() {

                            $('#register-form').validate({
                                rules:{
                                    name:{
                                        blankCheck : "",
                                        minlength: 3,
                                        remote: "<?php echo $module_site_url .'/ajx_exists/'.@$shop->id; ?>"

                                    },
                                    description:{
                                        required : true
                                    },
                                    shoptag:{
                                        required : true
                                    },
                                    cover:{
                                        required : true
                                    },
                                    icon:{
                                        required : true
                                    },
                                    user_name:{
                                        required : true
                                    },
                                    user_email:{
                                        required: true,
                                        remote: "<?php echo $module_site_url .'/ajx_exists_user/'.@$user->user_id; ?>"
                                    },
                                    user_password:{
                                        required: true,
                                        minlength: 4
                                    },
                                    conf_password:{
                                        required: true,
                                        equalTo: '#user_password'
                                    },
                                    latitude:{
                                        required : true
                                    },
                                    longitude:{
                                        required : true
                                    },
                                },
                                messages:{
                                    name:{
                                        blankCheck : "<?php echo get_msg( 'err_shop_name' ) ;?>",
                                        minlength: "<?php echo get_msg( 'err_shop_len' ) ;?>",
                                        remote: "<?php echo get_msg( 'err_shop_exist' ) ;?>."
                                    },
                                    description:{
                                        required : "<?php echo get_msg( 'err_description_label' ) ;?>."
                                    },
                                    shoptag:{
                                        required : "<?php echo get_msg( 'err_shop_tag_label' ) ;?>."
                                    },
                                    cover:{
                                        required : "<?php echo get_msg( 'err_image_missing' ) ;?>."
                                    },
                                    icon:{
                                        required : "<?php echo get_msg( 'err_icon_missing' ) ;?>."
                                    },
                                    user_name:{
                                        required: "<?php echo get_msg( 'err_user_name_blank' ); ?>"
                                    },
                                    user_email:{
                                        required: "<?php echo get_msg( 'err_user_email_blank' ); ?>",
                                        email: "<?php echo get_msg( 'err_user_email_invalid' ); ?>",
                                        remote: "<?php echo get_msg( 'err_user_email_exist' ); ?>"
                                    },
                                    user_password:{
                                        required: "<?php echo get_msg('f_user_pass_required'); ?>",
                                        minlength: "<?php echo get_msg('f_user_pass_length'); ?>"
                                    },
                                    conf_password:{
                                        required: "<?php echo get_msg('f_user_conf_required'); ?>",
                                        equalTo: "<?php echo get_msg('f_user_conf_length'); ?>"
                                    },
                                    latitude:{
                                        required : "<?php echo get_msg( 'select_map_location' ) ;?>."
                                    },
                                    longitude:{
                                        required : "<?php echo get_msg( 'select_map_location' ) ;?>."
                                    },
                                }

                            });

                            // custom validation
                            jQuery.validator.addMethod("blankCheck",function( value, element ) {

                                if(value == "") {
                                    return false;
                                } else {
                                    return true;
                                }
                            })


                        }

                        var marker;
                        var map;
                        var var_location = new google.maps.LatLng(21.5699946,39.3430764);
                        var previousZoom = 5;

                        function map_init() {
                            var var_mapoptions = {
                                center: var_location,
                                zoom: 5,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            };
                            map = new google.maps.Map(document.getElementById("map"),var_mapoptions);


                            var input = document.getElementById("pac-input");
                            var autocomplete = new google.maps.places.Autocomplete(input);
                            autocomplete.bindTo("bounds", map);



                            var markerIcon = {
                                url: "<?php echo img_url( 'thumbnail/marker.png'); ?>",
                            };


                            function placeMarker(location) {
                                if (marker == undefined){
                                    marker = new google.maps.Marker({
                                        position: location,
                                        map: map,
                                        animation: google.maps.Animation.DROP,
                                        icon:markerIcon,
                                    });
                                }
                                else{
                                    marker.setPosition(location);
                                }
                                map.setCenter(location);

                            }
                            google.maps.event.addListener(autocomplete, "place_changed", function(){
                                var place = autocomplete.getPlace();
                                if (place.geometry.viewport) {
                                    map.fitBounds(place.geometry.viewport);
                                } else {
                                    map.setCenter(place.geometry.location);
                                    map.setZoom(15);
                                }


                            });

                            google.maps.event.addListener(map, "click", function(event)
                            {
                                placeMarker(event.latLng);

                                var lat = parseFloat(event.latLng.lat());
                                var lng = parseFloat(event.latLng.lng());
                                var latlong = {lat: parseFloat(event.latLng.lat()), lng: parseFloat(event.latLng.lng())};

                                var geocoder = new google.maps.Geocoder;
                                geocoder.geocode({'location': latlong}, function(results, status) {
                                    if (status === 'OK') {
                                        if (results[0]) {
                                            previousZoom = map.zoom;
                                            document.getElementById("longitude").value = event.latLng.lng();
                                            document.getElementById("latitude").value = event.latLng.lat();
                                            var add = document.getElementById("address2").value = results[0].formatted_address;
                                            var enAddress = results[0].address_components;
                                            for (ac = 0; ac < enAddress.length; ac++) {
                                                if (enAddress[ac].types[0] == "route") { document.getElementById("address1").value = enAddress[ac].long_name; }
                                            }
                                        } else {
                                        }
                                    } else {
                                    }
                                });

                            });
                        }
                        if(document.getElementById("map")){
                            google.maps.event.addDomListener(window, 'load', map_init);
                        }


                        $('#mapmodals').on('shown.bs.modal', function () {
                            $("#address1").val('');
                            $("#address2").val('');

                            google.maps.event.trigger(map, "resize");

                            if ( $("#longitude").val() != "" )
                                var_location = new google.maps.LatLng($("#latitude").val(), $("#longitude").val());
                            map.setZoom(previousZoom);
                            document.getElementById("longitude").value = '';
                            document.getElementById("latitude").value = '';
                            map.setCenter(var_location);
                        });
                        <?php endif; ?>
                        $('#shoptag').change(function(){
                            var shop_tag = $(this).val();

                            $('#tagselect').val(shop_tag);

                        });
                    </script>
                    <style>
                        #map{
                            height: 600px;
                        }
                        .pac-card{margin:10px 10px 0 0;border-radius:2px 0 0 2px;box-sizing:border-box;-moz-box-sizing:border-box;outline:none;box-shadow:0 2px 6px rgba(0,0,0,0.3);background-color:#fff;font-family:Roboto}
                        #pac-container{padding-bottom:12px;margin-right:12px}
                        .pac-controls{display:inline-block;padding:5px 11px}
                        .pac-controls label{font-family:Roboto;font-size:13px;font-weight:300}
                        #pac-input,#pac-input2{background-color:#fff;font-family:Roboto;font-size:15px;font-weight:300;text-overflow:ellipsis;width:400px;z-index:20}
                        #pac-input2:focus,#pac-input:focus{border-color:#4d90fe}
                        #title{color:#fff;background-color:#4d90fe;font-size:25px;font-weight:500;padding:6px 12px}
                        #target{width:345px}
                        #pmap{height:450px}
                        .pac-container{background-color:#FFF;position:fixed;display:inline-block;float:left;z-index: 99999999;}
                        .modal-header{border-bottom:0}.modal-backdrop{z-index:10}​ .modal{z-index:200;top:10%}
                        .modal-header {
                            padding: 15px;
                            border-bottom: 1px solid #e5e5e5;
                        }
                        .modal-header:before, .modal-header:after {
                            content: " ";
                            display: table;
                        }
                        .modal-header:after {
                            clear: both;
                        }
                        .modal-header .close {
                            margin-top: -2px;
                        }
                        .close {
                            float: right;
                            font-size: 21px;
                            font-weight: bold;
                            line-height: 1;
                            color: #000;
                            text-shadow: 0 1px 0 #fff;
                            opacity: 0.2;
                            filter: alpha(opacity=20);
                        }
                        button.close {
                            padding: 0;
                            cursor: pointer;
                            background: transparent;
                            border: 0;
                            -webkit-appearance: none;
                        }
                        .modal-title {
                            margin: 0;
                            line-height: 1.428571429;
                            position: absolute;
                        }
                        h4, .h4 {
                            font-size: 18px;
                        }

                    </style>