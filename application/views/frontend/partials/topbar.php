<div class="topBar stickybar ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div id="logo">
                    <img src="<?php echo base_url("assets/frontend/");?>img/logo.png" class="img-fluid">
                </div>
                <div id="rightSection">
                    <div id="sbox" >
                        <div class="form">
                            <div class="input-group mb-4">
                                    <select class="form-control select2 selectTwo input-sm" inputmode="input" name="searchInput" id="searchInput">
                                    <option ><?php echo get_msg('Search')?></option>
                                </select>
                                 <div class="input-group-append">
                                    <button class="btn btn-secondary" type="button" id="searchbtn"
                                            style="background-color: #DD285D; border: 0; border-radius: 0 3px 3px 0;">
                                        <i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div id="openNavlink"><button class="openbtn" onclick="openNav()" id="openbtn">☰</button></div>

            <div class="clear"></div>
        </div>
    </div>
</div>
<style>


</style>
<script>
        function FetchData() {
            $(document).ready(function () {
            $("#searchInput").select2({
                // theme: "classic",
                // dir: "rtl",
                allowClear: true,
                placeholder: {
                    id: "",
                    placeholder: "<?php echo get_msg('Choose')?>"
                },
                language: {
                    inputTooShort: function (args) {

                        return "<?php echo get_msg('2_or_more_symbol')?>";
                    },
                    noResults: function () {
                        return "<?php echo get_msg('Not_Found')?>";
                    },
                    searching: function () {
                        return "<?php echo get_msg('Searching')?>...";
                    }
                },
                ajax: {
                    url: "https://shop.eshtri.net/rest/productlivesearch",
                    dataType: 'JSON',
                    delay: 100,
                    quietMillis: 50,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page,
                            producSearch: 'true',
                            lang:lang
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },

                placeholder: "<?php echo get_msg('product')?>...",
                allowClear: true,
                width: '100%',
                escapeMarkup: function (markup) {
                    return markup;
                },

                minimumInputLength: 2,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });
                $(document.body).on("change","#searchInput",function(e){
                    var productid = $(this).val();
                    window.location.href = "<?php echo site_url('shopproducts/productdetails');?>/"+productid+"";
                });
            });
        }
        setTimeout(FetchData, 1000);

        function formatRepo(repo) {
            if (repo.loading) {
                return repo.text;
            }
       // if(repo.thumbnail !=null){
            return "<div class='row slt-srs m-0'><div class='col-md-2 thumbnail sc-img p-0'><img class='sc-code lft' height='60px' width='60px'  src='https://eshtri.net/uploads/small/"+repo.thumbnail+"'/></div><div class='col-md-10 sc-con pl-3 pr-1'><span class='scsm-gp'><span class='sc-code lft'>" + repo.caption + "</span></span><span class='scsm-gp'><span class='sc-capt lft'><?php echo get_msg('price')?>: " + repo.price + "</span></span></span></div></div>";
           //return "<div class='row slt-srs m-0'><div class='col-md-1 thumbnail sc-img p-0'><img class='sc-code lft' height='60px' width='60px'  src='https://eshtri.net/uploads/small/"+repo.thumbnail+"'/></div><div class='col-md-11 sc-con pl-3 pr-1'><span class='scsm-gp'><span class='sc-code lft'>" + repo.caption + "</span></span><span class='scsm-gp'><span class='sc-capt lft'><?php echo get_msg('price')?>: " + repo.price + "</span></span></div></div>";
       //  return  "<div class='row slt-srs m-0'>" +
       //      "<div class='col-md-10 sc-con pl-3 pr-1'>" +
       //      "<span class='scsm-gp'>" +
       //      "<img class='sc-code lft' height='20px' width='20px'  src='https://eshtri.net/uploads/small/"+repo.thumbnail+"'></img>" +
       //      "<span class='sc-code lft' style='padding: 10px'>  " + repo.caption + "</span>" +
       //      "</span>" +
       //      "</div>" +
       //      "</div>";
       //      } else {
       //     return  "<div class='row slt-srs m-0'>" +
       //         "<div class='col-md-10 sc-con pl-3 pr-1'>" +
       //         "<span class='scsm-gp'>" +
       //         "<span class='sc-code lft'>" + repo.caption + "</span>" +
       //         "</span>" +
       //         "</div>" +
       //         "</div>";
       // }
        }

        function formatRepoSelection(repo) {
            if (repo.id === '') {
                return "<?php echo get_msg('product')?>...";
            }
            var imagePath='https://eshtri.net/uploads/small/'+repo.thumbnail;
            return repo.caption;
        }


</script>