<div class="topHeader ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="topHeaderLeft">
                    <?php
                    if($is_shop_dashboard){?>
                        <a href="<?php echo base_url('/'); ?>" class="back-btn btn"><i class="fa fa-chevron-left" aria-hidden="true"></i> <?php echo get_msg('Change_store')?></a>
                    <?php }?>
                    <a class="btn-outline" href="<?php echo base_url('shopproducts/shopdashboard/'.$shop_id); ?>">
                        <div id="logo">
                            <img src="<?php echo base_url("assets/frontend/");?>img/logo.png" class="img-fluid">
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="topHeaderMid">
                    <div id="sbox">

                        <div class="form">
                            <div class="input-group">
                                <select class="form-control select2 selectTwo input-sm" inputmode="input" name="searchInput" id="searchInput">
                                    <option ><?php echo get_msg('Search')?></option>
                                </select>
                                <div class="input-group-append">
                                    <button class="btn btn-secondary" type="button" id="searchbtn"
                                            style="background-color: #67c68f; border: 0; border-radius: 0 3px 3px 0;">
                                        <i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="openNavlink"><button class="openbtn" onclick="openNav()" id="openbtn">☰</button></div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="topHeaderRight">
                        <span class="lang-switcher">
                        <?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
                            <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english" class="elm__link no-barba">En</a>
                        <?php else: ?>
                            <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/arabic" class="elm__link no-barba">عربي </a>
                        <?php endif ?>
                        </span>
                    <?php if(!$is_login_page):?>
                        <?php if ($this->session->userdata('current_user_id')): ?>
                            <div id="looged-heade-mn-area">
                                <nav class="navbar navbar-expand-lg navbar-dark" id="looged-menu-header">
                                    <div class="collapse navbar-collapse" id="main_nav">
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropdown">
                                                <a class="nav-link  dropdown-toggle more-menu-logged" href="#" data-toggle="dropdown">  <?php echo get_msg('Account') ?> </a>
                                                <ul class="dropdown-menu">
                                                    <a href="<?php echo base_url('dashboard'); ?>"  class="dropdown-item"><i class="fa fa-user" aria-hidden="true active"></i>  <?php echo get_msg('Dashboard') ?></a>
                                                    <a href="<?php echo base_url('orderlist'); ?>"  class="dropdown-item"> <i class="fa fa-first-order" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Orders') ?></a>
                                                    <a href="<?php echo base_url('address'); ?>"  class="dropdown-item"><i class="fa fa-globe" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Address') ?></a>
                                                    <a href="<?php echo base_url('accountdetails'); ?>"  class="dropdown-item"><i class="fa fa-money" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Account_Details') ?></a>
                                                    <a href="<?php echo base_url('messages'); ?>"  class="dropdown-item"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Messages') ?></a>
                                                    <a href="<?php echo base_url('customerlogin/logout'); ?>"  class="dropdown-item"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Log_Out') ?></a>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div> <!-- navbar-collapse.// -->
                                </nav>
                            </div>
                        <?php else: ?>
                            <a class="login-link" href="<?php echo base_url('/login'); ?>" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-sign-in" aria-hidden="true" ></i>&nbsp; &nbsp;<?=lang('Login')?></a>
                        <?php endif ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    function FetchData() {
        $(document).ready(function () {
            $("#searchInput").select2({
                // theme: "classic",
                // dir: "rtl",
                allowClear: true,
                placeholder: {
                    id: "",
                    placeholder: "<?php echo get_msg('Choose')?>"
                },
                language: {
                    inputTooShort: function (args) {
                        return "<?php echo get_msg('2_or_more_symbol')?>";
                    },
                    noResults: function () {
                        return "<?php echo get_msg('Not_Found')?>";
                    },
                    searching: function () {
                        return "<?php echo get_msg('Searching')?>...";
                    }
                },
                ajax: {
                    url: "https://shop.eshtri.net/rest/productlivesearch",
                    dataType: 'JSON',
                    delay: 100,
                    quietMillis: 50,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            shop_id: "<?=$shop_id?>", // search term
                            page: params.page,
                            producSearch: 'true',
                            lang:lang
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },

                placeholder: "<?php echo get_msg('product')?>...",
                allowClear: true,
                width: '100%',
                escapeMarkup: function (markup) {
                    return markup;
                },

                minimumInputLength: 2,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });
            $('#searchInput').on('select2:select', function (e) {
                var data = e.params.data;
                var obj = {};
                obj["product_id"] = data.id;
                var attribute = data.is_has_attribute;
                var color = data.is_has_color;
                // if (attribute == 1 || color == 1) {
                //     $("#productdetsil").modal('toggle');
                // }else{
                if(obj["product_id"]){
                    var url = "<?php echo site_url('shopproducts/addtocart');?>";
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: {postdata: obj},
                        success: function (data) {
                            if (data.isError == false) {
                                confirmNormalMessages(data.isError, data.message, false);
                                // $("#productdetsil").modal('toggle');
                                // if(data.orderdata.payment_url){
                                //     window.location.href = data.orderdata.payment_url;
                                // }
                                var sub_total_amount=data.data.sub_total_amount;
                                var sub_total_amount=Number(sub_total_amount).toFixed(2);

                                var tax_amount=data.data.tax_amount;
                                var tax_amount=Number(tax_amount).toFixed(2);

                                var sub_total_with_tax=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount);
                                var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

                                var total_balance_amount=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount);
                                var total_balance_amount=Number(total_balance_amount).toFixed(2);

                                $('#sub_total').html(sub_total_amount);
                                $('#total_tax').html(tax_amount);
                                $('#sub_total_with_tax').html(sub_total_with_tax);
                                $('#total_balance_amount').html(total_balance_amount);
                                $('#mycartbalance').html(total_balance_amount);
                                $('#finaltotal').html(total_balance_amount);
                                $('#mycartcount').html("<?php echo get_msg('My_Cart'); ?>("+data.data.total_item_count+")");
                                $('#totalqty').html(data.data.total_item_count);

                                generateCartTable(data.data.details);
                            } else {
                                alert(data.message);
                            }
                        },
                        failure: function (data) {
                        }
                    })
                }
                // }

            });


            $(document.body).on("click","#searchbtn",function(e){
                var productid = $('#searchInput').val();
                window.location.href = "<?php echo site_url('shopproducts/productdetails');?>/"+productid+"";
            });
        });
    }
    setTimeout(FetchData, 1000);

    function formatRepo(repo) {

        if (repo.loading) {
            return repo.text;
        }
        return "<div class='row slt-srs m-0'><div class='col-md-2 thumbnail sc-img p-0'><img class='sc-code lft' height='60px' width='60px'  src='https://eshtri.net/uploads/small/"+repo.thumbnail+"'/></div><div class='col-md-10 sc-con pl-3 pr-1'><span class='scsm-gp'><span class='sc-code lft'>" + repo.caption + "</span></span><span class='scsm-gp'><span class='sc-capt lft'><?php echo get_msg('price')?>: " + repo.price + "</span></span><span class='scsm-gp'><span class='sc-capt lft'><a href='#' class='btn-cart' data-check-color='" + repo.is_has_color + "' data-check-attribute='" + repo.is_has_attribute + "'  data-id='" + repo.id + "'><button type='button' class='btn btn btn-block'><i class='fa fa-shopping-bag' aria-hidden='true'></i>&nbsp;<?php echo get_msg('Add_To_Cart')?></button></a></span></span></div></div>";
        // if(repo.thumbnail !=null){
        //     return  "<div class='row slt-srs m-0'>" +
        //         "<div class='col-md-10 sc-con pl-3 pr-1'>" +
        //         "<span class='scsm-gp'>" +
        //         "<img class='sc-code lft' height='20px' width='20px'  src='https://eshtri.net/uploads/small/"+repo.thumbnail+"'></img>" +
        //         "<span class='sc-code lft' style='padding: 10px'>  " + repo.caption + "</span>" +
        //         "</span>" +
        //         "</div>" +
        //         "</div>";
        // } else {
        //     return  "<div class='row slt-srs m-0'>" +
        //         "<div class='col-md-10 sc-con pl-3 pr-1'>" +
        //         "<span class='scsm-gp'>" +
        //         "<span class='sc-code lft'>" + repo.caption + "</span>" +
        //         "</span>" +
        //         "</div>" +
        //         "</div>";
        // }
    }

    function formatRepoSelection(repo) {
        if (repo.id === '') {
            return "<?php echo get_msg('product')?>...";
        }
        var imagePath='https://eshtri.net/uploads/small/'+repo.thumbnail;
        return repo.caption;
    }


</script>