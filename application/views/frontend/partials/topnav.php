<div class="topNav stickyNav">
    <div class="container">
        <div class="row">
        <div class="col-md-12">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item ">
                    <a class="nav-link active" href="<?php echo base_url('/'); ?>"><?=lang('Home')?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('shoplist'); ?>"> <?php echo get_msg('All_shops')?></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo base_url('aboutus'); ?>"><?=lang('AboutUs')?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('/areas'); ?>"> <?=lang('CoverageArea')?></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo base_url('/contactus'); ?>"> <?=lang('ContactUs')?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('/app'); ?>"> <?=lang('DownloadApp')?></a>
                </li>
            </ul>
        </div>
    </nav>
    </div>
    </div>
    </div>
</div>