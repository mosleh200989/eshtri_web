<?php
$cartproduct=$this->Product->get_one($transaction_detail->product_id);

$att_name_info = explode("#", $transaction_detail->product_attribute_name);
$att_price_info = explode("#", $transaction_detail->product_attribute_price);
$att_info_str = "";
$att_flag = 0;
if ($att_name_info[0]) {

    //loop attribute info
    for ($k = 0; $k < count($att_name_info); $k++) {

        if ($att_name_info[$k] != "") {
            $att_flag = 1;
            $att_info_str .= $att_name_info[$k] . " : " . $att_price_info[$k] . "(" . $transaction->currency_symbol . "),";

        }
    }

} else {
    $att_info_str = "";
}
$att_info_str = rtrim($att_info_str, ",");
?>
<div data-pvid="<?=$transaction_detail->id?>" id="tr<?=$transaction_detail->id?>" data-type="express" class="orderItem">
    <div class="quantity">
        <div class="caret caret-up qtypluscart" data-pid="<?=$transaction_detail->id?>" title="Add one more to bag">
            <svg version="1.1" x="0px" y="0px" viewBox="0 20 100 100">
                <polygon
                        points="46.34,39.003 46.34,39.003 24.846,60.499 29.007,64.657 50.502,43.163 71.015,63.677 75.175,59.519 50.502,34.844   "></polygon>
            </svg>
        </div>
        <span class="onHoverCursor">
            <span></span>
        <span class="itemqty"><?=$transaction_detail->qty?></span>
        <span></span>
        </span>
        <div class="caret caret-down qtyminuscart" data-pid="<?=$transaction_detail->id?>" title="Remove one from bag">
            <svg version="1.1" x="0px" y="0px" viewBox="0 -20 100 100">
                <polygon
                        points="53.681,60.497 53.681,60.497 75.175,39.001 71.014,34.843 49.519,56.337 29.006,35.823 24.846,39.982   49.519,64.656 "></polygon>
            </svg>
        </div>
    </div>
    <div class="picture">
        <div class="productPicture">
            <?php if($transaction_detail->product_photo){?><img src="<?php echo site_url( '/'); ?>uploads/small/<?php echo $transaction_detail->product_photo; ?>"
                                                                size="200" style="background-color:transparent;"
                                                                srcset="<?php echo site_url( '/'); ?>uploads/small/<?php echo $transaction_detail->product_photo; ?>"><?php } ?>

        </div>
    </div>
    <div class="name">
        <span class="cart-product-name"><?php
            if ($att_flag == 1) {
                echo getCaption($cartproduct->name, $cartproduct->name_alt).' - '. $att_info_str . '<br>';
            } else {
                echo getCaption($cartproduct->name, $cartproduct->name_alt);
            }
            if ($transaction_detail->product_color_id != "") {
                echo "<br>".get_msg('color').":";
                $color_value =  $this->Color->get_one($transaction_detail->product_color_id)->color_value . '}';
                echo '<div style="background-color:'.$this->Color->get_one($transaction_detail->product_color_id)->color_value.'; width: 20px; height: 20px; margin-top: -20px; margin-left: 50px;"></div>';
            }
            ?>
        </span>
        <div class="subText">
            <span><?php echo get_msg('SAR'); ?></span>
            <span id="price<?=$transaction_detail->id?>"><?=addonlyvat($transaction_detail->price)?></span>
            <?php
            if($transaction_detail->product_measurement && $transaction_detail->product_unit){
                echo "<span>".get_msg('product_unit')." : " . $transaction_detail->product_measurement . " " . $transaction_detail->product_unit."</span><br>";
            }
            ?>
        </div>
    </div>
    <div class="amount">
        <?php
        $itemSubTotal=$transaction_detail->qty * addonlyvat($transaction_detail->price);
        if($transaction_detail->stock_status==0){
            $outOfStockAmount +=$itemSubTotal;
            $outOfStockAmountWithTax += $itemSubTotal+ (($transaction->tax_percent / 100) * $itemSubTotal);
        }
        ?>
        <section>
            <div class="discountAmount">
                <span><?php echo get_msg('SAR'); ?></span>
                <span id="subtotal<?=$transaction_detail->id?>"><?=$itemSubTotal?></span>
            </div>
<!--            <div class="total isStrikeThrough">-->
<!--                <span>--><?php //echo get_msg('SAR'); ?><!--</span>-->
<!--                <span>396</span>-->
<!--            </div>-->
        </section>
        <div class="remove btn-delete" data-pid="<?=$transaction_detail->id?>" title="Remove from bag">
            <svg version="1.1" x="0px" y="0px" viewBox="0 0 100 100">
                <rect x="19.49" y="46.963" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 121.571 49.0636)"
                      width="62.267" height="5.495"></rect>
                <rect x="18.575" y="47.876" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 49.062 121.5686)"
                      width="62.268" height="5.495"></rect>
            </svg>
        </div>
    </div>
</div>