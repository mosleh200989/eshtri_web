<div class="aboutCompany">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="aboutCompanyInner">

                    <h3><?=lang('Hshopkeeper')?></h3>
                        <p><?=lang('marketingur')?></p>
                        <a href="<?php echo base_url('partnership'); ?>" class="btn add-buiness-btn center-xs">
                        <i class="fa fa-arrow-left"></i>
                        <?=lang('Add_YourStore')?>
                    </a>
                    <div class="socialIcon">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                        <i class="fa fa-instagram" aria-hidden="true"></i>
                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                        <i class="fa fa-youtube-play" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .aboutCompany .add-buiness-btn {
    border: 1px solid #fff;
    display: block;
    max-width: 172px;
    background: #97b13c;
    margin: 0 auto;
    border-radius: 60px;
    color: #fff;
}
</style>