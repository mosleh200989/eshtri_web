<div class="subscription">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="subTxt">
                    <h2 class="subscriptionHeading text-white">Subscription</h2>
                    <p class="subscriptionPara">Register now to get updates on promotions and coupons.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form1">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Email Address">
                        <div class="input-group-append">
                            <button class="btn btn-secondary" type="button"><span class="material-icons">
                              mail_outline</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>