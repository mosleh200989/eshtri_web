<!----Cart-->
<div class="atcart">
    <div class="cart">
        <a href=""  data-toggle="modal" data-target="#aaModal"><i class="fa fa-shopping-cart carta" aria-hidden="true"></i></a>
        <p><span id="mycartcount"><?php echo get_msg('My_Cart'); ?> <?=$transaction->total_item_count?></span></p>
        <div class="cartInner">
            <p><?php echo get_msg('SAR'); ?>  <span id="mycartbalance"><?=$transaction->balance_amount?></span></p>
        </div>
    </div>
</div>