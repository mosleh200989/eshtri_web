<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <p class="text-success"><?=lang('Copyright')?></p>
            </div>
            <div class="col-lg-6">
                <p class="text-white company"><?php echo get_msg('Design_Developed_by') ?> <a href="https://www.alama360.com/"><?php echo get_msg('Alama360') ?></a></p>
            </div>
        </div>
    </div>
</div>