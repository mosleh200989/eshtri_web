<link rel="stylesheet" type="text/css" href="<?php echo base_url( 'assets/plugins/toastr/toastr.min.css' ); ?>" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.min.js" ></script>
<script src="<?php echo base_url("assets/frontend/");?>js/custom.js"></script>
<script src="<?php echo base_url("assets/backend/");?>js/toastr.min.js"></script>
<script src="<?php echo base_url("assets/backend/");?>js/dashboard.js"></script>
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css'); ?>">
<script src="<?php echo base_url( 'assets/select2/select2.full.min.js' ); ?>"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js"></script>
<script  src="https://www.gstatic.com/firebasejs/8.2.1/firebase-auth.js"></script>
<script>

    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyAjp5OfeOhMfejWHUjy0TVtdwhTlc_NYxs",
        authDomain: "eshtri.firebaseapp.com",
        projectId: "eshtri",
        storageBucket: "eshtri.appspot.com",
        messagingSenderId: "895559269145",
        appId: "1:895559269145:web:972da2aa1208bf29b62388"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    let userName="", phoneNumber="", loginredirect="", loginbtn="", verifybtn="", code="", parsedNumber="";
    <?php if($is_login_page):?>
    userName = document.getElementById("userName");
    phoneNumber = document.getElementById("mobileNumber");
    loginredirect = document.getElementById("loginredirect");
    loginbtn = document.getElementById("loginbtn");
    verifybtn = document.getElementById("verifybtn");
    code = document.getElementById("otpId");
    <?php else:?>

    userName = document.getElementById("userNamepopup");
    phoneNumber = document.getElementById("mobileNumberpopup");
    loginredirect = document.getElementById("loginredirectpopup");
    loginbtn = document.getElementById("loginbtnpopup");
    verifybtn = document.getElementById("verifybtnpopup");
    code = document.getElementById("otpIdpopup");
    <?php endif?>

    const auth =firebase.auth();
    loginbtn.onclick=function() {
        loginbtn.style.display="none";
        parsedNumber = parseInt(phoneNumber.value, 10);
        if(parsedNumber.toString().length ==9) {
            parsedNumber = "+966" + parsedNumber;
            window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recptcha-container', {
                'size': 'invisible',
                'callback': (response) => {
                }
            });
            // reCAPTCHA solved, allow signInWithPhoneNumber.
            const appVerifier = window.recaptchaVerifier;
            firebase.auth().signInWithPhoneNumber(parsedNumber, appVerifier)
                .then(function (confirmationResult) {
                    window.confirmationResult = confirmationResult;
                    document.getElementById("login-lbl").style.display = "none";
                    document.getElementById("login-fields").style.display = "none";
                    document.getElementById("otp-lbl").style.display = "block";
                    document.getElementById("otp-text").style.display = "block";
                    document.getElementById("otp-fields").style.display = "block";
                    document.getElementById("otp-number").innerHTML = parsedNumber;

                    verifybtn.style.display = "block";
                }).catch((error) => {
                loginbtn.style.display = "block";
                //console.log("Error; SMS not sent")
            });
        }else{
            alert("<?php echo get_msg('Please_enter_correct_mobile_number') ?>");
            loginbtn.style.display = "block";
        }
    }
    verifybtn.onclick=function () {
        window.confirmationResult.confirm(code.value).then(function (response) {
            loginwithmobile(userName.value, parsedNumber, loginredirect.value);
        }).catch(function (error) {

        })
    }

    function loginwithmobile(name, mobile, redirecturl) {
        jQuery.ajax({
            type: 'POST',
            dataType: "json",
            data: {mobile: mobile, name: name, redirecturl:redirecturl},
            url: "<?php echo site_url('/customerlogin/loginsubmit');?>",
            success: function (data, textStatus) {
                if (data.isError == false) {
                    if(data.user.user_id){
                        window.location.href = data.redirect;
                    }
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                // serverErrorToast(errorThrown);
            }
        });
    }

</script>
</body>
<?php
updateCounter($page_name); // Updates page hits
updateVisitorInfo();
?>
</html>