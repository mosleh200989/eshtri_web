<!-- cart panel -->
<div class="shoppingCartWrapper">
    <div class="cartContainer">
        <div class="shoppingCartButton">
        </div>
        <div class="shoppingCart collapsed non-empty responsive">
            <div class="header">
                <div class="cart">
                    <svg version="1.1" id="Calque_1" x="0px" y="0px" style="fill:#FDD670;stroke:#FDD670;" width="21px"
                         height="32px" viewBox="0 0 100 160.13">
                        <g data-reactid=".1a9x2luibw0.3.0.1.0.0.0.0">
                            <polygon points="11.052,154.666 21.987,143.115 35.409,154.666  "></polygon>
                            <path d="M83.055,36.599c-0.323-7.997-1.229-15.362-2.72-19.555c-2.273-6.396-5.49-7.737-7.789-7.737   c-6.796,0-13.674,11.599-16.489,25.689l-3.371-0.2l-0.19-0.012l-5.509,1.333c-0.058-9.911-1.01-17.577-2.849-22.747   c-2.273-6.394-5.49-7.737-7.788-7.737c-8.618,0-17.367,18.625-17.788,37.361l-13.79,3.336l0.18,1.731h-0.18v106.605l17.466-17.762   l18.592,17.762V48.06H9.886l42.845-10.764l2.862,0.171c-0.47,2.892-0.74,5.865-0.822,8.843l-8.954,1.75v106.605l48.777-10.655   V38.532l0.073-1.244L83.055,36.599z M36.35,8.124c2.709,0,4.453,3.307,5.441,6.081c1.779,5.01,2.69,12.589,2.711,22.513   l-23.429,5.667C21.663,23.304,30.499,8.124,36.35,8.124z M72.546,11.798c2.709,0,4.454,3.308,5.44,6.081   c1.396,3.926,2.252,10.927,2.571,18.572l-22.035-1.308C61.289,21.508,67.87,11.798,72.546,11.798z M58.062,37.612l22.581,1.34   c0.019,0.762,0.028,1.528,0.039,2.297l-23.404,4.571C57.375,42.986,57.637,40.234,58.062,37.612z M83.165,40.766   c-0.007-0.557-0.01-1.112-0.021-1.665l6.549,0.39L83.165,40.766z"></path>
                        </g>
                    </svg>
                </div>
                <div class="itemCount">
                    <span class="totalqty"><?php echo $transaction->total_item_count; ?></span><span class="totalqtylbl"><?php echo get_msg('Items'); ?></span>
                    <span class="count-mobile">
                    <span><?php echo $transaction->total_item_count; ?></span>
                </span>
                </div>
                <button class="closeCartButtonTop"><?php echo get_msg('Close'); ?></button>
            </div>
            <section class="in-shopping-cart meter-full" id="shipping-cost-meter">
                <div class="costMeterSection">
                    <div class="costMeter">
                        <div class="containerr">
                            <div class="progress">
                                <div style="width:1265%;" class="bar">
                                </div>
                                <div class="meterInfoText">
                                    <div class="InfoTextLeft"><span>
                                </span>
                                        <span><?php echo get_msg('You_have_reduced_delivery_charge'); ?>
                                </span>
                                        <span>
                                </span>
                                    </div>
                                    <div class="InfoTextRight">
                                <span class="priceSection">
                                    <span><span><?php echo get_msg('SAR'); ?>
                                    </span><span>0
                                    </span>
                                </span>
                            </span><span class="helpIcon"><svg width="15px" height="15px"
                                                               style="fill:#fff;stroke:#fff;display:inline-block;vertical-align:middle;"
                                                               version="1.1" viewBox="0 0 100 100">
                             <path d="m50 5c-24.898 0-45 20.102-45 45s20.102 45 45 45 45-20.102 45-45-20.102-45-45-45zm7.1016 70c0 2.1992-1.8984 4.1016-4.1016 4.1016h-6.1992c-2.1992 0-4.1016-1.8984-4.1016-4.1016v-26.199c0-2.3008 1.8984-4.1016 4.1016-4.1016h6.1992c2.1992 0 4.1016 1.8984 4.1016 4.1016zm-7.2031-37.102c-4.6016 0-8.3984-3.8008-8.3984-8.5 0-4.6992 3.8008-8.5 8.3984-8.5 4.6992 0 8.5 3.8008 8.5 8.5 0 4.7031-3.7969 8.5-8.5 8.5z"
                                   data-reactid=".1a9x2luibw0.3.0.1.1.1.0.0.0.1.1.1.0.0"></path></svg>
                         </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="stickyHeader">
                <div class="itemCount">
                    <svg version="1.1" id="Calque_1" x="0px" y="0px" style="fill:#FDD670;stroke:#FDD670;" width="16px"
                         height="24px" viewBox="0 0 100 160.13">
                        <g>
                            <polygon points="11.052,154.666 21.987,143.115 35.409,154.666  "></polygon>
                            <path d="M83.055,36.599c-0.323-7.997-1.229-15.362-2.72-19.555c-2.273-6.396-5.49-7.737-7.789-7.737   c-6.796,0-13.674,11.599-16.489,25.689l-3.371-0.2l-0.19-0.012l-5.509,1.333c-0.058-9.911-1.01-17.577-2.849-22.747   c-2.273-6.394-5.49-7.737-7.788-7.737c-8.618,0-17.367,18.625-17.788,37.361l-13.79,3.336l0.18,1.731h-0.18v106.605l17.466-17.762   l18.592,17.762V48.06H9.886l42.845-10.764l2.862,0.171c-0.47,2.892-0.74,5.865-0.822,8.843l-8.954,1.75v106.605l48.777-10.655   V38.532l0.073-1.244L83.055,36.599z M36.35,8.124c2.709,0,4.453,3.307,5.441,6.081c1.779,5.01,2.69,12.589,2.711,22.513   l-23.429,5.667C21.663,23.304,30.499,8.124,36.35,8.124z M72.546,11.798c2.709,0,4.454,3.308,5.44,6.081   c1.396,3.926,2.252,10.927,2.571,18.572l-22.035-1.308C61.289,21.508,67.87,11.798,72.546,11.798z M58.062,37.612l22.581,1.34   c0.019,0.762,0.028,1.528,0.039,2.297l-23.404,4.571C57.375,42.986,57.637,40.234,58.062,37.612z M83.165,40.766   c-0.007-0.557-0.01-1.112-0.021-1.665l6.549,0.39L83.165,40.766z"></path>
                        </g>
                    </svg>
                    <p><span><span class="totalqty"><?php echo $transaction->total_item_count; ?></span> <?php echo get_msg('Items'); ?>
    </span></p>
                </div>
                <div class="total"><span><?php echo get_msg('SAR'); ?>
</span>
                    <div class="odometer odometer-auto-theme">
                        <?php echo "<span class='odometer-inside'>" . number_format(($transaction->sub_total_amount + $transaction->tax_amount), 2, '.', '') . "</span> "; ?>
                    </div>

                </div>
            </section>
            <div class="body">
                <section class="itemsHeader discountedPriceMessageHeader">
                    <div><span><?php echo get_msg('You_have_saved'); ?> <?php echo get_msg('SAR'); ?> <span
                                    id="discount-amount"></span>
        </span>
                    </div>
                </section>
                <div>
                    <section class="itemsHeader expressHeader">
                        <div>
                            <div class="cartItemsHeaderIcon">
                                <svg version="1.1" x="0px" y="0px" viewBox="0 20 90 90">
                                    <path class="st0"
                                          d="M59.4,32.6c0.2,0.2-0.2,0.3-0.2,0.3s-1.2,0.3-1.5,0.8c-0.3,0.3-0.8,1.5-0.8,1.5c0.7-0.2,1.5-0.3,2.3-0.7 c0.2,0,0.3,0.2,0.5,0.3c0.2,0-2,0.8-2.5,1.2c-0.3,0.3-2,1.7-2.5,1.7S53,37.9,53,37.9c0.2,0.3,0.3,0.7,0.7,1.2c0,0-1.2,0.7-1.2,0.8 c-0.2,0.2-1,1.2-1,1.2s3,0.3,4-1.3c-1,1.7-3.3,3.3-8.2,3.3c-5,0-9-2-11.7-3c-2.8-1.2-6.5-2.5-9.5-2.5c-2.8,0-6.2,0.8-8.4,2.9 c-3.9-1.5-9.2-0.7-11.7,1.7c-2.3,2.3-4.7,5.2-5,7.2c-0.5,2.2-1.7,7.4-3.3,9.7c-2.3,3.8-5,4.8-5.9,6.5c-0.8,1.7-0.8,5.2-0.7,5.2 c0.3,0.2,1.5,0,2.2-0.3c1.3-0.5,5-4.3,6.4-6.2c1.3-1.8,5.9-9.9,6.5-12.7c0.7-2.8,1.3-5.7,3.3-7.2c2.2-1.5,4.8-0.8,4.8-0.7 c0,0-2.5,2-2.7,7.5c-0.2,5.5,3.2,4.9,1.5,8.4c-1.7,3-6.7,1-8.4,2.8c-1.3,1.5,0,6.9,0,7.9c0,1-0.5,5.7-0.8,6.9c-0.2,1,0,1.7-0.2,2.3 c-0.2,0.5,1,0.7,1,0.7s-1,1.3-1.5,1c-0.5-0.2-0.8-0.2-1.2,0c-0.3,0-1,0.3-1.7,0l-2.7,5.7c0.3,0.2,0.8,0.3,1.3,0.2 c0.7-0.2,4-1.7,4.4-2c0.5-0.2,3.3-1.8,3.8-2.8c0.3-1,1-4.2,1-5.2c0.2-1,0.3-4.2,0.3-5c0-0.8-0.2-2.3,0-3c0.3-0.5,0.8-1.2,1.5-1.5 c0.8-0.2,1.8-0.2,2-0.2c0.3,0,3.2,0.3,3.7,0.3c0.7,0,3.3-0.5,4-0.8c0.8-0.2,1.3,0,1.5,0.3c0.3,0.3-0.3,0.8-0.3,1 c-0.2,0.2-1.5,1.8-1.8,2.5c-0.3,0.8-1.5,2.3-1.2,3c0.3,0.7,0.3,0.8,1.3,1.3c1,0.3,4.9,2.3,7,4.3c2.3,2.2,4.5,4.8,4.9,6.2 c0.5,1.5,1.3,1.7,1.8,1.7c0.5,0.2,1.5,0.3,1.5,1c0,0.7-0.2,1.5-0.2,1.5l6.2,2.3c0,0,0-2.2-0.2-3c-0.2-1-1.2-2-2-3 c-0.8-1.2-9.5-9.7-10.4-10.4c-0.8-0.5-1.5-1.2-1.7-2.5c-0.2-1.3,1.5-2.3,2.5-3.5c0.7-0.8,2.5-4.7,2.8-5.2c0.3-0.5,1.5-1.8,2.2-1.8 c0.8,0,2.7,1.2,5,2c1,0.3,8.2,2.2,9.2,2.5c1,0.2,3.2,0,3.8,0.7c0.7,0.5,0,1.8-0.2,2.7c-0.2,1-2,4.7-2,4.7s0.3,1.3,0.3,2.4 c0,1.2-0.5,5.3-1,6.7c-0.3,1.5-2.7,5.4-3.2,6c-0.3,0.5-0.8,0.8-0.8,1.7c0,0.8,1.5,1,2,1.7c0.7,0.5,0.2,1.5,0,2.5l6.7,2.8 c0-0.5,0.3-1.3,0-1.7c-0.2-0.5-1-1.5-1.5-2.2c-0.3-0.5-1.5-2.3-1.8-2.7c-0.3-0.5-0.3-1.5,0-2.3c0.2-0.8,2.3-6.5,2.5-7.2 c0.3-0.8,3.5-7.5,3.8-8.4c0.3-1,3-5.9,3.3-6.7c0.3-1,2.5-1.8,3.2-1.8c0.8,0,5,1.5,5.7,1.8c0.5,0.2,4.4,1.8,5,2.2 c0.8,0.3,0.3,1-0.2,1.2c-0.5,0.3-6.7,4.5-7.2,4.7c-0.5,0.2-1,0.7-1.3,0.7c-0.5,0-1.2,0.5-1.2,0.7c0,0.3-0.2,1-1,1c-0.7,0-2-1-2.8-2 c-1.2,0.7-2,1.2-2.5,2.2c-0.8,0.8-1.2,1.5-1.7,2.7c0.3,0.3,1,0.5,1.5,0.5h5.4c0.5,0,1.2-0.2,1.7-0.3c0.5-0.3,13.7-9,13.7-9 s2.2-1.7,2.2-3.2c0-1.2-1-1.5-1.3-2c-0.5-0.3-3.5-2.3-4-2.7c-0.7-0.5-3-1.8-3.7-2.2c-0.5-0.3-2.4-1-2.4-1c0.5-1.5,0.8-2.7,1-3 c0.2-0.3,0.2-3.4,0.3-3.9c0-0.5,0.8-2.7,1.2-3c0.3-0.3,1.2-3,1.3-3.5c0.2-0.3,1.5-4.2,2.2-4.2c0.7-0.2,1.3,0.5,2,0.5 c0.7,0.2,3.2-0.2,4.5,0c1.2,0.2,2.7,1.7,3,1.8c0.5,0.2,1.7,0.7,2,0.7c0.2,0,1.8-1.2,1.8-1.5c0-0.3-1.2-1-1.3-1 c-0.2-0.2-1.3-0.8-1.3-0.8c0,0.2,0.5-0.3,1.3-0.3c0.7,0,1.3,0.7,1.5,0.8c0.3,0.2,1.2,0.7,1.3,0.3c0.2-0.2,0.8-1,0.8-1.5 c0-0.7-0.7-1.5-1-1.8c-0.3-0.3-4.2-4-4.5-4.2c-0.5-0.2-2.5-2-2.5-2.5c-0.2-0.3-1.2-1.2-1.3-1.3c-0.2-0.2-5.2-3.7-5.9-4.2 c-0.7-0.5-1.3-1.3-1.3-2.2c0-0.8-0.3-3-0.3-3s-1.2-0.2-1.7,0.3c-0.5,0.3-1.7,3-2.3,3c-1.8-0.3-4.2,0.2-4.2,0.5 c0.2,0.5,1.8,0.2,1.8,0.2c0,0.2-0.7,1-1.2,1c-0.3,0-1.3,0.3-1.5,0.5c-0.3,0-1,0.5-1,0.5c0.5,0,1,0,1.7,0.2c0,0-1,0.7-1.5,0.7 c-0.5,0-2.2,0.5-2.7,0.7c-0.3,0.3-1.7,1.3-1.7,1.3h-1.5l0.2,0.3h0.8L59.4,32.6L59.4,32.6z"></path>
                                </svg>
                            </div>
                            <span><?php echo get_msg('Express_Delivery'); ?>
            </span>
                        </div>
                    </section>

                    <!--                    single cart item-->
                    <div id="cartitemsarea">
                    <?php
                    $conds['cart_header_id'] = $transaction->id;
                    $all_detail = $this->Cartdetail->get_all_by($conds);
                    $rowNumber = 0;
                    $outOfStockAmount = 0;
                    $outOfStockAmountWithTax = 0;
                    $no = 1;
                    foreach ($all_detail->result() as $transaction_detail):

                        $data['transaction_detail'] = $transaction_detail;
                        ?>
                        <?php $this->load->view('/frontend/partials/sidebarcartitem', $data); ?>

                        <?php
                        ?>
                    <?php endforeach; ?>
                    </div>
                    <!--                    single cart item end-->


                </div>
<!--                <section class="discountCodeContainer notEligible">-->
<!--                    <div class="discountCodeHeader">-->
<!--                        <button class="btnDiscount">-->
<!--            <span class="arrow-icon arrow-up"><svg width="10px" height="10px"-->
<!--                                                   style="fill:#787878;stroke:#787878;display:inline-block;vertical-align:middle;"-->
<!--                                                   viewBox="0 0 100 100">-->
<!--             <path transform="translate(0 -952.36)"-->
<!--                   d="m31.918 1045.4l36.164-31.684 12.918-11.316-12.918-11.316-36.164-31.684-12.918 11.316 36.168 31.684-36.168 31.684zm0 0"-->
<!--                   stroke="#000" stroke-linecap="round" stroke-width="2"></path></svg>-->
<!--         </span><span>--><?php //echo get_msg('Have_a_special_code?'); ?>
<!--         </span></button>-->
<!--                    </div>-->
<!--                    <div class="discountCodeContent">-->
<!--                    </div>-->
<!--                </section>-->
            </div>

            <div class="">
                <div class="footer">
                    <div class="shoppingtCartActionButtons">
                        <button id="placeOrderButton">
                <span class="placeOrderText"><?php echo get_msg('Place_Order'); ?>
                </span><span class="totalMoneyCount"><span><?php echo get_msg('SAR'); ?>
                </span><?php echo "<span id='total_balance_amount'>" . number_format(($transaction->sub_total_amount + $transaction->tax_amount), 2, '.', '') . "</span> "; ?><span>
                </span>
            </span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cart panel -->
<script>
    function generateCartTotals(orderdetails, transaction){
            var url = "https://shop.eshtri.net/rest/generatecarttotal?shop_id=<?=$shop_id?>&time=<?=$carttime?>&order_id="+transaction.id+"&isempty=false";
            $.ajax({
                url: url,
                type: 'POST',
                dataType: "json",
                //data: {"product_id": product_id, "time":"<?//=$carttime?>//", "shop_id":"<?//=$shop_id?>//"},
                // mode: 'cors',
                // headers: { 'Content-Type': 'application/json'},
                success: function (data) {
                    if (data.isError == false) {
                        if(data.data && data.data.details){
                            updateDetails(data.data.details, data.data);
                            generateCartTable(data.data.details);
                        }

                    } else {
                        alert(data.message);
                    }
                },
                failure: function (data) {
                }
            })
    }
    function updateDetails(orderdetails, transaction) {
        var totalItem = 0;
        var totalItemAmountNoTax = 0.0;
        var sub_total_amount = 0.0;
        var discount_total_amount = 0.0;
        var originalprice_total_amount = 0.0;
        var tax_amount = 0.0;
        orderdetails.forEach(function(sinleDetails) {
            if (sinleDetails && sinleDetails.id) {
                var qtyDtls = parseInt(sinleDetails.qty);
                var originalPriceDtls = parseFloat(sinleDetails.original_price);
                var discountPercentDtls = sinleDetails.discount_percent;
                var price = originalPriceDtls;
                var discount_amount = 0.0;
                if (discountPercentDtls > 0) {
                    discount_amount = ((parseFloat(sinleDetails.discount_percent) / 100) * originalPriceDtls);
                    discountPriceDtls = (originalPriceDtls - ((parseFloat(sinleDetails.discount_percent) / 100) * originalPriceDtls));
                    price = discountPriceDtls;
                }

                var stockDtls = sinleDetails.stock_status;
                var itemSubtotal = parseFloat(originalPriceDtls) * parseInt(qtyDtls);
                if (stockDtls == '1') {
                    totalItem += qtyDtls;
                    totalItemAmountNoTax += itemSubtotal;
                    var itemDiscountAmount = discount_amount * qtyDtls;
                    discount_total_amount += itemDiscountAmount;
                    sub_total_amount += price * qtyDtls;
                    var itemOriginalAmount = parseFloat(sinleDetails.original_price) * qtyDtls;
                    originalprice_total_amount += itemOriginalAmount;
                }
            }
        });

        var total_item_amount = sub_total_amount;
        if (transaction.coupon_discount_amount > 0) {
            total_item_amount = parseFloat(total_item_amount) - parseFloat(transaction.coupon_discount_amount);
        }
        tax_amount = ((parseFloat(transaction.tax_percent) / 100) * total_item_amount);

        sub_total_amount=Number(sub_total_amount).toFixed(2);
        tax_amount=Number(tax_amount).toFixed(2);

        var sub_total_with_tax=parseFloat(sub_total_amount)+parseFloat(tax_amount);
        var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

        var total_balance_amount=parseFloat(sub_total_amount)+parseFloat(tax_amount);
        var total_balance_amount=Number(total_balance_amount).toFixed(2);

        $('#total_balance_amount').html(total_balance_amount);
        $('#totalqty').html(totalItem);
        $('.totalqty').html(totalItem);
        $('.count-mobile').html(totalItem);
        $('.odometer-inside').html(total_balance_amount);
    }
    function generateCartTable(details){
        $("#cartitemsarea").html("");
        $(".shoppingtCartActionButtons").show();
        $.each(details, function(key,val) {
            if(val.product_photo){
                var img="uploads/small/"+val.product_photo;
            }else{
                var img="files/Icon-Small.png";
            }
            var itemSubTotal=parseFloat(val.price) * parseInt(val.qty);
            var itemSubTotal=Number(itemSubTotal).toFixed(2);

        var rowdata='<div data-pvid="'+val.id+'" id="tr'+val.id+'" data-type="express" class="orderItem"> ' +
            '<div class="quantity">' +
            '<div class="caret caret-up qtypluscart" data-pid="'+val.id+'" title="Add one more to bag">' +
            '<svg version="1.1" x="0px" y="0px" viewBox="0 20 100 100">' +
            '<polygon points="46.34,39.003 46.34,39.003 24.846,60.499 29.007,64.657 50.502,43.163 71.015,63.677 75.175,59.519 50.502,34.844   "></polygon>' +
            '</svg>' +
            '</div>' +
            '<span class="onHoverCursor">' +
            '<span></span>' +
            '<span class="itemqty">'+val.qty+'</span>' +
            '<span></span>' +
            '</span>' +
            '<div class="caret caret-down qtyminuscart" data-pid="'+val.id+'" title="Remove one from bag">' +
            '<svg version="1.1" x="0px" y="0px" viewBox="0 -20 100 100">' +
            '<polygon points="53.681,60.497 53.681,60.497 75.175,39.001 71.014,34.843 49.519,56.337 29.006,35.823 24.846,39.982   49.519,64.656 "></polygon>' +
            '</svg>' +
            '</div>' +
            '</div>' +
            '<div class="picture">' +
            '<div class="productPicture">' +
            '<img src="<?php echo site_url('/');?>'+img+'" size="200" style="background-color:transparent;" srcset="<?php echo site_url('/');?>'+img+'">' +
            '</div>' +
            '</div>' +
            '<div class="name">' +
            '<span class="cart-product-name">'+val.product_name+'</span>' +
            '<div class="subText">' +
            '<span><?php echo get_msg('SAR'); ?></span>' +
            '<span id="price'+val.id+'">'+addonlyJSvat(val.price)+'</span>';
            if(val.product_measurement) {
                rowdata += '<span><?php echo get_msg('product_unit')?> : ' + val.product_measurement + ' ' + val.product_unit + '</span><br>';
            }
            rowdata +='</div></div>' +
            '<div class="amount">' +
            '<section>' +
            '<div class="discountAmount">' +
            '<span><?php echo get_msg('SAR'); ?></span>' +
            '<span id="subtotal1'+val.id+'">'+addonlyJSvat(itemSubTotal)+'</span>' +
            '</div>' +
            '</section>' +
            '<div class="remove btn-delete" data-pid="'+val.id+'" title="Remove from bag">' +
            '<svg version="1.1" x="0px" y="0px" viewBox="0 0 100 100">' +
            '<rect x="19.49" y="46.963" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 121.571 49.0636)" width="62.267" height="5.495"></rect>' +
            '<rect x="18.575" y="47.876" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 49.062 121.5686)" width="62.268" height="5.495"></rect>' +
            '</svg>' +
            '</div>' +
            '</div>' +
            '</div>';

                $('#cartitemsarea').append(rowdata);
        });
        runAfterJQ();
    }
    function generateCartEmptyTable(transactionid){
        $("#cartitemsarea").html("");
        var emptyhtml='<div class="emptyCart"><div class="nothingToSeeHereMoveOn"><div><img src="https://cdn.chaldal.net/asset/Egg.Grocery.Fabric/Egg.Grocery.Web/1.5.0+Release-1700/Default/components/header/ShoppingCart/images/emptyShoppingBag.png?q=low&amp;webp=1&amp;alpha=1"></div><span><?php echo get_msg('Your_shopping_bag_is_empty_Start_shopping'); ?></span></div></div>';
        $("#cartitemsarea").html(emptyhtml);
        $(".shoppingtCartActionButtons").hide();
        $('#total_balance_amount').html(0);
        $('#totalqty').html(0);
        $('.totalqty').html(0);
        $('.count-mobile').html(0);
        $('.odometer-inside').html(0);

        var url = "https://shop.eshtri.net/rest/generatecarttotal?shop_id=<?=$shop_id?>&time=<?=$carttime?>&order_id="+transactionid+"&isempty=true";
        $.ajax({
            url: url,
            type: 'POST',
            dataType: "json",
            success: function (data) {
            },
            failure: function (data) {
            }
        })

    }
    function addbtn() {
        $(document.body).on("click", "section.btn-cart", function (e) {
            var obj = {};
            var product_id = $(this).attr('data-id');
            var attribute = $(this).attr('data-check-attribute');
            var color = $(this).attr('data-check-color');
            // if (attribute == 1 || color == 1) {
            //     $("#productdetsil").modal('toggle');
            // }else{
            if(product_id){
                var url = "https://shop.eshtri.net/rest/addtocart?shop_id=<?=$shop_id?>&time=<?=$carttime?>&product_id="+product_id+"";
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: "json",
                    //data: {"product_id": product_id, "time":"<?//=$carttime?>//", "shop_id":"<?//=$shop_id?>//"},
                    // mode: 'cors',
                    // headers: { 'Content-Type': 'application/json'},
                    success: function (data) {
                        if (data.isError == false) {
                            generateCartTotals(data.data.details, data.data);

                        } else {
                            alert(data.message);
                        }
                    },
                    failure: function (data) {
                    }
                })
            }
            // }
            e.preventDefault();
        });
    }



    function runAfterJQ() {
        $(document).ready(function(){
            // Delete Trigger
            $('.btn-delete').click(function(){
                // get id and links
                var id = $(this).attr('data-pid');
                var url = "https://shop.eshtri.net/rest/plusminusqtycart?order_id=<?=$transaction->id?>&order_details_id="+id+"";
                jQuery.ajax({
                    type: 'POST',
                    dataType: "json",
                    //data: {order_id: "<?//=$transaction->id?>//", order_details_id: id},
                    url: url,
                    success: function (data, textStatus) {
                        if (data.isError == false) {
                             $('#tr'+id).remove();
                             if(data.data){
                                 generateCartTotals(data.data.details, data.data);
                             }else{
                                 generateCartEmptyTable("transaction_id");
                             }

                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        // serverErrorToast(errorThrown);
                    }
                });

            });

            jQuery(".qtyminuscart").click(function(e) {

                // Stop acting like a button
                e.preventDefault();

                // Get the field name
                // fieldName = jQuery(this).attr('field');

                // Get its current value
                var currentVal = parseInt(jQuery(this).parents('.quantity').find('.itemqty').html());
                var minimumVal =  1;
                // If it isn't undefined or its greater than 0
                var qty = minimumVal;
                if (!isNaN(currentVal) && currentVal > minimumVal) {
                    // Decrement one
                    qty = currentVal - 1;
                    jQuery(this).parents('.quantity').find('.itemqty').html(qty);
                } else {
                    // Otherwise put a 0 there
                    jQuery(this).parents('.quantity').find('.itemqty').html(minimumVal);
                }
                var id = jQuery(this).attr('data-pid');
                if(qty>0) {
                    var url = "https://shop.eshtri.net/rest/plusminusqtycart?order_id=<?=$transaction->id?>&order_details_id="+id+"&qty="+qty+"&increaseQtyData=true";
                    jQuery.ajax({
                        type: 'POST',
                        dataType: "json",
                        //data: {order_id: "<?//=$transaction->id?>//", qty: qty, order_details_id: id, increaseQtyData: true},
                        url: url,
                        success: function (data, textStatus) {
                            if (data.isError == false) {
                                var itemprice=$('#price'+id).html();
                                var subtotal=parseFloat(itemprice) * parseInt(qty);
                                var subtotal=Number(subtotal).toFixed(2);
                                $('#subtotal'+id).html(subtotal);
                                generateCartTotals(data.data.details, data.data);
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            // serverErrorToast(errorThrown);
                        }
                    });
                }

            });

            jQuery('.qtypluscart').click(function(e){
                e.preventDefault();
                // Get the field name
                //fieldName = jQuery(this).attr('field');
                // Get its current value
                var currentVal = parseInt(jQuery(this).parents('.quantity').find('.itemqty').html());
                var maximumVal =  99;

                //alert(maximum);
                // If is not undefined
                if (!isNaN(currentVal)) {
                    if(maximumVal!=0){
                        if(currentVal < maximumVal ){
                            // Increment
                            var qty=currentVal + 1;
                            jQuery(this).parents('.quantity').find('.itemqty').html(qty);
                            var id = jQuery(this).attr('data-pid');
                            if(qty>0) {
                                var url = "https://shop.eshtri.net/rest/plusminusqtycart?order_id=<?=$transaction->id?>&order_details_id="+id+"&qty="+qty+"&increaseQtyData=true";
                                jQuery.ajax({
                                    type: 'POST',
                                    dataType: "json",
                                    //data: {order_id: "<?//=$transaction->id?>//", qty: qty, order_details_id: id, increaseQtyData: true},
                                    url: url,
                                    success: function (data, textStatus) {
                                        if (data.isError == false) {
                                            var itemprice=$('#price'+id).html();
                                            var subtotal=parseFloat(itemprice) * parseInt(qty);
                                            var subtotal=Number(subtotal).toFixed(2);
                                            $('#subtotal'+id).html(subtotal);
                                            generateCartTotals(data.data.details, data.data);
                                        }
                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        // serverErrorToast(errorThrown);
                                    }
                                });
                            }
                        }
                    }

                } else {
                    // Otherwise put a 0 there
                    jQuery(this).prev('.cartqty').val(0);
                }
            });
            $(document.body).on("click", "a.btn-cart", function (e) {
                var obj = {};
                var product_id = $(this).attr('data-id');
                var attribute = $(this).attr('data-check-attribute');
                var color = $(this).attr('data-check-color');
                var qty = $("#productqtydetails").val();
                // if (attribute == 1 || color == 1) {
                //     $("#productdetsil").modal('toggle');
                // }else{
                if(product_id){
                    var url = "https://shop.eshtri.net/rest/addtocart?shop_id=<?=$shop_id?>&time=<?=$carttime?>&product_id="+product_id+"&qty="+qty+"";
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        //data: {"product_id": product_id, "time":"<?//=$carttime?>//", "shop_id":"<?//=$shop_id?>//"},
                        // mode: 'cors',
                        // headers: { 'Content-Type': 'application/json'},
                        success: function (data) {
                            if (data.isError == false) {
                                generateCartTotals(data.data.details, data.data);
                            } else {
                                alert(data.message);
                            }
                        },
                        failure: function (data) {
                        }
                    })
                }
                // }
                e.preventDefault();
            });
            jQuery("#placeOrderButton, .shopping_bottom_btn").click(function () {
                var totalamount=$("#total_balance_amount").html();
                if(parseFloat(totalamount)>99){
                    <?php
                    if($this->session->userdata('current_user_id')){?>
                        var redirecturl="<?=site_url('customerlogin/shippingaddress/'.$this->session->userdata('current_user_id'))?>";
                    <?php }else{ ?>
                        var redirecturl="<?=site_url('customerlogin/login/'.$shop_id)?>";
                    <?php } ?>
                    window.location.href = redirecturl;
                }else{
                    alert("<?php echo get_msg('minimum_order_should_be_more_then_100'); ?>")
                }
            });
        });
    }
    addbtn();
    runAfterJQ();
</script>