<div class="topHeader ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4  col-2">
                <div class="topHeaderLeft">
                    <a href="<?php echo base_url('/'); ?>">
                        <i class="fa fa-repeat" aria-hidden="true"></i>  <?php echo get_msg('Cart')?></a>
                </div>
            </div>
            <div class="col-lg-4 col-6">
                <div class="topHeaderMid">
                    <span><img src="<?php echo base_url("assets/frontend/");?>img/phone.png" class="img-fluid"> <a href="https://api.whatsapp.com/send?phone=+966535686591">0535686591</a></span>
                    <span><img src="<?php echo base_url("assets/frontend/");?>img/email.png" class="img-fluid"> info@alama360.com</span>
                </div>
            </div>
            <div class="col-lg-4  col-4">
                <?php if(!$is_login_page):?>
                    <?php if ($this->session->userdata('current_user_id')): ?>
                        <div id="looged-heade-mn-area">
                            <nav class="navbar navbar-expand-lg navbar-dark" id="looged-menu-header">
                                <div class="collapse navbar-collapse show" id="main_nav">
                                    <ul class="navbar-nav">
                                        <li class="nav-item dropdown <?=$this->session->userdata('site_lang') == 'arabic'? 'dropright': 'dropleft'?>">
                                            <a class="nav-link  dropdown-toggle more-menu-logged" href="#" data-toggle="dropdown">  <?php echo get_msg('Account') ?> </a>
                                            <ul class="dropdown-menu">
                                                <a href="<?php echo base_url('dashboard'); ?>"  class="dropdown-item"><i class="fa fa-user" aria-hidden="true active"></i>  <?php echo get_msg('Dashboard') ?></a>
                                                <a href="<?php echo base_url('orderlist'); ?>"  class="dropdown-item"> <i class="fa fa-first-order" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Orders') ?></a>
                                                <a href="<?php echo base_url('address'); ?>"  class="dropdown-item"><i class="fa fa-globe" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Address') ?></a>
                                                <a href="<?php echo base_url('accountdetails'); ?>"  class="dropdown-item"><i class="fa fa-money" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Account_Details') ?></a>
                                                <a href="<?php echo base_url('messages'); ?>"  class="dropdown-item"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Messages') ?></a>
                                                <a href="<?php echo base_url('customerlogin/logout'); ?>"  class="dropdown-item"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Log_Out') ?></a>
                                            </ul>
                                        </li>
                                    </ul>
                                </div> <!-- navbar-collapse.// -->
                            </nav>
                        </div>
                    <?php else: ?>
                        <div class="jsx-96852975 logoContainer"><img src="<?php echo base_url("assets/frontend/");?>img/logo.png" class="img-fluid jsx-96852975 logo"></div>
                    <?php endif ?>
                <?php endif ?>


            </div>
        </div>
    </div>
</div>
