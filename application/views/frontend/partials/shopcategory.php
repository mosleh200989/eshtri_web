<div id="mySidepanel" class="sidepanel">
    <ul class="sp">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()" id="closebtn">×</a>
        <?php
        if (!empty($tags) && count($tags->result()) > 0): ?>
            <?php foreach ($tags->result() as $tag): ?>
                <?php $default_photo = get_default_photo($tag->id, 'tag-icon'); ?>
                <li>
                    <a href="<?php echo base_url('shoplist'); ?>">
                        <img src="https://eshtri.net/uploads/small/<?php echo $default_photo->img_path; ?>"
                             class="img-fluid my-img tag-img" alt="">&nbsp;&nbsp<?php echo getCaption($tag->name, $tag->name_alt);?>
                    </a>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>

    </ul>
</div>