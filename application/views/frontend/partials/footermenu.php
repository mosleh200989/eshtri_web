<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="fbox1">
                    <h3><?php echo get_msg('Customer_Service') ?></h3>
                    <h4><?php echo get_msg('call_Us_toll_Free') ?></h4>
                    <ul>
                        <li><img  src="<?php echo base_url("assets/frontend/");?>img/phone.png" class="img-fluid"> &nbsp;&nbsp;<a href="https://api.whatsapp.com/send?phone=+966535686591">0535686591</a></li>
                        <li><img  src="<?php echo base_url("assets/frontend/");?>img/email.png" class="img-fluid">&nbsp;&nbsp;<?=lang('cEmail')?></li>
                        <li><img  src="<?php echo base_url("assets/frontend/");?>img/location.png" class="img-fluid">&nbsp;<?=lang('cOur_Address')?></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="fbox1">
                    <h3><?php echo get_msg('Quick_Links') ?></h3>
                    <ul>
                        <li>
                            <a href="<?php echo base_url('/'); ?>" class="">
                                <?=lang('Home')?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('/areas'); ?>">
                                <?=lang('CoverageArea')?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('/aboutus'); ?>">
                                <?=lang('AboutUs')?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('/contactus'); ?>">
                                <?=lang('ContactUs')?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('/app'); ?>">
                                <?=lang('DownloadApp')?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('/privacy'); ?>">
                                <?=lang('Privacy')?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('/terms'); ?>">
                                <?=lang('Terms')?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('/aboutus'); ?>">
                                <?=lang('AboutUs')?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="fbox1">
                    <h3><?php echo get_msg('For_Business') ?></h3>
                    <ul>
                        <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo get_msg('My_Account') ?></a></li>
                        <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo get_msg('Track_Your_Order') ?></a></li>
                        <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo get_msg('Return_Exchange') ?></a></li>
                        <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo get_msg('FAQ') ?></a></li>
                        <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo get_msg('Customer_Care') ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>