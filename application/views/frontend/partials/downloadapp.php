<div class="downloadApp">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center subscriptionHeading"><?php echo get_msg('Download_Now')?></h2>
                <div class="downloadAppIcon text-center">
                    <img src="<?php echo base_url("assets/frontend/");?>img/android.png" class="img-fluid shadow">
                    <img src="<?php echo base_url("assets/frontend/");?>img/iso.png" class="img-fluid shadow">
                </div>
            </div>
        </div>
    </div>
</div>