<div class="storeOwner">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center subscriptionHeading text-white mb-4">Are you a store owner&nbsp;</h2>
                <p class="text-white text-justify subscriptionPara mb-4">Would you like to join us? And access to hundreds of thousands of users who use the Buy Buy application to order everything they need? Do you want to double your sales and increase your profits? Join us and add your store to more orders. And help us in providing the best delivery service to our customers</p>
                <button type="button" class="btn btn d-block mx-auto">Add Your Store<span></span></button>
            </div>
        </div>
    </div>
</div>