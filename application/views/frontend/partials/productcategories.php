<div class="menuWrapper">
    <div>
        <div class="topMenu vertical">
            <div>
                <div class=" hamBergerMenuIcon">
                    <div class="bar1">
                    </div>
                    <div class="bar2">
                    </div>
                    <div class="bar3">
                    </div>
                </div>
            </div>
            <div class="menu">
<!--                <ul class="misc-menu">-->
<!--                    <li class="unselected">-->
<!--                        <div class="name">-->
<!--                            <a href="#">-->
<!--	                        <span>--><?php //echo get_msg('Offers')?><!--</span>-->
<!--                                <span class="nav-count-label offers-count"><span id="offer_count"></span></span>-->
<!--                            </a>-->
<!--                        </div>-->
<!--                    </li>-->
<!--                </ul>-->
                <?php
                $active=1;
                if (!empty($popularcategory) && count($popularcategory->result()) > 0): ?>
                <ul class="hasSelection level-0">
                    <?php foreach ($popularcategory->result() as $popularcat): ?>
                        <?php $imginfo = $this->Image->get_one_by(array('img_type' => 'category', 'img_parent_id' => $popularcat->id)); ?>
                    <?php
                    $conds['shop_id'] = $popularcat->shop_id;
                    $conds['cat_id'] = $popularcat->id;
                    $subcategories = $this->Subcategory->get_all_by( $conds )->result();
                    ?>
                    <li data-cid="<?=$popularcat->id?>" class="<?=$cat_id == $popularcat->id? 'in-selection-tree selected': 'not-in-selection-tree'?> unselected topLevel ">
                        <?php if($imginfo->img_path): ?>
                        <img class="MenuItemIcons" src="https://eshtri.net/uploads/<?php echo $imginfo->img_path; ?>" >
                        <?php endif; ?>
                        <div class="name">
                            <a href="<?php echo base_url('shopproducts/category/' . $popularcat->id); ?>"><?php echo getCaption($popularcat->name, $popularcat->name_alt);?></a>
                            <?php  if(count($subcategories)>0): ?>
                                <span><span>&nbsp;</span><i class="arrow-right"></i></span>
                            <?php endif; ?>
                        </div>
                        <?php  if(count($subcategories)>0): ?>
                            <ul class="hasSelection level-1">
                                <?php foreach ($subcategories as $subcat): ?>
                                <?php $imginfo = $this->Image->get_one_by(array('img_type' => 'sub_category', 'img_parent_id' => $subcat->id)); ?>
                                <li data-cid="<?=$subcat->id?>" class="<?=$sub_cat_id == $subcat->id? 'in-selection-tree selected': 'not-in-selection-tree'?> unselected">
                                    <div class="name">
                                        <a href="<?php echo base_url('shopproducts/subcategory/' . $subcat->id); ?>"><?php echo getCaption($subcat->name, $subcat->name_alt);?></a>
                                    </div>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                    <?php
                    $active++;
                    endforeach; ?>
                    </ul>
                <?php endif; ?>
<!--                <li data-cid="2" class="in-selection-tree unselected topLevel">-->
<!--                    <img class="MenuItemIcons" src="https://cdn.chaldal.net/_mpimage/food?src=https%3A%2F%2Feggyolk.chaldal.com%2Fapi%2FPicture%2FRaw%3FpictureId%3D13890&amp;q=low&amp;v=1&amp;m=40&amp;webp=1&amp;alpha=1">-->
<!--                    <div class="name"><a href="/grocery">Food</a>-->
<!--                        <span><span>&nbsp;</span><i class="arrow-right"></i></span>-->
<!--                    </div>-->
<!--                    <ul class="hasSelection level-1">-->
<!--                        <li data-cid="7" class="not-in-selection-tree unselected">-->
<!--                            <div class="name">-->
<!--                                <a href="/fruits-vegetables">Fruits &amp; Vegetables</a>-->
<!--                                <span><span>&nbsp;</span><i class="arrow-right"></i></span>-->
<!--                            </div>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </li>-->
            </div>
        </div>
    </div>
</div>
<div class="openMenuShadowDrop">
</div>