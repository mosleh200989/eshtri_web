<div class="dashnoardNavigation">
    <div class="userDetails">
        <div class="userDetails-content">
            <i class="fa fa-user-circle-o"  aria-hidden="true"></i>
            <!--                                            <img src="./daashboard_files/user.svg" class="img-fluid" style="width: 55%;">-->
            <h4 class="userDetails-name">Lorem Ipsum</h4>
        </div>
    </div>
    <div class="dnLink">
        <ul>
            <li>
                <a href="<?php echo base_url('dashboard'); ?>" ><i class="fa fa-user" aria-hidden="true active"></i>  <?php echo get_msg('Dashboard') ?></a>
                <a href="<?php echo base_url('dashboard/orders'); ?>" > <i class="fa fa-first-order" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Orders') ?></a>
                <a href="<?php echo base_url('dashboard/addresses'); ?>" ><i class="fa fa-globe" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Address') ?></a>
                <a href="<?php echo base_url('dashboard/profile'); ?>" ><i class="fa fa-money" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Account_Details') ?></a>
                <a href="<?php echo base_url('customerlogin/logout'); ?>" ><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Log_Out') ?></a>
            </li>
        </ul>
    </div>
</div>