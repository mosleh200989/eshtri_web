<!DOCTYPE html>
<?php if ($this->session->userdata('site_lang') == 'arabic'):
$condsd['symbol'] = "ar";
$language = $this->Language->get_one_by($condsd);
if($language->id !=null){
    $this->session->set_userdata('language_code', $language->symbol);
    $this->session->set_userdata( 'user_language_id', $language->id );
}
?>
<html lang="ar" dir="rtl" class="theme-grocery needs-cover">
<?php
print '<script>var lang="ar"; var pagename="'.$page_name.'";</script>';
else:
$condsd['symbol'] = "en";
$language = $this->Language->get_one_by($condsd);
if($language->id !=null){
    $this->session->set_userdata('language_code', $language->symbol);
    $this->session->set_userdata( 'user_language_id', $language->id );
}
?>
<html lang="en" dir="ltr" class="theme-grocery needs-cover">
<?php
print '<script>var lang="en"; var pagename="'.$page_name.'";</script>';
endif ?>
<head>

    <meta name="google" content="notranslate">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <title>اطلب من متجرك المفضل اون لاين مباشرة مع موقع اشتر</title>
    <link rel="alternate" hreflang="en" href="https://eshtri.net">
    <link rel="alternate" hreflang="ar" href="https://eshtri.net">
    <link rel="alternate" hreflang="x-default" href="https://eshtri.net">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="<?php echo base_url('/files/Icon-Small.png'); ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url('/files/Icon-Small.png'); ?>" type="image/x-icon">
    <meta name="keywords" content="الأزياء، الإلكترونيات، الجمال، منتجات الأطفال والمزيد" />
    <meta property="og:type" content="website">
    <meta property="og:image" content="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-57x57.png'); ?>">
    <meta property="og:site_name" content="eShtri">
    <meta name="theme-color" content="#ffffff" data-reactroot="">
    <meta name="description" content="ما يميز اشترِ هو سهولة الاستخدام، جودة الخدمة وتعدد الخيارات للحصول على متعة تسوق خاصة و شيقة وفرنا لكم أسهل طريقة لشراء المقاضي اونلاين">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url('assets/front/matajer/images/mob_icons/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
    <meta name="apple-itunes-app" content="app-id=1512181978">
    <meta name="google-play-app" content="app-id=com.alama360.eshtri">
    <meta name="name" content="اشتر" data-html-block="meta_block">
    <meta name="image" content="<?php echo base_url('assets/front/matajer/images/Untitled-14.png'); ?>" data-html-block="meta_block">
    <meta property="og:url" content="https://eshtri.net">
    <meta property="og:type" content="">
    <meta property="og:title" content="اشتر">
    <meta property="og:description" content="اطلب من متجرك المفضل اون لاين مباشرة مع موقع اشتر">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:title" content="اشتر">
    <meta name="twitter:description" content="اطلب من متجرك المفضل اون لاين مباشرة مع موقع اشتر">
    <meta name="theme-color" content="#15355a">
    <link rel="shortcut icon" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-57x57.png'); ?>" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">
    <meta name="robots" content="index, follow">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-57x57.png'); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-60x60.png'); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-72x72.png'); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-76x76.png'); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-114x114.png'); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-120x120.png'); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-144x144.png'); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-152x152.png'); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-180x180.png'); ?>">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url('assets/front/matajer/images/mob_icons/android-icon-192x192.png'); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/front/matajer/images/mob_icons/favicon-32x32.png'); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/front/matajer/images/mob_icons/favicon-96x96.png'); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/front/matajer/images/mob_icons/favicon-16x16.png'); ?>">
    <link rel="manifest" href="<?php echo base_url('assets/front/matajer/images/mob_icons/manifest.json'); ?>">

    <!-------css---------->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" ></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/frontend/");?>css/bootstrap/ltl/bootstrap.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/frontend/");?>css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/frontend/");?>css/style2.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" >
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/frontend/");?>css/styles.css"  media="all">
</head>

<body class="webpSupported">
<!---Modal--->
<?php if(!$is_login_page):?>
<!--LogIn Modal -->
<div class="modal fade login" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title " id="exampleModalLabel">
                    <div id="login-lbl"><strong><?php echo get_msg('Log_In') ?></strong></div>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="otp-model" id="otp-lbl" style="display: none">
                <input type="hidden" name="loginredirect" id="loginredirectpopup" value="<?=$redirect_url?>">
                <span id="otp-text">
                    <?php echo get_msg('You_just_received_phone_message_containing_OTP_Code_on') ?>
                </span>
                <span id="otp-number">xxxxxxx</span>
            </div>
            <div class="modal-body">
                <form>
                    <div id="recptcha-container"></div>
                    <div id="login-fields">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="<?php echo get_msg('YourName') ?>" id="userNamepopup">
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="<?php echo get_msg('MobileNumber') ?>" id="mobileNumberpopup" maxlength="10" onkeypress="return isNumber(event)">
                        </div>
                    </div>
                    <div id="otp-fields" style="display: none">
                        <div class="input-group">
                            <input type="number" class="form-control" placeholder="<?php echo get_msg('OTP') ?>" id="otpIdpopup" maxlength="6" onkeypress="return isNumber(event)">
                        </div>
                    </div>

                    <div class="input-group">
                        <button type="button" id="loginbtnpopup" class="btn btn-success btn-block"><?php echo get_msg('Submit') ?></button>
                        <button type="button" id="verifybtnpopup" class="btn btn-success btn-block" style="display: none"><?php echo get_msg('Verify') ?></button>
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>
<?php endif?>

<div class="modal fade shortmenu" id="popMenuModal" tabindex="-1" role="dialog" aria-labelledby="popMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action active">
                        <?php echo get_msg('eShtri') ?>
                    </a>
                    <?php if(!$is_login_page):?>
                    <?php if ($this->session->userdata('current_user_id')): ?>
                        <a href="<?php echo base_url('dashboard'); ?>"  class="list-group-item list-group-item-action"><i class="fa fa-user" aria-hidden="true active"></i>  <?php echo get_msg('Dashboard') ?></a>
                        <a href="<?php echo base_url('orderlist'); ?>"  class="list-group-item list-group-item-action"> <i class="fa fa-first-order" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Orders') ?></a>
                        <a href="<?php echo base_url('address'); ?>"  class="list-group-item list-group-item-action"><i class="fa fa-globe" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Address') ?></a>
                        <a href="<?php echo base_url('accountdetails'); ?>"  class="list-group-item list-group-item-action"><i class="fa fa-money" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Account_Details') ?></a>
                        <a href="<?php echo base_url('messages'); ?>"  class="list-group-item list-group-item-action"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Messages') ?></a>
                        <?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
                            <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english" class="list-group-item list-group-item-action"><i class="fa fa-language" aria-hidden="true"></i>&nbsp;EN&nbsp;</a>
                        <?php else: ?>
                            <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/arabic" class="list-group-item list-group-item-action"><i class="fa fa-language" aria-hidden="true"></i>&nbsp;عربي&nbsp; </a>
                        <?php endif ?>
                        <a href="<?php echo base_url('customerlogin/logout'); ?>"  class="list-group-item list-group-item-action"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Log_Out') ?></a>
                    <?php else: ?>

                    <?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
                        <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english" class="list-group-item list-group-item-action"><i class="fa fa-language" aria-hidden="true"></i>EN&nbsp;</a>
                    <?php else: ?>
                        <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/arabic" class="list-group-item list-group-item-action"><i class="fa fa-language" aria-hidden="true"></i>عربي&nbsp;</a>
                    <?php endif ?>
                    <a href="#"  class="list-group-item list-group-item-action loginactionlink" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-user" aria-hidden="true active"></i>  <?=lang('Login')?></a>
                        <?php endif ?>
                        <?php endif ?>


                </div>
            </div>
        </div>
    </div>
</div>
