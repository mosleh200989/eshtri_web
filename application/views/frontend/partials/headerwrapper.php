<div class="headerWrapper" style="height:55px;">
    <div class="headerWrapperWrapper">
        <div itemscope="" itemtype="http://schema.org/WebSite" class="top-header">
            <button class="hamburgerMenu hidden-xs">
                <svg style="fill:#ffffff;stroke:#ffffff;display:inline-block;vertical-align:middle;" width="25px" height="25px" version="1.1" viewBox="0 0 100 100">
                    <path d="m12 20v8h76v-8zm0 26v8h76v-8zm0 26v8h76v-8z" data-reactid=".1664h5ftci4.4.0.0.0.0.0"></path>
                </svg>
            </button>
<!--            <div class="logo hidden-xs" data-reactid=".1664h5ftci4.4.0.0.1"><a href="/" data-reactid=".1664h5ftci4.4.0.0.1.0"><img class="egg chaldal_logo" style="background-image:url(https://cdn.chaldal.net/asset/Egg.Grocery.Fabric/Egg.Grocery.Web/1.5.0+Release-1693/Default/components/header/Header/images/logo-small.png?q=low&amp;webp=1&amp;alpha=1);background-repeat:no-repeat;" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-reactid=".1664h5ftci4.4.0.0.1.0.0"></a>-->
<!--            </div>-->

            <meta itemprop="url" content="https://eshtri.net/">
            <form itemprop="potentialAction" itemscope="" itemtype="http://schema.org/SearchAction" class="searchArea">
                <div class="searchInput">
                    <meta itemprop="target" content="https://shop.eshtri.net/rest/productlivesearch?q=12&producSearch=true&lang=ar&shop_id=">
                    <select itemprop="query-input"  class="form-control select2 selectTwo input-sm searchBox" inputmode="input" name="searchInput" id="searchInput">
                        <option ><?php echo get_msg('Search')?></option>
                    </select>
                    <button type="submit"><svg class="search" version="1.1" x="0px" y="0px" viewBox="0 0 100 100"><path d="M44.5,78.5c-18.8,0-34-15.3-34-34s15.3-34,34-34s34,15.3,34,34S63.3,78.5,44.5,78.5z M44.5,18.1  C30,18.1,18.2,30,18.2,44.5S30,70.8,44.5,70.8S70.9,59,70.9,44.5S59,18.1,44.5,18.1z" data-reactid=".1664h5ftci4.4.0.0.3.0.2.0.0"></path><path d="M87.2,91c-1,0-2-0.4-2.7-1.1L63.1,68.5c-1.5-1.5-1.5-3.9,0-5.4s3.9-1.5,5.4,0l21.3,21.3  c1.5,1.5,1.5,3.9,0,5.4C89.2,90.6,88.2,91,87.2,91z" data-reactid=".1664h5ftci4.4.0.0.3.0.2.0.1"></path></svg></button>
                </div>
            </form>

            <div class="localeSwitch area hidden-sm hidden-xs">
                <?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
                    <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english"><p class="selectedLocale">EN&nbsp;</p></a>
                <?php else: ?>
                    <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/arabic"><p class="selectedLocale">عربي&nbsp;</p> </a>
                <?php endif ?>
            </div>

            <?php if(!$is_login_page):?>
                <?php if ($this->session->userdata('current_user_id')): ?>
                    <div id="looged-heade-mn-area">
                        <nav class="navbar navbar-expand-lg navbar-dark" id="looged-menu-header">
                            <div class="collapse navbar-collapse show" id="main_nav">
                                <ul class="navbar-nav">
                                    <li class="nav-item dropdown <?=$this->session->userdata('site_lang') == 'arabic'? 'dropright': 'dropleft'?>">
                                        <a class="nav-link  dropdown-toggle more-menu-logged only-desktop" href="#" data-toggle="dropdown">  <?php echo get_msg('Account') ?> </a>
                                        <div class="dotMenuIcon" data-toggle="modal" data-target="#popMenuModal">
                                            <ul class="icons btn-left showLeft">
                                                <li></li><li></li><li></li></ul>
                                        </div>
                                        <ul class="dropdown-menu">
                                            <a href="<?php echo base_url('dashboard'); ?>"  class="dropdown-item"><i class="fa fa-user" aria-hidden="true active"></i>  <?php echo get_msg('Dashboard') ?></a>
                                            <a href="<?php echo base_url('orderlist'); ?>"  class="dropdown-item"> <i class="fa fa-first-order" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Orders') ?></a>
                                            <a href="<?php echo base_url('address'); ?>"  class="dropdown-item"><i class="fa fa-globe" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Address') ?></a>
                                            <a href="<?php echo base_url('accountdetails'); ?>"  class="dropdown-item"><i class="fa fa-money" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Account_Details') ?></a>
                                            <a href="<?php echo base_url('messages'); ?>"  class="dropdown-item"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Messages') ?></a>
                                            <a href="<?php echo base_url('customerlogin/logout'); ?>"  class="dropdown-item"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Log_Out') ?></a>
                                        </ul>
                                    </li>
                                </ul>
                            </div> <!-- navbar-collapse.// -->
                        </nav>
                    </div>
                <?php else: ?>
                    <div class="loginArea authButtons area hidden-xs" data-toggle="modal" data-target="#exampleModal"><button class="signInBtn"><?=lang('Login')?></button></div>
                <?php endif ?>
            <?php endif ?>


            <div class="mui">

            </div>
            <div class="dotMenuIcon" data-toggle="modal" data-target="#popMenuModal">
                <ul class="icons btn-left showLeft">
                    <li></li><li></li><li></li></ul>
            </div>
        </div>
        <?php $this->load->view('/frontend/partials/productcategories'); ?>
    </div>
    </div>
<div class="primary_shopping_bottom_btn shopping_bottom_btn"><button class="place_order_btn"><?php echo get_msg('Place_Order'); ?></button></div>
<script>
    function FetchData() {
        $(document).ready(function () {
            $("#searchInput").select2({
                // theme: "classic",
                // dir: "rtl",
                allowClear: true,
                placeholder: {
                    id: "",
                    placeholder: "<?php echo get_msg('Choose')?>"
                },
                language: {
                    inputTooShort: function (args) {

                        return "<?php echo get_msg('2_or_more_symbol')?>";
                    },
                    noResults: function () {
                        return "<?php echo get_msg('Not_Found')?>";
                    },
                    searching: function () {
                        return "<?php echo get_msg('Searching')?>...";
                    }
                },
                ajax: {
                    url: "https://shop.eshtri.net/rest/productlivesearch",
                    dataType: 'JSON',
                    delay: 100,
                    quietMillis: 50,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page,
                            producSearch: 'true',
                            lang:lang
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },

                placeholder: "<?php echo get_msg('product')?>...",
                allowClear: true,
                width: '100%',
                escapeMarkup: function (markup) {
                    return markup;
                },

                minimumInputLength: 2,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            $('#searchInput').on('select2:select', function (e) {
                var data = e.params.data;
                var obj = {};
                var product_id = data.id;
                var attribute = data.is_has_attribute;
                var color = data.is_has_color;
                // if (attribute == 1 || color == 1) {
                //     $("#productdetsil").modal('toggle');
                // }else{
                if(product_id){
                    var url = "https://shop.eshtri.net/rest/addtocart?shop_id=<?=$shop_id?>&time=<?=$carttime?>&product_id="+product_id+"";
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        //data: {"product_id": product_id, "time":"<?//=$carttime?>//", "shop_id":"<?//=$shop_id?>//"},
                        // mode: 'cors',
                        // headers: { 'Content-Type': 'application/json'},
                        success: function (data) {
                            if (data.isError == false) {
                                generateCartTotals(data.data.details, data.data);

                            } else {
                                alert(data.message);
                            }
                        },
                        failure: function (data) {
                        }
                    })
                }

            });


            $(document.body).on("click",".searchInput button",function(e){
                var productid = $('#searchInput').val();
                window.location.href = "<?php echo site_url('shopproducts/productdetails');?>/"+productid+"";
            });

        });
    }
    setTimeout(FetchData, 1000);

    function formatRepo(repo) {

        if (repo.loading) {
            return repo.text;
        }
        return "<div class='row slt-srs m-0'><div class='col-md-2 thumbnail sc-img p-0'><img class='sc-code lft' height='60px' width='60px'  src='https://eshtri.net/uploads/small/"+repo.thumbnail+"'/></div><div class='col-md-10 sc-con pl-3 pr-1'><span class='scsm-gp'><span class='sc-code lft'>" + repo.caption + "</span></span><span class='scsm-gp'><span class='sc-capt lft'><?php echo get_msg('price')?>: " + repo.price + "</span></span><span class='scsm-gp'><span class='sc-capt lft'><a href='#' class='btn-cart' data-check-color='" + repo.is_has_color + "' data-check-attribute='" + repo.is_has_attribute + "'  data-id='" + repo.id + "'><button type='button' class='btn btn btn-block'><i class='fa fa-shopping-bag' aria-hidden='true'></i>&nbsp;<?php echo get_msg('Add_To_Cart')?></button></a></span></span></div></div>";
    }

    function formatRepoSelection(repo) {
        if (repo.id === '') {
            return "<?php echo get_msg('product')?>...";
        }
        var imagePath='https://eshtri.net/uploads/small/'+repo.thumbnail;
        return repo.caption;
    }


</script>