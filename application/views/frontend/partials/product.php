<div class="product">
    <div class="imageWrapper">
        <div class="imageWrapperWrapper">
            <img src="https://eshtri.net/uploads/<?php echo $product->thumbnail; ?>" size="200" style="background-color:transparent;" srcset="https://eshtri.net/uploads/<?php echo $product->thumbnail; ?>" class="img-fluid-product">
        </div>
        <div class="name"><?php echo getCaption($product->name, $product->name_alt); ?>
        </div>
<!--        <div class="subText">300 gm-->
<!--        </div>-->

        <?php if($product->is_discount==1){?>
        <div class="discountedPriceSection">
            <div class="discountedPrice">
                <span><?php echo get_msg('SAR')?></span>
                <span><?php echo addvatandcomission($product->unit_price , $product->id, $product->commission_plan);?></span>
            </div>
            <div class="price">
                <span><?php echo get_msg('SAR')?></span>
                <span><?php echo addvatandcomission($product->original_price , $product->id, $product->commission_plan);?></span>
            </div>
        </div>
       <?php }else{ ?>
            <div class="price">
	            <span><?php echo get_msg('SAR')?></span>
                <span><?php echo addvatandcomission($product->unit_price , $product->id, $product->commission_plan);?></span>
            </div>
        <?php } ?>
        <div class="overlay text"><p class="addText"><?php echo get_msg('Add_to_Shopping_Cart')?></p>
            <span ><a href="<?php echo base_url('shopproducts/productdetails/' . $product->id); ?>" class="btnShowDetails">
	<span>Details
</span>
<span>  &gt;
</span></a><a href="<?php echo base_url('shopproducts/productdetails/' . $product->id); ?>" class="btnShowDetailsIcon"><svg width="24px" height="24px" style="fill:#e1e1e1;stroke:#e1e1e1;display:inline-block;vertical-align:middle;" version="1.1" viewBox="0 0 100 100" data-reactid=".1664h5ftci4.6.2.0.0.0.0.2.4.1.0:$p3125.0.5.1.1.0"><path d="m50 5c-24.898 0-45 20.102-45 45s20.102 45 45 45 45-20.102 45-45-20.102-45-45-45zm7.1016 70c0 2.1992-1.8984 4.1016-4.1016 4.1016h-6.1992c-2.1992 0-4.1016-1.8984-4.1016-4.1016v-26.199c0-2.3008 1.8984-4.1016 4.1016-4.1016h6.1992c2.1992 0 4.1016 1.8984 4.1016 4.1016zm-7.2031-37.102c-4.6016 0-8.3984-3.8008-8.3984-8.5 0-4.6992 3.8008-8.5 8.3984-8.5 4.6992 0 8.5 3.8008 8.5 8.5 0 4.7031-3.7969 8.5-8.5 8.5z" data-reactid=".1664h5ftci4.6.2.0.0.0.0.2.4.1.0:$p3125.0.5.1.1.0.0"></path></svg></a>
</span>
        </div>
    </div>
    <span><a href="<?php echo base_url('shopproducts/productdetails/' . $product->id); ?>" class="btnShowDetails">
	<span><?php echo get_msg('Details')?>
</span>
<span>  &gt;
</span></a><a href="<?php echo base_url('shopproducts/productdetails/' . $product->id); ?>" class="btnShowDetailsIcon"><svg width="24px" height="24px" style="fill:#e1e1e1;stroke:#e1e1e1;display:inline-block;vertical-align:middle;" version="1.1" viewBox="0 0 100 100" data-reactid=".1664h5ftci4.6.2.0.0.0.0.2.4.1.0:$p3125.1.1.0"><path d="m50 5c-24.898 0-45 20.102-45 45s20.102 45 45 45 45-20.102 45-45-20.102-45-45-45zm7.1016 70c0 2.1992-1.8984 4.1016-4.1016 4.1016h-6.1992c-2.1992 0-4.1016-1.8984-4.1016-4.1016v-26.199c0-2.3008 1.8984-4.1016 4.1016-4.1016h6.1992c2.1992 0 4.1016 1.8984 4.1016 4.1016zm-7.2031-37.102c-4.6016 0-8.3984-3.8008-8.3984-8.5 0-4.6992 3.8008-8.5 8.3984-8.5 4.6992 0 8.5 3.8008 8.5 8.5 0 4.7031-3.7969 8.5-8.5 8.5z" data-reactid=".1664h5ftci4.6.2.0.0.0.0.2.4.1.0:$p3125.1.1.0.0"></path></svg></a>
</span>
    <?php
    if ($product->status==1 && $product->is_available==1): ?>
        <section class="addButtonWrapper border-radius-small btn-cart" data-check-color="<?php echo $product->is_has_color; ?>" data-check-attribute="<?php echo $product->is_has_attribute; ?>"  data-id="<?php echo $product->id; ?>">
	<span class="express" id="svgIcon"><svg width="22px" height="25px" style="display:inline-block;vertical-align:middle;" version="1.1" x="0px" y="0px" viewBox="-9 11 100 100"><path class="st0" d="M59.4,32.6c0.2,0.2-0.2,0.3-0.2,0.3s-1.2,0.3-1.5,0.8c-0.3,0.3-0.8,1.5-0.8,1.5c0.7-0.2,1.5-0.3,2.3-0.7 c0.2,0,0.3,0.2,0.5,0.3c0.2,0-2,0.8-2.5,1.2c-0.3,0.3-2,1.7-2.5,1.7S53,37.9,53,37.9c0.2,0.3,0.3,0.7,0.7,1.2c0,0-1.2,0.7-1.2,0.8 c-0.2,0.2-1,1.2-1,1.2s3,0.3,4-1.3c-1,1.7-3.3,3.3-8.2,3.3c-5,0-9-2-11.7-3c-2.8-1.2-6.5-2.5-9.5-2.5c-2.8,0-6.2,0.8-8.4,2.9 c-3.9-1.5-9.2-0.7-11.7,1.7c-2.3,2.3-4.7,5.2-5,7.2c-0.5,2.2-1.7,7.4-3.3,9.7c-2.3,3.8-5,4.8-5.9,6.5c-0.8,1.7-0.8,5.2-0.7,5.2 c0.3,0.2,1.5,0,2.2-0.3c1.3-0.5,5-4.3,6.4-6.2c1.3-1.8,5.9-9.9,6.5-12.7c0.7-2.8,1.3-5.7,3.3-7.2c2.2-1.5,4.8-0.8,4.8-0.7 c0,0-2.5,2-2.7,7.5c-0.2,5.5,3.2,4.9,1.5,8.4c-1.7,3-6.7,1-8.4,2.8c-1.3,1.5,0,6.9,0,7.9c0,1-0.5,5.7-0.8,6.9c-0.2,1,0,1.7-0.2,2.3 c-0.2,0.5,1,0.7,1,0.7s-1,1.3-1.5,1c-0.5-0.2-0.8-0.2-1.2,0c-0.3,0-1,0.3-1.7,0l-2.7,5.7c0.3,0.2,0.8,0.3,1.3,0.2 c0.7-0.2,4-1.7,4.4-2c0.5-0.2,3.3-1.8,3.8-2.8c0.3-1,1-4.2,1-5.2c0.2-1,0.3-4.2,0.3-5c0-0.8-0.2-2.3,0-3c0.3-0.5,0.8-1.2,1.5-1.5 c0.8-0.2,1.8-0.2,2-0.2c0.3,0,3.2,0.3,3.7,0.3c0.7,0,3.3-0.5,4-0.8c0.8-0.2,1.3,0,1.5,0.3c0.3,0.3-0.3,0.8-0.3,1 c-0.2,0.2-1.5,1.8-1.8,2.5c-0.3,0.8-1.5,2.3-1.2,3c0.3,0.7,0.3,0.8,1.3,1.3c1,0.3,4.9,2.3,7,4.3c2.3,2.2,4.5,4.8,4.9,6.2 c0.5,1.5,1.3,1.7,1.8,1.7c0.5,0.2,1.5,0.3,1.5,1c0,0.7-0.2,1.5-0.2,1.5l6.2,2.3c0,0,0-2.2-0.2-3c-0.2-1-1.2-2-2-3 c-0.8-1.2-9.5-9.7-10.4-10.4c-0.8-0.5-1.5-1.2-1.7-2.5c-0.2-1.3,1.5-2.3,2.5-3.5c0.7-0.8,2.5-4.7,2.8-5.2c0.3-0.5,1.5-1.8,2.2-1.8 c0.8,0,2.7,1.2,5,2c1,0.3,8.2,2.2,9.2,2.5c1,0.2,3.2,0,3.8,0.7c0.7,0.5,0,1.8-0.2,2.7c-0.2,1-2,4.7-2,4.7s0.3,1.3,0.3,2.4 c0,1.2-0.5,5.3-1,6.7c-0.3,1.5-2.7,5.4-3.2,6c-0.3,0.5-0.8,0.8-0.8,1.7c0,0.8,1.5,1,2,1.7c0.7,0.5,0.2,1.5,0,2.5l6.7,2.8 c0-0.5,0.3-1.3,0-1.7c-0.2-0.5-1-1.5-1.5-2.2c-0.3-0.5-1.5-2.3-1.8-2.7c-0.3-0.5-0.3-1.5,0-2.3c0.2-0.8,2.3-6.5,2.5-7.2 c0.3-0.8,3.5-7.5,3.8-8.4c0.3-1,3-5.9,3.3-6.7c0.3-1,2.5-1.8,3.2-1.8c0.8,0,5,1.5,5.7,1.8c0.5,0.2,4.4,1.8,5,2.2 c0.8,0.3,0.3,1-0.2,1.2c-0.5,0.3-6.7,4.5-7.2,4.7c-0.5,0.2-1,0.7-1.3,0.7c-0.5,0-1.2,0.5-1.2,0.7c0,0.3-0.2,1-1,1c-0.7,0-2-1-2.8-2 c-1.2,0.7-2,1.2-2.5,2.2c-0.8,0.8-1.2,1.5-1.7,2.7c0.3,0.3,1,0.5,1.5,0.5h5.4c0.5,0,1.2-0.2,1.7-0.3c0.5-0.3,13.7-9,13.7-9 s2.2-1.7,2.2-3.2c0-1.2-1-1.5-1.3-2c-0.5-0.3-3.5-2.3-4-2.7c-0.7-0.5-3-1.8-3.7-2.2c-0.5-0.3-2.4-1-2.4-1c0.5-1.5,0.8-2.7,1-3 c0.2-0.3,0.2-3.4,0.3-3.9c0-0.5,0.8-2.7,1.2-3c0.3-0.3,1.2-3,1.3-3.5c0.2-0.3,1.5-4.2,2.2-4.2c0.7-0.2,1.3,0.5,2,0.5 c0.7,0.2,3.2-0.2,4.5,0c1.2,0.2,2.7,1.7,3,1.8c0.5,0.2,1.7,0.7,2,0.7c0.2,0,1.8-1.2,1.8-1.5c0-0.3-1.2-1-1.3-1 c-0.2-0.2-1.3-0.8-1.3-0.8c0,0.2,0.5-0.3,1.3-0.3c0.7,0,1.3,0.7,1.5,0.8c0.3,0.2,1.2,0.7,1.3,0.3c0.2-0.2,0.8-1,0.8-1.5 c0-0.7-0.7-1.5-1-1.8c-0.3-0.3-4.2-4-4.5-4.2c-0.5-0.2-2.5-2-2.5-2.5c-0.2-0.3-1.2-1.2-1.3-1.3c-0.2-0.2-5.2-3.7-5.9-4.2 c-0.7-0.5-1.3-1.3-1.3-2.2c0-0.8-0.3-3-0.3-3s-1.2-0.2-1.7,0.3c-0.5,0.3-1.7,3-2.3,3c-1.8-0.3-4.2,0.2-4.2,0.5 c0.2,0.5,1.8,0.2,1.8,0.2c0,0.2-0.7,1-1.2,1c-0.3,0-1.3,0.3-1.5,0.5c-0.3,0-1,0.5-1,0.5c0.5,0,1,0,1.7,0.2c0,0-1,0.7-1.5,0.7 c-0.5,0-2.2,0.5-2.7,0.7c-0.3,0.3-1.7,1.3-1.7,1.3h-1.5l0.2,0.3h0.8L59.4,32.6L59.4,32.6z" data-reactid=".1664h5ftci4.6.2.0.0.0.0.2.4.1.0:$p3125.2.0.0.0"></path></svg></span>
            <p class="buyText"><?php echo get_msg('Add_To_Cart')?></p>
        </section>
    <?php else: ?>
        <span class="text-center out_of_stock"><?php echo get_msg('out_of_stock')?></span>
    <?php endif; ?>
</div>