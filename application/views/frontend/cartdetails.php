

<!-- Button trigger modal -->

<!-- Modal -->
<div id="wrapper" class="cartdetails">
    <?php $this->load->view( '/frontend/partials/carttopheader'); ?>
    <div class="row">
        <div class="col-md-12">
            <!---------------shop Body------------------>
            <div class="container-fluid cartdetailsarea">
                <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="panel panel-default margin-top-lg">
                                    <div class="panel-heading text-bold">
                                        <i class="fa fa-shopping-cart margin-right-sm"></i> <?php echo get_msg('Shopping_Cart'); ?> [ <?php echo get_msg('Items'); ?>: <span id="total-items"><?=$transaction->total_item_count?></span> ]
                                        <a href="<?php echo base_url('shopproducts/shopdashboard/'.$shop_id); ?>" class="pull-right hidden-xs">
                                            <i class="fa fa-share"></i>
                                            <?php echo get_msg('Continue_Shopping'); ?>
                                         </a>
                                    </div>
                                    <div class="panel-body" style="padding:0;">
                                        <div class="cart-empty-msg hide" style="display: none;">
                                            <h4 class="text-bold">Empty Cart</h4>                                </div>
                                        <div class="cart-contents">
                                            <div class="table-responsive">
                                                <table id="cart-table" class="table table-condensed table-striped table-cart margin-bottom-no">
                                                    <thead>
                                                    <tr>
                                                        <th width="100"><?php echo get_msg('product_image'); ?></th>
                                                        <th><?php echo get_msg('product_details'); ?></th>
                                                        <th width="100"><span class="th-title"><?php echo get_msg('btn_delete')?></span></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $conds['cart_header_id'] = $transaction->id;
                                                    $all_detail =  $this->Cartdetail->get_all_by( $conds );
                                                    $rowNumber=0;
                                                    $outOfStockAmount=0;
                                                    $outOfStockAmountWithTax=0;
                                                    $no=1;
                                                    foreach($all_detail->result() as $transaction_detail):

                                                        ?>
                                                        <tr id="tr<?=$transaction_detail->id?>">
                                                            <td><?php if($transaction_detail->product_photo){?><img src="<?php echo site_url( '/'); ?>uploads/small/<?php echo $transaction_detail->product_photo; ?>" width="70" height="70"><?php } ?></td>
                                                            <td>
                                                                <?php
                                                                $att_name_info  = explode("#", $transaction_detail->product_attribute_name);
                                                                $att_price_info = explode("#", $transaction_detail->product_attribute_price);
                                                                $att_info_str = "";
                                                                $att_flag = 0;
                                                                if( $att_name_info[0]) {

                                                                    //loop attribute info
                                                                    for($k = 0; $k < count($att_name_info); $k++) {

                                                                        if($att_name_info[$k] != "") {
                                                                            $att_flag = 1;
                                                                            $att_info_str .= $att_name_info[$k] . " : " . $att_price_info[$k] . "(". $transaction->currency_symbol ."),";

                                                                        }
                                                                    }

                                                                } else {
                                                                    $att_info_str = "";
                                                                }

                                                                $att_info_str = rtrim($att_info_str, ",");


                                                                if( $att_flag == 1 ) {

                                                                    echo "<h6>".$this->Product->get_one($transaction_detail->product_id)->name .'</h6>' . $att_info_str  . '<br>' ;

                                                                } else {

                                                                    echo "<h6>".$this->Product->get_one($transaction_detail->product_id)->name . '</h6>';

                                                                }


                                                                if ($transaction_detail->product_color_id != "") {

                                                                    echo "".get_msg('color').":";
                                                                    $color_value =  $this->Color->get_one($transaction_detail->product_color_id)->color_value . '}';

                                                                }

                                                                ?>

                                                                <div style="background-color:<?php echo  $this->Color->get_one($transaction_detail->product_color_id)->color_value ; ?>; width: 20px; height: 20px; margin-top: -20px; margin-left: 50px;">
                                                                </div>

                                                                <?php
                                                                if($transaction_detail->product_measurement && $transaction_detail->product_unit){
                                                                    echo "".get_msg('product_unit')." : " . $transaction_detail->product_measurement . " " . $transaction_detail->product_unit."<br>";
                                                                }
                                                                ?>
                                                                <?php
                                                                if($transaction_detail->shipping_cost>0){
                                                                    echo "".get_msg('shipping_cost')." : " . $transaction_detail->shipping_cost ." ". $transaction->currency_symbol;} ?>

                                                                <input type="hidden" class="id" name="idDtls[<?=$rowNumber?>]" value="<?=$transaction_detail->id?>">
                                                                <input type="hidden" min="0" step="any" class="originalPrice form-control" name="originalPriceDtls[<?=$rowNumber?>]" value="<?=$transaction_detail->original_price?>">

                                                                <span><strong><?php echo get_msg('price'); ?></strong> <?php echo "<span id='price".$transaction_detail->id."'>".$transaction_detail->price."</span>".$transaction_detail->currency_symbol; ?></span><br>
                                                                <div class="input-group item-quantity">
                                                                    <span><strong><?php echo get_msg('Prd_qty'); ?></strong> </span>
                                                                    <input data-id="<?php echo $transaction_detail->id;?>"  name="qtyDtls[<?=$rowNumber?>]" value="<?=$transaction_detail->qty?>" type="number" readonly="" class="form-control cartqty" min="1" max="1593">
                                                                    <span class="input-group-btn ">
                                                        <button type="button" value="quantity" class="quantity-right-plus btn qtypluscart" data-type="plus" data-field="">
                                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                                        </button>
                                                        <button type="button" value="quantity" class="quantity-left-minus btn qtyminuscart" data-type="minus" data-field="">
                                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                                        </button>
                                                    </span>
                                                                </div>

                                                                <?php
                                                                $itemSubTotal=$transaction_detail->qty * $transaction_detail->price;
                                                                if($transaction_detail->stock_status==0){
                                                                    $outOfStockAmount +=$itemSubTotal;
                                                                    $outOfStockAmountWithTax += $itemSubTotal+ (($transaction->tax_percent / 100) * $itemSubTotal);
                                                                }


                                                                ?>
                                                                <span><strong><?php echo get_msg('sub_total'); ?>:  </strong> <?php echo "<span id='subtotal".$transaction_detail->id."' >".$itemSubTotal ."</span>". "" . $transaction->currency_symbol; ?></span><br>
                                                                <input type="hidden" class="stock form-control" name="stockDtls[<?=$rowNumber?>]" value="1">
                                                            </td>

                                                            <td>

                                                                <a herf='#' class='btn-delete btn btn-danger' id="<?php echo $transaction_detail->id;?>">
                                                                    <i style='font-size: 18px;' class='fa fa-trash-o'></i>
                                                                </a>

                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $no++;
                                                        $rowNumber++; ?>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cart-contents">
                                        <div id="cart-helper" class="panel panel-footer margin-bottom-no">
                                            <a href="<?php echo base_url('shopproducts/shopdashboard/'.$shop_id); ?>" class="btn btn-primary btn-sm pull-right">
                                                <i class="fa fa-share"></i><?php echo get_msg('Continue_Shopping'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="cart-contents">
                                    <div id="sticky-con" class="margin-top-lg">
                                        <div class="panel panel-default">
                                            <div class="panel-heading text-bold">
                                                <i class="fa fa-calculator margin-right-sm"></i> <?php echo get_msg('Cart_Totals'); ?></div>
                                            <div class="panel-body">
                                                <table class="table">
                                                    <?php
                                                    if($outOfStockAmount>0){ ?>
                                                        <tr>
                                                            <th><?php echo get_msg('out_of_stock_amount'); ?> (-):</th>
                                                            <td style="text-align: end;"><span style="color:red"><b><?php echo $outOfStockAmount . " : ".$outOfStockAmountWithTax." ".$transaction->currency_symbol; ?></b></span></td>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr style="<?=$transaction->coupon_discount_amount >0? '':'display:none'?>">
                                                        <th><?php echo get_msg('trans_coupon_discount_amount'); ?></th>
                                                        <td style="text-align: end;"><?php echo number_format($transaction->coupon_discount_amount, 2, '.', '') . " ". $transaction->currency_symbol; ?></td>
                                                    </tr>

                                                    <tr>
                                                        <th style="width:50%"><?php echo get_msg('trans_item_sub_total'); ?></th>
                                                        <td style="text-align: end;"><?php echo "<span id='sub_total'>".number_format($transaction->sub_total_amount, 2, '.', '') . "</span> ". $transaction->currency_symbol; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th><?php echo get_msg('trans_overall_tax'); ?> <?php echo "(" . $transaction->tax_percent . "%)"  ?> : (+)</th>
                                                        <td style="text-align: end;"><?php echo "<span id='total_tax'>".number_format($transaction->tax_amount, 2, '.', '') . "</span> ". $transaction->currency_symbol; ?></td>
                                                    </tr>

                                                    <tr>
                                                        <th style="width:50%"><?php echo get_msg('sub_total_with_tax'); ?></th>
                                                        <td style="text-align: end;"><span style="color:red"><b><?php echo  "<span id='sub_total_with_tax'>".(number_format($transaction->sub_total_amount + $transaction->tax_amount, 2, '.', '') )  . "</span> ". $transaction->currency_symbol; ?></b></span></td>
                                                    </tr>
                                                    <tr style="<?=$transaction->shipping_method_amount >0? '':'display:none'?>">
                                                        <th><?php echo get_msg('trans_shipping_cost'); ?>: (+)</th>
                                                        <td style="text-align: end;">
                                                            <input type="hidden" min="0" class="shippingCost form-control" name="shipping_method_amount" value="<?=$transaction->shipping_method_amount?>"><b><?php echo  $transaction->shipping_method_amount  . " ". $transaction->currency_symbol; ?></b>
                                                        </td>
                                                    </tr>

                                                    <tr style="<?=$transaction->shipping_tax_percent >0? '':'display:none'?>">
                                                        <th><?php echo get_msg('trans_shipping_tax'); ?> <?php echo "(" . $transaction->shipping_tax_percent . ")"  ?>% : (+)</th>
                                                        <td style="text-align: end;"><?php echo $transaction->shipping_method_amount * $transaction->shipping_tax_percent . " ". $transaction->currency_symbol; ?></td>
                                                    </tr>


                                                    <tr>
                                                        <th><?php echo get_msg('trans_total_balance_amount'); ?></th>
                                                        <td style="text-align: end;">
                                                            <?php
                                                            echo  "<span id='total_balance_amount'>".number_format(($transaction->sub_total_amount + ($transaction->tax_amount + $transaction->shipping_method_amount + ($transaction->shipping_method_amount * $transaction->shipping_tax_percent)) ), 2, '.', ''). "</span> ";
                                                            echo " ". $transaction->currency_symbol;
                                                            ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <a href="<?php echo base_url('cart/checkout/'.$shop_id); ?>" class="btn btn-primary btn-lg btn-block"><?php echo get_msg('Checkout'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            </div>
            <!------------------shop Body------------------>
            <div class="clear"></div>

        </div>
    </div>

    <script>
        function generateCartTable(details){
            $("#carttable tbody").html("");
            $.each(details, function(key,val) {
                if(val.product_photo){
                    var img="uploads/small/"+val.product_photo;
                }else{
                    var img="files/Icon-Small.png";
                }
                var itemSubTotal=parseFloat(val.price) * parseInt(val.qty);
                var itemSubTotal=Number(itemSubTotal).toFixed(2);
                var rowdata='<tr id="tr'+val.id+'"><td><img src="<?php echo site_url('/');?>'+img+'" width="70" height="70"></td>';
                rowdata+='<td><h6>'+val.product_name+'</h6><div style="background-color:; width: 20px; height: 20px; margin-top: -20px; margin-left: 50px;"></div><input type="hidden" class="id" name="idDtls['+key+']" value="'+val.id+'">';
                rowdata+='<input type="hidden" min="0" step="any" class="originalPrice form-control" name="originalPriceDtls['+key+']" value="'+val.price+'"><span><strong><?php echo get_msg('price'); ?></strong> <span id="price'+val.id+'">'+val.price+'</span><?php echo get_msg('SAR'); ?></span><br>';
                rowdata+='<div class="input-group item-quantity"><span><strong><?php echo get_msg('Prd_qty'); ?></strong> </span><input data-id="'+val.id+'" name="qtyDtls['+key+']" value="'+val.qty+'" type="number" readonly="" class="form-control cartqty" min="1" max="1593">';
                rowdata+='<span class="input-group-btn "><button type="button" value="quantity" class="quantity-right-plus btn qtypluscart" data-type="plus" data-field=""><i class="fa fa-plus" aria-hidden="true"></i>';
                rowdata+='</button><button type="button" value="quantity" class="quantity-left-minus btn qtyminuscart" data-type="minus" data-field=""><i class="fa fa-minus" aria-hidden="true"></i></button>';
                rowdata+='</span></div><span><strong><?php echo get_msg('sub_total'); ?> :  </strong> <span id="subtotal'+val.id+'">'+itemSubTotal+'</span></span><br><input type="hidden" class="stock form-control" name="stockDtls['+key+']" value="'+val.stock_status+'">';
                rowdata+='</td><td><a herf="#" class="btn-delete btn btn-danger" id="'+val.id+'"><i style="font-size: 18px;" class="fa fa-trash-o"></i></a></td></tr>';
                if(key==0){
                    $('#carttable tbody').append(rowdata);
                }else{
                    $('#carttable tbody tr:last').after(rowdata);
                }
            });
            runAfterJQ();
        }
        function addbtn() {
            $(document.body).on("click", "a.btn-cart", function (e) {
                var obj = {};
                obj["product_id"] = $(this).attr('data-id');
                var attribute = $(this).attr('data-check-attribute');
                var color = $(this).attr('data-check-color');
                // if (attribute == 1 || color == 1) {
                //     $("#productdetsil").modal('toggle');
                // }else{
                if(obj["product_id"]){
                    var url = "<?php echo site_url('shopproducts/addtocart');?>";
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: {postdata: obj},
                        success: function (data) {
                            if (data.isError == false) {
                                confirmNormalMessages(data.isError, data.message, false);
                                // $("#productdetsil").modal('toggle');
                                // if(data.orderdata.payment_url){
                                //     window.location.href = data.orderdata.payment_url;
                                // }
                                var sub_total_amount=data.data.sub_total_amount;
                                var sub_total_amount=Number(sub_total_amount).toFixed(2);

                                var tax_amount=data.data.tax_amount;
                                var tax_amount=Number(tax_amount).toFixed(2);

                                var sub_total_with_tax=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount);
                                var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

                                var total_balance_amount=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount);
                                var total_balance_amount=Number(total_balance_amount).toFixed(2);

                                $('#sub_total').html(sub_total_amount);
                                $('#total_tax').html(tax_amount);
                                $('#sub_total_with_tax').html(sub_total_with_tax);
                                $('#total_balance_amount').html(total_balance_amount);
                                $('#mycartbalance').html(total_balance_amount);
                                $('#finaltotal').html(total_balance_amount);
                                $('#mycartcount').html("<?php echo get_msg('My_Cart'); ?>("+data.data.total_item_count+")");
                                $('#totalqty').html(data.data.total_item_count);

                                generateCartTable(data.data.details);
                            } else {
                                alert(data.message);
                            }
                        },
                        failure: function (data) {
                        }
                    })
                }
                // }
                e.preventDefault();
            });
        }

        function runAfterJQ() {
            $(document).ready(function(){

                // Delete Trigger
                $('.btn-delete').click(function(){
                    // get id and links
                    var id = $(this).attr('id');

                    jQuery.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {order_id: "<?=$transaction->id?>", order_details_id: id},
                        url: "<?php echo site_url('/shopproducts/plusminusqtycart');?>",
                        success: function (data, textStatus) {
                            if (data.isError == false) {

                                $('#tr'+id).remove();
                                var sub_total_amount=data.data.sub_total_amount;
                                var sub_total_amount=Number(sub_total_amount).toFixed(2);

                                var tax_amount=data.data.tax_amount;
                                var tax_amount=Number(tax_amount).toFixed(2);

                                var sub_total_with_tax=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount);
                                var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

                                var total_balance_amount=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount);
                                var total_balance_amount=Number(total_balance_amount).toFixed(2);

                                $('#sub_total').html(sub_total_amount);
                                $('#total_tax').html(tax_amount);
                                $('#sub_total_with_tax').html(sub_total_with_tax);
                                $('#total_balance_amount').html(total_balance_amount);
                                $('#mycartbalance').html(total_balance_amount);
                                $('#finaltotal').html(total_balance_amount);

                                $('#mycartcount').html("<?php echo get_msg('My_Cart'); ?>("+data.data.total_item_count+")");
                                $('#totalqty').html(data.data.total_item_count);

                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            // serverErrorToast(errorThrown);
                        }
                    });

                });

                jQuery(".qtyminuscart").click(function(e) {

                    // Stop acting like a button
                    e.preventDefault();

                    // Get the field name
                    fieldName = jQuery(this).attr('field');

                    // Get its current value
                    var currentVal = parseInt(jQuery(this).parents('span').prev('.cartqty').val());
                    var minimumVal =  jQuery(this).parents('span').prev('.cartqty').attr('min');
                    // If it isn't undefined or its greater than 0
                    var qty = minimumVal;
                    if (!isNaN(currentVal) && currentVal > minimumVal) {
                        // Decrement one
                        qty = currentVal - 1;
                        jQuery(this).parents('span').prev('.cartqty').val(qty);
                    } else {
                        // Otherwise put a 0 there
                        jQuery(this).parents('span').prev('.cartqty').val(minimumVal);

                    }
                    var id = jQuery(this).parents('span').prev('.cartqty').attr('data-id');
                    if(qty>0) {
                        jQuery.ajax({
                            type: 'POST',
                            dataType: "json",
                            data: {order_id: "<?=$transaction->id?>", qty: qty, order_details_id: id, increaseQtyData: true},
                            url: "<?php echo site_url('/shopproducts/plusminusqtycart');?>",
                            success: function (data, textStatus) {
                                if (data.isError == false) {

                                    var itemprice=$('#price'+id).html();
                                    var subtotal=parseFloat(itemprice) * parseInt(qty);
                                    var subtotal=Number(subtotal).toFixed(2);

                                    $('#subtotal'+id).html(subtotal);;

                                    var sub_total_amount=data.data.sub_total_amount;
                                    var sub_total_amount=Number(sub_total_amount).toFixed(2);

                                    var tax_amount=data.data.tax_amount;
                                    var tax_amount=Number(tax_amount).toFixed(2);

                                    var sub_total_with_tax=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount);
                                    var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

                                    var total_balance_amount=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount);
                                    var total_balance_amount=Number(total_balance_amount).toFixed(2);

                                    $('#sub_total').html(sub_total_amount);
                                    $('#total_tax').html(tax_amount);
                                    $('#sub_total_with_tax').html(sub_total_with_tax);
                                    $('#total_balance_amount').html(total_balance_amount);
                                    $('#mycartbalance').html(total_balance_amount);
                                    $('#finaltotal').html(total_balance_amount);
                                    $('#mycartcount').html("<?php echo get_msg('My_Cart'); ?>("+data.data.total_item_count+")");
                                    $('#totalqty').html(data.data.total_item_count);
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                // serverErrorToast(errorThrown);
                            }
                        });
                    }

                });

                jQuery('.qtypluscart').click(function(e){
                    e.preventDefault();
                    // Get the field name
                    //fieldName = jQuery(this).attr('field');
                    // Get its current value
                    var currentVal = parseInt(jQuery(this).parents('span').prev('.cartqty').val());
                    var maximumVal =  jQuery(this).parents('span').prev('.cartqty').attr('max');

                    //alert(maximum);
                    // If is not undefined
                    if (!isNaN(currentVal)) {
                        if(maximumVal!=0){
                            if(currentVal < maximumVal ){
                                // Increment
                                var qty=currentVal + 1;
                                jQuery(this).parents('span').prev('.cartqty').val(qty);
                                var id = jQuery(this).parents('span').prev('.cartqty').attr('data-id');
                                if(qty>0) {
                                    jQuery.ajax({
                                        type: 'POST',
                                        dataType: "json",
                                        data: {order_id: "<?=$transaction->id?>", qty: qty, order_details_id: id, increaseQtyData: true},
                                        url: "<?php echo site_url('/shopproducts/plusminusqtycart');?>",
                                        success: function (data, textStatus) {
                                            if (data.isError == false) {
                                                var itemprice=$('#price'+id).html();
                                                var subtotal=parseFloat(itemprice) * parseInt(qty);
                                                var subtotal=Number(subtotal).toFixed(2);

                                                $('#subtotal'+id).html(subtotal);

                                                var sub_total_amount=data.data.sub_total_amount;
                                                var sub_total_amount=Number(sub_total_amount).toFixed(2);

                                                var tax_amount=data.data.tax_amount;
                                                var tax_amount=Number(tax_amount).toFixed(2);

                                                var sub_total_with_tax=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount);
                                                var sub_total_with_tax=Number(sub_total_with_tax).toFixed(2);

                                                var total_balance_amount=parseFloat(data.data.sub_total_amount)+parseFloat(data.data.tax_amount)+parseFloat(data.data.shipping_method_amount);
                                                var total_balance_amount=Number(total_balance_amount).toFixed(2);

                                                $('#sub_total').html(sub_total_amount);
                                                $('#total_tax').html(tax_amount);
                                                $('#sub_total_with_tax').html(sub_total_with_tax);
                                                $('#total_balance_amount').html(total_balance_amount);
                                                $('#mycartbalance').html(total_balance_amount);
                                                $('#finaltotal').html(total_balance_amount);
                                                $('#mycartcount').html("<?php echo get_msg('My_Cart'); ?>("+data.data.total_item_count+")");
                                                $('#totalqty').html(data.data.total_item_count);
                                            }
                                        },
                                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                                            // serverErrorToast(errorThrown);
                                        }
                                    });
                                }
                            }
                        }

                    } else {
                        // Otherwise put a 0 there
                        jQuery(this).prev('.cartqty').val(0);
                    }
                });

            });
        }
        addbtn();
        runAfterJQ();
    </script>



    <style>

    </style>
</div>
<!-------wrapper------->