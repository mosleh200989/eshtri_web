

<!-- Button trigger modal -->

<!-- Modal -->
<div id="wrapper" class="dashboarddetails">
    <?php $this->load->view('/frontend/partials/topheader'); ?>
    <div class="row">
        <div class="col-md-12">
            <!---------------shop Body------------------>
       <div class="container-fluid dashboard">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <!---Dashboard---->
                            <div class="dashboardSection container">
                                <div class="dashnoardNavigation side-contents">
                                    <div id="sticky-con">
                                    <div class="userDetails">
                                        <div class="userDetails-content">
                                            <i class="fa fa-user-circle-o"  aria-hidden="true"></i>
<!--                                            <img src="./daashboard_files/user.svg" class="img-fluid" style="width: 55%;">-->
                                            <h4 class="userDetails-name">Lorem Ipsum</h4>
                                        </div>
                                    </div>
                                    <div class="dnLink">
                                        <ul>
                                            <li>
                                                <a href="<?php echo base_url('dashboard'); ?>" ><i class="fa fa-user" aria-hidden="true active"></i>  <?php echo get_msg('Dashboard') ?></a>
                                                <a href="<?php echo base_url('orderlist'); ?>" > <i class="fa fa-first-order" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Orders') ?></a>
                                                <a href="<?php echo base_url('address'); ?>" ><i class="fa fa-globe" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Address') ?></a>
                                                <a href="<?php echo base_url('accountdetails'); ?>" ><i class="fa fa-money" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Account_Details') ?></a>
                                                <a href="<?php echo base_url('messages'); ?>" ><i class="fa fa-comments" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Messages') ?></a>
                                                <a href="<?php echo base_url('logout'); ?>" ><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; <?php echo get_msg('Log_Out') ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                    </div>
                                </div>
                                <div class="dashnoardDetails">
                                    <div class="dashnoardInner">
                                        <div id="img2" style="display: block;">

<!--                                            --><?php
//                                                                                  echo "<pre>";
//                                         print_r($transactiondetails);
//                                                                                  echo "</pre>";
//                                           ?>
                                            <div class="row">
                                                <div class="col-lg-12 mx-auto">
                                                    <!-- List group-->
                                                    <ul class="list-group shadow">
                                                        <!-- list group item-->

                                                        <li class="list-group-item">
                                                            <!-- Custom content-->
                                                            <div class="media align-items-lg-center flex-column flex-lg-row p-3">
                                                                <div class="media-body order-2 order-lg-1">
                                                                    <?php
                                                                    $shopName=$this->Shop->get_one_by( array( 'shop' => $transactiondetails->shop_id));
                                                                    $statusTitle=$this->Transactionstatus->get_one_by( array( 'transactionstatus' => $transactiondetails->trans_status_id));
                                                              /*      echo "<pre>";
                                                                    print_r($statusTitle);

                                                                    echo "</pre>";*/
                                                                    ?>
                                                                    <div class="card card-2">
                                                                        <div class="backBody">
                                                                            <div class="card-heade headerCard">
                                                                                <h4 class="text-white headers"><span class="fa fa-check red-text pr-3 text-danger"></span> <?php echo get_msg('Order_No')?> :  <?php echo $transactiondetails->id; ?></h4>

                                                                            </div>
                                                                            <div class="card-body bg-white inBody">

                                                                                <h5 class="mt-0 font-weight-bold mb-2"><h6><label class="allLabel"> <?php echo get_msg('Shop_Name')?> : </label> <?php echo $transactiondetails->id; ?></h6></h5>
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6> <label class="allLabel"> <?php echo get_msg('total_item_count')?> : </label> <?php echo $transactiondetails->total_item_count; ?></h6></h5>
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"> <?php echo get_msg('total_item_amount')?> : </label>  <?php echo $transactiondetails->currency_symbol; ?> <?php echo $transactiondetails->total_item_amount; ?></h6></h5>
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"> <?php echo get_msg('discount')?> : </label> <?php echo $transactiondetails->discount_amount; ?></h6></h5>
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"> <?php echo get_msg('coupon_discount')?> : </label> <?php echo $transactiondetails->coupon_discount_amount; ?></h6></h5>

                                                                            </div>

                                                                            <div class="card-body bg-white inBody">

                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"> <?php echo get_msg('Sub_Total')?> : </label>  <?php echo $transactiondetails->sub_total_amount; ?></h6></h5>
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"> <?php echo get_msg('Tax')?>(<?php echo $transactiondetails->tax_percent; ?>) : </label> <?php echo $transactiondetails->tax_amount; ?></h6></h5>
                                                                                <h5 class="mt-0 font-weight-bold mb-2 red-text-color"> <h6><label class="text-danger"><?php echo get_msg('Shipping_Cost')?></label> : <?php echo $transactiondetails->currency_symbol; ?> <?php echo $transactiondetails->shipping_amount; ?></h6></h5>
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"><?php echo get_msg('shipping_tax')?>(<?php echo $transactiondetails->shipping_tax_percent; ?>) : </label><?php echo $transactiondetails->shipping_tax_percent; ?></h6></h5>

                                                                            </div>
                                                                            <div class="card-body bg-white inBody">
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="text-danger allLabel"><?php echo get_msg('Total')?></label></label> : SAR <?php echo $transactiondetails->balance_amount; ?></h6></h5>

                                                                            </div>
                                                                            <div class="card-body bg-white inBody">
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="text-danger allLabel"><?php echo get_msg('Payment_Status')?></label> : <?php echo $statusTitle->title; ?></h6></h5>

                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <br>

                                                                    <div class="card card-2">
                                                                        <div class="backBody">
                                                                            <div class="card-heade headerCard">
                                                                                <h4 class="text-white headers"><span class="fa fa-clock-o text-danger"></span>  <?php echo get_msg('Delivery_Time')?></h4>
                                                                            </div>
                                                                            <div class="card-body bg-white inBody">
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"> <?php echo get_msg('Date')?> : </label> <?php echo $transactiondetails->delivery_date; ?></h6></h5>
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"> <?php echo get_msg('Time')?> : </label> (<?php echo $transactiondetails->delivery_time_from; ?> <?php echo $transactiondetails->delivery_time_to; ?>)</h6></h5>

                                                                            </div>
                                                                            <div class="card-body bg-white inBody">
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"><?php echo get_msg('Submitted_Date')?> :  </label><?php echo $transactiondetails->added_date; ?></h6></h5>

                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                    <br>
                                                                    <div class="card card-2">
                                                                        <div class="bg-secondary backBody">
                                                                            <div class="card-heade  headerCard">
                                                                                <h4 class="text-white headers"><span class="fa fa-shopping-cart text-danger"></span>  <?php echo get_msg('Shop')?> </h4>

                                                                            </div>
                                                                            <div class="card-body bg-white inBody">
                                                                                <div class="row">
                                                                                    <div  class="col-md-2 shopImage">
                                                                                        <?php
                                                                                        $default_photo = get_default_photo($shopName->id, 'shop');
                                                                                        if($default_photo->img_path !=null) { ?>
                                                                                            <img src="https://eshtri.net/uploads/<?php echo $default_photo->img_path;?>"  class="img-fluid">
                                                                                        <?php } else { ?>
                                                                                            <img src="<?php echo site_url( '/'); ?>files/Icon-Small.png" class="img-fluid">
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                    <div class="col-md-10  shopName">
                                                                                        <h5 class="mt-0 font-weight-bold mb-2"> <h6><?php echo get_msg('Date')?> : <?php echo $shopName->name; ?></h6></h5>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                    <div class="card card-2">
                                                                        <div class="bg-secondary backBody">
                                                                            <div class="card-heade  headerCard">
                                                                                <h4 class="text-white headers"><span class="fa fa-map-marker text-danger"></span>  <?php echo get_msg('Shipping_Address')?> </h4>

                                                                            </div>
                                                                            <div class="card-body bg-white inBody">
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"><?php echo get_msg('Phone')?> :  </label> <?php echo $transactiondetails->shipping_phone; ?></h6></h5>
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><label class="allLabel"><?php echo get_msg('Email')?>  : </label> <?php echo $transactiondetails->shipping_email; ?></h6></h5>
                                                                                <h5 class="mt-0 font-weight-bold mb-2"> <h6><?php echo $transactiondetails->shipping_address_1; ?></h6></h5>

                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                    <br>

                                                                    <table class="table table-striped">
                                                                        <thead>
                                                                        <tr>
                                                                            <th width="100"><?php echo get_msg('product_image'); ?></th>
                                                                            <th><?php echo get_msg('product_details'); ?></th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php
                                                                        $conds['transactions_header_id'] = $transaction->id;
                                                                        $all_detail =  $this->Transactiondetail->get_all_by( $conds );
                                                                        $rowNumber=0;
                                                                        $outOfStockAmount=0;
                                                                        $outOfStockAmountWithTax=0;
                                                                        $no=1;
                                                                        foreach($transactiondetails->details as $transaction_detail):

                                                                            ?>
                                                                            <tr>
                                                                                <!--                        <td width="20">--><?//=$no?><!--</td>-->
                                                                                <td><?php if($transaction_detail->product_photo){?><img src="<?php echo site_url( '/'); ?>uploads/small/<?php echo $transaction_detail->product_photo; ?>" width="100" height="100"><?php } ?></td>
                                                                                <td>
                                                                                    <?php
                                                                                    $att_name_info  = explode("#", $transaction_detail->product_attribute_name);
                                                                                    $att_price_info = explode("#", $transaction_detail->product_attribute_price);
                                                                                    $att_info_str = "";
                                                                                    $att_flag = 0;
                                                                                    if( count($att_name_info[0]) > 0 ) {

                                                                                        //loop attribute info
                                                                                        for($k = 0; $k < count($att_name_info); $k++) {

                                                                                            if($att_name_info[$k] != "") {
                                                                                                $att_flag = 1;
                                                                                                $att_info_str .= $att_name_info[$k] . " : " . $att_price_info[$k] . "(". $transaction->currency_symbol ."),";

                                                                                            }
                                                                                        }

                                                                                    } else {
                                                                                        $att_info_str = "";
                                                                                    }

                                                                                    $att_info_str = rtrim($att_info_str, ",");


                                                                                    if( $att_flag == 1 ) {

                                                                                        echo "<h6>".$this->Product->get_one($transaction_detail->product_id)->name .'</h6>' . $att_info_str  . '<br>' ;

                                                                                    } else {

                                                                                        echo "<h6>".$this->Product->get_one($transaction_detail->product_id)->name . '</h6>';

                                                                                    }


                                                                                    if ($transaction_detail->product_color_id != "") {

                                                                                        echo "".get_msg('color').":";

                                                                                        $color_value =  $this->Color->get_one($transaction_detail->product_color_id)->color_value . '}';


                                                                                    }

                                                                                    ?>

                                                                                    <div style="background-color:<?php echo  $this->Color->get_one($transaction_detail->product_color_id)->color_value ; ?>; width: 20px; height: 20px; margin-top: -20px; margin-left: 50px;">
                                                                                    </div>

                                                                                    <?php
                                                                                    if($transaction_detail->product_measurement && $transaction_detail->product_unit){
                                                                                        echo "".get_msg('product_unit')." : " . $transaction_detail->product_measurement . " " . $transaction_detail->product_unit."<br>";
                                                                                    }
                                                                                    ?>
                                                                                    <?php
                                                                                    if($transaction_detail->shipping_cost>0){
                                                                                        echo "".get_msg('shipping_cost')." : " . $transaction_detail->shipping_cost ." ". $transaction->currency_symbol;} ?>


                                                                                    <span><strong><?php echo get_msg('price'); ?></strong> <?php echo $transaction_detail->price.$transaction_detail->currency_symbol; ?></span><br>



                                                                                    <div class="input-group item-quantity">
                                                                                        <span><strong><?php echo get_msg('Prd_qty'); ?></strong> <?=$transaction_detail->qty?></span>


                                                                                    </div>

                                                                                    <?php
                                                                                    $itemSubTotal=$transaction_detail->qty * $transaction_detail->price;
                                                                                    if($transaction_detail->stock_status==0){
                                                                                        $outOfStockAmount +=$itemSubTotal;
                                                                                        $outOfStockAmountWithTax += $itemSubTotal+ (($transaction->tax_percent / 100) * $itemSubTotal);
                                                                                    }


                                                                                    ?>
                                                                                    <span><strong><?php echo get_msg('sub_total'); ?>:  </strong> <?php echo $itemSubTotal . "" . $transaction->currency_symbol; ?></span><br>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                            $no++;
                                                                            $rowNumber++; ?>
                                                                        <?php endforeach; ?>
                                                                        </tbody>
                                                                    </table>

                                                            </div> <!-- End -->

                                                        </li>
                                                    </ul> <!-- End -->
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!---Dashborad--->

                        </div>
                    </div>
                </div>
            </div>


            <!------------------shop Body------------------>
            <div class="clear"></div>

        </div>
    </div>



<style>
    .allLabel{
        font-weight: normal;
    }
    .headerCard{
        background-color: #15355C;
    }
    .inBody{
        padding: 10px;
        border-radius: 20px 20px !important;
        margin: 5px;

    }
    .backBody{
        padding: 5px;
        border-radius: 20px 20px !important;
        margin-top: 10px;
        background-color: #D9D9D9 !important;
    }
    .headers{
        padding: 10px;
    }
    .headerCard{
        border-radius: 20px 20px !important;
    }
    .dashboarddetails{
        margin-top: 50px;
    }
    #sticky-con {
        position: sticky;
        top: 105px;
        width: 100%;
    }
    .side-contents {
        top: 0;
        left: 0;
        /*width: 100%;*/
        height: 100%;
    }
    .dashnoardDetails{
        height: auto;
    }
    .shopImage{
        padding-left: 15px;
        padding-right: 26px;
    }
    .shopName{
        padding: 12px;
    }
    #img2 img{
         margin-left: 0px !important;
         margin-bottom: 0px !important;
    }
</style>
<script>


    /*const myFunction6 = () => {
        document.getElementById("img1").style.display ='block';
        document.getElementById("img2").style.display ='none'
        document.getElementById("img3").style.display ='none'
        document.getElementById("img4").style.display ='none'
        document.getElementById("img5").style.display ='none'

    }

    const myFunction7 = () => {
        document.getElementById("img2").style.display ='block'
        document.getElementById("img3").style.display ='none'
        document.getElementById("img1").style.display ='none'
        document.getElementById("img4").style.display ='none'
        document.getElementById("img5").style.display ='none'
    }

    const myFunction8 = () => {
        document.getElementById("img3").style.display ='block'
        document.getElementById("img2").style.display ='none'
        document.getElementById("img1").style.display ='none'
        document.getElementById("img4").style.display ='none'
        document.getElementById("img5").style.display ='none'
    }
    const myFunction9 = () => {
        document.getElementById("img4").style.display ='block'
        document.getElementById("img3").style.display ='none'
        document.getElementById("img2").style.display ='none'
        document.getElementById("img1").style.display ='none'
        document.getElementById("img5").style.display ='none'
    }
    const myFunction10 = () => {
        document.getElementById("img5").style.display ='block'
        document.getElementById("img4").style.display ='none'
        document.getElementById("img3").style.display ='none'
        document.getElementById("img2").style.display ='none'
        document.getElementById("img1").style.display ='none'
    }
    const myFunction11 = () => {

    }*/

</script>

</div>
<!-------wrapper------->