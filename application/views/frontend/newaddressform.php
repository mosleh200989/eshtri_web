
<!-- Button trigger modal -->

<!-- Modal -->
<div id="wrapper" class="shipping_addressdetails">
    <?php $this->load->view( '/frontend/partials/topheader'); ?>
    <div class="container" >

        <div class="row">
            <div class="col-lg-7">
                <div class="checkoutMain" id="address-form-inner">
                <div class="checkout">
                    <div class="checkoutOne">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkout_contact_information_left">
                                    <?php echo get_msg('Add_a_new_address'); ?>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="checkoutTwo">
                        <p><strong><?php echo get_msg('Personal_Information'); ?></strong></p>

                        <a href="<?php echo base_url('customerlogin/newaddressmap/'.$current_user_id); ?>" class="Change_from_map hidden-xs">
                            <i class="fa fa-pencil"></i>
                            <?php echo get_msg('Change_from_map'); ?>
                        </a>
                        <form action="<?php echo base_url('/'); ?>rest/users/add_address/api_key/teamalamathebest" enctype="multipart/form-data" method="post" accept-charset="utf-8" novalidate="novalidate" name="defaultForm" id="defaultForm">
                            <div class="row">
                                <div id="contact-alert" class="alert" style="display: none;">&nbsp;
                                </div>
                            </div>
                            <div class="input-group">
                                <input type="hidden" id="user_id" name="user_id" value="<?=$current_user_id?>">
                                <input type="hidden" id="latitute" name="latitute">
                                <input type="hidden" id="lontitude" name="lontitude">
                                <input type="hidden" id="shop_id" name="shop_id" value="<?=$selected_front_shop_id?>">
                                <input type="text" placeholder="<?php echo get_msg('Name'); ?>" class="form-control" id="name" name="receiverName" value="<?=$user->user_name?>">
                            </div>
                            <div class="input-group">
                                <input type="email" placeholder="<?php echo get_msg('Email'); ?>" class="form-control" id="email" name="email" value="<?=$user->user_email?>">
                            </div>
                            <div class="input-group">
                                <?php
                                $mobileno=str_replace("+966","",$user->user_phone);
                                $mobile=ltrim($mobileno, '0');
                                if (strlen($mobile)>11) {
                                    $mobile=str_replace("966","",$mobile);
                                }else{
                                    $mobile=$mobile;
                                }
                                ?>
                                <input type="text" placeholder="<?php echo get_msg('Phone_Number'); ?>" class="form-control" id="phone" name="mobileNo" value="0<?=$mobile?>" maxlength="10" onkeypress="return isNumber(event)">
                            </div>
                            <div class="input-group">
                                <input type="text" placeholder="<?php echo get_msg('City'); ?>" class="form-control" id="city" name="city">
                            </div>
                            <div class="input-group">
                                <textarea class="form-control" placeholder="<?php echo get_msg('address'); ?>" name="address" id="address" cols="30" rows="10"></textarea>
                            </div>
                            <div class="input-group">
                                <textarea class="form-control" placeholder="<?php echo get_msg('landmark'); ?>" name="landmark" id="landmark" cols="30" rows="10"></textarea>
                            </div>
                            <div class="form-check">

                                <label class="form-check-label" for="is_default"><input type="checkbox" style="width: auto" class="form-check-input" id="is_default" value="1"> <?php echo get_msg('is_default'); ?></label>
                            </div>
                            <!--                                    <input type="submit" class="btn btn-primary shadow" value="--><?php //echo get_msg('Save_Address'); ?><!--">-->
                            <button type="submit" class="btn btn-primary"><?php echo get_msg('Save_Address'); ?></button>
                            <!--                                    <button type="submit" class="btn btn">--><?php //echo get_msg('Save_Address'); ?><!--</button>-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!------------------shop Body------------------>
    <div class="clear"></div>

</div>

<script>
    document.getElementById("latitute").value  = localStorage.getItem("lat");
    document.getElementById("lontitude").value  = localStorage.getItem("lng");
    document.getElementById("address").value  = localStorage.getItem("formatted_address");
    document.getElementById("city").value  = localStorage.getItem("city");
    console.log(localStorage.getItem("city"));
    function valid_email(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    $("#defaultForm").submit(function(e) {

        // avoid to execute the actual submit of the form.
        var name = $('#name').val();
        var mobile = $('#phone').val();
        var email = $('#email').val();
        var address = $('#address').val();
        var city = $('#city').val();
        $("#contact-alert").hide("fast");
        var phoneno = /^\d{10}$/;
        var phonenoValidate = /^(05)([0-9]{8})$/g;
        if(!name) {
            $("#contact-alert").html('<h3 class="alert alert-danger" role="alert"><?=get_msg('please_enter_your_name')?></h3>');
            $("#contact-alert").show("slow");
            $('#name').focus();
            return false;
        }
        if(!(mobile)) {
            $("#contact-alert").html('<h3 class="alert alert-danger" role="alert"><?=get_msg('please_enter_your_mobile')?></h3>');
            $("#contact-alert").show("slow");
            $('#phone').focus();
            return false;
        }
        if(!mobile.match(phoneno)) {
            $("#contact-alert").html('<h3 class="alert alert-danger" role="alert"><?=get_msg('please_enter_correct_mobile_no')?></h3>');
            $("#contact-alert").show("slow");
            $('#email').focus();
            return false;
        }
        if(!mobile.match(phonenoValidate)) {
            $("#contact-alert").html('<h3 class="alert alert-danger" role="alert"><?=get_msg('please_enter_correct_mobile_no')?></h3>');
            $("#contact-alert").show("slow");
            $('#email').focus();
            return false;
        }
        if(!(address)) {
            $("#contact-alert").html('<h3 class="alert alert-danger" role="alert"><?=get_msg('please_enter_your_message')?></h3>');
            $("#contact-alert").show("slow");
            $('#message').focus();
            return false;
        }
        if(email && !valid_email(email)) {
            $("#contact-alert").html('<h3 class="alert alert-danger" role="alert"><?=get_msg('please_enter_valid_email')?></h3>');
            $("#contact-alert").show("slow");
            $('#email').focus();
            return false;
        }



        $("#contact-alert").hide("fast");
        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                if(data.user_id){
                    var redirecturl="<?php echo site_url('/customerlogin/shippingaddress/'.$current_user_id.'');?>";
                    window.location.href = redirecturl;
                }else{
                    alert("<?php echo get_msg('Please_select_location_from_map'); ?>");
                }
            }
        });

        e.preventDefault();
    });

</script>