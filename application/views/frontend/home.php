<div id="page">
    <div class="app catalog navOpen chaldal-theme">
        <div class="mui">
            <div class="authDialogs"></div>
        </div>
        <span >
        </span>
        <?php $this->load->view('/frontend/partials/sidebarcartdetails'); ?>
        <?php $this->load->view('/frontend/partials/headerwrapper'); ?>
        <div class="everythingElseWrapper">
            <section class="bodyTable">
                <div>
                    <div class="landingPage2">
                    <div class="mainContainer">
                        <?php
                        if (!empty($popularcategory) && count($popularcategory->result()) > 0): ?>
                            <section id="product-categories" class="categoryTiles">
                                <div class="initialLabel">
                                    <h2>
                                        <span> </span>
                                        <span><?php echo get_msg('Our_Product_Categories')?></span>
                                        <span> </span>
                                    </h2>
                                </div>
                                <div class="mainTile">
                                    <ul class="hasSelection level-0">
                                        <?php foreach ($popularcategory->result() as $popularcat): ?>
                                            <?php $imginfo = $this->Image->get_one_by(array('img_type' => 'category', 'img_parent_id' => $popularcat->id)); ?>
                                            <a href="<?php echo base_url('shopproducts/category/' . $popularcat->id); ?>" class="">
                                                <div class="categoryBox">
                                                    <div class="categoryName"><?php echo getCaption($popularcat->name, $popularcat->name_alt);?>
                                                    </div>
                                                    <div class="categoryImg">
                                                        <?php if($imginfo->img_path): ?>
                                                            <img class="MenuItemIcons" src="https://eshtri.net/uploads/<?php echo $imginfo->img_path; ?>" >
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php
                                        endforeach; ?>
                                    </ul>
                                </div>
                            </section>
                        <?php endif; ?>
                    </div>
                    </div>
                </div>
            </section>
        </div>

    </div>