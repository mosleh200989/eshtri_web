

<!-- Button trigger modal -->

<!-- Modal -->
<div id="wrapper" class="shipping_addressdetails">
    <?php $this->load->view( '/frontend/partials/topheader'); ?>
    <div class="row">
        <div class="col-md-12">
            <!---------------shop Body------------------>
            <div class="container-fluid shipping_address">
                <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="jsx-2001395417 progressIndicator threeSteps">
                            <div class="jsx-2001395417 siteWidthContainer">
                                <ul class="jsx-2001395417 steps">
                                    <li class="jsx-2001395417 step completed"><?php echo get_msg('Shipping_Address'); ?></li>
                                    <li class="jsx-2001395417 step active"><?php echo get_msg('confirmation'); ?></li>
                                    <li class="jsx-2001395417 step active"><?php echo get_msg('payment'); ?></li>
                                    <li class="jsx-2001395417 step"><?php echo get_msg('Order_Placed'); ?></li>
                                </ul>
                            </div>
                        </div>


                        <div id="content" class="jsx-3438394366 siteWidthContainer">
                            <div class="jsx-3438394366 pageWrapper">
                                <div class="jsx-1676968614 container">
                                    <div class="cart-contents">
                                        <div id="sticky-con" class="margin-top-lg">
                                            <div class="panel panel-default">
                                                <div class="panel-heading text-bold">
                                                    <i class="fa fa-calculator margin-right-sm"></i> <?php echo get_msg('Cart_Totals'); ?></div>
                                                <div class="panel-body">
                                                    <table class="table">
                                                        <tr style="<?=$transaction->total_item_count >0? '':'display:none'?>">
                                                            <th><?php echo get_msg('total_item_count'); ?></th>
                                                            <td style="text-align: end;"><?php echo $transaction->total_item_count; ?></td>
                                                        </tr>
                                                        <?php
                                                        if($outOfStockAmount>0){ ?>
                                                            <tr>
                                                                <th><?php echo get_msg('out_of_stock_amount'); ?> (-):</th>
                                                                <td style="text-align: end;"><span style="color:red"><b><?php echo $outOfStockAmount . " : ".$outOfStockAmountWithTax." ".$transaction->currency_symbol; ?></b></span></td>
                                                            </tr>
                                                        <?php } ?>
                                                        <tr style="<?=$transaction->coupon_discount_amount >0? '':'display:none'?>">
                                                            <th><?php echo get_msg('trans_coupon_discount_amount'); ?></th>
                                                            <td style="text-align: end;"><?php echo number_format($transaction->coupon_discount_amount, 2, '.', '') . " ". $transaction->currency_symbol; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <th style="width:50%"><?php echo get_msg('trans_item_sub_total'); ?></th>
                                                            <td style="text-align: end;"><?php echo "<span id='sub_total'>".number_format($transaction->sub_total_amount, 2, '.', '') . "</span> ". $transaction->currency_symbol; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th><?php echo get_msg('trans_overall_tax'); ?> <?php echo "(" . $transaction->tax_percent . "%)"  ?> : (+)</th>
                                                            <td style="text-align: end;"><?php echo "<span id='total_tax'>".number_format($transaction->tax_amount, 2, '.', '') . "</span> ". $transaction->currency_symbol; ?></td>
                                                        </tr>

                                                        <tr>
                                                            <th style="width:50%"><?php echo get_msg('sub_total_with_tax'); ?></th>
                                                            <td style="text-align: end;"><span style="color:red"><b><?php echo  "<span id='sub_total_with_tax'>".(number_format($transaction->sub_total_amount + $transaction->tax_amount, 2, '.', '') )  . "</span> ". $transaction->currency_symbol; ?></b></span></td>
                                                        </tr>
                                                        <tr style="<?=$transaction->shipping_method_amount >0? '':'display:none'?>">
                                                            <th><?php echo get_msg('trans_shipping_cost'); ?>: (+)</th>
                                                            <td style="text-align: end;">
                                                                <input type="hidden" min="0" class="shippingCost form-control" name="shipping_method_amount" value="<?=$transaction->shipping_method_amount?>"><b><?php echo  $transaction->shipping_method_amount  . " ". $transaction->currency_symbol; ?></b>
                                                            </td>
                                                        </tr>

                                                        <tr style="<?=$transaction->shipping_tax_percent >0? '':'display:none'?>">
                                                            <th><?php echo get_msg('trans_shipping_tax'); ?> <?php echo "(" . $transaction->shipping_tax_percent . ")"  ?>% : (+)</th>
                                                            <td style="text-align: end;"><?php echo $transaction->shipping_method_amount * $transaction->shipping_tax_percent . " ". $transaction->currency_symbol; ?></td>
                                                        </tr>


                                                        <tr>
                                                            <th><?php echo get_msg('trans_total_balance_amount'); ?></th>
                                                            <td style="text-align: end;">
                                                                <?php
                                                                echo  "<span id='total_balance_amount'>".number_format(($transaction->sub_total_amount + ($transaction->tax_amount + $transaction->shipping_method_amount + ($transaction->shipping_method_amount * $transaction->shipping_tax_percent)) ), 2, '.', ''). "</span> ";
                                                                echo " ". $transaction->currency_symbol;
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <a href="<?php echo base_url('checkout/checkout_payment/'.$transaction->id); ?>" class="btn btn-primary btn-lg btn-block"><?php echo get_msg('confirm'); ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>





                    </div>
                </div>
            </div>
            </div>
            <!------------------shop Body------------------>
            <div class="clear"></div>

        </div>
    </div>

</div>
<!-------wrapper------->

<style>
    button {
        background-color: transparent;
        cursor: pointer;
    }
    label.jsx-1676968614{
        cursor: pointer;
    }
    .shipping_address{
        margin-top: 65px;
    }
    .progressIndicator.jsx-2001395417 {
        padding: 18px 0px 15px;
        width: 100%;
        background-color: rgb(255, 255, 255);
    }
    .siteWidthContainer {
        padding: 0 15px;
        margin: 0 auto;
        max-width: 1400px;
        box-sizing: border-box;
    }
    ul.steps.jsx-2001395417 {
        display: flex;
        counter-reset: progressCount 0;
        -webkit-box-pack: center;
        justify-content: center;
    }
    li.step.jsx-2001395417 {
        list-style: none;
        display: inline-block;
        position: relative;
        text-align: center;
        font-size: 0.75rem;
        color: rgb(126, 133, 155);
        z-index: 0;
        transition: all 300ms ease-in-out 0s;
    }
    li.step.active.jsx-2001395417 {
        color: rgb(64, 69, 83);
        font-weight: bold;
    }
    .progressIndicator.threeSteps.jsx-2001395417 li.step.jsx-2001395417 {
        flex: 0 1 33.33%;
    }
    li.step.jsx-2001395417::before {
        content: counter(progressCount);
        counter-increment: progressCount 1;
        background-color: rgb(255, 255, 255);
        border-radius: 100%;
        display: block;
        text-align: center;
        margin: 0px auto 5px;
    }
    li.step.jsx-2001395417::before {
        width: 1.667rem;
        height: 1.667rem;
        line-height: 1.667rem;
        border: 0.083rem solid rgb(226, 229, 241);
        padding: 0.103rem;
    }
    li.step.active.jsx-2001395417::before, li.step.completed.jsx-2001395417::before {
        width: 1.667rem;
        height: 1.667rem;
        line-height: 1.667rem;
        border: 0.186rem solid rgb(255, 255, 255);
        padding: 0px;
    }
    li.step.active.jsx-2001395417::before {
        color: rgb(255, 255, 255);
        background-color: rgb(56, 174, 4);
    }
    li.step.jsx-2001395417::after {
        content: "";
        position: absolute;
        width: 100%;
        height: 0.33rem;
        top: 0.85rem;
        background-color: rgb(226, 229, 241);
        border-radius: 5px;
        right: -50%;
        z-index: -1;
    }
    .progressIndicator.threeSteps.jsx-2001395417 li.step.jsx-2001395417:last-child, .progressIndicator.fourSteps.jsx-2001395417 li.step.jsx-2001395417:last-child {
        flex: 1 0 10%;
    }
    li.step.completed.jsx-2001395417::before {
        font-size: 0.8rem;
        color: rgb(255, 255, 255);
        background-color: rgb(56, 174, 4);
    }
    li.step.active.jsx-2001395417::before, li.step.completed.jsx-2001395417::before {
        width: 1.667rem;
        height: 1.667rem;
        line-height: 1.667rem;
        border: 0.186rem solid rgb(255, 255, 255);
        padding: 0px;
    }
    li.step.completed.jsx-2001395417::after {
        background-color: rgb(56, 174, 4);
    }
    @media only screen and (min-width: 768px){
        .siteWidthContainer {
            padding: 0 30px;
        }
        li.step.jsx-2001395417 {
            display: flex;
            white-space: nowrap;
            -webkit-box-align: baseline;
            align-items: baseline;
            font-size: 1rem;
        }
        li.step.jsx-2001395417::before {
            flex: 0 0 1.667rem;
            display: inline-block;
            margin: 0px 5px 5px;
        }
        li.step.active.jsx-2001395417::before {
            margin: 0px 5px 5px;
        }
        li.step.jsx-2001395417::after {
            flex: 1 1 60%;
            display: inline-block;
            position: relative;
            top: unset;
            right: unset;
            margin: 0px 8px 0px 12px;
        }
        .progressIndicator.threeSteps.jsx-2001395417 li.step.jsx-2001395417:last-child, .progressIndicator.fourSteps.jsx-2001395417 li.step.jsx-2001395417:last-child {
            flex: 0 0 10%;
        }
        li.step.completed.jsx-2001395417::after {
            background-color: rgb(226, 229, 241);
        }
    }
    @media only screen and (min-width: 992px) {
        .siteWidthContainer {
            padding: 0 45px;
        }
    }

 /*address css start*/

    .siteWidthContainer {
        padding: 0 15px;
        margin: 0 auto;
        max-width: 1400px;
        box-sizing: border-box;
    }
    #content.jsx-3438394366 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        width: 100%;
    }
    .container.jsx-1676968614 {
        padding: 20px 0px;
    }

</style>

