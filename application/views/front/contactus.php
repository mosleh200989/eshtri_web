<?php if ($this->session->userdata('site_lang') == 'arabic'): 
    $condsd['symbol'] = "ar";
    $language = $this->Language->get_one_by($condsd);
    if($language->id !=null){
    $this->session->set_userdata('language_code', $language->symbol);
    $this->session->set_userdata( 'user_language_id', $language->id );
    }  
    ?>
<html lang="ar">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="ar" data-lang="ar" class="contact">
            <?php 
        else: 
                $condsd['symbol'] = "en";
                $language = $this->Language->get_one_by($condsd);
                if($language->id !=null){
                $this->session->set_userdata('language_code', $language->symbol);
                $this->session->set_userdata( 'user_language_id', $language->id );
                }  
                ?>
<html lang="en">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="en" data-lang="en" class="contact">
            <?php endif ?>
<?php $this->load->view('front/includes/header-menu'); ?>
<div class="page-content">
    <div class="wrapper">
        <div class="page-banner page-banner-2 page-banner-home" style="background-image: url(<?php echo base_url('assets/front/matajer/images/3322.jpg'); ?>) !important">
            <div class="banner-slogan">
                <div class="container z-titles-2" id="z-titles">
                    <div class="z-title-2">
                        <?=lang('cContactUs')?>
                    </div>
                    <div class="z-title-3">
                        <?=lang('cCustomer_service')?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="pure-form pure-form-stacked">
                <div class="contact-us-block row">
                    <form action="<?php echo base_url('/'); ?>rest/contacts/add/api_key/teamalamathebest" enctype="multipart/form-data" method="post" accept-charset="utf-8" novalidate="novalidate" id="contact-form" style="margin:0">
                    <div class="col-md-8" id="x-right-block">
                        <div class="row">
                            <div id="contact-alert" class="alert" style="display: none;">&nbsp;
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="x-custom-label">
                                    <?=lang('cName')?>
                                </label>
                                <input type="text" id="name" name="name" class="form-control" placeholder=" <?=lang('cName_placeholder')?>">
                            </div>
                            <div class="col-md-6">
                                <label class="x-custom-label">
                                    <?=lang('ceMobile')?>
                                </label>
                                <input type="tel" id="phone" name="phone" class="form-control" placeholder="<?=lang('ceMobile_placeholder')?>">
                            </div>
                        </div>
                        <div class="clearfix">
                            <label class="x-custom-label">
                                <?=lang('ceEmail')?>
                            </label>
                            <input type="email" id="email" name="email" class="form-control" placeholder="<?=lang('ceEmail_placeholder')?>">
                        </div>
                        <div class="clearfix">
                            <label class="x-custom-label">
                                <?=lang('cMessage')?>
                            </label>
                            <textarea class="form-control" id="message" name="message" rows="4" placeholder="<?=lang('cMessage_placeholder')?>"></textarea>
                        </div>
                        <div class="clearfix">
                            <button class="x-custom-button btn-send">
                                <i class="fa fa-send"></i>
                            </button>
                        </div>
                    </div>
                    </form>
                    <div class="col-md-4" id="x-left-block">
                        <h2>
                            <?=lang('cContact_Information')?>
                        </h2>
                        <ul>
                            <li>
                                <i class="fa fa-map-marker"></i>
                                
                                    <?=lang('cOur_Address')?>
                                
                            </li>
                            <li>
                                <i class="fa fa-phone"></i>
                                <span style="direction: ltr" class="x-number">
                                    <?=lang('cMobile')?>
                                </span>
                            </li>
                            <li>
                                <i class="fa fa-at"></i>
                                <?=lang('cEmail')?>
                            </li>
                        </ul>
                    </div>
                </div>
            
        </div>
<?php $this->load->view('front/includes/appslink'); ?>
    </div>
    <script type="text/javascript" src="<?php echo base_url('assets/jquery/jquery.min.js'); ?>"></script>
    <script>
        // Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
// MIT License. See license.txt
function valid_email(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
$(function() {
   $("#contact-form").submit(function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.
        var name = $('#name').val();
        var mobile = $('#phone').val();
        var email = $('#email').val();
        var message = $('#message').val();
        $("#contact-alert").hide("fast");
       var phoneno = /^\d{10}$/;
       var phonenoValidate = /^(05)([0-9]{8})$/g;

        if(!name) {
            $("#contact-alert").html('<h3 class="alert alert-danger" role="alert"><?=get_msg('please_enter_your_name')?></h3>');
            $("#contact-alert").show("slow");
            $('#name').focus();
            return false;
        }
        if(!(mobile)) {
            $("#contact-alert").html('<h3 class="alert alert-danger" role="alert"><?=get_msg('please_enter_your_mobile')?></h3>');
            $("#contact-alert").show("slow");
            $('#phone').focus();
            return false;
        }
       if(!(mobile)) {
           $("#contact-alert").html('<h3 class="alert alert-danger" role="alert"><?=get_msg('please_enter_your_mobile')?></h3>');
           $("#contact-alert").show("slow");
           $('#phone').focus();
           return false;
       }
        if(!mobile.match(phoneno)) {
            $("#contact-alert").html('<h3 class="alert alert-danger" role="alert"><?=get_msg('please_enter_correct_mobile_no')?></h3>');
            $("#contact-alert").show("slow");
            $('#email').focus();
            return false;
        }
        if(!mobile.match(phonenoValidate)) {
            $("#contact-alert").html('<h3 class="alert alert-danger" role="alert"><?=get_msg('please_enter_correct_mobile_no')?></h3>');
            $("#contact-alert").show("slow");
            $('#email').focus();
            return false;
        }
        if(!(message)) {
           $("#contact-alert").html('<h3 class="alert alert-danger" role="alert"><?=get_msg('please_enter_your_message')?></h3>');
           $("#contact-alert").show("slow");
            $('#message').focus();
            return false;
        }
        if(!valid_email(email)) {
            $("#contact-alert").html('<h3 class="alert alert-danger" role="alert"><?=get_msg('please_enter_valid_email')?></h3>');
            $("#contact-alert").show("slow");
            $('#email').focus();
            return false;
        }

        

    $("#contact-alert").hide("fast");
    var form = $(this);
    var url = form.attr('action');
    
    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
            if(data.message == "success_contact")
                
             $("#contact-alert").html('<h3 class="alert alert-success" role="alert"><?=get_msg('success_contact')?></h3>');
            $("#contact-alert").show("slow");
            //Thank you for your message
               $('#contact-form').trigger("reset"); // show response from the php script.
           }
         });

    
});
});

frappe.ready(function() {

    $('.btn-send').off("click").on("click", function() {

        var name = $('[name="full_name"]').val();
        var mobile = $('[name="mobile"]').val();
        var email = $('[name="email"]').val();
        var message = $('[name="message"]').val();

        if(!(name)) {
            console.log(name);
            msgprint(__("Please enter your name so that we \
                can get back to you. Thanks!"));
            $('[name="name"]').focus();
            return false;
        }
        if(!(mobile)) {
            msgprint(__("Please enter your mobile so that we \
                can get back to you. Thanks!"));
            $('[name="mobile"]').focus();
            return false;
        }
        if(!(email)) {
            msgprint(__("Please enter your email so that we \
                can get back to you. Thanks!"));
            $('[name="email"]').focus();
            return false;
        }
        if(!valid_email(email)) {
            msgprint(__("You seem to have written your name instead of your email. \
                Please enter a valid email address so that we can get back."));
            $('[name="email"]').focus();
            return false;
        }

        if(!(message)) {
            msgprint(__("Please enter your message so that we \
                can get back to you. Thanks!"));
            $('[name="message"]').focus();
            return false;
        }

        $("#contact-alert").toggle(false);

        frappe.call({
            type: "POST",
            method: "matajer.templates.pages.main.send_message",
            btn: this,
            args: {
                subject: $('[name="subject"]').val(),
                sender: email,
                name:name,
                mobile:mobile,
                message: message
            },
            callback: function(r) {
                if(r.message==="okay") {
                    msgprint(__("Thank you for your message"), "success");
                } else {
                    msgprint(__("There were errors"));
                    console.log(r.exc);
                }
                $(':input').val('');
            }
        });

    return false;
    });

});

var msgprint = function(txt, type) {
    type = type || "warning";
    console.warn(txt);
    if(txt) {
        var contact_alert = $("#contact-alert");
        contact_alert.removeClass("alert-warning alert-success");
        contact_alert.addClass("alert-" + type).html(txt).toggle(true);
    }
};
    </script>
    </div>

    </div>

    <!-- footer -->
    <?php $this->load->view('front/includes/footer'); ?>
</body></html>
