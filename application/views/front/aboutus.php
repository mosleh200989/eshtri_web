<?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
<html lang="ar">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="ar" data-lang="ar">
            <?php else: ?>
<html lang="en">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="en" data-lang="en">
            <?php endif ?>
<?php $this->load->view('front/includes/header-menu'); ?>
<div class="page-content">

<div class="wrapper">
        <div class="page-banner page-banner-2 page-banner-home">
            <div class="banner-slogan">
                <div class="container" id="z-titles">
                    <div class="z-title-1">
                        <?=lang('WeCreated')?>
                    </div>
                    <div class="z-title-2">
					    <?=lang('UniqueExp')?>
                    </div>
                    <div class="z-title-3">
                       <?=lang('onlineshop')?>
                    </div>

                </div>
            </div>
            <div class="z-page-navs">
                <ul>
                    <li class="active">
                        <a href="#idea">
                            <span class="z-icon">
                                <img src="<?php echo base_url('assets/front/matajer/images/icons/1.png');?>">
                            </span>
                            <span class="z-description">
                                <?=lang('Idea')?>                               
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#team">
                            <span class="z-icon">
                                <img src="<?php echo base_url('assets/front/matajer/images/icons/2.png');?>">
                            </span>
                            <span class="z-description">
                                <?=lang('Team')?>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#experience">
                            <span class="z-icon">
                                <img src="<?php echo base_url('assets/front/matajer/images/icons/3.png');?>">
                            </span>
                            <span class="z-description">
                                <?=lang('Experience')?>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>


        <div class="page-section" style="padding-bottom: 0">
            <div class="container">
                <div class="section-row row" id="idea">
     <h1 class="section-title">من نحن</h1>
<p>تأسست مؤسسة عبدالحكيم علي عبدالحكيم بخاري للتجارة برقم تجاري 4030339937 في عام 2015 بدأنا بتنفيذ الخطط والعمل عليها بخطوات تابثة. </p>
<p>دخلنا سوق العمل وبدأنا بمجال التجاري حيث تميزنا بثقة العملاء بنا عن طريق تقديم أرقى وأكفا الخدمات لهم . </p>

<p>انتقلنا إلى خطوتنا التالية حيث تكمن في تطوير احدث التقنيات الفعالة ذات الاعتمادية العالية وهيا الأنظمة الأولية لتقنية الذكاء الاصطناعي للإدارة والتشغيل في مجال تجارة وقد تم ذلك بفضل من الله وكرمه بكفاءة عالية وإمكانيات قوية بطريقة احترافية فنحن في اشترِ نطمح لِنُكون خطاً بيانيا صاعدا من الثقة اللامحدودة بعد تحقيق هذه النجاحات الممتدة منذ تأسيس المؤسسة. </p>
<h1 class="section-title">الرؤية</h1>
<p>أن نكون من أهم المنصات التجارية على مستوى العالم . </p>
<h1 class="section-title">الرسالة</h1>
<p>نسعى لأن يكون لنا دور وبصمة واضحة ونجاح باهر في تقديم الأفضل لعملائنا </p>


                    <div class="col-sm-6 visible-xs">
                        <div class="section-image">
                            <img src="<?php echo base_url('assets/front/matajer/images/icons/4.png');?>" class="wow fadeIn">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <h1 class="section-title">
                            <?=lang('Idea')?> 
                        </h1>
                        <p class="section-desc">
                            <?=lang('eshtridesc')?> 
                        </p>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <div class="section-image">
                            <img src="<?php echo base_url('assets/front/matajer/images/icons/4.png');?>" class="wow fadeIn img1" style="width: 70% !important">
                        </div>
                    </div>
                </div>

                <div class="section-row row" id="experience">
                    <div class="col-sm-6">
                        <div class="section-image">
                            <img src="<?php echo base_url('assets/front/matajer/images/icons/5.png');?>" class="wow fadeIn img1">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <h1 class="section-title ">
                            <?=lang('Experience')?>
                        </h1>
                        <p class="section-desc">
                            <?=lang('eshtriexp')?>
                        </p>
                    </div>
                </div>

                <div id="team" class="section-row row" style="padding-bottom: 0; border-bottom: 0">
                    <div class="col-md-12 col-sm-12 m-t-60">
                        <h1 class="section-title text-center">
                            <?=lang('Team')?>
                        </h1>
                        <p class="section-desc">
                            <?=lang('eshtriteam')?>
                        </p>
                    </div>
                </div>

            </div>
            <img style="width: 100%" src="<?php echo base_url('assets/front/matajer/images/icons/Untitled-13.png');?>">
        </div>

<?php $this->load->view('front/includes/appslink'); ?>
    </div>
    </div>

    <!-- footer -->
    <?php $this->load->view('front/includes/footer'); ?>
</body></html>
