
<?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
<html lang="ar">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="ar" data-lang="ar">
            <?php else: ?>
<html lang="en">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="en" data-lang="en">
            <?php endif ?>

<?php $this->load->view('front/includes/header-menu'); ?>
<div class="page-content">
<div class="wrapper">
    <div class="page-banner page-banner-home">
        <div class="banner-slogan">
            <div class="container">
                <h1 class="slogan-title wow fadeIn">
                    <?=lang('headtitle')?>
                </h1>
                <h2 class="slogan-desc wow fadeIn">
                    <?=lang('suptitle')?>
                </h2>
                <div class="text-center">
                    <a class="btn x-header-btn visible-xs" href="#download-apps-now">
                        <i class="fa fa-download"></i>
                        <?=lang('hDownload_App')?>
                    </a>
                </div>
            </div>
        </div>
        <div id="shop-now">
            <a class="btn btn-outline btn-light" href="#">Start Shopping</a>
            
        </div>
        <!-- /.banner-slogan -->
        <img src="<?php echo base_url('assets/front/matajer/images/9-en.png'); ?>" class="iphone-image wow fadeIn">
    </div>
    <section class="page-section section-center v-padding-40">
        <div class="container">
            <h2 class="section-title">
                <?=lang('Features')?>
            </h2>
            <p class="section-desc">
                <?=lang('Featureline')?>

            </p>
            <div class="features features-icon-top features-center">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="feature">
                            <img class="wow fadeIn" src="<?php echo base_url('assets/front/matajer/images/1.png'); ?>">
                            <h4>
                                <?=lang('Allproducts')?>
                            </h4>
                            <div>
                                <?=lang('Allproductsdesc')?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="feature">
                            <img class="wow fadeIn" src="<?php echo base_url('assets/front/matajer/images/Untitled-6.png'); ?>">
                            <h4>
                                <?=lang('MethodPaying')?>
                            </h4>
                            <div>
                                <?=lang('MethodPayingdesc')?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="feature">
                            <img class="wow fadeIn" src="<?php echo base_url('assets/front/matajer/images/2.png'); ?>">
                            <h4>
                                <?=lang('DeliveryPlace')?>
                            </h4>
                            <div>
                                <?=lang('DeliveryPlacedesc')?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="feature">
                            <img class="wow fadeIn" src="<?php echo base_url('assets/front/matajer/images/5.png'); ?>">
                            <h4>
                                <?=lang('SchedulOrder')?>
                            </h4>
                            <div>
                                <?=lang('SchedulOrderdesc')?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="feature">
                            <img class="wow fadeIn" src="<?php echo base_url('assets/front/matajer/images/3.png'); ?>">
                            <h4>
                                <?=lang('SupportShops')?>
                            </h4>
                            <div>
                                <?=lang('SupportShopsdesc')?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="feature">
                            <img class="wow fadeIn" src="<?php echo base_url('assets/front/matajer/images/4.png'); ?>">
                            <h4>
                                <?=lang('TechnicalSup')?>
                            </h4>
                            <div>
                                <?=lang('TechnicalSupdesc')?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="feature">
                            <img class="wow fadeIn" src="<?php echo base_url('assets/front/matajer/images/6.png'); ?>">
                            <h4>
                                <?=lang('returnPolicy')?>
                            </h4>
                            <div>
                                <?=lang('returnPolicydesc')?>
                                <a href="<?php echo base_url('/policies'); ?>"><?=lang('more')?></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="feature">
                            <img class="wow fadeIn" src="<?php echo base_url('assets/front/matajer/images/7.png'); ?>">
                            <h4>
                                <?=lang('cancelOrder')?>
                            </h4>
                            <div>
                                <?=lang('cancelOrderdesc')?>
                                <a href="<?php echo base_url('/policies'); ?>"><?=lang('more')?></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="feature">
                            <img class="wow fadeIn" src="<?php echo base_url('assets/front/matajer/images/7-1.png'); ?>">
                            <h4>
                                <?=lang('shipmentPolicy')?>
                            </h4>
                            <div>
                                <?=lang('shipmentPolicydesc')?>
                                <a href="<?php echo base_url('/policies'); ?>"><?=lang('more')?></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <div class="page-section">
        <div class="container">
            <div class="section-row row">
                <div class="col-sm-6">
                    <div class="section-image">
                        <img src="<?php echo base_url('assets/front/matajer/images/8.png'); ?>" class="wow fadeIn">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 m-t-60">
                    <h2 class="section-title">
                        <?=lang('Hshop_method')?>
                    </h2>
                    <p class="section-desc">
                        <?=lang('Eshtri_Direct')?>
                    </p>
                    <a href="#download-apps-now" class="btn btn-outline btn-light center-xs">
                        <i class="fa fa-download"></i>
                        <?=lang('hDownload_App')?>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 visible-xs">
                    <div class="section-image">
                        <img src="<?php echo base_url('assets/front/matajer/images/Untitled-13-en.png'); ?>" class="wow fadeIn">
                    </div>
                    <!-- /.section-image -->
                </div>
                <div class="col-md-5 col-sm-5 m-t-60 col-md-push-1">
                    <h2 class="section-title">
                        <?=lang('SoonTM')?>
                    </h2>
                    <p class="section-desc">
                        <?=lang('SoonTMdesc')?>
                    </p>
                    <a href="<?php echo base_url('areas'); ?>" class="btn btn-outline btn-light center-xs">
                        <i class="fa fa-arrow-left"></i>
                        <?=lang('HSupported_loc')?>
                    </a>
                </div>
                <div class="col-sm-6 col-md-push-1 hidden-xs">
                    <div class="section-image ">
                        <img src="<?php echo base_url('assets/front/matajer/images/Untitled-13-en.png'); ?>" class="wow fadeIn">
                    </div>
                    <!-- /.section-image -->
                </div>
            </div>
        </div>
    </div>
    <section class="page-section section-center v-padding-40 back11">
        <h2 class="section-title">
            <?=lang('HApp_Screen')?>

        </h2>
        <div class="all-images">
            <img src="<?php echo base_url('assets/front/matajer/images/Untitled-14-en.png'); ?>" class="wow fadeIn">
        </div>
    </section>
    <!-- amer -->
    <div class="page-section add-buisness">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 m-t-60 ">
                    <h2 class="section-title section-center add-buisness-title">
                       <?=lang('Hshopkeeper')?>
                    </h2>
                    <p class="section-desc text-center">
                        <?=lang('marketingur')?>
                    </p>
                    <a href="<?php echo base_url('partnership'); ?>" class="btn add-buiness-btn center-xs">
                        <i class="fa fa-arrow-left"></i>
                        <?=lang('Add_YourStore')?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('front/includes/appslink'); ?>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 0">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe style="width: 100%; height: 400px;" src="https://www.youtube.com/embed/XjwjzsT_Jh8" frameborder="0" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </div>
</div>


	</div> 

    <!-- footer -->
    <?php $this->load->view('front/includes/footer'); ?>
</body></html>
