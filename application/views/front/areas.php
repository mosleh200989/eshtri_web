<?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
<html lang="ar">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="ar" data-lang="ar">
            <?php else: ?>
<html lang="en">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="en" data-lang="en">
            <?php endif ?>
<?php $this->load->view('front/includes/header-menu'); ?>
<div class="page-content">

<div class="wrapper">
        <div class="page-banner page-banner-3 page-banner-home">
            <div class="banner-slogan">
                <div class="container" style="margin-top: 70px">
                    <h1 class="lg-text wow fadeIn">
                        <?=lang('CityCover')?>
                    </h1>
                    <h2 class="wow fadeIn">
                        <?=lang('SoonCover')?>
                    </h2>
                    <a href="#supportedareas" class="areas">
                    <?=lang('Covered')?>
                    </a>
                </div>
            </div>
        </div>

        <section class="page-section text-center">
            <h1 class="section-title" id="supportedareas">
                <?=lang('Covered')?>
            </h1>
            <div class="container">
                <div class="section-desc">

                    <ul class="supported-areas-list">
                        
                            <li>
                                <a href="">
                                    
                                        <?=lang('RIYADH')?>
                                    
                                </a>
                            </li>
        
                        
                           
                        
                            <li>
                                <a href="">
                                    
                                        <?=lang('MEDINA')?>
                                    
                                </a>
                            </li>
                        
                            
                        
                        
                            <li>
                                <a href="">
                                    
                                        <?=lang('JEDDAH')?>
                                    
                                </a>
                            </li>
                        
                            <li>
                                <a href="">
                                    
                                        <?=lang('MAKKAH')?>
                                    
                                </a>
                            </li>
                        
                    </ul>
                </div>
            </div>

        </section>

<?php $this->load->view('front/includes/appslink'); ?>

    </div>


    </div> 

    <!-- footer -->
    <?php $this->load->view('front/includes/footer'); ?>
</body></html>
