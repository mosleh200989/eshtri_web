<?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
<html lang="ar">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="ar" data-lang="ar" class="app">
            <?php else: ?>
<html lang="en">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="en" data-lang="en" class="app">
            <?php endif ?>
<?php $this->load->view('front/includes/header-menu'); ?>
<div class="page-content">

<div class="wrapper">
        <div class="page-banner page-banner-4 page-banner-home">
            <div class="banner-slogan">
                <div class="container">
                    <h1 class="lg-text wow fadeIn">
                        <?=lang('Shopping_overwhelming')?>
                    </h1>
                    <h2 class="section-desc-1">
                       <?=lang('app_dilvery')?>
                    </h2>
                </div>
            </div>
        </div>

<?php $this->load->view('front/includes/appslink'); ?>

        <script type="text/javascript">
            window.onload = function () {
                if (navigator.userAgent.toLowerCase().indexOf("android") > -1) {
                    window.location.href = 'https://play.google.com/store/apps/details?id=com.exa.nanamarket';
                }
                if (navigator.userAgent.toLowerCase().indexOf("iphone") > -1) {
                    window.location.href = 'https://itunes.apple.com/us/app/n-na-markt/id1055344588?l=ar&ls=1&mt=8';
                }
            };
        </script>

    </div>


    </div>

    <!-- footer -->
    <?php $this->load->view('front/includes/footer'); ?>
</body></html>
