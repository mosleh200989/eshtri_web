<head> 
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="generator" content="frappe">
<!--    -->
	<title>اطلب من متجرك المفضل اون لاين مباشرة مع موقع اشتر</title>
	<link rel="alternate" hreflang="en" href="https://eshtri.net">
    <link rel="alternate" hreflang="ar" href="https://eshtri.net">
    <link rel="alternate" hreflang="x-default" href="https://eshtri.net">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

        <style type="text/css">
            body{
                font-family: 'Open Sans', sans-serif !important;
            }
        </style>
  
	<link rel="shortcut icon" href="<?php echo base_url('/files/Icon-Small.png'); ?>" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url('/files/Icon-Small.png'); ?>" type="image/x-icon">
                        
<?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
            <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/front/css/eshtri-web-ar.css'); ?>">
            <?php else: ?>
             <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/front/css/eshtri-web-en.css'); ?>">
            <?php endif ?>
                        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/front/css/eshtri-web-global.css'); ?>">
                    
                        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/front/css/eshtri-web-font.css'); ?>">
                     
                        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/front/css/eshtri-web-smartbanner.css'); ?>">
                        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/front/css/erpnext-web.css'); ?>">
            <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-57x57.png'); ?>">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-60x60.png'); ?>">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-72x72.png'); ?>">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-76x76.png'); ?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-114x114.png'); ?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-120x120.png'); ?>">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-144x144.png'); ?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-152x152.png'); ?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('assets/front/matajer/images/mob_icons/apple-icon-180x180.png'); ?>">
        <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url('assets/front/matajer/images/mob_icons/android-icon-192x192.png'); ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/front/matajer/images/mob_icons/favicon-32x32.png'); ?>">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/front/matajer/images/mob_icons/favicon-96x96.png'); ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/front/matajer/images/mob_icons/favicon-16x16.png'); ?>">
        <link rel="manifest" href="<?php echo base_url('assets/front/matajer/images/mob_icons/manifest.json'); ?>">
    
<meta name="description" content="ما يميز اشترِ هو سهولة الاستخدام، جودة الخدمة وتعدد الخيارات للحصول على متعة تسوق خاصة و شيقة وفرنا لكم أسهل طريقة لشراء المقاضي اونلاين">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo base_url('assets/front/matajer/images/mob_icons/ms-icon-144x144.png'); ?>">
<meta name="theme-color" content="#ffffff">
<meta name="apple-itunes-app" content="app-id=1512181978">
<meta name="google-play-app" content="app-id=com.alama360.eshtri">
<meta name="name" content="اشتر" data-html-block="meta_block">
<meta name="image" content="<?php echo base_url('assets/front/matajer/images/Untitled-14.png'); ?>" data-html-block="meta_block">
<meta property="og:url" content="https://eshtri.net">
<meta property="og:type" content="">
<meta property="og:title" content="اشتر">
<meta property="og:description" content="اطلب من متجرك المفضل اون لاين مباشرة مع موقع اشتر">
<meta name="twitter:card" content="">
<meta name="twitter:site" content="">
<meta name="twitter:title" content="اشتر">
<meta name="twitter:description" content="اطلب من متجرك المفضل اون لاين مباشرة مع موقع اشتر">
<script src="//code.tidio.co/41gfxsxbldgxou0duoyizshquzynaqr7.js" async></script>
<script data-dapp-detection="">
(function() {
  let alreadyInsertedMetaTag = false

  function __insertDappDetected() {
    if (!alreadyInsertedMetaTag) {
      const meta = document.createElement('meta')
      meta.name = 'dapp-detected'
      document.head.appendChild(meta)
      alreadyInsertedMetaTag = true
    }
  }

  if (window.hasOwnProperty('web3')) {
    // Note a closure can't be used for this var because some sites like
    // www.wnyc.org do a second script execution via eval for some reason.
    window.__disableDappDetectionInsertion = true
    // Likely oldWeb3 is undefined and it has a property only because
    // we defined it. Some sites like wnyc.org are evaling all scripts
    // that exist again, so this is protection against multiple calls.
    if (window.web3 === undefined) {
      return
    }
    __insertDappDetected()
  } else {
    var oldWeb3 = window.web3
    Object.defineProperty(window, 'web3', {
      configurable: true,
      set: function (val) {
        if (!window.__disableDappDetectionInsertion)
          __insertDappDetected()
        oldWeb3 = val
      },
      get: function () {
        if (!window.__disableDappDetectionInsertion)
          __insertDappDetected()
        return oldWeb3
      }
    })
  }
})()</script><script>
		window.frappe = {};
		frappe.ready_events = [];
		frappe.ready = function(fn) {
			frappe.ready_events.push(fn);
		}
		window.dev_server = 0;

    
    </script>
</head>
