        <div class="page-navbar">
    <nav class="navbar ">
        <div class="container">
<?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
            <div id="x-nav-logo" class="pull-right">
                <div class="new-logo-wrapper">
                    <a href="<?php echo base_url('/'); ?>">
                        <img src="<?php echo base_url('assets/front/matajer/images/navlogo-en.png'); ?>">
                    </a>
                </div>
            </div>
            <?php else: ?>
             <div id="x-nav-logo" class="pull-left">
                <div class="new-logo-wrapper">
                    <a href="<?php echo base_url('/'); ?>">
                        <img src="<?php echo base_url('assets/front/matajer/images/navlogo-en.png'); ?>">
                    </a>
                </div>
            </div>
            <?php endif ?>
            

            <div class="contact-us-hint">
<?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
            <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english" class="elm__link no-barba">En</a>
            <?php else: ?>
             <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/arabic" class="elm__link no-barba">عربي </a>
            <?php endif ?>
                
            </div>
            <button style="margin: 0" class="navbar-toggle btn btn-link btn-icon" type="button">
                <i>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </i>
            </button>
<?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
            <div id="x-nav-logo" class="pull-right">
		<div class="new-logo-wrapper">
                <a class="navbar-brand" href="<?php echo base_url('/'); ?>">
                    <img src="<?php echo base_url('assets/front/matajer/images/logo-en.png'); ?>">
                </a>
		</div>
            </div>
            <?php else: ?>
             <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo base_url('/'); ?>">
                    <img src="<?php echo base_url('assets/front/matajer/images/logo-en.png'); ?>">
                </a>
            </div>
            <?php endif ?>
            
            <div class="navbar-container">
                <ul class="nav navbar-nav navbar-main">
                    <li>
                        <a href="<?php echo base_url('/'); ?>">

                        </a>
                    </li><li>
                            <a href="<?php echo base_url('/'); ?>" class="">
                                <?=lang('Home')?>
                            </a>
                        </li><li>
                            <a href="<?php echo base_url('/areas'); ?>">
                                <?=lang('CoverageArea')?>
                            </a>
                        </li><li>
                            <a href="<?php echo base_url('/aboutus'); ?>">
                                <?=lang('AboutUs')?>
                            </a>
                        <li>
                            <a href="<?php echo base_url('/contactus'); ?>">
                                <?=lang('ContactUs')?>
                            </a>
                        </li>
			<li>
                            <a href="<?php echo base_url('/app'); ?>">
                                <?=lang('DownloadApp')?>
                            </a>
                        </li>
			<li>
                            <a href="<?php echo base_url('/login'); ?>">
                                <?=lang('Login')?>
                            </a>
                        </li>
			<li class="lang-switcher">
                            <?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
            <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english" class="elm__link no-barba">En</a>
            <?php else: ?>
             <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/arabic" class="elm__link no-barba">عربي </a>
            <?php endif ?>
                        </li>
                    
                </ul>

                <ul class="nav navbar-buttons"><li>
                            <a class="btn btn-outline btn-light" href="<?php echo base_url('/partnership'); ?>">
                                <?=lang('Workwithus')?>
                            </a>
                        </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
