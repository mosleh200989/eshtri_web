<footer class="footer-bottom">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-lg-push-6 col-xs-12 nana-social-links">
        <ul class="nav">
          <li>
              <?=lang('Connect_With_Us')?>
          </li>
        </ul>
        <ul class="social">
          <li>

               <a href="https://www.facebook.com/eshtriapp/" target="_blank" class="btn btn-primary btn-circle btn-icon btn-outline btn-sm"><i class="fa fa-facebook"></i></a>
          
              <a href="https://twitter.com/EShtri?s=09" target="_blank" class="btn btn-primary btn-circle btn-icon btn-outline btn-sm"><i class="fa fa-twitter"></i></a>
          
              <a href="https://www.instagram.com/eshtri_app" target="_blank" class="btn btn-primary btn-circle btn-icon btn-outline btn-sm"><i class="fa fa-instagram"></i></a>
                    
          </li>
        </ul>
      </div>
        <div class="col-lg-6 col-lg-pull-6 col-xs-12">
        <p class="footer-copyright">
            <?=lang('Copyright')?>
        </p>
            <p class="termsprivacy">
                            <a href="<?php echo base_url('/privacy'); ?>">
                                <?=lang('Privacy')?>
                            </a>
                            -
                            <a href="<?php echo base_url('/terms'); ?>">
                                <?=lang('Terms')?>
                            </a>
                            -
                            <a href="<?php echo base_url('/aboutus'); ?>">
                                <?=lang('AboutUs')?>
                            </a>
                             
            </p>
      </div>
    </div>
  </div>
</footer><!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
        <!-- js should be loaded in body! -->
        <script type="text/javascript" src="<?php echo base_url('assets/front/frappe/js/lib/jquery/jquery.min.js'); ?>"></script>
      <!--    <script type="text/javascript" src="<?php echo base_url('assets/front/js/frappe-web.min.js'); ?>"></script>
	
	   
	    <script type="text/javascript" src="<?php echo base_url('assets/front/js/nana-web-libraries.js'); ?>"></script>
	    <script type="text/javascript" src="<?php echo base_url('assets/front/js/nana-web-main.js'); ?>"></script>
	    <script type="text/javascript" src="<?php echo base_url('assets/front/js/frappe-web.min.js'); ?>"></script>
	    <script type="text/javascript" src="<?php echo base_url('assets/front/js/erpnext-web.min.js'); ?>"></script>


	<script>frappe.csrf_token = "None";</script>! -->
<script type="text/javascript">
  $(".navbar-toggle").click(function(){
      $("body").toggleClass("navbar-open");
      $(this).toggleClass("active");
    });
</script>