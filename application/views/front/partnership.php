<?php if ($this->session->userdata('site_lang') == 'arabic'): ?>
<html lang="ar">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="ar" data-lang="ar" class="partnership">
            <?php else: ?>
<html lang="en">
<?php $this->load->view('front/includes/header'); ?>
<body data-path="en" data-lang="en" class="partnership">
            <?php endif ?>
<?php $this->load->view('front/includes/header-menu'); ?>
<div class="page-content">

<div class="wrapper">
        <div class="page-banner page-banner-2 page-banner-home page-banner-addbusiness">
            <div class="banner-slogan">
                <div class="container">
                    <h1 class="slogan-title wow fadeIn">
                        <?=lang('PAddurStore')?>
                    </h1>
                    <h2 class="slogan-desc wow fadeIn">
                        <?=lang('increaseursale')?>
                    </h2>
                </div>
            </div>
        </div>

        <!-- amer -->
        <div class="page-section ">
            <div class="container">

                <div class="row">
                    <div class="col-md-12 col-sm-12 m-t-60 ">
                        <p class="section-desc text-center">
                         <?=lang('greatenefit')?>
                        </p>

                    </div>
                </div>
            </div>
        </div>

        <section class="page-section section-center v-padding-40">
            <div class="container">
                <h2 class="section-title">
                    <?=lang('Benefits')?>
                </h2>
                <div class="features features-icon-top features-center">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="feature">
                                <img class="wow fadeIn" src="<?php echo base_url('assets/front/matajer/images/persons.png');?>">
                                <h4> <?=lang('Reach')?></h4>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="feature">
                                <img class="wow fadeIn" src="<?php echo base_url('assets/front/matajer/images/money.png');?>">
                                <h4>
                                    <?=lang('profits')?>
                                </h4>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="feature">
                                <img class="wow fadeIn" src="<?php echo base_url('assets/front/matajer/images/mcommerce.png');?>">
                                <h4>
                                    <?=lang('e-commerce')?>
                                </h4>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="feature">
                                <img class="wow fadeIn" src="<?php echo base_url('assets/front/matajer/images/marketing.png');?>">
                                <h4>
                                    <?=lang('Freemarketing')?>
                                </h4>
                            </div>
                        </div>

                    </div>
                    <div class="text-center"> <?=lang('Inshort')?>
                    </div>

                </div>
            </div>
        </section>

        <div class="page-section ">
            <div class="container">

                <div class="row">
                    <div class="col-md-12 col-sm-12">
<!--                        <a targer="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSfapEj85SGksOQ4YWHdyLXo9FXL6ZQeU3VfDUOsYItag2o2ow/viewform" class="btn add-buiness-btn-inner center-xs">-->
<!--                           --><?//=lang('PAddurStore')?>
<!--                        </a> -->
                        <a targer="_blank" href="<?php echo base_url('register');?>" class="btn add-buiness-btn-inner center-xs">
                           <?=lang('PAddurStore')?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
<?php $this->load->view('front/includes/appslink'); ?>

    </div>


    </div>

    <!-- footer -->
    <?php $this->load->view('front/includes/footer'); ?>
</body></html>
