<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Frontend Controller which extends PS main Controller
 * 1) Loading Template
 */
class FE_Controller extends PS_Controller {


	/**
	 * constructs required variables
	 * 1) template path
	 * 2) base url
	 * 3) site url
	 *
	 * @param      <type>  $auth_level   The auth level
	 * @param      <type>  $module_name  The module name
	 */
	function __construct($auth_level, $module_name )
	{
		parent::__construct( $auth_level, $module_name );
        $this->model = $this->{$model};
		// template path
		$this->template_path = $this->config->item( 'fe_view_path' );

		// base url & site url
		$fe_url = $this->config->item( 'fe_url' );

		if ( !empty( $fe_url )) {
		// if fe controller path is not empty,
			
			$this->module_url = $fe_url .'/'. $this->module_url;
		}

		// load meta data
		$this->load_metadata();

		// load widget library
		$this->load->library( 'PS_Widget' );
		$this->ps_widget->set_template_path( $this->template_path );
	}

	/**
	 * returns site url for controller
	 *
	 * @param      boolean  $path   The path
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	function module_site_url( $path = false )
	{
		if ( $path ) {
		// if the path is exists,
			
			return site_url( $path );
		}

		return site_url();
	}

	function insert_images_icon_and_cover( $files, $img_type, $img_parent_id, $type )
	{
		
		// return false if the image type is empty
		if ( empty( $img_type )) return false;

		// return false if the parent id is empty
		if ( empty( $img_parent_id )) return false;

		
		if($type == "cover") {
			
			// upload images
			$upload_data = $this->ps_image->upload_cover( $files );
				
			if ( isset( $upload_data['error'] )) {
			// if there is an error in uploading

				// set error message
				$this->data['error'] = $upload_data['error'];
				
				return;
			}
			$image = array(
				'img_parent_id'=> $img_parent_id,
				'img_type' => $img_type,
				'img_desc' => "",
				'img_path' => $upload_data[0]['file_name'],
				'img_width'=> $upload_data[0]['image_width'],
				'img_height'=> $upload_data[0]['image_height']
			);
			
			if ( ! $this->Image->save( $image )) {
			// if error in saving image
				
				// set error message
				$this->data['error'] = get_msg( 'err_model' );
				
				return false;
			}
		} else if($type == "icon") {

			// upload images
			$upload_data = $this->ps_image->upload_icon( $files );
				
			if ( isset( $upload_data['error'] )) {
			// if there is an error in uploading

				// set error message
				$this->data['error'] = $upload_data['error'];
				
				return;
			}
			$image = array(
				'img_parent_id'=> $img_parent_id,
				'img_type' => $img_type,
				'img_desc' => "",
				'img_path' => $upload_data[0]['file_name'],
				'img_width'=> $upload_data[0]['image_width'],
				'img_height'=> $upload_data[0]['image_height']
			);

			
			if ( ! $this->Image->save( $image )) {
			// if error in saving image
				
				// set error message
				$this->data['error'] = get_msg( 'err_model' );
				
				return false;
			}

		}
		
		return true;
	}

    function get_all_related_product_trending( $conds = array(), $limit = false, $offset = false )
    {

        // where clause
        // inner join with products and touches
        $this->db->select("prd.*");
        $this->db->from('mk_products as prd');
        $this->db->join('mk_touches as tou', 'prd.id = tou.type_id');
        $this->db->where( "tou.type_name", "product");
        $this->db->where( "prd.status", "1" );
        $this->db->where( "tou.type_id !=", $conds['id']);
        $this->db->where( "prd.cat_id =", $conds['cat_id']);

        $this->db->group_by("tou.type_id");
        $this->db->order_by("count(DISTINCT tou.id)", "DESC");

        if ( $limit ) {
            // if there is limit, set the limit

            $this->db->limit($limit);
        }

        if ( $offset ) {
            // if there is offset, set the offset,
            $this->db->offset($offset);
        }

        return $this->db->get();

    }

    function timeslotcustom()
    {
        $start = date('Y-m-d', strtotime('+1 day'));
        $end = date('Y-m-d', strtotime('+5 day'));
        $datesArr=getDatesFromRange($start, $end);
        $finaldata = array();
        foreach ($datesArr as $singleDate) {
            $timeslots = $this->Timeslotsdetails->get_all_by(array('slot_date' => $singleDate))->result();
            foreach ($timeslots as $singletimeslot) {
                $singletimeslot->start_time=str_replace("AM","ص",$singletimeslot->start_time);
                $singletimeslot->start_time=str_replace("PM","م",$singletimeslot->start_time);

                $singletimeslot->end_time=str_replace("AM","ص",$singletimeslot->end_time);
                $singletimeslot->end_time=str_replace("PM","م",$singletimeslot->end_time);
            }

            $finaldata[] = array('id' => date('d', strtotime($singleDate)), 'slot_date' => $singleDate, 'day_name' => get_msg(''.date('l', strtotime($singleDate)).''),'timeslots' => $timeslots);
        }

        return $finaldata;
    }


}