<?php
class LanguageLoader
{
    function initialize() {
        $ci =& get_instance();
        $ci->load->helper('language');
        $siteLang = $ci->session->userdata('site_lang');
        if ($siteLang) {
	$ci->lang->load('header',$siteLang);
	$ci->lang->load('footer',$siteLang);
	$ci->lang->load('home',$siteLang);
	$ci->lang->load('contactus',$siteLang);
	$ci->lang->load('aboutus',$siteLang);
	$ci->lang->load('app',$siteLang);
	$ci->lang->load('areas',$siteLang);
	$ci->lang->load('partnership',$siteLang);
	$ci->lang->load('privacy',$siteLang);
        } else {
        $ci->lang->load('header','arabic');
	$ci->lang->load('footer','arabic');
	$ci->lang->load('home','arabic');
	$ci->lang->load('contactus','arabic');
	$ci->lang->load('aboutus','arabic');
	$ci->lang->load('app','arabic');
	$ci->lang->load('areas','arabic');
	$ci->lang->load('partnership','arabic');
	$ci->lang->load('privacy','arabic');
        }
    }
}
